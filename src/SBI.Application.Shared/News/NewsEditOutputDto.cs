﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using CRM.Application.Shared.AttachmentDocuments;
using CRM.Localization;

namespace CRM.Application.Shared.News
{
    public class NewsEditOutputDto : AuditedEntityDto<long>, IPassivable
    {
        public Guid UniqueId { get; set; }
        public List<LocalizationDto> Subjects { get; set; }
        public List<LocalizationDto> Contents { get; set; }
        public List<LocalizationDto> ShortDescriptions { get; set; }
        public int CategoryId { get; set; }
        public NewsCategoryOutputDto Category { get; set; }
        public int SortOrder { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsPublic { get; set; }
        public bool IsActive { get; set; }
        public AttachmentDocumentOutputDto File { get; set; }
    }
}
