﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using JetBrains.Annotations;

namespace CRM.Application.Shared.ClientInquiries
{
    public class PagedClientInquiryResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
        public bool? IsActive { get; set; }
    }
}

