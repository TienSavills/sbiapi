﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using CRM.Localization;

namespace CRM.Application.Shared.News
{
    public class NewsUpdateDto : EntityDto<long>, IPassivable
    {
        public List<LocalizationDto> Subjects { get; set; }
        public List<LocalizationDto> Contents { get; set; }
        public List<LocalizationDto> ShortDescriptions { get; set; }
        public int CategoryId { get; set; }
        public int SortOrder { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsPublic { get; set; }
    }
}
