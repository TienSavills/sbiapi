﻿using System;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using CRM.Application.Shared.AttachmentDocuments;
using CRM.Application.Shared.Users;

namespace CRM.Application.Shared.News
{
    public class NewsOutputDto : AuditedEntityDto<long>, IPassivable
    {
        public Guid UniqueId { get; set; }
        public Guid SubjectNameId { get; set; }
        public Guid ContentNameId { get; set; }
        public int CategoryId { get; set; }
        public Guid ShortDescriptionNameId { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string ShortDescription { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public bool IsPublic { get; set; }
        public NewsCategoryOutputDto Category { get; set; }
        public UserOutputDto CreatorUser { get; set; }
        public UserOutputDto LastModifierUser { get; set; }
        public AttachmentDocumentOutputDto File { get; set; }
    }
}
