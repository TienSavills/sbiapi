﻿using System;
using Abp.Application.Services.Dto;

namespace CRM.Application.Shared.News
{
    public class NewsCategoryOutputDto : EntityDto<int>
    {
        public Guid NameId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int SortOrder { get; set; }
    }
}
