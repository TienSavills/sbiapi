﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using CRM.Localization;

namespace CRM.Application.Shared.News
{
    public class NewsCreateDto : IPassivable
    {
        public List<LocalizationDto> Subjects { get; set; }
        public List<LocalizationDto> Contents { get; set; }
        public List<LocalizationDto> ShortDescriptions { get; set; }
        public int CategoryId { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsAllProject { get; set; }
        public bool IsPublic { get; set; }
    }
}
