﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using CRM.Localization;

namespace CRM.Application.Shared.News
{
    public class NewsCategoryInputDto : EntityDto<int>
    {
        public bool IsActive { get; set; }
        public int SortOrder { get; set; }
        public List<LocalizationDto> Names { get; set; }
    }
}
