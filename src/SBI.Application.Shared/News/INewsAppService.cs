﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace CRM.Application.Shared.News
{
    public interface INewsAppService : IApplicationService
    {
        Task<PagedResultDto<NewsOutputDto>> GetAllAsync(PagedNewsResultRequestDto input);
        Task<NewsOutputDto> CreateAsync(NewsCreateDto input);
        Task<NewsOutputDto> UpdateAsync(NewsUpdateDto input);
        Task Active(long id, bool isActive);
        Task Public(EntityDto<long> input, bool isPublic);
        Task<NewsOutputDto> GetAsync(long id);
        Task<NewsEditOutputDto> GetEventForEdit(long id);
    }
}
