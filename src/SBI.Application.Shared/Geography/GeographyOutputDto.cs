﻿namespace CRM.Application.Shared.Geography
{
    public class GeographyOutputDto
    {
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }
}
