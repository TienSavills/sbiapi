﻿namespace CRM.Application.Shared.Geography
{
    public class GeographyInputDto
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
