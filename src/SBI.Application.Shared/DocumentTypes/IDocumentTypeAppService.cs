﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using CRM.Application.Shared.CatTypes;

namespace CRM.Application.Shared.DocumentTypes
{
    public interface IDocumentTypeAppService: IApplicationService
    {
        Task<PagedResultDto<DocumentTypeOutputDto>> GetListDocumentTypes(FilterBasicInputDto input);
        Task<List<DocumentTypeOutputDto>> GetListDocumentTypes(string keyword);
    }
}
