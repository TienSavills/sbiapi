﻿namespace CRM.Application.Shared.DocumentTypes
{
    public class DocumentTypeOutputDto
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}
