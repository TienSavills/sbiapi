﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.RequestContact
{
    public class RequestContactOutputDto
    {
        public long Id { get; set; }
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public long RequestId { get; set; }
        public bool IsActive { get; set; }
    }
}
