﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactOrganizationUnit
{
    public class ContactOrganizationUnitInputDto:Entity<long>
    {
        public long ContactId { get; set; }
        public long OrganizationUnitId { get; set; }
        public bool IsActive { get; set; }
    }
}
