﻿using Abp.Domain.Entities;

namespace CRM.Application.Shared.ListingUsers
{
    public class ListingUserInputDto:Entity<long>
    {
        public long UserId { get; set; }
        public long ListingId { get; set; }
        public bool IsActive { get; set; }
    }
}
