﻿using CRM.Application.Shared.Users;

namespace CRM.Application.Shared.ListingUsers
{
    public class ListingUserOutputDto
    {
        public long Id { get; set; }
        public int ListingId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
    }
}
