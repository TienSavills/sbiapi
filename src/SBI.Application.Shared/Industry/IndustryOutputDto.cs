﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Industry
{
    public class IndustryOutputDto
    {
        public int Id { get; set; }
        public string IndustryName { get; set; }
        public string IndustryDescription { get; set; }
        public int? ParentId { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
