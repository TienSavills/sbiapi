﻿namespace CRM.Application.Shared.Grades
{
    public class GradeInputDto
    {
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string GradeGroup { get; set; }
    }
}
