﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Grades
{
    public class GradeOutputDto
    {
        public  int Id { get; set; } 
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string GradeGroup { get; set; }
    }
}
