﻿using System.Collections.Generic;
using Abp.Application.Services;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Grades
{
    public interface IGradeAppService : IApplicationService
    {

       Task<List<GradeOutputDto>> GetListGrade(string keyword);
    }
}
