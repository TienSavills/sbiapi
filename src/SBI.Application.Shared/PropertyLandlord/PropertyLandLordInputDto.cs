﻿using System.Collections.Generic;
using System.ComponentModel;

namespace CRM.Application.Shared.PropertyLandlord
{
    public class PropertyLandlordInputDto
    {
        public List< long> PropertyLandlordIds { get; set; }
    }
}
