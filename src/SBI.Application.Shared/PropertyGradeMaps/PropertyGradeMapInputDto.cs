﻿using System.Collections.Generic;

namespace CRM.Application.Shared.PropertyGradeMaps
{
    public class PropertyGradeMapInputDto
    {
        public List<int> GradeIds { get; set; }
    }
}
