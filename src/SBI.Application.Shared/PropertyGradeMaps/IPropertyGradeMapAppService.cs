﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.PropertyGradeMaps
{
    public interface IPropertyGradeMapAppService : IApplicationService
    {
        Task<long> CreatePropertyGradeMap(PropertyGradeMapInputDto input);
        List<PropertyGradeMapOutputDto> GetListPropertyGradeMap(long PropertyId);
        Task<long> UpdatePropertyGradeMap(long id, PropertyGradeMapInputDto input);
    }
}
