﻿namespace CRM.Application.Shared.PropertyGradeMaps
{
    public class PropertyGradeMapOutputDto
    {
        public long Id { get; set; }
        public int GradeId { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string GradeGroup { get; set; }
    }
}
