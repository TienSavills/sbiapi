﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Payment
{
    public class PaymentOutputDto
    {
        public long? Id { get; set; }
        public long DealId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal EstPercent { get; set; }
        public DateTime EstDate { get; set; }
        public decimal EstAmount { get; set; }
        public decimal VATAmount { get; set; }
        public DateTime? ActDate { get; set; }
        public decimal? ActAmount { get; set; }
        public decimal? UsdAmount { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string PaymentTimeLine { get; set; }
    }
}
