﻿namespace CRM.Application.Shared.InquiryCategory
{
    public class InquiryCategoryOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
