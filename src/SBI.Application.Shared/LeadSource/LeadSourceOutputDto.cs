﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.LeadSource
{
    public class LeadSourceOutputDto
    {
        public int Id { get; set; }
        public string LeadSourceName { get; set; }
        public string LeadSourceDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
