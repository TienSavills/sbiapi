﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactEmail
{
    public class ContactEmailOutputDto
    {
        public long Id { get; set; }
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? IsOptedOut { get; set; }
        public bool? IsInvalid { get; set; }
        public bool IsActive { get; set; }
    }
}
