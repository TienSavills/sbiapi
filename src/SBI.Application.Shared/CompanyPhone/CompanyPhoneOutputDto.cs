﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyPhone
{
    public class CompanyPhoneOutputDto
    {
        public long? Id { get; set; }
        public int CompanyId { get; set; }
        public Entities.Company.Company Company { get; set; }
        public int? PhoneTypeId { get; set; }
        public string PhoneTypeName { get; set; }
        public bool IsPrimary { get; set; }
        public Entities.OtherCategory.OtherCategory PhoneType { get; set; }
        public string Phone { get; set; }
        public string PhoneCode { get; set; }
        public int? CountryId { get; set; }
        public bool IsActive { get; set; }
    }
}
