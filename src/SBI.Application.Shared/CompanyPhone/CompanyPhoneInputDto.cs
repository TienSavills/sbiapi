﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyPhone
{
    public class CompanyPhoneInputDto:Entity<long>
    {
        public long CompanyId { get; set; }
        public int? PhoneTypeId { get; set; }
        public string Phone { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
        public int? CountryId { get; set; }
    }
}
