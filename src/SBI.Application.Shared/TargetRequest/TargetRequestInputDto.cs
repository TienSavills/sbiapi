﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.TargetRequest
{
    public class TargetRequestInputDto
    {
        public long? Id { get; set; }
        public long TargetId { get; set; }
        public long RequestId { get; set; }
        public bool IsActive { get; set; }
    }
}
