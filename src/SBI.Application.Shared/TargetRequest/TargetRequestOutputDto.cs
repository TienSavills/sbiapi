﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.TargetRequest
{
    public class TargetRequestOutputDto
    {
        public long? Id { get; set; }
        public long TargetId { get; set; }
        public long RequestId { get; set; }
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string Title { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
    }
}
