﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityCategory
{
    public class OpportunityCategoryOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int? ParentId { get; set; }
        public string TypeCode { get; set; }
        public string TargetCode { get; set; }
        public bool IsActive { get; set; }
    }
}
