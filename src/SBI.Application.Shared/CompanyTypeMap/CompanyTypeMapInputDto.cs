﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyTypeMap
{
    public class CompanyTypeMapInputDto:Entity<long>
    {
        public long CompanyId { get; set; }
        public int CompanyTypeId { get; set; }
        public bool? IsActive { get; set; }
    }
}
