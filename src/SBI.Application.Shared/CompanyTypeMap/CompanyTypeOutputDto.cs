﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyTypeMap
{
    public class CompanyTypeMapOutputDto:Entity<long>
    {
        public long CompanyId { get; set; }
        public int CompanyTypeId { get; set; }
        public string CompanyTypeName { get; set; }
        public bool? IsActive { get; set; }
    }
}
