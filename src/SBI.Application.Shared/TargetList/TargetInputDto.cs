﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.TargetList
{
    public class TargetInputDto
    {
        public long? Id { get; set; }
        public string TargetName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public List<long?> contactIds { get; set; }
        public List<long?> requestIds { get; set; }
    }
}
