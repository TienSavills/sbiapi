﻿using CRM.Application.Shared.TargetContact;
using CRM.Application.Shared.TargetRequest;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.TargetList
{
    public class TargetOutputDto
    {
        public long? Id { get; set; }
        public string TargetName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsStatic { get; set; }
        public long CreatorUserId { get; set; }
        public string CreatorName { get; set; }
        public string CreatorSurname { get; set; }
        public long? LastModifierUserId { get; set; }
        public string LastModifierName { get; set; }
        public string LastModifierSurname { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
    }
}
