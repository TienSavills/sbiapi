﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.TargetContact;
using CRM.Application.Shared.TargetRequest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.TargetList
{
    public interface ITargetListAppService:IApplicationService
    {
        Task<PagedResultDto<TargetOutputDto>> GetListTargets(string input);
        Task<TargetOutputDto> GetTargetDetails(long id);
        Task<TargetOutputDto> CreateOrUpdateAsync(TargetInputDto input);
        Task<PagedResultDto<TargetContactOutputDto>> GetListTargetContacts(string input);
        Task<PagedResultDto<TargetRequestOutputDto>> GetListTargetRequests(string input);
        Task<PagedResultDto<TargetRequestOutputDto>> GetListCampaignRequests(string input);
        Task<PagedResultDto<TargetContactOutputDto>> GetListCampaignContacts(string input);
        Task<List<TargetOutputDto>> GetTargetListByEmail(string email);
        Task<List<TargetContactOutputDto>> CreateOrUpdateTargetByEmail(string email, string targetName);
    }
}
