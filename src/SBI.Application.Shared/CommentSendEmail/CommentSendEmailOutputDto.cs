﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CommentSendEmail
{
    public class CommentSendEmailInputDto
    {
        public long Id { get; set; }
        public long CommentId { get; set; }
        public long UserId { get; set; }
    }
}
