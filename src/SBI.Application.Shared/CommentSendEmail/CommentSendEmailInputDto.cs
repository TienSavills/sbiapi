﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CommentSendEmail
{
    public class CommentSendEmailOutputDto
    {
        public long Id { get; set; }
        public long CommentId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
    }
}
