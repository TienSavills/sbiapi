﻿using System.Collections.Generic;
using System.ComponentModel;

namespace CRM.Application.Shared.PropertyTenant
{
    public class PropertyTenantInputDto
    {
        public long PropertyTenantId { get; set; }
        public int TenantTypeId { get; set; }
        [DefaultValue(true)]
        public bool? IsActive { get; set; }
    }
}
