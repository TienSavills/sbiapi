﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityOrganizationUnit
{
    public class OpportunityOrganizationUnitInputDto:Entity<long>, IPassivable
    {
        public long OpportunityId { get; set; }
        public long OrganizationUnitId { get; set; }
        public int? InstructionId { get; set; }
        public bool IsActive { get; set; }
        public long? FeeAmount { get; set; }
        public bool? IsPrimary { get; set; }
    }
}
