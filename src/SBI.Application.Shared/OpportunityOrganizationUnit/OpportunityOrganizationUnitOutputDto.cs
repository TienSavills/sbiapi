﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Application.Shared.OrganizationUnit;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityOrganizationUnit
{
    public class OpportunityOrganizationUnitOutputDto
    {
        public long Id { get; set; }
        public long OpportunityId { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnitOutputDto OrganizationUnit { get; set; }
        public string OrganizationUnitName { get; set; }
        public int? InstructionId { get; set; }
        public string InstructionName { get; set; }
        public long? FeeAmount { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPrimary { get; set; }
    }
}
