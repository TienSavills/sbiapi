﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.PropertyClaims
{
    public class PropertyClaimOutputDto
    {
        public long Id { get; set; }
        public long PropertyId { get; set; }
        public int? NumberOfUnits { get; set; }
        public int? OccupancyUnit { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? RentPrice { get; set; }
        public bool? IncludedVAT { get; set; }
        public decimal? VATRate { get; set; }
        public string PropertyName { get; set; }
        public DateTime? LaunchingTime { get; set; }
        public bool? IncludedServiceCharge { get; set; }
        public decimal? ServiceChargeRate { get; set; }
        public decimal? LeasableTotalArea { get; set; }
        public decimal? LeasedArea { get; set; }
        public string Description { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime ? ToDate { get; set; }
        public DateTime? DataDate { get; set; }
        public int? ConstructionStatusId { get; set; }
        public bool IsActive { get; set; }
        public string ProjectName { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string StateName { get; set; }
        public DateTime? ExpectedLaunchingTime { get; set; }
        public string QuaterExpectedLaunching { get; set; }
        public string Address { get; set; }
    }
}
