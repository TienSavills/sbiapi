﻿using CRM.Application.Shared.PageHelper.Dto;

namespace CRM.Application.Shared.PropertyClaims
{
    public class PropertyClaimFilterInputDto : PagedInputDto
    {
        public string Keyword { get; set; }
        public long? Id { get; set; }
        public long? PropertyId { get; set; }
    }
}
