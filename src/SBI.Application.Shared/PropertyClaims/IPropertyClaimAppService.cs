﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.PropertyClaims
{
    public interface IPropertyClaimAppService
    {
        Task<PropertyClaimOutputDto> GetPropertyClaimDetail(long propertyId);

        Task<PropertyClaimOutputDto> CreatePropertyClaimAsync(PropertyClaimInputDto input);

        Task<PropertyClaimOutputDto> UpdatePropertyClaimAsync(long propertyId, PropertyClaimInputDto input);

        Task<PagedResultDto<PropertyClaimOutputDto>> FilterPropertyClaimAsync(PropertyClaimFilterInputDto input);

    }
}
