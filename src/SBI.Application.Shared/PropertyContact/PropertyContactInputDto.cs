﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.PropertyContact
{
    public class PropertyContactInputDto
    {
        public long? Id { get; set; }
        public long ContactId { get; set; }
        public bool IsActive { get; set; }
    }
}
