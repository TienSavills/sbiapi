﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.PropertyContact
{
    public class PropertyContactOutputDto
    {
        public long Id { get; set; }
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public long PropertyId { get; set; }
        public bool IsActive { get; set; }
    }
}
