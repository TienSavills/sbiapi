﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.TenantTypes
{
    public class TenantTypeOutputDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string TypeName { set; get; }
    }
}
