﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.TenantTypes
{
    public interface ITenantTypeAppService:IApplicationService
    {
        Task<PagedResultDto<TenantTypeOutputDto>> GetListTenantType(FilterBasicInputDto input);
        Task<List<TenantTypeOutputDto>> GetListTenantType(string keyword);
    }
}
