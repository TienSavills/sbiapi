﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Comment
{
    public interface ICommentAppService:IApplicationService
    {
        Task<PagedResultDto<CommentOutputDto>> GetCommentByReference(int moduleId, long referenceId, FilterBasicInputDto input);
        Task<List<CommentOutputDto>> GetCommentDetails(long commentId);
       Task<CommentOutputDto> CreateCommentAsync(int moduleId, long referenceId, CommentInputDto input, bool isSend = false);
        Task<CommentOutputDto> UpdateCommentAsync(long commentId, CommentInputDto input);
    }
}
