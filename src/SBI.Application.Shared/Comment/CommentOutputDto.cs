﻿using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.CommentSendEmail;
using CRM.Application.Shared.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Comment
{
    public class CommentOutputDto 
    {
        public long Id { get; set; }
        public int ModuleId { get; set; }
        public long ReferenceId { get; set; }
        public long? ParentId { get; set; }
        public string ParentBusinessName { get; set; }
        public string ParentLegalName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long CreatorUserId { get; set; }
        public UserOutputDto CreatorUser { get; set; }
        public int ChildCount { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool? IsSend { get; set; }
        public UserOutputDto LastModifierUser { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public List<CommentSendEmailOutputDto> CommentSendEmail { get; set; }
    }
}
