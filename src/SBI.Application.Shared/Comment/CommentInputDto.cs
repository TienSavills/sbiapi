﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Comment
{
    public class CommentInputDto
    {
        public long? ParentId { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public List<long> UserIds { get; set; }
    }
}
