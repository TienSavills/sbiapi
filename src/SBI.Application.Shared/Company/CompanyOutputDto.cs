﻿using CRM.Application.Shared.Comment;
using CRM.Application.Shared.CompanyEmail;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyPhone;
using CRM.Application.Shared.CompanyUser;
using System;
using System.Collections.Generic;
using System.Text;
using CRM.Application.Shared.CompanyAddress;
using CRM.Application.Shared.CompanyTypeMap;
using CRM.Application.Shared.CompanyContact;

namespace CRM.Application.Shared.Company
{
    public class CompanyOutputDto
    {
        public long Id { get; set; }
        public int? ParentId { get; set; }
        public string ParentBusinessName { get; set; }
        public string ParentLegalName { get; set; }
        public string Capid { get; set; }
        public string LegalName { get; set; }
        public string BusinessName { get; set; }
        public string Vatcode { get; set; }
        public int? ClientTypeId { get; set; }
        public string ClientTypeName { get; set; }
        public int? IndustryId { get; set; }
        public string IndustryName { get; set; }
        public int? IndustryLevel2Id { get; set; }
        public string IndustryLevel2Name { get; set; }
        public int? LeadSourceId { get; set; }
        public string LeadSourceName { get; set; }
        public string Website { get; set; }
        public string KeyClientManagement { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsPersonal { get; set; }
        public int? TypeId { get; set; }
        public string TypeName { get; set; }
        public string SicCode { get; set; }
        public string CreatorUserName { get; set; }
        public DateTime CreationTime { get; set; }
        public string LastModifierUserName { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public bool? IsVerified { get; set; }
        public int? NationalityId { get; set; }
        public string NationalityName { get; set; }
        public List<CompanyPhoneOutputDto> CompanyPhone { get; set; }
        public List<CompanyAddressOutputDto> CompanyAddress { get; set; }
        public List<CompanyTypeMapOutputDto> CompanyTypeMap { get; set; }
        public List<CompanyContactOutputDto> CompanyContact { get; set; }
        public List<CompanyUserV1OutputDto> CompanyUser { get; set; }
        public List<CompanyOrganizationUnitV1OutputDto> CompanyOrganizationUnit { get; set; }
    }
}
