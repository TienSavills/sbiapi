﻿using CRM.Application.Shared.ContactTypeMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Company
{
    public class CompanyInputDto
    {
        public long? Id { get; set; }
        public int? ParentId { get; set; }
        public string Capid { get; set; }
        public string LegalName { get; set; }
        public string BusinessName { get; set; }
        public string Vatcode { get; set; }
        public int? ClientTypeId { get; set; }
        public int? IndustryId { get; set; }
        public int? IndustryLevel2Id { get; set; }
        public int? LeadSourceId { get; set; }
        public string Website { get; set; }
        public string KeyClientManagement { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPersonal { get; set; }
        public int? TypeId { get; set; }
        public string SicCode { get; set; }
        public bool? IsVerified { get; set; }
        public int? NationalityId { get; set; }
    }
}
