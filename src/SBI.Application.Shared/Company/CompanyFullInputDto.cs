﻿using CRM.Application.Shared.CompanyAddress;
using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyPhone;
using CRM.Application.Shared.CompanyTypeMap;
using CRM.Application.Shared.CompanyUser;
using CRM.Application.Shared.ContactTypeMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Company
{
    public class CompanyFullInputDto
    {
        public long? Id { get; set; }
        public int? ParentId { get; set; }
        public string Capid { get; set; }
        public string LegalName { get; set; }
        public string BusinessName { get; set; }
        public string Vatcode { get; set; }
        public int? ClientTypeId { get; set; }
        public int? IndustryId { get; set; }
        public int? IndustryLevel2Id { get; set; }
        public int? LeadSourceId { get; set; }
        public string Website { get; set; }
        public string KeyClientManagement { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPersonal { get; set; }
        public int? TypeId { get; set; }
        public string SicCode { get; set; }
        public bool? IsVerified { get; set; }
        public int? NationalityId { get; set; }
        public List<CompanyAddressInputDto> CompanyAddress  { get; set; } 
        public List<CompanyContactInputDto> CompanyContact { get; set; }
        public List<int> CompanyTypeIds { get; set; }
        public List<CompanyPhoneInputDto> CompanyPhone { get; set; }
        public List<CompanyUserInputDto> CompanyUser { get; set; }
        public List<CompanyOrganizationUnitInputDto> CompanyOrganizationUnit { get; set; }
        public List<long> CompanyOrganizationUnitIds { get; set; }
        public List<long> CompanyUserIds { get; set; }


    }
}
