﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.CompanyEmail;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyPhone;
using CRM.Application.Shared.CompanyUser;
using System.Collections.Generic;
using System.Threading.Tasks;
using CRM.Application.Shared.CompanyAddress;
using CRM.Application.Shared.CompanyTypeMap;

namespace CRM.Application.Shared.Company
{
    public interface ICompanyAppService: IApplicationService
    {
        Task<PagedResultDto<CompanyOutputDto>> GetListCompany(string filterJson);
        Task<PagedResultDto<CompanyEmailOutputDto>> GetListCompanyEmail(FilterBasicInputDto input,long companyId);
        Task<PagedResultDto<CompanyPhoneOutputDto>> GetListCompanyPhone(FilterBasicInputDto input,long companyId);
        Task<PagedResultDto<CompanyOrganizationUnitOutputDto>> GetListCompanyOrganizationUnit(FilterBasicInputDto input,long companyId);
        Task<PagedResultDto<CompanyUserOutputDto>> GetListCompanyUser(FilterBasicInputDto input, long companyId);
        Task<PagedResultDto<CompanyContactOutputDto>> GetListCompanyContact(FilterBasicInputDto input, long companyId);
        Task<CompanyOutputDto> GetCompanyDetails(long id);
        Task<CompanyOutputDto> CreateCompanyAsync(CompanyInputDto input);
        Task<CompanyOutputDto> CreateOrUpdateCompanyFullAsync(CompanyFullInputDto input);
        Task<CompanyOutputDto> UpdateCompanyAsync(long companyId, CompanyInputDto input);
        Task<CompanyOutputDto> UpdateCompanyAddressAsync(long companyId,List<CompanyAddressInputDto> companyAddress);
        List<CompanyPhoneOutputDto> CreateOrUpdateCompanyPhone(long companyId, List<CompanyPhoneInputDto> companyPhones);
        List<CompanyTypeMapOutputDto> CreateOrUpdateCompanyTypeMap(List<CompanyTypeMapInputDto> companyTypeMapIds);
        List<CompanyEmailOutputDto> CreateOrUpdateCompanyEmail(long companyId, List<CompanyEmailInputDto> companyEmails);
        List<CompanyUserOutputDto> CreateOrUpdateCompanyUser(long companyId, List<CompanyUserInputDto> companyUsers);
        List<CompanyContactOutputDto> CreateOrUpdateCompanyContact(long companyId, List<CompanyContactInputDto> input);
        List<CompanyOrganizationUnitOutputDto> CreateOrUpdateCompanyOrganizationUnit(long companyId, List<CompanyOrganizationUnitInputDto> companyOrganizationUnits);
        Task<bool> MergeCompany(List<long> sourceCompanyIds, long targetCompanyId);
    }
}
