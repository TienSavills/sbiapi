﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.DealAdjust
{
    public class DealAdjustOutputDto
    {
        public long Id { get; set; }
        public long DealId { get; set; }
        public long? OrganizationUnitId { get; set; }
        public string OrganizationUnitName { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IsActive { get; set; }
    }
}
