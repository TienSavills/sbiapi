﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.DealAdjust
{
    public class DealAdjustInputDto:Entity<long>
    {
        public long DealId { get; set; }
        public long OrganizationUnitId { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
