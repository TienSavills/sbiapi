﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.ProjectGradeMaps
{
    public interface IProjectGradeMapAppService : IApplicationService
    {
        Task<long> CreateProjectGradeMap(ProjectGradeMapInputDto input);
        List<ProjectGradeMapOutputDto> GetListProjectGradeMap(long projectId);
        Task<long> UpdateProjectGradeMap(long id, ProjectGradeMapInputDto input);
    }
}
