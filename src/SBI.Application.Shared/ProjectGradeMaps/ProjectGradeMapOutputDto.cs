﻿namespace CRM.Application.Shared.ProjectGradeMaps
{
    public class ProjectGradeMapOutputDto
    {
        public long Id { get; set; }
        public int GradeId { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string GradeGroup { get; set; }
    }
}
