﻿using System.Collections.Generic;

namespace CRM.Application.Shared.ProjectGradeMaps
{
    public class ProjectGradeMapInputDto
    {
        public List<int> GradeIds { get; set; }
    }
}
