﻿using System.Collections.Generic;

namespace CRM.Application.Shared.ProjectTenant
{
    public class ProjectTenantInputDto
    {
       public long ProjectTenantId { get; set; }
       public int TenantTypeId { get; set; }
    }
}
