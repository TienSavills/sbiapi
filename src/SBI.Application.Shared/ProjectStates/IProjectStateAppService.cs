﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.ProjectStates
{
    public interface IProjectStateAppService:IApplicationService
    {
        Task<PagedResultDto<ProjectStateOutputDto>> GetListProjectStates(FilterBasicInputDto input);
        Task<List<ProjectStateOutputDto>> GetListProjectStates(string keyword);

    }
}
