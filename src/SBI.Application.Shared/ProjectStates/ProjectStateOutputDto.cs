﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ProjectStates
{
    public class ProjectStateOutputDto
    {
        public int Id { get; set; }
        public string ProjectStateName { get; set; }
        public bool IsActive { get; set; }

    }
}
