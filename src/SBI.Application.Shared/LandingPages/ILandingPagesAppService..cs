﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.Deal;
using CRM.Application.Shared.DealInvoiceInfo;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;

namespace CRM.Application.Shared.LandingPages
{
    public interface ILandingPagesAppService :IApplicationService
    {
    }
}
