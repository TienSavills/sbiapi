﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyContact
{
    public class ContactCompanyOutputDto
    {
        public long Id { get; set; }
        public long CompanyId { get; set; }
        public string BusinessName { get; set; }
        public bool? IsPrimary { get; set; }
        public string LegalName { get; set; }
        public string Title { get; set; }
        public long ContactId { get; set; }
        public bool IsActive { get; set; }
    }
}
