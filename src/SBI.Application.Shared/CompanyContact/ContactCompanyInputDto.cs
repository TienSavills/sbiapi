﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyContact
{
    public class ContactCompanyInputDto:Entity<long>
    {
        public long ContactId { get; set; }
        public long CompanyId { get; set; }
        public string Title { get; set; }
        public bool? IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
