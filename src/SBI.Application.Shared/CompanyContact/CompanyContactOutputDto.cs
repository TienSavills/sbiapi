﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyContact
{
    public class CompanyContactOutputDto
    {
        public long Id { get; set; }
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public long CompanyId { get; set; }
        public bool IsActive { get; set; }
    }
}
