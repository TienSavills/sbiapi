﻿namespace CRM.Application.Shared.TenureTypes
{
    public class TenureTypeOutputDto
    {
        public int Id { get; set; }
        public string TenureTypeName { get; set; }
    }
}
