﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using CRM.Application.Shared.CatTypes;

namespace CRM.Application.Shared.TenureTypes
{
    public interface ITenureTypeAppService: IApplicationService
    {
        Task<PagedResultDto<TenureTypeOutputDto>> GetListTenureTypes(FilterBasicInputDto input);
        Task<List<TenureTypeOutputDto>> GetListTenureTypes(string keyword);
    }
}
