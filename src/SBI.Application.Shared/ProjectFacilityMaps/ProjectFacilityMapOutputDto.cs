﻿
namespace CRM.Application.Shared.ProjectFacilityMaps
{
    public class ProjectFacilityMapOutputDto
    {
        public long Id { get; set; }
        public int FacilityId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
