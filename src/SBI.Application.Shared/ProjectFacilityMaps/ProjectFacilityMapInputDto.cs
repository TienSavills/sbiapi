﻿using System.Collections.Generic;

namespace CRM.Application.Shared.ProjectFacilityMaps
{
    public class ProjectFacilityMapInputDto
    {
        public List<int> FacilityIds { get; set; }
    }
}
