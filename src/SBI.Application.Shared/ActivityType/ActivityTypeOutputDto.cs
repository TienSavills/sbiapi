﻿using CRM.Application.Shared.Comment;
using CRM.Application.Shared.CompanyEmail;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyPhone;
using CRM.Application.Shared.CompanyUser;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ActivityType
{
    public class ActivityTypeOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
