﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OrganizationUnit
{
    public class OrganizationUnitOutputDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string DisplayName { get; set; }
    }
}
