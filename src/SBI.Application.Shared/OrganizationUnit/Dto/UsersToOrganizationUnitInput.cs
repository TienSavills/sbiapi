﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM.Application.Shared.OrganizationUnit.Dto
{
    public class UsersToOrganizationUnitInput
    {
        public long[] Userid { get; set; }

        [Range(1, long.MaxValue)]
        public long OrganizationUnitId { get; set; }
    }
}