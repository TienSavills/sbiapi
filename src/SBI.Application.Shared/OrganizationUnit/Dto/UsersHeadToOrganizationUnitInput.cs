﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM.Application.Shared.OrganizationUnit.Dto
{
    public class UsersHeadToOrganizationUnitInput
    {
        public long UserId { get; set; }
        public bool IsHead { get; set; }

        [Range(1, long.MaxValue)]
        public long OrganizationUnitId { get; set; }
    }
}