﻿using System;
using Abp.Application.Services.Dto;

namespace CRM.Application.Shared.OrganizationUnit.Dto
{
    public class OrganizationUnitUserListDto : EntityDto<long>
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }
        public DateTime AddedTime { get; set; }
        public bool IsHead { get; set; }
    }
}