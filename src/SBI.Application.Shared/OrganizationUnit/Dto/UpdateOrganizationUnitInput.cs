using System.ComponentModel.DataAnnotations;
using Abp.Organizations;

namespace CRM.Application.Shared.OrganizationUnit.Dto
{
    public class UpdateOrganizationUnitInput
    {
        [Range(1, long.MaxValue)]
        public long Id { get; set; }

        [Required]
        [StringLength(Abp.Organizations.OrganizationUnit.MaxDisplayNameLength)]
        public string DisplayName { get; set; }
    }
}