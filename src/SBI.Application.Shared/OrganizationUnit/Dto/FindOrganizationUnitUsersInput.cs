﻿using CRM.Application.Shared.Dto;

namespace CRM.Application.Shared.OrganizationUnit.Dto
{
    public class FindOrganizationUnitUsersInput : PagedAndFilteredInputDto
    {
        public long OrganizationUnitId { get; set; }
    }
}
