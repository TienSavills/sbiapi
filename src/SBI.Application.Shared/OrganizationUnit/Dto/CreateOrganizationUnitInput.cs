﻿using System.ComponentModel.DataAnnotations;

namespace CRM.Application.Shared.OrganizationUnit.Dto
{
    public class CreateOrganizationUnitInput
    {
        public long? ParentId { get; set; }

        [Required]
        [StringLength(Abp.Organizations.OrganizationUnit.MaxDisplayNameLength)]
        public string DisplayName { get; set; } 
    }
}