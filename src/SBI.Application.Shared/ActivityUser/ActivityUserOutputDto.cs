﻿using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.Users;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ActivityUser
{
    public class ActivityUserOutputDto
    {
        public long Id { get; set; }
        public int ActivityId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public UserOutputDto User { get; set; }
        public bool? IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
