﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Application.Shared.ActivityUser
{
    public class ActivityUserInputDto
    {
        public long? Id { get; set; }
        public long UserId { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
