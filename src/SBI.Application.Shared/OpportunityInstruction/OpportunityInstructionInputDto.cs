﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityInstruction
{
    public class OpportunityInstructionInputDto
    {
        public long? Id { get; set; }
        public long InstructionId { get; set; }
        public bool IsActive { get; set; }
    }
}
