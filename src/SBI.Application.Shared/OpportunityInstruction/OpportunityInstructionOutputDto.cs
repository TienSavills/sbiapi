﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityInstruction
{
    public class OpportunityInstructionOutputDto
    {
        public long Id { get; set; }
        public int InstructionId { get; set; }
        public string InstructionName { get; set; }
        public long OpportunityId { get; set; }
        public bool IsActive { get; set; }
    }
}
