﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.UnitHistory
{
    public class UnitHistoryOutputDto
    {
        public long? Id { get; set; }
        public long FloorId { get; set; }
        public string FloorName { get; set; }
        public string ProjectName { get; set; }
        public long ProjectId { get; set; }
        public long OrgTenantId { get; set; }
        public string TenantLegalName { get; set; }
        public string TenantBusinessName { get; set; }
        public long UnitId { get; set; }
        public string UnitName { get; set; }
        public int? Cars { get; set; }
        public int? Motorbikes { get; set; }
        public string ItServices { get; set; }
        public string BackupPower { get; set; }
        public decimal? Space { get; set; }
        public int? HeadCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string ReasonMove { get; set; }
        public bool IsActive { get; set; }
        public decimal? Monthly { get; set; }
        public decimal? Per { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierUserName { get; set; }
        public string Description { get; set; }
        public int? Incentives { get; set; }
        //public int? Term { get; set; }
        public decimal? NetEffectiveRental { get; set; }
    }
}
