﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.UnitHistory
{
    public interface IUnitHistoryAppService: IApplicationService
    {
        Task<List<UnitHistoryOutputDto>> GetListUnitHistory(long unitId);
        Task<List<UnitHistoryOutputDto>> GetListUnitHistoryByCompany(long orgTenantId);
        Task<UnitHistoryOutputDto> GetUnitHistoryDetails(long id);
        Task<UnitHistoryOutputDto> UpdateAsync(long id, UnitHistoryInputDto input);
        Task<UnitHistoryOutputDto> CreateAsync(UnitHistoryInputDto input);
        Task DeleteCurrentTenant(long id);
    }
}
