﻿using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.ContactAddress;
using CRM.Application.Shared.ContactEmail;
using CRM.Application.Shared.ContactOrganizationUnit;
using CRM.Application.Shared.ContactPhone;
using CRM.Application.Shared.ContactUser;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Contact
{
    public class ContactValidateInputDto
    {
        public List< ContactPhoneInputDto> ContactPhones { get; set; }
        public List<ContactCompanyInputDto> ContactCompany { get; set; }
        public List<ContactAddressInputDto> ContactAddress { get; set; }
        public List<ContactEmailInputDto> ContactEmails { get; set; }
        public List<ContactUserInputDto> ContactUsers { get; set; }
        public List<ContactOrganizationUnitInputDto> ContactOrganizationUnits { get; set; }
    }
}
