﻿using CRM.Application.Shared.Property.Dto;
using CRM.Application.Shared.Request;
using System;
using System.Collections.Generic;
using System.Text;
using CRM.Application.Shared.ContactAddress;

namespace CRM.Application.Shared.Contact
{
    public class ContactOutputDto
    {
        public long Id { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondEmail { get; set; }
        public bool? IsVietnamese { get; set; }
        public string CAPID { get; set; }
        public string Gender { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondPhone { get; set; }
        public string Description { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string Title { get; set; }
        public int? LevelId { get; set; }
        public string Level { get; set; }
        public int? IndustryId { get; set; }
        public string Industry { get; set; }
        public int? ClientTypeId { get; set; }
        public string ClientType { get; set; }
        public int? LeadSourceId { get; set; }
        public string LeadSource { get; set; }
        public bool IsActive { get; set; }
        public long CompanyId { get; set; }
        public string LegalName { get; set; }
        public string BusinessName { get; set; }
        public string PropertyName { get; set; }
        public long? RequestId { get; set; }
        public long? OpportunityId { get; set; }
        public long? PropertyId { get; set; }
        public bool? IsNotCall { get; set; }
        public bool? EmailOptOut { get; set; }
        public bool IsInvalid { get; set; }
        public string CreatorUserName { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public string LastModifierUserName { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public bool? IsVerified { get; set; }
        public string Requestor { get; set; }
        public int? NationalityId { get; set; }
        public string NationalityName { get; set; }
        public List<ContactAddressOutputDto> ContactAddress { get; set; }
        public List<ContactUser.ContactUserV1OutputDto> ContactUser { get; set; }
        public List<ContactEmail.ContactEmailOutputDto> ContactEmail { get; set; }
        public List<ContactPhone.ContactPhoneOutputDto> ContactPhone { get; set; }
        public List<CompanyContact.ContactCompanyOutputDto> ContactCompany { get; set; }
        public List<ContactTypeMap.ContactTypeMapOutputDto> ContactTypeMap { get; set; }
        public List<ContactOrganizationUnit.ContactOrganizationUnitV1OutputDto> ContactOrganizationUnit { get; set; }
    }
}
