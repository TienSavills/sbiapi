﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.ContactEmail;
using CRM.Application.Shared.ContactOrganizationUnit;
using CRM.Application.Shared.ContactPhone;
using CRM.Application.Shared.ContactUser;
using System.Collections.Generic;
using System.Threading.Tasks;
using CRM.Application.Shared.ContactAddress;
using CRM.EntitiesCustom;
using CRM.Application.Shared.ContactTypeMap;

namespace CRM.Application.Shared.Contact
{
    public interface IContactAppService: IApplicationService
    {
        Task<PagedResultDto<ContactOutputDto>> GetListContact (string filterJson);
        Task<List<ContactOutputDto>> FilterContactForTarget(string filterJson);
        Task<PagedResultDto<ContactOutputDto>> GetListExistContact(string filterJson);
        Task<ContactOutputDto> GetContactDetails(long id, bool isShowFull);
        Task<PagedResultDto<ContactCompanyOutputDto>> GetListContactCompany(FilterBasicInputDto input, long contactId);
        Task<ContactOutputDto> CreateContactAsync(ContactInputDto input);
        Task<ContactOutputDto> CreateOrUpdateContactAsync(ContactFullInputDto input);
        Task<ContactOutputDto> UpdateContactAsync(long contactId, ContactInputDto input);
        Task<ContactOutputDto> UpdateContactAddressAsync(long contactId,List<ContactAddressInputDto> contactAddress);
        List<ContactPhoneOutputDto> CreateOrUpdateContactPhone(long contactId, List<ContactPhoneInputDto> contactPhones);
        List<ContactEmailOutputDto> CreateOrUpdateContactEmail(long contactId, List<ContactEmailInputDto> contactEmails);
        List<ContactUserOutputDto> CreateOrUpdateContactUser(long contactId, List<ContactUserInputDto> contactUsers);
        List<ContactOrganizationUnitOutputDto> CreateOrUpdateContactOrganizationUnit(long contactId, List<ContactOrganizationUnitInputDto> contactOrganizationUnits);
        List<ContactTypeMapOutputDto> CreateOrUpdateContactTypeMap(List<ContactTypeMapInputDto> input);
        Task<PagedResultDto<ContactEmailOutputDto>> GetListContactEmail(FilterBasicInputDto input, long contactId);
        Task<PagedResultDto<ContactEmailOutputDto>> GetListStatusContactEmail(FilterBasicInputDto input);
        Task<PagedResultDto<ContactPhoneOutputDto>> GetListContactPhone(FilterBasicInputDto input, long contactId);
        Task<PagedResultDto<ContactOrganizationUnitOutputDto>> GetListContactOrganizationUnit(FilterBasicInputDto input, long contactId);
        Task<PagedResultDto<ContactUserOutputDto>> GetListContactUser(FilterBasicInputDto input, long contactId);
        List<ContactCompanyOutputDto> CreateOrUpdateContactCompany(long contactId, List<ContactCompanyInputDto> input);
        bool CheckExistContact(ContactValidateInputDto input);
        bool RequestShare(long contactId);
        bool ApproveShare(long id);
        bool RejectShare(long id);
        Task<List<ExistContactDto>> GetListPendingApprove(string filterJson);
        Task<bool> MergeContact(List<long> sourceContactIds, long targetContactId);
    }
}
