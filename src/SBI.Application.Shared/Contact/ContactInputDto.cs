﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Contact
{
    public class ContactInputDto
    {
        public long? Id { get; set; }
        public string CAPID { get; set; }
        public string Description { get; set; }
        public string ContactName { get; set; }
        public bool? IsVietnamese { get; set; }
        public bool? IsVerified { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Title { get; set; }
        public int? LevelId { get; set; }
        public int? IndustryId { get; set; }
        public int? ClientTypeId { get; set; }
        public int? LeadSourceId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsNotCall { get; set; }
        public bool? EmailOptOut { get; set; }
        public int? NationalityId { get; set; }
    }
}
