﻿namespace CRM.Application.Shared.UnitFacilityMaps
{
    public class UnitFacilityMapOutputDto
    {
        public long Id { get; set; }
        public int FacilityId { get; set; }
        public long UnitId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
