﻿using System.Collections.Generic;
using Abp.Domain.Entities;

namespace CRM.Application.Shared.UnitFacilityMaps
{
    public class UnitFacilityMapInputDto : Entity<long>
    {
        public int FacilityId { get; set; }
        public long UnitId { get; set; }
        public bool IsActive { get; set; }
    }
}
