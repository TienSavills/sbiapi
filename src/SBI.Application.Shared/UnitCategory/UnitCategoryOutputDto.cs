﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.UnitCategory
{
    public class UnitCategoryOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int SortOrder { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
