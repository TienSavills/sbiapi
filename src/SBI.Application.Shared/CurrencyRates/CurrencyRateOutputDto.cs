﻿using CRM.Entities.Currencys;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CurrencyRates
{
    public class CurrencyRateOutputDto
    {
        public long Id { get; set; }
        public int CurrencyId { get; set; }
        public decimal RateBaseVND { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool IsActive { get; set; }
        public Currency Currency { get; set; }
    }
}
