﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.CurrencyRates
{
    public interface ICurrencyRateAppService:IApplicationService
    {
        Task<List<CurrencyRateOutputDto>> GetListCurrencyRates(CurrencyRateFilterInputDto filters);
        Task<CurrencyRateOutputDto> CreateCurrencyRateAsync(CurrencyRateInputDto input);
        Task<CurrencyRateOutputDto> UpdateCurrencyRateAsync(long id, CurrencyRateInputDto input);
        Task<CurrencyRateOutputDto> GetCurrencyRateDetail(long id);
    }
}
