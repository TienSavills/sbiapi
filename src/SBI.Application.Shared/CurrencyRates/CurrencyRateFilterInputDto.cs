﻿using CRM.Application.Shared.PageHelper.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CurrencyRates
{
    public class CurrencyRateFilterInputDto : PagedInputDto
    {
        public string Keyword { get; set; }
    }
}
