﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CurrencyRates
{
    public class CurrencyRateInputDto
    {
        public int CurrencyId { get; set; }
        public decimal RateBaseVND { get; set; }
        public bool IsActive { get; set; }
    }
}
