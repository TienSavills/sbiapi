﻿
namespace CRM.Application.Shared.ProjectTransportationMaps
{
    public class ProjectTransportationMapOutputDto
    {
        public long Id { get; set; }
        public int TransportationId { get; set; }
        public long ProjectId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
