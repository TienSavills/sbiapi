﻿using System.Collections.Generic;

namespace CRM.Application.Shared.ProjectTransportationMaps
{
    public class ProjectTransportationMapInputDto
    {
        public List<int> TransportationIds { get; set; }
    }
}
