﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactPhone
{
    public class ContactPhoneInputDto:Entity<long>
    {
        public long ContactId { get; set; }
        public int? PhoneTypeId { get; set; }
        public int? CountryId { get; set; }
        public string Phone { get; set; }
        public bool? IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
