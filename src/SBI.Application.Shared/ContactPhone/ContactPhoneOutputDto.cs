﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactPhone
{
    public class ContactPhoneOutputDto
    {
        public long? Id { get; set; }
        public int ContactId { get; set; }
        public Entities.Contacts.Contact Contact { get; set; }
        public string PhoneCode { get; set; }
        public bool? IsPrimary { get; set; }
        public int? PhoneTypeId { get; set; }
        public string PhoneTypeName { get; set; }
        public Entities.OtherCategory.OtherCategory PhoneType { get; set; }
        public int? CountryId { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }
    }
}
