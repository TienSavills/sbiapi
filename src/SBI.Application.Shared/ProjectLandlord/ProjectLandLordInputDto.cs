﻿using System.Collections.Generic;

namespace CRM.Application.Shared.ProjectLandlord
{
    public class ProjectLandlordInputDto
    {
        public List<long> ProjectLandlordIds { get; set; }
    }
}
