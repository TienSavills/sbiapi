﻿using CRM.Application.Shared.Company;

namespace CRM.Application.Shared.ProjectLandlord
{
    public class ProjectLandlordOutputDto
    {
        public long? Id { get; set; }
        public long CompanyId { get; set; }
        public string LegalName { get; set; }
        public string BusinessName { get; set; }
        public string Vatcode { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string Website { get; set; }
        public string KeyClientManagement { get; set; }
        public string Description { get; set; }
        public bool IsPersonal { get; set; }
    }
}
