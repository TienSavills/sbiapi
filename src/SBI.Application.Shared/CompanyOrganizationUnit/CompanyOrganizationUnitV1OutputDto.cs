﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Application.Shared.OrganizationUnit;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyOrganizationUnit
{
    public class CompanyOrganizationUnitV1OutputDto
    {
        public long Id { get; set; }
        public long CompanyId { get; set; }
        public long OrganizationUnitId { get; set; }
        public string OrganizationUnitName{ get; set; }
        public bool IsActive { get; set; }
    }
}
