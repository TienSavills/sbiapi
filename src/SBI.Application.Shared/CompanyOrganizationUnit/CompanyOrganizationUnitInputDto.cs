﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyOrganizationUnit
{
    public class CompanyOrganizationUnitInputDto:Entity<long>
    {
        public long CompanyId { get; set; }
        public long OrganizationUnitId { get; set; }
        public bool IsActive { get; set; }
    }
}
