﻿using System.ComponentModel;
using Abp.Domain.Entities;

namespace CRM.Application.Shared.UnitAddress
{
    public class UnitAddressInputDto : Entity<long>
    {
        public long UnitId { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
    }
}
