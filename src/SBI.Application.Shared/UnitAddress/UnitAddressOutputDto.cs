﻿namespace CRM.Application.Shared.UnitAddress
{
    public class UnitAddressOutputDto
    {
        public long Id { get; set; }
        public  long UnitId { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }
}
