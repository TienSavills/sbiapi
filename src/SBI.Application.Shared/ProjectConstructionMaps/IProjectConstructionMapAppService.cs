﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.ProjectConstructionMaps
{
    public interface IProjectConstructionMapAppService:IApplicationService
    {
        Task<List<ProjectConstructionMapOutputDto>> FilterProjectConstructionMapAsync(ProjectConstructionMapFilterInputDto filters);
        Task<ProjectConstructionMapOutputDto> CreateProjectConstructionMapAsync(ProjectConstructionMapInputDto input);
        Task<ProjectConstructionMapOutputDto> UpdateProjectConstructionMapAsync(long id, ProjectConstructionMapInputDto input);
        Task<ProjectConstructionMapOutputDto> GetProjectConstructionMapDetail(long id);
    }
}
