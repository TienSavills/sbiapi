﻿using CRM.Application.Shared.PageHelper.Dto;

namespace CRM.Application.Shared.ProjectConstructionMaps
{
    public class ProjectConstructionMapFilterInputDto : PagedInputDto
    {
        public string Keyword { get; set; }
        public long? ProjectId { get; set; }
    }
}
