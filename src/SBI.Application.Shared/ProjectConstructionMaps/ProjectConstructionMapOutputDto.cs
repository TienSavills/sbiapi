﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ProjectConstructionMaps
{
    public class ProjectConstructionMapOutputDto
    {
        public long Id { get; set; }
        public long ProjectId { get; set; }
        public int ConstructionStatusId { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime ActualDate { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public Entities.ConstructionStatus.ConstructionStatus ConstructionStatus { get; set; }
        public Entities.Projects.Project Project { get; set; }
    }
}
