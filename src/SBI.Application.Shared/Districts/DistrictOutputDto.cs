﻿namespace CRM.Application.Shared.Districts
{
    public class DistrictOutputDto
    {
        public int Id { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public int ProvinceId { get; set; }
    }
}
