﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Campaign
{
    public class CampaignContactRequestInputDto
    {
        public long CampaignId { get; set; }
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string Title { get; set; }
        public string Gender { get; set; }
    }
}
