﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.ResponseHelper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Campaign
{
    public interface ICampaignAppService:IApplicationService
    {
        Task<PagedResultDto<CampaignOutputDto>> GetListCampaigns(string input);
        Task<CampaignOutputDto> GetCampaignDetails(long id);
        Task<CampaignOutputDto> CreateOrUpdateAsync(CampaignInputDto input);
        Task<List<CampaignContactRequestOutputDto>> GetListCampaignContactRequest(string input);
        Task<PagedResultDto<EmailHistoryOutputDto>> GetListCampaignEmailHistory(long campaignId,string input);
        Task SendEmail(long campaignId, List<CampaignContactRequestInputDto> input);
        Task<TempleteSendgridOutputDto> GetTempleteSendgrid(string templetType);
        Task<ResultApiModel> CheckEmail(string email);
    }
}
