﻿using CRM.Application.Shared.CampaignTarget;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Campaign
{
    public class CampaignOutputDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
        public int StatusId { get; set; }
        public int SendCount { get; set; }
        public string ContentEmail { get; set; }
        public string StatusName { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierName { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public List<CampaignTargetOutputDto> CampaignTarget { get; set; }
    }
}
