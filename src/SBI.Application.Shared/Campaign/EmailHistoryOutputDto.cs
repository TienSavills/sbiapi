﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Campaign
{
    public class EmailHistoryOutputDto
    {
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string SentTo { get; set; }
        public bool IsSuccess { get; set; }
        public int TotalCount { get; set; }
    }
}
