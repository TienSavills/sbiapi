﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Campaign
{
    public class CampaignInputDto
    {
        public long ?Id { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
        public string ContentEmail { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public List<long?> TargetIncludeIds { get; set; }
        public List<long?> TargetExcludeIds { get; set; }
    }
}
