﻿using CRM.Application.Shared.ActivityType;
using CRM.Application.Shared.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ActivityReminder
{
    public class ActivityReminderOutputDto
    {
        public long Id { get; set; }
        public long ActivityId { get; set; }
        public int ModuleId { get; set; }
        public ActivityTypeOutputDto Module { get; set; }
        public long ReferenceId { get; set; }
        public string ReferenceName { get; set; }
        public int TypeId { get; set; }
        public ActivityTypeOutputDto Type { get; set; }
        public string TypeName { get; set; }
        public int FormatId { get; set; }
        public ActivityTypeOutputDto Format { get; set; }
        public string FormatName { get; set; }
        public int Value { get; set; }
        public bool IsActive { get; set; }
        public long CreatorUserId { get; set; }
        public UserOutputDto CreatorUser { get; set; }
        public long? LastModifierUserId { get; set; }
        public UserOutputDto LastModifierUser { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
    }
}
