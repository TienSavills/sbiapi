﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ActivityReminder
{
    public class ActivityReminderInputDto
    {
        public long? Id { get; set; }
        public int ModuleId { get; set; }
        public long ReferenceId { get; set; }
        public int TypeId { get; set; }
        public int FormatId { get; set; }
        public int Value { get; set; }
        public bool IsActive { get; set; }
    }
}
