﻿using System.Collections.Generic;

namespace CRM.Application.Shared.Provinces
{
    public class ProvinceOutputDto
    {
        public int Id { get; set; }
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public int CountryId { get; set; }
        public bool IsActive { get; set; }
        public List<Districts.DistrictOutputDto> Districts { get; set; }
    }
}
