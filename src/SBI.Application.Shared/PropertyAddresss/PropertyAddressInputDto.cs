﻿using System.ComponentModel;
using CRM.Application.Shared.Geography;

namespace CRM.Application.Shared.PropertyAddresss
{
    public class PropertyAddressInputDto
    {
        public long? Id { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
    }
}
