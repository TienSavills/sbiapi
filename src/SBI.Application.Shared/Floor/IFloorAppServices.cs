﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Floor
{
    public interface IFloorAppServices : IApplicationService
    {
        Task<List<FloorOutputDto>> GetListFloors(long projectId);
        Task<FloorOutputDto> GetFloorDetails(long id);
        Task<FloorOutputDto> CreateOrUpdateAsync(long? id, FloorInputDto input);
        Task<FloorOutputDto> OrderFloor(long id, int num);
        Task<List<FloorOutputDto>> OrderListFloor(List<long> ids);
    }
}
