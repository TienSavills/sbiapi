﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Floor
{
    public class FloorInputDto
    {
        public long? Id { get; set; }
        public long ProjectId { get; set; }
        public string FloorName { get; set; }
        public double? Size { get; set; }
        public int? Order { get; set; }
        public bool IsActive { get; set; }
    }
}
