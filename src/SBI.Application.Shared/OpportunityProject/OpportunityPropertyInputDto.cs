﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityProject
{
    public class OpportunityProjectInputDto:Entity<long>
    {
        public long OpportunityId { get; set; }
        public long ProjectId { get; set; }
        public bool IsActive { get; set; }
    }
}
