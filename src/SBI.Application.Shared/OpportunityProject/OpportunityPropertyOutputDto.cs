﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Application.Shared.Project.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityProject
{
    public class OpportunityProjectOutputDto
    {
        public long Id { get; set; }
        public long OpportunityId { get; set; }
        public long ProjectId { get; set; }
        public string ProjectName { get; set; }
        public bool IsActive { get; set; }
    }
}
