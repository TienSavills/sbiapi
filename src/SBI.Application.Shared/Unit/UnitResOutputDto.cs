﻿using System;
using System.Collections.Generic;
using System.Text;
using CRM.Application.Shared.ProjectFacilityMaps;
using CRM.Application.Shared.UnitAddress;
using CRM.Application.Shared.UnitFacilityMaps;
using CRM.Application.Shared.UnitViewMaps;

namespace CRM.Application.Shared.Unit
{
    public class UnitResOutputDto
    {
        public  long? ProjectId { get; set; }
        public  string ProjectName { get; set; }
        public long? Id { get; set; }
        public long? FloorId { get; set; }
        public  string FloorName { get; set; }
        public string UnitName { get; set; }
        public decimal? Size { get; set; }
        public int StatusId { get; set; }
        public  string StatusName { get; set; }
        public string Description { get; set; }
        public int UnitTypeId { get; set; }
        public string UnitTypeName { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public long? Price { get; set; }
        public int? Balcony { get; set; }
        public int? HelperRoom { get; set; }
        public int BedRoom { get; set; }
        public int BathRoom { get; set; }
        public int? LivingRoom { get; set; }
        public int FacingId { get; set; }
        public  string FacingName { get; set; }
        public decimal? SaleAble { get; set; }
        public decimal? Gross { get; set; }
        public int? MotobikePark { get; set; }
        public bool? IsPet { get; set; }
        public int? CarPark { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierUserName { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
        public List<UnitFacilityMapOutputDto> UnitFacilities { get; set; }
        public List<UnitViewMapOutputDto> UnitViews { get; set; }
        public List<UnitAddressOutputDto> UnitAddress { get; set; }
    }
}
