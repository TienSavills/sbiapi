﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;

namespace CRM.Application.Shared.Unit
{
    public class UnitResInputDto:Entity<long>
    {
        public long? FloorId { get; set; }
        public string UnitName { get; set; }
        public decimal? Size { get; set; }
        public int StatusId { get; set; }
        public string Description { get; set; }
        public int UnitTypeId { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public long? Price { get; set; }
        public  int? MotobikePark { get; set; }
        public int? CarPark { get; set; }
        public int? Balcony { get; set; }
        public int? HelperRoom { get; set; }
        public int BedRoom { get; set; }
        public int BathRoom { get; set; }
        public int? LivingRoom { get; set; }
        public int FacingId { get; set; }
        public decimal? SaleAble { get; set; }
        public decimal? Gross { get; set; }
        public bool? IsPet { get; set; }
        public List<int> FacilityIds { get; set; }
        public List<int> ViewIds { get; set; }
        public List<UnitAddress.UnitAddressInputDto> UnitAddress { get; set; }
    }
}
