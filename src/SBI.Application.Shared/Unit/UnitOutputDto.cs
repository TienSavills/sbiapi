﻿using CRM.Application.Shared.UnitHistory;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Unit
{
    public class UnitOutputDto
    {
        public long Id { get; set; }
        public string ExpiredType { get; set; }
        public long FloorId { get; set; }
        public string FloorName { get; set; }
        public string UnitName { get; set; }
        public long ProjectId { get; set; }
        public string ProjectName { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public DateTime? StartDate { get; set; }
        public long? OrgTenantId { get; set; }
        public string OrgTenantBusinessName { get; set; }
        public string OrgTenantLegalName { get; set; }
        public decimal? Size { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        public int UnitTypeId { get; set; }
        public string UnitTypeName { get; set; }
        public decimal? Monthly { get; set; }
        public decimal? Per { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierUserName { get; set; }
        public long? Price { get; set; }
        public UnitHistoryOutputDto UnitHistory { get; set; }
    }
}
