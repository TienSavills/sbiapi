﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Unit
{
    public class UnitInputDto
    {
        public long? Id { get; set; }
        public long FloorId { get; set; }
        public string UnitName { get; set; }
        public decimal? Size { get; set; }
        public int StatusId { get; set; }
        public string Description { get; set; }
        public int UnitTypeId { get; set; }
        public decimal? Monthly { get; set; }
        public decimal? Per { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public long? Price { get; set; }
    }
    public class UnitOrderDto
    {
        public long UnitId { get; set; }
        public int Num { get; set; }
    }
}
