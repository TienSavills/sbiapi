﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Unit
{
    public interface IUnitAppService : IApplicationService
    {
        Task<List<UnitOutputDto>> GetListUnitsByFloor(long floorId);
        Task<List<UnitOutputDto>> GetListUnitsByProject(long projectId);
        Task<UnitOutputDto> GetUnitDetails(long id);
        Task<UnitOutputDto> CreateOrUpdateAsync(long? id, UnitInputDto input);
        Task<UnitResOutputDto> CreateOrUpdateResidentialAsync(UnitResInputDto input);
        Task<UnitOutputDto> OrderUnit(long id, int num);
        Task<List<UnitOutputDto>> MatchingRequest(long requestId);
        Task<PagedResultDto<UnitOutputDto>> GetListUnits(string input);
        Task<PagedResultDto<UnitResOutputDto>> GetListUnitResAsync(long? projectId, long? floorId, string input);
        Task<UnitResOutputDto> GetUnitResDetails(long id);
        Task<List<UnitOutputDto>> OrderListUnit(List<long> ids);
    }
}
