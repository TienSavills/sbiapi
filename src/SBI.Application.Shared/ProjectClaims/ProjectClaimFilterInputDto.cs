﻿using CRM.Application.Shared.PageHelper.Dto;

namespace CRM.Application.Shared.ProjectClaims
{
    public class ProjectClaimFilterInputDto 
    {
        public string FilterJson { get; set; }

    }
}
