﻿using CRM.Application.Shared.ProjectClaimRoomTypeMaps;
using CRM.Application.Shared.ProjectTenant;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ProjectClaims
{
    public class ProjectClaimOutputDto
    {
        public long Id { get; set; }
        public long ProjectId { get; set; }
        public int? NumberOfUnits { get; set; }
        public int? OccupancyUnit { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? RentPrice { get; set; }
        public bool? IncludedVAT { get; set; }
        public decimal? VATRate { get; set; }
        public string ProjectName { get; set; }
        public DateTime? LaunchingTime { get; set; }
        public bool? IncludedServiceCharge { get; set; }
        public decimal? ServiceChargeRate { get; set; }
        public decimal? LeasableTotalArea { get; set; }
        public string Description { get; set; }
        public decimal? LeasedArea { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? DataDate { get; set; }
        public int? ConstructionStatusId { get; set; }
        public bool IsActive { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string ProjectStateName { get; set; }
        public string StateName { get; set; }
        public string Address { get; set; }
        public int? DataPeriod { get; set; }
        public int? Period { get; set; }
        public int? ProjectStateId { get; set; }
        public int ResearchTypeId { get; set; }
        public int? Year { get; set; }
        public double? USD { get; set; }
        public double? CurrentUSD { get; set; }
        public List<ProjectTenantOutputDto> ProjectTenants { get; set; }
        public List<ProjectGradeMaps.ProjectGradeMapOutputDto> ProjectGrades { get; set; }
        public List<ProjectClaimRoomTypeMapOutputDto> ProjectClaimRoomType { get; set; }
    }
}
