﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.ProjectClaimRoomTypeMaps;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.ProjectClaims
{
    public interface IProjectClaimAppService:IApplicationService
    {
        Task<ProjectClaimOutputDto> GetProjectClaimDetail(long id);

        Task<ProjectClaimOutputDto> CreateProjectClaimAsync(ProjectClaimInputDto input);

        Task<ProjectClaimOutputDto> UpdateProjectClaimAsync(long id, ProjectClaimInputDto input);

        Task<PagedResultDto<ProjectClaimOutputDto>> FilterProjectClaimAsync(string input);
        Task<ProjectClaimOutputDto> UpdateProjectClaimDetailAsync(long id, List<ProjectClaimRoomTypeMapInputDto> projectClaimRoomTypeInput);
    }
}
