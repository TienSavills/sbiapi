﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.DealOpportunity
{
    public class DealOpportunityInputDto:Entity<long>
    {
        public long DealId { get; set; }
        public long OpportunityId { get; set; }
        public bool IsActive { get; set; }
    }
}
