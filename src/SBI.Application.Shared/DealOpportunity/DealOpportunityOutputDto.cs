﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.DealOpportunity
{
    public class DealOpportunityOutputDto
    {
        public long Id { get; set; }
        public long DealId { get; set; }
        public string DealName { get; set; }
        public long OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        public long OpportunityAmount { get; set; }
        public bool IsActive { get; set; }
    }
}
