﻿using CRM.Application.Shared.PageHelper.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Wards
{
    public class WardFilterInputDto:PagedInputDto
    {
        public string Keyword { get; set; }
         public int? DistrictId { get; set; }
    }
}
