﻿using CRM.Application.Shared.Districts;
using CRM.Entities.Districs;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Wards
{
    public class WardOutputDto
    {
        public int Id { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }
        public int DistrictId { get; set; }
        public DistrictOutputDto District { get; set; }
    }
}
