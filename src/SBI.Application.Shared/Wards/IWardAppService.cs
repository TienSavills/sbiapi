﻿using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Wards
{
    public interface IWardAppService
    {
        Task<PagedResultDto<WardOutputDto>> GetListWard(WardFilterInputDto input);
    }
}
