﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Financial
{
    public  interface ICapacityAppService : IApplicationService
    {
        Task<List<CapacityOutputDto>> FilterCapacity(string input);
        Task<CapacityOutputDto> GetCapacityDetail(long id);
        Task<CapacityOutputDto> CreateCapacityAsync(CapacityInputDto input);
        Task<List<CapacityOutputDto>> CreateListCapacityAsync(List<CapacityInputDto> input);
        Task<CapacityOutputDto> UpdateCapacityAsync(long id, CapacityInputDto input);
    }
}
