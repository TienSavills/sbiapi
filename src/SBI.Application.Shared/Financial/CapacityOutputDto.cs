﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Financial
{
    public class CapacityOutputDto
    {
        public long Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public long OrganizationUnitId { get; set; }
        public string OrganizationUnitName { get; set; }
        public decimal? BudgetRevenue { get; set; }
        public decimal? Expense { get; set; }
        public decimal? BudgetExpense { get; set; }
        public decimal? ActRevenue { get; set; }
    }
}
