﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.Project.Dto;
using CRM.Application.Shared.RequestContact;
using CRM.Application.Shared.RequestOrganizationUnit;
using CRM.Application.Shared.RequestUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Request
{
    public interface IRequestAppService : IApplicationService
    {
        Task<PagedResultDto<RequestOutputDto>> GetListRequest(string filterJson);
        Task<PagedResultDto<RequestOutputDto>> GetListRequestByCompany(long orgTenantId);
        Task<List<RequestOutputDto>> MathchingUnit(long unitId);
        Task<List<ProjectOutputDto>> MatchingProjects(long requestId);
        Task<RequestOutputDto> GetRequestDetails(long id);
        Task<RequestOutputDto> CreateRequestAsync(RequestInputDto input);
        Task<RequestOutputDto> UpdateRequestAsync(long requestId, RequestInputDto input);
        List<RequestUserOutputDto> CreateOrUpdateRequestUser(long requestId, List<RequestUserInputDto> requestUsers);
        List<RequestOrganizationUnitOutputDto> CreateOrUpdateRequestOrganizationUnit(long requestId, List<RequestOrganizationUnitInputDto> requestOrganizationUnits);
        Task<PagedResultDto<RequestOrganizationUnitOutputDto>> GetListRequestOrganizationUnit(FilterBasicInputDto input, long requestId);
        Task<PagedResultDto<RequestUserOutputDto>> GetListRequestUser(FilterBasicInputDto input, long requestId);
        Task<PagedResultDto<RequestContactOutputDto>> GetListRequestContact(FilterBasicInputDto input, long requestId);
        List<RequestContactOutputDto> CreateOrUpdateRequestContact(long requestId, List<RequestContactInputDto> input);
    }
}
