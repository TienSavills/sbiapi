﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Request
{
    public class RequestInputDto
    {
        public long? Id { get; set; }
        public long orgTenantId { get; set; }
        public int? FromYearBuilt { get; set; }
        public int? ToYearBuilt { get; set; }
        public string Description { get; set; }
        public int? Cars { get; set; }
        public int? Motorbikes { get; set; }
        public string ItServices { get; set; }
        public string BackupPower { get; set; }
        public decimal? SpaceTo { get; set; }
        public decimal? SpaceFrom { get; set; }
        public int? HeadCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int? GradeId { get; set; }
        public decimal? MonthlyFrom { get; set; }
        public decimal? MonthlyTo { get; set; }
        public decimal? PerFrom { get; set; }
        public decimal? PerTo { get; set; }
        public DateTime? Timing { get; set; }
        public DateTime? ExpansionPlan { get; set; }
        public DateTime? OperationalDate { get; set; }
        public string Access { get; set; }
        public string Additional { get; set; }
        public string OtherOperational { get; set; }
        public string Security { get; set; }
        public int? RequirementTypeId { get; set; }
        public decimal? SizeFrom { get; set; }
        public decimal? SizeTo { get; set; }
        public DateTime? DateEst { get; set; }
        public int? StatusId { get; set; }
        public int? SourceId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsMultiSpace { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public int? RequestTypeId { get; set; }
        public long? FromPrice { get; set; }
        public long? ToPrice { get; set; }
    }
}
