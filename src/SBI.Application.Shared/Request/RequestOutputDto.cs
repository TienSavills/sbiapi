﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Request
{
    public class RequestOutputDto
    {
        public long Id { get; set; }
        public int? RequestTypeId { get; set; }
        public string RequestTypeName { get; set; }
        public long orgTenantId { get; set; }
        public string TenantBusinessName { get; set; }
        public string TenantLegalName { get; set; }
        public int? FromYearBuilt { get; set; }
        public int? ToYearBuilt { get; set; }
        public string Description { get; set; }
        public int? Cars { get; set; }
        public int? Motorbikes { get; set; }
        public string ItServices { get; set; }
        public string BackupPower { get; set; }
        public decimal? SpaceTo { get; set; }
        public decimal? SpaceFrom { get; set; }
        public int? HeadCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int? GradeId { get; set; }
        public string GradeName { get; set; }
        public decimal? MonthlyFrom { get; set; }
        public decimal? MonthlyTo { get; set; }
        public decimal? PerFrom { get; set; }
        public decimal? PerTo { get; set; }
        public DateTime? Timing { get; set; }
        public DateTime? ExpansionPlan { get; set; }
        public DateTime? OperationalDate { get; set; }
        public string Access { get; set; }
        public string Additional { get; set; }
        public string OtherOperational { get; set; }
        public string Security { get; set; }
        public int? RequirementTypeId { get; set; }
        public string RequirementTypeName { get; set; }
        public decimal? SizeFrom { get; set; }
        public decimal? SizeTo { get; set; }
        public DateTime? DateEst { get; set; }
        public int? StatusId { get; set; }
        public string StatusName { get; set; }
        public int? SourceId { get; set; }
        public string SourceName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public bool? IsMultiSpace { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public long CreatorUserId { get; set; }
        public string CreatorUserName { get; set; }
        public string CreatorUserSurName { get; set; }
        public long? LastModifierUserId { get; set; }
        public string LastModifierName { get; set; }
        public string LastModifierSurName { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? FromPrice { get; set; }
        public long? ToPrice { get; set; }
    }
}
