﻿using System.Collections.Generic;
using Abp.Application.Services;
using CRM.Application.Shared.Facilities;

namespace CRM.Application.Shared.View
{
    public interface IViewAppService : IApplicationService
    {

        List<FacilityOutputDto> GetListView(int? id);
    }
}
