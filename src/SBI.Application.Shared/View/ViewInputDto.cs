﻿namespace CRM.Application.Shared.View
{
    public class ViewInputDto
    {
        public string Code { get; set; }
        public string ViewName { get; set; }
        public string ViewDescription { get; set; }
    }
}
