﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyUser
{
    public class CompanyUserV1OutputDto
    {
        public long Id { get; set; }
        public int CompanyId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public bool? IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
