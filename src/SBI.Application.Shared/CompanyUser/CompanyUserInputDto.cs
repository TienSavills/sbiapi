﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Application.Shared.CompanyUser
{
    public class CompanyUserInputDto:Entity<long>
    {
        public long UserId { get; set; }
        public long CompanyId { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
