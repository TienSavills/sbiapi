﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Application.Shared.Property.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityProperty
{
    public class OpportunityPropertyOutputDto
    {
        public long Id { get; set; }
        public long OpportunityId { get; set; }
        public long PropertyId { get; set; }
        public string PropertyName { get; set; }
        public bool IsActive { get; set; }
    }
}
