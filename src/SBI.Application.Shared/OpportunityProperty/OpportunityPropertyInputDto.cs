﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityProperty
{
    public class OpportunityPropertyInputDto
    {
        public long? Id { get; set; }
        public long PropertyId { get; set; }
        public bool IsActive { get; set; }
    }
}
