﻿using System;

namespace CRM.Application.Shared.ConstructionStatus
{
    public class ConstructionStatusOuputDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreationTime { get; set; }
    }
}
