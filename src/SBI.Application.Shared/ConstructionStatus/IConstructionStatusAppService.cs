﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.ConstructionStatus;

namespace CRM.Application.Shared.ConstructionStatus
{
    public interface IConstructionStatusAppService
    {
        Task<PagedResultDto<ConstructionStatusOuputDto>> GetListConstructionStatus(FilterBasicInputDto input);
        Task<List<ConstructionStatusOuputDto>> GetListConstructionStatus(string keyword);
    }
}
