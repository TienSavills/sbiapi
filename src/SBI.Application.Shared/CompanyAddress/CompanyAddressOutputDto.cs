﻿namespace CRM.Application.Shared.CompanyAddress
{
    public class CompanyAddressOutputDto
    {
        public long Id { get; set; }
        public int CountryId { get; set; }
        public string Country { get; set; }
        public int? ProvinceId { get; set; }
        public string Province { get; set; }
        public int? DistrictId { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrimary { get; set; }
    }
}
