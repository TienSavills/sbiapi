﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityContact
{
    public class OpportunityContactInputDto:Entity<long>
    {
        public long ContactId { get; set; }
        public long OpportunityId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPrimary { get; set; }
    }
}
