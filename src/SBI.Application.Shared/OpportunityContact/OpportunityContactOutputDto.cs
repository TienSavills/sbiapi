﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityContact
{
    public class OpportunityContactOutputDto
    {
        public long Id { get; set; }
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public long OpportunityId { get; set; }
        public bool? IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
