﻿using Abp.Application.Services;
using CRM.Application.Shared.Rate;
using CRM.Application.Shared.Unit;
using CRM.Application.Shared.UnitHistory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Statistic
{
    public interface IStatisticAppService : IApplicationService
    {
        Task<dynamic> CountStatusRequestAsync();
        Task<List<UnitOutputDto>> GetListUnitExpiryAsync();
        Task<dynamic> CountUnitExpiryAsync(long? projectId);
        Task<dynamic> CountUnitStatusAsync(long? projectId);
        Task<dynamic> CountUnitTypeAsync(long? projectId);
        Task<dynamic> CountOpportunityDeparmentStatusAsync(DateTime? dateFrom, DateTime? dateTo, int[] organizationUnitIds, string departmentIds, int? officeId);
        Task<dynamic> CountOpportunityStageAsync(DateTime? dateFrom, DateTime? dateTo, string departmentIds, int? officeId);
        Task<dynamic> CountOpportunityStageAsyncV1(DateTime? dateFrom, DateTime? dateTo, int[] departmentIds, int? officeId);
        Task<dynamic> CountOpportunityLastModifyAsync();
        Task<dynamic> CountLastModifyAsync();
        Task<dynamic> CountStatusPropertyAsync();
        Task<dynamic> CountStatusCompanyAsync();
        Task<dynamic> CountStatusContactAsync();
        Task<dynamic> CountStatusOpportunityAsync(DateTime? dateFrom, DateTime? dateTo, int[] organizationUnitIds, string teamIds, int? officeId);
        Task<dynamic> CountStatusOpportunityAsyncV1(DateTime? dateFrom, DateTime? dateTo, int[] teamIds, int? officeId);
        Task<dynamic> CountStatusDealAsync();
        Task<dynamic> DealOpportunityRevenue(string input);
        Task<dynamic> MMMReport(string input);
        Task<dynamic> UserActivities(string input);
        Task<dynamic> UserActivitiesFromTo(DateTime dateFrom, DateTime dateTo, string teamIds, int? officeId, string keyword);
        Task<dynamic> ReportCommercial(string input);
        Task<dynamic> CountProjectTypeAsync(long? projectId);
        Task<dynamic> CountProjectFacilityAsync(long? projectId);
        Task<dynamic> CountProjectGradeAsync(long? projectId);
        Task<RateOutputDto> ExchangeRate(string basic);
        string GetLinkDashboard();
    }
}
