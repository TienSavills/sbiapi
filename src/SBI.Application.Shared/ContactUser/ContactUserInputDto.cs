﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactUser
{
    public class ContactUserInputDto : Entity<long>
    {
        public long ContactId { get; set; }
        public long UserId { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
