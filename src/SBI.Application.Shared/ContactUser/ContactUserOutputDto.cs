﻿using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.Users;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactUser
{
    public class ContactUserOutputDto
    {
        public long Id { get; set; }
        public int ContactId { get; set; }
        public long UserId { get; set; }
        public UserOutputDto User { get; set; }
        public bool? IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
