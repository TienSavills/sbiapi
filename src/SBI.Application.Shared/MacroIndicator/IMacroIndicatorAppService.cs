﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.MacroIndicator
{
    public interface IMacroIndicatorAppService : IApplicationService
    {
        Task<MacroIndicatorOutputDto> GetMacroIndicatorDetail(long id);

        Task<MacroIndicatorOutputDto> CreateMacroIndicatorAsync(MacroIndicatorInputDto input);

        Task<MacroIndicatorOutputDto> UpdateMacroIndicatorAsync(long id, MacroIndicatorInputDto input);

        Task<PagedResultDto<MacroIndicatorOutputDto>> FilterMacroIndicatorAsync(string input);
        //Task<MacroIndicatorOutputDto> UpdateMacroIndicatorDetailAsync(long id);
    }
}
