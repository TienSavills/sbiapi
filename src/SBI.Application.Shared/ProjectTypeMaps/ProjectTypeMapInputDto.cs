﻿using System.Collections.Generic;

namespace CRM.Application.Shared.ProjectTypeMaps
{
    public class ProjectTypeMapInputDto
    {
        public List<int> ProjectTypeIds { get; set; }
    }
}
