﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.ProjectTypeMaps
{
    public interface IProjectTypeMapAppService : IApplicationService
    {
        Task<long> CreateProjectTypeMap(ProjectTypeMapInputDto input);
        List<ProjectTypeMapOutputDto> GetListProjectTypeMap(long projectId);
        Task<long> UpdateProjectTypeMap(long id, ProjectTypeMapInputDto input);
    }
}
