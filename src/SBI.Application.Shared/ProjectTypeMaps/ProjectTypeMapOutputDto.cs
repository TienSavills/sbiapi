﻿
namespace CRM.Application.Shared.ProjectTypeMaps
{
    public class ProjectTypeMapOutputDto
    {
        public long ? Id { get; set; }
        public int PropertyTypeId { get; set; }
        public string Code { get; set; }
        public string TypeName { get; set; }

    }
}
