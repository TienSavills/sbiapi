﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Category
{
    public class CategoryOutputBasic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SortOrder { get; set; }
        public string TypeCode { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
    }
}
