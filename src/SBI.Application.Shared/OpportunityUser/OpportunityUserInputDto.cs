﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Application.Shared.OpportunityUser
{
    public class OpportunityUserInputDto:Entity<long>
    {
        public long OpportunityId { get; set; }
        public long UserId { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
