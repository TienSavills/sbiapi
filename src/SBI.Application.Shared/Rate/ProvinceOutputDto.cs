﻿namespace CRM.Application.Shared.Rate
{
    public class RateOutputDto
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public decimal USD { get; set; }
        public decimal VND { get; set; }
    }
}
