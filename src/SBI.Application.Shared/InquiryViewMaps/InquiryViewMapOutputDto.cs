﻿namespace CRM.Application.Shared.InquiryViewMaps
{
    public class InquiryViewMapOutputDto
    {
        public long Id { get; set; }
        public int ViewId { get; set; }
        public long InquiryId { get; set; }
        public string Code { get; set; }
        public string ViewName { get; set; }
        public string ViewDescription { get; set; }
    }
}
