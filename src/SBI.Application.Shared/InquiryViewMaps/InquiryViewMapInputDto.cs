﻿using Abp.Domain.Entities;

namespace CRM.Application.Shared.InquiryViewMaps
{
    public class InquiryViewMapInputDto : Entity<long>
    {
        public int ViewId { get; set; }
        public long InquiryId { get; set; }
        public bool IsActive { get; set; }
    }
}
