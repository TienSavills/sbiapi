﻿using System.Collections.Generic;
using Abp.Application.Services;

namespace CRM.Application.Shared.Facilities
{
    public interface IFacilityAppService : IApplicationService
    {

        List<FacilityOutputDto> GetListFacility(int? id);
    }
}
