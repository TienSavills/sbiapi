﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.AssetClass
{
    public class AssetClassOutputDto
    {
        public int Id { get; set; }
        public string AssetClassName { get; set; }
        public string AssetClassDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
