﻿using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.OpportunityContact;
using CRM.Application.Shared.OpportunityOrganizationUnit;
using CRM.Application.Shared.OpportunityProperty;
using CRM.Application.Shared.OpportunityUser;
using CRM.Application.Shared.Property.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Opportunity
{
    public class OpportunityInputDto
    {
        public long? Id { get; set; }
        public long? RequestId { get; set; }
        public long? OrganizationUnitId { get; set; }
        public string PrimaryDepartment { get; set; }
        public int? StageId { get; set; }
        public long CompanyId { get; set; }
        public int? CurrencyId { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? SourceId { get; set; }
        public int? IndustryId { get; set; }
        public int StatusId { get; set; }
        public decimal? Probability { get; set; }
        public int? ClientTypeId { get; set; }
        public string OpportunityName { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public string FullPostalAddress { get; set; }
        public decimal PercentCompleted { get; set; }
        public DateTime? ExpectedCloseDate { get; set; }
        public bool IsActive { get; set; }
        public List<int> AssetClassIds { get; set; }
        public List<int> InstructionIds { get; set; }
        public List<long> PropertyIds { get; set; }
        public List<long> ProjectIds { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public int? PrimaryInstructionId { get; set; }
        public int? PrimaryAssetClassId { get; set; }
        public long? ValuationValues { get; set; }
        public string GeoCode { get; set; }
        public long? UnitId { get; set; }
        public long? FeeAmount { get; set; }
        public long? BudgetAmount { get; set; }
        public decimal? Size { get; set; }
        public int? IndustryLevel2Id { get; set; }
    }
}
