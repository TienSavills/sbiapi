﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.OpportunityContact;
using CRM.Application.Shared.OpportunityLeadUser;
using CRM.Application.Shared.OpportunityOrganizationUnit;
using CRM.Application.Shared.OpportunityUser;
using CRM.EntitiesCustom;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Opportunity
{
    public interface IOpportunityAppService:IApplicationService
    {
        Task<Attachment> GenerateOpportunityFile(DateTime? dateFrom, DateTime? dateTo, int? officeId, string departmentIds, string statusIds);
        Task<byte[]> DownloadOpportunity(DateTime? dateFrom, DateTime? dateTo, int? officeId, string departmentIds, string statusIds);
        Task<PagedResultDto<OpportunityOutputDto>> Filter(string input);
        Task<PagedResultDto<OpportunityOutputDto>> FilterOpportunityAdvisory(string input);
        Task<PagedResultDto<OpportunityOutputDto>> FilterOpportunityCommercial(string input);
        Task<List<ExportOpportunityOutput>> ExportOpportunity(DateTime? dateFrom, DateTime? dateTo, int[] organizationUnitIds, int? officeId, string departmentIds, string statusIds);
        Task<PagedResultDto<OpportunityOutputDto>> OpportunityNotInDeal(string input);
        Task<OpportunityOutputDto> GetOpportunityDetail(long opportunityId);
        Task<OpportunityOutputDto> CreateOpportunityAsync(OpportunityInputDto input);
        Task<OpportunityOutputDto> CreateOrOpportunityFullAsync(OpportunityFullInputDto input);
        Task<OpportunityOutputDto> UpdateOpportunityAsync(long opportunityId, OpportunityInputDto input);
        List<OpportunityContactOutputDto> CreateOrUpdateOpportunityContact( List<OpportunityContactInputDto> input);
        List<OpportunityUserOutputDto> CreateOrUpdateOpportunityUser( List<OpportunityUserInputDto> input);
        List<OpportunityLeadOutputDto> CreateOrUpdateOpportunityLead(List<OpportunityLeadInputDto> input);
        List<OpportunityOrganizationUnitOutputDto> CreateOrUpdateOpportunityOrganizationUnit(List<OpportunityOrganizationUnitInputDto> input);
        Task<bool> SendMailWeeklyReport(string displayName, string emailAddress);
        Task SendMailOpportunity(long opportunityId);
        bool ConvertOpportunityToDeal(long opportunityId);
    }
}
