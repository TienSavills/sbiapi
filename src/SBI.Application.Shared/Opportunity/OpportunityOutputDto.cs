﻿using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.OpportunityAssetClass;
using CRM.Application.Shared.OpportunityContact;
using CRM.Application.Shared.OpportunityInstruction;
using CRM.Application.Shared.OpportunityLeadUser;
using CRM.Application.Shared.OpportunityOrganizationUnit;
using CRM.Application.Shared.OpportunityProject;
using CRM.Application.Shared.OpportunityProperty;
using CRM.Application.Shared.OpportunityUser;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Opportunity
{
    public class OpportunityOutputDto
    {
        public long Id { get; set; }
        public int? StageId { get; set; }
        public string StageName { get; set; }
        public decimal? PercentCompleted { get; set; }
        public long? RequestId { get; set; }
        public long? OrganizationUnitId { get; set; }
        public string OrganizationUnitName { get; set; }
        public string PrimaryDepartment { get; set; }
        public long? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string LegalName { get; set; }
        public string BusinessName { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public DateTime? CloseDate { get; set; }
        public long? DealId { get; set; }
        public string DealName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string ContractNumber { get; set; }
        public int? SourceId { get; set; }
        public string SourceName { get; set; }
        public int? IndustryId { get; set; }
        public string IndustryName { get; set; }
        public int? IndustryLevel2Id { get; set; }
        public string IndustryLevel2Name { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public decimal? Probability { get; set; }
        public int? ClientTypeId { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public int? PrimaryInstructionId { get; set; }
        public string PrimaryInstructionName { get; set; }
        public int? PrimaryAssetClassId { get; set; }
        public string PrimaryAssetClassName { get; set; }
        public string ClientTypeName { get; set; }
        public string OpportunityName { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string FullPostalAddress { get; set; }
        public DateTime? ExpectedCloseDate { get; set; }
        public bool IsActive { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierName { get; set; }
        public string LastModifierUserName { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? ValuationValues { get; set; }
        public string GeoCode { get; set; }
        public long? UnitId { get; set; }
        public string UnitName { get; set; }
        public long? FeeAmount { get; set; }
        public long? BudgetAmount { get; set; }
        public decimal? Size { get; set; }
        public List<OpportunityAssetClassOutputDto> AssetClass { get; set; }
        public List<OpportunityInstructionOutputDto> Instruction { get; set; }
        public List<OpportunityPropertyOutputDto> Property { get; set; }
        public List<OpportunityProjectOutputDto> Project { get; set; }
        public List<OpportunityUserOutputDto> OpportunityUser { get; set; }
        public List<OpportunityLeadOutputDto> OpportunityLead { get; set; }
        public List<OpportunityOrganizationUnitOutputDto> OpportunityOrganizationUnit { get; set; }
        public List<OpportunityContactOutputDto> OpportunityContact { get; set; }
    }
}
