﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CampaignTarget
{
    public class CampaignTargetOutputDto
    {
        public long? Id { get; set; }
        public long CampaignId { get; set; }
        public long TargetId { get; set; }
        public string TargetName { get; set; }
        public int TypeId { get; set; }
        public string TypeName{ get; set; }
        public bool IsActive { get; set; }
    }
}
