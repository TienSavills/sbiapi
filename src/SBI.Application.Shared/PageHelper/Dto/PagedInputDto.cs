﻿using System.ComponentModel.DataAnnotations;

namespace CRM.Application.Shared.PageHelper.Dto
{
    public class PagedInputDto 
    {
        [Range(1, AppConsts.MaxPageSize)]
        [Required]
        public int PageSize { get; set; }

        [Range(0, int.MaxValue)]
        [Required]
        public int PageNumber { get; set; }

        public PagedInputDto()
        {
            PageSize = AppConsts.DefaultPageSize;
            PageNumber = AppConsts.DefaultPageNumber;
        }
    }
}