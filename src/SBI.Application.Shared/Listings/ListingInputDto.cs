﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using CRM.Application.Shared.ListingUsers;

namespace CRM.Application.Shared.Listings
{
    public class ListingInputDto:Entity<long>
    {
        public long? ProjectId { get; set; }
        public long? FloorId { get; set; }
        public long UnitId { get; set; }
        public string ListingName { get; set; }
        public decimal? Size { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public DateTime? DateAvailable { get; set; }
        public decimal? TotalCommission { get; set; }
        public decimal Price { get; set; }
        public string FloorZone { get; set; }
        public bool IsKey { get; set; }
        public string Description { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsPublic { get; set; }
        public bool IsActive { get; set; }
        public  List<long> UserIds { get; set; }
    }
}
