﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace CRM.Application.Shared.Listings
{
    public interface IListingAppService : IApplicationService
    {
        Task<ListingOutputDto> GetListingDetails(long id);
        Task<ListingOutputDto> CreateOrUpdateAsync(ListingInputDto input);
        Task<List<ListingOutputDto>> MatchingListing(long requestId);
        Task<PagedResultDto<ListingOutputDto>> GetListListings(string input);
        Task<PagedResultDto<ListingOutputDto>> GetListListingForClient(long companyId, string input);
        Task<PagedResultDto<ListingOutputDto>> ListingForLandingPages(long? projectId, string input);
    }
}
