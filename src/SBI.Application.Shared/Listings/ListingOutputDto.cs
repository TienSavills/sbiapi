﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using CRM.Application.Shared.ListingUsers;
using CRM.Application.Shared.UnitHistory;

namespace CRM.Application.Shared.Listings
{
    public class ListingOutputDto:Entity<long>
    {
        public long? ProjectId { get; set; }
        public long? FloorId { get; set; }
        public long UnitId { get; set; }
        public string ListingName { get; set; }
        public decimal? Size { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public DateTime? DateAvailable { get; set; }
        public decimal? TotalCommission { get; set; }
        public decimal Price { get; set; }
        public string FloorZone { get; set; }
        public bool IsKey { get; set; }
        public string Description { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsPublic { get; set; }
        public bool IsActive { get; set; }
        public string ProjectName { get; set; }
        public string FloorName { get; set; }
        public string UnitName { get; set; }
        public string StatusName { get; set; }
        public string TypeName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public string UrlMainPhoto { get; set; }
        public List<ListingUserOutputDto> Users { get; set; }
    }
}
