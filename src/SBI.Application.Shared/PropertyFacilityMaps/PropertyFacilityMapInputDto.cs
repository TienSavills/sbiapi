﻿using System.Collections.Generic;

namespace CRM.Application.Shared.PropertyFacilityMaps
{
    public class PropertyFacilityMapInputDto
    {
        public List<int> FacilityIds { get; set; }
    }
}
