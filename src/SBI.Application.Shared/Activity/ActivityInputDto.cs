﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Activity
{
    public class ActivityInputDto
    {
        public string ActivityName { get; set; }
        public int? StatusId { get; set; }
        public int? TypeId { get; set; }
        public int? TaskStatusId { get; set; }
        public int? TaskPriorityId { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? DateStart { get; set; }
        public bool? Direction { get; set; }
    }
}
