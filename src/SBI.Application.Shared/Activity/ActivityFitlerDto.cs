﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Activity
{
    public class ActivityFitlerDto
    {
        public long? CompanyId { get; set; }
        public int[] TypeIds { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Keyword { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; } 
    }
}
