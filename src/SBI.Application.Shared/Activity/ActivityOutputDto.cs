﻿
using CRM.Application.Shared.ActivityAttendee;
using CRM.Application.Shared.ActivityOrganizationUnit;
using CRM.Application.Shared.ActivityReminder;
using CRM.Application.Shared.ActivityType;
using CRM.Application.Shared.ActivityUser;
using CRM.Application.Shared.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Activity
{
    public class ActivityOutputDto
    {
        public long Id { get; set; }
        public int? TypeId { get; set; }
        public ActivityTypeOutputDto Type { get; set; }
        public string TypeName { get; set; }
        public string ActivityName { get; set; }
        public int ModuleId { get; set; }
        public ActivityTypeOutputDto Module { get; set; }
        public string ModuleName { get; set; }
        public long ReferenceId { get; set; }
        public string ReferenceName { get; set; }
        public int? StatusId { get; set; }
        public ActivityTypeOutputDto Status { get; set; }
        public string StatusName { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? DateStart { get; set; }
        public bool? Direction { get; set; }
        public int? TaskStatusId { get; set; }
        public ActivityTypeOutputDto TaskStatus { get; set; }
        public string TaskStatusName { get; set; }
        public int? TaskPriorityId { get; set; }
        public ActivityTypeOutputDto TaskPriority { get; set; }
        public string TaskPriorityName { get; set; }
        public long CreatorUserId { get; set; }
        public UserOutputDto CreatorUser { get; set; }
        public string CreatorUserSurName { get; set; }
        public string CreatorUserName { get; set; }
        public long? LastModifierUserId { get; set; }
        public UserOutputDto LastModifierUser { get; set; }
        public string LastModifierUserSurName { get; set; }
        public string LastModifierUserName { get; set; } 
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public List<ActivityAttendeeOutputDto> ActivityAttendee { get; set; }
        public List<ActivityReminderOutputDto> ActivityReminder { get; set; }
        public List<ActivityUserOutputDto> ActivityUser { get; set; }
        public List<ActivityOrganizationUnitOutputDto> ActivityOrganizationUnit { get; set; }
    }
}
