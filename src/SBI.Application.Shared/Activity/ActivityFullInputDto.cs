﻿using CRM.Application.Shared.ActivityAttendee;
using CRM.Application.Shared.ActivityOrganizationUnit;
using CRM.Application.Shared.ActivityReminder;
using CRM.Application.Shared.ActivityUser;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Activity
{
    public class ActivityFullInputDto
    {
        public long? Id { get; set; }
        public string ActivityName { get; set; }
        public int? StatusId { get; set; }
        public int? TypeId { get; set; }
        public int? TaskStatusId { get; set; }
        public int? TaskPriorityId { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? DateStart { get; set; }
        public bool? Direction { get; set; }
        public List<long> ActivityUserIds { get; set; }
        public List<long> ActivityOrganizationUnitIds { get; set; }
        public List<ActivityReminderInputDto> ActivityReminder { get; set; }

    }
}
