﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.ActivityAttendee;
using CRM.Application.Shared.ActivityOrganizationUnit;
using CRM.Application.Shared.ActivityReminder;
using CRM.Application.Shared.ActivityType;
using CRM.Application.Shared.ActivityUser;
using CRM.Application.Shared.CatTypes;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Activity
{
    public interface IActivityAppService : IApplicationService
    {
        Task<PagedResultDto<ActivityOutputDto>> GetListActivityByModule(int moduleId, long referenceId, FilterBasicInputDto input);
        Task<ActivityOutputDto> GetActivityDetails(long id);
        Task<ActivityOutputDto> CreateActivityAsync(int moduleId, long referenceId, ActivityInputDto input);
        Task<ActivityOutputDto> CreateOrUpadteActivityFullAsync(int moduleId, long referenceId, ActivityFullInputDto input);
        Task<ActivityOutputDto> UpdateActivityAsync(long id, ActivityInputDto input);
        Task<ActivityOutputDto> DeactiveActivity(long id);
        List<ActivityReminderOutputDto> CreateOrUpdateActivityReminderAsync(long id, List<ActivityReminderInputDto> input);
        List<ActivityAttendeeOutputDto> CreateOrUpdateActivityAttendeeAsync(long id, List<ActivityAttendeeInputDto> input);
        Task<List<ActivityTypeOutputDto>> GetListActivityType(string keyword);
        List<ActivityUserOutputDto> CreateOrUpdateActivityUser(long activityId, List<ActivityUserInputDto> activityUser);
        List<ActivityOrganizationUnitOutputDto> CreateOrUpdateActivityOrganizationUnit(long activityId, List<ActivityOrganizationUnitInputDto> activityOrganizationUnits);
        Task<PagedResultDto<ActivityOrganizationUnitOutputDto>> GetListActivityOrganizationUnit(FilterBasicInputDto input, long activityId);
        Task<PagedResultDto<ActivityUserOutputDto>> GetListActivityUser(FilterBasicInputDto input, long activityId);
        Task<PagedResultDto<ActivityAttendeeOutputDto>> GetListActivityAttendee(long activityId, FilterBasicInputDto input);
        Task<PagedResultDto<ActivityReminderOutputDto>> GetListActivityReminder(long activityId, FilterBasicInputDto input);
        Task<PagedResultDto<ActivityOutputDto>> GetListActivityByType(ActivityFitlerDto input);
        Task<PagedResultDto<ActivityOutputDto>> GetListActivityContactOfCompany(ActivityFitlerDto input);
        Task<PagedResultDto<ActivityOutputDto>> GetListActivityOfCompany(long id);
    }
}
