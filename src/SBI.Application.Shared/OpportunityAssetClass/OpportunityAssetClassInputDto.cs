﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityAssetClass
{
    public class OpportunityAssetClassInputDto:Entity<long>
    {
        public long AssetClassId { get; set; }
        public long OpportunityId { get; set; }
        public bool IsActive { get; set; }
    }
}
