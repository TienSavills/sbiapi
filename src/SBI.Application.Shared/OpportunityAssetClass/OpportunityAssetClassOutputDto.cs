﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityAssetClass
{
    public class OpportunityAssetClassOutputDto
    {
        public long Id { get; set; }
        public long OpportunityId { get; set; }
        public int AssetClassId { get; set; }
        public string AssetClassName { get; set; }
        public bool IsActive { get; set; }
    }
}
