﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.ReportClaims
{
    public interface IReportClaimAppService:IApplicationService
    {
        Task<dynamic> GetReportClaim(string paramJson, string nameStore);
    }
}
