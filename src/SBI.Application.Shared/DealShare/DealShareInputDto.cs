﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.DealShare
{
    public class DealShareInputDto:Entity<long>
    {
        public long DealId { get; set; }
        public long? OrganizationUnitId { get; set; }
        public int? InstructionId { get; set; }
        public long? UserId { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
