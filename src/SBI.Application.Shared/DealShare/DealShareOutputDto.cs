﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.DealShare
{
    public class DealShareOutputDto
    {
        public long Id { get; set; }
        public long DealId { get; set; }
        public long? OrganizationUnitId { get; set; }
        public string OrganizationUnitName { get; set; }
        public int? InstructionId { get; set; }
        public string InstructionName { get; set; }
        public long? UserId { get; set; }
        public string SurName { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
