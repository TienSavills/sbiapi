﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.CompanyEmail
{
    public class CompanyEmailOutputDto
    {
        public long Id { get; set; }
        public long CompanyId { get; set; }
        public string Email { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? IsOptedOut { get; set; }
        public bool? IsInvalid { get; set; }
        public bool IsActive { get; set; }
    }
}
