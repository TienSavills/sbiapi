﻿using CRM.Application.Shared.Geography;
using System.ComponentModel;

namespace CRM.Application.Shared.ProjectAddresss
{
    public class ProjectAddressInputDto
    {
        public long ProjectId { get; set; }
        public long? Id { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public int? WardId { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        [DefaultValue(true)]
        public bool  IsActive { get; set; }
        public int? RegionId { get; set; }
    }
}
