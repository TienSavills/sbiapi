﻿using CRM.Application.Shared.Geography;

namespace CRM.Application.Shared.ProjectAddresss
{
    public class ProjectAddressOutputDto
    {
        public long Id { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? RegionId { get; set; }
        public string RegionName { get; set; }
        //public GeographyOutputDto Geography { get; set; }
    }
}
