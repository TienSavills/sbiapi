﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.EmailHistory
{
    public class EmailHistoryOutputDto
    {
        public string Module { get; set; }
        public string SentTo { get; set; }
        public string EmailSubject { get; set; }
        public string EmailContent { get; set; }
        public string ResultBody { get; set; }
        public int? StatusCode { get; set; }
        public bool IsSuccess { get; set; }
    }
}
