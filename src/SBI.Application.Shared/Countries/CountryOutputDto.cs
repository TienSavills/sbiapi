﻿using System.Collections.Generic;

namespace CRM.Application.Shared.Countries
{
    public class CountryOutputDto
    {
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string PhoneCode { get; set; }
        public string CountryName { get; set; }
        public string Continent { get; set; }
        public List<Provinces.ProvinceOutputDto> Provinces { get; set; }
    }
}
