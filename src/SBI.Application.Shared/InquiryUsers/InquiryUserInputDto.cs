﻿using Abp.Domain.Entities;

namespace CRM.Application.Shared.InquiryUsers
{
    public class InquiryUserInputDto : Entity<long>
    {
        public long UserId { get; set; }
        public long InquiryId { get; set; }
        public bool IsActive { get; set; }
    }
}
