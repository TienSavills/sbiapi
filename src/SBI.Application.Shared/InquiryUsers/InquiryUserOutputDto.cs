﻿namespace CRM.Application.Shared.InquiryUsers
{
    public class InquiryUserOutputDto
    {
        public long Id { get; set; }
        public int InquiryId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
    }
}
