﻿using CRM.Application.Shared.PageHelper.Dto;

namespace CRM.Application.Shared.Project.Dto
{
    public class ProjectFilterInputDto : PagedInputDto
    {
        public string FilterJson { get; set; }
    }
}
