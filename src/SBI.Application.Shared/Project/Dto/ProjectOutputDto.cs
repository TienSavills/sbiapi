﻿using System;
using System.Collections.Generic;
using CRM.Application.Shared.ProjectAddresss;
using CRM.Application.Shared.ProjectFacilityMaps;
using CRM.Application.Shared.ProjectTenant;
using CRM.Application.Shared.ProjectTypeMaps;
using CRM.Application.Shared.ProjectLandlord;
using CRM.Application.Shared.ProjectTransportationMaps;

namespace CRM.Application.Shared.Project.Dto
{
    public class ProjectOutputDto
    {
        public long Id { get; set; }
        public string Url { get; set; }
        public int? ConstructionStatusId { get; set; }
        public int? TenantTypeId { get; set; }
        public string StateName { get; set; }
        public DateTime? YearRenovated { get; set; }
        public string ProjectName { get; set; }
        public decimal? TotalSize { get; set; }
        public int  NumberOfFloors { get; set; }
        public int NumberOfUnits { get; set; }
        public bool? OutOfMarket { get; set; }
        public DateTime? OutOfDate { get; set; }
        public DateTime? LaunchingTime { get; set; }
        public DateTime? BuiltDate { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public string UrlMainPhoto { get; set; }
        public int? TenureTypeId { get; set; }
        public string TenureTypeName { get; set; }
        public long? LandlordId { get; set; }
        public string LandlordName { get; set; }
        public long? PropertyManagementId{ get; set; }
        public string PropertyManagementName { get; set; }
        public decimal? CeilingHeight { get; set; }
        public string BackupPower { get; set; }
        public string AirConditioning { get; set; }
        public decimal? ElectricityCost { get; set; }
        public decimal? WaterCost { get; set; }
        public decimal? MotorbikeCost { get; set; }
        public decimal? DedicatedCarCost { get; set; }
        public decimal? ManagementFee { get; set; }
        public decimal? AskingRent { get; set; }
        public decimal? LandArea { get; set; }
        public decimal? AskingPrice { get; set; }
        public long? ContactId { get; set; }
        public string ContactName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public List<ProjectAddressOutputDto> ProjectAddress { get; set; }
        public List<ProjectFacilityMapOutputDto> ProjectFacilities { get; set; }
        public List<ProjectTransportationMapOutputDto> ProjectTransportations { get; set; }
        public List<ProjectTypeMapOutputDto> ProjectTypes { get; set; }
        public List<ProjectGradeMaps.ProjectGradeMapOutputDto> ProjectGrades { get; set; }
        public long? Price { get; set; }
    }
}
