﻿using System;
using System.Collections.Generic;

namespace CRM.Application.Shared.Project.Dto
{
    public class ProjectInputDto
    {
        public long? Id  { get; set; }
        public int? ConstructionStatusId { get; set; }
        public string ProjectName { get; set; }
        public DateTime? LaunchingTime { get; set; }
        public DateTime? BuiltDate { get; set; }
        public DateTime? YearRenovated { get; set; }
        public int? NumberOfFloors { get; set; }
        public int? NumberOfUnits { get; set; }
        public decimal? TotalSize { get; set; }
        public int? TenureTypeId { get; set; }
        public bool? OutOfMarket { get; set; }
        public DateTime? OutOfDate { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public long? LandlordId { get; set; }
        public long? PropertyManagementId { get; set; }
        public decimal? CeilingHeight { get; set; }
        public string BackupPower { get; set; }
        public string AirConditioning { get; set; }
        public decimal? ElectricityCost { get; set; }
        public decimal? WaterCost { get; set; }
        public decimal? MotorbikeCost { get; set; }
        public decimal? DedicatedCarCost { get; set; }
        public decimal? ManagementFee { get; set; }
        public decimal? AskingRent { get; set; }
        public decimal? LandArea { get; set; }
        public decimal? AskingPrice { get; set; }
        public long? ContactId { get; set; }
        public long? Price { get; set; }
        public List<int> GradeIds { get; set; }
        public List<int> ProjectTypeIds { get; set; }
        public List<int> FacilityIds { get; set; }
        public List<int> TransportationIds { get; set; }
    }
}
