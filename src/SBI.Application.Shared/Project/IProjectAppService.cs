﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.Project.Dto;
using CRM.Application.Shared.ProjectAddresss;
using CRM.Application.Shared.ProjectFacilityMaps;
using CRM.Application.Shared.ProjectTenant;
using CRM.Application.Shared.ProjectTypeMaps;
using CRM.Application.Shared.ProjectLandlord;
using CRM.Application.Shared.ProjectGradeMaps;
using Newtonsoft.Json.Linq;

namespace CRM.Application.Shared.Project
{
    public interface IProjectAppService : IApplicationService
    {
        /// <summary>
        /// TO NGOC AN, 09 Oct 2018
        /// Get property detail, included: ProjectAddress, ProjectFacilityMap, ProjectTransportationMap, ProjectTypeMap
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        Task<ProjectOutputDto> GetProjectDetail(long propertyId);

        Task<ProjectOutputDto> CreateOrUpdateAsync(ProjectInputDto input);

        /// <summary>
        /// TO NGOC AN, 06 Oct 2018
        /// Create new property
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ProjectOutputDto> CreateProjectAsync(ProjectInputDto input);

        /// <summary>
        /// TO NGOC AN, 06 Oct 2018
        /// Update simple a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ProjectOutputDto> UpdateProjectAsync(long propertyId, ProjectInputDto input);

        /// <summary>
        /// TO NGOC AN, 06 Oct 2018
        /// Insert or update property detail.
        /// Insert or update transport, address, types, facilities...
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="propertyAddress"></param>
        /// <param name="propertyFacilities"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        Task<ProjectOutputDto> UpdateProjectDetailAsync(long propertyId,
            List<ProjectAddressInputDto> propertyAddress,
            ProjectFacilityMapInputDto propertyFacilities,
            ProjectTypeMapInputDto propertyType,
            //List<ProjectTenantInputDto> propertyTenants,
            //ProjectLandlordInputDto propertyLandlords,
            ProjectGradeMapInputDto ProjectGrade);

        /// <summary>
        /// TO NGOC AN, 08 Oct, 2018
        /// Filter properties:
        /// Support: keyword
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedResultDto<ProjectOutputDto>> FilterProject(string input);

    }
}
