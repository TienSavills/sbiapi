﻿using CRM.Application.Shared.PageHelper.Dto;
using System;

namespace CRM.Application.Shared.CatTypes
{
    public class FilterBasicInputDto: PagedInputDto
    {
        public string Keyword { get; set; }
        public bool? IsInvalid { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
