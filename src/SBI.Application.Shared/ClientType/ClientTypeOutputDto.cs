﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ClientType
{
    public class ClientTypeOutputDto
    {
        public int Id { get; set; }
        public string ClientTypeName { get; set; }
        public string ClientTypeDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
