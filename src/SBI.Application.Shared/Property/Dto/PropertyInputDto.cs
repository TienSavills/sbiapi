﻿using System;

namespace CRM.Application.Shared.Property.Dto
{
    public class PropertyInputDto
    {
        public long? ProjectId { get; set; }
        public string PropertyCode { get; set; }
        public string PropertyName { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public int? RequestTypeId { get; set; }
        public int? StatusId { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? Square { get; set; }
        public int? Floor { get; set; }
        public int? FloorCount { get; set; }
        public int? BedroomCount { get; set; }
        public int? YearBuilt { get; set; }
        public int? BarthroomCount { get; set; }
        public int? UnitsCount { get; set; }
        public decimal? Price { get; set; }
    }
}
