﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Property.Dto
{
    public class PropertyMapFilter
    {
        public List<int> CountryIds { get; set; }
        public List<int> ProvinceIds { get; set; }
        public List<int> DistrictIds { get; set; }
        public string Address { get; set; }
        public List<int> PropertyTypeIds { get; set; }
        public List<int> TransportationIds { get; set; }
        public List<int> FacilityIds { get; set; }
        public List<int> TenantIds { get; set; }
        public List<int> LanlordIds { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int? Km { get; set; }
    }
}
