﻿using System;
using System.Collections.Generic;
using CRM.Application.Shared.PropertyAddresss;
using CRM.Application.Shared.PropertyFacilityMaps;
using CRM.Application.Shared.PropertyTypeMap;
using CRM.Application.Shared.PropertyTenant;
using CRM.Application.Shared.PropertyGradeMaps;

namespace CRM.Application.Shared.Property.Dto
{
    public class PropertyOutputDto
    {
        public long Id { get; set; }
        public long? ProjectId { get; set; }
        public string PropertyCode { get; set; }
        public string PropertyName { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public int? RequestTypeId { get; set; }
        public string RequestType { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public int? CurrencyId { get; set; }
        public string Currency { get; set; }
        public decimal? Square { get; set; }
        public int? BedroomCount { get; set; }
        public int MatchingCount { get; set; }
        public int? YearBuilt { get; set; }
        public int? Floor { get; set; }
        public int? FloorCount { get; set; }
        public int? BarthroomCount { get; set; }
        public int? UnitsCount { get; set; }
        public decimal? Price { get; set; }
        public string ProjectName { get; set; }
        public List<PropertyAddressOutputDto> PropertyAddress { get; set; }
        //public List<PropertyFacilityMapOutputDto> PropertyFacilities { get; set; }
        //public List<PropertyTypeMapOutputDto> PropertyTypes { get; set; }
        //public List<PropertyTenantOutputDto> PropertyTenants { get; set; }
        //public List<PropertyLandlordOutputDto> PropertyLandlords { get; set; }
        //public List<PropertyGradeMapOutputDto> PropertyGrades { get; set; }
    }
}
