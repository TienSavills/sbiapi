﻿using CRM.Application.Shared.PageHelper.Dto;

namespace CRM.Application.Shared.Property.Dto
{
    public class PropertyFilterInputDto : PagedInputDto
    {
        public string Keyword { get; set; }
        public long? ProjectId { get; set; }
        public long? Id { get; set; }
    }
}
