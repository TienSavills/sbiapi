﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.Property.Dto;
using CRM.Application.Shared.PropertyAddresss;
using CRM.Application.Shared.PropertyFacilityMaps;
using CRM.Application.Shared.PropertyTenant;
using CRM.Application.Shared.PropertyLandlord;
using CRM.Application.Shared.PropertyTypeMap;
using CRM.Application.Shared.PropertyGradeMaps;
using CRM.Application.Shared.PropertyOrganizationUnit;
using CRM.Application.Shared.PropertyUser;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.Contact;
using CRM.Application.Shared.PropertyContact;
using CRM.Application.Shared.Project.Dto;

namespace CRM.Application.Shared.Property
{
    public interface IPropertyAppService : IApplicationService
    {
        Task<PagedResultDto<PropertyOutputDto>> FilterProperty(string filterJson);

        Task<PropertyOutputDto> GetPropertyDetail(long propertyId);

        Task<PropertyOutputDto> CreatePropertyAsync(PropertyInputDto input);
        Task<ContactOutputDto> GetContactByProperty(long propertyId);
        Task<PagedResultDto<PropertyOutputDto>> MachingProperties(long requestId, PropertyFilterInputDto input);

        Task<PropertyOutputDto> UpdatePropertyAsync(long propertyId, PropertyInputDto input);

        Task<List<PropertyAddressOutputDto>> UpdatePropertyAddress(long propertyId, List<PropertyAddressInputDto> propertyAddress);

        Task<List<PropertyFacilityMapOutputDto>> UpdatePropertyFacility(long propertyId, PropertyFacilityMapInputDto propertyFacilities);

        Task<List<PropertyTypeMapOutputDto>> UpdatePropertyType(long propertyId, PropertyTypeMapInputDto propertyTypes);

        //Task<List<PropertyLandlordOutputDto>> UpdatePropertyLandlord(long propertyId, PropertyLandlordInputDto propertyLandlords);

        //Task<List<PropertyTenantOutputDto>> UpdatePropertyTenant(long propertyId, List<PropertyTenantInputDto> propertyTenants);

        Task<List<PropertyGradeMapOutputDto>> UpdatePropertyGrade(long propertyId, PropertyGradeMapInputDto propertyGrades);
        List<PropertyUserOutputDto> CreateOrUpdatePropertyUser(long propertyId, List<PropertyUserInputDto> propertyUsers);
        List<PropertyOrganizationUnitOutputDto> CreateOrUpdatePropertyOrganizationUnit(long propertyId, List<PropertyOrganizationUnitInputDto> propertyPropertyOrganizationUnits);
        Task<PagedResultDto<PropertyOrganizationUnitOutputDto>> GetListPropertyOrganizationUnit(FilterBasicInputDto input, long propertyId);
        Task<PagedResultDto<PropertyUserOutputDto>> GetListPropertyUser(FilterBasicInputDto input, long propertyId);
        Task<PagedResultDto<PropertyContactOutputDto>> GetListPropertyContact(FilterBasicInputDto input, long propertyId);
        List<PropertyContactOutputDto> CreateOrUpdatePropertyContact(long propertyId, List<PropertyContactInputDto> input);
        Task<List<ProjectMapOutputDto>> GetMapProperty(string searchJson);
        //Task<List<ProjectMapOutputDto>> GetMapProperty();
    }
}
