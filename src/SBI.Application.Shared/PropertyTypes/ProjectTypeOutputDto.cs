﻿namespace CRM.Application.Shared.PropertyTypes
{
    public class PropertyTypeOutputDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string TypeName { get; set; }
        public bool IsActive { get; set; }
        public bool RequiredBuilding { get; set; }
        public bool RequiredFloor { get; set; }
        public bool RequiredUnit { get; set; }
        public string Target { get; set; }
    }
}
