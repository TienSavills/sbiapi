﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.PropertyConstructionMaps
{
    public interface IPropertyConstructionMapAppService : IApplicationService
    {
        Task<List<PropertyConstructionMapOutputDto>> FilterPropertyConstructionMapAsync(PropertyConstructionMapFilterInputDto filters);
        Task<PropertyConstructionMapOutputDto> CreatePropertyConstructionMapAsync(PropertyConstructionMapInputDto input);
        Task<PropertyConstructionMapOutputDto> UpdatePropertyConstructionMapAsync(long id, PropertyConstructionMapInputDto input);
        Task<PropertyConstructionMapOutputDto> GetPropertyConstructionMapDetail(long id);
    }
}
