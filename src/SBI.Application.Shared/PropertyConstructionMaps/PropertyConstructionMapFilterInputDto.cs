﻿using CRM.Application.Shared.PageHelper.Dto;

namespace CRM.Application.Shared.PropertyConstructionMaps
{
    public class PropertyConstructionMapFilterInputDto : PagedInputDto
    {
        public string Keyword { get; set; }
        public long? PropertyId { get; set; }
    }
}
