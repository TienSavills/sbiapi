﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.PropertyConstructionMaps
{
    public class PropertyConstructionMapOutputDto
    {
        public long Id { get; set; }
        public long PropertyId { get; set; }
        public int ConstructionStatusId { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime ActualDate { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public Entities.ConstructionStatus.ConstructionStatus ConstructionStatus { get; set; }
        public Entities.Properties.Property Property { get; set; }
    }
}
