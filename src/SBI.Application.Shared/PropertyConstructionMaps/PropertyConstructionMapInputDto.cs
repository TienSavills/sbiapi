﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Application.Shared.PropertyConstructionMaps
{
   public class PropertyConstructionMapInputDto
    {
        public long PropertyId { get; set; }
        public int ConstructionStatusId { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime ActualDate { get; set; }
        public string Description { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
    }
}
