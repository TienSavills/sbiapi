﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.RequestOrganizationUnit
{
    public class RequestOrganizationUnitInputDto
    {
        public long? Id { get; set; }
        public long OrganizationUnitId { get; set; }
        public bool IsActive { get; set; }
    }
}
