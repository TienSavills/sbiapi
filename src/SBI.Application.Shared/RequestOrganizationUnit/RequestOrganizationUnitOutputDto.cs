﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Application.Shared.OrganizationUnit;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.RequestOrganizationUnit
{
    public class RequestOrganizationUnitOutputDto
    {
        public long Id { get; set; }
        public long RequestId { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnitOutputDto OrganizationUnit { get; set; }
        public bool IsActive { get; set; }
    }
}
