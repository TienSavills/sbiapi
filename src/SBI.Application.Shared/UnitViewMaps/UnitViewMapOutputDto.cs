﻿namespace CRM.Application.Shared.UnitViewMaps
{
    public class UnitViewMapOutputDto
    {
        public long Id { get; set; }
        public int ViewId { get; set; }
        public long UnitId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
