﻿using System.Collections.Generic;
using Abp.Domain.Entities;

namespace CRM.Application.Shared.UnitViewMaps
{
    public class UnitViewMapInputDto : Entity<long>
    {
        public int ViewId { get; set; }
        public long UnitId { get; set; }
        public bool IsActive { get; set; }
    }
}
