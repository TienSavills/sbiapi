﻿using CRM.Application.Shared.ActivityType;
using CRM.Application.Shared.Comment;
using CRM.Application.Shared.CompanyEmail;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyPhone;
using CRM.Application.Shared.CompanyUser;
using CRM.Application.Shared.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ActivityAttendee
{
    public class ActivityAttendeeOutputDto
    {
        public long Id { get; set; }
        public long ActivityId { get; set; }
        public int ModuleId { get; set; }
        public ActivityTypeOutputDto Module { get; set; }
        public string ModuleName { get; set; }
        public long ReferenceId { get; set; }
        public string ReferenceName { get; set; }
        public int StatusId { get; set; }
        public ActivityTypeOutputDto Status { get; set; }
        public string StatusName { get; set; }
        public bool IsActive { get; set; }
        public long CreatorUserId { get; set; }
        public UserOutputDto CreatorUser { get; set; }
        public long? LastModifierUserId { get; set; }
        public UserOutputDto LastModifierUser { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
    }
}
