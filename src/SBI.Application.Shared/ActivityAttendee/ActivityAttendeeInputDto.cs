﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ActivityAttendee
{
    public class ActivityAttendeeInputDto
    {
        public long? Id { get; set; }
        public int ModuleId { get; set; }
        public long ReferenceId { get; set; }
        public int StatusId { get; set; }
        public bool IsActive { get; set; }
    }
}
