﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactTypeMap
{
    public class ContactTypeMapInputDto:Entity<long>
    {
        public long ContactId { get; set; }
        public int ContactTypeId { get; set; }
        public bool? IsActive { get; set; }
    }
}
