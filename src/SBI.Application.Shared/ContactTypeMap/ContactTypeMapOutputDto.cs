﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ContactTypeMap
{
    public class ContactTypeMapOutputDto
    {
        public long Id { get; set; }
        public long? ContactId { get; set; }
        public string ContactName { get; set; }
        public int ContactTypeId { get; set; }
        public string ContactTypeName { get; set; }
        public bool? IsActive { get; set; }
    }
}
