﻿
namespace CRM.Application.Shared.PropertyTypeMap
{
    public class PropertyTypeMapOutputDto
    {
        public long  Id { get; set; }
        public int PropertyTypeId { get; set; }
        public string Code { get; set; }
        public string TypeName { get; set; }
    }
}
