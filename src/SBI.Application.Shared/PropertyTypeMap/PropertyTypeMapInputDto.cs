﻿using System.Collections.Generic;

namespace CRM.Application.Shared.PropertyTypeMap
{
    public class PropertyTypeMapInputDto
    {
        public List<int> PropertyTypeIds { get; set; }
    }
}
