﻿namespace CRM.Application.Shared.ListingCategory
{
    public class ListingCategoryOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
