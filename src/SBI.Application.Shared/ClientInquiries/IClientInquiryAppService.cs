﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.Listings;

namespace CRM.Application.Shared.ClientInquiries
{
    public interface IClientInquiryAppService : IApplicationService
    {
        Task<ClientInquiryOutputDto> GetAsync(long id);
        Task<ClientInquiryOutputDto> CreateOrUpdateAsync(ClientInquiryInputDto input);
        Task<PagedResultDto<ClientInquiryOutputDto>> GetAllAsync(PagedClientInquiryResultRequestDto input);
    }
}
