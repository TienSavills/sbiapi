﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.Category;
using CRM.Application.Shared.Users;

namespace CRM.Application.Shared.ClientInquiries
{
    public class ClientInquiryOutputDto : Entity<long>
    {
        public string Gender { get; set; }
        public string ClientName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Messenge { get; set; }
        public int? StatusId { get; set; }
        public string StatusName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public List<ClientInquiryUserOutputDto> Users { get; set; }
    }
}
