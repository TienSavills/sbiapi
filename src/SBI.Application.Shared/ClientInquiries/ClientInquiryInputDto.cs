﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Application.Shared.ClientInquiries
{
    public class ClientInquiryInputDto :Entity<long>
    {
        public string Gender { get; set; }
        public string ClientName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Messenge { get; set; }
        public int StatusId { get; set; }
        public bool IsActive { get; set; }
        public List<long>UserIds { get; set; }
    }
}
