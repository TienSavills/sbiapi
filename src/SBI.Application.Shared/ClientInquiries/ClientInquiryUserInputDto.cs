﻿using Abp.Domain.Entities;

namespace CRM.Application.Shared.ClientInquiries
{
    public class ClientInquiryUserInputDto : Entity<long>
    {
        public long UserId { get; set; }
        public long ClientInquiryId { get; set; }
        public bool IsActive { get; set; }
    }
}
