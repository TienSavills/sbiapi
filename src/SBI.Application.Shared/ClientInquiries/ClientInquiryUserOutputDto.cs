﻿using Abp.Domain.Entities;

namespace CRM.Application.Shared.ClientInquiries
{
    public class ClientInquiryUserOutputDto : Entity<long>
    {
        public long ClientInquiryId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
    }
}
