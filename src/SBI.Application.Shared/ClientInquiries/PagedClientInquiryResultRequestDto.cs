﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using JetBrains.Annotations;

namespace CRM.Application.Shared.News
{
    public class PagedNewsResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
        public List<int> CategoryIds { get; set; } = new List<int>();
        public bool? IsActive { get; set; }
        public bool? IsPublic { get; set; }
    }
}

