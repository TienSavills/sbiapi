﻿namespace CRM.Application.Shared
{
    public class AppConsts
    {
        /// <summary>
        /// Default page size for paged requests.
        /// </summary>
        public const int DefaultPageSize = 10;
        public const int DefaultPageNumber = 1;

        /// <summary>
        /// Maximum allowed page size for paged requests.
        /// </summary>
        public const int MaxPageSize = 1000;

        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public const string DefaultPassPhrase = "gsKxGZ012HLL3MI5";
        public static class GeocodeValues
        {
            public const decimal MinLat = -90;
            public const decimal MaxLat = +90;
            public const decimal MinLng = -180;
            public const decimal MaxLng = +180;
            public const decimal InvalidLat = 181;
            public const decimal InvalidLng = 181;
            public const string StateComponent = "administrative_area_level_1";
            public const string CountryComponent = "country";
            public const string PoliticalComponent = "political";
        }
    }
}
