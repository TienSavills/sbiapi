﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ProjectClaimRoomTypeMaps
{
    public class ProjectClaimRoomTypeMapOutputDto 
    {
        public long Id { get; set; }
        public long ProjectClaimId { get; set; }
        public int RoomTypeId { get; set; }
        public int NumberOfRoom { get; set; }
        public bool IsActive { get; set; }
        public string RoomTypeName { get; set; }
    }
}
