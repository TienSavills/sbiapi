﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ProjectClaimRoomTypeMaps
{
    public class ProjectClaimRoomTypeMapInputDto
    {
        public int RoomTypeId { get; set; }
        public int NumberOfRoom { get; set; }
    }
}
