﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.DealInvoiceInfo
{
    public class DealInvoiceInfoOutputDto
    {
        public long DealId { get; set; }
        public long CompanyId { get; set; }
        public long ContactId { get; set; }
        public string ATTN { get; set; }
        public string LegalName { get; set; }
        public string BusinessName { get; set; }
        public string CompanyAddress { get; set;}
        public string AddSavills { get; set; }
        public string CompanyVATCode { get; set;}
        public string EmployeeSign { get; set; }
        public string TitleEmployee { get; set; }
        public string ContactAddress { get; set; }
        public string ContactVATCode { get; set; }
    }
}
