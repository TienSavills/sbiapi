﻿using System.ComponentModel.DataAnnotations;

namespace CRM.Application.Shared.Dto
{
    public class PageSizeInputDto
    {
        [Required]
        [Range(1, AppConsts.MaxPageSize)]
        public int PageSize { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Page { get; set; }

        public PageSizeInputDto()
        {
            PageSize = AppConsts.DefaultPageSize;
        }
    }
}
