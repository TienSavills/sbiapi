﻿namespace CRM.Application.Shared.InquiryFacilityMaps
{
    public class InquiryFacilityMapOutputDto
    {
        public long Id { get; set; }
        public int FacilityId { get; set; }
        public long InquiryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
