﻿using Abp.Domain.Entities;

namespace CRM.Application.Shared.InquiryFacilityMaps
{
    public class InquiryFacilityMapInputDto : Entity<long>
    {
        public int FacilityId { get; set; }
        public long InquiryId { get; set; }
        public bool IsActive { get; set; }
    }
}
