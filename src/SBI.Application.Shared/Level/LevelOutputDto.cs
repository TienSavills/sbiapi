﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Level
{
    public class LevelOutputDto
    {
        public int Id { get; set; }
        public string LevelName { get; set; }
        public string LevelDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
