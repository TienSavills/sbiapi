﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Currentcys
{
    public interface ICurrencyAppService:IApplicationService
    {
        Task<List<CurrencyOutputDto>> GetListCurrencys(string keyword);
    }
}
