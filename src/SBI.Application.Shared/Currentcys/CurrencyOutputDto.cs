﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Currentcys
{
    public class CurrencyOutputDto
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
    }
}
