﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Users
{
    public class UserOutputDto
    {
        public long Id { get; set; }
        public string SurName { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
    }
}
