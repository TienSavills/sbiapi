﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ActivityOrganizationUnit
{
    public class ActivityOrganizationUnitInputDto
    {
        public long? Id { get; set; }
        public long ActivityId { get; set; }
        public long OrganizationUnitId { get; set; }
        public bool IsActive { get; set; }
    }
}
