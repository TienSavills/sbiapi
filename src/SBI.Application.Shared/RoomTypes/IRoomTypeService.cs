﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CRM.Application.Shared.RoomTypes
{
    public interface IRoomTypeAppService : IApplicationService
    {
        Task<PagedResultDto<RoomTypeOutputDto>> GetListRoomTypes(FilterBasicInputDto input);
        Task<List<RoomTypeOutputDto>> GetListRoomTypes(string keyword);
    }
}
