﻿namespace CRM.Application.Shared.RoomTypes
{
    public class RoomTypeOutputDto
    {
        public int Id { get; set; }
        public string RoomTypeName { get; set; }
    }
}
