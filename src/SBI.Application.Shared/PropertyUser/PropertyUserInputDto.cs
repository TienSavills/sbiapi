﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.PropertyUser
{
    public class PropertyUserInputDto
    {
        public long? Id { get; set; }
        public long UserId { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
