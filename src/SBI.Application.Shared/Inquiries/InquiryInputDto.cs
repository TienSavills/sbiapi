﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using CRM.Application.Shared.InquiryAddress;

namespace CRM.Application.Shared.Inquiries
{
    public class InquiryInputDto:Entity<long>
    {
        public long ClientId { get; set; }
        public int TypeId { get; set; }
        public string Description { get; set; }
        public decimal? FromSize { get; set; }
        public decimal? ToSize { get; set; }
        public int StatusId { get; set; }
        public int SourceId { get; set; }
        public long? FromPrice { get; set; }
        public long? ToPrice { get; set; }
        public int? FacingId { get; set; }
        public int? LivingRoom { get; set; }
        public int? BedRoom { get; set; }
        public int? HelperRoom { get; set; }
        public int? BathRoom { get; set; }
        public int? Balcony { get; set; }
        public int? MotobikePark { get; set; }
        public int? CarPark { get; set; }
        public bool IsActive { get; set; }
        public List<InquiryAddressInputDto> InquiryAddress { get; set; }
        public List<int> ViewIds { get; set; }
        public List<long> UserIds { get; set; }
        public List<int> FacitityIds { get; set; }
    }
}
