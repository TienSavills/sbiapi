﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.Project.Dto;

namespace CRM.Application.Shared.Inquiries
{
    public interface IInquiryAppService : IApplicationService
    {
        Task<PagedResultDto<InquiryOutputDto>> GetListInquiries(string filterJson);
        Task<PagedResultDto<InquiryOutputDto>> GetListInquiryByClient(long clientId);
        Task<List<InquiryOutputDto>> MatchingInquiry(long listingId);
        Task<InquiryOutputDto> GetInquiryDetails(long id);
        Task<InquiryOutputDto> CreateOrUpdateAsync(InquiryInputDto input);
    }
}
