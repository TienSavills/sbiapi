﻿using System;
using System.Collections.Generic;
using CRM.Application.Shared.InquiryFacilityMaps;
using CRM.Application.Shared.InquiryUsers;
using CRM.Application.Shared.InquiryViewMaps;

namespace CRM.Application.Shared.Inquiries
{
    public class InquiryOutputDto
    {
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public decimal? FromSize { get; set; }
        public decimal? ToSize { get; set; }
        public int StatusId { get; set; }
        public int StatusName { get; set; }
        public long? FromPrice { get; set; }
        public long? ToPrice { get; set; }
        public int? FacingId { get; set; }
        public string FacingName { get; set; }
        public int SouceId { get; set; }
        public string SouceName { get; set; }
        public int? LivingRoom { get; set; }
        public int? BedRoom { get; set; }
        public int? HelperRoom { get; set; }
        public int? BathRoom { get; set; }
        public int? Balcony { get; set; }
        public int? MotobikePark { get; set; }
        public int? CarPark { get; set; }
        public bool IsActive { get; set; }
        public List<InquiryOutputDto> Address { get; set; }
        public List<InquiryViewMapOutputDto> Views { get; set; }
        public List<InquiryFacilityMapOutputDto> Facilities { get; set; }
        public List<InquiryUserOutputDto> Users { get; set; }
        public long CreatorUserId { get; set; }
        public long? LastModifierUserId { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
    }
}
