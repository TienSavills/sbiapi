﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Instruction
{
    public class InstructionOutputDto
    {
        public int Id { get; set; }
        public string InstructionName { get; set; }
        public string InstructionDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
