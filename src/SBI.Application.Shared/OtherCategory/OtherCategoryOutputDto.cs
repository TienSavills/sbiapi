﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OtherCategory
{
    public class OtherCategoryOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
