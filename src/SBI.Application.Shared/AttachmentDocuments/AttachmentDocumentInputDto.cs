﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography.Pkcs;
using System.Text;

namespace CRM.Application.Shared.AttachmentDocuments
{
    public class AttachmentDocumentInputDto
    {
        public string FileToUpload { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string ParentModuleName { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsMainPhoto { get; set; }
        public string Type { get; set; }
        public int Order { get; set; }
        public int DocumentTypeId { get; set; }
    }

    public class AttachmentDocumentUpdateInputDto
    {
        public int? DocumentTypeId { get; set; }

        public string FileName { get; set; }
        [DefaultValue(0)]
        public int? OrderNum { get; set; }
        public bool? IsActive { get; set; }
    }
}
