﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.AttachmentDocuments
{
    public interface IAttachmentDocumentAppService:IApplicationService
    {
        Task UploadFile(long parentId, AttachmentDocumentInputDto input);
        Task MarkMainPhoto(string moduleName, long fileId,long parentId);
        Task DeleteFile(long fileId);
        List<AttachmentDocumentOutputDto> GetByReferenceId(string moduleName, long parentId,string type);
        Task <AttachmentDocumentOutputDto> GetDocById(long fileId);
        Task<byte[]> DownLoadFileById(long fileId);
        Task<byte[]> DownLoadThumbById(long fileId);
        Task<AttachmentDocumentOutputDto> UpdateDocument(long id, AttachmentDocumentUpdateInputDto input);
    }
}
