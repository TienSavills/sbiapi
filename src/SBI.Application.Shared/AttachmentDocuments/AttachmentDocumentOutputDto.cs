﻿using CRM.Entities.DocumentTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.AttachmentDocuments
{
    public class AttachmentDocumentOutputDto
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string FilePath { get; set; }
        public long ParentId { get; set; }
        public int ParentModuleId { get; set; }
        public bool IsActive { get; set; }
        public bool IsMainPhoto { get; set; }
        public string UrlDownload { get; set; }
        public string UrlThumb { get; set; }
        public string Type { get; set; }
        public int OrderNum { get; set; }
        public DateTime LastModificationTime { get; set; }
        public DateTime CreationTime { get; set; }
        public int DocumentTypeId { get; set; }
        public DocumentType DocumentType { get; set; }
    }
}
