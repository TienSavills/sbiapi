﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Application.Shared.OpportunityLeadUser
{
    public class OpportunityLeadInputDto:Entity<long>
    {
        public long OpportunityId { get; set; }
        public long UserId { get; set; }
        public bool IsActive { get; set; }
    }
}
