﻿using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.Users;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.OpportunityLeadUser
{
    public class OpportunityLeadOutputDto
    {
        public long Id { get; set; }
        public int OpportunityId { get; set; }
        public long UserId { get; set; }
        public UserOutputDto User { get; set; }
        public string SurName { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public bool IsActive { get; set; }
    }
}
