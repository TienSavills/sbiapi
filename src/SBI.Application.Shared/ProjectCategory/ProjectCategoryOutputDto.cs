﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.ProjectCategory
{
    public class ProjectCategoryOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
