﻿using Abp.Domain.Entities;
using System.ComponentModel;

namespace CRM.Application.Shared.ContactAddress
{
    public class ContactAddressInputDto:Entity<long>
    {
        public long ContactId { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        [DefaultValue(true)]
        public bool  IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsPrimary { get; set; }
    }
}
