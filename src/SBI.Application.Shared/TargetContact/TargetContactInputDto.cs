﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.TargetContact
{
    public class TargetContactInputDto
    {
        public long? Id {get;set;}
        public long TargetId { get; set; }
        public long ContactId { get; set; }
        public bool IsActive { get; set; }
    }
}
