﻿using CRM.Application.Shared.DealAdjust;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Deal
{
    public class DealOutputDto
    {
        public long Id { get; set; }
        public string DealName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string ContractNumber { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public decimal FeeAmount { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierName { get; set; }
        public string LastModifierUserName { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public DateTime CreationTime { get; set; }
        public long OrganizationUnitId { get; set; }
        public string OrganizationUnitName { get; set; }
        public List<DealShareOutputDto> DealOrganizationUnit { get; set; }
        public List<DealShareOutputDto> DealUser { get; set; }
        public List<DealOpportunityOutputDto> DealOpportunity { get; set; }
        public List<PaymentOutputDto> Payment { get; set; }
        public List<PaymentOutputDto> PaymentAdjust { get; set; }
        public List<DealAdjustOutputDto> DealAdjust { get; set; }
        public decimal? PercentCompleted { get; set; }
        public DateTime? CommencementDate { get; set; }
        public DateTime? ReportDate { get; set; }
        public long? ValuationValues { get; set; }
        public int? Term { get; set; }
        public long? ContactId { get; set; }
        public string ContactName { get; set; }
        public long? CompanyId { get; set; }
        public string BusinessName { get; set; }
        public string LegalName { get; set; }
        public string ATTN { get; set; }
        public string CompanyAddress { get; set; }
        public string AddSavills { get; set; }
        public string CompanyVATCode { get; set; }
        public string EmployeeSign { get; set; }
        public string TitleEmployee { get; set; }
        public string ContactAddress { get; set; }
        public string ContactVATCode { get; set; }
        public long? Revenue { get; set; }
        public long? Paid { get; set; }
        public long? Outstanding { get; set; }
        public long? Forecast { get; set; }
        public DateTime? DraftReport { get; set; }
        public DateTime? FinalReport { get; set; }
    }
}
