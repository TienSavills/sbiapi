﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Application.Shared.DealAdjust;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.OpportunityCategory;
using CRM.Application.Shared.Payment;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Deal
{
    public class DealFullInputDto:Entity<long>
    {
        public string DealName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string ContractNumber { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public long OrganizationUnitId { get; set; }
        public decimal FeeAmount { get; set; }
        public int StatusId { get; set; }
        public decimal? PercentCompleted { get; set; }
        public DateTime? CommencementDate { get; set; }
        public DateTime? ReportDate { get; set; }
        public long? ValuationValues { get; set; }
        public int? Term { get; set; }
        public long? ContactId { get; set; }
        public long? CompanyId { get; set; }
        public string ATTN { get; set; }
        public string InvoiceLegalName { get; set; }
        public string InvoiceBusinessName { get; set; }
        public string CompanyAddress { get; set; }
        public string AddSavills { get; set; }
        public string CompanyVATCode { get; set; }
        public string EmployeeSign { get; set; }
        public string TitleEmployee { get; set; }
        public string ContactAddress { get; set; }
        public string ContactVATCode { get; set; }
        public DateTime? DraftReport { get; set; }
        public DateTime? FinalReport { get; set; }
        public List<DealShareInputDto> DealShare { get; set; }
        public List<DealAdjustInputDto> DealAdjust { get; set; }
        public List<DealOpportunityInputDto> DealOpportunity { get; set; }
        public List<PaymentInputDto> Payment { get; set; }

    }
}
