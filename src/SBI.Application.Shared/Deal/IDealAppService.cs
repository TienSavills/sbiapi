﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.DealInvoiceInfo;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Application.Shared.Deal
{
    public interface IDealAppService :IApplicationService
    {
        Task<PagedResultDto<DealOutputDto>> FilterDeal(string input);
        Task<PagedResultDto<DealOutputDto>> FilterDealAdvisory(string input);
        Task<PagedResultDto<DealOutputDto>> FilterDealCommercial(string input);
        Task<PagedResultDto<TrackingInvoiceOutputDto>> SpFilterTrackingInvoices(string input);
        Task<TrackingInvoiceOutputDto> GetDealTrackingInvoice(long id);
        Task<DealInvoiceInfoOutputDto> GetDealInvoiceInfo(long dealId);
        Task<DealOutputDto> GetDealDetail(long DealId);
        Task<dynamic> GetRptInvoice(long paymentId);
        Task<dynamic> GetInvoice(long paymentId);
        Task<DealOutputDto> CreateDealAsync(DealInputDto input);
        Task<DealOutputDto> CreateOrDealFullAsync(DealFullInputDto input);
        Task<DealOutputDto> UpdateDealAsync(long DealId, DealInputDto input);
        List<DealShareOutputDto> CreateOrUpdateDealShare(List<DealShareInputDto> input);
        List<DealOpportunityOutputDto> CreateOrUpdateDealOpportunity(List<DealOpportunityInputDto> input);
        List<PaymentOutputDto> CreateOrUpdatePayment(List<PaymentInputDto> input);
        List<PaymentOutputDto> CreateOrUpdatePaymentAdjust(List<PaymentInputDto> input);
        Task<PaymentOutputDto> UpdateTrackingInvoices(TrackingInvoiceInputDto input);
    }
}
