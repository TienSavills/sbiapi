﻿using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Deal
{
    public class TrackingInvoiceOutputDto
    {
        public long Id { get; set; }
        public long DealId { get; set; }
        public string DealName { get; set; }
        public string ContractNumber { get; set; }
        public string BusinessName { get; set; }
        public string LegalName { get; set; }
        public string ContactName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public DateTime? EstDate { get; set; }
        public string ProformaInvoiceNo { get; set; }
        public string Description { get; set; }
        public decimal NetAmount { get; set; }
        public decimal VATAmount { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal ActAmount { get; set; }
        public DateTime? ActDate { get; set; }
        public decimal UnPaid { get; set; }
        public bool? IsSign { get; set; }
        public DateTime? RevenueDate { get; set; }
        public string Department { get; set; }
        public bool? IsActive { get; set; }
        public List<DealShareOutputDto> DealOrganizationUnit { get; set; }
    }
}
