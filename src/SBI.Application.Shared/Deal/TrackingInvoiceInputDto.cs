﻿using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Application.Shared.Deal
{
    public class TrackingInvoiceInputDto
    {
        public long Id { get; set; }
        public int StatusId { get; set; }
        public decimal ActAmount { get; set; }
        public DateTime? ActDate { get; set; }
        public bool? IsSign { get; set; }
        public DateTime? RevenueDate { get; set; }
    }
}
