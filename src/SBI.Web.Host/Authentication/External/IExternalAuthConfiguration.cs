﻿using System.Collections.Generic;

namespace CRM.Web.Host.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
