﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.Project;
using CRM.Application.Shared.Project.Dto;
using CRM.Application.Shared.ProjectAddresss;
using CRM.Application.Shared.ProjectFacilityMaps;
using CRM.Application.Shared.ProjectTenant;
using CRM.Application.Shared.ProjectLandlord;
using CRM.Application.Shared.ProjectTypeMaps;
using CRM.Application.Shared.ProjectGradeMaps;
using Newtonsoft.Json.Linq;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class ProjectController : SBIControllerBase
    {
        private readonly IProjectAppService _projectAppservice;
        public ProjectController(IProjectAppService projectAppservice)
        {
            _projectAppservice = projectAppservice;
        }
        [HttpGet("{projectId}")]
        public async Task<ProjectOutputDto> GetProjectDetail(long projectId)
        {
            return await _projectAppservice.GetProjectDetail(projectId);
        }
        [HttpPost("CreateProject")]
        public async Task<ProjectOutputDto> CreateProject([FromBody] ProjectInputDto input)
        {
            return await _projectAppservice.CreateProjectAsync(input);
        }
        [HttpPost("CreateOrUpdate")]
        public async Task<ProjectOutputDto> CreateOrUpdate([FromBody] ProjectInputDto input)
        {
            return await _projectAppservice.CreateOrUpdateAsync(input);
        }
        [HttpPut("UpdateProject/{projectId}")]
        public async Task<ProjectOutputDto> UpdateProject(long projectId, [FromBody] ProjectInputDto input)
        {
            return await _projectAppservice.UpdateProjectAsync(projectId, input);
        }

        [HttpPut("UpdateProjectAddress/{projectId}")]
        public async Task<ProjectOutputDto> UpdateProjectAddress(long projectId, [FromBody] List<ProjectAddressInputDto> input)
        {
            return await _projectAppservice.UpdateProjectDetailAsync(projectId, input, new ProjectFacilityMapInputDto(), new ProjectTypeMapInputDto(),new ProjectGradeMapInputDto());
        }

        [HttpPut("UpdateProjectFactility/{projectId}")]
        public async Task<ProjectOutputDto> UpdateProjectFactilities(long projectId, [FromBody] ProjectFacilityMapInputDto input)
        {
            return await _projectAppservice.UpdateProjectDetailAsync(projectId, new List<ProjectAddressInputDto>(), input, new ProjectTypeMapInputDto(), new ProjectGradeMapInputDto());
        }

        [HttpPut("UpdateProjectType/{projectId}")]
        public async Task<ProjectOutputDto> UpdateProjectTypes(long projectId, [FromBody] ProjectTypeMapInputDto input)
        {
            return await _projectAppservice.UpdateProjectDetailAsync(projectId, new List<ProjectAddressInputDto>(), new ProjectFacilityMapInputDto(), input, new ProjectGradeMapInputDto());
        }

        [HttpPut("UpdateProjectTenant/{projectId}")]
        public async Task<ProjectOutputDto> UpdateProjectTenants(long projectId, [FromBody] List<ProjectTenantInputDto> input)
        {
            return await _projectAppservice.UpdateProjectDetailAsync(projectId, new List<ProjectAddressInputDto>(), new ProjectFacilityMapInputDto(), new ProjectTypeMapInputDto(), new ProjectGradeMapInputDto());
        }
        //[HttpPut("UpdateProjectLandlord/{projectId}")]
        //public async Task<ProjectOutputDto> UpdateProjectLandlords(long projectId, [FromBody] ProjectLandlordInputDto input)
        //{
        //    return await _projectAppservice.UpdateProjectDetailAsync(projectId, new List<ProjectAddressInputDto>(), new ProjectFacilityMapInputDto(), new ProjectTypeMapInputDto(), new List<ProjectTenantInputDto>(), input, new ProjectGradeMapInputDto());
        //}

        [HttpPut("UpdateProjectGrade/{projectId}")]
        public async Task<ProjectOutputDto> UpdateProjectGrades(long projectId, [FromBody] ProjectGradeMapInputDto input)
        {
            return await _projectAppservice.UpdateProjectDetailAsync(projectId, new List<ProjectAddressInputDto>(), new ProjectFacilityMapInputDto(), new ProjectTypeMapInputDto(), input);
        }

        [HttpPost("Filters")]
        public async Task<PagedResultDto<ProjectOutputDto>> FilterProjects([FromBody] object input)
        {
            return await _projectAppservice.FilterProject(input.ToString());
        }
    }
}
