﻿using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.ProjectClaims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.Project;
using CRM.Application.Shared.ProjectClaimRoomTypeMaps;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class ProjectClaimController : SBIControllerBase
    {
        private readonly IProjectClaimAppService _projectClaimAppservice;

        public ProjectClaimController(IProjectClaimAppService projectClaimAppservice)
        {
            _projectClaimAppservice = projectClaimAppservice;
        }
        [HttpPost("Create")]
        public async Task<ProjectClaimOutputDto> CreateProjectClaimAsync([FromBody]ProjectClaimInputDto input)
        {
            return await _projectClaimAppservice.CreateProjectClaimAsync(input);
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<ProjectClaimOutputDto>> FilterProjectClaimAsync([FromBody]object input)
        {
            return await _projectClaimAppservice.FilterProjectClaimAsync(input.ToString());
        }
        [HttpGet("{id}")]
        public async Task<ProjectClaimOutputDto> GetProjectClaimDetail(long id)
        {
            return await _projectClaimAppservice.GetProjectClaimDetail(id);
        }
        [HttpPut ("Update/{id}")]
        public async Task<ProjectClaimOutputDto> UpdateProjectClaimAsync(long id, [FromBody]ProjectClaimInputDto input)
        {
            return await _projectClaimAppservice.UpdateProjectClaimAsync(id, input);
        }

        [HttpPut("UpdateProjectClaimRoomType/{id}")]
        public async Task<ProjectClaimOutputDto> UpdateProjectClaimRoomType(long id, [FromBody] List<ProjectClaimRoomTypeMapInputDto> input)
        {
            return await _projectClaimAppservice.UpdateProjectClaimDetailAsync(id, input);
        }
    }
}
