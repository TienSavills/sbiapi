﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.Company;
using CRM.Services.Company;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.CompanyAddress;
using CRM.Application.Shared.CompanyEmail;
using CRM.Application.Shared.CompanyPhone;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyUser;
using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.CompanyTypeMap;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class CompanyController : SBIControllerBase
    {
        private readonly ICompanyAppService _companyAppService;
        public CompanyController(ICompanyAppService companyAppService)
        {
            _companyAppService = companyAppService;
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<CompanyOutputDto>> FilterCompany([FromBody]object input)
        {
            return await _companyAppService.GetListCompany(input.ToString());
        }
        [HttpGet("{companyId}")]
        public async Task<CompanyOutputDto> GetCompanyDetails(long companyId)
        {
            return await _companyAppService.GetCompanyDetails(companyId);
        }
        [HttpGet("CompanyContact/{companyId}")]
        public async Task<PagedResultDto<CompanyContactOutputDto>> GetCompanyContact(long companyId, FilterBasicInputDto input)
        {
            return await _companyAppService.GetListCompanyContact(input, companyId);
        }
        [HttpGet("GetListCompanyUser/{companyId}")]
        public async Task<PagedResultDto<CompanyUserOutputDto>> GetListCompanyUser(FilterBasicInputDto input, long companyId)
        {
            return await _companyAppService.GetListCompanyUser(input, companyId);
        }
        [HttpGet("GetListCompanyEmail/{companyId}")]
        public async Task<PagedResultDto<CompanyEmailOutputDto>> GetListCompanyEmail(FilterBasicInputDto input, long companyId)
        {
            return await _companyAppService.GetListCompanyEmail(input, companyId); ;
        }
        [HttpGet("GetListCompanyPhone/{companyId}")]
        public async Task<PagedResultDto<CompanyPhoneOutputDto>> GetListCompanyPhone(FilterBasicInputDto input, long companyId)
        {
            return await _companyAppService.GetListCompanyPhone(input, companyId); ;
        }
        [HttpGet("GetListCompanyOrganizationUnit/{companyId}")]
        public async Task<PagedResultDto<CompanyOrganizationUnitOutputDto>> GetListCompanyOrganizationUnit(FilterBasicInputDto input, long companyId)
        {
            return await _companyAppService.GetListCompanyOrganizationUnit(input, companyId); ;
        }
        [HttpPost("Create")]
        public async Task<CompanyOutputDto> CreateCompanyAsync([FromBody]CompanyInputDto input)
        {
            return await _companyAppService.CreateCompanyAsync(input);
        }
        [HttpPost("CreateOrUpdateCompany")]
        public async Task<CompanyOutputDto> CreateOrUpdateCompanyFullAsync([FromBody]CompanyFullInputDto input)
        {
            return await _companyAppService.CreateOrUpdateCompanyFullAsync(input);
        }
        [HttpPut("Update/{companyId}")]
        public async Task<CompanyOutputDto> UpdateCompanyAsync(long companyId, [FromBody] CompanyInputDto input)
        {
            return await _companyAppService.UpdateCompanyAsync(companyId, input);
        }
        [HttpPut("CreateOrUpdateAddress/{companyId}")]
        public async Task<CompanyOutputDto> UpdateCompanyAddressAsync(long companyId, [FromBody] List<CompanyAddressInputDto> companyAddress)
        {
            return await _companyAppService.UpdateCompanyAddressAsync(companyId, companyAddress);
        }
        [HttpPut("CreateOrUpdateEmails/{companyId}")]
        public List<CompanyEmailOutputDto> UpdateCompanyEmailsAsync(long companyId, [FromBody] List<CompanyEmailInputDto> companyEmail)
        {
            return _companyAppService.CreateOrUpdateCompanyEmail(companyId, companyEmail);
        }
        [HttpPut("CreateOrUpdatePhones/{companyId}")]
        public List<CompanyPhoneOutputDto> UpdateCompanyPhoneAsync(long companyId, [FromBody] List<CompanyPhoneInputDto> companyPhones)
        {
            return _companyAppService.CreateOrUpdateCompanyPhone(companyId, companyPhones);
        }
        [HttpPut("CreateOrUpdateType")]
        public List<CompanyTypeMapOutputDto> CreateOrUpdateType([FromBody] List<CompanyTypeMapInputDto> companyTypeMaps)
        {
            return _companyAppService.CreateOrUpdateCompanyTypeMap(companyTypeMaps);
        }
        [HttpPut("CreateOrUpdateUsers/{companyId}")]
        public List<CompanyUserOutputDto> UpdateCompanyUsersAsync(long companyId, [FromBody] List<CompanyUserInputDto> companyUser)
        {
            return _companyAppService.CreateOrUpdateCompanyUser(companyId, companyUser);
        }
        [HttpPut("CreateOrUpdateOrganizationUnits/{companyId}")]
        public List<CompanyOrganizationUnitOutputDto> UpdateCompanyOrganizationUnitAsync(long companyId, [FromBody] List<CompanyOrganizationUnitInputDto> companyOrganizationUnits)
        {
            return _companyAppService.CreateOrUpdateCompanyOrganizationUnit(companyId, companyOrganizationUnits);
        }
        [HttpPut("CreateOrUpdateCompanyConact/{companyId}")]
        public List<CompanyContactOutputDto> UpdateCompanyContactAsync(long companyId, [FromBody] List<CompanyContactInputDto> companyContacts)
        {
            return _companyAppService.CreateOrUpdateCompanyContact(companyId, companyContacts);
        }
        [HttpPut("MergeCompany")]
        public Task<bool> MergeCompany([FromBody]List<long> sourceCompanyIds, long targetCompanyId)
        {
            return _companyAppService.MergeCompany(sourceCompanyIds, targetCompanyId);
        }
    }
}
