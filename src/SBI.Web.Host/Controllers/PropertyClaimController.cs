﻿using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.PropertyClaims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.Property;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class PropertyClaimController: SBIControllerBase
    {
        private readonly IPropertyClaimAppService _propertyClaimAppservice;

        public PropertyClaimController(IPropertyClaimAppService propertyClaimAppservice)
        {
            _propertyClaimAppservice = propertyClaimAppservice;
        }
        [HttpPost("Create")]
        public async Task<PropertyClaimOutputDto> CreatePropertyClaimAsync([FromBody]PropertyClaimInputDto input)
        {
            return await _propertyClaimAppservice.CreatePropertyClaimAsync(input);
        }
        [HttpGet("Filters")]
        public async Task<PagedResultDto<PropertyClaimOutputDto>> FilterPropertyClaimAsync(PropertyClaimFilterInputDto input)
        {
            return await _propertyClaimAppservice.FilterPropertyClaimAsync(input);
        }
        [HttpGet("{id}")]
        public async Task<PropertyClaimOutputDto> GetPropertyClaimDetail(long id)
        {
            return await _propertyClaimAppservice.GetPropertyClaimDetail(id);
        }
        [HttpPut ("Update/{id}")]
        public async Task<PropertyClaimOutputDto> UpdatePropertyClaimAsync(long id, [FromBody]PropertyClaimInputDto input)
        {
            return await _propertyClaimAppservice.UpdatePropertyClaimAsync(id, input);
        }
    }
}
