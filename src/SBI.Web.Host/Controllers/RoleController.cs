﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Services.Roles.Dto;
using CRM.Services.Roles;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class RoleController : SBIControllerBase
    {
        private readonly IRoleAppService _roleService;

        public RoleController(IRoleAppService roleService)
        {
            _roleService = roleService;
        }

        [HttpPost("Create")]
        public async Task<RoleDto> Create([FromBody]CreateRoleDto input)
        {
            return await _roleService.Create(input);
        }
        [HttpDelete ("Delete")]
        public Task Delete(EntityDto<int> input)
        {
            return _roleService.Delete(input);
        }
        [HttpGet("Get")]
        public async Task<RoleDto> Get(EntityDto<int> input)
        {
            return await _roleService.Get(input);
        }

        [HttpGet("GetList")]
        public async Task<ListResultDto<RoleListDto>> GetList(GetRolesInput input)
        {
            return await _roleService.GetRolesAsync(input);
        }
        [HttpGet("GetAllPermissions")]
        public async Task<ListResultDto<FlatPermissionDto>> GetAllPermissions()
        {
            return await _roleService.GetAllPermissions();
        }
        [HttpGet("GetRoleForEdit")]
        public async Task<GetRoleForEditOutput> GetRoleForEdit(EntityDto input)
        {
            return await _roleService.GetRoleForEdit(input);
        }

        [HttpPut("Update")]
        public async Task<RoleDto> Update([FromBody]RoleDto input)
        {
            return await _roleService.Update(input);
        }
    }
}
