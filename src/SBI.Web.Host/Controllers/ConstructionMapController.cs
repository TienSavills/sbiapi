﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.Company;
using CRM.Services.Company;
using CRM.Application.Shared.PropertyConstructionMaps;
using CRM.Application.Shared.ProjectConstructionMaps;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class ConstructionMapController : SBIControllerBase
    {
        private readonly IPropertyConstructionMapAppService _propertyConstructionMapService;
        private readonly IProjectConstructionMapAppService _projectConstructionMapService;
        public ConstructionMapController(IPropertyConstructionMapAppService propertyConstructionMapService
            , IProjectConstructionMapAppService projectConstructionMapService)
        {
            _projectConstructionMapService = projectConstructionMapService;
            _propertyConstructionMapService = propertyConstructionMapService;
        }
        [HttpPost("Create/Project")]
        public async Task<ProjectConstructionMapOutputDto> CreateProjectConstructionMapAsync([FromBody] ProjectConstructionMapInputDto input)
        {
            return await _projectConstructionMapService.CreateProjectConstructionMapAsync(input);
        }
        [HttpPost("Create/Property")]
        public async Task<PropertyConstructionMapOutputDto> CreatePropertyConstructionMapAsync([FromBody]PropertyConstructionMapInputDto input)
        {
            return await _propertyConstructionMapService.CreatePropertyConstructionMapAsync(input);
        }
        [HttpGet("Filters/Project")]
        public async Task<List<ProjectConstructionMapOutputDto>> FilterProjectConstructionMapAsync(ProjectConstructionMapFilterInputDto filters)
        {
            return await _projectConstructionMapService.FilterProjectConstructionMapAsync(filters);
        }
        [HttpGet("Filters/Property")]
        public async Task<List<PropertyConstructionMapOutputDto>> GetListPropertyConstructionMaps(PropertyConstructionMapFilterInputDto filters)
        {
            return await _propertyConstructionMapService.FilterPropertyConstructionMapAsync(filters);
        }
        [HttpGet("Detail/Project/{id}")]
        public async Task<ProjectConstructionMapOutputDto> GetProjectConstructionMapDetail(long id)
        {
            return await _projectConstructionMapService.GetProjectConstructionMapDetail(id);
        }
        [HttpGet("Detail/Property/{id}")]
        public async Task<PropertyConstructionMapOutputDto> GetPropertyConstructionMapDetail(long id)
        {
            return await _propertyConstructionMapService.GetPropertyConstructionMapDetail(id);
        }
        [HttpPut("Update/Project/{id}")]
        public async Task<ProjectConstructionMapOutputDto> UpdateProjectConstructionMapAsync(long id, [FromBody] ProjectConstructionMapInputDto input)
        {
            return await _projectConstructionMapService.UpdateProjectConstructionMapAsync(id, input);
        }
        [HttpPut("Update/Property/{id}")]
        public async Task<PropertyConstructionMapOutputDto> UpdatePropertyConstructionMapAsync(long id, [FromBody] PropertyConstructionMapInputDto input)
        {
            return await _propertyConstructionMapService.UpdatePropertyConstructionMapAsync(id, input);
        }
    }
}
