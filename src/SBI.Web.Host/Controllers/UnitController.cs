﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using CRM.Application.Shared.Unit;
using CRM.Services.Unit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class UnitController : SBIControllerBase
    {
        private readonly IUnitAppService _unitAppServce;
        public UnitController(
            IUnitAppService unitAppServce
            )
        {
            _unitAppServce = unitAppServce;
        }
        [HttpGet("{unitId}")]
        public async Task<UnitOutputDto> GetUnitDetails(long unitId)
        {
            return await _unitAppServce.GetUnitDetails(unitId);
        }
        [HttpGet("MatchingRequest/{requestId}")]
        public async Task<List<UnitOutputDto>> MatchingRequest(long requestId)
        {
            return await _unitAppServce.MatchingRequest(requestId);
        }
        [HttpGet("GetUnitByProject/{projectId}")]
        public async Task<List<UnitOutputDto>> GetListUnitsByProject(long projectId)
        {
            return await _unitAppServce.GetListUnitsByProject(projectId);
        }
        [HttpGet("GetUnitByFloor/{floorId}")]
        public async Task<List<UnitOutputDto>> GetListUnitsByFloor(long floorId)
        {
            return await _unitAppServce.GetListUnitsByFloor(floorId);
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<UnitOutputDto>> GetListUnits([FromBody] object input)
        {
            return await _unitAppServce.GetListUnits(input.ToString());
        }
        [HttpPost("FilterUnitRes")]
        public async Task<PagedResultDto<UnitResOutputDto>> GetListUnitResAsync(long? projectId, long? floorId, [FromBody] object input)
        {
            return await _unitAppServce.GetListUnitResAsync(projectId, floorId, input.ToString());
        }

        [HttpGet("GetUnitResDetails/{id}")]
        public async Task<UnitResOutputDto> GetUnitResDetails(long id)
        {
            return await _unitAppServce.GetUnitResDetails(id);
        }

        [HttpPost("CreateOrUpdateRes")]
        public async Task<UnitResOutputDto> CreateOrUpdateRes([FromBody] UnitResInputDto input)
        {
            return await _unitAppServce.CreateOrUpdateResidentialAsync(input);
        }

        [HttpPost("Create")]
        public async Task<UnitOutputDto> Create([FromBody] UnitInputDto input)
        {
            return await _unitAppServce.CreateOrUpdateAsync(null, input);
        }
        [HttpPut("Update/{unitId}")]
        public async Task<UnitOutputDto> Update(long unitId, [FromBody] UnitInputDto input)
        {
            return await _unitAppServce.CreateOrUpdateAsync(unitId, input);
        }
        [HttpPut("Order")]
        public async Task<UnitOutputDto> OrderUnit([FromBody] UnitOrderDto input)
        {
            return await _unitAppServce.OrderUnit(input.UnitId, input.Num);
        }
        [HttpPut("OrderList")]
        public async Task<List<UnitOutputDto>> OrderList([FromBody]List<long> ids)
        {
            return await _unitAppServce.OrderListUnit(ids);
        }
    }
}
