﻿using CRM.Application.Shared.Floor;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class FloorController : SBIControllerBase
    {
        private readonly IFloorAppServices _floorAppServce;
        public FloorController(
            IFloorAppServices floorAppServce
            )
        {
            _floorAppServce = floorAppServce;
        }
        [HttpGet("{floorId}")]
        public async Task<FloorOutputDto> GetFloorDetails(long floorId)
        {
            return await _floorAppServce.GetFloorDetails(floorId);
        }
        [HttpGet("GetFloorByProject/{projectId}")]
        public async Task<List<FloorOutputDto>> GetListFloors(long projectId)
        {
            return await _floorAppServce.GetListFloors(projectId);
        }
        [HttpPost("Create")]
        public async Task<FloorOutputDto> Create([FromBody] FloorInputDto input)
        {
            return await _floorAppServce.CreateOrUpdateAsync(null,input);
        }
        [HttpPut("Update/{floorId}")]
        public async Task<FloorOutputDto> Update(long floorId, [FromBody] FloorInputDto input)
        {
            return await _floorAppServce.CreateOrUpdateAsync(floorId, input);
        }
        [HttpPut("Order")]
        public async Task<FloorOutputDto> OrderFloor(long floorId, int num)
        {
            return await _floorAppServce.OrderFloor(floorId, num);
        }
        [HttpPut("OrderList")]
        public async Task<List<FloorOutputDto>> OrderList([FromBody]List<long>ids)
        {
            return await _floorAppServce.OrderListFloor(ids);
        }
    }
}
