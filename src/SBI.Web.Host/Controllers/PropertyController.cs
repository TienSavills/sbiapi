﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.PropertyAddresss;
using CRM.Application.Shared.PropertyFacilityMaps;
using CRM.Application.Shared.Property;
using CRM.Application.Shared.Property.Dto;
using CRM.Application.Shared.PropertyTypeMap;
using CRM.Application.Shared.PropertyLandlord;
using CRM.Application.Shared.PropertyTenant;
using CRM.Application.Shared.PropertyGradeMaps;
using CRM.Application.Shared.PropertyOrganizationUnit;
using CRM.Application.Shared.PropertyUser;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.PropertyContact;
using CRM.Application.Shared.Project.Dto;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class PropertyController : SBIControllerBase
    {
        private readonly IPropertyAppService _propertyAppService;
        public PropertyController(IPropertyAppService propertyAppService)
        {
            _propertyAppService = propertyAppService;
        }

        [HttpPost("Filters")]
        public async Task<PagedResultDto<PropertyOutputDto>> FilterProperty([FromBody]object input)
        {
            return await _propertyAppService.FilterProperty(input.ToString());
        }
        [HttpGet("MachingProperties/{requestId}")]
        public async Task<PagedResultDto<PropertyOutputDto>> MachingProperties(long requestId, PropertyFilterInputDto input)
        {
            return await _propertyAppService.MachingProperties(requestId, input);
        }
        [HttpGet("GetListPropertyOrganizationUnit/{propertyId}")]
        public async Task<PagedResultDto<PropertyOrganizationUnitOutputDto>> GetListPropertyOrganizationUnit(long propertyId, FilterBasicInputDto input)
        {
            return await _propertyAppService.GetListPropertyOrganizationUnit(input, propertyId);
        }
        [HttpGet("GetListPropertyUser/{propertyId}")]
        public async Task<PagedResultDto<PropertyUserOutputDto>> GetListPropertyUser(long propertyId, FilterBasicInputDto input)
        {
            return await _propertyAppService.GetListPropertyUser(input, propertyId);
        }
        [HttpGet("GetMapProperty")]
        public async Task<List<ProjectMapOutputDto>> GetMapProperty(string searchJson)
        {
            return await _propertyAppService.GetMapProperty(searchJson);
        }
        [HttpGet("{propertyId}")]
        public async Task<PropertyOutputDto> GetPropertyDetail(long propertyId)
        {
            return await _propertyAppService.GetPropertyDetail(propertyId);
        }
        [HttpGet("PropertyContact/{propertyId}")]
        public async Task<PagedResultDto<PropertyContactOutputDto>> GetPropertyContact(long propertyId, FilterBasicInputDto input)
        {
            return await _propertyAppService.GetListPropertyContact(input, propertyId);
        }
        [HttpPost("CreateProperty")]
        public async Task<PropertyOutputDto> CreateProperty([FromBody] PropertyInputDto input)
        {
            return await _propertyAppService.CreatePropertyAsync(input);
        }

        [HttpPut("UpdateProperty/{propertyId}")]
        public async Task<PropertyOutputDto> UpdateProperty(long propertyId, [FromBody] PropertyInputDto input)
        {
            return await _propertyAppService.UpdatePropertyAsync(propertyId, input);
        }

        [HttpPut("UpdatePropertyAddress/{propertyId}")]
        public async Task<List<PropertyAddressOutputDto>> UpdatePropertyAddress(long propertyId, [FromBody] List<PropertyAddressInputDto> input)
        {
            return await _propertyAppService.UpdatePropertyAddress(propertyId, input);
        }

        [HttpPut("UpdatePropertyFactility/{propertyId}")]
        public async Task<List<PropertyFacilityMapOutputDto>> UpdatePropertyFactilities(long propertyId, [FromBody] PropertyFacilityMapInputDto input)
        {
            return await _propertyAppService.UpdatePropertyFacility(propertyId, input);
        }
        [HttpPut("UpdatePropertyType/{propertyId}")]
        public async Task<List<PropertyTypeMapOutputDto>> UpdatePropertyTypes(long propertyId, [FromBody] PropertyTypeMapInputDto input)
        {
            return await _propertyAppService.UpdatePropertyType(propertyId, input);
        }
        //[HttpPut("UpdatePropertyTenant/{propertyId}")]
        //public async Task<List<PropertyTenantOutputDto>> UpdatePropertyTenants(long propertyId, [FromBody]   List<PropertyTenantInputDto> input)
        //{
        //    return await _propertyAppService.UpdatePropertyTenant(propertyId, input);
        //}

        //[HttpPut("UpdatePropertyLandlord/{propertyId}")]
        //public async Task<List<PropertyLandlordOutputDto>> UpdatePropertyLandlords(long propertyId, [FromBody] PropertyLandlordInputDto input)
        //{
        //    return await _propertyAppService.UpdatePropertyLandlord(propertyId, input);
        //}

        [HttpPut("UpdatePropertyGrade/{propertyId}")]
        public async Task<List<PropertyGradeMapOutputDto>> UpdatePropertyGrades(long propertyId, [FromBody] PropertyGradeMapInputDto input)
        {
            return await _propertyAppService.UpdatePropertyGrade(propertyId, input);
        }
        [HttpPut("CreateOrUpdatePropertyOrganizationUnit/{propertyId}")]
        public List<PropertyOrganizationUnitOutputDto> CreateOrUpdatePropertyOrganizationUnit(long propertyId, [FromBody] List<PropertyOrganizationUnitInputDto> propertyOrganizationUnits)
        {
            return _propertyAppService.CreateOrUpdatePropertyOrganizationUnit(propertyId, propertyOrganizationUnits);
        }
        [HttpPut("CreateOrUpdatePropertyUser/{propertyId}")]
        public List<PropertyUserOutputDto> CreateOrUpdatePropertyUser(long propertyId, [FromBody] List<PropertyUserInputDto> propertyUsers)
        {
            return _propertyAppService.CreateOrUpdatePropertyUser(propertyId, propertyUsers);
        }
        [HttpPut("CreateOrUpdatePropertyContact/{propertyId}")]
        public List<PropertyContactOutputDto> UpdatePropertyContactAsync(long propertyId, [FromBody] List<PropertyContactInputDto> propertyContacts)
        {
            return _propertyAppService.CreateOrUpdatePropertyContact(propertyId, propertyContacts);
        }
    }
}
