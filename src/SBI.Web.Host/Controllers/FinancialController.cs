﻿using Abp.Application.Services.Dto;
using CRM.Application.Shared.Financial;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class FinancialController : SBIControllerBase
    {
        private readonly ICapacityAppService _capacityAppService;
        public FinancialController(ICapacityAppService capacityAppService)
        {
            _capacityAppService = capacityAppService;
        }
        [HttpPost("Filter")]
        public async Task<List<CapacityOutputDto>> FilterCapacity([FromBody]object filter)
        {
            return await _capacityAppService.FilterCapacity(filter.ToString());
        }
       
        [HttpGet("Details/{id}")]
        public async Task<CapacityOutputDto> GetCapacityDetails(long id)
        {
            return await _capacityAppService.GetCapacityDetail(id);
        }
        [HttpPost("Create")]
        public async Task<CapacityOutputDto> CreateCapacityAsync([FromBody] CapacityInputDto input)
        {
            return await _capacityAppService.CreateCapacityAsync(input);
        }
        [HttpPut("Update/{id}")]
        public async Task<CapacityOutputDto> UpdateCapacityAsync(long id, [FromBody] CapacityInputDto input)
        {
            return await _capacityAppService.UpdateCapacityAsync(id, input);
        }
    }
}
