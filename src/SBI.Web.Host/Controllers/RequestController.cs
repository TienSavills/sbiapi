﻿using CRM.Application.Shared.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.RequestOrganizationUnit;
using CRM.Application.Shared.RequestUser;
using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.RequestContact;
using CRM.Application.Shared.Project.Dto;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class RequestController : SBIControllerBase
    {
        private readonly IRequestAppService _requestAppService;
        public RequestController(IRequestAppService requestAppService)
        {
            _requestAppService = requestAppService;
        }
        [HttpPost("GetListRequest")]
        public async Task<PagedResultDto<RequestOutputDto>> GetListRequest([FromBody]object input)
        {
            return await _requestAppService.GetListRequest(input.ToString());
        }
        [HttpGet("GetListRequestByCompany/{orgTenantId}")]
        public async Task<PagedResultDto<RequestOutputDto>> GetListRequestByCompany(long orgTenantId)
        {
            return await _requestAppService.GetListRequestByCompany(orgTenantId);
        }
        
        [HttpGet("RequestContact/{requestId}")]
        public async Task<PagedResultDto<RequestContactOutputDto>> GetRequestContact(long requestId, FilterBasicInputDto input)
        {
            return await _requestAppService.GetListRequestContact(input, requestId);
        }
        [HttpGet("MatchingUnits/{unitId}")]
        public async Task<List<RequestOutputDto>> MathchingUnits(long unitId)
        {
            return await _requestAppService.MathchingUnit(unitId);
        }
        [HttpGet("MatchingProjects")]
        public async Task<List<ProjectOutputDto>> MatchingProjects(long requestId)
        {
            return await _requestAppService.MatchingProjects(requestId);
        }
        [HttpGet("GetDetails/{requestId}")]
        public Task<RequestOutputDto> GetRequestDetails(long requestId)
        {
            return _requestAppService.GetRequestDetails(requestId);
        }
        [HttpGet("GetListRequestOrganizationUnit/{requestId}")]
        public async Task<PagedResultDto<RequestOrganizationUnitOutputDto>> GetListRequestOrganizationUnit(long requestId, FilterBasicInputDto input)
        {
            return await _requestAppService.GetListRequestOrganizationUnit(input, requestId);
        }
        [HttpGet("GetListRequestUser/{requestId}")]
        public async Task<PagedResultDto<RequestUserOutputDto>> GetListRequestUser(long requestId, FilterBasicInputDto input)
        {
            return await _requestAppService.GetListRequestUser(input, requestId);
        }
        [HttpPut("Update/{requestId}")]
        public Task<RequestOutputDto> UpdateRequestAsync(long requestId, [FromBody] RequestInputDto input)
        {
            return _requestAppService.UpdateRequestAsync(requestId, input);
        }
        [HttpPost("Create")]
        public Task<RequestOutputDto> CreateRequestAsync([FromBody]RequestInputDto input)
        {
            return _requestAppService.CreateRequestAsync(input);
        }
        [HttpPut("CreateOrUpdateRequestOrganizationUnit/{requestId}")]
        public List<RequestOrganizationUnitOutputDto> CreateOrUpdateRequestOrganizationUnit(long requestId,[FromBody] List<RequestOrganizationUnitInputDto> requestOrganizationUnits)
        {
            return _requestAppService.CreateOrUpdateRequestOrganizationUnit(requestId, requestOrganizationUnits);
        }
        [HttpPut("CreateOrUpdateRequestUser/{requestId}")]
        public List<RequestUserOutputDto> CreateOrUpdateRequestUser(long requestId, [FromBody] List<RequestUserInputDto> requestUsers)
        {
            return _requestAppService.CreateOrUpdateRequestUser(requestId, requestUsers);
        }
        [HttpPut("CreateOrUpdateRequestContact/{requestId}")]
        public List<RequestContactOutputDto> UpdateRequestContactAsync(long requestId, [FromBody] List<RequestContactInputDto> requestContacts)
        {
            return _requestAppService.CreateOrUpdateRequestContact(requestId, requestContacts);
        }
    }
}
