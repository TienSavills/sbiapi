﻿using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.AttachmentDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{

    [Route("api/[controller]")]
    public class AttachmentDocumentController : SBIControllerBase
    {
        private readonly IAttachmentDocumentAppService _attachmentDocumentAppService;
        public AttachmentDocumentController(IAttachmentDocumentAppService attachmentDocumentAppService)
        {
            _attachmentDocumentAppService = attachmentDocumentAppService;
        }
        [HttpGet("{fileId}")]
        public async Task<AttachmentDocumentOutputDto> GetFileById(long fileId)
        {
            return await _attachmentDocumentAppService.GetDocById(fileId);
        }
        [HttpGet("{moduleName}/{parentId}/{type}")]
        public List<AttachmentDocumentOutputDto> GetListFile(string moduleName,long parentId,string type)
        {
            return _attachmentDocumentAppService.GetByReferenceId( moduleName, parentId,type);
        }
        [HttpPost("UploadFile/{parentId}")]
        [RequestSizeLimit(AppConsts.MaxLimitSize)]
        public async Task UploadFile(long parentId, [FromBody] AttachmentDocumentInputDto input)
        {
            await _attachmentDocumentAppService.UploadFile(parentId, input);
        }
        [HttpPost("UpdateFile/{id}")]
        [RequestSizeLimit(AppConsts.MaxLimitSize)]
        public async Task UploadFile(long id, [FromBody] AttachmentDocumentUpdateInputDto input)
        {
            await _attachmentDocumentAppService.UpdateDocument(id, input);
        }
        [HttpPut("MarkMainPhoto/{moduleName}/{parentId}/{fileId}")]
        [RequestSizeLimit(AppConsts.MaxLimitSize)]
        public async Task MarkMainPhoto(string moduleName,long parentId, long fileId)
        {
            await _attachmentDocumentAppService.MarkMainPhoto(moduleName,parentId, fileId);
        }
        [HttpGet("Download/{fileId}")]
        public async Task<ActionResult> DownloadFileById(long fileId)
        {
            try
            {
                var file = await _attachmentDocumentAppService.GetDocById(fileId);
                var fileBytes = await _attachmentDocumentAppService.DownLoadFileById(fileId);
                return File(fileBytes, file.MimeType, file.FileName);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
        [HttpGet("DownloadThumb/{fileId}")]
        public async Task<ActionResult> DownloadThumbById(long fileId)
        {
            try
            {
                var file = await _attachmentDocumentAppService.GetDocById(fileId);
                var fileBytes = await _attachmentDocumentAppService.DownLoadThumbById(fileId);
                return File(fileBytes, file.MimeType, file.FileName);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
