﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using CRM.Application.Shared.Inquiries;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class InquiryController : SBIControllerBase
    {
        private readonly IInquiryAppService _inquiryAppService;
        public InquiryController(
            IInquiryAppService inquiryAppService
            )
        {
            _inquiryAppService = inquiryAppService;
        }
        [HttpGet("{id}")]
        public async Task<InquiryOutputDto> GetInquiryDetails(long id)
        {
            return await _inquiryAppService.GetInquiryDetails(id);
        }
        [HttpGet("MatchingInquiry/{listingId}")]
        public async Task<List<InquiryOutputDto>> MatchingInquiry(long listingId)
        {
            return await _inquiryAppService.MatchingInquiry(listingId);
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<InquiryOutputDto>> GetListInquiries([FromBody] object input)
        {
            return await _inquiryAppService.GetListInquiries(input.ToString());
        }
        [HttpGet("InquiryByClient/{clientId}")]
        public async Task<PagedResultDto<InquiryOutputDto>> GetListInquiryByClient(long clientId)
        {
            return await _inquiryAppService.GetListInquiryByClient(clientId);
        }
        [HttpPost("CreateOrUpdate")]
        public async Task<InquiryOutputDto> CreateOrUpdateAsync([FromBody] InquiryInputDto input)
        {
            return await _inquiryAppService.CreateOrUpdateAsync(input);
        }
    }
}
