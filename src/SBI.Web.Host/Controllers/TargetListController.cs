﻿using Abp.Application.Services.Dto;
using CRM.Application.Shared.TargetContact;
using CRM.Application.Shared.TargetList;
using CRM.Application.Shared.TargetRequest;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class TargetListController : SBIControllerBase
    {
        private readonly ITargetListAppService _targetListAppService;

        public TargetListController(ITargetListAppService targetListAppService)
        {
            _targetListAppService = targetListAppService;
        }
        [HttpPost("Create")]
        public async Task<TargetOutputDto> CreateOrUpdateAsync([FromBody] TargetInputDto input)
        {
            return await _targetListAppService.CreateOrUpdateAsync(input);
        }
        [HttpPost("CreateOrUpdateTargetByEmail")]
        public async Task<List<TargetContactOutputDto>> CreateOrUpdateTargeByEmailAsync(string email, string listTargetName)
        {
            return await _targetListAppService.CreateOrUpdateTargetByEmail(email, listTargetName);
        }
        [HttpPut("Update")]
        public async Task<TargetOutputDto> Update([FromBody] TargetInputDto input)
        {
            return await _targetListAppService.CreateOrUpdateAsync(input);
        }
        [HttpPost("FilterTargetContacts")]
        public async Task<PagedResultDto<TargetContactOutputDto>> GetListTargetContacts( [FromBody] string input)
        {
            return await _targetListAppService.GetListTargetContacts(input);
        }
        [HttpPost("FilterTargetRequests")]
        public async Task<PagedResultDto<TargetRequestOutputDto>> GetListTargetRequests([FromBody] string input)
        {
            return await _targetListAppService.GetListTargetRequests(input);
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<TargetOutputDto>> GetListTargets([FromBody]object input)
        {
            return await _targetListAppService.GetListTargets(input.ToString());
        }
        [HttpGet("Details")]
        public async Task<TargetOutputDto> GetTargetDetails(long id)
        {
            return await _targetListAppService.GetTargetDetails(id);
        }
        [HttpGet("GetTargetListByEmail")]
        public async Task<List<TargetOutputDto>> GetTargetListByEmail(string email)
        {
            return await _targetListAppService.GetTargetListByEmail(email);
        }
    }
}
