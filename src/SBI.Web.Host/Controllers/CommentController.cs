﻿using CRM.Application.Shared.Comment;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.CatTypes;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class CommentController : SBIControllerBase
    {
        private readonly ICommentAppService _commentAppService;
        public CommentController(ICommentAppService commentAppService)
        {
            _commentAppService = commentAppService;
        }
        [HttpGet("GetCommentByReference/{moduleId}/{referenceId}")]
        public async   Task<PagedResultDto<CommentOutputDto>> GetCommentByReference(int moduleId, long referenceId, FilterBasicInputDto input)
        {
            return await _commentAppService.GetCommentByReference(moduleId, referenceId, input);
        }
        [HttpGet("GetCommentDetails/{commentId}")]
        public async Task<List<CommentOutputDto>> GetCommentDetails(long commentId)
        {
            return await _commentAppService.GetCommentDetails(commentId);
        }
        [HttpPost("Create/{moduleId}/{referenceId}/{isSend}")]
        public Task<CommentOutputDto> CreateCommentAsync(int moduleId, long referenceId,[FromBody] CommentInputDto input, bool isSend= false )
        {
            return _commentAppService.CreateCommentAsync(moduleId, referenceId, input, isSend);
        }
        [HttpPut("Update/{commentId}")]
        public async Task<CommentOutputDto> UpdateCommentAsync(long commentId,[FromBody] CommentInputDto input)
        {
            return await _commentAppService.UpdateCommentAsync(commentId,input);
        }
    }
}
