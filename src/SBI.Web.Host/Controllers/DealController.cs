﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using CRM.Application.Shared.Deal;
using CRM.Application.Shared.DealInvoiceInfo;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class DealController : SBIControllerBase
    {

        private readonly IDealAppService _dealAppService;
        public DealController(IDealAppService dealAppService)
        {
            _dealAppService = dealAppService;
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<DealOutputDto>> FilterDeal([FromBody]object input)
        {
            return await _dealAppService.FilterDeal(input.ToString());
        }
        [HttpPost("FilterDealAdvisory")]
        public async Task<PagedResultDto<DealOutputDto>> FilterDealAdvisory([FromBody]object input)
        {
            return await _dealAppService.FilterDealAdvisory(input.ToString());
        }
        [HttpPost("FilterDealCommercial")]
        public async Task<PagedResultDto<DealOutputDto>> FilterDealCommercial([FromBody]object input)
        {
            return await _dealAppService.FilterDealCommercial(input.ToString());
        }
        [HttpPost("FilterTrackingInvoices")]
        public async Task<PagedResultDto<TrackingInvoiceOutputDto>> SpFilterTrackingInvoices([FromBody]object input)
        {
            return await _dealAppService.SpFilterTrackingInvoices(input.ToString());
        }
        [HttpGet("GetDetailTrackingInvoice")]
        public async Task<TrackingInvoiceOutputDto> GetDetailTrackingInvoice(long paymentId )
        {
            return await _dealAppService.GetDealTrackingInvoice(paymentId);
        }
        [HttpGet("{dealId}")]
        public async Task<DealOutputDto> GetDealDetail(long dealId)
        {
            return await _dealAppService.GetDealDetail(dealId);
        }
        [HttpGet("GetDealInvoiceInfo")]
        public async Task<DealInvoiceInfoOutputDto> GetDealInvoiceInfo(long dealId)
        {
            return await _dealAppService.GetDealInvoiceInfo(dealId);
        }
        [HttpGet("RptInvoice/{paymentId}")]
        public async Task<dynamic> GetRptInvoice(long paymentId)
        {
            return await _dealAppService.GetRptInvoice(paymentId);
        }
        [HttpGet("GetInvoice")]
        public async Task<dynamic> GetInvoice(long paymentId)
        {
            return await _dealAppService.GetInvoice(paymentId);
        }
        [HttpPost("CreateDeal")]
        public async Task<DealOutputDto> CreateDeal([FromBody] DealInputDto input)
        {
            return await _dealAppService.CreateDealAsync(input);
        }
        [HttpPost("CreateOrUpdate")]
        public async Task<DealOutputDto> CreateOrUpdate([FromBody] DealFullInputDto input)
        {
            return await _dealAppService.CreateOrDealFullAsync(input);
        }
        [HttpPut("UpdateDeal/{dealId}")]
        public async Task<DealOutputDto> UpdateDeal(long dealId, [FromBody] DealInputDto input)
        {
            return await _dealAppService.UpdateDealAsync(dealId, input);
        }
        [HttpPut("CreateOrUpdateDealShare")]
        public List<DealShareOutputDto> CreateOrUpdateDealShare([FromBody] List<DealShareInputDto> dealShares)
        {
            return _dealAppService.CreateOrUpdateDealShare(dealShares);
        }
        [HttpPut("CreateOrUpdateDealOpportuinity")]
        public List<DealOpportunityOutputDto> UpdateDealOpportunityAsync([FromBody] List<DealOpportunityInputDto> dealOpportunites)
        {
            return _dealAppService.CreateOrUpdateDealOpportunity(dealOpportunites);
        }
        [HttpPut("CreateOrUpdatePayment")]
        public List<PaymentOutputDto> UpdatePaymentAsync([FromBody] List<PaymentInputDto> payments)
        {
            return _dealAppService.CreateOrUpdatePayment(payments);
        }
        [HttpPut("CreateOrUpdatePaymentAdjust")]
        public List<PaymentOutputDto> CreateOrUpdatePaymentAdjust([FromBody] List<PaymentInputDto> payments)
        {
            return _dealAppService.CreateOrUpdatePaymentAdjust(payments);
        }
        [HttpPut("UpdateTrackingInvoice")]
        public async Task<PaymentOutputDto> UpdateTrackingInvoice([FromBody] TrackingInvoiceInputDto input)
        {
            return await _dealAppService.UpdateTrackingInvoices(input);
        }
    }
}
