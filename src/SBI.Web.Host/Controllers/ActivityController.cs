﻿using Abp.Application.Services.Dto;
using CRM.Application.Shared.Activity;
using CRM.Application.Shared.ActivityAttendee;
using CRM.Application.Shared.ActivityOrganizationUnit;
using CRM.Application.Shared.ActivityReminder;
using CRM.Application.Shared.ActivityType;
using CRM.Application.Shared.ActivityUser;
using CRM.Application.Shared.CatTypes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class ActivityController : SBIControllerBase
    {
        private readonly IActivityAppService _activityAppService;
        public ActivityController(IActivityAppService activityAppService)
        {
            _activityAppService = activityAppService;
        }
        [HttpGet("{moduleId}/{referenceId}")]

        public async Task<PagedResultDto<ActivityOutputDto>> FilterActivity(int moduleId, long referenceId, FilterBasicInputDto filter)
        {
            return await _activityAppService.GetListActivityByModule(moduleId, referenceId, filter);
        }
        [HttpGet("ActivityType")]
        public async Task<List<ActivityTypeOutputDto>> GetListActivityType(string keyword)
        {
            return await _activityAppService.GetListActivityType(keyword);
        }
        [HttpPost("ActivityByType")]
        public async Task<PagedResultDto<ActivityOutputDto>> GetListActivityByType([FromBody] ActivityFitlerDto input)
        {
            return await _activityAppService.GetListActivityByType(input);
        }
        [HttpPost("ActivityContactOfCompany")]
        public async Task<PagedResultDto<ActivityOutputDto>> GetListActivityContactOfCompany([FromBody] ActivityFitlerDto input)
        {
            return await _activityAppService.GetListActivityContactOfCompany(input);
        }
        [HttpGet("ActivityOfCompany")]
        public async Task<PagedResultDto<ActivityOutputDto>> ActivityOfCompany(long companyId)
        {
            return await _activityAppService.GetListActivityOfCompany(companyId);
        }
        [HttpGet("{activityId}")]
        public async Task<ActivityOutputDto> GetActivityDetails(long activityId)
        {
            return await _activityAppService.GetActivityDetails(activityId);
        }
        [HttpGet("GetListActivityOrganizationUnit/{activityId}")]
        public async Task<PagedResultDto<ActivityOrganizationUnitOutputDto>> GetListActivityOrganizationUnit(FilterBasicInputDto input, long activityId)
        {
            return await _activityAppService.GetListActivityOrganizationUnit(input, activityId);
        }
        [HttpGet("GetListActivityUser/{activityId}")]
        public async Task<PagedResultDto<ActivityUserOutputDto>> GetListActivityUser(long activityId, FilterBasicInputDto input)
        {
            return await _activityAppService.GetListActivityUser(input, activityId);
        }

        [HttpGet("GetListActivityReminder/{activityId}")]
        public async Task<PagedResultDto<ActivityReminderOutputDto>> GetListActivityReminder(long activityId, FilterBasicInputDto input)
        {
            return await _activityAppService.GetListActivityReminder(activityId, input);
        }
        [HttpGet("GetListActivityAttendee/{activityId}")]
        public async Task<PagedResultDto<ActivityAttendeeOutputDto>> GetListActivityAttendee(long activityId, FilterBasicInputDto input)
        {
            return await _activityAppService.GetListActivityAttendee(activityId, input);
        }
        [HttpPost("Create/{moduleId}/{referenceId}")]
        public async Task<ActivityOutputDto> CreateActivityAsync(int moduleId, long referenceId, [FromBody]ActivityInputDto input)
        {
            return await _activityAppService.CreateActivityAsync(moduleId, referenceId, input);
        }
        [HttpPost("CreateOrUpdate/{moduleId}/{referenceId}")]
        public async Task<ActivityOutputDto> CreateOrUpadteActivityFullAsync(int moduleId, long referenceId, [FromBody]ActivityFullInputDto input)
        {
            return await _activityAppService.CreateOrUpadteActivityFullAsync(moduleId, referenceId, input);
        }
        [HttpPut("Update/{activityId}")]
        public async Task<ActivityOutputDto> UpdateActivityAsync(long activityId, [FromBody] ActivityInputDto input)
        {
            return await _activityAppService.UpdateActivityAsync(activityId, input);
        }

        [HttpPut("Deactive")]
        public async Task<ActivityOutputDto> UpdateActivityAsync(long activityId)
        {
            return await _activityAppService.DeactiveActivity(activityId);
        }

        [HttpPut("ActiviteAttendee/{activityId}")]
        public List<ActivityAttendeeOutputDto> CreateOrUpdateActiviteAttendeeAsync(long activityId, [FromBody] List<ActivityAttendeeInputDto> input)
        {
            return _activityAppService.CreateOrUpdateActivityAttendeeAsync(activityId, input);
        }
        [HttpPut("ActiviteReminder/{activityId}")]
        public List<ActivityReminderOutputDto> CreateOrUpdateActiviteReminderAsync(long activityId, [FromBody] List<ActivityReminderInputDto> input)
        {
            return _activityAppService.CreateOrUpdateActivityReminderAsync(activityId, input);
        }
        [HttpPut("CreateOrUpdateUsers/{activityId}")]
        public List<ActivityUserOutputDto> UpdateActivityUsersAsync(long activityId, [FromBody] List<ActivityUserInputDto> activityUser)
        {
            return _activityAppService.CreateOrUpdateActivityUser(activityId, activityUser);
        }
        [HttpPut("CreateOrUpdateOrganizationUnits/{activityId}")]
        public List<ActivityOrganizationUnitOutputDto> UpdateActivityOrganizationUnitAsync(long activityId, [FromBody] List<ActivityOrganizationUnitInputDto> activityOrganizationUnits)
        {
            return _activityAppService.CreateOrUpdateActivityOrganizationUnit(activityId, activityOrganizationUnits);
        }
    }
}
