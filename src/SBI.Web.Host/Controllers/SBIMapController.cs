﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CRM.Services.SBIMap;

namespace CRM.Web.Host.Controllers
{
    [Route("api/[controller]/[action]")]
    public class SBIMapController : SBIControllerBase
    {
        private readonly SbiMapAppService _sbiMapAppService;
        public SBIMapController(SbiMapAppService sbiMapAppService)
        {
            _sbiMapAppService = sbiMapAppService;
        }
        [HttpGet ("GetSBIMap")]
        public  List<dynamic> GetMap(long? fromValue, long? toValue, string instructionId = null, string statusId = null, string assetClass = null,
          string departmentId = null, string projectType = null)
        {
            return _sbiMapAppService.GetSBIMap(fromValue, toValue, instructionId, statusId, assetClass, departmentId, projectType);
        }
        [HttpGet("GetSource")]
        public List<dynamic> GetSource(string tableName)
        {
            return _sbiMapAppService.GetSource(tableName);
        }
    }
}
