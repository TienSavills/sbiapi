using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CRM.Services.Authorization.Accounts;
using CRM.Services.Authorization.Accounts.Dto;
using CRM.Services.Users;
using CRM.Application.Shared.CatTypes;
using Abp.Application.Services.Dto;
using CRM.Services.Users.Dto;
using CRM.Authorization.Users;
using Microsoft.AspNetCore.Http;
using System.Linq;
using CRM.Authorization.Users.Profile.Dto;
using CRM.Sessions;
using CRM.Sessions.Dto;
using CRM.Services.Roles.Dto;
using Abp.Application.Services;
using Abp.Authorization;
using CRM.Application.Shared.Users;

namespace CRM.Web.Host.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : SBIControllerBase
    {
        private readonly IAccountAppService _accountService;
        private readonly IUserAppService _userAppService;
        private readonly ISessionAppService _sessionAppService;
        public AccountController(IAccountAppService accountService
            , IUserAppService userAppService
            , ISessionAppService sessionAppService
            )
        {
            _sessionAppService = sessionAppService;
            _accountService = accountService;
            _userAppService = userAppService;
        }

        [HttpGet("IsTenantAvailable")]
        public async Task<IsTenantAvailableOutput> IsTenantAvailable([FromBody]IsTenantAvailableInput input)
        {
            return await _accountService.IsTenantAvailable(input);
        }
        [HttpGet("GetListUser")]
        public PagedResultDto<UserDto> GetListUser(FilterBasicInputDto input)
        {
            return  _userAppService.GetListUsers(input);
        }
        [HttpGet("Get")]
        public async Task<UserDto> Get(long id)
        {
            return await _userAppService.Get(id);
        }
        [HttpGet("GetRoles")]
        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            return await _userAppService.GetRoles();
        }
        [HttpPost("ChangeLanguage")]
        public async Task ChangeLanguage([FromBody] ChangeUserLanguageDto input)
        {
             await _userAppService.ChangeLanguage(input);
        }
        [HttpPost("Register")]
        public async Task<RegisterOutput> Register([FromBody]RegisterInput input)
        {
            return await _accountService.Register(input);
        }

        [HttpPut("ResetPassword")]
        public async Task<ResetPasswordOutput> ResetPassword([FromBody]ResetPasswordInput input)
        {
            return await _accountService.ResetPassword(input);
        }
        [HttpPut("SendPasswordResetCode")]
        public async Task SendPasswordResetCode([FromBody]SendPasswordResetCodeInput input)
        {
            await _accountService.SendPasswordResetCode(input);
        }

        [HttpPost("ChangePassword")]
        public async Task ChangePassword([FromBody]ChangePasswordInput input)
        {
            await _accountService.ChangePassword(input);
        }
        [HttpPost("UpdateProfilePicture")]
        public async Task UpdateProfilePicture([FromBody]UpdateProfilePictureInput input)
        {
            await _accountService.UpdateProfilePicture(input);
        }
        [HttpPost("UploadProfilePictureTemp")]
        public UploadProfilePictureOutput UploadProfilePictureTemp()
        {
            var profilePictureFile = Request.Form.Files.First();
            return _accountService.UploadProfilePictureTemp(profilePictureFile);
        }
        [HttpGet("GetProfilePicture")]
        public async Task<GetProfilePictureOutput> GetProfilePicture()
        {
            return await _accountService.GetProfilePicture();
        }
        [HttpGet("GetCurrentLoginInformations")]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            return await _sessionAppService.GetCurrentLoginInformations();
        }
        [HttpPost("Create")]
        public async Task<UserDto> Create([FromBody] CreateUserDto input)
        {
            return await _userAppService.Create(input);
        }
        [HttpPut("Update")]
        public async Task<UserDto> Update([FromBody] UserDto input)
        {
            return await _userAppService.Update(input);
        }
        [HttpDelete("Delete")]
        public async Task Delete(EntityDto<long> input)
        {
            await _userAppService.Delete(input);
        }
    }
}
