﻿using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.CurrencyRates;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class CurrencyController: SBIControllerBase
    {
        private readonly ICurrencyRateAppService _currencyRateAppService;
        public CurrencyController(ICurrencyRateAppService currencyRateAppService)
        {
            _currencyRateAppService = currencyRateAppService;
        }
        [HttpPost("Create")]
        public async Task<CurrencyRateOutputDto> CreateCurrencyRateAsync([FromBody]CurrencyRateInputDto input)
        {
           return await _currencyRateAppService.CreateCurrencyRateAsync(input);
        }
        [HttpGet("{id}")]
        public async Task<CurrencyRateOutputDto> GetCurrencyRateDetail(long id)
        {
            return await _currencyRateAppService.GetCurrencyRateDetail(id);
        }

        [HttpGet("Filters")]
        public async Task<List<CurrencyRateOutputDto>> GetListCurrencyRates(CurrencyRateFilterInputDto filters)
        {
            return await _currencyRateAppService.GetListCurrencyRates(filters);
        }
        [HttpPut("Update/{id}")]
        public async Task<CurrencyRateOutputDto> UpdateCurrencyRateAsync(long id, [FromBody] CurrencyRateInputDto input)
        {
            return await _currencyRateAppService.UpdateCurrencyRateAsync(id,input);
        }
    }
}
