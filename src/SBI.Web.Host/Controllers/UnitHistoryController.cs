﻿using CRM.Application.Shared.UnitHistory;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class UnitHistoryController : SBIControllerBase
    {
        private readonly IUnitHistoryAppService _unitHistoryAppServce;
        public UnitHistoryController(
            IUnitHistoryAppService unitHistoryAppServce
            )
        {
            _unitHistoryAppServce = unitHistoryAppServce;
        }

        [HttpPost("Create")]
        public async Task<UnitHistoryOutputDto> CreateAsync([FromBody]UnitHistoryInputDto input)
        {
            return await _unitHistoryAppServce.CreateAsync(input);
        }
        [HttpDelete("Delete")]
        public async Task DeleteCurrentTenant(long id)
        {
             await _unitHistoryAppServce.DeleteCurrentTenant(id);
        }
        [HttpGet("GetListHistoryUnit/{unitId}")]
        public async Task<List<UnitHistoryOutputDto>> GetListUnitHistory(long unitId)
        {
            return await _unitHistoryAppServce.GetListUnitHistory(unitId);
        }

        [HttpGet("GetListHistoryUnitByCompany/{orgTenantId}")]
        public async Task<List<UnitHistoryOutputDto>> GetListUnitHistoryByCompany(long orgTenantId)
        {
            return await _unitHistoryAppServce.GetListUnitHistoryByCompany(orgTenantId);
        }
        [HttpGet("Details/{id}")]
        public async Task<UnitHistoryOutputDto> GetUnitHistoryDetails(long id)
        {
            return await _unitHistoryAppServce.GetUnitHistoryDetails(id);
        }
        [HttpPut("Update/{id}")]
        public async Task<UnitHistoryOutputDto> UpdateAsync(long id, [FromBody]UnitHistoryInputDto input)
        {
            return await _unitHistoryAppServce.UpdateAsync(id, input);
        }
    }
}
