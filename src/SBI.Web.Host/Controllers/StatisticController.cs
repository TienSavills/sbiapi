﻿using Abp.Extensions;
using CRM.Application.Shared.Statistic;
using CRM.Application.Shared.Unit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class StatisticController : SBIControllerBase
    {
        private readonly IStatisticAppService _statisticAppService;
        public StatisticController(IStatisticAppService statisticAppService)
        {
            _statisticAppService = statisticAppService;
        }
        [HttpGet("Company")]
        public Task<dynamic> CountStatusCompanyAsync()
        {
            return _statisticAppService.CountStatusCompanyAsync();
        }
        [HttpGet("Contact")]
        public Task<dynamic> CountStatusContactAsync()
        {
            return _statisticAppService.CountStatusContactAsync();
        }
        [HttpGet("Deal")]
        public Task<dynamic> CountStatusDealAsync()
        {
            return _statisticAppService.CountStatusDealAsync();
        }
        [HttpGet("Opportunity")]
        public Task<dynamic> CountStatusOpportunityAsync(DateTime? dateFrom, DateTime? dateTo, [FromQuery] int[] organizationUnitIds, string departmentIds, int? officeId)
        {
            return _statisticAppService.CountStatusOpportunityAsync(dateFrom, dateTo, organizationUnitIds, departmentIds, officeId);
        }
        [HttpGet("CountOpportunityStatusV1")]
        public Task<dynamic> CountStatusOpportunityAsyncV1(DateTime? dateFrom, DateTime? dateTo, [FromQuery] int[] departmentIds, int? officeId)
        {
            return _statisticAppService.CountStatusOpportunityAsyncV1(dateFrom, dateTo, departmentIds, officeId);
        }
        [HttpGet("OpportunityStage")]
        public Task<dynamic> CountStatusOpportunityStageAsync(DateTime? dateFrom, DateTime? dateTo, string departmentIds, int? officeId)
        {
            return _statisticAppService.CountOpportunityStageAsync(dateFrom, dateTo, departmentIds, officeId);
        }
        [HttpGet("CountOpportunityStageV1")]
        public Task<dynamic> CountStatusOpportunityStageAsyncV1(DateTime? dateFrom, DateTime? dateTo, [FromQuery] int[] departmentIds, int? officeId)
        {
            return _statisticAppService.CountOpportunityStageAsyncV1(dateFrom, dateTo, departmentIds, officeId);
        }
        [HttpGet("OpportunityLastModify")]
        public Task<dynamic> CountStatusOpportunityLastModifyAsync()
        {
            return _statisticAppService.CountOpportunityLastModifyAsync();
        }
        [HttpGet("ContactLastModify")]
        public Task<dynamic> CountStatusContactLastModifyAsync()
        {
            return _statisticAppService.CountLastModifyAsync();
        }
        [HttpGet("OpportunityDepartment")]
        public Task<dynamic> CountOpportunityDeparmentStatusAsync(DateTime? dateFrom, DateTime? dateTo, [FromQuery] int[] organizationUnitIds, string teamIds, int? officeId)
        {
            return _statisticAppService.CountOpportunityDeparmentStatusAsync(dateFrom, dateTo, organizationUnitIds, teamIds, officeId);
        }
        [HttpGet("Property")]
        public Task<dynamic> CountStatusPropertyAsync()
        {
            return _statisticAppService.CountStatusPropertyAsync();
        }
        [HttpGet("Request")]
        public Task<dynamic> CountStatusRequestAsync()
        {
            return _statisticAppService.CountStatusRequestAsync();
        }

        [HttpGet("GetListUnitExpired")]
        public async Task<List<UnitOutputDto>> GetListUnitExpiryAsync()
        {
            return await _statisticAppService.GetListUnitExpiryAsync();
        }
        [HttpGet("CountUnitExpired")]
        public Task<dynamic> CountUnitExpiryAsync(long? projectId)
        {
            return _statisticAppService.CountUnitExpiryAsync(projectId);
        }
        [HttpGet("CountUnitStatus")]
        public Task<dynamic> CountUnitStatusAsync(long? projectId)
        {
            return _statisticAppService.CountUnitStatusAsync(projectId);
        }
        [HttpGet("CountUnitType")]
        public Task<dynamic> CountUnitTypeAsync(long? projectId)
        {
            return _statisticAppService.CountUnitTypeAsync(projectId);
        }
        [HttpPost("DealOpportunityRevenue")]
        public Task<dynamic> DealOpportunityRevenue([FromBody]object input)
        {
            return _statisticAppService.DealOpportunityRevenue(input.ToString());
        }

        [HttpPost("MMMReport")]
        public Task<dynamic> MMMReport([FromBody]object input)
        {
            return _statisticAppService.MMMReport(input.ToString());
        }
        [HttpPost("UserActivities")]
        public Task<dynamic> UserActivities([FromBody]object input)
        {
            return _statisticAppService.UserActivities(input.ToString());
        }
        [HttpGet("UserActivitiesFromTo")]
        public Task<dynamic> UserActivitiesFromTo(DateTime dateFrom, DateTime dateTo, string teamIds, int? officeId, string keyword)
        {
            return _statisticAppService.UserActivitiesFromTo(dateFrom, dateTo, teamIds, officeId, keyword);
        }
        [HttpPost("ReportCommercial")]
        public Task<dynamic> ReportCommercial([FromBody]object input)
        {
            return _statisticAppService.ReportCommercial(input.ToString());
        }
        [HttpGet("CountProjectType")]
        public async Task<dynamic> CountProjectTypeAsync(long? projectId)
        {
            return await _statisticAppService.CountProjectTypeAsync(projectId);
        }
        [HttpGet("CountProjectFacility")]
        public async Task<dynamic> CountProjectFacilityAsync(long? projectId)
        {
            return await _statisticAppService.CountProjectFacilityAsync(projectId);
        }
        [HttpGet("CountProjectGrade")]
        public async Task<dynamic> CountProjectGradeAsyn(long? projectId)
        {
            return await _statisticAppService.CountProjectGradeAsync(projectId);
        }
        [HttpGet("ExchangeRate")]
        public async Task<dynamic> ExchangeRate(string basic)
        {
            return await _statisticAppService.ExchangeRate(basic);
        }

        [HttpGet("GetLinkDashboard")]
        public string GetLinkDashboard()
        {
            return _statisticAppService.GetLinkDashboard();
        }
    }
}
