﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.MacroIndicator;
using CRM.Services.MacroIndicators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class MacroIndicatorController : SBIControllerBase
    {
        private readonly MacroIndicatorAppService _macroIndicatorAppservice;

        public MacroIndicatorController(MacroIndicatorAppService macroIndicatorAppservice)
        {
            _macroIndicatorAppservice = macroIndicatorAppservice;
        }
        [HttpPost("Create")]
        public async Task<MacroIndicatorOutputDto> CreateMacroIndicatorAsync([FromBody]MacroIndicatorInputDto input)
        {
            return await _macroIndicatorAppservice.CreateMacroIndicatorAsync(input);
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<MacroIndicatorOutputDto>> FilterMacroIndicatorAsync([FromBody]object input)
        {
            return await _macroIndicatorAppservice.FilterMacroIndicatorAsync(input.ToString());
        }
        [HttpGet("{id}")]
        public async Task<MacroIndicatorOutputDto> GetMacroIndicatorDetail(long id)
        {
            return await _macroIndicatorAppservice.GetMacroIndicatorDetail(id);
        }
        [HttpPut("Update/{id}")]
        public async Task<MacroIndicatorOutputDto> UpdateMacroIndicatorAsync(long id, [FromBody]MacroIndicatorInputDto input)
        {
            return await _macroIndicatorAppservice.UpdateMacroIndicatorAsync(id, input);
        }
    }
}
