﻿using Abp.Application.Services.Dto;
using CRM.Application.Shared.Campaign;
using CRM.Application.Shared.TargetList;
using CRM.ResponseHelper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class CampaignController : SBIControllerBase
    {
        private readonly ICampaignAppService _capmpaignAppService;
        public CampaignController(ICampaignAppService capmpaignAppService,
             ITargetListAppService targetListAppService)
        {
            _capmpaignAppService = capmpaignAppService;
        }
        [HttpPost("Create")]
        public async Task<CampaignOutputDto> Create([FromBody] CampaignInputDto input)
        {
            return await _capmpaignAppService.CreateOrUpdateAsync(input);
        }
        [HttpPut("Update")]
        public async Task<CampaignOutputDto> Update([FromBody] CampaignInputDto input)
        {
            return await _capmpaignAppService.CreateOrUpdateAsync(input);
        }
        [HttpGet("Details")]
        public async Task<CampaignOutputDto> GetCampaignDetails(long campaignId)
        {
            return await _capmpaignAppService.GetCampaignDetails(campaignId);
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<CampaignOutputDto>> GetListCampaigns([FromBody]object input)
        {
            return await _capmpaignAppService.GetListCampaigns(input.ToString());
        }
        [HttpPost("FilterCampaignContacts")]
        public async Task<List<CampaignContactRequestOutputDto>> GetListCampaignContacts([FromBody] string input)
        {
            return await _capmpaignAppService.GetListCampaignContactRequest(input);
        }
        [HttpPost("FilterCampaignEmailHistories")]
        public async Task<PagedResultDto<EmailHistoryOutputDto>> GetListCampaignEmailHistory(long campaignId,[FromBody] string input)
        {
            return await _capmpaignAppService.GetListCampaignEmailHistory(campaignId,input);
        }
        [HttpPost("SendMail")]
        public async Task SendMail(long campaignId, [FromBody] List<CampaignContactRequestInputDto> input)
        {
            await _capmpaignAppService.SendEmail(campaignId, input);

        }
        [HttpGet("GetTemplateSendgrid")]
        public async Task<TempleteSendgridOutputDto> GetTempleteSendgrid(string templetType )
        {
            return await _capmpaignAppService.GetTempleteSendgrid(templetType);
        }
    }
}
