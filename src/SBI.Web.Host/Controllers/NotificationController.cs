﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using CRM.Application.Shared.Deal;
using CRM.Application.Shared.DealInvoiceInfo;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;
using CRM.Services.Notifications;
using CRM.Services.Notifications.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class NotificationController : SBIControllerBase
    {

        private readonly INotificationAppService _notificationAppService;
        public NotificationController(INotificationAppService notificationAppService)
        {
            _notificationAppService = notificationAppService;
        }
        [HttpGet("GetUserNotifications")]
        public async Task<GetNotificationsOutput> GetUserNotifications(GetUserNotificationsInput input)
        {
            return await _notificationAppService.GetUserNotifications(input);
        }

        [HttpPost("SetAllNotificationsAsRead")]
        public async Task SetAllNotificationsAsRead()
        {
            await _notificationAppService.SetAllNotificationsAsRead();
        }
        [HttpPut("SetNotificationAsRead")]
        public async Task SetNotificationAsRead(EntityDto<Guid> input)
        {
            await _notificationAppService.SetNotificationAsRead(input);
        }
        [HttpGet("GetNotificationSettings")]
        public async Task<GetNotificationSettingsOutput> GetNotificationSettings()
        {
            return await _notificationAppService.GetNotificationSettings();
        }
        [HttpPut("UpdateNotificationSettings")]
        public async Task UpdateNotificationSettings(UpdateNotificationSettingsInput input)
        {
            await _notificationAppService.UpdateNotificationSettings(input);
        }
        [HttpDelete("DeleteNotification")]
        public async Task DeleteNotification(EntityDto<Guid> input)
        {
            await _notificationAppService.DeleteNotification(input);
        }
        [HttpDelete("DeleteAllUserNotifications")]
        public async Task DeleteAllUserNotifications(DeleteAllUserNotificationsInput input)
        {
            await _notificationAppService.DeleteAllUserNotifications(input);
        }
    }
}
