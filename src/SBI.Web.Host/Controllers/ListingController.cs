﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using CRM.Application.Shared.Listings;

namespace CRM.Web.Host.Controllers
{
    [Route("api/[controller]")]
    public class ListingController : SBIControllerBase
    {
        private readonly IListingAppService _listingAppService;
        public ListingController(
            IListingAppService listingAppService
            )
        {
            _listingAppService = listingAppService;
        }
        [HttpGet("{id}")]
        public async Task<ListingOutputDto> GetListingDetails(long id)
        {
            return await _listingAppService.GetListingDetails(id);
        }
        [HttpGet("MatchingListing/{inquiryId}")]
        public async Task<List<ListingOutputDto>> MatchingRequest(long inquiryId)
        {
            return await _listingAppService.MatchingListing(inquiryId);
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<ListingOutputDto>> GetListListings([FromBody] object input)
        {
            return await _listingAppService.GetListListings(input.ToString());
        }

        [HttpPost("CreateOrUpdate")]
        public async Task<ListingOutputDto> CreateOrUpdateAsync([FromBody] ListingInputDto input)
        {
            return await _listingAppService.CreateOrUpdateAsync(input);
        }

        [HttpPost("GetListListingForClient")]
        public async Task<PagedResultDto<ListingOutputDto>> GetListListingForClient(long companyId, [FromBody] object input)
        {
            return await _listingAppService.GetListListingForClient(companyId, input.ToString());
        }

        [HttpPost("ListingForLandingPages")]
        public async Task<PagedResultDto<ListingOutputDto>> GetListListingForClient(long? projectId, [FromBody] object input)
        {
            return await _listingAppService.ListingForLandingPages(projectId, input.ToString());
        }
    }
}
