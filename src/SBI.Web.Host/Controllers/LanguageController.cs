using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CRM.Services.Users;
using Abp.Application.Services.Dto;
using CRM.Authorization.Users;
using System.Linq;
using Abp.Authorization;
using CRM.Authorization.Users.Profile.Dto;
using CRM.Services.Localization;
using CRM.Services.Localization.Dto;
using Castle.MicroKernel.Util;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class LanguageController : SBIControllerBase 
    {
        private readonly ILanguageAppService _languageService;

        public LanguageController(ILanguageAppService languageService)
        {
            _languageService = languageService;
        }
        [HttpPost("CreateOrUpdateLanguage")]
        public async Task CreateOrUpdateLanguage(CreateOrUpdateLanguageInput input)
        {
           await _languageService.CreateOrUpdateLanguage(input);
        }
        [HttpDelete ("DeleteLanguage")]
        public async Task DeleteLanguage(EntityDto input)
        {
            await _languageService.DeleteLanguage(input);
        }
        [HttpGet("GetLanguageForEdit")]
        public async Task<GetLanguageForEditOutput> GetLanguageForEdit(NullableIdDto input)
        {
           return await _languageService.GetLanguageForEdit(input);
        }
        [HttpGet("GetLanguages")]
        public async Task<GetLanguagesOutput> GetLanguages()
        {
            return await _languageService.GetLanguages();
        }
        [HttpGet("GetLanguageTexts")]
        public async Task<PagedResultDto<LanguageTextListDto>> GetLanguageTexts(GetLanguageTextsInput input)
        {
            return await _languageService.GetLanguageTexts(input);
        }

        [HttpPut("SetDefaultLanguage")]
        public async Task SetDefaultLanguage(SetDefaultLanguageInput input)
        {
             await _languageService.SetDefaultLanguage(input);
        }
        [HttpPut("UpdateLanguageText")]
        public async Task UpdateLanguageText(UpdateLanguageTextInput input)
        {
            await _languageService.UpdateLanguageText(input);
        }
    }
}
