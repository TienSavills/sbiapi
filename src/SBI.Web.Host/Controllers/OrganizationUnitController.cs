﻿using CRM.Application.Shared.OrganizationUnit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.OrganizationUnit.Dto;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class OrganizationUnitController : SBIControllerBase
    {
        private readonly IOrganizationUnitAppService _organizationUnitAppService;
        public OrganizationUnitController(IOrganizationUnitAppService organizationUnitAppService)
        {
            _organizationUnitAppService = organizationUnitAppService;
        }
        [HttpPost("AddUsersToOrganizationUnit")]
        public async Task AddUsersToOrganizationUnit([FromBody]UsersHeadToOrganizationUnitInput input)
        {
            await _organizationUnitAppService.AddUsersToOrganizationUnit(input);
        }
        [HttpPost("CreateOrganizationUnit")]
        public async Task<OrganizationUnitDto> CreateOrganizationUnit([FromBody]CreateOrganizationUnitInput input)
        {
            return await _organizationUnitAppService.CreateOrganizationUnit(input);
        }
        [HttpDelete("DeleteOrganizationUnit")]
        public async Task DeleteOrganizationUnit(EntityDto<long> input)
        {
            await _organizationUnitAppService.DeleteOrganizationUnit(input);
        }
        [HttpGet("FindUsers")]
        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindOrganizationUnitUsersInput input)
        {
            return await _organizationUnitAppService.FindUsers(input);
        }
        [HttpGet("GetOrganizationUnits")]
        public async Task<ListResultDto<OrganizationUnitDto>> GetOrganizationUnits(int? parentId)
        {
            return await _organizationUnitAppService.GetOrganizationUnits(parentId);
        }
        [HttpGet("GetListOffice")]
        public async Task<ListResultDto<OrganizationUnitDto>> GetListOffice()
        {
            return await _organizationUnitAppService.GetListOffice();
        }
        [HttpGet("GetOrganizationUnitUsers")]
        public async Task<PagedResultDto<OrganizationUnitUserListDto>> GetOrganizationUnitUsers(GetOrganizationUnitUsersInput input)
        {
            return await _organizationUnitAppService.GetOrganizationUnitUsers(input);
        }
        [HttpPost("MoveOrganizationUnit")]
        public async Task<OrganizationUnitDto> MoveOrganizationUnit([FromBody] MoveOrganizationUnitInput input)
        {
            return await _organizationUnitAppService.MoveOrganizationUnit(input);
        }
        [HttpDelete("RemoveUserFromOrganizationUnit")]
        public async Task RemoveUserFromOrganizationUnit(UserToOrganizationUnitInput input)
        {
            await _organizationUnitAppService.RemoveUserFromOrganizationUnit(input);
        }
        [HttpPost("UpdateOrganizationUnit")]
        public async Task<OrganizationUnitDto> UpdateOrganizationUnit([FromBody] UpdateOrganizationUnitInput input)
        {
            return await _organizationUnitAppService.UpdateOrganizationUnit(input);
        }
    }
}
