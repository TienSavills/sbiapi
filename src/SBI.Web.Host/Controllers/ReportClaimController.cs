﻿using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.ReportClaims;
using CRM.Services.ReportClaims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class ReportClaimController : SBIControllerBase
    {
        private readonly ReportClaimAppService _reportClaimAppService;
        public ReportClaimController(ReportClaimAppService reportClaimAppService)
        {
            _reportClaimAppService = reportClaimAppService;
        }
        [HttpPost("GetReport/{nameStore}")]
        public async Task <dynamic> GetReportClaim([FromBody] string paramJson, string nameStore)
        {
            return await _reportClaimAppService.GetReportClaim(paramJson,"Sp"+ nameStore);
        }
    }
}
