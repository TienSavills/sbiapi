﻿using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;
using CRM.Application.Shared;
using CRM.Application.Shared.Countries;
using CRM.Application.Shared.Districts;
using CRM.Application.Shared.Provinces;
using CRM.Application.Shared.ConstructionStatus;
using CRM.Application.Shared.PropertyTypes;
using CRM.Application.Shared.TenantTypes;
using CRM.Application.Shared.Grades;
using CRM.Application.Shared.Currentcys;
using CRM.Application.Shared.DocumentTypes;
using CRM.Application.Shared.Wards;
using CRM.Application.Shared.TenureTypes;
using CRM.Application.Shared.ProjectStates;
using CRM.Application.Shared.RoomTypes;
using CRM.Application.Shared.Industry;
using CRM.Application.Shared.ClientType;
using CRM.Application.Shared.Level;
using CRM.Application.Shared.LeadSource;
using CRM.Application.Shared.Category;
using CRM.Application.Shared.OtherCategory;
using CRM.Application.Shared.OpportunityCategory;
using CRM.Application.Shared.AssetClass;
using CRM.Application.Shared.Instruction;
using CRM.Application.Shared.ProjectCategory;
using CRM.Application.Shared.UnitCategory;
using CRM.Application.Shared.CampaignCategory;
using CRM.Application.Shared.InquiryCategory;
using CRM.Application.Shared.ListingCategory;
using CRM.Application.Shared.News;
using CRM.Application.Shared.Project.Dto;

namespace CRM.Web.Host.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : SBIControllerBase
    {
        private readonly ICategoryAppService _categoryAppService;
        public CategoryController(ICategoryAppService categoryAppService)
        {
            _categoryAppService = categoryAppService;
        }

        [HttpGet("GetListCountry")]
        public async Task<List<CountryOutputDto>> GetListCountry(int? id)
        {
            return await _categoryAppService.GetListCountry(id);
        }
        [HttpGet("GetListCountryFull")]
        public async Task<List<CountryOutputDto>> GetListCountryFull()
        {
            return await _categoryAppService.GetListCountryFull();
        }
        [HttpGet("GetListProjectCountry")]
        public async Task<List<CountryOutputDto>> GetListProjectCountry(int? id)
        {
            return await _categoryAppService.GetListProjectCountry(id);
        }
        [HttpGet("GetListCountryByModule")]
        public async Task<List<CountryOutputDto>> GetListCountryByModule(bool? isCompany, bool? isContact)
        {
            return await _categoryAppService.GetListCountryByModule(isCompany, isContact);
        }
        [HttpGet("GetListDistrict/{provinceId}")]
        public async Task<List<DistrictOutputDto>> GetListDistrict(int provinceId)
        {
            return await _categoryAppService.GetListDistrict(provinceId);
        }
        [HttpGet("GetListProjectDistrict/{provinceId}")]
        public async Task<List<DistrictOutputDto>> GetListProjectDistrict(int provinceId)
        {
            return await _categoryAppService.GetListProjectDistrict(provinceId);
        }

        [HttpGet("GetListDistrictByModule/{provinceId}")]
        public async Task<List<DistrictOutputDto>> GetListDistrictByModule(int provinceId, bool? isCompany, bool? isContact)
        {
            return await _categoryAppService.GetListDistrictByModule(provinceId, isCompany, isContact);
        }

        [HttpGet("GetListProvinceByModule/{countryId}")]
        public async Task<List<ProvinceOutputDto>> GetListProvinceByModule(int countryId, bool? isCompany, bool? isContact)
        {
            return await _categoryAppService.GetListProvinceByModule(countryId, isCompany, isContact);
        }
        [HttpGet("GetListProvince/{countryId}")]
        public async Task<List<ProvinceOutputDto>> GetListProvince(int countryId)
        {
            return await _categoryAppService.GetListProvince(countryId);
        }
        [HttpGet("GetListProjectProvince/{countryId}")]
        public async Task<List<ProvinceOutputDto>> GetListProjectProvince(int countryId)
        {
            return await _categoryAppService.GetListProjectProvince(countryId);
        }
        [HttpGet("GetListFacilities")]
        public async Task<List<CategoryOutputBasic>> GetListFacility(string keyword)
        {
            return await _categoryAppService.GetListFacility(keyword);
        }
        [HttpGet("GetListFacings")]
        public async Task<List<CategoryOutputBasic>> GetListFacings(string keyword)
        {
            return await _categoryAppService.GetListFacing(keyword);
        }
        [HttpGet("GetListPropertyTypes")]
        public async Task<List<PropertyTypeOutputDto>> GetListPropertyType(string keyword)
        {
            return await _categoryAppService.GetListPropertyType(keyword);
        }

        [HttpGet("GetListConstructionStatus")]
        public async Task<List<ConstructionStatusOuputDto>> GetListConstructionStatus(string keyword)
        {
            return await _categoryAppService.GetListConstructionStatus(keyword);
        }

        //[HttpGet("GetListTenantTypes")]
        //public async Task<List<TenantTypeOutputDto>> GetListTenantType(string keyword)
        //{
        //    return await _categoryAppService.GetListTenantType(keyword);
        //}

        [HttpGet("GetListGrades")]
        public async Task<List<GradeOutputDto>> GetListGrade(string keyword)
        {
            return await _categoryAppService.GetListGrade(keyword);
        }

        [HttpGet("GetListViews")]
        public async Task<List<CategoryOutputBasic>> GetListView(string keyword)
        {
            return await _categoryAppService.GetListView(keyword);
        }
        [HttpGet("GetListTransportations")]
        public async Task<List<CategoryOutputBasic>> GetListTransportation(string keyword)
        {
            return await _categoryAppService.GetListTransportation(keyword);
        }
        [HttpGet("GetListCurrencies")]
        public async Task<List<CurrencyOutputDto>> GetListCurrencies(string keyword)
        {
            return await _categoryAppService.GetListCurrencies(keyword);
        }
        [HttpGet("GetListDocumentTypes")]
        public async Task<List<DocumentTypeOutputDto>> GetListDocumentTypes(string keyword)
        {
            return await _categoryAppService.GetListDocumentTypes(keyword);
        }
        [HttpGet("GetListWards")]
        public async Task<List<WardOutputDto>> GetListWard(int? districtId)
        {
            return await _categoryAppService.GetListWard(districtId);
        }
        [HttpGet("GetListTenureTypes")]
        public async Task<List<TenureTypeOutputDto>> GetListTenureType(string keyword)
        {
            return await _categoryAppService.GetListTenureType(keyword);
        }

        [HttpGet("GetListProjectStates")]
        public async Task<List<ProjectStateOutputDto>> GetListProjectState(string keyword)
        {
            return await _categoryAppService.GetListProjectState(keyword);
        }
        [HttpGet("GetListRoomTypes")]
        public async Task<List<RoomTypeOutputDto>> GetListRoomType(string keyword)
        {
            return await _categoryAppService.GetListRoomType(keyword);
        }
        [HttpGet("GetListIndustries")]
        public async Task<List<IndustryOutputDto>> GetListIndustries(string keyword)
        {
            return await _categoryAppService.GetListIndustries(keyword);
        }
        [HttpGet("GetListClientTypes")]
        public async Task<List<ClientTypeOutputDto>> GetListClientType(string keyword)
        {
            return await _categoryAppService.GetListClientType(keyword);
        }

        [HttpGet("GetListLevels")]
        public async Task<List<LevelOutputDto>> GetListLevels(string keyword)
        {
            return await _categoryAppService.GetListLevels(keyword);
        }
        [HttpGet("GetListLeadSources")]
        public async Task<List<LeadSourceOutputDto>> GetListLeadSource(string keyword)
        {
            return await _categoryAppService.GetListLeadSource(keyword);
        }
        [HttpGet("GetListOtherCategories")]
        public async Task<List<OtherCategoryOutputDto>> GetListCategory(string keyword)
        {
            return await _categoryAppService.GetListOtherCategory(keyword);
        }
        [HttpGet("GetListOpportunityCategories")]
        public async Task<List<OpportunityCategoryOutputDto>> GetListOpportunityCategory(string keyword)
        {
            return await _categoryAppService.GetListOpportunityCategory(keyword);
        }
        [HttpGet("GetListAssetClass")]
        public async Task<List<AssetClassOutputDto>> GetListAssetClass(string keyword)
        {
            return await _categoryAppService.GetListAssetClass(keyword);
        }
        [HttpGet("GetListInstruction")]
        public async Task<List<InstructionOutputDto>> GetListInstruction(string keyword)
        {
            return await _categoryAppService.GetListInstruction(keyword);
        }
        [HttpGet("GetListProjectCategory")]
        public async Task<List<ProjectCategoryOutputDto>> GetListProjectCategory(string keyword)
        {
            return await _categoryAppService.GetListProjectCategory(keyword);
        }
        [HttpGet("GetListUnitCategory")]
        public async Task<List<UnitCategoryOutputDto>> GetListUnitCategory(string keyword)
        {
            return await _categoryAppService.GetListUnitCategory(keyword);
        }
        [HttpGet("GetListListingCategory")]
        public async Task<List<ListingCategoryOutputDto>> GetListListingCategory(string keyword)
        {
            return await _categoryAppService.GetListListingCategory(keyword);
        }
        [HttpGet("GetInquiryCategory")]
        public async Task<List<InquiryCategoryOutputDto>> GetListInquiryCategory(string keyword)
        {
            return await _categoryAppService.GetListInquiryCategory(keyword);
        }
        [HttpGet("GetListCampaignCategory")]
        public async Task<List<CampaignCategoryOutputDto>> GetListCampaignCategory(string keyword)
        {
            return await _categoryAppService.GetListCampaignCategory(keyword);
        }
        [HttpGet("GetMapProperty")]
        public async Task<List<ProjectMapOutputDto>> GetMapProperty(string searchJson)
        {
            return await _categoryAppService.GetMapProperty(searchJson);
        }
        [HttpGet("GetListStageAdvisory")]
        public async Task<List<OpportunityCategoryOutputDto>> GetListStageAdvisory(string keyword)
        {
            return await _categoryAppService.GetListStageAdvisory(keyword);
        }
        [HttpGet("GetListDealStatusAdvisory")]
        public async Task<List<OpportunityCategoryOutputDto>> GetListDealStatusAdvisory(string keyword)
        {
            return await _categoryAppService.GetListDealStatusAdvisory(keyword);
        }
        [HttpGet("GetListStageCommercial")]
        public async Task<List<OpportunityCategoryOutputDto>> GetListStageCommercial(string keyword)
        {
            return await _categoryAppService.GetListStageCommercial(keyword);
        }
        [HttpGet("GetListDealStatusCommercial")]
        public async Task<List<OpportunityCategoryOutputDto>> GetListDealStatusCommercial(string keyword)
        {
            return await _categoryAppService.GetListDealStatusCommercial(keyword);
        }
        [HttpGet("GetListNewsCategory")]
        public async Task<List<NewsCategoryOutputDto>> GetListNewsCategory(string keyword)
        {
            return await _categoryAppService.GetListNewsCategory(keyword);
        }
    }
}
