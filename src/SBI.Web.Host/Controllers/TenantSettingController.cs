﻿using Microsoft.AspNetCore.Mvc;
using CRM.Services.TenantSetting;
using System.Threading.Tasks;
using Abp.Authorization;
using CRM.Application.Shared.TenantSetting;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class TenantSettingController : SBIControllerBase
    {
        private readonly TenantSettingsAppService _tenantSettingsAppService;
        public TenantSettingController(TenantSettingsAppService tenantSettingsAppService)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
           
        }
       [HttpGet("GetAllSettings")]
        public async Task<TenantSettingsEditDto> GetAllSettings()
        {
            return await _tenantSettingsAppService.GetAllSettings();
        }
        [HttpPost("UpdateAllSettings")]
        public async Task UpdateAllSettings(TenantSettingsEditDto input)
        {
            await _tenantSettingsAppService.UpdateAllSettings(input);
        }
    }
}
