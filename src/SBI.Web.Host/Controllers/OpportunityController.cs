﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using CRM.Application.Shared.Opportunity;
using CRM.Application.Shared.OpportunityOrganizationUnit;
using CRM.Application.Shared.OpportunityUser;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.OpportunityContact;
using CRM.Application.Shared.OpportunityLeadUser;
using System;
using CRM.EntitiesCustom;
using System.IO;
using SendGrid.Helpers.Mail;

namespace CRM.Web.Host.Controllers
{
    [Route("api/[controller]")]
    public class OpportunityController : SBIControllerBase
    {
        private readonly IOpportunityAppService _opportunityAppService;
        public OpportunityController(IOpportunityAppService opportunityAppService)
        {
            _opportunityAppService = opportunityAppService;
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<OpportunityOutputDto>> Filter([FromBody]object input)
        {
            return await _opportunityAppService.Filter(input.ToString());
        }
        [HttpPost("OpportunityAdvisory")]
        public async Task<PagedResultDto<OpportunityOutputDto>> OpportunityAdvisory([FromBody]object input)
        {
            return await _opportunityAppService.FilterOpportunityAdvisory(input.ToString());
        }
        [HttpPost("OpportunityCommercial")]
        public async Task<PagedResultDto<OpportunityOutputDto>> FilterOpportunityCommercial([FromBody]object input)
        {
            return await _opportunityAppService.FilterOpportunityCommercial(input.ToString());
        }
        [HttpGet("ExportOportunityDetails")]
        public async Task<Attachment> ExportOportunityDetails(DateTime? dateFrom, DateTime? dateTo, int? officeId, string departmentIds, string statusIds)
        {
            return await _opportunityAppService.GenerateOpportunityFile(dateFrom, dateTo, officeId, departmentIds, statusIds);
        }
        [HttpGet("DownloadOpportunityDetails")]
        public async Task<byte[]> DownloadOpportunity(DateTime? dateFrom, DateTime? dateTo, int? officeId, string departmentIds, string statusIds)
        {
            return await _opportunityAppService.DownloadOpportunity(dateFrom, dateTo, officeId, departmentIds, statusIds);
        }

        [HttpGet("ReportOpportunityDetails")]
        public async Task<List<ExportOpportunityOutput>> FilterOpportunity(DateTime? dateFrom, DateTime? dateTo, [FromQuery] int[] organizationUnitIds, int? officeId, string departmentIds, string statusIds)
        {
            return await _opportunityAppService.ExportOpportunity(dateFrom, dateTo, organizationUnitIds, officeId, departmentIds, statusIds);
        }

        [HttpPost("SuggestOpportunity")]
        public async Task<PagedResultDto<OpportunityOutputDto>> OpportunityNotInDeal([FromBody]string input)
        {
            return await _opportunityAppService.OpportunityNotInDeal(input);
        }

        [HttpGet("{opportunityId}")]
        public async Task<OpportunityOutputDto> GetOpportunityDetail(long opportunityId)
        {
            return await _opportunityAppService.GetOpportunityDetail(opportunityId);
        }
        [HttpPost("CreateOpportunity")]
        public async Task<OpportunityOutputDto> CreateOpportunity([FromBody] OpportunityInputDto input)
        {
            return await _opportunityAppService.CreateOpportunityAsync(input);
        }
        [HttpPost("CreateOrUpdateOpportunity")]
        public async Task<OpportunityOutputDto> CreateOrUpdateOpportunity([FromBody] OpportunityFullInputDto input)
        {
            return await _opportunityAppService.CreateOrOpportunityFullAsync(input);
        }

        [HttpPut("UpdateOpportunity/{opportunityId}")]
        public async Task<OpportunityOutputDto> UpdateOpportunity(long opportunityId, [FromBody] OpportunityInputDto input)
        {
            return await _opportunityAppService.UpdateOpportunityAsync(opportunityId, input);
        }

        [HttpPut("CreateOrUpdateOpportunityOrganizationUnit")]
        public List<OpportunityOrganizationUnitOutputDto> CreateOrUpdateOpportunityOrganizationUnit([FromBody] List<OpportunityOrganizationUnitInputDto> opportunityOrganizationUnits)
        {
            return _opportunityAppService.CreateOrUpdateOpportunityOrganizationUnit(opportunityOrganizationUnits);
        }
        [HttpPut("CreateOrUpdateOpportunityUser")]
        public List<OpportunityUserOutputDto> CreateOrUpdateOpportunityUser([FromBody] List<OpportunityUserInputDto> opportunityUsers)
        {
            return _opportunityAppService.CreateOrUpdateOpportunityUser(opportunityUsers);
        }
        [HttpPut("CreateOrUpdateOpportunityLead")]
        public List<OpportunityLeadOutputDto> CreateOrUpdateOpportunityLead([FromBody] List<OpportunityLeadInputDto> opportunityLeads)
        {
            return _opportunityAppService.CreateOrUpdateOpportunityLead(opportunityLeads);
        }
        [HttpPut("CreateOrUpdateOpportunityContact")]
        public List<OpportunityContactOutputDto> UpdateOpportunityContactAsync([FromBody] List<OpportunityContactInputDto> opportunityContacts)
        {
            return _opportunityAppService.CreateOrUpdateOpportunityContact(opportunityContacts);
        }

        //[HttpGet("DownloadOpportunity")]
        //public MemoryStream DownloadFileById()
        //{
        //    var file = _opportunityAppService.GenerateOpportunityFile(null);
        //    return file;
        //}
        [HttpGet("WeeklyReport")]
        public Task<bool> WeeklyReportOpportunity(string displayName, string email)
        {
            return _opportunityAppService.SendMailWeeklyReport(displayName, email);
        }
        [HttpGet("SendMailOpportunity")]
        public async Task SendMailOpportunity(long opptunityId)
        {
            await _opportunityAppService.SendMailOpportunity(opptunityId);
        }
        [HttpPost("ConvertOpportunityToDeal")]
        public bool ConvertOpportunityToDeal(long opportunityId)
        {
            return _opportunityAppService.ConvertOpportunityToDeal(opportunityId);
        }
    }
}
