﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using CRM.Application.Shared.Contact;
using CRM.Services.Contact;
using CRM.Application.Shared.ContactEmail;
using CRM.Application.Shared.ContactPhone;
using CRM.Application.Shared.ContactUser;
using CRM.Application.Shared.ContactOrganizationUnit;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.ContactAddress;
using CRM.EntitiesCustom;
using CRM.Application.Shared.ContactTypeMap;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class ContactController : SBIControllerBase
    {
        private readonly IContactAppService _contactAppService;
        public ContactController(IContactAppService contactAppService)
        {
            _contactAppService = contactAppService;
        }
        [HttpPost("Filters")]
        public async Task<PagedResultDto<ContactOutputDto>> FilterContact([FromBody]object input)
        {
            return await _contactAppService.GetListContact(input.ToString());
        }

        [HttpPost("FilterContactForTarget")]
        public async Task<List<ContactOutputDto>> FilterContactForTarget([FromBody]string input)
        {
            return await _contactAppService.FilterContactForTarget(input);
        }

        [HttpPost("GetListExistContact")]
        public async Task<PagedResultDto<ContactOutputDto>> GetListExistContact([FromBody]string input)
        {
            return await _contactAppService.GetListExistContact(input);
        }
        [HttpGet("{contactId}")]
        public async Task<ContactOutputDto> GetContactDetails(long contactId, bool isShowFull)
        {
            return await _contactAppService.GetContactDetails(contactId, isShowFull);
        }
        [HttpGet("GetListContactEmail/{contactId}")]
        public async Task<PagedResultDto<ContactEmailOutputDto>> GetListContactEmail(FilterBasicInputDto input, long contactId)
        {
            return await _contactAppService.GetListContactEmail(input, contactId); ;
        }
        [HttpGet("GetListStatusContactEmail")]
        public async Task<PagedResultDto<ContactEmailOutputDto>> GetListStatusContactEmail(FilterBasicInputDto input)
        {
            return await _contactAppService.GetListStatusContactEmail(input); ;
        }
        [HttpGet("GetListContactCompany/{contactId}")]
        public async Task<PagedResultDto<ContactCompanyOutputDto>> GetListContactCompany(long contactId, FilterBasicInputDto input)
        {
            return await _contactAppService.GetListContactCompany(input, contactId);
        }
        [HttpGet("GetListContactPhone/{contactId}")]
        public async Task<PagedResultDto<ContactPhoneOutputDto>> GetListContactPhone(FilterBasicInputDto input, long contactId)
        {
            return await _contactAppService.GetListContactPhone(input, contactId); ;
        }
        [HttpGet("GetListContactOrganizationUnit/{contactId}")]
        public async Task<PagedResultDto<ContactOrganizationUnitOutputDto>> GetListContactOrganizationUnit(FilterBasicInputDto input, long contactId)
        {
            return await _contactAppService.GetListContactOrganizationUnit(input, contactId); ;
        }
        [HttpGet("GetListContactUser/{contactId}")]
        public async Task<PagedResultDto<ContactUserOutputDto>> GetListContactUser(FilterBasicInputDto input, long contactId)
        {
            return await _contactAppService.GetListContactUser(input, contactId); ;
        }

        [HttpPost("Create")]
        public async Task<ContactOutputDto> CreateContactAsync([FromBody]ContactInputDto input)
        {
            return await _contactAppService.CreateContactAsync(input);
        }
        [HttpPost("CreateOrUpdateContact")]
        public async Task<ContactOutputDto> CreateOrUpdateContactAsync([FromBody]ContactFullInputDto input)
        {
            return await _contactAppService.CreateOrUpdateContactAsync(input);
        }
        [HttpPut("Update/{contactId}")]
        public async Task<ContactOutputDto> UpdateContactAsync(long contactId, [FromBody] ContactInputDto input)
        {
            return await _contactAppService.UpdateContactAsync(contactId, input);
        }
        [HttpPut("CreateOrUpdateAddress/{contactId}")]
        public async Task<ContactOutputDto> UpdateContactAddressAsync(long contactId, [FromBody] List<ContactAddressInputDto> ContactAddress)
        {
            return await _contactAppService.UpdateContactAddressAsync(contactId, ContactAddress);
        }
        [HttpPut("CreateOrUpdateEmails/{contactId}")]
        public List<ContactEmailOutputDto> UpdateContactEmailsAsync(long contactId, [FromBody] List<ContactEmailInputDto> contactEmail)
        {
            return _contactAppService.CreateOrUpdateContactEmail(contactId, contactEmail);
        }
        [HttpPut("CreateOrUpdatePhones/{contactId}")]
        public List<ContactPhoneOutputDto> UpdateContactPhoneAsync(long contactId, [FromBody] List<ContactPhoneInputDto> contactPhones)
        {
            return _contactAppService.CreateOrUpdateContactPhone(contactId, contactPhones);
        }
        [HttpPut("CreateOrUpdateUsers/{contactId}")]
        public List<ContactUserOutputDto> UpdateContactUsersAsync(long contactId, [FromBody] List<ContactUserInputDto> contactUser)
        {
            return _contactAppService.CreateOrUpdateContactUser(contactId, contactUser);
        }
        [HttpPut("CreateOrUpdateOrganizationUnits/{contactId}")]
        public List<ContactOrganizationUnitOutputDto> UpdateContactOrganizationUnitAsync(long contactId, [FromBody] List<ContactOrganizationUnitInputDto> contactOrganizationUnits)
        {
            return _contactAppService.CreateOrUpdateContactOrganizationUnit(contactId, contactOrganizationUnits);
        }
        [HttpPut("CreateOrUpdateType")]
        public List<ContactTypeMapOutputDto> UpdateContactTypeAsync([FromBody] List<ContactTypeMapInputDto> contactOrganizationUnits)
        {
            return _contactAppService.CreateOrUpdateContactTypeMap(contactOrganizationUnits);
        }
        [HttpPut("CreateOrUpdateContactCompanies/{contactId}")]
        public List<ContactCompanyOutputDto> CreateOrUpdateContactCompanies(long contactId, [FromBody] List<ContactCompanyInputDto> input)
        {
            return _contactAppService.CreateOrUpdateContactCompany(contactId, input);
        }
        [HttpPost("CheckExistContact")]
        public bool CheckExistContact([FromBody]ContactValidateInputDto input)
        {
            return _contactAppService.CheckExistContact(input);
        }
        [HttpPost("RequestShare")]
        public bool RequestShare(long contactId)
        {
            return _contactAppService.RequestShare(contactId);
        }
        [HttpPut("ApproveShare")]
        public bool ApproveShare(long id)
        {
            return _contactAppService.ApproveShare(id);
        }
        [HttpPut("RejectShare")]
        public bool RejectShare(long id)
        {
            return _contactAppService.RejectShare(id);
        }
        [HttpPost("GetListPendingApprove")]

        public async Task<List<ExistContactDto>> GetListPendingApprove([FromBody]string input)
        {
             return await  _contactAppService.GetListPendingApprove(input.ToString());
        }
        [HttpPut("MergeContact")]
        public Task<bool> MergeContact([FromBody]List<long> sourceContactIds, long targetContactId)
        {
            return _contactAppService.MergeContact(sourceContactIds, targetContactId);
        }
    }
}
