using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace CRM.Web.Host.Controllers
{
    public abstract class SBIControllerBase: AbpController
    {
        protected SBIControllerBase()
        {
            LocalizationSourceName = SBIConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
