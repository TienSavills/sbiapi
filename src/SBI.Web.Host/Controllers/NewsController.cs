﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using CRM.Application.Shared.Unit;
using CRM.Services.Unit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using CRM.Application.Shared.News;

namespace CRM.Web.Host.Controllers
{
    [AbpAuthorize]
    [Route("api/[controller]")]
    public class NewsController : SBIControllerBase
    {
        private readonly INewsAppService _newsAppService;
        public NewsController(
            INewsAppService newsAppService
            )
        {
            _newsAppService = newsAppService;
        }
        [HttpGet("{id}")]
        public async Task<NewsOutputDto> GetDetails(long id)
        {
            return await _newsAppService.GetAsync(id);
        }

        [HttpGet("GetNewsForEdit/{id}")]
        public async Task<NewsEditOutputDto> GetEventForEdit(long id)
        {
            return await _newsAppService.GetEventForEdit(id);
        }

        [HttpGet("Filters")]
        public async Task<PagedResultDto<NewsOutputDto>> GetListUnits(PagedNewsResultRequestDto input)
        {
            return await _newsAppService.GetAllAsync(input);
        }

        [HttpPost("CreateAsync")]
        public async Task<NewsOutputDto> CreateAsync([FromBody] NewsCreateDto input)
        {
            return await _newsAppService.CreateAsync(input);
        }

        [HttpPut("UpdateAsync")]
        public async Task<NewsOutputDto> UpdateAsync([FromBody] NewsUpdateDto input)
        {
            return await _newsAppService.UpdateAsync(input);
        }
        [HttpPut("Active")]
        public async Task Active(long id, bool isActive)
        { 
            await _newsAppService.Active(id, isActive);
        }
      
    }
}
