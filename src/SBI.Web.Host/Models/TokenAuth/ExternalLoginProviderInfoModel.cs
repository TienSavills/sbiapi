﻿using Abp.AutoMapper;
using CRM.Web.Host.Authentication.External;

namespace CRM.Web.Host.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
