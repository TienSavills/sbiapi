﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using CRM.Configuration;

namespace CRM.Web.Host.Configuration
{
    public static class HostingEnvironmentExtensions
    {
        public static IConfigurationRoot GetAppConfiguration(this IHostingEnvironment env)
        {
            return AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName, env.IsDevelopment());
        }
    }
}
