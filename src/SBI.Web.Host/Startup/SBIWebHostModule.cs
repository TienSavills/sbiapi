﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using CRM.EntityFrameworkCore;
using Abp.AspNetCore;
using Abp.AspNetCore.SignalR;
using Abp.Zero.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using CRM.Web.Host.Authentication.JwtBearer;
using CRM.Web.Host.Configuration;
using Abp.AppFactory.SendGrid;
using Abp.Configuration.Startup;
using Abp.Threading.BackgroundWorkers;
using Abp.Timing;
using CRM.BackgroundJob;
using CRM.Web.Host.Authentication.External;

namespace CRM.Web.Host.Startup
{
    [DependsOn(
            typeof(SBIApplicationModule),
            typeof(SBIEntityFrameworkModule),
            typeof(AbpAspNetCoreModule),
            typeof(AbpAspNetCoreSignalRModule),
            typeof(SendGridModule)
        )]
    public class SBIWebHostModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public SBIWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                SBIConsts.ConnectionStringName
            );

            // Use database for language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            // Configuration.Modules.AbpAspNetCore()
            //      .CreateControllersForAppServices(
            //          typeof(SBIApplicationModule).GetAssembly()
            //      );

            ConfigureTokenAuth();
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SBIWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!DatabaseCheckHelper.Exist(_appConfiguration["ConnectionStrings:Default"]))
            {
                return;
            }

            if (IocManager.Resolve<IMultiTenancyConfig>().IsEnabled)
            {
                var workManager = IocManager.Resolve<IBackgroundWorkerManager>();
                //workManager.Add(IocManager.Resolve<CRMReminderWorker>()); 
                //workManager.Add(IocManager.Resolve<CRMCheckEmail>());
                workManager.Add(IocManager.Resolve<CRMSendOpportunityWeekly>());
            }


            Clock.Provider = ClockProviders.Utc;
         //   ConfigureExternalAuthProviders();
        }

        private void ConfigureTokenAuth()
        {
            IocManager.Register<TokenAuthConfiguration>();
            var tokenAuthConfig = IocManager.Resolve<TokenAuthConfiguration>();

            tokenAuthConfig.SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appConfiguration["Authentication:JwtBearer:SecurityKey"]));
            tokenAuthConfig.Issuer = _appConfiguration["Authentication:JwtBearer:Issuer"];
            tokenAuthConfig.Audience = _appConfiguration["Authentication:JwtBearer:Audience"];
            tokenAuthConfig.SigningCredentials = new SigningCredentials(tokenAuthConfig.SecurityKey, SecurityAlgorithms.HmacSha256);
            tokenAuthConfig.Expiration = TimeSpan.FromDays(99);
        }
        private void ConfigureExternalAuthProviders()
        {
            var externalAuthConfiguration = IocManager.Resolve<ExternalAuthConfiguration>();
            if (bool.Parse(_appConfiguration["Authentication:Facebook:IsEnabled"]))
            {
                externalAuthConfiguration.Providers.Add(
                    new ExternalLoginProviderInfo(
                        "Facebook",
                        _appConfiguration["Authentication:Facebook:AppId"],
                        _appConfiguration["Authentication:Facebook:AppSecret"],
                        typeof(ExternalLoginProviderInfo)
                    )
                );
            }

            //if (bool.Parse(_appConfiguration["Authentication:Google:IsEnabled"]))
            //{
            //    externalAuthConfiguration.Providers.Add(
            //        new ExternalLoginProviderInfo(
            //            GoogleAuthProviderApi.Name,
            //            _appConfiguration["Authentication:Google:ClientId"],
            //            _appConfiguration["Authentication:Google:ClientSecret"],
            //            typeof(GoogleAuthProviderApi)
            //        )
            //    );
            //}

            ////not implemented yet. Will be implemented with https://github.com/aspnetzero/aspnet-zero-angular/issues/5
            //if (bool.Parse(_appConfiguration["Authentication:Microsoft:IsEnabled"]))
            //{
            //    externalAuthConfiguration.Providers.Add(
            //        new ExternalLoginProviderInfo(
            //            MicrosoftAuthProviderApi.Name,
            //            _appConfiguration["Authentication:Microsoft:ConsumerKey"],
            //            _appConfiguration["Authentication:Microsoft:ConsumerSecret"],
            //            typeof(MicrosoftAuthProviderApi)
            //        )
            //    );
            //}
        }
    }
}
