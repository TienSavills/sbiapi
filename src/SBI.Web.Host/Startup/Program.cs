﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace CRM.Web.Host.Startup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                    options.Limits.MaxRequestBodySize = 4120100000;
                })
                .Build();
        }
    }
}
