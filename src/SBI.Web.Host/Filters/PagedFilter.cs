using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Application.Services.Dto;
using Abp.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CRM.Web.Host.Filters {
    internal static class PagedExtention {
        public static string Map(this IQueryCollection query, string key) {
            if (query.TryGetValue(key, out var values)) {
                if (values.Any()) {
                    return values.FirstOrDefault();
                }
            }
            return null;
        }

        public static int TryGetInt(this IQueryCollection query, string key) {
            try {
                var value = query.Map(key);
                if (string.IsNullOrEmpty(value)) return 0;
                return int.Parse(value);
            } catch (Exception ex) {
                throw new UserFriendlyException(ex.Message, key);
            }
        }

        public static int TryGetPageSize(this HttpRequest request) {
            return request.Query.TryGetInt("pageSize");
        }

        public static int TryGetSkip(this HttpRequest request) {
            return request.Query.TryGetInt("skip");
        }

        public static string TryGetSorting(this HttpRequest request) {
            return request.Query.Map("sorting");
        }

        public static int TryGetPage(this HttpRequest request) {
            return request.Query.TryGetInt("page");
        }

        public static IQueryable<T> PagingFilter<T>(this HttpRequest request, IQueryable<T> list) {
            var pageSize = request.TryGetPageSize();
            var skip = request.TryGetSkip();
            var sorting = request.TryGetSorting();
            var page = request.TryGetPage();
            try {
                if (!string.IsNullOrEmpty(sorting)) list = (IQueryable<T>)list.OrderBy(sorting) as IQueryable<T>;
            } catch (Exception ex) {
                throw new UserFriendlyException(ex.Message);
            }
            if (page > 0 && pageSize > 0) {
                list = list.Page(page, pageSize);
            } else {
                if (skip > 0) list = list.Skip(skip);
                if (pageSize > 0) list = list.Take(pageSize);
            }
            return list;
        }
    }

    public class PagedFilterAttribute : ActionFilterAttribute {
        public override void OnActionExecuted(ActionExecutedContext context) {
            var objectResult = context.Result as ObjectResult;
            if (objectResult != null) {
                if (objectResult.Value is IQueryable) {
                    var list = objectResult.Value as IQueryable<dynamic>;
                    var totalCount = list.Count();
                    list = context.HttpContext.Request.PagingFilter(list);

                    var paged = new PagedResultDto<dynamic>(totalCount, list.ToList());

                    objectResult.Value = paged;
                }
            }
        }

    }
}