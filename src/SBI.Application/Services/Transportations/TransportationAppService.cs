﻿using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using CRM.Application.Shared.Transportation;
using CRM.Entities.ProjectTransportationMaps;
using CRM.Entities.Transportation;
using Microsoft.EntityFrameworkCore;

namespace CRM.Services.Transportations
{
    public class TransportationAppService : SBIAppServiceBase, ITransportationAppService
    {
        private readonly IRepository<Transportation, int> _repositoryTransportation;
        private readonly IRepository<ProjectTransportationMaps, long> _repoProjectTransportationMap;
        public TransportationAppService(IRepository<Transportation, int> repoTransportation, IRepository<ProjectTransportationMaps, long> repoProjectTransportationMap)
        {
            _repositoryTransportation = repoTransportation;
            _repoProjectTransportationMap = repoProjectTransportationMap;
        }

        public void ValidateProjectTransportations(List<int> transportationIds)
        {
            if (_repositoryTransportation.GetAll().Count(x => transportationIds.Contains(x.Id) && x.IsActive) < transportationIds.Count)
                throw new UserFriendlyException(L("ProjectTransportationIdNotFound"));
        }
        public List<ProjectTransportationMaps> CreateOrUpdateListProjectTransportations(long projectId, List<int> transportationIds)
        {

            var currentProjectTransportations = _repoProjectTransportationMap.GetAll().Where(x => x.ProjectId == projectId).ToList();
            transportationIds.ForEach(x =>
            {
                var objUpdate = currentProjectTransportations.FirstOrDefault(obj => obj.TransportationId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoProjectTransportationMap.Update(objUpdate);
                    return;
                }
                var entity = new ProjectTransportationMaps
                {
                    ProjectId = projectId,
                    TransportationId = x,
                    IsActive = true
                };
                _repoProjectTransportationMap.Insert(entity);
            });
            var deleteEntities = currentProjectTransportations.Where(x => transportationIds.All(y => y != x.TransportationId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoProjectTransportationMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repoProjectTransportationMap.GetAll().Where(x => x.ProjectId == projectId && x.IsActive).Include(x => x.Transportation).ToList();
        }
    }
}
