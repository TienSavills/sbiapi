﻿using System.Threading.Tasks;
using CRM.Services.Configuration.Dto;

namespace CRM.Services.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
