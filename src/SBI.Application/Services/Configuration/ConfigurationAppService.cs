﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using CRM.Configuration;
using CRM.Services.Configuration.Dto;

namespace CRM.Services.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : SBIAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
