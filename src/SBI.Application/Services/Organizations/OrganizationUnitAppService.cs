﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Organizations;
using System.Linq.Dynamic.Core;
using Abp.Extensions;
using Microsoft.EntityFrameworkCore;
using CRM.Application.Shared.OrganizationUnit;
using CRM.Application.Shared.OrganizationUnit.Dto;
using CRM.Authorization.Users;
using CRM.Entities.UserOrganizationUnitDetails;
using Abp;

namespace CRM.Services.Organizations
{

    public class OrganizationUnitAppService : SBIAppServiceBase, IOrganizationUnitAppService
    {
        private readonly OrganizationUnitManager _organizationUnitManager;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<UserOrganizationUnitDetails, long> _userOrganizationUnitDetailsRepository;


        public OrganizationUnitAppService(
            OrganizationUnitManager organizationUnitManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<UserOrganizationUnitDetails, long> userOrganizationUnitDetailsRepository
            )
        {
            _organizationUnitManager = organizationUnitManager;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userOrganizationUnitDetailsRepository = userOrganizationUnitDetailsRepository;
        }

        public async Task<ListResultDto<OrganizationUnitDto>> GetOrganizationUnits(int? parentId)
        {
            var query = _organizationUnitRepository.GetAll()
                .Where(x => x.Code.Contains("PL-"))
                .WhereIf(parentId.HasValue,x=>x.ParentId==parentId ||x.Id==parentId)
                .OrderBy(x => x.DisplayName);

            var items = await query.ToListAsync();

            return new ListResultDto<OrganizationUnitDto>(
                items.Select(item =>
                {
                    var dto = ObjectMapper.Map<OrganizationUnitDto>(item);
                    dto.MemberCount = items.Count;
                    return dto;
                }).ToList());
        }
        public async Task<ListResultDto<OrganizationUnitDto>> GetOrganizationUnitsByUser(long userId)
        {
            var query =
                from ou in _organizationUnitRepository.GetAll()
                join uou in _userOrganizationUnitRepository.GetAll().Where(x => x.UserId == userId) on ou.Id equals uou.OrganizationUnitId into g
                orderby ou.DisplayName
                select new { ou, memberCount = g.Count() };

            var items = await query.ToListAsync();

            return new ListResultDto<OrganizationUnitDto>(
                items.Select(item =>
                {
                    var dto = ObjectMapper.Map<OrganizationUnitDto>(item.ou);
                    dto.MemberCount = item.memberCount;
                    return dto;
                }).ToList());
        }
        public async Task<ListResultDto<OrganizationUnitDto>> GetListOffice()
        {
            var query = _organizationUnitRepository.GetAll().Where(x => x.Code.Contains("BD"))
                .OrderBy(x => x.DisplayName);

            var items = await query.ToListAsync();

            return new ListResultDto<OrganizationUnitDto>(
                items.Select(item =>
                {
                    var dto = ObjectMapper.Map<OrganizationUnitDto>(item);
                    dto.DisplayName = dto.DisplayName.Replace("-BD", "").Replace(" BD", "");
                    dto.MemberCount = items.Count;
                    return dto;
                }).ToList());
        }

        public async Task<PagedResultDto<OrganizationUnitUserListDto>> GetOrganizationUnitUsers(GetOrganizationUnitUsersInput input)
        {
            var query = from uou in _userOrganizationUnitRepository.GetAll()
                        join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                        join userDetail in _userOrganizationUnitDetailsRepository.GetAll() on uou.Id equals userDetail.UserOrganizationUnitId into temp
                        from userDetail in temp.DefaultIfEmpty()
                        join user in UserManager.Users on uou.UserId equals user.Id
                        where uou.OrganizationUnitId == input.Id && userDetail.IsDeleted.Value==false
                        select new { uou, user, userDetail };

            var totalCount = await query.CountAsync();
            var items = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<OrganizationUnitUserListDto>(
                totalCount,
                items.Select(item =>
                {
                    var dto = ObjectMapper.Map<OrganizationUnitUserListDto>(item.user);
                    dto.AddedTime = item.uou.CreationTime;
                    dto.IsHead= item.userDetail != null && item.userDetail.IsHead;
                    return dto;
                }).ToList());
        }


        public async Task<OrganizationUnitDto> CreateOrganizationUnit(CreateOrganizationUnitInput input)
        {
            var organizationUnit = new OrganizationUnit(AbpSession.TenantId, input.DisplayName, input.ParentId);

            await _organizationUnitManager.CreateAsync(organizationUnit);
            await CurrentUnitOfWork.SaveChangesAsync();

            return ObjectMapper.Map<OrganizationUnitDto>(organizationUnit);
        }


        public async Task<OrganizationUnitDto> UpdateOrganizationUnit(UpdateOrganizationUnitInput input)
        {
            var organizationUnit = await _organizationUnitRepository.GetAsync(input.Id);

            organizationUnit.DisplayName = input.DisplayName;

            await _organizationUnitManager.UpdateAsync(organizationUnit);

            return await CreateOrganizationUnitDto(organizationUnit);
        }


        public async Task<OrganizationUnitDto> MoveOrganizationUnit(MoveOrganizationUnitInput input)
        {
            await _organizationUnitManager.MoveAsync(input.Id, input.NewParentId);

            return await CreateOrganizationUnitDto(
                await _organizationUnitRepository.GetAsync(input.Id)
                );
        }

        public async Task DeleteOrganizationUnit(EntityDto<long> input)
        {
            await _organizationUnitManager.DeleteAsync(input.Id);
        }


        public async Task RemoveUserFromOrganizationUnit(UserToOrganizationUnitInput input)
        {
            var userOu = await _userOrganizationUnitRepository.GetAll().Where(x => x.OrganizationUnitId == input.OrganizationUnitId && x.UserId == input.UserId).SingleOrDefaultAsync();
            var userHead = await _userOrganizationUnitDetailsRepository.GetAll().Where(x => x.UserOrganizationUnitId == userOu.Id).SingleOrDefaultAsync();
            if (userHead != null)
            {
                userHead.IsDeleted = true;
                await _userOrganizationUnitDetailsRepository.UpdateAsync(userHead);
            }
            await UserManager.RemoveFromOrganizationUnitAsync(input.UserId, input.OrganizationUnitId);

        }

        public async Task AddUsersToOrganizationUnit(UsersHeadToOrganizationUnitInput input)
        {
            await AddToOrganizationUnitAsync(input.UserId, input.OrganizationUnitId, input.IsHead);

        }
        private async Task AddToOrganizationUnitAsync(long userId, long ouId, bool isHead)
        {
            await AddToOrganizationUnitAsync(
                 await GetUserByIdAsync(userId),
                 await _organizationUnitRepository.GetAsync(ouId)
                 , isHead
                 );
        }
        private async Task AddToOrganizationUnitAsync(User user, OrganizationUnit ou, bool isHead)
        {
            var currentOus = await UserManager.GetOrganizationUnitsAsync(user);

            if (currentOus.Any(cou => cou.Id == ou.Id))
            {
                var userOu = await _userOrganizationUnitRepository.GetAll().Where(x => x.OrganizationUnitId == ou.Id && x.UserId == user.Id).SingleOrDefaultAsync();
                var userHead = await _userOrganizationUnitDetailsRepository.GetAll().Where(x => x.UserOrganizationUnitId == userOu.Id).SingleOrDefaultAsync();
                if (userHead.IsHead != isHead)
                {
                    userHead.IsHead = isHead;
                    await _userOrganizationUnitDetailsRepository.InsertOrUpdateAsync(userHead);
                }
            }
            else
            {
                var userOuId = await _userOrganizationUnitRepository.InsertOrUpdateAndGetIdAsync(new UserOrganizationUnit(user.TenantId, user.Id, ou.Id));
                await _userOrganizationUnitDetailsRepository.InsertOrUpdateAsync(new UserOrganizationUnitDetails()
                {
                    UserOrganizationUnitId = userOuId,
                    IsHead = isHead
                });
            }
        }
        /// <summary>
        /// Gets a user by given id.
        /// Throws exception if no user found with given id.
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>User</returns>
        /// <exception cref="AbpException">Throws exception if no user found with given id</exception>
        public virtual async Task<User> GetUserByIdAsync(long userId)
        {
            var user = await UserManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                throw new AbpException("There is no user with id: " + userId);
            }

            return user;
        }
        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindOrganizationUnitUsersInput input)
        {
            var userIdsInOrganizationUnit = _userOrganizationUnitRepository.GetAll()
                .Where(uou => uou.OrganizationUnitId == input.OrganizationUnitId)
                .Select(uou => uou.UserId);

            var query = UserManager.Users
                .Where(u => !userIdsInOrganizationUnit.Contains(u.Id))
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            var userCount = await query.CountAsync();
            var users = await query
                .OrderBy(u => u.Name)
                .ThenBy(u => u.Surname)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<NameValueDto>(
                userCount,
                users.Select(u =>
                    new NameValueDto(
                        u.FullName + " (" + u.EmailAddress + ")",
                        u.Id.ToString()
                    )
                ).ToList()
            );
        }

        private async Task<OrganizationUnitDto> CreateOrganizationUnitDto(OrganizationUnit organizationUnit)
        {
            var dto = ObjectMapper.Map<OrganizationUnitDto>(organizationUnit);
            dto.MemberCount = await _userOrganizationUnitRepository.CountAsync(uou => uou.OrganizationUnitId == organizationUnit.Id);
            return dto;
        }
    }
}