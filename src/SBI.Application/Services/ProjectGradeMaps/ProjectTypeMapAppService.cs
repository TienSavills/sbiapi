﻿using System.Collections.Generic;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using CRM.Application.Shared.ProjectGradeMaps;
using CRM.Entities.ProjectGradeMaps;

namespace CRM.Services.ProjectGradeMaps
{
    public class ProjectGradeMapAppService : SBIAppServiceBase, IProjectGradeMapAppService
    {
        private readonly IRepository<ProjectGradeMap, long> _repoProjectGradeMap;

        public ProjectGradeMapAppService(IRepository<ProjectGradeMap, long> repoProjectGradeMap)
        {
            _repoProjectGradeMap = repoProjectGradeMap;
        }
        public Task<long> CreateProjectGradeMap(ProjectGradeMapInputDto input)
        {
            var data = ObjectMapper.Map<ProjectGradeMap>(input);
            var id = _repoProjectGradeMap.InsertOrUpdateAndGetIdAsync(data);
            return id;
        }

        public List<ProjectGradeMapOutputDto> GetListProjectGradeMap(long projectId)
        {
            var data = _repoProjectGradeMap.GetAll().Where(x => x.ProjectId == projectId)
                .Include(x=>x.Grade);
            return ObjectMapper.Map<List<ProjectGradeMapOutputDto>>(data);
        }
        public Task<long> UpdateProjectGradeMap(long id, ProjectGradeMapInputDto input)
        {
            var entity = ObjectMapper.Map<ProjectGradeMap>(input);
            return _repoProjectGradeMap.InsertOrUpdateAndGetIdAsync(entity);
        }      
    }
}
