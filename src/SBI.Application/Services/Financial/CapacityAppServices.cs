﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using CRM.Application.Shared.Financial;
using CRM.Entities.Financial;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.Financial
{
    public class CapacityAppServices : SBIAppServiceBase, ICapacityAppService
    {
        private readonly IRepository<Capacity, long> _repositoryCapacity;
        private ConfigSetting _options;
        public CapacityAppServices(IRepository<Capacity, long> repositoryCapacity
             , IOptions<ConfigSetting> options
            )
        {
            _repositoryCapacity = repositoryCapacity;
            _options = options.Value;
        }
        public async Task<CapacityOutputDto> CreateCapacityAsync(CapacityInputDto input)
        {
            ValidateCreateOrUpdate(input);
            var entity = ObjectMapper.Map<Capacity>(input);
            entity.IsActive = true;
            entity.Id = await _repositoryCapacity.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<CapacityOutputDto>(entity);
        }

        public Task<List<CapacityOutputDto>> CreateListCapacityAsync(List<CapacityInputDto> input)
        {
            throw new NotImplementedException();
        }

        public async Task<List<CapacityOutputDto>> FilterCapacity(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<CapacityFilterDto>("FilterCapacity",
                   new
                   {
                       @FilterJson = input,
                       @UserId = AbpSession.UserId.Value
                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<CapacityOutputDto>>(output);
                return results;
            }
        }

        public async Task<CapacityOutputDto> GetCapacityDetail(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<CapacityFilterDto>("[FilterCapacity]",
                    new
                    {
                        @Id = id,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var result = ObjectMapper.Map<CapacityOutputDto>(output.SingleOrDefault());
                return result;
            }
        }

        public async Task<CapacityOutputDto> UpdateCapacityAsync(long id, CapacityInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("NotExistCapacity"));
            ValidateCreateOrUpdate(input);
            var entity = _repositoryCapacity.Get(id);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repositoryCapacity.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<CapacityOutputDto>(entity);
        }
        private void ValidateCreateOrUpdate(CapacityInputDto input)
        {
            if (input.Id.HasValue)
            {
                if (_repositoryCapacity.GetAll().Any(x=>x.Year==input.Year 
                && x.Month==input.Month
                && x.OrganizationUnitId==input.OrganizationUnitId
                && x.Id!=input.Id))
                   throw new UserFriendlyException(L("ExistThisDepartment"));
            }
            else
            {
                if (_repositoryCapacity.GetAll().Any(x => x.Year == input.Year
               && x.Month == input.Month
               && x.OrganizationUnitId == input.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistThisDepartment"));
            }
        }
    }
}
