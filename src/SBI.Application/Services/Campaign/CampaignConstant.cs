﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Campaign
{
    public static class CampaignConstant
    {
        public const string Include= "INCLUDE";
        public const string Exclude = "EXCLUDE";
        public const string Dynamic = "dynamic";
    }
}
