﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using CRM.Application.Shared.Campaign;
using CRM.Configuration;
using CRM.Email;
using CRM.EmailHelper;
using CRM.Entities.CampaignTarget;
using CRM.EntitiesCustom;
using CRM.ResponseHelper;
using CRM.SBIMap;
using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.Campaign
{
    public class CampaignAppService : SBIAppServiceBase, ICampaignAppService
    {
        private IRepository<Entities.Campaign.Campaign, long> _repoCampaign;
        private IRepository<Entities.CampaignTarget.CampaignTargetList, long> _repoCampaignTarget;
        private IRepository<Entities.CampaignCategory.CampaignCategory, int> _repoCampaignCategory;
        private readonly IRepository<Entities.EmailHistory.EmailHistory, long> _repoEmailHistory;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly IEmailSenderHelper _emailSenderHelper;
        private ConfigSetting _options;
        private readonly IConfigHelper _configHelper;
        public CampaignAppService(IOptions<ConfigSetting> options
            , IRepository<Entities.Campaign.Campaign, long> repoCampaign
            , IRepository<Entities.CampaignTarget.CampaignTargetList, long> repoCampaignTarget
            , IRepository<Entities.CampaignCategory.CampaignCategory, int> repoCampaignCategory
            , IEmailTemplateProvider emailTemplateProvider
            , IEmailSenderHelper emailSenderHelper
            , IConfigHelper configHelper
            , IRepository<Entities.EmailHistory.EmailHistory, long> repoEmailHistory
            )
        {
            _repoCampaign = repoCampaign;
            _repoCampaignTarget = repoCampaignTarget;
            _repoCampaignCategory = repoCampaignCategory;
            _options = options.Value;
            _emailTemplateProvider = emailTemplateProvider;
            _emailSenderHelper = emailSenderHelper;
            _configHelper = configHelper;
            _repoEmailHistory = repoEmailHistory;
        }
        public async Task<CampaignOutputDto> CreateOrUpdateAsync(CampaignInputDto input)
        {
            var output = await CreateOrUpdateCampaignAsync(input);
            return output;
        }

        public async Task<CampaignOutputDto> GetCampaignDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<CampaignFilterDto>("SpFilterCampaign",
                   new
                   {
                       @Id = id
                       ,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<CampaignOutputDto>(output.SingleOrDefault());
                return results;
            }
        }

        public async Task<List<CampaignContactRequestOutputDto>> GetListCampaignContactRequest(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<CampaignContactRequestOutputDto>("SpFilterCampaignContactRequest",
                   new
                   {
                       @FilterJson = input
                       ,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<CampaignContactRequestOutputDto>>(output);
                return results;
            }
        }

        public async Task<PagedResultDto<EmailHistoryOutputDto>> GetListCampaignEmailHistory(long campaignId, string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<EmailHistoryOutputDto>("SpFilterCampaignContactHistory",
                   new
                   {
                       @FilterJson = input
                       ,
                       @CampaignId = campaignId
                       ,
                       @UserId = AbpSession.UserId.Value
                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<EmailHistoryOutputDto>>(output);
                return new PagedResultDto<EmailHistoryOutputDto>(totalCount, results);
            }
        }

        public async Task<PagedResultDto<CampaignOutputDto>> GetListCampaigns(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<CampaignFilterDto>("SpFilterCampaign",
                   new
                   {
                       @FilterJson = input
                       ,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<CampaignOutputDto>>(output);
                return new PagedResultDto<CampaignOutputDto>(totalCount, results);
            }
        }

        private async Task<CampaignOutputDto> CreateOrUpdateCampaignAsync(CampaignInputDto input)
        {
            var output = new CampaignOutputDto();
            if (input.Id.HasValue)
            {
                var entity = _repoCampaign.Get(input.Id.Value);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoCampaign.UpdateAsync(entityUpdate);
                CreateOrUpdateTargetList(input.Id.Value, Campaign.CampaignConstant.Include, input.TargetIncludeIds);
                CreateOrUpdateTargetList(input.Id.Value, Campaign.CampaignConstant.Exclude, input.TargetExcludeIds);
                output = ObjectMapper.Map<CampaignOutputDto>(entity);
            }
            else
            {
                var entity = ObjectMapper.Map<Entities.Campaign.Campaign>(input);
                entity.IsActive = true;
                entity.Id = await _repoCampaign.InsertAndGetIdAsync(entity);
                CreateOrUpdateTargetList(entity.Id, Campaign.CampaignConstant.Include, input.TargetIncludeIds);
                CreateOrUpdateTargetList(entity.Id, Campaign.CampaignConstant.Exclude, input.TargetExcludeIds);
                output = ObjectMapper.Map<CampaignOutputDto>(entity);
            }
            return output;
        }
        public List<CampaignTargetList> CreateOrUpdateTargetList(long campaignId, string typeName, List<long?> targetListIds)
        {
            if (targetListIds != null)
            {
                var campaignCategoryInclude = _repoCampaignCategory.FirstOrDefault(x => x.Name.ToUpper() == typeName);
                var currentTargetInclude = _repoCampaignTarget.GetAll().Where(x => x.CampaignId == campaignId && x.TypeId == campaignCategoryInclude.Id).ToList();
                targetListIds.ForEach(x =>
                {
                    var objUpdate = currentTargetInclude.FirstOrDefault(obj => obj.TargetId == x);
                    if (objUpdate != null)
                    {
                        if (objUpdate.IsActive) return;
                        objUpdate.IsActive = true;
                        _repoCampaignTarget.Update(objUpdate);
                        return;
                    }
                    var entity = new CampaignTargetList
                    {
                        CampaignId = campaignId,
                        TargetId = x.Value,
                        IsActive = true,
                        TypeId = campaignCategoryInclude.Id
                    };
                    _repoCampaignTarget.Insert(entity);
                });
                var deleteEntities = currentTargetInclude.Where(x => targetListIds.All(y => y != x.TargetId)).ToList();
                deleteEntities.ForEach(x =>
                {
                    x.IsActive = false;
                    _repoCampaignTarget.Update(x);
                });
                CurrentUnitOfWork.SaveChanges();
                return _repoCampaignTarget.GetAll().Where(x => x.CampaignId == campaignId && x.IsActive).ToList();
            }
            else
                return new List<CampaignTargetList>();
        }
        //Send email campaign
        public async Task SendEmail(long campaignId, List<CampaignContactRequestInputDto> input)
        {
            var campaign = await _repoCampaign.FirstOrDefaultAsync(x => x.Id == campaignId);
            if (campaign != null)
            {
                campaign.SendCount = campaign.SendCount + 1;
                var templeteId = campaign.Template;
                var host = _configHelper.GetServerRoot();
                input.ForEach(x =>
                {
                    var subcription = string.Format(host + "/subscription?email={0}", x.PrimaryEmail);
                    var confirm = string.Format(host + "/confirm?email={0}", x.PrimaryEmail);
                    var bindingContend = BindingTemplete(campaign.Name, campaign.ContentEmail, campaign.Name
                        , subcription, x.ContactName, x.Gender, confirm);
                    if (!_repoEmailHistory.GetAll().Where
                     (y => y.ContactId == x.ContactId && x.CampaignId == campaignId && y.IsSuccess == true
                     && y.SentTo.ToUpper() == x.PrimaryEmail).Any())
                    {
                        var jobId = Hangfire.BackgroundJob.Enqueue(() => _emailSenderHelper.SendAsync(campaignId, x.ContactId, templeteId, "sbi@savills.com.vn", x.PrimaryEmail, campaign.Name, bindingContend, "Campaign"));
                    }
                });
                await _repoCampaign.UpdateAsync(campaign);
            }
        }

        public async Task<ResultApiModel> CheckEmail(string email)
        {
            return await _emailSenderHelper.CheckEmailAsync(email);
        }
        // binding templete
        private string BindingTemplete(string subject, string content, string campaignName, string compaignSubcription, string contactName
            , string Gender, string link)
        {
            var emailContent = new StringBuilder(_emailTemplateProvider.GetEmailTemplate($"CRM.EmailTemplate.Campaign.html"));
            var dataBindings = new Dictionary<string, string>() {
                        {Email.Constants.Campaign.Content, content},
                      {Email.Constants.Campaign.CampaignName, campaignName},
                      {Email.Constants.Campaign.CampaignSubcription,compaignSubcription},
                      {Email.Constants.Campaign.ContactName,contactName},
                      {Email.Constants.Campaign.Gender,Gender},
                      {Email.Constants.Campaign.Link, link},
            };

            return _emailTemplateProvider.BindDataToTemplate(emailContent.ToString(), dataBindings);
        }
        //get templete from sendgrid
        public Task<TempleteSendgridOutputDto> GetTempleteSendgrid(string templeteType)
        {
            var results = _emailSenderHelper.GetTempleteSendgrid(string.IsNullOrEmpty(templeteType) ? CampaignConstant.Dynamic : templeteType);
            return results;
        }
    }
}
