﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Abp;
using Abp.AppFactory.Interfaces;
using Abp.AppFactory.SendGrid.Email;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.Comment;
using CRM.Application.Shared.CommentSendEmail;
using CRM.Configuration;
using CRM.EmailHelper;
using CRM.Entities.Comment;
using CRM.Entities.CommentSendMail;
using CRM.Entities.OpportuityCategory;
using CRM.Entities.OpportunityLeadUser;
using CRM.Entities.OpportunityUser;
using CRM.Notifications.Interfaces;
using CRM.Services.Activiy;
using Microsoft.EntityFrameworkCore;

namespace CRM.Services.Commnent
{
    [AbpAuthorize]
    public class CommentAppService : SBIAppServiceBase, ICommentAppService
    {
        private readonly IRepository<Comment, long> _repoCommnent;
        private readonly IRepository<CommentSendEmail, long> _repoCommnentSendEmail;
        private readonly IRepository<OpportunityUser, long> _repoOpportunityUser;
        private readonly IRepository<OpportunityLead, long> _repoOpportunityLead;
        private readonly IRepository<Entities.Opportunity.Opportunity, long> _repoOpportunity;
        private readonly ISendGrid _sendGrid;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly IRepository<Entities.Company.Company, long> _repositoryCompany;
        private readonly IRepository<Entities.Contacts.Contact, long> _repositoryContact;
        private readonly IRepository<Entities.Requests.Request, long> _repositoryRequest;
        private readonly IRepository<Entities.Deal.Deal, long> _repositoryDeal;
        private readonly IRepository<Entities.Listings.Listings, long> _repositoryListing;
        private readonly IRepository<Entities.Unit.Unit, long> _repositoryUnit;
        private readonly IAppNotifier _appNotifier;
        private readonly IConfigHelper _configHelper;

        public CommentAppService(
            IRepository<Comment, long> repoCommnent
            , IRepository<OpportunityUser, long> repoOpportunityUser
            , IRepository<OpportunityLead, long> repoOpportunityLead
            , IRepository<Entities.Opportunity.Opportunity, long> repoOpportunity
            , ISendGrid sendGrid
            , IEmailTemplateProvider emailTemplateProvider
            , IRepository<CommentSendEmail, long> repoCommnentSendEmail
            , IRepository<Entities.Company.Company, long> repositoryCompany
            , IRepository<Entities.Contacts.Contact, long> repositoryContact
            , IRepository<Entities.Requests.Request, long> repositoryRequest
            , IRepository<Entities.Deal.Deal, long> repositoryDeal
            , IRepository<Entities.Listings.Listings, long> repositoryListing
            , IRepository<Entities.Unit.Unit, long> repositoryUnit
             , IAppNotifier appNotifier
            , IConfigHelper configHelper
            )
        {
            _repoCommnent = repoCommnent;
            _repoCommnentSendEmail = repoCommnentSendEmail;
            _repoOpportunityLead = repoOpportunityLead;
            _repoOpportunityUser = repoOpportunityUser;
            _repoOpportunity = repoOpportunity;
            _sendGrid = sendGrid;
            _repositoryCompany = repositoryCompany;
            _repositoryContact = repositoryContact;
            _repositoryRequest = repositoryRequest;
            _repositoryDeal = repositoryDeal;
            _emailTemplateProvider = emailTemplateProvider;
            _repositoryListing = repositoryListing;
            _repositoryUnit = repositoryUnit;
            _configHelper = configHelper;
            _appNotifier = appNotifier;
        }
        public async Task<CommentOutputDto> CreateCommentAsync(int moduleId, long referenceId, CommentInputDto input, bool isSend = false)
        {
            var entity = ObjectMapper.Map<Comment>(input);
            entity.IsActive = true;
            entity.ModuleId = moduleId;
            entity.ReferenceId = referenceId;
            entity.IsSend = isSend;
            entity.Id = await _repoCommnent.InsertAndGetIdAsync(entity);
            long commentId = entity.Id;
            // cap nhat lai lastmodify khi comment cho thang cha
            if (input.ParentId.HasValue)
            {
                var parentComment = _repoCommnent.Get(input.ParentId.Value);
                parentComment.LastModificationTime = DateTime.UtcNow;
                await _repoCommnent.UpdateAsync(parentComment);
                commentId = parentComment.Id;
            }
            if (input.UserIds != null && input.UserIds.Any())
            {
                foreach (var userId in input.UserIds)
                {
                    var entityComentSend = new Entities.CommentSendMail.CommentSendEmail
                    {
                        CommentId = entity.Id,
                        UserId = userId,
                        IsActive = true
                    };
                    await _repoCommnentSendEmail.InsertAndGetIdAsync(entityComentSend);
                }
            }

            if (input.UserIds != null && isSend == true && input.UserIds.Any())
            {
                var host = _configHelper.GetHostRequest();
                string name = string.Empty; string link = string.Empty;
                if (ActivityConstant.Company == entity.ModuleId)
                {
                    name = "Company" + "/" + (await _repositoryCompany.GetAsync(entity.ReferenceId)).BusinessName;
                    link = string.Format(host + AppConsts.LinkCompany, new object[] { referenceId, commentId });
                }
                if (ActivityConstant.Contact == entity.ModuleId)
                {
                    name = "Contact" + "/" + (await _repositoryContact.GetAsync(entity.ReferenceId)).ContactName;
                    link = string.Format(host + AppConsts.LinkContact, new object[] { referenceId, commentId });
                }
                if (ActivityConstant.Listing == entity.ModuleId)
                {
                    var listing = await _repositoryListing.GetAsync(entity.ReferenceId);
                    var unit = await _repositoryUnit.GetAsync(listing.UnitId);
                    name = "Listing" + "/" + unit.UnitName;
                    link = string.Format(host + AppConsts.LinkListing, new object[] { referenceId, commentId });
                }
                if (ActivityConstant.Opportunity == entity.ModuleId)
                {
                    name = "Opportunity" + "/" + (await _repoOpportunity.GetAsync(entity.ReferenceId)).OpportunityName;
                    link = string.Format(host + AppConsts.LinkOpportunity, new object[] { referenceId, commentId });
                }
                if (ActivityConstant.Request == entity.ModuleId)
                {
                    name = "Request" + "/" + (await _repositoryRequest.GetAsync(entity.ReferenceId)).Id.ToString();
                    link = string.Format(host + AppConsts.LinkRequest, new object[] { referenceId, commentId });
                }
                if (ActivityConstant.Deal == entity.ModuleId)
                {
                    name = "Deal" + "/" + (await _repositoryDeal.GetAsync(entity.ReferenceId)).DealName.ToString();
                    link = string.Format(host + AppConsts.LinkDeal, new object[] { referenceId, commentId });
                }
                foreach (var userId in input.UserIds)
                {
                    var user = await UserManager.GetUserByIdAsync(userId);
                    var email = user.EmailAddress.ToString();
                    await SendMailComment(user.UserName, email, input.Description, name, link);
                    List<UserIdentifier> userIdentifiers = new List<UserIdentifier> {user.ToUserIdentifier()};
                    await _appNotifier.SendMessageAsync(userIdentifiers, input.Description);
                }
            }
            return ObjectMapper.Map<CommentOutputDto>(entity);
        }
        public async Task SendMailComment(string displayName, string emailAddress, string contend, string name, string link)
        {
            var subject = "New Comment";
            var emailContent = new StringBuilder(_emailTemplateProvider.GetEmailTemplate($"CRM.EmailTemplate.CommentSendEmail.html"));
            var dataBindings = new Dictionary<string, string>() {
                  {EmailConsts.Commment.DisplayName, displayName},
                  {EmailConsts.Commment.Name,name},
                  {EmailConsts.Commment.Contend,contend},
                  {EmailConsts.Commment.Link,link},
        };

            var bodyHtmlContent = _emailTemplateProvider.BindDataToTemplate(emailContent.ToString(), dataBindings);
            await _sendGrid.SendAsync(new SendGridEmail()
            {
                SenderName = "CRM Notify",
                SenderEmailAddress = "crm@sadec.co",
                SubjectContent = subject,
                BodyHtmlContent = bodyHtmlContent,
                RecepientEmailAddress = emailAddress
            });
        }

        public async Task<PagedResultDto<CommentOutputDto>> GetCommentByReference(int moduleId, long referenceId, FilterBasicInputDto input)
        {
            var query = _repoCommnent.GetAll()
              .Include(x => x.Module)
              .Include(x => x.CreatorUser)
              .Include(x => x.LastModifierUser)
              .Where(x => x.ModuleId == moduleId && x.ReferenceId == referenceId && x.ParentId == null)
              .WhereIf(!string.IsNullOrEmpty(input.Keyword), x => x.Description.ToUpper().Contains(input.Keyword.ToUpper()))
               .OrderByDescending(x => x.LastModificationTime ?? x.CreationTime);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .ToListAsync();
            var output = ObjectMapper.Map<List<CommentOutputDto>>(results);
            var ids = output.Select(x => x.Id);
            var commentSends = await _repoCommnentSendEmail.GetAll().Where(x => ids.Contains(x.CommentId))
                .Include(x => x.User)
                .ToListAsync();
            var commentSendOutput = ObjectMapper.Map<List<CommentSendEmailOutputDto>>(commentSends);
            output.ForEach(x =>
            {
                x.ChildCount = _repoCommnent.Count(y => y.ParentId == x.Id);
                x.CommentSendEmail = commentSendOutput.Where(y => y.CommentId == x.Id).ToList();
            }
            );
            return new PagedResultDto<CommentOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<List<CommentOutputDto>> GetCommentDetails(long commentId)
        {
            var entity = await _repoCommnent.GetAll().Where(x => x.ParentId == commentId)
              .Include(x => x.Module)
              .Include(x => x.CreatorUser)
              .Include(x => x.LastModifierUser)
              .OrderByDescending(x => x.LastModificationTime ?? x.CreationTime)
              .ToListAsync();
            var output = ObjectMapper.Map<List<CommentOutputDto>>(entity);
            var ids = output.Select(x => x.Id);
            var commentSends = await _repoCommnentSendEmail.GetAll().Where(x => ids.Contains(x.CommentId))
                .Include(x => x.User).ToListAsync();
            var commentSendOutput = ObjectMapper.Map<List<CommentSendEmailOutputDto>>(commentSends);
            output.ForEach(x =>
            {
                x.ChildCount = _repoCommnent.Count(y => y.ParentId == x.Id);
                x.CommentSendEmail = commentSendOutput.Where(y => y.CommentId == x.Id).ToList();
            }
            );
            return output;
        }

        public async Task<CommentOutputDto> UpdateCommentAsync(long commentId, CommentInputDto input)
        {
            if (commentId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("CommentIdNotFound"));
            var entity = await _repoCommnent.GetAsync(commentId);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoCommnent.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<CommentOutputDto>(entity);
        }
    }
}
