﻿using Abp.Application.Services;
using CRM.Application.Shared.ProjectClaims;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using CRM.Entities.ProjectClaims;
using CRM.SBIMap;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using System.Linq;
using CRM.EntitiesCustom;
using Abp.UI;
using CRM.Services.Currencys;
using CRM.Application.Shared.ProjectClaimRoomTypeMaps;
using CRM.Entities.ProjectClaimRoomTypeMaps;
using CRM.Services.ProjectRoomTypeMaps;
using Abp.Domain.Uow;

namespace CRM.Services.ProjectClaims
{
    public class ProjectClaimAppService : SBIAppServiceBase, IProjectClaimAppService
    {
        private readonly IRepository<ProjectClaim, long> _repoProjectClaim;
        private readonly IRepository<ProjectClaimRoomTypeMap, long> _repoProjectClaimRoomType;
        private readonly ProjectClaimRoomTypeAppService _projectClaimRoomTypeAppService;
        private ConfigSetting _options;
        public ProjectClaimAppService(
            IOptions<ConfigSetting> options
            , IRepository<ProjectClaim, long> repoProjectClaim
            , IRepository<ProjectClaimRoomTypeMap, long> repoProjectClaimRoomType
            ,ProjectClaimRoomTypeAppService projectClaimRoomTypeAppService)
        {
            _repoProjectClaim = repoProjectClaim;
            _repoProjectClaimRoomType = repoProjectClaimRoomType;
            _options = options.Value;
            _projectClaimRoomTypeAppService = projectClaimRoomTypeAppService;
        }
        [UnitOfWork]
        public async Task<ProjectClaimOutputDto> CreateProjectClaimAsync(ProjectClaimInputDto input)
        {
            ValidateCreateProjectClaim(input);
            var dataDate = GetDataDate(input.DataPeriod, input.Period, input.Year);
            var listProjectClaim = _repoProjectClaim.GetAll()
                .Where(x => x.ProjectId == input.ProjectId && x.ResearchTypeId==input.ResearchTypeId).ToList();
            if (listProjectClaim != null)
            {
                foreach (var objUpdate in listProjectClaim)
                {
                     objUpdate.ToDate = DateTime.UtcNow;
                    objUpdate.IsActive = false;
                    _repoProjectClaim.Update(objUpdate);
                }
            }
            var entity = ObjectMapper.Map<ProjectClaim>(input);
            entity.IsActive = true;
            entity.FromDate = DateTime.UtcNow;
            entity.ToDate = Convert.ToDateTime(CurrencyConstant.DefaultDate);
            entity.DataDate = dataDate;
            entity.Id = await _repoProjectClaim.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<ProjectClaimOutputDto>(entity);
        }

        public async Task<PagedResultDto<ProjectClaimOutputDto>> FilterProjectClaimAsync(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ProjectClaimFilterDto>("SpFilterProjectClaim",
                    new
                    {
                        @FilterJson= input
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ProjectClaimOutputDto>>(output);
                return new PagedResultDto<ProjectClaimOutputDto>(totalCount, results);
            }
        }

        public async Task<ProjectClaimOutputDto> GetProjectClaimDetail(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ProjectClaimFilterDto>("SpFilterProjectClaim",
                    new
                    {
                        @Id = id,
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ProjectClaimOutputDto>>(output).SingleOrDefault();
                return results;
            }
        }

        public async Task<ProjectClaimOutputDto> UpdateProjectClaimDetailAsync(long projectClaimId,
            List<ProjectClaimRoomTypeMapInputDto> projectClaimRoomTypeMap)
        {
            var entity = await _repoProjectClaim.GetAsync(projectClaimId);
            var output = ObjectMapper.Map<ProjectClaimOutputDto>(entity);


            #region Insert or update project Claim

            if (projectClaimRoomTypeMap != null)
            {
                _projectClaimRoomTypeAppService.ValidateProjectClaimRoomType(projectClaimRoomTypeMap.Select(x => x.RoomTypeId).ToList());
                output.ProjectClaimRoomType = ObjectMapper.Map<List<ProjectClaimRoomTypeMapOutputDto>>(_projectClaimRoomTypeAppService.CreateOrUpdateListProjectClaimRoomType(entity.Id, projectClaimRoomTypeMap));
            }
            #endregion
            return output;
        }

        public async Task<ProjectClaimOutputDto> UpdateProjectClaimAsync(long id, ProjectClaimInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("ProjectClaimIdNotFound"));
            ValidateUpdateProjectClaim(input);
            var entity = _repoProjectClaim.Get(id);
            var entityUpdate = ObjectMapper.Map(input, entity);
            entityUpdate.DataDate = GetDataDate(input.DataPeriod, input.Period, input.Year);
            await _repoProjectClaim.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<ProjectClaimOutputDto>(entity);
        }

        private DateTime GetDataDate(int dataPeriod, int period, int year)
        {
            DateTime dataDate=new DateTime();
            ValidatePeriod(dataPeriod, period);
            if (dataPeriod == ProjectClaimConstant.DATAMONTH)
            {
                dataDate = GetLastDayOfMonth(period, year);
            }
            if (dataPeriod == ProjectClaimConstant.DATAQUARTER)
            {
                if (period == ProjectClaimConstant.Q1)
                {
                    dataDate = GetLastDayOfMonth( ProjectClaimConstant.MAR, year);
                }
                if (period == ProjectClaimConstant.Q2)
                {
                    dataDate =GetLastDayOfMonth( ProjectClaimConstant.JUN, year);
                }
                if (period == ProjectClaimConstant.Q3)
                {
                    dataDate = GetLastDayOfMonth(ProjectClaimConstant.SEP, year);
                }
                if (period == ProjectClaimConstant.Q4)
                {
                    dataDate = GetLastDayOfMonth(ProjectClaimConstant.DEC, year);
                }

            }
            if (dataPeriod == ProjectClaimConstant.DATAHALFYEAR)
            {
                if (period == ProjectClaimConstant.FRIST6)
                {
                    dataDate = GetLastDayOfMonth(ProjectClaimConstant.JUN, year);
                }
                if (period == ProjectClaimConstant.LAST6)
                {
                    dataDate = GetLastDayOfMonth(ProjectClaimConstant.DEC, year);
                }
            }
            if (dataPeriod == ProjectClaimConstant.YEAR)
            {
                if (period == ProjectClaimConstant.DATAYEAR)
                {
                    dataDate = GetLastDayOfMonth(ProjectClaimConstant.DEC, year);
                }
            }
            return dataDate;
        }
        public DateTime GetLastDayOfMonth(int month,int year)
        {
            DateTime dtResult = new DateTime(year, month, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-(dtResult.Day));
            return dtResult;
        }
        private void ValidateCreateProjectClaim(ProjectClaimInputDto input)
        {
            if (input.ResearchTypeId < AppConsts.MinValuePrimaryKey|| input.ResearchTypeId > AppConsts.MaxValueResearchType)
                throw new UserFriendlyException(L("ResearchTypeIdNotFound"));
            //if (_repoProjectClaim.Count(x=>x.ProjectId==input.ProjectId&&x.Year==input.Year&&x.DataPeriod==input.DataPeriod&&x.Period==input.Period)>0)
            //    throw new UserFriendlyException(L("ProjectClaimIdExist"));
            if (input.DataPeriod== ProjectClaimConstant.DATAMONTH)
            {
                if (new int[] { ProjectClaimConstant.JAN, ProjectClaimConstant.FEB, ProjectClaimConstant.MAR }.Contains( input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period==input.Period
                   ||(( x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period==ProjectClaimConstant.Q1)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR)))>0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.APR, ProjectClaimConstant.MAY, ProjectClaimConstant.JUN }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                   || ((x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period == ProjectClaimConstant.Q2)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.JUL, ProjectClaimConstant.AUG, ProjectClaimConstant.SEP }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                   || ((x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period == ProjectClaimConstant.Q3)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.OCT, ProjectClaimConstant.NOV, ProjectClaimConstant.NOV }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                   || ((x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period == ProjectClaimConstant.Q4)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }

            }
            if (input.DataPeriod == ProjectClaimConstant.DATAQUARTER)
            {
                if (new int[] { ProjectClaimConstant.Q1 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.JAN, ProjectClaimConstant.FEB, ProjectClaimConstant.MAR }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
               
                if (new int[] { ProjectClaimConstant.Q2 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.APR, ProjectClaimConstant.MAY, ProjectClaimConstant.JUN }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.Q3 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.JUL, ProjectClaimConstant.AUG, ProjectClaimConstant.SEP }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.Q4 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.OCT, ProjectClaimConstant.NOV, ProjectClaimConstant.DEC }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                        && (x.DataPeriod == ProjectClaimConstant.YEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
            }
            if (input.DataPeriod == ProjectClaimConstant.DATAHALFYEAR)
            {
                if (new int[] { ProjectClaimConstant.FRIST6 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH  && 
                        new int[] { ProjectClaimConstant.JAN, ProjectClaimConstant.FEB, ProjectClaimConstant.MAR
                        ,ProjectClaimConstant.APR, ProjectClaimConstant.MAY, ProjectClaimConstant.JUN
                        }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAQUARTER &&
                        new int[] { ProjectClaimConstant.Q1, ProjectClaimConstant.Q2}.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.YEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.LAST6 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH &&
                        new int[] { ProjectClaimConstant.JUL, ProjectClaimConstant.AUG, ProjectClaimConstant.SEP
                        ,ProjectClaimConstant.OCT, ProjectClaimConstant.NOV, ProjectClaimConstant.DEC
                        }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAQUARTER &&
                        new int[] { ProjectClaimConstant.Q3, ProjectClaimConstant.Q4 }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.YEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
            }
            if (input.DataPeriod == ProjectClaimConstant.DATAYEAR)
            {
                if (_repoProjectClaim.Count(x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Year == input.Year && x.DataPeriod != ProjectClaimConstant.DATAYEAR) > 0)
                {
                    throw new UserFriendlyException(L("ProjectClaimIdExist"));
                }
            }
        }
        private void ValidateUpdateProjectClaim(ProjectClaimInputDto input)
        {
            if (input.ResearchTypeId < AppConsts.MinValuePrimaryKey || input.ResearchTypeId > AppConsts.MaxValueResearchType)
                throw new UserFriendlyException(L("ResearchTypeIdNotFound"));
            //if (_repoProjectClaim.Count(x => x.Id!=input.Id
            //&& x.ProjectId==input.ProjectId && x.Year == input.Year
            //&& x.DataPeriod == input.DataPeriod && x.Period == input.Period) > 0)
            //    throw new UserFriendlyException(L("ProjectClaimIdExist"));

            if (input.DataPeriod == ProjectClaimConstant.DATAMONTH)
            {
                if (_repoProjectClaim.Count(x =>x.ResearchTypeId==input.ResearchTypeId&& x.Id != input.Id&& x.ProjectId == input.ProjectId && x.Year == input.Year && x.DataPeriod != ProjectClaimConstant.DATAMONTH) > 0)
                {
                    throw new UserFriendlyException(L("ProjectClaimIdExist"));
                }
                if (new int[] { ProjectClaimConstant.JAN, ProjectClaimConstant.FEB, ProjectClaimConstant.MAR }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Id != input.Id
                   && x.Year == input.Year && x.Period==input.Period
                   || ((x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period == ProjectClaimConstant.Q1)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.APR, ProjectClaimConstant.MAY, ProjectClaimConstant.JUN }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Id != input.Id 
                   && x.Year == input.Year && x.Period == input.Period
                   || ((x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period == ProjectClaimConstant.Q2)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.JUL, ProjectClaimConstant.AUG, ProjectClaimConstant.SEP }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Id != input.Id
                   && x.Year == input.Year && x.Period == input.Period
                   || ((x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period == ProjectClaimConstant.Q3)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.OCT, ProjectClaimConstant.NOV, ProjectClaimConstant.NOV }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                   (x => x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId && x.Id != input.Id 
                   && x.Year == input.Year && x.Period == input.Period
                   || ((x.DataPeriod == ProjectClaimConstant.DATAQUARTER && x.Period == ProjectClaimConstant.Q4)
                   && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                   && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
            }
            if (input.DataPeriod == ProjectClaimConstant.DATAQUARTER)
            {
                if (new int[] { ProjectClaimConstant.Q1 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x =>x.Id!=input.Id && x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId 
                        && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.JAN, ProjectClaimConstant.FEB, ProjectClaimConstant.MAR }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }

                if (new int[] { ProjectClaimConstant.Q2 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.Id != input.Id && x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId
                        && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.APR, ProjectClaimConstant.MAY, ProjectClaimConstant.JUN }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.FRIST6)
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.Q3 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.Id != input.Id && x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId
                        && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.JUL, ProjectClaimConstant.AUG, ProjectClaimConstant.SEP }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.Q4 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.Id != input.Id && x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId
                        && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH && new int[] { ProjectClaimConstant.OCT, ProjectClaimConstant.NOV, ProjectClaimConstant.DEC }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAHALFYEAR && x.Period == ProjectClaimConstant.LAST6)
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
            }
            if (input.DataPeriod == ProjectClaimConstant.DATAHALFYEAR)
            {
                if (new int[] { ProjectClaimConstant.FRIST6 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.Id != input.Id && x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId
                        && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH &&
                        new int[] { ProjectClaimConstant.JAN, ProjectClaimConstant.FEB, ProjectClaimConstant.MAR
                        ,ProjectClaimConstant.APR, ProjectClaimConstant.MAY, ProjectClaimConstant.JUN
                        }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAQUARTER &&
                        new int[] { ProjectClaimConstant.Q1, ProjectClaimConstant.Q2 }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
                if (new int[] { ProjectClaimConstant.LAST6 }.Contains(input.Period))
                {
                    if (_repoProjectClaim.Count
                        (x => x.Id != input.Id && x.ResearchTypeId == input.ResearchTypeId && x.ProjectId == input.ProjectId
                        && x.Year == input.Year && x.Period == input.Period
                        || ((x.DataPeriod == ProjectClaimConstant.DATAMONTH &&
                        new int[] { ProjectClaimConstant.JUL, ProjectClaimConstant.AUG, ProjectClaimConstant.SEP
                        ,ProjectClaimConstant.OCT, ProjectClaimConstant.NOV, ProjectClaimConstant.DEC
                        }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAQUARTER &&
                        new int[] { ProjectClaimConstant.Q3, ProjectClaimConstant.Q4 }.Contains(x.Period.Value))
                        && (x.DataPeriod == ProjectClaimConstant.DATAYEAR))) > 0)
                    {
                        throw new UserFriendlyException(L("ProjectClaimIdExist"));
                    }
                }
               
            }
            if (input.DataPeriod == ProjectClaimConstant.DATAYEAR)
            {
                if (_repoProjectClaim.Count(x => x.ResearchTypeId == input.ResearchTypeId && x.Id != input.Id && x.ProjectId == input.ProjectId && x.Year == input.Year && x.DataPeriod != ProjectClaimConstant.DATAYEAR) > 0)
                {
                    throw new UserFriendlyException(L("ProjectClaimIdExist"));
                }
            }
        }
        private void ValidatePeriod(int dataPeriod, int period)
        {
            if (dataPeriod < ProjectClaimConstant.DATAMONTH && dataPeriod > ProjectClaimConstant.DATAYEAR)
            {
                throw new UserFriendlyException(L("DataPeriodNotFound"));
            }
            if (dataPeriod == ProjectClaimConstant.DATAMONTH)
            {
                if (period < ProjectClaimConstant.JAN || period > ProjectClaimConstant.DEC)
                {
                    throw new UserFriendlyException(L("PeriodNotFound"));
                }
            }
            if (dataPeriod == ProjectClaimConstant.DATAQUARTER)
            {
                if (period < ProjectClaimConstant.Q1 || period > ProjectClaimConstant.Q4)
                {
                    throw new UserFriendlyException(L("PeriodNotFound"));
                }
            }
            if (dataPeriod == ProjectClaimConstant.DATAHALFYEAR)
            {
                if (period < ProjectClaimConstant.FRIST6 || period > ProjectClaimConstant.LAST6)
                {
                    throw new UserFriendlyException(L("PeriodNotFound"));
                }
            }
            if (dataPeriod == ProjectClaimConstant.DATAYEAR)
            {
                if (period != ProjectClaimConstant.YEAR)
                {
                    throw new UserFriendlyException(L("PeriodNotFound"));
                }
            }
        }
    }
}
