﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.ProjectClaims
{
    public static class ProjectClaimConstant
    {
        public const int DATAMONTH = 1;
        public const int DATAQUARTER = 2;
        public const int DATAHALFYEAR = 3;
        public const int DATAYEAR = 4;
        public const int DefaultDate = 30;

        public const int JAN = 1;
        public const int FEB = 2;
        public const int MAR = 3;
        public const int APR = 4;
        public const int MAY = 5;
        public const int JUN = 6;
        public const int JUL = 7;
        public const int AUG = 8;
        public const int SEP = 9;
        public const int OCT = 10;
        public const int NOV = 11;
        public const int DEC = 12;

        public const int Q1 = 1;
        public const int Q2 = 2;
        public const int Q3 = 3;
        public const int Q4 = 4;

        public const int FRIST6 = 1;
        public const int LAST6 = 2;

        public const int YEAR = 1;
    }
}
