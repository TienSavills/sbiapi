﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.ProjectClaims
{
    public class MonthQuater
    {
        public List<int> MonthQuaterQ1 { get { return new List<int> { 1, 2, 3 }; } set { } }
        public List<int> MonthQuaterQ2 { get { return new List<int> { 4, 5, 6 }; } set { } }
        public List<int> MonthQuaterQ3 { get { return new List<int> { 7, 8, 9 }; } set { } }
        public List<int> MonthQuaterQ4 { get { return new List<int> { 10, 11,12 }; } set { } }
        public List<int> MonthFristYear { get { return new List<int> { 1,2,3,4,5,6 }; } set { } }
        public List<int> MonthLastYear { get { return new List<int> { 7,8,9,10,11,12 }; } set { } }
        public List<int> MonthYear { get { return new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }; } set { } }
        public List<int> QuaterFristYear { get { return new List<int> { 1,2 }; } set { } }
        public List<int> QuaterLastYear { get { return new List<int> { 3,4 }; } set { } }
        public List<int> HalfFristYear { get { return new List<int> { 1}; } set { } }
        public List<int> HalLastYear { get { return new List<int> { 2 }; } set { } }
        public List<int> Years { get { return new List<int> { 1 }; } set { } }

    }
}
