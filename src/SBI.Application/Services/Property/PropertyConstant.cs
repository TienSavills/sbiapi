﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Property
{
    public static class PropertyConstant
    {
        public const string PropertyStatus = "PROPERTYSTATUS";
        public const string RequestStatus = "REQUESTSTATUS";
        public const string RequestType = "REQUESTTYPE";
    }
}
