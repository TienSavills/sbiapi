﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using CRM.Application.Shared.PropertyAddresss;
using CRM.Application.Shared.PropertyFacilityMaps;
using CRM.Entities.Countries;
using CRM.Entities.Districs;
using CRM.Entities.PropertyAddresss;
using CRM.Entities.PropertyFacilityMaps;
using CRM.Entities.Provinces;
using CRM.SBIMap;
using CRM.EntitiesCustom;
using CRM.Services.Facilities;
using CRM.Services.ProjectTypes;
using CRM.Application.Shared.PropertyTenant;
using CRM.Services.Company;
using CRM.Entities.Projects;
using CRM.Application.Shared.Property;
using CRM.Application.Shared.Property.Dto;
using CRM.Entities.PropertyTypeMaps;
using CRM.Application.Shared.PropertyTypeMap;
using CRM.Application.Shared.PropertyLandlord;
//using CRM.Entities.PropertyTenants;
//using CRM.Entities.PropertyLandlords;
using CRM.Application.Shared.PropertyGradeMaps;
using CRM.Services.Grades;
using CRM.Entities.PropertyGradeMaps;
using CRM.Entities.OtherCategory;
using CRM.Services.Property;
using CRM.Entities.Currencys;
using CRM.Application.Shared.PropertyUser;
using CRM.Application.Shared.PropertyOrganizationUnit;
using CRM.Application.Shared.CatTypes;
using CRM.Entities.PropertyOrganizationUnit;
using CRM.Entities.PropertyUser;
using CRM.Application.Shared.Contact;
using CRM.Application.Shared.PropertyContact;
using CRM.Entities.PropertyContact;
using CRM.Application.Shared.Project.Dto;

namespace CRM.Services.Property
{
    public class PropertyAppService : SBIAppServiceBase, IPropertyAppService
    {
        private readonly IRepository<Entities.Properties.Property, long> _repoProperty;
        private readonly IRepository<PropertyFacilityMap, long> _repoPropertyFacilityMap;
        private readonly IRepository<PropertyGradeMap, long> _repoPropertyGradeMap;
        private readonly IRepository<PropertyTypeMap, long> _repoPropertyTypeMap;
        private readonly IRepository<PropertyAddress, long> _repoPropertyAddress;
        //private readonly IRepository<PropertyTenant, long> _repositoryPropertyTenant;
        //private readonly IRepository<PropertyLandlord, long> _repositoryPropertyLandlord;
        private readonly IRepository<Project, long> _repositoryProject;
        private readonly IRepository<Country, int> _repositoryCountry;
        private readonly IRepository<District, int> _repositoryDistrict;
        private readonly IRepository<Province, int> _repositoryProvince;
        private readonly IRepository<OtherCategory, int> _repositoryOtherCategory;
        private readonly IRepository<Currency, int> _repositoryCurrency;
        private readonly IRepository<PropertyOrganizationUnit, long> _repositoryPropertyOrganizationUnit;
        private readonly IRepository<PropertyUser, long> _repositoryPropertyUser;
        private readonly FacilityAppService _facilityAppService;
        private readonly ProjectTypeAppService _projectTypeAppService;
        private readonly GradeAppService _gradeAppService;
        private readonly CompanyAppService _companyAppService;
        private readonly IRepository<PropertyContact, long> _repositoryPropertyContact;
        private ConfigSetting _options;
        public PropertyAppService(IRepository<Entities.Properties.Property, long> repoProperty
            , IRepository<PropertyFacilityMap, long> repoPropertyFacilityMap
            , ProjectTypeAppService projectTypeAppService
            , IRepository<PropertyAddress, long> repoPropertyAddress, IOptions<ConfigSetting> options
            , IRepository<Country, int> repositoryCountry, IRepository<District, int> repositoryDistrict
            , IRepository<Province, int> repositoryProvince
            , IRepository<Project, long> repositoryProject
            //, IRepository<PropertyLandlord, long> repositoryPropertyLandlord
            , IRepository<OtherCategory, int> repositoryOtherCategory
            //, IRepository<PropertyTenant, long> repositoryPropertyTenant
            , IRepository<Currency, int> repositoryCurrency
            , FacilityAppService facilityAppService
            , CompanyAppService companyAppService
            , IRepository<PropertyTypeMap, long> repoPropertyTypeMap
            , GradeAppService gradeAppService
            , IRepository<PropertyGradeMap, long> repoPropertyGradeMap
            , IRepository<PropertyOrganizationUnit, long> repositoryPropertyOrganizationUnit
            , IRepository<PropertyUser, long> repositoryPropertyUser
             , IRepository<PropertyContact, long> repositoryPropertyContact
            )
        {
            _repoProperty = repoProperty;
            _repoPropertyFacilityMap = repoPropertyFacilityMap;
            _repoPropertyTypeMap = repoPropertyTypeMap;
            _repoPropertyAddress = repoPropertyAddress;
            _repositoryCountry = repositoryCountry;
            _repositoryDistrict = repositoryDistrict;
            _repositoryProvince = repositoryProvince;
            _repositoryProject = repositoryProject;
            //_repositoryPropertyLandlord = repositoryPropertyLandlord;
            //_repositoryPropertyTenant = repositoryPropertyTenant;
            _repositoryOtherCategory = repositoryOtherCategory;
            _facilityAppService = facilityAppService;
            _projectTypeAppService = projectTypeAppService;
            _companyAppService = companyAppService;
            _repoPropertyGradeMap = repoPropertyGradeMap;
            _gradeAppService = gradeAppService;
            _repositoryCurrency = repositoryCurrency;
            _repositoryPropertyUser = repositoryPropertyUser;
            _repositoryPropertyOrganizationUnit = repositoryPropertyOrganizationUnit;
            _repositoryPropertyContact = repositoryPropertyContact;
            _options = options.Value;
        }

        #region public function <APIs>

      
        public async Task<PropertyOutputDto> GetPropertyDetail(long propertyId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<PropertyFilterDto>("SpFilterproperties",
                    new
                    {
                        @ID = propertyId,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<PropertyOutputDto>>(output).SingleOrDefault();
                return results;
            }
        }
        public async Task<PropertyOutputDto> CreatePropertyAsync(PropertyInputDto input)
        {
            ValidateCreateProperty(input);
            var entity = ObjectMapper.Map<Entities.Properties.Property>(input);
            entity.IsActive = true;
            entity.Id = await _repoProperty.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<PropertyOutputDto>(entity);
        }
        public async Task<ContactOutputDto> GetContactByProperty(long propertyId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ContactFilterDto>("[SpFilterContact]",
                    new
                    {
                        @PropertyId = propertyId
                    }, commandType: CommandType.StoredProcedure);
                var result = ObjectMapper.Map<ContactOutputDto>(output.SingleOrDefault());
                return result;
            }
        }

        #region update proerty details
        public async Task<PropertyOutputDto> UpdatePropertyAsync(long propertyId, PropertyInputDto input)
        {
            if (propertyId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("PropertyIdNotFound"));
            var entity = _repoProperty.Get(propertyId);
            ValidateCreateProperty(input);
            var entityUpdate = ObjectMapper.Map(input, entity);
            entityUpdate.Id = propertyId;
            await _repoProperty.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<PropertyOutputDto>(entity);
        }
        #endregion

        #region insert or update property address
        [UnitOfWork]
        public async Task<List<PropertyAddressOutputDto>> UpdatePropertyAddress(long propertyId, List<PropertyAddressInputDto> propertyAddress)
        {
            var entity = await _repoProperty.GetAsync(propertyId);
            if (propertyAddress == null )
                throw new UserFriendlyException(L("ErrorAddress"));
            ValidatePropertyListAddress(propertyAddress);
            var output = ObjectMapper.Map<List<PropertyAddressOutputDto>>(CreateOrUpdateListPropertyAddress(entity.Id, propertyAddress));
            return output;
        }
        #endregion

        #region Insert or update property facility
        public async Task<List<PropertyFacilityMapOutputDto>> UpdatePropertyFacility(long propertyId, PropertyFacilityMapInputDto propertyFacilities)
        {
            var entity = await _repoProperty.GetAsync(propertyId);
            #region Insert or update property facilities
            if (propertyFacilities.FacilityIds == null)
                throw new UserFriendlyException(L("ErrorFacility"));
            _facilityAppService.ValidateProjectFacilities(propertyFacilities.FacilityIds);
            var output = ObjectMapper.Map<List<PropertyFacilityMapOutputDto>>(_facilityAppService.CreateOrUpdateListPropertyFacilities(entity.Id, propertyFacilities.FacilityIds));
            return output;
            #endregion
        }
        #endregion

        #region Insert or update property grade
        public async Task<List<PropertyGradeMapOutputDto>> UpdatePropertyGrade(long propertyId, PropertyGradeMapInputDto propertyGrades)
        {
            var entity = await _repoProperty.GetAsync(propertyId);
            #region Insert or update property facilities
            if (propertyGrades.GradeIds == null)
                throw new UserFriendlyException(L("ErrorGrade"));
            _gradeAppService.ValidateProjectGrades(propertyGrades.GradeIds);
            var output = ObjectMapper.Map<List<PropertyGradeMapOutputDto>>(_gradeAppService.CreateOrUpdateListPropertyGrades(entity.Id, propertyGrades.GradeIds));
            return output;
            #endregion
        }
        #endregion

        #region Insert or update property propertyType
        public async Task<List<PropertyTypeMapOutputDto>> UpdatePropertyType(long propertyId, PropertyTypeMapInputDto propertyTypes)
        {
            var entity = await _repoProperty.GetAsync(propertyId);
            if (propertyTypes.PropertyTypeIds == null)
                throw new UserFriendlyException(L("ErrorType"));
            _projectTypeAppService.ValidatePropertyTypes(propertyTypes.PropertyTypeIds);
            var output = ObjectMapper.Map<List<PropertyTypeMapOutputDto>>(_projectTypeAppService.CreateOrUpdateListPropertyTypes(entity.Id, propertyTypes.PropertyTypeIds));
            return output;
        }
        #endregion

        #region Insert or update property landlord
        //public async Task<List<PropertyLandlordOutputDto>> UpdatePropertyLandlord(long propertyId, PropertyLandlordInputDto propertyLandlords)
        //{
        //    var entity = await _repoProperty.GetAsync(propertyId);
        //    if (propertyLandlords == null)
        //        throw new UserFriendlyException(L("ErrorProperty"));
        //    _companyAppService.ValidateCompanyIds(propertyLandlords.PropertyLandlordIds);
        //    var output = ObjectMapper.Map<List<PropertyLandlordOutputDto>>(_companyAppService.CreateOrUpdateListPropertyLandlords(entity.Id, propertyLandlords));
        //    return output;
        //}
        #endregion

        #region Insert or update property tenants
        //public async Task<List<PropertyTenantOutputDto>> UpdatePropertyTenant(long propertyId, List<PropertyTenantInputDto> propertyTenants)
        //{
        //    var entity = await _repoProperty.GetAsync(propertyId);
        //    if (propertyTenants == null)
        //        throw new UserFriendlyException(L("ErrorProperty"));
        //    _companyAppService.ValidateTennatCompany(propertyTenants.Select(x => x.PropertyTenantId).ToList(), propertyTenants.Select(x => x.TenantTypeId).ToList());
        //    var output = ObjectMapper.Map<List<PropertyTenantOutputDto>>(_companyAppService.CreateOrUpdateListPropertyTenants(entity.Id, propertyTenants));
        //    return output;
        //}
        #endregion

        public List<PropertyUserOutputDto> CreateOrUpdatePropertyUser(long propertyId, List<PropertyUserInputDto> propertyUsers)
        {
            ValidatePropertyListUser(propertyId, propertyUsers);
            var output = ObjectMapper.Map<List<PropertyUserOutputDto>>(CreateOrUpdateListPropertyUser(propertyId, propertyUsers));
            return output;
        }
        public List<PropertyOrganizationUnitOutputDto> CreateOrUpdatePropertyOrganizationUnit(long propertyId, List<PropertyOrganizationUnitInputDto> propertyPropertyOrganizationUnits)
        {
            ValidatePropertyListOrganizationUnit(propertyId, propertyPropertyOrganizationUnits);
            var output = ObjectMapper.Map<List<PropertyOrganizationUnitOutputDto>>(CreateOrUpdateListPropertyOrganizationUnit(propertyId, propertyPropertyOrganizationUnits));
            return output;
        }
        public async Task<PagedResultDto<PropertyOrganizationUnitOutputDto>> GetListPropertyOrganizationUnit(FilterBasicInputDto input, long propertyId)
        {
            var query = _repositoryPropertyOrganizationUnit.GetAll()
                .Include(x => x.OrganizationUnit)
                .Where(x => x.PropertyId == propertyId);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber-1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<PropertyOrganizationUnitOutputDto>>(results);
            return new PagedResultDto<PropertyOrganizationUnitOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<PropertyUserOutputDto>> GetListPropertyUser(FilterBasicInputDto input, long propertyId)
        {
            var query = _repositoryPropertyUser.GetAll()
                .Include(x => x.User)
                .Where(x => x.PropertyId == propertyId);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber-1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<PropertyUserOutputDto>>(results);
            return new PagedResultDto<PropertyUserOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<PropertyOutputDto>> FilterProperty(string filterJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<PropertyFilterDto>("SpFilterproperties",
                    new
                    {
                        @FilterJson = filterJson,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<PropertyOutputDto>>(output);
                return new PagedResultDto<PropertyOutputDto>(totalCount, results);
            }
        }

        public async Task<PagedResultDto<PropertyOutputDto>> MachingProperties(long requestId, PropertyFilterInputDto input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<PropertyFilterDto>("SpFilterMachingProperties",
                    new
                    {
                        @PageNumber = input.PageNumber,
                        @PageSize = input.PageSize,
                        @RequestId = requestId,
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<PropertyOutputDto>>(output);
                return new PagedResultDto<PropertyOutputDto>(totalCount, results);
            }
        }

        public async Task<PagedResultDto<PropertyContactOutputDto>> GetListPropertyContact(FilterBasicInputDto input, long propertyId)
        {
            var query = _repositoryPropertyContact.GetAll()
              .Include(x => x.Contact)
              .Where(x => x.PropertyId == propertyId);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<PropertyContactOutputDto>>(results);
            return new PagedResultDto<PropertyContactOutputDto>(
                totalCount,
                output
            );
        }

        public List<PropertyContactOutputDto> CreateOrUpdatePropertyContact(long propertyId, List<PropertyContactInputDto> input)
        {
            ValidatePropertyContactList(propertyId, input);
            var output = ObjectMapper.Map<List<PropertyContactOutputDto>>(CreateOrUpdateListPropertyContact(propertyId, input));
            return output;
        }
        public async Task<List<ProjectMapOutputDto>> GetMapProperty(string SearchJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ProjectFilterMapDto>("SpSearchMap",
                    new
                    {
                        @SearchJson = SearchJson,
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ProjectMapOutputDto>>(output);
                return results;
            }
        }
        #endregion

        #region Action
        private List<PropertyContact> CreateOrUpdateListPropertyContact(long propertyId, List<PropertyContactInputDto> inputs)
        {
            var entities = new List<PropertyContact>();
            inputs.ForEach(x =>
            {
                entities.Add(CreateOrUpdatePropertyContact(propertyId, x));
            });
            return entities;
        }
        private List<PropertyUser> CreateOrUpdateListPropertyUser(long propertyId, List<PropertyUserInputDto> propertyUsers)
        {
            var entities = new List<PropertyUser>();
            propertyUsers.ForEach(x =>
            {
                entities.Add(CreateOrUpdatePropertyUser(propertyId, x));
            });
            return entities;
        }
        private List<PropertyOrganizationUnit> CreateOrUpdateListPropertyOrganizationUnit(long propertyId, List<PropertyOrganizationUnitInputDto> propertyOrganizationUnits)
        {

            var entities = new List<PropertyOrganizationUnit>();
            propertyOrganizationUnits.ForEach(x =>
            {
                entities.Add(CreateOrUpdatePropertyOrganizationUnit(propertyId, x));
            });
            return entities;
        }
        #endregion

        #region Validate
        //PropertyContact
        private void ValidatePropertyContactList(long propertyId, List<PropertyContactInputDto> inputs)
        {
            inputs.ForEach(x => ValidatePropertyContact(propertyId, x));
        }
        private void ValidatePropertyContact(long propertyId, PropertyContactInputDto input)
        {
            if (input.Id.HasValue)
            {
                if (!_repositoryPropertyContact.GetAll().Any(x => x.Id == input.Id.Value))
                    throw new UserFriendlyException(L("PropertyContactIdNotFound"));
            }
            else
            {
                if (_repositoryPropertyContact.GetAll().Any(x => x.PropertyId == propertyId && x.ContactId == input.ContactId))
                    throw new UserFriendlyException(L("ExistPropertyContact"));
            }
        }
        //PropertyUser
        private void ValidatePropertyListUser(long propertyId, List<PropertyUserInputDto> propertyUsers)
        {
            propertyUsers.ForEach(x => ValidatePropertyUser(propertyId, x));
        }
        private void ValidatePropertyUser(long propertyId, PropertyUserInputDto propertyUser)
        {
            if (propertyUser.Id.HasValue)
            {
                if (!_repositoryPropertyUser.GetAll().Any(x => x.Id == propertyUser.Id.Value))
                    throw new UserFriendlyException(L("PropertyUserIdNotFound"));
            }
            else
            {
                if (_repositoryPropertyUser.GetAll().Any(x => x.PropertyId == propertyId && x.UserID == propertyUser.UserId && x.IsActive == true && x.IsFollwer == propertyUser.IsFollwer))
                    throw new UserFriendlyException(L("ExistPropertyUser"));
            }
        }
        //OrganizationUnit
        private void ValidatePropertyListOrganizationUnit(long propertyId, List<PropertyOrganizationUnitInputDto> propertyOrganizationUnits)
        {
            propertyOrganizationUnits.ForEach(x => ValidatePropertyOrganizationUnit(propertyId, x));
        }
        private void ValidatePropertyOrganizationUnit(long propertyId, PropertyOrganizationUnitInputDto propertyOrganizationUnit)
        {
            if (propertyOrganizationUnit.Id.HasValue)
            {
                if (!_repositoryPropertyOrganizationUnit.GetAll().Any(x => x.Id == propertyOrganizationUnit.Id.Value))
                    throw new UserFriendlyException(L("PropertyOrganizationIdNotFound"));
            }
            else
            {
                if (_repositoryPropertyOrganizationUnit.GetAll().Any(x => x.PropertyId == propertyId && x.IsActive == true && x.OrganizationUnitId == propertyOrganizationUnit.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistPropertyOrganizationUnit"));
            }
        }
        #endregion

        #region Private Method

        private List<PropertyAddress> CreateOrUpdateListPropertyAddress(long propertyId, List<PropertyAddressInputDto> propertyAddress)
        {
            var entityAddress = _repoPropertyAddress.GetAll().Where(x => x.PropertyId == propertyId).SingleOrDefault();
            var entities = new List<PropertyAddress>();
            propertyAddress.ForEach(x =>
            {

                if (entityAddress != null)
                    x.Id = entityAddress.Id;
                entities.Add(CreateOrUpdatePropertyAddress(propertyId, x));
            });
            return entities;
        }

        private PropertyAddress CreateOrUpdatePropertyAddress(long propertyId, PropertyAddressInputDto input)
        {
            var entityAddress = _repoPropertyAddress.GetAll().Where(x => x.PropertyId == propertyId).SingleOrDefault();
            PropertyAddress entity = new PropertyAddress();
            if (entityAddress != null)
            {
                var addressId = entityAddress.Id;
                entity = ObjectMapper.Map(input, entityAddress);
                entity.Id = addressId;
            }
            else
            {
                entity = ObjectMapper.Map<PropertyAddress>(input);
            }
            entity.PropertyId = propertyId;
            entity.IsActive = true;
            entity.Latitude = input.Latitude;
            entity.Longitude = input.Longitude;
            _repoPropertyAddress.InsertOrUpdate(entity);
            return entity;
        }

        private void ValidatePropertyListAddress(List<PropertyAddressInputDto> propertyAddress)
        {
            propertyAddress.ForEach(ValidatePropertyAddress);
        }

        private void ValidatePropertyAddress(PropertyAddressInputDto propertyAddress)
        {
            if (propertyAddress.Id.HasValue)
            {
                if (!_repoPropertyAddress.GetAll().Any(x => x.Id == propertyAddress.Id.Value))
                    throw new UserFriendlyException(L("PropertyAddressIdNotFound"));
            }
            if (propertyAddress.DistrictId.HasValue)
            {
                if (!_repositoryDistrict.GetAll().Any(x => x.Id == propertyAddress.DistrictId.Value))
                    throw new UserFriendlyException(L("PropertyDistrictIdNotFound"));
            }
            if (propertyAddress.ProvinceId.HasValue)
            {
                if (!_repositoryProvince.GetAll().Any(x => x.Id == propertyAddress.ProvinceId.Value))
                    throw new UserFriendlyException(L("PropertyProvinceIdNotFound"));
            }
            if (!_repositoryCountry.GetAll().Any(x => x.Id == propertyAddress.CountryId))
                throw new UserFriendlyException(L("PropertyCountryIdNotFound"));
            if (propertyAddress.Latitude.HasValue && propertyAddress.Longitude.HasValue)
            {
                if (propertyAddress.Latitude < AppConsts.GeocodeValues.MinLat
                    || propertyAddress.Latitude > AppConsts.GeocodeValues.MaxLat
                    || propertyAddress.Longitude < AppConsts.GeocodeValues.MinLng
                    || propertyAddress.Longitude > AppConsts.GeocodeValues.MaxLng)
                    throw new UserFriendlyException(L("PropertyLatLngInvalid"));
            }

            if (string.IsNullOrEmpty(propertyAddress.Address))
                throw new UserFriendlyException(L("RequiredField", nameof(propertyAddress.Address)));

        }

        private void ValidateCreateProperty(PropertyInputDto input)
        {
            if (string.IsNullOrEmpty(input.PropertyCode))
                throw new UserFriendlyException(L("RequiredField", nameof(input.PropertyCode)));
            if (string.IsNullOrEmpty(input.PropertyName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.PropertyName)));
            if (input.StatusId.HasValue)
            {
                if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == input.StatusId && x.TypeCode.ToUpper() == PropertyConstant.PropertyStatus))
                    throw new UserFriendlyException(L("PropertyStatusIdNotFound"));
            }
            if (input.RequestTypeId.HasValue)
            {
                if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == input.RequestTypeId && x.TypeCode.ToUpper() == PropertyConstant.RequestType))
                    throw new UserFriendlyException(L("PropertyRequestTypeIdNotFound"));
            }
            if (input.CurrencyId.HasValue)
            {
                if (!_repositoryCurrency.GetAll().Any(x => x.Id == input.CurrencyId))
                    throw new UserFriendlyException(L("PropertyCurrencyIdNotFound"));
            }
        }
        private PropertyUser CreateOrUpdatePropertyUser(long propertyId, PropertyUserInputDto input)
        {
            PropertyUser entity = new PropertyUser();
            if (input.Id != null)
            {
                var entityUser = _repositoryPropertyUser.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityUser);
            }
            else
            {
                entity = ObjectMapper.Map<PropertyUser>(input);
            }
            entity.PropertyId = propertyId;
            entity.Id = _repositoryPropertyUser.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        [UnitOfWork]
        private PropertyOrganizationUnit CreateOrUpdatePropertyOrganizationUnit(long propertyId, PropertyOrganizationUnitInputDto input)
        {
            PropertyOrganizationUnit entity = new PropertyOrganizationUnit();
            if (input.Id != null)
            {
                var entityOrganizationUnit = _repositoryPropertyOrganizationUnit.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityOrganizationUnit);
            }
            else
            {
                entity = ObjectMapper.Map<PropertyOrganizationUnit>(input);
            }
            entity.PropertyId = propertyId;
            entity.Id = _repositoryPropertyOrganizationUnit.InsertOrUpdateAndGetId(entity);
            return entity;
        }

        private PropertyContact CreateOrUpdatePropertyContact(long propertyId, PropertyContactInputDto input)
        {
            PropertyContact entity = new PropertyContact();
            if (input.Id != null)
            {
                var entityContact = _repositoryPropertyContact.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityContact);
            }
            else
            {
                entity = ObjectMapper.Map<PropertyContact>(input);
            }
            entity.PropertyId = propertyId;
            entity.Id = _repositoryPropertyContact.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        #endregion
    }
}
