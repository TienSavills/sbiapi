﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Authorization.Users.Profile.Dto
{
    public class GetProfilePictureOutput
    {
        public string ProfilePicture { get; set; }

        public GetProfilePictureOutput(string profilePicture)
        {
            ProfilePicture = profilePicture;
        }
    }
}
