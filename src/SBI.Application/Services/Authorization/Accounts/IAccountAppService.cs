﻿using System.Threading.Tasks;
using Abp.Application.Services;
using CRM.Authorization.Users;
using CRM.Authorization.Users.Profile.Dto;
using CRM.Services.Authorization.Accounts.Dto;
using Microsoft.AspNetCore.Http;

namespace CRM.Services.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
        Task<ResetPasswordOutput> ResetPassword(ResetPasswordInput input);
        Task ChangePassword(ChangePasswordInput input);
        Task SendPasswordResetCode(SendPasswordResetCodeInput input);
        Task UpdateProfilePicture(UpdateProfilePictureInput input);
        UploadProfilePictureOutput UploadProfilePictureTemp(IFormFile profilePictureFile);
        Task<GetProfilePictureOutput> GetProfilePicture();
    }
}
