using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Configuration;
using Abp.Extensions;
using Abp.IO;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Abp.Zero.Configuration;
using CRM.Authentication.Helper;
using CRM.Authorization.Users;
using CRM.Authorization.Users.Profile.Dto;
using CRM.Configuration;
using CRM.Email;
using CRM.EmailHelper;
using CRM.Helpers;
using CRM.IO;
using CRM.Services.Authorization.Accounts.Dto;
using CRM.Storage;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace CRM.Services.Authorization.Accounts
{
    public class AccountAppService : SBIAppServiceBase, IAccountAppService
    {
        private const int MaxProfilPictureBytes = 1048576; //1MB
        private const int ResizedMaxProfilPictureBytesUserFriendlyValue = 1024;
        private const int MaxProfilPictureBytesUserFriendlyValue = 5;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IEmailSenderHelper _emailSender;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly IConfigHelper _configHelper;
        private readonly IHostingEnvironment _env;
        private const int MaxProfilePictureSize = 5242880; //5MB
        AppFolders appFolders;

        public AccountAppService(
            UserRegistrationManager userRegistrationManager
             , IPasswordHasher<User> passwordHasher
            , IEmailSenderHelper emailSender
            , IEmailTemplateProvider emailTemplateProvider
            , IConfigHelper configHelper
            , IHostingEnvironment env
            , IBinaryObjectManager binaryObjectManager)
        {
            _binaryObjectManager = binaryObjectManager;
            _userRegistrationManager = userRegistrationManager;
            _passwordHasher = passwordHasher;
            _emailTemplateProvider = emailTemplateProvider;
            _emailSender = emailSender;
            _configHelper = configHelper;
            _env = env;
            appFolders = SetAppFolders();
        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }
        public async Task SendPasswordResetCode(SendPasswordResetCodeInput input)
        {
            var user = await UserManager.FindByNameOrEmailAsync(input.EmailAddress);
            if (user == null)
            {
                throw new UserFriendlyException(L("AccountYourEmailNotActiveOrNotFound"));
            }
            var host = _configHelper.GetServerRoot();
            user.SetNewPasswordResetCode();
            var link = string.Format(host + "/account/reset-password?userId={0}&resetCode={1}", user.Id, user.PasswordResetCode);
            await SendPasswordResetLinkAsync(user, link);
        }

        public async Task<ResetPasswordOutput> ResetPassword(ResetPasswordInput input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var user = await UserManager.GetUserByIdAsync(input.UserId);
                if (user == null || user.PasswordResetCode.IsNullOrEmpty() || user.PasswordResetCode != input.ResetCode)
                {
                    throw new UserFriendlyException(L("InvalidPasswordResetCode"), L("InvalidPasswordResetCode_Detail"));
                }

                user.Password = _passwordHasher.HashPassword(user, input.Password);
                user.PasswordResetCode = null;
                user.IsEmailConfirmed = true;

                await UserManager.UpdateAsync(user);

                return new ResetPasswordOutput
                {
                    CanLogin = user.IsActive,
                    UserName = user.UserName
                };
            }
        }
        public async Task<RegisterOutput> Register(RegisterInput input)
        {
            var user = await _userRegistrationManager.RegisterAsync(
                input.Name,
                input.Surname,
                input.EmailAddress,
                input.UserName,
                input.Password,
                true // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
            );

            var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)
            };
        }
        public async Task ChangePassword(ChangePasswordInput input)
        {
            await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
            var user = await GetCurrentUserAsync();
            CheckErrors(await UserManager.ChangePasswordAsync(user, input.CurrentPassword, input.NewPassword));
        }
        //profile
        public async Task UpdateProfilePicture(UpdateProfilePictureInput input)
        {
            var appFolders = SetAppFolders();
            var tempProfilePicturePath = Path.Combine(appFolders.TempFileDownloadFolder, input.FileName);

            byte[] byteArray;

            using (var fsTempProfilePicture = new FileStream(tempProfilePicturePath, FileMode.Open))
            {
                using (var bmpImage = new Bitmap(fsTempProfilePicture))
                {
                    var width = input.Width == 0 ? bmpImage.Width : input.Width;
                    var height = input.Height == 0 ? bmpImage.Height : input.Height;
                    var bmCrop = bmpImage.Clone(new Rectangle(input.X, input.Y, width, height), bmpImage.PixelFormat);

                    using (var stream = new MemoryStream())
                    {
                        bmCrop.Save(stream, bmpImage.RawFormat);
                        byteArray = stream.ToArray();
                    }
                }
            }

            if (byteArray.Length > MaxProfilPictureBytes)
            {
                throw new UserFriendlyException(L("ResizedProfilePicture_Warn_SizeLimit", ResizedMaxProfilPictureBytesUserFriendlyValue));
            }

            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);

            if (user.ProfilePictureId.HasValue)
            {
                await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
            }

            var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
            await _binaryObjectManager.SaveAsync(storedFile);

            user.ProfilePictureId = storedFile.Id;
            FileHelper.DeleteIfExists(tempProfilePicturePath);
        }
        public UploadProfilePictureOutput UploadProfilePictureTemp(IFormFile profilePictureFile)
        {
            try
            {

                //Check input
                if (profilePictureFile == null)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
                }

                if (profilePictureFile.Length > MaxProfilePictureSize)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit", MaxProfilPictureBytesUserFriendlyValue));
                }

                byte[] fileBytes;
                using (var stream = profilePictureFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                if (!ImageFormatHelper.GetRawImageFormat(fileBytes).IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                {
                    throw new Exception("Uploaded file is not an accepted image file !");
                }

                //Delete old temp profile pictures
                AppFileHelper.DeleteFilesInFolderIfExists(appFolders.TempFileDownloadFolder, "userProfileImage_" + AbpSession.UserId.Value);

                //Save new picture
                var fileInfo = new FileInfo(profilePictureFile.FileName);
                var tempFileName = "userProfileImage_" + AbpSession.UserId.Value + fileInfo.Extension;
                var tempFilePath = Path.Combine(appFolders.TempFileDownloadFolder, tempFileName);
                File.WriteAllBytes(tempFilePath, fileBytes);

                using (var bmpImage = new Bitmap(tempFilePath))
                {
                    return new UploadProfilePictureOutput
                    {
                        FileName = tempFileName,
                        Width = bmpImage.Width,
                        Height = bmpImage.Height
                    };
                }
            }
            catch (UserFriendlyException ex)
            {
                return new UploadProfilePictureOutput(new ErrorInfo(ex.Message));
            }
        }
        [DisableAuditing]
        public async Task<GetProfilePictureOutput> GetProfilePicture()
        {
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            if (user.ProfilePictureId == null)
            {
                return new GetProfilePictureOutput(string.Empty);
            }

            return await GetProfilePictureById(user.ProfilePictureId.Value);
        }
        #region private API
        private AppFolders SetAppFolders()
        {
            var appFolders = new AppFolders();

            appFolders.SampleProfileImagesFolder = Path.Combine(_env.WebRootPath, $"Common{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}SampleProfilePics");
            appFolders.TempFileDownloadFolder = Path.Combine(_env.WebRootPath, $"Temp{Path.DirectorySeparatorChar}Downloads");
            appFolders.WebLogsFolder = Path.Combine(_env.ContentRootPath, $"App_Data{Path.DirectorySeparatorChar}Logs");

            try
            {
                DirectoryHelper.CreateIfNotExists(appFolders.TempFileDownloadFolder);
                return appFolders;
            }
            catch { return new AppFolders(); }
        }
        public async Task<GetProfilePictureOutput> GetProfilePictureById(Guid profilePictureId)
        {
            return await GetProfilePictureByIdInternal(profilePictureId);
        }
        private async Task<GetProfilePictureOutput> GetProfilePictureByIdInternal(Guid profilePictureId)
        {
            var bytes = await GetProfilePictureByIdOrNull(profilePictureId);
            if (bytes == null)
            {
                return new GetProfilePictureOutput(string.Empty);
            }

            var result = new GetProfilePictureOutput(Convert.ToBase64String(bytes));
            return result;
        }
        private async Task<byte[]> GetProfilePictureByIdOrNull(Guid profilePictureId)
        {
            var file = await _binaryObjectManager.GetOrNullAsync(profilePictureId);
            if (file == null)
            {
                return null;
            }

            return file.Bytes;
        }
        public async Task SendPasswordResetLinkAsync(User user, string link = null)
        {
            if (user.PasswordResetCode.IsNullOrEmpty())
            {
                throw new Exception("PasswordResetCode should be set in order to send password reset link.");
            }
            //var tenancyName = GetTenancyNameOrNull(user.TenantId);
            StringBuilder emailTemplate = new StringBuilder(_emailTemplateProvider.GetEmailTemplate($"CRM.EmailTemplate.ActiveEmail.html"));
            var dataBindings = new Dictionary<string, string>() {
                        {EmailConsts.ResetPass.Link, link},
                      {EmailConsts.ResetPass.ResetCode, user.PasswordResetCode},
            };
            var bodyHtmlContent = _emailTemplateProvider.BindDataToTemplate(emailTemplate.ToString(), dataBindings);
            await ReplaceBodyAndSend(user.EmailAddress, L("Password Reset Email"), bodyHtmlContent);
        }

        private async Task ReplaceBodyAndSend(string emailAddress, string subject, string emailTemplate)
        {
            await _emailSender.SendAsyncWithAttachments("info@sadec.co"
                  , emailAddress
                  , subject
                  , emailTemplate
                  , new List<SendGrid.Helpers.Mail.Attachment>(), true);
        }
        #endregion
    }
}
