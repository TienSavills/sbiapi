﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Services.Roles.Dto;

namespace CRM.Services.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
