﻿using CRM.Application.Shared.TenureTypes;
using System.Collections.Generic;
using Abp.Domain.Repositories;
using Abp.Collections.Extensions;
using CRM.Entities.TenureTypes;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using CRM.Application.Shared.CatTypes;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CRM.Services.TenureTypes
{
    public class TenureTypeAppService : SBIAppServiceBase, ITenureTypeAppService
    {
        private readonly IRepository<TenureType, int> _repoTenureType;

        public TenureTypeAppService (IRepository<TenureType, int> repoTenureType)
        {
            _repoTenureType = repoTenureType;
        }
        public async Task<PagedResultDto<TenureTypeOutputDto>> GetListTenureTypes(FilterBasicInputDto input)
        {
            var query = _repoTenureType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.TenureTypeName.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber-1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.TenureTypeName)
                .ToListAsync();
            var output = ObjectMapper.Map<List<TenureTypeOutputDto>>(results);
            return new PagedResultDto<TenureTypeOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<List<TenureTypeOutputDto>> GetListTenureTypes(string keyword)
        {
            var query = _repoTenureType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.TenureTypeName.ToUpper().Contains(keyword.ToUpper()));

            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<TenureTypeOutputDto>>(results);
        }
    }
}
