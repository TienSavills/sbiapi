﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using CRM.Application.Shared;
using CRM.Application.Shared.AssetClass;
using CRM.Application.Shared.CampaignCategory;
using CRM.Application.Shared.Category;
using CRM.Application.Shared.ClientType;
using CRM.Application.Shared.ConstructionStatus;
using CRM.Application.Shared.Countries;
using CRM.Application.Shared.Currentcys;
using CRM.Application.Shared.Districts;
using CRM.Application.Shared.DocumentTypes;
using CRM.Application.Shared.Grades;
using CRM.Application.Shared.Industry;
using CRM.Application.Shared.InquiryCategory;
using CRM.Application.Shared.Instruction;
using CRM.Application.Shared.LeadSource;
using CRM.Application.Shared.Level;
using CRM.Application.Shared.ListingCategory;
using CRM.Application.Shared.News;
using CRM.Application.Shared.OpportunityCategory;
using CRM.Application.Shared.OtherCategory;
using CRM.Application.Shared.Project.Dto;
using CRM.Application.Shared.ProjectCategory;
using CRM.Application.Shared.ProjectStates;
using CRM.Application.Shared.PropertyTypes;
using CRM.Application.Shared.Provinces;
using CRM.Application.Shared.RoomTypes;
using CRM.Application.Shared.TenantTypes;
using CRM.Application.Shared.TenureTypes;
using CRM.Application.Shared.UnitCategory;
using CRM.Application.Shared.Wards;
using CRM.Entities.AssetClass;
using CRM.Entities.CampaignCategory;
using CRM.Entities.ClientType;
using CRM.Entities.CompanyAddress;
using CRM.Entities.ConstructionStatus;
using CRM.Entities.ContactAddress;
using CRM.Entities.Countries;
using CRM.Entities.Currencys;
using CRM.Entities.Districs;
using CRM.Entities.DocumentTypes;
using CRM.Entities.Facilities;
using CRM.Entities.Facing;
using CRM.Entities.Grades;
using CRM.Entities.Industry;
using CRM.Entities.InquiryCategory;
using CRM.Entities.Instruction;
using CRM.Entities.LeadSource;
using CRM.Entities.Level;
using CRM.Entities.ListingCategory;
using CRM.Entities.News;
using CRM.Entities.OpportuityCategory;
using CRM.Entities.OtherCategory;
using CRM.Entities.ProjectAddresss;
using CRM.Entities.ProjectCategory;
using CRM.Entities.ProjectStates;
using CRM.Entities.PropertyTypes;
using CRM.Entities.PropertyValidation;
using CRM.Entities.Provinces;
using CRM.Entities.RoomTypes;
//using CRM.Entities.TenantTypes;
using CRM.Entities.TenureTypes;
using CRM.Entities.Transportation;
using CRM.Entities.UnitCategory;
using CRM.Entities.View;
using CRM.Entities.Wards;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace CRM.Services.Category
{
    public class CategoryAppService : SBIAppServiceBase, ICategoryAppService
    {
        private readonly IRepository<ClientType, int> _repoClientType;
        private readonly IRepository<ConstructionStatus, int> _repoConstructionStatus;
        private readonly IRepository<Country, int> _repoCountry;
        private readonly IRepository<Currency, int> _repoCurrency;
        private readonly IRepository<District, int> _repoDistrict;
        private readonly IRepository<DocumentType, int> _repoDocumentType;
        private readonly IRepository<Facility, int> _repoFacility;
        private readonly IRepository<View, int> _repoView;
        private readonly IRepository<Facing, int> _repoFacing;
        private readonly IRepository<Grade, int> _repoGrade;
        private readonly IRepository<Industry, int> _repoIndustry;
        private readonly IRepository<LeadSource, int> _repoLeadSource;
        private readonly IRepository<Level, int> _repoLevel;
        private readonly IRepository<ProjectState, int> _repoProjectState;
        private readonly IRepository<RoomType, int> _repoRoomType;
        private readonly IRepository<PropertyType, int> _repoPropertyType;
        private readonly IRepository<TenureType, int> _repoTenureType;
        private readonly IRepository<Ward, int> _repoWard;
        private readonly IRepository<Province, int> _repoProvince;
        private readonly IRepository<PropertyValidation, int> _repoPropertyValidation;
        private readonly IRepository<OtherCategory, int> _repoOtherCategory;
        private readonly IRepository<OpportunityCategory, int> _repoOpportunityCategory;
        private readonly IRepository<ListingCategory, int> _repoListingCategory;
        private readonly IRepository<InquiryCategory, int> _repoInquiryCategory;
        private readonly IRepository<Instruction, int> _repoInstruction;
        private readonly IRepository<AssetClass, int> _repoAssetClass;
        private readonly IRepository<ProjectCategory, int> _repoProjectCategory;
        private readonly IRepository<UnitCategory, int> _repoUnitCategory;
        private readonly IRepository<ProjectAddress, long> _repoProjectAddress;
        private readonly IRepository<CompanyAddress, long> _repoCompanyAddress;
        private readonly IRepository<ContactAddress, long> _repoContactAddress;
        private readonly IRepository<CampaignCategory, int> _repoCampaignCategory;
        private readonly IRepository<Transportation, int> _repoTransportation;
        private readonly IRepository<NewsCategory, int> _repoNewsCategory;
        private readonly ITranslationLanguageTextsCore _translationLanguageTextsCore;
        private readonly ConfigSetting _options;
        public CategoryAppService
            (
               IOptions<ConfigSetting> options,
               IRepository<ClientType, int> repoClientType,
                IRepository<ConstructionStatus, int> repoConstructionStatus,
                IRepository<Country, int> repoCountry,
                IRepository<CompanyAddress, long> repoCompanyAddress,
                IRepository<ContactAddress, long> repoContactAddress,
                IRepository<View, int> repoView,
                IRepository<Facing, int> repoFacing,
                IRepository<Currency, int> repoCurrency,
                IRepository<District, int> repoDistrict,
                IRepository<DocumentType, int> repoDocumentType,
                IRepository<Facility, int> repoFacility,
                IRepository<Grade, int> repoGrade,
                IRepository<Industry, int> repoIndustry,
                IRepository<LeadSource, int> repoLeadSource,
                IRepository<Level, int> repoLevel,
                IRepository<ProjectState, int> repoProjectState,
                IRepository<RoomType, int> repoRoomType,
                IRepository<PropertyType, int> repoPropertyType,
                //IRepository<TenantType, int> repoTenantType,
                IRepository<TenureType, int> repoTenureType,
                IRepository<Ward, int> repoWard,
                IRepository<Province, int> repoProvince,
                IRepository<PropertyValidation, int> repoPropertyValidation,
                IRepository<OtherCategory, int> repoOtherCategory,
                IRepository<OpportunityCategory, int> repoOpportunityCategory,
                IRepository<ListingCategory, int> repoListingCategory,
                IRepository<InquiryCategory, int> repoInquiryCategory,
                IRepository<Instruction, int> repoInstruction,
                IRepository<AssetClass, int> repoAssetClass,
                IRepository<ProjectCategory, int> repoProjectCategory,
                IRepository<UnitCategory, int> repoUnitCategory,
                IRepository<ProjectAddress, long> repoProjectAddress,
                IRepository<Transportation, int> repoTransportation,
                IRepository<CampaignCategory, int> repoCampaignCategory,
                IRepository<NewsCategory, int> repoNewsCategory,
                    ITranslationLanguageTextsCore translationLanguageTextsCore
            )
        {
            _repoClientType = repoClientType;
            _repoConstructionStatus = repoConstructionStatus;
            _repoCountry = repoCountry;
            _repoCurrency = repoCurrency;
            _repoDistrict = repoDistrict;
            _repoDocumentType = repoDocumentType;
            _repoFacility = repoFacility;
            _repoGrade = repoGrade;
            _repoIndustry = repoIndustry;
            _repoLeadSource = repoLeadSource;
            _repoLevel = repoLevel;
            _repoProjectState = repoProjectState;
            _repoRoomType = repoRoomType;
            _repoPropertyType = repoPropertyType;
            _repoTenureType = repoTenureType;
            _repoWard = repoWard;
            _repoProvince = repoProvince;
            _repoPropertyValidation = repoPropertyValidation;
            _repoOtherCategory = repoOtherCategory;
            _repoOpportunityCategory = repoOpportunityCategory;
            _repoInstruction = repoInstruction;
            _repoAssetClass = repoAssetClass;
            _repoProjectCategory = repoProjectCategory;
            _repoUnitCategory = repoUnitCategory;
            _repoProjectAddress = repoProjectAddress;
            _repoCampaignCategory = repoCampaignCategory;
            _repoCompanyAddress = repoCompanyAddress;
            _repoContactAddress = repoContactAddress;
            _repoView = repoView;
            _repoListingCategory = repoListingCategory;
            _repoInquiryCategory = repoInquiryCategory;
            _repoTransportation = repoTransportation;
            _repoFacing = repoFacing;
            _repoNewsCategory = repoNewsCategory;
            _translationLanguageTextsCore = translationLanguageTextsCore;
            _options = options.Value;
        }
        public async Task<List<ClientTypeOutputDto>> GetListClientType(string keyword)
        {
            var query = _repoClientType.GetAll()
               .Where(x => x.IsActive == true)
               .WhereIf(!string.IsNullOrEmpty(keyword),
                   x => x.ClientTypeName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ClientTypeOutputDto>>(results);
        }

        public async Task<List<ConstructionStatusOuputDto>> GetListConstructionStatus(string keyword)
        {
            var query = _repoConstructionStatus.GetAll()
              .Where(x => x.IsActive == true)
              .WhereIf(!string.IsNullOrEmpty(keyword),
                  x => x.Code.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ConstructionStatusOuputDto>>(results);
        }

        public async Task<List<CountryOutputDto>> GetListCountry(int? id)
        {
            var query = _repoCountry.GetAll().Where(x => x.IsActive == true);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CountryOutputDto>>(results);
        }
        public async Task<List<CountryOutputDto>> GetListCountryFull()
        {
            var query = _repoCountry.GetAll().Where(x => x.IsActive == true)
                .Include(x => x.Provinces).ThenInclude(y => y.Districts);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CountryOutputDto>>(results);
        }
        public async Task<List<CountryOutputDto>> GetListProjectCountry(int? id)
        {
            var listCountries = _repoProjectAddress.GetAll().Distinct().Select(x => x.CountryId);
            var query = _repoCountry.GetAll().Where(x => x.IsActive == true)
                .Where(x => listCountries.Contains(x.Id));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CountryOutputDto>>(results);
        }
        public async Task<List<CountryOutputDto>> GetListCountryByModule(bool? isCompany, bool? isContact)
        {
            var countryIds = isCompany.HasValue && isCompany == true ? _repoCompanyAddress.GetAll().Distinct().Select(x => x.CountryId) : _repoContactAddress.GetAll().Distinct().Select(x => x.CountryId);
            var query = _repoCountry.GetAll().Where(x => x.IsActive == true)
                .WhereIf(isCompany.HasValue, x => countryIds.Contains(x.Id))
                .WhereIf(isContact.HasValue, x => countryIds.Contains(x.Id));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CountryOutputDto>>(results);
        }
        public async Task<List<CurrencyOutputDto>> GetListCurrencies(string keyword)
        {
            var query = _repoCurrency.GetAll()
              .Where(x => x.IsActive == true)
              .WhereIf(!string.IsNullOrEmpty(keyword),
                  x => x.CurrencyName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CurrencyOutputDto>>(results);
        }

        public async Task<List<DistrictOutputDto>> GetListDistrict(int provinceId)
        {
            var query = _repoDistrict.GetAll()
                 .Where(x => x.IsActive == true)
                .Where(x => x.ProvinceId == provinceId)
                .OrderBy(x => x.DistrictName);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<DistrictOutputDto>>(results);
        }
        public async Task<List<DistrictOutputDto>> GetListProjectDistrict(int provinceId)
        {
            var listProvinces = _repoProjectAddress.GetAll().Distinct().Select(x => x.DistrictId);
            var query = _repoDistrict.GetAll()
                 .Where(x => x.IsActive == true)
                .Where(x => x.ProvinceId == provinceId)
                .Where(x => listProvinces.Contains(x.Id))
                 .OrderBy(x => x.DistrictName);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<DistrictOutputDto>>(results);
        }
        public async Task<List<DistrictOutputDto>> GetListDistrictByModule(int provinceId, bool? isCompany, bool? isContact)
        {
            var districtIds = isCompany.HasValue && isCompany == true ? _repoCompanyAddress.GetAll().Distinct().Select(x => x.DistrictId) : _repoContactAddress.GetAll().Distinct().Select(x => x.DistrictId);
            var query = _repoDistrict.GetAll()
                 .Where(x => x.IsActive == true)
                .Where(x => x.ProvinceId == provinceId)
                .Where(x => districtIds.Contains(x.Id))
                 .OrderBy(x => x.DistrictName); ;
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<DistrictOutputDto>>(results);
        }
        public async Task<List<DocumentTypeOutputDto>> GetListDocumentTypes(string keyword)
        {
            var query = _repoDocumentType.GetAll()
             .Where(x => x.IsActive == true)
             .WhereIf(!string.IsNullOrEmpty(keyword),
                 x => x.TypeName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<DocumentTypeOutputDto>>(results);
        }

        public async Task<List<CategoryOutputBasic>> GetListFacility(string keyword)
        {
            var query = _repoFacility.GetAll()
              .Where(x => x.IsActive == true)
              .WhereIf(!string.IsNullOrEmpty(keyword),
                  x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CategoryOutputBasic>>(results);
        }

        public async Task<List<CategoryOutputBasic>> GetListFacing(string keyword)
        {
            var query = _repoFacing.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CategoryOutputBasic>>(results);
        }

        public async Task<List<CategoryOutputBasic>> GetListView(string keyword)
        {
            var query = _repoView.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CategoryOutputBasic>>(results);
        }

        public async Task<List<CategoryOutputBasic>> GetListTransportation(string keyword)
        {
            var query = _repoTransportation.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CategoryOutputBasic>>(results);
        }


        public async Task<List<GradeOutputDto>> GetListGrade(string keyword)
        {
            var query = _repoGrade.GetAll()
             .Where(x => x.IsActive == true)
             .WhereIf(!string.IsNullOrEmpty(keyword),
                 x => x.GradeName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<GradeOutputDto>>(results);
        }

        public async Task<List<IndustryOutputDto>> GetListIndustries(string keyword)
        {
            var query = _repoIndustry.GetAll()
              .Where(x => x.IsActive == true)
              .WhereIf(!string.IsNullOrEmpty(keyword),
                  x => x.IndustryName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<IndustryOutputDto>>(results);
        }

        public async Task<List<LeadSourceOutputDto>> GetListLeadSource(string keyword)
        {
            var query = _repoLeadSource.GetAll()
             .Where(x => x.IsActive == true)
             .WhereIf(!string.IsNullOrEmpty(keyword),
                 x => x.LeadSourceName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<LeadSourceOutputDto>>(results);
        }

        public async Task<List<LevelOutputDto>> GetListLevels(string keyword)
        {
            var query = _repoLevel.GetAll()
              .Where(x => x.IsActive == true)
              .WhereIf(!string.IsNullOrEmpty(keyword),
                  x => x.LevelName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<LevelOutputDto>>(results);
        }

        public async Task<List<ProjectStateOutputDto>> GetListProjectState(string keyword)
        {
            var query = _repoProjectState.GetAll()
               .Where(x => x.IsActive == true)
               .WhereIf(!string.IsNullOrEmpty(keyword),
                   x => x.ProjectStateName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ProjectStateOutputDto>>(results);
        }

        public async Task<List<PropertyTypeOutputDto>> GetListPropertyType(string keyword)
        {
            var query = _repoPropertyType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.TypeName.ToUpper().Contains(keyword.ToUpper()));

            var results = await query.ToListAsync();

            var output = ObjectMapper.Map<List<PropertyTypeOutputDto>>(results);
            foreach (var item in output)
            {
                var propertyValidation = await _repoPropertyValidation.GetAll().Where(x => x.PropertyTypeId == item.Id).SingleOrDefaultAsync();
                if (propertyValidation != null)
                {
                    item.RequiredBuilding = propertyValidation.RequiredBuilding;
                    item.RequiredFloor = propertyValidation.RequiredFloor;
                    item.RequiredUnit = propertyValidation.RequiredUnit;
                }
            }
            return output;
        }

        public async Task<List<ProvinceOutputDto>> GetListProvince(int countryId)
        {

            var query = _repoProvince.GetAll()
               .Where(x => x.IsActive == true)
              .Where(x => x.CountryId == countryId);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ProvinceOutputDto>>(results);
        }

        public async Task<List<ProvinceOutputDto>> GetListProjectProvince(int countryId)
        {
            var listProvinces = _repoProjectAddress.GetAll().Distinct().Select(x => x.ProvinceId);
            var query = _repoProvince.GetAll()
               .Where(x => x.IsActive == true)
               .Where(x => listProvinces.Contains(x.Id))
              .Where(x => x.CountryId == countryId);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ProvinceOutputDto>>(results);
        }

        public async Task<List<ProvinceOutputDto>> GetListProvinceByModule(int countryId, bool? isCompany, bool? isContact)
        {
            var provinceIds = isCompany.HasValue && isCompany == true ? _repoCompanyAddress.GetAll().Distinct().Select(x => x.ProvinceId) : _repoContactAddress.GetAll().Distinct().Select(x => x.ProvinceId);

            var query = _repoProvince.GetAll()
               .Where(x => x.IsActive == true)
               .Where(x => provinceIds.Contains(x.Id))
              .Where(x => x.CountryId == countryId);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ProvinceOutputDto>>(results);
        }
        public async Task<List<RoomTypeOutputDto>> GetListRoomType(string keyword)
        {
            var query = _repoRoomType.GetAll()
             .Where(x => x.IsActive == true)
             .WhereIf(!string.IsNullOrEmpty(keyword),
                 x => x.RoomTypeName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<RoomTypeOutputDto>>(results);
        }

        public async Task<List<ProjectMapOutputDto>> GetMapProperty(string SearchJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ProjectFilterMapDto>("SpSearchMap",
                    new
                    {
                        @SearchJson = SearchJson,
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ProjectMapOutputDto>>(output);
                return results;
            }
        }

        public async Task<List<TenureTypeOutputDto>> GetListTenureType(string keyword)
        {
            var query = _repoTenureType.GetAll()
               .Where(x => x.IsActive == true)
               .WhereIf(!string.IsNullOrEmpty(keyword),
                   x => x.TenureTypeName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<TenureTypeOutputDto>>(results);
        }

        public async Task<List<WardOutputDto>> GetListWard(int? districtId)
        {
            var query = _repoWard.GetAll()
              .Where(x => x.IsActive == true)
            .WhereIf(districtId.HasValue, x => x.DistrictId == districtId);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<WardOutputDto>>(results);
        }
        public async Task<List<OtherCategoryOutputDto>> GetListOtherCategory(string keyword)
        {
            var query = _repoOtherCategory.GetAll()
            .Where(x => x.IsActive == true)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<OtherCategoryOutputDto>>(results);
        }

        public async Task<List<OpportunityCategoryOutputDto>> GetListOpportunityCategory(string keyword)
        {
            var query = _repoOpportunityCategory.GetAll()
            .Where(x => x.IsActive == true)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.Name.ToUpper().Contains(keyword.ToUpper()))
            .OrderBy(x => x.TypeCode)
            .ThenBy(x => x.SortOrder);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<OpportunityCategoryOutputDto>>(results);
        }
        public async Task<List<ListingCategoryOutputDto>> GetListListingCategory(string keyword)
        {
            var query = _repoListingCategory.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.Name.ToUpper().Contains(keyword.ToUpper()))
                .OrderBy(x => x.TypeCode)
                .ThenBy(x => x.SortOrder);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ListingCategoryOutputDto>>(results);
        }
      
        public async Task<List<InquiryCategoryOutputDto>> GetListInquiryCategory(string keyword)
        {
            var query = _repoInquiryCategory.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.Name.ToUpper().Contains(keyword.ToUpper()))
                .OrderBy(x => x.TypeCode)
                .ThenBy(x => x.SortOrder);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<InquiryCategoryOutputDto>>(results);
        }
        public async Task<List<OpportunityCategoryOutputDto>> GetListStageAdvisory(string keyword)
        {
            var query = _repoOpportunityCategory.GetAll()
            .Where(x => x.IsActive == true)
            .Where(x => x.TargetCode == Deal.DealConstant.TargetCodeStageAdvisory || x.TargetCode == Deal.DealConstant.TargetCodeOpportunityStage)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.Name.ToUpper().Contains(keyword.ToUpper()))
            .OrderBy(x => x.TypeCode)
            .ThenBy(x => x.SortOrder);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<OpportunityCategoryOutputDto>>(results);
        }
        public async Task<List<OpportunityCategoryOutputDto>> GetListDealStatusAdvisory(string keyword)
        {
            var query = _repoOpportunityCategory.GetAll()
            .Where(x => x.IsActive == true)
            .Where(x => x.TargetCode == Deal.DealConstant.TypeCodeDealStatus || x.TargetCode == Deal.DealConstant.TypeCodeDealStatusAdvisory)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.Name.ToUpper().Contains(keyword.ToUpper()))
            .OrderBy(x => x.TypeCode)
            .ThenBy(x => x.SortOrder);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<OpportunityCategoryOutputDto>>(results);
        }
        public async Task<List<OpportunityCategoryOutputDto>> GetListStageCommercial(string keyword)
        {
            var query = _repoOpportunityCategory.GetAll()
                .Where(x => x.IsActive == true)
                .Where(x => x.TargetCode == Deal.DealConstant.TargetCodeStageCommercial || x.TargetCode == Deal.DealConstant.TargetCodeOpportunityStage)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.Name.ToUpper().Contains(keyword.ToUpper()))
                .OrderBy(x => x.TypeCode)
                .ThenBy(x => x.SortOrder);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<OpportunityCategoryOutputDto>>(results);
        }
        public async Task<List<OpportunityCategoryOutputDto>> GetListDealStatusCommercial(string keyword)
        {
            var query = _repoOpportunityCategory.GetAll()
                .Where(x => x.IsActive == true)
                .Where(x => x.TargetCode == Deal.DealConstant.TypeCodeDealStatus || x.TargetCode == Deal.DealConstant.TypeCodeDealStatusCommercial)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.Name.ToUpper().Contains(keyword.ToUpper()))
                .OrderBy(x => x.TypeCode)
                .ThenBy(x => x.SortOrder);
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<OpportunityCategoryOutputDto>>(results);
        }
        public async Task<List<AssetClassOutputDto>> GetListAssetClass(string keyword)
        {
            var query = _repoAssetClass.GetAll()
            .Where(x => x.IsActive == true)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.AssetClassName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<AssetClassOutputDto>>(results);
        }
        public async Task<List<InstructionOutputDto>> GetListInstruction(string keyword)
        {
            var query = _repoInstruction.GetAll()
            .Where(x => x.IsActive == true)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.InstructionName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<InstructionOutputDto>>(results);
        }

        public async Task<List<ProjectCategoryOutputDto>> GetListProjectCategory(string keyword)
        {
            var query = _repoProjectCategory.GetAll()
            .Where(x => x.IsActive == true)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ProjectCategoryOutputDto>>(results);
        }
        public async Task<List<UnitCategoryOutputDto>> GetListUnitCategory(string keyword)
        {
            var query = _repoUnitCategory.GetAll()
            .Where(x => x.IsActive == true)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<UnitCategoryOutputDto>>(results);
        }
        public async Task<List<CampaignCategoryOutputDto>> GetListCampaignCategory(string keyword)
        {
            var query = _repoCampaignCategory.GetAll()
            .Where(x => x.IsActive == true)
            .WhereIf(!string.IsNullOrEmpty(keyword),
                x => x.Name.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CampaignCategoryOutputDto>>(results);
        }
        public async Task<List<NewsCategoryOutputDto>> GetListNewsCategory(string keyword)
        {
            var query = _repoNewsCategory.GetAll().Where(x => x.IsActive);
            var items = await query.ToListAsync();
            var output = ObjectMapper.Map<List<NewsCategoryOutputDto>>(items);
            await _translationLanguageTextsCore.AppendTranslation((x, tran) => x.Name = tran?.Value, x => x.NameId, output.ToArray());
            return output;
        }

    }
}
