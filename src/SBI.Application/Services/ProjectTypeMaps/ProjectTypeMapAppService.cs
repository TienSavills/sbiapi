﻿using System.Collections.Generic;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using CRM.Application.Shared.ProjectTypeMaps;
using CRM.Entities.ProjectTypeMaps;

namespace CRM.Services.ProjectTypeMaps
{
    public class ProjectTypeMapAppService : SBIAppServiceBase, IProjectTypeMapAppService
    {
        private readonly IRepository<ProjectTypeMap, long> _repoProjectTypeMap;

        public ProjectTypeMapAppService(IRepository<ProjectTypeMap, long> repoProjectTypeMap)
        {
            _repoProjectTypeMap = repoProjectTypeMap;
        }
        public Task<long> CreateProjectTypeMap(ProjectTypeMapInputDto input)
        {
            var data = ObjectMapper.Map<ProjectTypeMap>(input);
            var id = _repoProjectTypeMap.InsertOrUpdateAndGetIdAsync(data);
            return id;
        }

        public List<ProjectTypeMapOutputDto> GetListProjectTypeMap(long projectId)
        {
            var data = _repoProjectTypeMap.GetAll().Where(x => x.ProjectId == projectId)
                .Include(x=>x.PropertyType);
            return ObjectMapper.Map<List<ProjectTypeMapOutputDto>>(data);
        }
        public Task<long> UpdateProjectTypeMap(long id, ProjectTypeMapInputDto input)
        {
            var entity = ObjectMapper.Map<ProjectTypeMap>(input);
            return _repoProjectTypeMap.InsertOrUpdateAndGetIdAsync(entity);
        }

        public ProjectTypeMapOutputDto GetProjectTypeMapById(long id)
        {
            var data = _repoProjectTypeMap.GetAll()
                .Include(x => x.PropertyType)
                .Where(x => x.Id == id);
            return ObjectMapper.Map<ProjectTypeMapOutputDto>(data);
        }
    }
}
