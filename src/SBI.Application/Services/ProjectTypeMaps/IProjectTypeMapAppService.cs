﻿using Abp.Application.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using CRM.Application.Shared.ProjectTypeMaps;

namespace CRM.Services.ProjectTypeMaps
{
    public interface IProjectTypeMapAppService : IApplicationService
    {
        Task<long> CreateProjectTypeMap(ProjectTypeMapInputDto input);

        Task<long> UpdateProjectTypeMap(long id, ProjectTypeMapInputDto input);

        ProjectTypeMapOutputDto GetProjectTypeMapById(long id);

        List<ProjectTypeMapOutputDto> GetListProjectTypeMap(long propertyId);
    }
}
