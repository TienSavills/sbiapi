﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using CRM.Application.Shared.UnitHistory;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.UnitHistory
{
    public class UnitHistoryAppService : SBIAppServiceBase, IUnitHistoryAppService
    {
        private readonly IRepository<Entities.UnitHistory.UnitHistory, long> _repoUnitHistory;
        private readonly IRepository<Entities.Floor.Floor, long> _repoFloor;
        private readonly IRepository<Entities.Projects.Project, long> _repoProject;
        private readonly IRepository<Entities.Unit.Unit, long> _repoUnit;
        private readonly IRepository<Entities.Company.Company, long> _repoCompany;
        public UnitHistoryAppService(
            IRepository<Entities.UnitHistory.UnitHistory, long> repoUnitHistory
            , IRepository<Entities.Floor.Floor, long> repoFloor
            , IRepository<Entities.Projects.Project, long> repoProject
            , IRepository<Entities.Unit.Unit, long> repoUnit
            , IRepository<Entities.Company.Company, long> repoCompany
            )
        {
            _repoUnitHistory = repoUnitHistory;
            _repoFloor = repoFloor;
            _repoProject = repoProject;
            _repoUnit = repoUnit;
            _repoCompany = repoCompany;
        }

        public async Task<UnitHistoryOutputDto> CreateAsync(UnitHistoryInputDto input)
        {
            if (!_repoCompany.GetAll().Where(x => x.Id == input.OrgTenantId).Any())
                throw new UserFriendlyException(L("CompanyIdNotFound"));
            if (!_repoUnit.GetAll().Where(x => x.Id == input.UnitId).Any())
                throw new UserFriendlyException(L("UnitIdNotFound"));

            var currentUnitHistories = await _repoUnitHistory.GetAll()
                .Where(x => x.UnitId == input.UnitId && x.IsActive == true).ToListAsync();
            currentUnitHistories.ForEach(x =>
            {
                x.IsActive = false;
                _repoUnitHistory.Update(x);
            });
            var entity = ObjectMapper.Map<Entities.UnitHistory.UnitHistory>(input);
            entity.Id = await _repoUnitHistory.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<UnitHistoryOutputDto>(entity);
        }
        public async Task<UnitHistoryOutputDto> UpdateAsync(long id, UnitHistoryInputDto input)
        {
            if (!_repoCompany.GetAll().Where(x => x.Id == input.OrgTenantId).Any())
                throw new UserFriendlyException(L("CompanyIdNotFound"));
            if (!_repoUnit.GetAll().Where(x => x.Id == input.UnitId).Any())
                throw new UserFriendlyException(L("UnitIdNotFound"));
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("UnitHistoryIdNotFound"));
            var currentTenant = _repoUnitHistory.GetAll().Where(x => x.UnitId == input.UnitId && x.IsActive == true).SingleOrDefault();
            if (currentTenant != null && currentTenant.Id != input.Id && input.IsActive == true)
                throw new UserFriendlyException(L("OnlyOneCurrentTenant"));
            var entity = _repoUnitHistory.Get(id);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoUnitHistory.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<UnitHistoryOutputDto>(entity);
        }
        public async Task DeleteCurrentTenant(long id)
        {
            await _repoUnitHistory.DeleteAsync(id);
        }

        public async Task<List<UnitHistoryOutputDto>> GetListUnitHistory(long unitId)
        {
            var data = await _repoUnitHistory.GetAll().Where(x => x.UnitId == unitId && x.IsDeleted == false)
               .Include(x => x.Unit).ThenInclude(x => x.Floor).ThenInclude(y => y.Project)
               .Include(x => x.OrgTenant)
               .Include(x => x.CreatorUser)
               .Include(x => x.LastModifierUser)
               .ToListAsync();
            var output = ObjectMapper.Map<List<UnitHistoryOutputDto>>(data);
            foreach (var x in output)
            {
                if (x.ExpiredDate.HasValue && x.StartDate.HasValue && x.ExpiredDate.Value != x.StartDate.Value && x.Per.HasValue && x.Incentives.HasValue)
                {
                    var term = ((x.ExpiredDate.Value.Year - x.StartDate.Value.Year) * 12) + x.ExpiredDate.Value.Month - x.StartDate.Value.Month;
                    if (term > 0)
                        x.NetEffectiveRental = x.Per * ((term - x.Incentives) / Convert.ToDecimal(term));
                }
            }
            return output;
        }
        public async Task<List<UnitHistoryOutputDto>> GetListUnitHistoryByCompany(long orgTenantId)
        {
            var listCompany = _repoCompany.GetAll().Where(x => x.IsDeleted == false && (x.Id == orgTenantId || x.ParentId == orgTenantId))
                .Select(x => x.Id)
                .ToArray();

            var data = await _repoUnitHistory.GetAll().Where(x => listCompany.Any(p => p == x.OrgTenantId))
               .Include(x => x.Unit).ThenInclude(x => x.Floor).ThenInclude(y => y.Project)
               .Include(x => x.OrgTenant)
               .Include(x => x.CreatorUser)
               .Include(x => x.LastModifierUser)
               .ToListAsync();
            var output = ObjectMapper.Map<List<UnitHistoryOutputDto>>(data);
            foreach (var x in output)
            {
                if (x.ExpiredDate.HasValue && x.StartDate.HasValue && x.ExpiredDate.Value != x.StartDate.Value && x.Per.HasValue && x.Incentives.HasValue)
                {
                    var term = ((x.ExpiredDate.Value.Year - x.StartDate.Value.Year) * 12) + x.ExpiredDate.Value.Month - x.StartDate.Value.Month;
                    if (term > 0)
                        x.NetEffectiveRental = x.Per * ((term - x.Incentives) / Convert.ToDecimal(term));
                }
            }
            return output;
        }
        public async Task<UnitHistoryOutputDto> GetUnitHistoryDetails(long id)
        {
            var data = await _repoUnitHistory.GetAll().Where(x => x.Id == id && x.IsDeleted == false)
                .Include(x => x.Unit).ThenInclude(x => x.Floor).ThenInclude(y => y.Project)
               .Include(x => x.OrgTenant)
               .Include(x => x.CreatorUser)
               .Include(x => x.LastModifierUser)
               .SingleOrDefaultAsync();
            var output = ObjectMapper.Map<UnitHistoryOutputDto>(data);
            if (output.ExpiredDate.HasValue && output.StartDate.HasValue && output.ExpiredDate.Value != output.StartDate.Value && output.Per.HasValue && output.Incentives.HasValue)
            {
                var term = ((output.ExpiredDate.Value.Year - output.StartDate.Value.Year) * 12) + output.ExpiredDate.Value.Month - output.StartDate.Value.Month;
                if (term > 0)
                    output.NetEffectiveRental = output.Per * ((term - output.Incentives) / Convert.ToDecimal(term));
            }
            return output;
        }

    }
}
