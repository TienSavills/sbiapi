﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using CRM.Application.Shared.Wards;
using CRM.Entities.Wards;
using Microsoft.EntityFrameworkCore;

namespace CRM.Services.Wards
{
    public class WardAppService : SBIAppServiceBase, IWardAppService
    {
        private readonly IRepository<Ward, int> _repositoryWard;
        public WardAppService(IRepository<Ward, int> repositoryWard)
        {
            _repositoryWard = repositoryWard;
        }
        public async Task<PagedResultDto<WardOutputDto>> GetListWard(WardFilterInputDto input)
        {
            var query = _repositoryWard.GetAll()
               .Include(x=>x.District)
               .Where(x => x.IsActive == true)
               .WhereIf(input.DistrictId.HasValue,x=>x.DistrictId==input.DistrictId)
               .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                   x => x.WardName.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query.Page(input.PageNumber,input.PageSize)
                .OrderBy(x => x.WardName)
                .ToListAsync();
            var output = ObjectMapper.Map<List<WardOutputDto>>(results);
            return new PagedResultDto<WardOutputDto>(
                totalCount,
                output
            );
        }
    }
}
