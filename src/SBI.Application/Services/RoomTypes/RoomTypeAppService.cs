﻿using System.Collections.Generic;
using Abp.Domain.Repositories;
using CRM.Application.Shared.RoomTypes;
using CRM.Entities.RoomTypes;
using Abp.Collections.Extensions;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using CRM.Application.Shared.CatTypes;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CRM.Services.RoomTypes
{
    public class RoomTypeAppService : SBIAppServiceBase, IRoomTypeAppService
    {
        private readonly IRepository<RoomType, int> _repoRoomType;
        public RoomTypeAppService(IRepository<RoomType, int> repoRoomType)
        {
            _repoRoomType = repoRoomType;
        }
        public async Task<PagedResultDto<RoomTypeOutputDto>> GetListRoomTypes(FilterBasicInputDto input)
        {

            var query = _repoRoomType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.RoomTypeName.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber-1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.RoomTypeName)
                .ToListAsync();
            var output = ObjectMapper.Map<List<RoomTypeOutputDto>>(results);
            return new PagedResultDto<RoomTypeOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<List<RoomTypeOutputDto>> GetListRoomTypes(string keyword)
        {

            var query = _repoRoomType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.RoomTypeName.ToUpper().Contains(keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<RoomTypeOutputDto>>(results);

        }
    }
}
