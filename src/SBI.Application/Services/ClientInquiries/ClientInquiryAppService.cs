﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using CRM.Application.Shared.ClientInquiries;
using CRM.Application.Shared.Listings;
using CRM.Application.Shared.ListingUsers;
using CRM.Application.Shared.News;
using CRM.Authorization;
using CRM.Authorization.Users;
using CRM.Entities.ClientInquiries;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace CRM.Services.ClientInquiries
{

    public class ClientInquiryAppService : SBIAppServiceBase, IClientInquiryAppService
    {
        private readonly UserManager _userManager;
        private readonly IRepository<Entities.ClientInquiries.ClientInquiry, long> _repoClientInquiry;
        private readonly IRepository<Entities.ClientInquiries.ClientInquiryUsers, long> _repoClientInquiryUser;
        private readonly IRepository<Entities.ClientInquiries.ClientInquiryCategory, int> _repoClientInquiryCategory;
        private readonly ConfigSetting _options;
        public ClientInquiryAppService(
            IRepository<Entities.ClientInquiries.ClientInquiry, long> repoClientInquiry
            , IRepository<Entities.ClientInquiries.ClientInquiryUsers, long> repoClientInquiryUser
            , IRepository<Entities.ClientInquiries.ClientInquiryCategory, int> repoClientInquiryCategory
            , IOptions<ConfigSetting> options
            , UserManager userManager
            )
        {
            _repoClientInquiry = repoClientInquiry;
            _repoClientInquiryUser = repoClientInquiryUser;
            _repoClientInquiryCategory = repoClientInquiryCategory;
            _options = options.Value;
            _userManager = userManager;
        }

        public async Task<ClientInquiryOutputDto> CreateOrUpdateAsync(ClientInquiryInputDto input)
        {
            if (!_repoClientInquiryCategory.GetAll().Where(x => x.Id == input.StatusId).Any())
                throw new UserFriendlyException(L("StatusIdNotFound"));
           
            Entities.ClientInquiries.ClientInquiry entity;
            if (input.Id > 0)
            {
                if (input.Id < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("ClientInquiryNotFound"));
                entity = await _repoClientInquiry.GetAsync(input.Id);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoClientInquiry.UpdateAsync(entityUpdate);
            }
            else
            {
                entity = ObjectMapper.Map<Entities.ClientInquiries.ClientInquiry>(input);
                entity.IsActive = true;
                entity.Id = await _repoClientInquiry.InsertAndGetIdAsync(entity);
            }
            await CurrentUnitOfWork.SaveChangesAsync();
            if (input.UserIds != null)
            {
                await UpdateChilds(
                    _repoClientInquiryUser,
                    x => x.UserId = entity.Id,
                    x => x.UserId == entity.Id,
                    (x, y) => x.UserId == y.UserId,
                    input.UserIds.Select(x => new ClientInquiryUserInputDto()
                    { UserId = x, ClientInquiryId = entity.Id, IsActive = true }).ToArray()
                );
            }
            return ObjectMapper.Map<ClientInquiryOutputDto>(entity);

        }

        public async Task<PagedResultDto<ClientInquiryOutputDto>> GetAllAsync(PagedClientInquiryResultRequestDto input)
        {
            var  query =  from clientInquiry in  _repoClientInquiry.GetAll()
                .Include(x => x.Status)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                where (string.IsNullOrEmpty(input.Keyword) || clientInquiry.ClientName.Contains(input.Keyword)|| clientInquiry.Messenge.Contains(input.Keyword))
                          select clientInquiry;
            var totalCount = await query.CountAsync();
            var items = await query.OrderByDescending(x => x.LastModificationTime ?? x.CreationTime).PageBy(input)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ClientInquiryOutputDto>>(items);

            return new PagedResultDto<ClientInquiryOutputDto>(
                totalCount,
                output
            );
        }


        public async Task<ClientInquiryOutputDto> GetAsync(long id)
        {
            var entity = await _repoClientInquiry.GetAll()
                .Include(x=>x.Status)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser).FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
                throw new EntityNotFoundException(typeof(Entities.ClientInquiries.ClientInquiry), id);
            var output = ObjectMapper.Map<ClientInquiryOutputDto>(entity);
            return output;
        }
    }
}
