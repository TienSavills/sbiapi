﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Services.MultiTenancy.Dto;

namespace CRM.Services.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
