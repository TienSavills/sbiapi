﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Request
{
    public static class RequestConstant
    {
        public const string RequestStatus= "REQUESTSTATUS";
        public const string RequestType = "REQUIREMENTTYPE";
        public const string RequestSource = "REQUESTSOURCE";
    }
}
