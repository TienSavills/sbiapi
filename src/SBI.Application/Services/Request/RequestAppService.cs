﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using CRM;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.Request;
using CRM.Application.Shared.RequestOrganizationUnit;
using CRM.Application.Shared.RequestUser;
using CRM.Entities.Currencys;
using CRM.Entities.OtherCategory;
using CRM.Entities.PropertyTypes;
using CRM.Entities.RequestOrganizationUnit;
using CRM.Entities.RequestUser;
using CRM.SBIMap;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using CRM.EntitiesCustom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Application.Shared.RequestContact;
using CRM.Entities.RequestContact;
using CRM.Application.Shared.Project.Dto;

namespace CRM.Services.Request
{
    [AbpAuthorize]
    public class RequestAppService : SBIAppServiceBase, IRequestAppService
    {
        private readonly IRepository<RequestOrganizationUnit, long> _repositoryRequestOrganizationUnit;
        private readonly IRepository<Entities.Requests.Request, long> _repositoryRequest;
        private readonly IRepository<RequestUser, long> _repositoryRequestUser;
        private readonly IRepository<Currency, int> _repositoryCurrency;
        private readonly IRepository<OtherCategory, int> _repositoryOtherCategory;
        private readonly IRepository<PropertyType, int> _repositoryPropertyType;
        private readonly IRepository<RequestContact, long> _repositoryRequestContact;
        private ConfigSetting _options;
        public RequestAppService(IRepository<Entities.Requests.Request, long> repositoryRequest
            , IRepository<RequestOrganizationUnit, long> repositoryRequestOrganizationUnit
            , IRepository<RequestUser, long> repositoryRequestUser
            , IRepository<OtherCategory, int> repositoryOtherCategory
            , IRepository<Currency, int> repositoryCurrency
            , IRepository<PropertyType, int> repositoryPropertyType
            , IRepository<RequestContact, long> repositoryRequestContact
            , IOptions<ConfigSetting> options
            )
        {
            _repositoryRequest = repositoryRequest;
            _repositoryRequestOrganizationUnit = repositoryRequestOrganizationUnit;
            _repositoryRequestUser = repositoryRequestUser;
            _repositoryOtherCategory = repositoryOtherCategory;
            _repositoryCurrency = repositoryCurrency;
            _repositoryPropertyType = repositoryPropertyType;
            _repositoryRequestContact = repositoryRequestContact;
            _options = options.Value;
        }
        #region Public API
        public async Task<RequestOutputDto> CreateRequestAsync(RequestInputDto input)
        {
            ValidateCreateRequest(input);
            var entity = ObjectMapper.Map<Entities.Requests.Request>(input);
            entity.IsActive = true;
            entity.Id = await _repositoryRequest.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<RequestOutputDto>(entity);
        }
        public async Task<RequestOutputDto> UpdateRequestAsync(long requestId, RequestInputDto input)
        {
            if (requestId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("RequestIdNotFound"));
            var entity = _repositoryRequest.Get(requestId);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repositoryRequest.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<RequestOutputDto>(entity);
        }

        public async Task<PagedResultDto<RequestOutputDto>> GetListRequest(string filterJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<RequestFilterDto>("SpFilterRequest",
                    new
                    {
                        @FilterJson = filterJson,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<RequestOutputDto>>(output);
                return new PagedResultDto<RequestOutputDto>(totalCount, results);
            }
        }
        public async Task<List<ProjectOutputDto>> MatchingProjects(long requestId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ProjectFilterDto>("SpFilterMatchingProjects",
                   new
                   {
                       @RequestId = requestId
                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ProjectOutputDto>>(output);
                return results;
            }
        }
        public async Task<List<RequestOutputDto>> MathchingUnit(long unitId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<RequestOutputDto>("SpFilterMatchingRequest",
                    new
                    {
                        @Unitid = unitId,
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<RequestOutputDto>>(output);
                return results;
            }
        }
        public async Task<RequestOutputDto> GetRequestDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<RequestFilterDto>("SpFilterRequest",
                    new
                    {
                        @Id = id,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var result = ObjectMapper.Map<RequestOutputDto>(output.SingleOrDefault());
                return result;
            }
        }

        public async Task<PagedResultDto<RequestOutputDto>> GetListRequestByCompany(long orgTenantId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<RequestFilterDto>("SpFilterRequest",
                    new
                    {
                        @OrgTenantId= orgTenantId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<RequestOutputDto>>(output);
                return new PagedResultDto<RequestOutputDto>(totalCount, results);
            }
        }

        public List<RequestUserOutputDto> CreateOrUpdateRequestUser(long requestId, List<RequestUserInputDto> requestUsers)
        {
            ValidateRequestListUser(requestId, requestUsers);
            var output = ObjectMapper.Map<List<RequestUserOutputDto>>(CreateOrUpdateListRequestUser(requestId, requestUsers));
            return output;
        }
        public List<RequestContactOutputDto> CreateOrUpdateRequestContact(long requestId, List<RequestContactInputDto> input)
        {
            ValidateRequestContactList(requestId, input);
            var output = ObjectMapper.Map<List<RequestContactOutputDto>>(CreateOrUpdateListRequestContact(requestId, input));
            return output;
        }
        public List<RequestOrganizationUnitOutputDto> CreateOrUpdateRequestOrganizationUnit(long requestId, List<RequestOrganizationUnitInputDto> requestRequestOrganizationUnits)
        {
            ValidateRequestListOrganizationUnit(requestId, requestRequestOrganizationUnits);
            var output = ObjectMapper.Map<List<RequestOrganizationUnitOutputDto>>(CreateOrUpdateListRequestOrganizationUnit(requestId, requestRequestOrganizationUnits));
            return output;
        }

        public async Task<PagedResultDto<RequestOrganizationUnitOutputDto>> GetListRequestOrganizationUnit(FilterBasicInputDto input, long requestId)
        {
            var query = _repositoryRequestOrganizationUnit.GetAll()
                .Include(x => x.OrganizationUnit)
                .Where(x => x.RequestId == requestId);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<RequestOrganizationUnitOutputDto>>(results);
            return new PagedResultDto<RequestOrganizationUnitOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<RequestUserOutputDto>> GetListRequestUser(FilterBasicInputDto input, long requestId)
        {
            var query = _repositoryRequestUser.GetAll()
                .Include(x => x.User)
                .Where(x => x.RequestId == requestId);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<RequestUserOutputDto>>(results);
            return new PagedResultDto<RequestUserOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<RequestContactOutputDto>> GetListRequestContact(FilterBasicInputDto input, long requestId)
        {
            var query = _repositoryRequestContact.GetAll()
              .Include(x => x.Contact)
              .Where(x => x.RequestId == requestId);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<RequestContactOutputDto>>(results);
            return new PagedResultDto<RequestContactOutputDto>(
                totalCount,
                output
            );
        }



        #endregion

        #region Action
        private List<RequestContact> CreateOrUpdateListRequestContact(long requestId, List<RequestContactInputDto> inputs)
        {
            var entities = new List<RequestContact>();
            inputs.ForEach(x =>
            {
                entities.Add(CreateOrUpdateRequestContact(requestId, x));
            });
            return entities;
        }
        private List<RequestUser> CreateOrUpdateListRequestUser(long requestId, List<RequestUserInputDto> requestUsers)
        {
            var entities = new List<RequestUser>();
            requestUsers.ForEach(x =>
            {
                entities.Add(CreateOrUpdateRequestUser(requestId, x));
            });
            return entities;
        }
        private List<RequestOrganizationUnit> CreateOrUpdateListRequestOrganizationUnit(long requestId, List<RequestOrganizationUnitInputDto> requestOrganizationUnits)
        {

            var entities = new List<RequestOrganizationUnit>();
            requestOrganizationUnits.ForEach(x =>
            {
                entities.Add(CreateOrUpdateRequestOrganizationUnit(requestId, x));
            });
            return entities;
        }
        #endregion

        #region Validate
        public void ValidateCreateRequest(RequestInputDto input)
        {
            if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == input.StatusId && x.TypeCode.ToUpper() == RequestConstant.RequestStatus))
                throw new UserFriendlyException(L("RequestStatusIdNotFound"));
            if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == input.RequirementTypeId && x.TypeCode.ToUpper() == RequestConstant.RequestType))
                throw new UserFriendlyException(L("RequirementTypeIdNotFound"));
            if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == input.SourceId && x.TypeCode.ToUpper() == RequestConstant.RequestSource))
                throw new UserFriendlyException(L("SourceIdNotFound"));
        }
        //RequestContact
        private void ValidateRequestContactList(long requestId, List<RequestContactInputDto> inputs)
        {
            inputs.ForEach(x => ValidateRequestContact(requestId, x));
        }

        private void ValidateRequestContact(long requestId, RequestContactInputDto input)
        {
            if (input.Id.HasValue)
            {
                if (!_repositoryRequestContact.GetAll().Any(x => x.Id == input.Id.Value))
                    throw new UserFriendlyException(L("RequestContactIdNotFound"));
            }
            else
            {
                if (_repositoryRequestContact.GetAll().Any(x => x.RequestId == requestId && x.ContactId == input.ContactId))
                    throw new UserFriendlyException(L("ExistRequestContact"));
            }
        }
        //RequestUser
        private void ValidateRequestListUser(long requestId, List<RequestUserInputDto> requestUsers)
        {
            requestUsers.ForEach(x => ValidateRequestUser(requestId, x));
        }
        private void ValidateRequestUser(long requestId, RequestUserInputDto requestUser)
        {
            if (requestUser.Id.HasValue)
            {
                if (!_repositoryRequestUser.GetAll().Any(x => x.Id == requestUser.Id.Value))
                    throw new UserFriendlyException(L("RequestUserIdNotFound"));
            }
            else
            {
                if (_repositoryRequestUser.GetAll().Any(x => x.RequestId == requestId && x.UserID == requestUser.UserId &&x.IsActive==true && x.IsFollwer == requestUser.IsFollwer))
                    throw new UserFriendlyException(L("ExistRequestUser"));
            }
        }
        //OrganizationUnit
        private void ValidateRequestListOrganizationUnit(long requestId, List<RequestOrganizationUnitInputDto> requestOrganizationUnits)
        {
            requestOrganizationUnits.ForEach(x => ValidateRequestOrganizationUnit(requestId, x));
        }
        private void ValidateRequestOrganizationUnit(long requestId, RequestOrganizationUnitInputDto requestOrganizationUnit)
        {
            if (requestOrganizationUnit.Id.HasValue)
            {
                if (!_repositoryRequestOrganizationUnit.GetAll().Any(x => x.Id == requestOrganizationUnit.Id.Value))
                    throw new UserFriendlyException(L("RequestOrganizationIdNotFound"));
            }
            else
            {
                if (_repositoryRequestOrganizationUnit.GetAll().Any(x => x.RequestId == requestId && x.IsActive == true && x.OrganizationUnitId == requestOrganizationUnit.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistRequestOrganizationUnit"));
            }
        }
        #endregion

        #region Private API
        [UnitOfWork]
        private RequestContact CreateOrUpdateRequestContact(long requestId, RequestContactInputDto input)
        {
            RequestContact entity = new RequestContact();
            if (input.Id != null)
            {
                var entityContact = _repositoryRequestContact.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityContact);
            }
            else
            {
                entity = ObjectMapper.Map<RequestContact>(input);
            }
            entity.RequestId = requestId;
            entity.Id = _repositoryRequestContact.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        [UnitOfWork]
        private RequestUser CreateOrUpdateRequestUser(long requestId, RequestUserInputDto input)
        {
            RequestUser entity = new RequestUser();
            if (input.Id != null)
            {
                var entityUser = _repositoryRequestUser.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityUser);
            }
            else
            {
                entity = ObjectMapper.Map<RequestUser>(input);
            }
            entity.RequestId = requestId;
            entity.Id = _repositoryRequestUser.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        [UnitOfWork]
        private RequestOrganizationUnit CreateOrUpdateRequestOrganizationUnit(long requestId, RequestOrganizationUnitInputDto input)
        {
            RequestOrganizationUnit entity = new RequestOrganizationUnit();
            if (input.Id != null)
            {
                var entityOrganizationUnit = _repositoryRequestOrganizationUnit.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityOrganizationUnit);
            }
            else
            {
                entity = ObjectMapper.Map<RequestOrganizationUnit>(input);
            }
            entity.RequestId = requestId;
            entity.Id = _repositoryRequestOrganizationUnit.InsertOrUpdateAndGetId(entity);
            return entity;
        }

        #endregion
    }
}
