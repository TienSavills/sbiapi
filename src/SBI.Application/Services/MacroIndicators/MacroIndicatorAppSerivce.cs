﻿using CRM.Application.Shared.MacroIndicator;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using CRM.SBIMap;
using Abp.Domain.Repositories;
using CRM.Entities.MacroIndicators;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Data.SqlClient;
using CRM.EntitiesCustom;
using System.Data;
using Dapper;
using Abp.Application.Services;
using Abp.UI;

namespace CRM.Services.MacroIndicators
{
    public class MacroIndicatorAppService : SBIAppServiceBase, IMacroIndicatorAppService
    {
        private readonly IRepository<MacroIndicator, long> _repoMacroIndicator;
        private ConfigSetting _options;
        public MacroIndicatorAppService(
            IRepository<MacroIndicator, long> repoMacroIndicator
            , IOptions<ConfigSetting> options
            )
        {
            _repoMacroIndicator = repoMacroIndicator;
            _options = options.Value;
        }
        #region public API
        public async Task<MacroIndicatorOutputDto> CreateMacroIndicatorAsync(MacroIndicatorInputDto input)
        {
            ValidateCreateMacroIndicator(input);
            var entity = ObjectMapper.Map<MacroIndicator> (input);
            entity.Id = await _repoMacroIndicator.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<MacroIndicatorOutputDto>(entity);
        }

        public async Task<PagedResultDto<MacroIndicatorOutputDto>> FilterMacroIndicatorAsync(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<MacroIndicatorFilterDto>("SpFilterMacroIndicator",
                    new
                    {
                        @FilterJson = input
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<MacroIndicatorOutputDto>>(output);
                return new PagedResultDto<MacroIndicatorOutputDto>(totalCount, results);
            }
        }

        public async Task<MacroIndicatorOutputDto> GetMacroIndicatorDetail(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<MacroIndicatorFilterDto>("SpFilterMacroIndicator",
                    new
                    {
                        @Id = id
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<MacroIndicatorOutputDto>>(output).SingleOrDefault();
                return results;
            }
        }

        public async Task<MacroIndicatorOutputDto> UpdateMacroIndicatorAsync(long id, MacroIndicatorInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("MacroIndicatorIdNotFound"));
            ValidateUpdateMacroIndicator(input,id);
            var entity = _repoMacroIndicator.Get(id);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoMacroIndicator.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<MacroIndicatorOutputDto>(entity);
        }
        #endregion

        #region private API
        public void ValidateCreateMacroIndicator(MacroIndicatorInputDto input)
        {
           if( _repoMacroIndicator.Count(x=>Convert.ToDateTime(x.DataDate).Year== Convert.ToDateTime(input.DataDate).Year)>0)
                 throw new UserFriendlyException(L("YearMacroIndicatorExist"));
           if(!input.DataDate.HasValue)
                throw new UserFriendlyException(L("DataDateInvalid"));
        }
        public void ValidateUpdateMacroIndicator(MacroIndicatorInputDto input,long id)
        {
            if (_repoMacroIndicator.Count(x =>x.Id!=id && Convert.ToDateTime(x.DataDate).Year == Convert.ToDateTime(input.DataDate).Year) > 0)
                throw new UserFriendlyException(L("YearMacroIndicatorExist"));
        }
        #endregion
    }
}
