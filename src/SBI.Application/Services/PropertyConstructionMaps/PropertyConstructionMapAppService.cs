﻿using Abp.Application.Services;
using CRM.Application.Shared.PropertyConstructionMaps;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using CRM.SBIMap;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using Abp.Linq.Extensions;
using System.Linq;
using CRM.EntitiesCustom;
using Abp.UI;
using CRM.Services.Currencys;
using CRM.Entities.Properties;
using Microsoft.EntityFrameworkCore;
using CRM.Entities.ProperyConstructionMaps;

namespace CRM.Services.PropertyConstructionMaps
{
    public class PropertyConstructionMapAppService : SBIAppServiceBase, IPropertyConstructionMapAppService
    {
        private readonly IRepository<PropertyConstructionMap, long> _repoPropertyConstructionMap;
        private readonly IRepository<Entities.Properties.Property, long> _repoProperty;
        private readonly IRepository<Entities.ConstructionStatus.ConstructionStatus, int> _repoConstructionStatus;
        private ConfigSetting _options;
        public PropertyConstructionMapAppService(
            IOptions<ConfigSetting> options
            ,IRepository<PropertyConstructionMap, long> repoPropertyConstructionMap
            , IRepository<Entities.Properties.Property, long> repoProperty
            , IRepository<Entities.ConstructionStatus.ConstructionStatus, int> repoConstructionStatus)
        {
            _repoPropertyConstructionMap = repoPropertyConstructionMap;
            _repoProperty = repoProperty;
            _repoConstructionStatus = repoConstructionStatus;
            _options = options.Value;
        }
        #region public API
        public async Task<PropertyConstructionMapOutputDto> CreatePropertyConstructionMapAsync(PropertyConstructionMapInputDto input)
        {
            ValidateCreateOrUpdatePropertyConstruction(input);
            var entity = ObjectMapper.Map<PropertyConstructionMap>(input);
            entity.IsActive = true;
            entity.Id = await _repoPropertyConstructionMap.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<PropertyConstructionMapOutputDto>(entity);
        }

        public async Task<List<PropertyConstructionMapOutputDto>> FilterPropertyConstructionMapAsync(PropertyConstructionMapFilterInputDto filters)
        {
            var query =  _repoPropertyConstructionMap.GetAll()
                .Include(x => x.Property)
                .Include(x => x.ConstructionStatus)
                .WhereIf(filters.PropertyId.HasValue,x=>x.PropertyId==filters.PropertyId)
                .WhereIf(!string.IsNullOrEmpty(filters.Keyword)
                , x => x.Property.PropertyName.ToUpper().Contains(filters.Keyword.ToUpper())
                ||x.ConstructionStatus.StateName.ToUpper().Contains(filters.Keyword.ToUpper()));
            var results = await query.ToListAsync();
            if (results.Count() == 0)
                throw new UserFriendlyException(L("PropertyConstructionMapIdNotFound"));
            return ObjectMapper.Map<List<PropertyConstructionMapOutputDto>>(results);
        }


        public async Task<PropertyConstructionMapOutputDto> GetPropertyConstructionMapDetail(long id)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("PropertyConstructionMapIdNotFound"));
            var results = await _repoPropertyConstructionMap.GetAll().Where(x => x.Id == id)
                .Include(x => x.Property)
                .Include(x => x.ConstructionStatus).SingleOrDefaultAsync();
            if (results == null)
                throw new UserFriendlyException(L("PropertyConstructionMapIdNotFound"));
            return ObjectMapper.Map<PropertyConstructionMapOutputDto>(results);
        }

        public async Task<PropertyConstructionMapOutputDto> UpdatePropertyConstructionMapAsync(long id, PropertyConstructionMapInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("PropertyConstructionMapIdNotFound"));
            var entity = _repoPropertyConstructionMap.Get(id);
            if (entity==null)
                throw new UserFriendlyException(L("PropertyConstructionMapIdNotFound"));
            ValidateCreateOrUpdatePropertyConstruction(input);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoPropertyConstructionMap.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<PropertyConstructionMapOutputDto>(entity);
        }
        #endregion

        #region Private API
        private void ValidateCreateOrUpdatePropertyConstruction(PropertyConstructionMapInputDto input)
        {
            if(_repoProperty.Count(x=>x.Id==input.PropertyId)==0)
                throw new UserFriendlyException(L("PropertyIdNotFound"));
            if (_repoConstructionStatus.Count(x => x.Id == input.ConstructionStatusId) == 0)
                throw new UserFriendlyException(L("ConstructionStatusIdNotFound"));
        }
        #endregion
    }
}
