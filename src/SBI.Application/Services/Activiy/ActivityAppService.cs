﻿using CRM.Application.Shared.Activity;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using CRM.Entities.ActivityType;
using CRM.Entities.ActivityReminder;
using CRM.Application.Shared.ActivityAttendee;
using CRM.Application.Shared.ActivityReminder;
using System.Linq;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Abp.Collections.Extensions;
using Abp.Domain.Uow;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.ActivityUser;
using CRM.Application.Shared.ActivityOrganizationUnit;
using CRM.Entities.ActivityUser;
using CRM.Entities.ActivityOrganizationUnit;
using CRM.Application.Shared.ActivityType;
using Abp.Linq.Extensions;
using Abp.Authorization;
using CRM.Entities.ActivityAttendee;
using CRM.Entities.Activity;
using System.Linq.Dynamic.Core;
using CRM.Authorization.Users;
using System.Transactions;

namespace CRM.Services.Activiy
{
    [AbpAuthorize]
    public class ActivityAppService : SBIAppServiceBase, IActivityAppService
    {
        private readonly IRepository<Activity, long> _repositoryActivity;
        private readonly IRepository<User, long> _repositoryUser;
        private readonly IRepository<ActivityType, int> _repositoryActivityType;
        private readonly IRepository<ActivityReminder, long> _repositoryActivityReminder;
        private readonly IRepository<ActivityAttendee, long> _repositoryActivityAttendee;
        private readonly IRepository<ActivityOrganizationUnit, long> _repositoryActivityOrganizationUnit;
        private readonly IRepository<ActivityUser, long> _repositoryActivityUser;
        private readonly IRepository<Entities.Company.Company, long> _repositoryCompany;
        private readonly IRepository<Entities.Contacts.Contact, long> _repositoryContact;
        private readonly IRepository<Entities.CompanyContact.CompanyContact, long> _repositoryCompanyContact;
        private readonly IRepository<Entities.Projects.Project, long> _repositoryProject;
        private readonly IRepository<Entities.Requests.Request, long> _repositoryRequest;
        private readonly IRepository<Entities.Opportunity.Opportunity, long> _repositoryOpportunity;
        private readonly IRepository<Entities.Deal.Deal, long> _repositoryDeal;

        public ActivityAppService(
            IRepository<Activity, long> repositoryActivity
            , IRepository<ActivityType, int> repositoryActivityType
            , IRepository<ActivityReminder, long> repositoryActivityReminder
            , IRepository<ActivityAttendee, long> repositoryActivityAttendee
            , IRepository<ActivityOrganizationUnit, long> repositoryActivityOrganizationUnit
            , IRepository<ActivityUser, long> repositoryActivityUser
            , IRepository<Entities.Company.Company, long> repositoryCompany
            , IRepository<Entities.CompanyContact.CompanyContact, long> repositoryCompanyContact
            , IRepository<Entities.Contacts.Contact, long> repositoryContact
            , IRepository<Entities.Opportunity.Opportunity, long> repositoryOpportunity
            , IRepository<Entities.Projects.Project, long> repositoryProject
            , IRepository<Entities.Requests.Request, long> repositoryRequest
            , IRepository<Entities.Deal.Deal, long> repositoryDeal
            , IRepository<User, long> repositoryUser
            )
        {
            _repositoryActivity = repositoryActivity;
            _repositoryActivityType = repositoryActivityType;
            _repositoryActivityAttendee = repositoryActivityAttendee;
            _repositoryActivityReminder = repositoryActivityReminder;
            _repositoryActivityOrganizationUnit = repositoryActivityOrganizationUnit;
            _repositoryActivityUser = repositoryActivityUser;
            _repositoryCompany = repositoryCompany;
            _repositoryCompanyContact = repositoryCompanyContact;
            _repositoryContact = repositoryContact;
            _repositoryProject = repositoryProject;
            _repositoryRequest = repositoryRequest;
            _repositoryDeal = repositoryDeal;
            _repositoryUser = repositoryUser;
            _repositoryOpportunity = repositoryOpportunity;
        }

        public async Task<ActivityOutputDto> CreateActivityAsync(int moduleId, long referenceId, ActivityInputDto input)
        {
            ValidateCreateOrUpdateActivity(moduleId, referenceId, null, input);
            var entity = ObjectMapper.Map<Activity>(input);
            entity.ModuleId = moduleId;
            entity.ReferenceId = referenceId;
            entity.IsActive = true;
            entity.Id = await _repositoryActivity.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<ActivityOutputDto>(entity);
        }
        public async Task<ActivityOutputDto> CreateOrUpadteActivityFullAsync(int moduleId, long referenceId, ActivityFullInputDto input)
        {
            ValidateCreateOrUpdateActivityFull(moduleId, input);
            var entity = ObjectMapper.Map<Activity>(input);
            entity.ModuleId = moduleId;
            entity.ReferenceId = referenceId;
            entity.IsActive = true;
            entity.Id = await _repositoryActivity.InsertOrUpdateAndGetIdAsync(entity);
            var output = ObjectMapper.Map<ActivityOutputDto>(entity);
            if (input.ActivityOrganizationUnitIds != null)
            {
                output.ActivityOrganizationUnit = ObjectMapper.Map<List<ActivityOrganizationUnitOutputDto>>(CreateOrUpdateListActivityOrganizationUnitV1(output.Id, input.ActivityOrganizationUnitIds));
            }
            //if (input.ActivityReminder != null)
            //{
            //    output.ActivityReminder = ObjectMapper.Map<List<ActivityReminderOutputDto>>(CreateOrUpdateListActivityReminder(output.Id, input.ActivityReminder));
            //}
            if (input.ActivityUserIds != null)
            {
                output.ActivityUser = ObjectMapper.Map<List<ActivityUserOutputDto>>(CreateOrUpdateListActivityUserV1(entity.Id, input.ActivityUserIds));
            }
            return output;
        }
        public List<ActivityAttendeeOutputDto> CreateOrUpdateActivityAttendeeAsync(long id, List<ActivityAttendeeInputDto> input)
        {
            ValidateActivityListAttendee(id, input);
            var output = ObjectMapper.Map<List<ActivityAttendeeOutputDto>>(CreateOrUpdateListActivityAttendee(id, input));
            return output;
        }

        public List<ActivityReminderOutputDto> CreateOrUpdateActivityReminderAsync(long id, List<ActivityReminderInputDto> input)
        {
            ValidateActivityListReminder(id, input);
            var output = ObjectMapper.Map<List<ActivityReminderOutputDto>>(CreateOrUpdateListActivityReminder(id, input));
            return output;
        }

        public async Task<ActivityOutputDto> GetActivityDetails(long id)
        {
            var entity = await _repositoryActivity.GetAll().Where(x => x.Id == id)
                .Include(x => x.ActivityOrganizationUnit)
                    .ThenInclude(y => y.OrganizationUnit)
                .Include(x => x.ActivityReminder)
                    .ThenInclude(y => y.Format).Include(x => x.Type)
                .Include(x => x.ActivityUser)
                      .ThenInclude(y => y.User)
                .Include(x => x.Status)
                .Include(x => x.Module)
                .Include(x => x.Type)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskPriority)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser).SingleOrDefaultAsync();
            if (entity == null) throw new UserFriendlyException(L("NotExistActivity"));
            var output = ObjectMapper.Map<ActivityOutputDto>(entity);
            if (ActivityConstant.Company == entity.ModuleId)
            {
                output.ReferenceName = _repositoryCompany.Get(entity.ReferenceId).BusinessName;
            }
            if (ActivityConstant.Contact == entity.ModuleId)
            {
                output.ReferenceName = _repositoryContact.Get(entity.ReferenceId).ContactName;
            }
            if (ActivityConstant.Opportunity == entity.ModuleId)
            {
                output.ReferenceName = _repositoryOpportunity.Get(entity.ReferenceId).OpportunityName;
            }
            if (ActivityConstant.Project == entity.ModuleId)
            {
                output.ReferenceName = _repositoryProject.Get(entity.ReferenceId).ProjectName;
            }
            if (ActivityConstant.Request == entity.ModuleId)
            {
                output.ReferenceName = _repositoryRequest.Get(entity.ReferenceId).Id.ToString();
            }
            if (ActivityConstant.Deal == entity.ModuleId)
            {
                output.ReferenceName = _repositoryDeal.Get(entity.ReferenceId).DealName.ToString();
            }
            return output;
        }

        public async Task<PagedResultDto<ActivityOutputDto>> GetListActivityByModule(int moduleId, long referenceId, FilterBasicInputDto input)
        {

            var query = _repositoryActivity.GetAll().Where(x => x.IsActive == true)
                .Where(x => x.ModuleId == moduleId && x.ReferenceId == referenceId)
                .Include(x => x.ActivityOrganizationUnit).ThenInclude(y => y.OrganizationUnit)
                .Include(x => x.ActivityReminder).ThenInclude(y => y.Format).Include(x => x.Type)
                .Include(x => x.ActivityUser)
                    .ThenInclude(y => y.User)
                .Include(x => x.Module)
                .Include(x => x.Status)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskPriority)
                .Include(x => x.Type)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ActivityOutputDto>>(results);
            output.ForEach(x =>
            {
                if (ActivityConstant.Company == x.ModuleId)
                {
                    x.ReferenceName = _repositoryCompany.Get(x.ReferenceId).BusinessName;
                }
                if (ActivityConstant.Contact == x.ModuleId)
                {
                    x.ReferenceName = _repositoryContact.Get(x.ReferenceId).ContactName;
                }
                if (ActivityConstant.Opportunity == x.ModuleId)
                {
                    x.ReferenceName = _repositoryOpportunity.Get(x.ReferenceId).OpportunityName;
                }
                if (ActivityConstant.Project == x.ModuleId)
                {
                    x.ReferenceName = _repositoryProject.Get(x.ReferenceId).ProjectName;
                }
                if (ActivityConstant.Request == x.ModuleId)
                {
                    x.ReferenceName = _repositoryRequest.Get(x.ReferenceId).Id.ToString();
                }
                if (ActivityConstant.Deal == x.ModuleId)
                {
                    x.ReferenceName = _repositoryDeal.Get(x.ReferenceId).DealName.ToString();
                }
            }
           );
            return new PagedResultDto<ActivityOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<ActivityOutputDto>> GetListActivityOfCompany(long companyId)
        {
            var contactIds = _repositoryCompanyContact.GetAll().Where(x => x.CompanyId == companyId && x.IsActive == true)
                .Select(x => x.ContactId).ToList();
            var opportunityIds = _repositoryOpportunity.GetAll().Where(x => x.CompanyId == companyId && x.IsActive == true)
               .Select(x => x.Id).ToList();

            var query = _repositoryActivity.GetAll().Where(x => x.IsActive == true)
                .Where(x => (x.ModuleId == ActivityConstant.Contact && contactIds.Any(y => y == x.ReferenceId))
                || (x.ModuleId == ActivityConstant.Opportunity && opportunityIds.Any(z => z == x.ReferenceId))
                || (x.ModuleId == ActivityConstant.Company && x.ReferenceId== companyId)
                )
                 .Include(x => x.ActivityOrganizationUnit)
                    .ThenInclude(y => y.OrganizationUnit)
                .Include(x => x.ActivityReminder).ThenInclude(y => y.Format).Include(x => x.Type)
                .Include(x => x.ActivityUser)
                       .ThenInclude(y => y.User)
                .Include(x => x.Module)
                .Include(x => x.Status)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskPriority)
                .Include(x => x.Type)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = await query.CountAsync();
            var results = await query
                .ToListAsync();
            var output = ObjectMapper.Map<List<ActivityOutputDto>>(results);
            output.ForEach(x =>
            {
                if (ActivityConstant.Contact == x.ModuleId)
                {
                    x.ReferenceName = _repositoryContact.Get(x.ReferenceId).ContactName;
                }
                if (ActivityConstant.Opportunity == x.ModuleId)
                {
                    x.ReferenceName = _repositoryOpportunity.Get(x.ReferenceId).OpportunityName;
                }
            }
           );
            return new PagedResultDto<ActivityOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<ActivityOutputDto>> GetListActivityContactOfCompany(ActivityFitlerDto input)
        {
            var contactIds = _repositoryCompanyContact.GetAll().Where(x => x.CompanyId == input.CompanyId && x.IsActive == true)
                .Select(x => x.ContactId).ToList();
            var opportunityIds = _repositoryOpportunity.GetAll().Where(x => x.CompanyId == input.CompanyId && x.IsActive == true)
               .Select(x => x.Id).ToList();
            var query = _repositoryActivity.GetAll().Where(x=>x.IsActive==true)
                .Where(x => (x.ModuleId == ActivityConstant.Contact && contactIds.Any(y => y == x.ReferenceId))
                || (x.ModuleId == ActivityConstant.Opportunity && opportunityIds.Any(z => z == x.ReferenceId)))
                 .Include(x => x.ActivityOrganizationUnit)
                    .ThenInclude(y => y.OrganizationUnit)
                .Include(x => x.ActivityReminder).ThenInclude(y => y.Format).Include(x => x.Type)
                .Include(x => x.ActivityUser)
                       .ThenInclude(y => y.User)
                .Include(x => x.Module)
                .Include(x => x.Status)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskPriority)
                .Include(x => x.Type)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = await query.CountAsync();
            var results = await query
                .ToListAsync();
            var output = ObjectMapper.Map<List<ActivityOutputDto>>(results);
            output.ForEach(x =>
            {
                if (ActivityConstant.Contact == x.ModuleId)
                {
                    x.ReferenceName = _repositoryContact.Get(x.ReferenceId).ContactName;
                }
                if (ActivityConstant.Opportunity == x.ModuleId)
                {
                    x.ReferenceName = _repositoryOpportunity.Get(x.ReferenceId).OpportunityName;
                }
            }
           );
            return new PagedResultDto<ActivityOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ActivityOutputDto>> GetListActivityByType(ActivityFitlerDto input)
        {

            var query = _repositoryActivity.GetAll().Where(x => x.IsActive == true)
                .Where(x => input.TypeIds.Any(y => x.TypeId == y))
                .WhereIf(input.DateTo.HasValue && input.DateFrom.HasValue, y => input.DateFrom.Value.Date <= y.DateStart && y.DateStart.Value.Date <= input.DateTo)
                 .Include(x => x.ActivityOrganizationUnit)
                    .ThenInclude(y => y.OrganizationUnit)
                .Include(x => x.ActivityReminder).ThenInclude(y => y.Format).Include(x => x.Type)
                .Include(x => x.ActivityUser)
                       .ThenInclude(y => y.User)
                .Include(x => x.Module)
                .Include(x => x.Status)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskPriority)
                .Include(x => x.Type)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword), x => x.ActivityName.ToUpper().Contains(input.Keyword.ToUpper())
                || x.Description.ToUpper().Contains(input.Keyword.ToUpper())
                || x.CreatorUser.UserName.ToUpper().Contains(input.Keyword.ToUpper()))
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = await query.CountAsync();
            List<ActivityOutputDto> output = new List<ActivityOutputDto>();
            if (input.PageSize.HasValue)
            {
                var results = await query
               .Skip((input.PageNumber.Value - 1) * input.PageSize.Value)
               .Take(input.PageSize.Value)
               .ToListAsync();
                output = ObjectMapper.Map<List<ActivityOutputDto>>(results);
            }
            else
            {
                var results = await query
                    .ToListAsync();
                output = ObjectMapper.Map<List<ActivityOutputDto>>(results);
            }
            output.ForEach(x =>
            {
                if (ActivityConstant.Company == x.ModuleId)
                {
                    x.ReferenceName = _repositoryCompany.Get(x.ReferenceId).BusinessName;
                }
                if (ActivityConstant.Contact == x.ModuleId)
                {
                    x.ReferenceName = _repositoryContact.Get(x.ReferenceId).ContactName;
                }
                if (ActivityConstant.Opportunity == x.ModuleId)
                {
                    x.ReferenceName = _repositoryOpportunity.Get(x.ReferenceId).OpportunityName;
                }
                if (ActivityConstant.Project == x.ModuleId)
                {
                    x.ReferenceName = _repositoryProject.Get(x.ReferenceId).ProjectName;
                }
                if (ActivityConstant.Request == x.ModuleId)
                {
                    x.ReferenceName = _repositoryRequest.Get(x.ReferenceId).Id.ToString();
                }
                if (ActivityConstant.Deal == x.ModuleId)
                {
                    x.ReferenceName = _repositoryDeal.Get(x.ReferenceId).DealName.ToString();
                }
            }
             );
            return new PagedResultDto<ActivityOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ActivityAttendeeOutputDto>> GetListActivityAttendee(long activityId, FilterBasicInputDto input)
        {

            var query = _repositoryActivityAttendee.GetAll()
                .Where(x => x.ActivityId == activityId)
                .Include(x => x.Module)
                .Include(x => x.Status)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ActivityAttendeeOutputDto>>(results);
            output.ForEach(x =>
          {
              if (ActivityConstant.AttUser == x.ModuleId)
              {
                  var user = _repositoryUser.Get(x.ReferenceId);
                  x.ReferenceName = user.Surname + " " + user.Name;
              }
              if (ActivityConstant.AttContact == x.ModuleId)
              {
                  x.ReferenceName = _repositoryContact.Get(x.ReferenceId).ContactName;
              }
          }
            );
            return new PagedResultDto<ActivityAttendeeOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ActivityReminderOutputDto>> GetListActivityReminder(long activityId, FilterBasicInputDto input)
        {

            var query = _repositoryActivityReminder.GetAll()
                .Where(x => x.ActivityId == activityId)
                .Include(x => x.Module)
                .Include(x => x.Format)
                .Include(x => x.Type)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ActivityReminderOutputDto>>(results);
            output.ForEach(x =>
            {
                if (ActivityConstant.Company == x.ModuleId)
                {
                    x.ReferenceName = _repositoryCompany.Get(x.ReferenceId).BusinessName;
                }
                if (ActivityConstant.Contact == x.ModuleId)
                {
                    x.ReferenceName = _repositoryContact.Get(x.ReferenceId).ContactName;
                }
                if (ActivityConstant.Opportunity == x.ModuleId)
                {
                    x.ReferenceName = _repositoryOpportunity.Get(x.ReferenceId).OpportunityName;
                }
                if (ActivityConstant.Project == x.ModuleId)
                {
                    x.ReferenceName = _repositoryProject.Get(x.ReferenceId).ProjectName;
                }

                if (ActivityConstant.Request == x.ModuleId)
                {
                    x.ReferenceName = _repositoryRequest.Get(x.ReferenceId).Id.ToString();
                }
                if (ActivityConstant.Deal == x.ModuleId)
                {
                    x.ReferenceName = _repositoryDeal.Get(x.ReferenceId).DealName.ToString();
                }
            }
           );
            return new PagedResultDto<ActivityReminderOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<ActivityOutputDto> UpdateActivityAsync(long id, ActivityInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("NotExistActivity"));
            ValidateCreateOrUpdateActivity(null, null, id, input);
            var entity = _repositoryActivity.Get(id);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repositoryActivity.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<ActivityOutputDto>(entity);
        }

        public async Task<ActivityOutputDto> DeactiveActivity(long id)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("NotExistActivity"));
            var entity = _repositoryActivity.Get(id);
            entity.IsActive = false;
            await _repositoryActivity.UpdateAsync(entity);
            return ObjectMapper.Map<ActivityOutputDto>(entity);
        }

        public List<ActivityUserOutputDto> CreateOrUpdateActivityUser(long activityId, List<ActivityUserInputDto> activityUsers)
        {
            ValidateActivityListUser(activityId, activityUsers);
            var output = ObjectMapper.Map<List<ActivityUserOutputDto>>(CreateOrUpdateListActivityUser(activityId, activityUsers));
            return output;
        }

        public List<ActivityOrganizationUnitOutputDto> CreateOrUpdateActivityOrganizationUnit(long activityId, List<ActivityOrganizationUnitInputDto> activityActivityOrganizationUnits)
        {
            ValidateActivityListOrganizationUnit(activityId, activityActivityOrganizationUnits);
            var output = ObjectMapper.Map<List<ActivityOrganizationUnitOutputDto>>(CreateOrUpdateListActivityOrganizationUnit(activityId, activityActivityOrganizationUnits));
            return output;
        }

        public async Task<List<ActivityTypeOutputDto>> GetListActivityType(string keyword)
        {

            var query = _repositoryActivityType.GetAll()
                  .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.TypeCode.ToUpper().Contains(keyword.ToUpper()));

            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ActivityTypeOutputDto>>(results);

        }

        public async Task<PagedResultDto<ActivityOrganizationUnitOutputDto>> GetListActivityOrganizationUnit(FilterBasicInputDto input, long activityId)
        {
            var query = _repositoryActivityOrganizationUnit.GetAll()
                .Include(x => x.OrganizationUnit)
                .Where(x => x.ActivityId == activityId)
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = query.Count();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ActivityOrganizationUnitOutputDto>>(results);
            return new PagedResultDto<ActivityOrganizationUnitOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ActivityUserOutputDto>> GetListActivityUser(FilterBasicInputDto input, long activityId)
        {
            var query = _repositoryActivityUser.GetAll()
                .Include(x => x.User)
                .Where(x => x.ActivityId == activityId)
                .OrderByDescending(x => x.LastModificationTime)
                .OrderByDescending(x => x.CreationTime);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ActivityUserOutputDto>>(results);
            return new PagedResultDto<ActivityUserOutputDto>(
                totalCount,
                output
            );
        }

        #region Action
        private List<ActivityAttendee> CreateOrUpdateListActivityAttendee(long activityId, List<ActivityAttendeeInputDto> input)
        {

            var entities = new List<ActivityAttendee>();
            input.ForEach(x =>
            {
                entities.Add(CreateOrUpdateActivityAttendee(activityId, x));
            });
            return entities;
        }
        private List<ActivityReminder> CreateOrUpdateListActivityReminder(long activityId, List<ActivityReminderInputDto> input)
        {

            var entities = new List<ActivityReminder>();
            input.ForEach(x =>
            {
                entities.Add(CreateOrUpdateActivityReminder(activityId, x));
            });
            return entities;
        }
        private List<ActivityUser> CreateOrUpdateListActivityUser(long activityId, List<ActivityUserInputDto> activityUsers)
        {
            var entities = new List<ActivityUser>();
            activityUsers.ForEach(x =>
            {
                entities.Add(CreateOrUpdateActivityUser(activityId, x));
            });
            return entities;
        }
        public List<ActivityUserOutputDto> CreateOrUpdateListActivityUserV1(long activityId, List<long> userIds)
        {

            var currentActivityUser = _repositoryActivityUser.GetAll().Where(x => x.ActivityId == activityId).ToList();
            userIds.ForEach(x =>
            {
                var objUpdate = currentActivityUser.FirstOrDefault(obj => obj.UserId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryActivityUser.Update(objUpdate);
                    return;
                }
                var entity = new ActivityUser()
                {
                    ActivityId = activityId,
                    UserId = x,
                    IsActive = true
                };
                _repositoryActivityUser.Insert(entity);
            });
            var deleteEntities = currentActivityUser.Where(x => userIds.All(y => y != x.UserId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryActivityUser.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listActivityUser = _repositoryActivityUser.GetAll().Where(x => x.ActivityId == activityId && x.IsActive).Include(x => x.User).ToList();
            return ObjectMapper.Map<List<ActivityUserOutputDto>>(listActivityUser);
        }
        private List<ActivityOrganizationUnit> CreateOrUpdateListActivityOrganizationUnit(long activityId, List<ActivityOrganizationUnitInputDto> activityOrganizationUnits)
        {

            var entities = new List<ActivityOrganizationUnit>();
            activityOrganizationUnits.ForEach(x =>
            {
                entities.Add(CreateOrUpdateActivityOrganizationUnit(activityId, x));
            });
            return entities;
        }
        public List<ActivityOrganizationUnitOutputDto> CreateOrUpdateListActivityOrganizationUnitV1(long activityId, List<long> organizationUnitIds)
        {

            var currentActivityOrganizationUnit = _repositoryActivityOrganizationUnit.GetAll().Where(x => x.ActivityId == activityId).ToList();
            organizationUnitIds.ForEach(x =>
            {
                var objUpdate = currentActivityOrganizationUnit.FirstOrDefault(obj => obj.OrganizationUnitId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryActivityOrganizationUnit.Update(objUpdate);
                    return;
                }
                var entity = new ActivityOrganizationUnit()
                {
                    ActivityId = activityId,
                    OrganizationUnitId = x,
                    IsActive = true
                };
                _repositoryActivityOrganizationUnit.Insert(entity);
            });
            var deleteEntities = currentActivityOrganizationUnit.Where(x => organizationUnitIds.All(y => y != x.OrganizationUnitId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryActivityOrganizationUnit.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listActivityOrganizationUnit = _repositoryActivityOrganizationUnit.GetAll().Where(x => x.ActivityId == activityId && x.IsActive).Include(x => x.OrganizationUnit).ToList();
            return ObjectMapper.Map<List<ActivityOrganizationUnitOutputDto>>(listActivityOrganizationUnit);
        }
        #endregion

        #region Validate
        public void ValidateCreateOrUpdateActivity(int? moduleId, long? referenceId, long? activityId, ActivityInputDto input)
        {
            if (input.StatusId.HasValue)
            {
                if (!_repositoryActivityType.GetAll().Any(x => x.Id == input.StatusId && x.TypeCode.ToUpper() == ActivityConstant.ActivityStatus))
                    throw new UserFriendlyException(L("NotExistStatus"));
            }
            if (string.IsNullOrEmpty(input.Description))
            {
                throw new UserFriendlyException(L("InputDescription"));
            }
            if (string.IsNullOrEmpty(input.ActivityName))
            {
                throw new UserFriendlyException(L("InputActivityName"));
            }
            //udpate activity
            if (activityId.HasValue)
            {
                if (!_repositoryActivity.GetAll().Any(x => x.Id == activityId))
                    throw new UserFriendlyException(L("NotExistActivity"));
            }
            //create activity
            else
            {
                if (!_repositoryActivityType.GetAll().Any(x => x.Id == moduleId && x.TypeCode.ToUpper() == ActivityConstant.Module))
                    throw new UserFriendlyException(L("NotExistModule"));
            }
        }
        public void ValidateCreateOrUpdateActivityFull(int? moduleId, ActivityFullInputDto input)
        {
            if (input.StatusId.HasValue)
            {
                if (!_repositoryActivityType.GetAll().Any(x => x.Id == input.StatusId && input.StatusId != 0 && x.TypeCode.ToUpper() == ActivityConstant.ActivityStatus))
                    throw new UserFriendlyException(L("NotExistStatus"));
            }
            if (string.IsNullOrEmpty(input.Description))
            {
                throw new UserFriendlyException(L("InputDescription"));
            }
            if (string.IsNullOrEmpty(input.ActivityName))
            {
                throw new UserFriendlyException(L("InputActivityName"));
            }
            //udpate activity
            if (input.Id.HasValue)
            {
                if (!_repositoryActivity.GetAll().Any(x => x.Id == input.Id.Value))
                    throw new UserFriendlyException(L("NotExistActivity"));
            }
            //create activity
            else
            {
                if (!_repositoryActivityType.GetAll().Any(x => x.Id == moduleId && x.TypeCode.ToUpper() == ActivityConstant.Module))
                    throw new UserFriendlyException(L("NotExistModule"));
            }
        }
        public void ValidateActivityListAttendee(long id, List<ActivityAttendeeInputDto> input)
        {
            input.ForEach(x => ValidateActivityAttendee(id, x));
        }
        public void ValidateActivityAttendee(long activityId, ActivityAttendeeInputDto input)
        {
            if (input.Id.HasValue)
            {
                if (!_repositoryActivityAttendee.GetAll().Any(x => x.Id == input.Id))
                    throw new UserFriendlyException(L("NotExistActivityAttendee"));
            }

            if (!_repositoryActivity.GetAll().Any(x => x.Id == activityId))
                throw new UserFriendlyException(L("NotExistActivity"));
            if (!_repositoryActivityType.GetAll().Any(x => x.Id == input.ModuleId && x.TypeCode.ToUpper() == ActivityConstant.AttendeeModule))
                throw new UserFriendlyException(L("NotExistAttendeeModule"));
            if (!_repositoryActivityType.GetAll().Any(x => x.Id == input.StatusId && x.TypeCode.ToUpper() == ActivityConstant.AttendeeStatus))
                throw new UserFriendlyException(L("NotExistAttendeeStatus"));

        }
        public void ValidateActivityListReminder(long id, List<ActivityReminderInputDto> input)
        {
            input.ForEach(x => ValidateActivityReminder(id, x));
        }
        public void ValidateActivityReminder(long activityId, ActivityReminderInputDto input)
        {
            if (input.Id.HasValue)
            {
                if (!_repositoryActivityReminder.GetAll().Any(x => x.Id == input.Id))
                    throw new UserFriendlyException(L("NotExistActivityReminder"));
            }
            if (!_repositoryActivity.GetAll().Any(x => x.Id == activityId))
                throw new UserFriendlyException(L("NotExistActivity"));
            if (!_repositoryActivityType.GetAll().Any(x => x.Id == input.ModuleId && x.TypeCode.ToUpper() == ActivityConstant.Module))
                throw new UserFriendlyException(L("NotExistReminderModule"));
            if (!_repositoryActivityType.GetAll().Any(x => x.Id == input.TypeId && x.TypeCode.ToUpper() == ActivityConstant.ReminderType))
                throw new UserFriendlyException(L("NotExistReminderType"));
            if (!_repositoryActivityType.GetAll().Any(x => x.Id == input.FormatId && x.TypeCode.ToUpper() == ActivityConstant.ReminderFormat))
                throw new UserFriendlyException(L("NotExistReminderFormat"));

        }
        private void ValidateActivityListUser(long activityId, List<ActivityUserInputDto> activityUsers)
        {
            activityUsers.ForEach(x => ValidateActivityUser(activityId, x));
        }
        private void ValidateActivityUser(long activityId, ActivityUserInputDto activityUser)
        {
            if (activityUser.Id.HasValue)
            {
                if (!_repositoryActivityUser.GetAll().Any(x => x.Id == activityUser.Id.Value))
                    throw new UserFriendlyException(L("ActivityUserIdNotFound"));
            }
            else
            {
                if (_repositoryActivityUser.GetAll().Any(x => x.ActivityId == activityId && x.UserId == activityUser.UserId && x.IsFollwer == activityUser.IsFollwer))
                    throw new UserFriendlyException(L("ExistActivityUser"));
            }
        }
        private void ValidateActivityListOrganizationUnit(long activityId, List<ActivityOrganizationUnitInputDto> activityOrganizationUnits)
        {
            activityOrganizationUnits.ForEach(x => ValidateActivityOrganizationUnit(activityId, x));
        }
        private void ValidateActivityOrganizationUnit(long activityId, ActivityOrganizationUnitInputDto activityOrganizationUnit)
        {
            if (activityOrganizationUnit.Id.HasValue)
            {
                if (!_repositoryActivityOrganizationUnit.GetAll().Any(x => x.Id == activityOrganizationUnit.Id.Value))
                    throw new UserFriendlyException(L("ActivityOrganizationIdNotFound"));
            }
            else
            {
                if (_repositoryActivityOrganizationUnit.GetAll().Any(x => x.ActivityId == activityId && x.OrganizationUnitId == activityOrganizationUnit.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistActivityOrganizationUnit"));
            }
        }
        #endregion

        #region Private API
        [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        private ActivityAttendee CreateOrUpdateActivityAttendee(long activityId, ActivityAttendeeInputDto input)
        {
            ActivityAttendee activityAttendee = new ActivityAttendee();
            if (input.Id != null)
            {
                var entity = _repositoryActivityAttendee.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                activityAttendee = ObjectMapper.Map(input, entity);
            }
            else
            {
                activityAttendee = ObjectMapper.Map<ActivityAttendee>(input);
            }
            activityAttendee.ActivityId = activityId;
            activityAttendee.Id = _repositoryActivityAttendee.InsertOrUpdateAndGetId(activityAttendee);
            return activityAttendee;
        }
        private ActivityReminder CreateOrUpdateActivityReminder(long activityId, ActivityReminderInputDto input)
        {
            ActivityReminder activityReminder = new ActivityReminder();
            if (input.Id != null && input.Id > 0)
            {
                var entity = _repositoryActivityReminder.Single(x => x.Id == input.Id);
                activityReminder = ObjectMapper.Map(input, entity);
            }
            else
            {
                activityReminder = ObjectMapper.Map<ActivityReminder>(input);
            }
            activityReminder.ActivityId = activityId;
            activityReminder.Id = _repositoryActivityReminder.InsertOrUpdateAndGetId(activityReminder);
            return activityReminder;
        }
        [UnitOfWork]
        private ActivityUser CreateOrUpdateActivityUser(long activityId, ActivityUserInputDto input)
        {
            ActivityUser entity = new ActivityUser();
            if (input.Id != null)
            {
                var entityUser = _repositoryActivityUser.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityUser);
            }
            else
            {
                entity = ObjectMapper.Map<ActivityUser>(input);
            }
            entity.ActivityId = activityId;
            entity.Id = _repositoryActivityUser.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        private ActivityOrganizationUnit CreateOrUpdateActivityOrganizationUnit(long activityId, ActivityOrganizationUnitInputDto input)
        {
            ActivityOrganizationUnit entity = new ActivityOrganizationUnit();
            if (input.Id != null)
            {
                var entityOrganizationUnit = _repositoryActivityOrganizationUnit.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityOrganizationUnit);
            }
            else
            {
                entity = ObjectMapper.Map<ActivityOrganizationUnit>(input);
            }
            entity.ActivityId = activityId;
            entity.Id = _repositoryActivityOrganizationUnit.InsertOrUpdateAndGetId(entity);
            return entity;
        }


        #endregion
    }
}
