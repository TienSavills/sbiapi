﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Activiy
{
    public static class ActivityConstant
    {
        public const string Module = "MODULE";
        public const string ActivityStatus = "ACTIVITYSTATUS";
        public const string TaskPriority = "TASKPRIORITY";
        public const string ReminderType = "REMINDERTYPE";
        public const string ReminderFormat = "REMINDERFORMAT";
        public const string AttendeeStatus = "ATTENDEESTATUS";
        public const string AttendeeModule = "MODULEATTENDEE";
        public const int Company = 7;
        public const int Contact = 8;
        public const int Opportunity = 9;
        public const int Project = 25;
        public const int Request = 26;
        public const int AttUser = 27;
        public const int AttContact = 28;
        public const int Listing = 29;
        public const int Deal = 33;

    }
}
