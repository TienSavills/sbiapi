﻿using Abp.Authorization;
using CRM;
using CRM.Application.Shared.Opportunity;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using CRM.SBIMap;
using Abp.Domain.Repositories;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using Dapper;
using System.Transactions;
using CRM.EntitiesCustom;
using System.Linq;
using Abp.UI;
using CRM.Application.Shared.OpportunityProperty;
using CRM.Application.Shared.OpportunityInstruction;
using CRM.Application.Shared.OpportunityAssetClass;
using CRM.Application.Shared.OpportunityOrganizationUnit;
using CRM.Application.Shared.OpportunityUser;
using Abp.Domain.Uow;
using CRM.Entities.OpportunityUser;
using CRM.Entities.OpportunityOrganizationUnit;
using CRM.Entities.OpportunityAssetClass;
using CRM.Entities.OpportunityInstruction;
using CRM.Entities.OpportunityProperty;
using Microsoft.EntityFrameworkCore;
using CRM.Entities.Currencys;
using CRM.Entities.OpportunityContact;
using CRM.Application.Shared.OpportunityContact;
using CRM.Entities.OpportuityCategory;
using CRM.Entities.DealOpportunity;
using CRM.Entities.AssetClass;
using CRM.Entities.Instruction;
using CRM.Entities.Industry;
using CRM.Entities.LeadSource;
using CRM.Entities.ClientType;
using CRM.Entities.OpportunityLeadUser;
using CRM.Application.Shared.OpportunityLeadUser;
using Abp.AppFactory.Interfaces;
using Abp.AppFactory.SendGrid.Email;
using System.Net;
using CRM.EmailHelper;
using CRM.Entities.Comment;
using SendGrid.Helpers.Mail;
using CRM.Configuration;
using CRM.Application.Shared.OpportunityProject;
using CRM.Entities.OpportunityProject;
using CRM.ExportHelper;
using Microsoft.AspNetCore.Hosting;
using CRM.Email;
using CRM.Services.Organizations;
using System.Data;
using Abp.Organizations;

//using CRM.ExportHelper;

namespace CRM.Services.Opportunity
{
    [AbpAuthorize]
    public class OpportunityAppService : SBIAppServiceBase, IOpportunityAppService
    {
        private readonly ConfigSetting _options;
        private readonly OrganizationUnitAppService _servicesOrganizationUnit;
        private readonly IEmailSenderHelper _emailSenderHelper;
        private readonly IRepository<Entities.Opportunity.Opportunity, long> _repoOpportunity;
        private readonly IRepository<OpportunityUser, long> _repoOpportunityUser;
        private readonly IRepository<OpportunityLead, long> _repoOpportunityLead;
        private readonly IRepository<OpportunityOrganizationUnit, long> _repoOpportunityOrganizationUnit;
        private readonly IRepository<OpportunityAssetClass, long> _repoOpportunityAssetClass;
        private readonly IRepository<OpportunityInstruction, long> _repoOpportunityInstruction;
        private readonly IRepository<OpportunityContact, long> _repoOpportunityContact;
        private readonly IRepository<OpportunityProperty, long> _repoOpportunityProperty;
        private readonly IRepository<Entities.Properties.Property, long> _repoProperty;
        private readonly IRepository<OpportunityProject, long> _repoOpportunityProject;
        private readonly IRepository<Entities.Projects.Project, long> _repoProject;
        private readonly IRepository<Entities.Company.Company, long> _repoCompany;
        private readonly IRepository<Currency, int> _repoCurrency;
        private readonly IRepository<Entities.Requests.Request, long> _repoRequest;
        private readonly IRepository<OpportunityCategory, int> _repoOpportunityCategory;
        private readonly IRepository<DealOpportunity, long> _repoDealOpportunity;
        private readonly IRepository<Entities.Deal.Deal, long> _repoDeal;
        private readonly IRepository<Entities.DealShare.DealShare, long> _repoDealShare;
        private readonly IRepository<AssetClass, int> _repoAssetClass;
        private readonly IRepository<Instruction, int> _repoInstruction;
        private readonly IRepository<Industry, int> _repoIndustry;
        private readonly IRepository<LeadSource, int> _repoLeadSource;
        private readonly IRepository<ClientType, int> _repoClientType;
        private readonly ISendGrid _sendGrid;
        private readonly IExportHelper _exportHelper;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly IRepository<Comment, long> _repoComment;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IConfigHelper _configHelper;

        public OpportunityAppService(
             IEmailSenderHelper emailSenderHelper
             , IRepository<Entities.Opportunity.Opportunity, long> repoOpportunity
             , OrganizationUnitAppService servicesOrganizationUnit
            , IRepository<OpportunityUser, long> repoOpportunityUser
            , IRepository<OpportunityOrganizationUnit, long> repoOpportunityOrganizationUnit
            , IRepository<OpportunityAssetClass, long> repoOpportunityAssetClass
            , IRepository<OpportunityInstruction, long> repoOpportunityInstruction
            , IRepository<OpportunityContact, long> repoOpportunityContact
            , IRepository<OpportunityProperty, long> repoOpportunityProperty
            , IRepository<OpportunityProject, long> repoOpportunityProject
            , IRepository<OpportunityCategory, int> repoOpportunityCategory
            , IRepository<Entities.Properties.Property, long> repoProperty
            , IRepository<Entities.DealShare.DealShare, long> repoDealShare
            , IRepository<Entities.Projects.Project, long> repoProject
            , IRepository<Entities.Deal.Deal, long> repoDeal
            , IRepository<Entities.Company.Company, long> repoCompany
            , IRepository<Currency, int> repoCurrency
            , IRepository<OpportunityLead, long> repoOpportunityLead
            , IRepository<Entities.Requests.Request, long> repoRequest
            , IRepository<DealOpportunity, long> repoDealOpportunity
            , IRepository<AssetClass, int> repoAssetClass
            , IRepository<Instruction, int> repoInstruction
            , IRepository<Industry, int> repoIndustry
            , IRepository<LeadSource, int> repoLeadSource
            , IRepository<ClientType, int> repoClientType
            , IEmailTemplateProvider emailTemplateProvider
            , IRepository<Comment, long> repoComment
            , ISendGrid sendGrid
            , IExportHelper exportHelper
            , IConfigHelper configHelper
            , IHostingEnvironment hostingEnvironment
             , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IOptions<ConfigSetting> options
            )
        {
            _repoOpportunity = repoOpportunity;
            _repoOpportunityUser = repoOpportunityUser;
            _repoOpportunityOrganizationUnit = repoOpportunityOrganizationUnit;
            _repoOpportunityAssetClass = repoOpportunityAssetClass;
            _repoOpportunityInstruction = repoOpportunityInstruction;
            _repoOpportunityProperty = repoOpportunityProperty;
            _repoOpportunityProject = repoOpportunityProject;
            _repoDealShare = repoDealShare;
            _repoOpportunityCategory = repoOpportunityCategory;
            _repoProperty = repoProperty;
            _repoProject = repoProject;
            _repoOpportunityLead = repoOpportunityLead;
            _repoCompany = repoCompany;
            _repoCurrency = repoCurrency;
            _repoRequest = repoRequest;
            _repoOpportunityContact = repoOpportunityContact;
            _repoDealOpportunity = repoDealOpportunity;
            _repoAssetClass = repoAssetClass;
            _repoInstruction = repoInstruction;
            _repoIndustry = repoIndustry;
            _repoLeadSource = repoLeadSource;
            _repoClientType = repoClientType;
            _sendGrid = sendGrid;
            _configHelper = configHelper;
            _repoComment = repoComment;
            _emailTemplateProvider = emailTemplateProvider;
            _exportHelper = exportHelper;
            _options = options.Value;
            _hostingEnvironment = hostingEnvironment;
            _emailSenderHelper = emailSenderHelper;
            _servicesOrganizationUnit = servicesOrganizationUnit;
            _organizationUnitRepository = organizationUnitRepository;
            _repoDeal = repoDeal;
        }
        public async Task<OpportunityOutputDto> CreateOpportunityAsync(OpportunityInputDto input)
        {
            var output = await CreateOrUpdateOpportunityAsync(null, input);
            return output;
        }
        public async Task<OpportunityOutputDto> CreateOrOpportunityFullAsync(OpportunityFullInputDto input)
        {
            var output = await CreateOrUpdateOpportunityFullAsync(input);
            return output;
        }

        public async Task<PagedResultDto<OpportunityOutputDto>> Filter(string input)
        {
            return await FilterOpportunityGroupDepartment(input, null);
        }

        public async Task<PagedResultDto<OpportunityOutputDto>> FilterOpportunityAdvisory(string input)
        {
            var departmentIds = await GetDepartmentIds(new List<string>(new string[]
             {
                OpportunityConstant.HcmcBcs, OpportunityConstant.HcmcFs, OpportunityConstant.HcmcRes,
                OpportunityConstant.HnBcs, OpportunityConstant.HnFs, OpportunityConstant.HnRes
             }));
            return await FilterOpportunityGroupDepartment(input, departmentIds);
        }

        public async Task<PagedResultDto<OpportunityOutputDto>> FilterOpportunityCommercial(string input)
        {
            var departmentIds = await GetDepartmentIds(new List<string>(new string[]
            {
                OpportunityConstant.HcmcIndustrial, OpportunityConstant.HnIndustrial
                , OpportunityConstant.HcmcPm, OpportunityConstant.HnPm
                , OpportunityConstant.HcmcBd, OpportunityConstant.HnBd
                , OpportunityConstant.HcmcInvestment, OpportunityConstant.HnInvestment
                , OpportunityConstant.HcmcSale, OpportunityConstant.HnSale
                , OpportunityConstant.HcmcLeasing, OpportunityConstant.HnLeasing
                , OpportunityConstant.HcmcRetail, OpportunityConstant.HnRetail
            }));
            return await FilterOpportunityGroupDepartment(input, departmentIds);
        }

        private async Task<PagedResultDto<OpportunityOutputDto>> FilterOpportunityGroupDepartment(string input,
            string groupDepartmentIds)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<OpportunityFilterDto>("SpFilterOpportunity",
                    new
                    {
                        @FilterJson = input
                        ,
                        @GroupDepartmentIds = groupDepartmentIds
                        ,
                        @UserId = AbpSession.UserId.Value

                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<OpportunityOutputDto>>(output);
                return new PagedResultDto<OpportunityOutputDto>(totalCount, results);
            }
        }

        private async Task<string> GetDepartmentIds(List<string> groups)
        {
            var departmentIds = await _organizationUnitRepository.GetAll().Where(x => groups.Any(y => x.Code == y)).Select(y => y.Id).ToListAsync();
            return string.Join(",", departmentIds);
        }

        public async Task<Attachment> GenerateOpportunityFile(DateTime? dateFrom, DateTime? dateTo, int? officeId, string departmentIds, string statusIds)
        {
            //check permission
            var ou = await _servicesOrganizationUnit.GetOrganizationUnitsByUser(AbpSession.UserId.Value);
            if (!ou.Items.Any(x => x.Id == Organizations.OrganizationsConstant.Manager))
            {
                throw new UserFriendlyException(L("ContactSBITeam"));
            }
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ExportOpportunityOutput>("SpWeeklyReport",
                  new
                  {
                      @UserId = AbpSession.UserId.Value,
                      @DateFrom = dateFrom.Value,
                      @DateTo = dateTo.Value,
                      @OfficeId = officeId,
                      @DepartmentIds = departmentIds,
                      @StatusIds = statusIds
                  }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ExportOpportunityOutput>>(output);
                var dt = GenarateColumnOpportunity();
                var bidingData = GenarateDataOpportunity(dt, results);
                var filePath = string.Format($"{ _hostingEnvironment.ContentRootPath} { @"\CRM.EmailTemplate\ExportOpportunity.xlsx"}");
                var wbContent = _exportHelper.GetWorkbookContent(dt, 1, 3);
                var memoryFile = _exportHelper.ExportWorkbookFile(wbContent, filePath);
                var attachment = new SendGrid.Helpers.Mail.Attachment()
                {
                    Content = Convert.ToBase64String(memoryFile.ToArray()),
                    Filename = "ReportOpportunity.xls",
                    Type = "application/octet-stream"
                };
                return attachment;
            }
        }
        public async Task<byte[]> DownloadOpportunity(DateTime? dateFrom, DateTime? dateTo, int? officeId, string departmentIds, string statusIds)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ExportOpportunityOutput>("SpWeeklyReport",
                  new
                  {
                      @UserId = AbpSession.UserId.Value,
                      @DateFrom = dateFrom.Value,
                      @DateTo = dateTo.Value,
                      @OfficeId = officeId,
                      @DepartmentIds = departmentIds,
                      @StatusIds = statusIds
                  }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ExportOpportunityOutput>>(output);
                var dt = GenarateColumnOpportunity();
                GenarateDataOpportunity(dt, results);
                var filePath = string.Format($"{ _hostingEnvironment.ContentRootPath} { @"\CRM.EmailTemplate\ExportOpportunity.xlsx"}");
                var wbContent = _exportHelper.GetWorkbookContent(dt, 1, 5);
                var memoryFile = _exportHelper.ExportWorkbookFile(wbContent, filePath);
                return memoryFile.ToArray();
            }
        }
        public async Task<List<ExportOpportunityOutput>> ExportOpportunity(DateTime? dateFrom, DateTime? dateTo, int[] organizationUnitIds, int? officeId, string departmentIds, string statusIds)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ExportOpportunityOutput>("SpWeeklyReport",
                   new
                   {
                       @UserId = AbpSession.UserId.Value,
                       @DateFrom = dateFrom.Value,
                       @DateTo = dateTo.Value,
                       @OfficeId = officeId,
                       @DepartmentIds = departmentIds,
                       @OrganizationUnitIds = base.ParseToString(organizationUnitIds),
                       @StatusIds = statusIds
                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ExportOpportunityOutput>>(output);
                return results;
            }
        }
        public async Task<PagedResultDto<OpportunityOutputDto>> OpportunityNotInDeal(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<OpportunityFilterDto>("SpFilterOpportunityNotInDeal",
                   new
                   {
                       @FilterJson = input
                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<OpportunityOutputDto>>(output);
                return new PagedResultDto<OpportunityOutputDto>(totalCount, results);
            }
        }
        public async Task<OpportunityOutputDto> GetOpportunityDetail(long opportunityId)
        {
            return await GetDetails(opportunityId);
        }
        public async Task<OpportunityOutputDto> UpdateOpportunityAsync(long opportunityId, OpportunityInputDto input)
        {
            if (opportunityId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("OpportunityIdNotFound"));
            var output = await CreateOrUpdateOpportunityAsync(opportunityId, input);
            return output;
        }
        public List<OpportunityContactOutputDto> CreateOrUpdateOpportunityContact(List<OpportunityContactInputDto> input)
        {
            ValidateOpportunityListContact(input);
            var output = ObjectMapper.Map<List<OpportunityContactOutputDto>>(CreateOrUpdateListOpportunityContact(input));
            return output;
        }
        public List<OpportunityUserOutputDto> CreateOrUpdateOpportunityUser(List<OpportunityUserInputDto> opportunityUsers)
        {
            ValidateOpportunityListUser(opportunityUsers);
            var output = ObjectMapper.Map<List<OpportunityUserOutputDto>>(CreateOrUpdateListOpportunityUser(opportunityUsers));
            return output;
        }
        public List<OpportunityLeadOutputDto> CreateOrUpdateOpportunityLead(List<OpportunityLeadInputDto> opportunityLeads)
        {
            ValidateOpportunityListLead(opportunityLeads);
            var output = ObjectMapper.Map<List<OpportunityLeadOutputDto>>(CreateOrUpdateListOpportunityLead(opportunityLeads));
            return output;
        }
        public List<OpportunityOrganizationUnitOutputDto> CreateOrUpdateOpportunityOrganizationUnit(List<OpportunityOrganizationUnitInputDto> opportunityOpportunityOrganizationUnits)
        {
            ValidateOpportunityListOrganizationUnit(opportunityOpportunityOrganizationUnits);
            var output = ObjectMapper.Map<List<OpportunityOrganizationUnitOutputDto>>(CreateOrUpdateListOpportunityOrganizationUnit(opportunityOpportunityOrganizationUnits));
            return output;
        }

        [UnitOfWork]
        public bool ConvertOpportunityToDeal(long opportunityId)
        {
            try
            {
                ValidateConvertOpportunityToDeal(opportunityId);
                var oppStageSignId = _repoOpportunityCategory.GetAll()
                    .Where(x => x.TypeCode.ToUpper() == Opportunity.OpportunityConstant.OpportunityStage.ToUpper() && x.Name.ToUpper() == OpportunityConstant.OppStageWip.ToUpper()).Select(y => y.Id).FirstOrDefault();

                var oppStatusContractId = _repoOpportunityCategory.GetAll()
                  .Where(x => x.TypeCode.ToUpper() == Opportunity.OpportunityConstant.OpportunityStatus.ToUpper() && x.Name.ToUpper() == OpportunityConstant.OppStatusContract.ToUpper()).Select(y => y.Id).FirstOrDefault();

                var primaryContact = _repoOpportunityContact.GetAll()
                .Where(x => x.OpportunityId == opportunityId && x.IsActive == true && x.IsPrimary == true).FirstOrDefault();

                var dealStatusId = _repoOpportunityCategory.GetAll()
                    .Where(x => x.TypeCode.ToUpper() == Deal.DealConstant.TypeCodeDealStatus.ToUpper() && x.Name.ToUpper() == Deal.DealConstant.DealWip.ToUpper()).Select(y => y.Id).SingleOrDefault();
                var opportunity = _repoOpportunity.Get(opportunityId);
                opportunity.StageId = oppStageSignId;
                opportunity.StatusId = oppStatusContractId;
                opportunity.Probability = 100;
                _repoOpportunity.Update(opportunity);
                var deal = new Entities.Deal.Deal
                {
                    DealName = opportunity.OpportunityName,
                    OrganizationUnitId = opportunity.OrganizationUnitId.Value,
                    FeeAmount = opportunity.Amount.Value,
                    IsActive = true,
                    StatusId = dealStatusId,
                    ContactId = primaryContact != null ? primaryContact.ContactId : default,
                    CompanyId = opportunity.CompanyId,
                    Description = opportunity.Description,
                    PercentCompleted = 0
                };
                var dealId = _repoDeal.InsertAndGetId(deal);
                var dealOpportunity = new Entities.DealOpportunity.DealOpportunity
                {
                    DealId = dealId,
                    OpportunityId = opportunityId,
                    IsActive = true
                };
                _repoDealOpportunity.Insert(dealOpportunity);
                var opportunityOrganizationUnits = _repoOpportunityOrganizationUnit.GetAll()
                    .Where(x => x.OpportunityId == opportunityId && x.IsActive == true).ToList();
                opportunityOrganizationUnits.ForEach(x =>
                {
                    var dealOrganizationUnit = new Entities.DealShare.DealShare
                    {
                        DealId = dealId,
                        FeeAmount = x.FeeAmount ?? 0,
                        InstructionId = x.InstructionId,
                        IsActive = true,
                        OrganizationUnitId = x.OrganizationUnitId,
                        IsPrimary = x.IsPrimary ?? false
                    };
                    _repoDealShare.InsertOrUpdate(dealOrganizationUnit);
                });
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.ToString());
            }

        }
        #region validate 
        private void ValidateConvertOpportunityToDeal(long opportunityId)
        {
            if (_repoDealOpportunity.GetAll().Any(x => x.OpportunityId == opportunityId && x.IsActive == true))
            {
                throw new UserFriendlyException(L("OpportunityExistInDeal"));
            }
            if (!_repoOpportunity.GetAll().Any(x => x.Id == opportunityId && x.Amount.HasValue))
            {
                throw new UserFriendlyException(L("InputOpportunityAmount"));
            }
            if (!_repoOpportunity.GetAll().Any(x => x.Id == opportunityId && x.OrganizationUnitId.HasValue))
            {
                throw new UserFriendlyException(L("InputDepartment"));
            }
        }
        private void ValidateOpportunityAssetClass(List<int> assetClassIds)
        {
            if (_repoAssetClass.Count(x => assetClassIds.Contains(x.Id) && x.IsActive) < assetClassIds.Count)
                throw new UserFriendlyException(L("AssetClassIdNotFound"));
        }

        private void ValidateOpportunityIntruction(List<int> instructionIds)
        {
            if (_repoInstruction.Count(x => instructionIds.Contains(x.Id) && x.IsActive) < instructionIds.Count)
                throw new UserFriendlyException(L("InstructionIdNotFound"));
        }

        private void ValidateOpportunityProperty(List<long> propertyIds)
        {
            if (_repoProperty.GetAll().Count(x => propertyIds.Contains(x.Id) && x.IsActive.Value) < propertyIds.Count)
                throw new UserFriendlyException(L("PropertyIdNotFound"));
        }
        private void ValidateOpportunityProject(List<long> projectIds)
        {
            if (_repoProject.GetAll().Count(x => projectIds.Contains(x.Id) && x.IsActive) < projectIds.Count)
                throw new UserFriendlyException(L("ProjectIdNotFound"));
        }
        private void ValidateOpportunityInfor(OpportunityInputDto infor)
        {
            if (_repoCompany.Count(x => x.Id == infor.CompanyId && x.IsActive) != 1)
                throw new UserFriendlyException(L("CompanyIdNotFound"));

            if (_repoOpportunityCategory.Count(x => x.Id == infor.StatusId && x.IsActive && x.TypeCode.ToUpper() == OpportunityConstant.OpportunityStatus.ToUpper()) != 1)
                throw new UserFriendlyException(L("StatusIdNotFound"));

            if (infor.ClientTypeId.HasValue)
            {
                if (_repoClientType.Count(x => x.Id == infor.ClientTypeId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("ClientTypeIdNotFound"));
            }
            if (infor.IndustryId.HasValue)
            {
                if (_repoIndustry.Count(x => x.Id == infor.IndustryId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("IndustryIdNotFound"));
            }
            if (infor.RequestId.HasValue)
            {
                if (_repoRequest.Count(x => x.Id == infor.RequestId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("RequestIdNotFound"));
            }
            if (infor.SourceId.HasValue)
            {
                if (_repoLeadSource.Count(x => x.Id == infor.SourceId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("LeadSourceIdNotFound"));
            }
            if (infor.CurrencyId.HasValue)
            {
                if (_repoCurrency.Count(x => x.Id == infor.CurrencyId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("CurrencyIdNotFound"));
            }
            if (infor.PrimaryAssetClassId.HasValue)
            {
                if (_repoAssetClass.Count(x => x.Id == infor.PrimaryAssetClassId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("PrimaryAssetClassIdNotFound"));
            }
            if (infor.PrimaryInstructionId.HasValue)
            {
                if (_repoInstruction.Count(x => x.Id == infor.PrimaryInstructionId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("PrimaryInstructionIdNotFound"));
            }

            if ((infor.Probability > 0 || infor.StatusId > 0 || infor.StageId > 0) && infor.Id.HasValue)
            {
                var opportunity = _repoOpportunity.Get(infor.Id.Value);
                if (_repoDealOpportunity.Count(x => x.OpportunityId == infor.Id && x.IsActive) > 0
                      && (opportunity.Probability != infor.Probability || opportunity.StageId != infor.StageId || opportunity.StatusId != infor.StatusId))
                    throw new UserFriendlyException(L("OpportunityExistOtherDeal"));
            }
            if (string.IsNullOrEmpty(infor.OpportunityName))
                throw new UserFriendlyException(L("RequiredField", nameof(infor.OpportunityName)));
            if (!infor.OrganizationUnitId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.PrimaryDepartment)));
            if (!infor.StageId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.StageId)));
            if (!infor.ExpectedCloseDate.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.ExpectedCloseDate)));
            if (!infor.Amount.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.Amount)));
            if (!infor.Probability.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.Probability)));
            if (!infor.SourceId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.SourceId)));
        }

        private void ValidateOpportunityFullInfor(OpportunityFullInputDto infor)
        {
            if (_repoCompany.Count(x => x.Id == infor.CompanyId && x.IsActive) != 1)
                throw new UserFriendlyException(L("CompanyIdNotFound"));

            if (_repoOpportunityCategory.Count(x => x.Id == infor.StatusId && x.IsActive && x.TypeCode.ToUpper() == OpportunityConstant.OpportunityStatus.ToUpper()) != 1)
                throw new UserFriendlyException(L("StatusIdNotFound"));

            if (infor.ClientTypeId.HasValue)
            {
                if (_repoClientType.Count(x => x.Id == infor.ClientTypeId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("ClientTypeIdNotFound"));
            }
            if (infor.IndustryId.HasValue)
            {
                if (_repoIndustry.Count(x => x.Id == infor.IndustryId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("IndustryIdNotFound"));
            }
            if (infor.RequestId.HasValue)
            {
                if (_repoRequest.Count(x => x.Id == infor.RequestId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("RequestIdNotFound"));
            }
            if (infor.SourceId.HasValue)
            {
                if (_repoLeadSource.Count(x => x.Id == infor.SourceId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("LeadSourceIdNotFound"));
            }
            if (infor.CurrencyId.HasValue)
            {
                if (_repoCurrency.Count(x => x.Id == infor.CurrencyId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("CurrencyIdNotFound"));
            }
            if (infor.PrimaryAssetClassId.HasValue)
            {
                if (_repoAssetClass.Count(x => x.Id == infor.PrimaryAssetClassId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("PrimaryAssetClassIdNotFound"));
            }
            if (infor.PrimaryInstructionId.HasValue)
            {
                if (_repoInstruction.Count(x => x.Id == infor.PrimaryInstructionId.Value && x.IsActive) != 1)
                    throw new UserFriendlyException(L("PrimaryInstructionIdNotFound"));
            }

            if ((infor.Probability > 0 || infor.StatusId > 0 || infor.StageId > 0) && infor.Id > 0)
            {
                var opportunity = _repoOpportunity.Get(infor.Id);
                if (_repoDealOpportunity.Count(x => x.OpportunityId == infor.Id && x.IsActive) > 0
                      && (opportunity.Probability != infor.Probability || opportunity.StageId != infor.StageId || opportunity.StatusId != infor.StatusId))
                    throw new UserFriendlyException(L("OpportunityExistOtherDeal"));
            }
            if (string.IsNullOrEmpty(infor.OpportunityName))
                throw new UserFriendlyException(L("RequiredField", nameof(infor.OpportunityName)));
            if (infor.OpportunityOrganizationUnit == null || !infor.OpportunityOrganizationUnit.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("InputPrimaryDepartment"));
            //if (!infor.OrganizationUnitId.HasValue)
            //    throw new UserFriendlyException(L("RequiredField", nameof(infor.PrimaryDepartment)));
            if (!infor.StageId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.StageId)));
            if (!infor.ExpectedCloseDate.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.ExpectedCloseDate)));
            if (!infor.Amount.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.Amount)));
            if (!infor.Probability.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.Probability)));
            if (!infor.SourceId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(infor.SourceId)));
        }
        //OpportunityContact
        private void ValidateOpportunityListContact(List<OpportunityContactInputDto> opportunityContacts)
        {
            if (opportunityContacts.Any() && !opportunityContacts.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("NeedOneContactIsPriamry")); ;
            opportunityContacts.ForEach(x => ValidateOpportunityContact(x));
        }
        private void ValidateOpportunityContact(OpportunityContactInputDto opportunityContact)
        {
            if (opportunityContact.Id > 0)
            {
                if (!_repoOpportunityContact.GetAll().Any(x => x.Id == opportunityContact.Id))
                    throw new UserFriendlyException(L("OpportunityContactIdNotFound"));
            }
            else
            {
                if (_repoOpportunityContact.GetAll().Any(x => x.OpportunityId == opportunityContact.OpportunityId && x.IsActive == true && x.ContactId == opportunityContact.ContactId))
                    throw new UserFriendlyException(L("ExistOpportunityContact"));
            }
        }
        //OpportunityUser
        private void ValidateOpportunityListUser(List<OpportunityUserInputDto> opportunityUsers)
        {
            opportunityUsers.ForEach(x => ValidateOpportunityUser(x));
        }
        private void ValidateOpportunityUser(OpportunityUserInputDto opportunityUser)
        {
            if (opportunityUser.Id > 0)
            {
                if (!_repoOpportunityUser.GetAll().Any(x => x.Id == opportunityUser.Id))
                    throw new UserFriendlyException(L("OpportunityUserIdNotFound"));
            }
            else
            {
                if (_repoOpportunityUser.GetAll().Any(x => x.OpportunityId == opportunityUser.OpportunityId && x.IsActive == true && x.UserId == opportunityUser.UserId && x.IsFollwer == opportunityUser.IsFollwer))
                    throw new UserFriendlyException(L("ExistOpportunityUser"));
            }
        }


        //OpportunityLead
        private void ValidateOpportunityListLead(List<OpportunityLeadInputDto> opportunityLeads)
        {
            opportunityLeads.ForEach(x => ValidateOpportunityLead(x));
        }
        private void ValidateOpportunityLead(OpportunityLeadInputDto opportunityLead)
        {
            if (opportunityLead.Id > 0)
            {
                if (!_repoOpportunityLead.GetAll().Any(x => x.Id == opportunityLead.Id))
                    throw new UserFriendlyException(L("OpportunityLeadIdNotFound"));
            }
            else
            {
                if (_repoOpportunityLead.GetAll().Any(x => x.OpportunityId == opportunityLead.OpportunityId && x.IsActive == true && x.UserId == opportunityLead.UserId))
                    throw new UserFriendlyException(L("ExistOpportunityLead"));
            }
        }

        //Opportunity OrganizationUnit
        private void ValidateOpportunityListOrganizationUnit(List<OpportunityOrganizationUnitInputDto> opportunityOrganizationUnits)
        {
            if (!opportunityOrganizationUnits.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("NeedOneTeamIsPriamry")); ;
            opportunityOrganizationUnits.ForEach(x => ValidateOpportunityOrganizationUnit(x));
        }
        private void ValidateOpportunityOrganizationUnit(OpportunityOrganizationUnitInputDto opportunityOrganizationUnit)
        {
            //update
            if (opportunityOrganizationUnit.Id > 0)
            {
                if (!_repoOpportunityOrganizationUnit.GetAll().Any(x => x.Id == opportunityOrganizationUnit.Id))
                    throw new UserFriendlyException(L("OpportunityOrganizationIdNotFound"));
            }
            //Insert
            else
            {
                if (_repoOpportunityOrganizationUnit.GetAll().Any(x => x.OpportunityId == opportunityOrganizationUnit.OpportunityId && x.IsActive == true && x.OrganizationUnitId == opportunityOrganizationUnit.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistOpportunityOrganizationUnit"));
            }
        }
        private void ValidateOpportunityListOrganizationUnitV1(long opportunityId, List<OpportunityOrganizationUnitInputDto> opportunityOrganizationUnits)
        {
            if (!opportunityOrganizationUnits.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("NeedOneTeamIsPriamry")); ;
            opportunityOrganizationUnits.ForEach(x => ValidateOpportunityOrganizationUnitV1(opportunityId, x));
        }
        private void ValidateOpportunityOrganizationUnitV1(long opportunityId, OpportunityOrganizationUnitInputDto opportunityOrganizationUnit)
        {
            //update
            if (opportunityOrganizationUnit.Id > 0)
            {
                if (!_repoOpportunityOrganizationUnit.GetAll().Any(x => x.Id == opportunityOrganizationUnit.Id))
                    throw new UserFriendlyException(L("OpportunityOrganizationIdNotFound"));
            }
            //Insert
            else
            {
                if (_repoOpportunityOrganizationUnit.GetAll().Any(x => x.OpportunityId == opportunityOrganizationUnit.OpportunityId && x.IsActive == true && x.OrganizationUnitId == opportunityOrganizationUnit.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistOpportunityOrganizationUnit"));
            }
        }
        #endregion

        #region Private API
        //Create or Update
        private async Task<OpportunityOutputDto> CreateOrUpdateOpportunityAsync(long? opportunityId,
           OpportunityInputDto input)
        {
            var output = new OpportunityOutputDto();
            if (opportunityId.HasValue && opportunityId > 0)
            {
                var entity = _repoOpportunity.Get(opportunityId.Value);
                if (opportunityId < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("OpportunityIdNotFound"));
                ValidateOpportunityInfor(input);
                if (input.Amount.HasValue && input.Amount != entity.Amount && _repoDealOpportunity.GetAll().Any(x => x.IsActive == true && x.OpportunityId == input.Id.Value))
                {
                    var dealId = _repoDealOpportunity.GetAll().Where(x => x.OpportunityId == input.Id.Value && x.IsActive).Select(x => x.DealId).FirstOrDefault();
                    var currentOppId = _repoDealOpportunity.GetAll().Where(x => x.DealId == dealId && x.OpportunityId != input.Id && x.IsActive).Select(y => y.OpportunityId);
                    var fee = _repoOpportunity.GetAll().Where(x => currentOppId.Contains(x.Id)).Sum(y => y.Amount);
                    var totalFee = fee + input.Amount;
                    var deal = _repoDeal.Get(dealId);
                    deal.FeeAmount = totalFee.Value;
                    _repoDeal.Update(deal);
                }
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoOpportunity.UpdateAsync(entityUpdate);
                output = ObjectMapper.Map<OpportunityOutputDto>(entity);
            }
            else
            {
                ValidateOpportunityInfor(input);
                var entity = ObjectMapper.Map<Entities.Opportunity.Opportunity>(input);
                entity.IsActive = true;
                entity.Id = await _repoOpportunity.InsertAndGetIdAsync(entity);
                output = ObjectMapper.Map<OpportunityOutputDto>(entity);
            }
            if (input.PropertyIds != null)
            {
                ValidateOpportunityProperty(input.PropertyIds);
                output.Property = ObjectMapper.Map<List<OpportunityPropertyOutputDto>>(CreateOrUpdateListOpportunityProperty(output.Id, input.PropertyIds));
            }
            if (input.ProjectIds != null)
            {
                ValidateOpportunityProject(input.ProjectIds);
                output.Project = ObjectMapper.Map<List<OpportunityProjectOutputDto>>(CreateOrUpdateListOpportunityProject(output.Id, input.ProjectIds));
            }
            if (input.InstructionIds != null)
            {
                ValidateOpportunityIntruction(input.InstructionIds);
                output.Instruction = ObjectMapper.Map<List<OpportunityInstructionOutputDto>>(CreateOrUpdateListOpportunityInstruction(output.Id, input.InstructionIds));
            }
            if (input.AssetClassIds != null)
            {
                ValidateOpportunityAssetClass(input.AssetClassIds);
                output.AssetClass = ObjectMapper.Map<List<OpportunityAssetClassOutputDto>>(CreateOrUpdateListOpportunityAssetClass(output.Id, input.AssetClassIds));
            }

            return output;
        }
        private async Task<OpportunityOutputDto> CreateOrUpdateOpportunityFullAsync(OpportunityFullInputDto input)
        {
            ValidateOpportunityFullInfor(input);
            var primaryDepartment = input.OpportunityOrganizationUnit.Where(x => x.IsPrimary == true).SingleOrDefault();
            input.OrganizationUnitId = primaryDepartment.OrganizationUnitId;
            var entity = new Entities.Opportunity.Opportunity();
            if (input.Id > 0)
            {
                if (input.Id < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("OpportunityIdNotFound"));
                entity = await _repoOpportunity.GetAsync(input.Id);
                var entityUpdate = ObjectMapper.Map(input, entity);
                if (input.Amount.HasValue && input.Amount != entity.Amount && _repoDealOpportunity.GetAll().Any(x => x.IsActive == true && x.OpportunityId == input.Id))
                {
                    var dealId = _repoDealOpportunity.GetAll().Where(x => x.OpportunityId == input.Id && x.IsActive).Select(x => x.DealId).FirstOrDefault();
                    var currentOppId = _repoDealOpportunity.GetAll().Where(x => x.DealId == dealId && x.OpportunityId != input.Id && x.IsActive).Select(y => y.OpportunityId);
                    var fee = _repoOpportunity.GetAll().Where(x => currentOppId.Contains(x.Id)).Sum(y => y.Amount);
                    var totalFee = fee + input.Amount;
                    var deal = _repoDeal.Get(dealId);
                    deal.FeeAmount = totalFee.Value;
                    await _repoDeal.UpdateAsync(deal);
                }
                await _repoOpportunity.UpdateAsync(entityUpdate);
            }
            else
            {
                var entityInsert = ObjectMapper.Map(input, entity);
                entity.IsActive = true;
                entity.Id = await _repoOpportunity.InsertAndGetIdAsync(entityInsert);
            }
            var output = ObjectMapper.Map<OpportunityOutputDto>(entity);

            if (input.PropertyIds != null)
            {
                ValidateOpportunityProperty(input.PropertyIds);
                output.Property = ObjectMapper.Map<List<OpportunityPropertyOutputDto>>(CreateOrUpdateListOpportunityProperty(output.Id, input.PropertyIds));
            }
            if (input.ProjectIds != null)
            {
                ValidateOpportunityProject(input.ProjectIds);
                await UpdateChilds(
                     _repoOpportunityProject,
                     x => x.OpportunityId = entity.Id,
                     x => x.OpportunityId == entity.Id,
                     (x, y) => x.ProjectId == y.ProjectId,
                     input.ProjectIds.Select(x => new OpportunityProjectInputDto { ProjectId = x, OpportunityId = entity.Id, IsActive = true }).ToArray()
                     );
                //output.Project = ObjectMapper.Map<List<OpportunityProjectOutputDto>>(CreateOrUpdateListOpportunityProject(output.Id, input.ProjectIds));
            }
            //if (input.InstructionIds != null)
            //{
            //    ValidateOpportunityIntruction(input.InstructionIds);
            //    output.Instruction = ObjectMapper.Map<List<OpportunityInstructionOutputDto>>(CreateOrUpdateListOpportunityInstruction(output.Id, input.InstructionIds));
            //}
            //bo khi deploy ba moi vi da dua xuong share
            if (input.AssetClassIds != null)
            {
                ValidateOpportunityAssetClass(input.AssetClassIds);
                await UpdateChilds(
                   _repoOpportunityAssetClass,
                   x => x.OpportunityId = entity.Id,
                   x => x.OpportunityId == entity.Id,
                   (x, y) => x.AssetClassId == y.AssetClassId,
                   input.AssetClassIds.Select(x => new OpportunityAssetClassInputDto { AssetClassId = x, OpportunityId = entity.Id, IsActive = true }).ToArray()
                   );
                //  output.AssetClass = ObjectMapper.Map<List<OpportunityAssetClassOutputDto>>(CreateOrUpdateListOpportunityAssetClass(output.Id, input.AssetClassIds));
            }
            if (input.OpportunityContact != null)
            {
                ValidateOpportunityListContact(input.OpportunityContact);
                await UpdateChilds(
                _repoOpportunityContact,
                x => x.OpportunityId = entity.Id,
                x => x.OpportunityId == entity.Id,
                (x, y) => x.ContactId == y.ContactId,
                  input.OpportunityContact.Select(x => new OpportunityContactInputDto { OpportunityId = entity.Id, IsPrimary = x.IsPrimary, ContactId = x.ContactId, IsActive = true }).ToArray()
                         );
                // output.OpportunityContact = ObjectMapper.Map<List<OpportunityContactOutputDto>>(CreateOrUpdateListOpportunityContactV1(output.Id, input.OpportunityContact));
            }
            if (input.OpportunityLeadIds != null)
            {
                await UpdateChilds(
                 _repoOpportunityLead,
                 x => x.OpportunityId = entity.Id,
                 x => x.OpportunityId == entity.Id,
                 (x, y) => x.UserId == y.UserId,
                 input.OpportunityLeadIds.Select(x => new OpportunityLeadInputDto { UserId = x, OpportunityId = entity.Id, IsActive = true }).ToArray()
                 );
                // output.OpportunityLead = ObjectMapper.Map<List<OpportunityLeadOutputDto>>(CreateOrUpdateListOpportunityLeadV1(output.Id, input.OpportunityLeadIds));
            }
            if (input.OpportunityUserIds != null)
            {
                await UpdateChilds(
                _repoOpportunityUser,
                x => x.OpportunityId = entity.Id,
                x => x.OpportunityId == entity.Id,
                (x, y) => x.UserId == y.UserId,
                input.OpportunityUserIds.Select(x => new OpportunityUserInputDto { UserId = x, OpportunityId = entity.Id, IsActive = true }).ToArray()
                );
                // output.OpportunityUser = ObjectMapper.Map<List<OpportunityUserOutputDto>>(CreateOrUpdateListOpportunityUserV1(output.Id, input.OpportunityUserIds));
            }
            if (input.OpportunityOrganizationUnit != null)
            {
                //ValidateOpportunityListOrganizationUnitV1(output.Id, input.OpportunityOrganizationUnit);
                await UpdateChilds(
               _repoOpportunityOrganizationUnit,
               x => x.OpportunityId = entity.Id,
               x => x.OpportunityId == entity.Id,
               (x, y) => x.Id == y.Id,
               input.OpportunityOrganizationUnit.Select(x => new OpportunityOrganizationUnitInputDto
               {
                   OpportunityId = entity.Id
                   ,
                   OrganizationUnitId = x.OrganizationUnitId
                    ,
                   InstructionId = x.InstructionId
                    ,
                   FeeAmount = x.FeeAmount
                   ,
                   IsPrimary = x.IsPrimary
                   ,
                   IsActive = true
               }
               ).ToArray()
               );
                //output.OpportunityOrganizationUnit = ObjectMapper.Map<List<OpportunityOrganizationUnitOutputDto>>(CreateOrUpdateListOpportunityOrganizationUnitV1(output.Id, input.OpportunityOrganizationUnit));
            }
            return await GetDetails(entity.Id);
        }
        public async Task SendMailOpportunity(long opportunityId)
        {
            var userAssgin = await _repoOpportunityUser.GetAll()
                    .Include(x => x.User)
                    .Where(x => x.OpportunityId == opportunityId && x.IsActive == true).ToListAsync();
            var userLead = await _repoOpportunityLead.GetAll()
                 .Include(x => x.User)
                .Where(x => x.OpportunityId == opportunityId && x.IsActive == true).ToListAsync();

            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ExportOpportunityOutput>("SpWeeklyReport",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                       ,
                        @Id = opportunityId
                    }, commandType: CommandType.StoredProcedure);
                var result = output.SingleOrDefault();
                var host = _configHelper.GetServerRoot();
                var link = string.Format(host + "/real-estate/opportunity-details?id={0}", opportunityId);
                foreach (var user in userAssgin)
                {
                    var email = user.User.EmailAddress.ToString();
                    await SendMailChangeOpportunity(user.User.UserName, result.Description, result.CreationTime.ToShortDateString(), result.Amount
                        , result.Assigned, result.LeadBy, result.Company, result.Contact, email, result.OpportunityName
                        , result.Status, result.Department, link);
                }
                foreach (var lead in userLead)
                {
                    var email = lead.User.EmailAddress.ToString();
                    await SendMailChangeOpportunity(lead.User.UserName, result.Description, result.CreationTime.ToShortDateString(), result.Amount
                       , result.Assigned, result.LeadBy, result.Company, result.Contact, email, result.OpportunityName
                       , result.Status, result.Department, link);
                }
            }
        }
        private async Task SendMailChangeOpportunity(string displayName, string description, string dateCreated, long? fee, string assigned, string lead, string company, string contact
            , string emailAddress, string opportunitName, string status, string department, string link)
        {
            var subject = "The opportunity has been changed";
            var emailContent = new StringBuilder(_emailTemplateProvider.GetEmailTemplate($"CRM.EmailTemplate.OpportunityChanged.html"));
            var dataBindings = new Dictionary<string, string>() {
                      {EmailConsts.Opportunity.Assigned, assigned},
                      {EmailConsts.Opportunity.DisplayName, displayName},
                      {EmailConsts.Opportunity.Lead, lead},
                      {EmailConsts.Opportunity.Company, company},
                      {EmailConsts.Opportunity.Contact, contact},
                      {EmailConsts.Opportunity.OpportunityName,opportunitName},
                      {EmailConsts.Opportunity.Department,department},
                      {EmailConsts.Opportunity.Status, status},
                      {EmailConsts.Opportunity.Description, description},
                      {EmailConsts.Opportunity.Fee, fee.Value.ToString()},
                      {EmailConsts.Opportunity.DateCreated,dateCreated},
                      {EmailConsts.Opportunity.Link, link},
            };

            var bodyHtmlContent = _emailTemplateProvider.BindDataToTemplate(emailContent.ToString(), dataBindings);

            await _sendGrid.SendAsync(new SendGridEmail()
            {
                SenderName = "CRM Notify",
                SenderEmailAddress = "info@sadec.co",
                SubjectContent = subject,
                BodyHtmlContent = bodyHtmlContent,
                RecepientEmailAddress = emailAddress,
            });
        }

        public async Task<bool> SendMailWeeklyReport(string displayName, string emailAddress)
        {
            var subject = "Weekly report";
            var emailContent = new StringBuilder(_emailTemplateProvider.GetEmailTemplate($"CRM.EmailTemplate.OpportunityWeeklyReport.html"));
            DateTime dateFrom = DateTime.Now.AddDays(-7);
            DateTime dateTo = DateTime.Now.AddDays(-1);
            var FromToDate = string.Format("From {0} to {1}", new object[] { dateFrom.ToString("dd-MMM-yyyy"), dateTo.ToString("dd-MMM-yyyy") });
            var host = _configHelper.GetServerRoot();
            var linkDepartment = string.Format(host + "/reports/opportunity-departments?dateFrom={0}&dateTo={1}", new object[] { dateFrom.ToShortDateString(), dateTo.ToShortDateString() });
            var linkUserActivity = string.Format(host + "/reports/user-activity?dateFrom={0}&dateTo={1}", new object[] { dateFrom.ToShortDateString(), dateTo.ToShortDateString() });
            var dataBindings = new Dictionary<string, string>() {
                  {EmailConsts.WeeklyReportOpportunity.DisplayName, displayName},
                        {EmailConsts.WeeklyReportOpportunity.FromToDate, FromToDate},
                      {EmailConsts.WeeklyReportOpportunity.LinkDepartment, linkDepartment},
                      {EmailConsts.WeeklyReportOpportunity.LinkUserActivity, linkUserActivity},
            };
            var bodyHtmlContent = _emailTemplateProvider.BindDataToTemplate(emailContent.ToString(), dataBindings);
            var response = await _sendGrid.SendAsync(new SendGridEmail()
            {
                SenderName = "CRM Notify",
                SenderEmailAddress = "info@sadec.co",
                SubjectContent = subject,
                BodyHtmlContent = bodyHtmlContent,
                RecepientEmailAddress = emailAddress,
            });
            //await _emailSenderHelper.SendAsyncWithAttachments("info@sadec.co"
            //    , emailAddress
            //    , subject
            //    , bodyHtmlContent
            //    , null, true);
            return response.StatusCode == HttpStatusCode.Accepted;
        }
        //Opportunity Contact
        private List<OpportunityContactOutputDto> CreateOrUpdateListOpportunityContact(List<OpportunityContactInputDto> opportunityContacts)
        {
            var entities = new List<OpportunityContactOutputDto>();
            opportunityContacts.ForEach(x =>
            {
                entities.Add(CreateOrUpdateOpportunityContact(x));
            });
            return entities;
        }

        //OpportunityUser
        private List<OpportunityUserOutputDto> CreateOrUpdateListOpportunityUser(List<OpportunityUserInputDto> opportunityUsers)
        {
            var entities = new List<OpportunityUserOutputDto>();
            opportunityUsers.ForEach(x =>
            {
                entities.Add(CreateOrUpdateOpportunityUser(x));
            });
            return entities;
        }

        //OpportunityLead
        private List<OpportunityLeadOutputDto> CreateOrUpdateListOpportunityLead(List<OpportunityLeadInputDto> opportunityLeads)
        {
            var entities = new List<OpportunityLeadOutputDto>();
            opportunityLeads.ForEach(x =>
            {
                entities.Add(CreateOrUpdateOpportunityLead(x));
            });
            return entities;
        }

        //OrganizationUnit
        private List<OpportunityOrganizationUnitOutputDto> CreateOrUpdateListOpportunityOrganizationUnit(List<OpportunityOrganizationUnitInputDto> opportunityOrganizationUnits)
        {

            var entities = new List<OpportunityOrganizationUnitOutputDto>();
            opportunityOrganizationUnits.ForEach(x =>
            {
                entities.Add(CreateOrUpdateOpportunityOrganizationUnit(x));
            });
            return entities;
        }


        // create or update full contat, organizationUnit, lead, user
        private List<OpportunityContact> CreateOrUpdateListOpportunityContact(long opportunityId, List<OpportunityContactInputDto> opportunityContacts)
        {
            var entities = new List<OpportunityContact>();
            opportunityContacts.ForEach(x =>
            {
                entities.Add(CreateOrUpdateOpportunityContact(opportunityId, x));
            });
            return entities;
        }
        private List<OpportunityContactOutputDto> CreateOrUpdateListOpportunityContactV1(long opportunityId, List<OpportunityContactInputDto> opportunityContact)
        {
            var currentOpportunityContact = _repoOpportunityContact.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            var companyContactIds = opportunityContact.Select(x => x.Id).ToList();
            var entities = new List<OpportunityContact>();
            opportunityContact.ForEach(x =>
            {
                entities.Add(CreateOrUpdateOpportunityContact(opportunityId, x));
            });
            var deleteEntities = currentOpportunityContact.Where(x => companyContactIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityContact.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityContact = _repoOpportunityContact.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive);
            return ObjectMapper.Map<List<OpportunityContactOutputDto>>(listOpportunityContact);
        }

        //OrganizationUnit
        private List<OpportunityOrganizationUnitOutputDto> CreateOrUpdateListOpportunityOrganizationUnitV1(long opportunityId, List<OpportunityOrganizationUnitInputDto> opportunityOrganizationUnit)
        {
            var currentOpportunityOrganizationUnit = _repoOpportunityOrganizationUnit.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            var companyOrganizationUnitIds = opportunityOrganizationUnit.Select(x => x.Id).ToList();
            var entities = new List<OpportunityOrganizationUnit>();
            opportunityOrganizationUnit.ForEach(x =>
            {
                entities.Add(CreateOrUpdateOpportunityOrganizationUnit(opportunityId, x));
            });
            var deleteEntities = currentOpportunityOrganizationUnit.Where(x => companyOrganizationUnitIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityOrganizationUnit.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityOrganizationUnit = _repoOpportunityOrganizationUnit.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive);
            return ObjectMapper.Map<List<OpportunityOrganizationUnitOutputDto>>(listOpportunityOrganizationUnit);
        }
        #endregion

        #region Action

        //Opportunity Lead
        [UnitOfWork]
        private OpportunityLeadOutputDto CreateOrUpdateOpportunityLead(OpportunityLeadInputDto input)
        {
            OpportunityLead entity;
            if (input.Id > 0)
            {
                var entityLead = _repoOpportunityLead.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityLead);
            }
            else
            {
                entity = ObjectMapper.Map<OpportunityLead>(input);
                entity.IsActive = true;
            }
            entity.Id = _repoOpportunityLead.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            var oppLead = _repoOpportunityLead.GetAll().Where(x => x.Id == entity.Id).Include(x => x.User).SingleOrDefault();
            // gui email
            //var opp = _repoOpportunity.GetAll().Where(x => x.Id == input.OpportunityId).SingleOrDefault();
            //var status = _repoOpportunityCategory.GetAll().Where(x => x.Id == opp.StatusId).SingleOrDefault();
            //var lastComment = _repoComment.GetAll()
            //    .Where(x => x.ModuleId == ActivityConstant.Opportunity
            //    && x.ReferenceId == input.OpportunityId
            //    ).OrderByDescending(x => x.CreationTime)
            //    .SingleOrDefault();
            //var host = _configHelper.GetServerRoot();
            //var link = string.Format(host + "/real-estate/opportunity-details?id={0}", input.OpportunityId);
            //var email = oppLead.User.EmailAddress.ToString();
            //var content = "This opportunity has been lead by you ";
            //SendMailChangeOpportunity(content, oppLead.User.FullName, email, opp.OpportunityName, status.Name.ToString(), status.Name.ToString(), (lastComment == null) ? "" : lastComment.Description.ToString(), link);
            return ObjectMapper.Map<OpportunityLeadOutputDto>(oppLead);
        }

        //Opportunity OrganizationUnit
        [UnitOfWork]
        private OpportunityOrganizationUnitOutputDto CreateOrUpdateOpportunityOrganizationUnit(OpportunityOrganizationUnitInputDto input)
        {
            OpportunityOrganizationUnit entity = new OpportunityOrganizationUnit();
            if (input.Id > 0)
            {
                var entityOrganizationUnit = _repoOpportunityOrganizationUnit.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityOrganizationUnit);
            }
            else
            {
                entity = ObjectMapper.Map<OpportunityOrganizationUnit>(input);
                entity.IsActive = true;
            }
            entity.Id = _repoOpportunityOrganizationUnit.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            var oppOrgan = _repoOpportunityOrganizationUnit.GetAll().Where(x => x.Id == entity.Id).Include(x => x.OrganizationUnit).SingleOrDefault();
            return ObjectMapper.Map<OpportunityOrganizationUnitOutputDto>(oppOrgan);
        }
        //Opportunity Contact
        [UnitOfWork]
        private OpportunityContactOutputDto CreateOrUpdateOpportunityContact(OpportunityContactInputDto input)
        {
            OpportunityContact entity = new OpportunityContact();
            if (input.Id > 0)
            {
                var entityContact = _repoOpportunityContact.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityContact);
            }
            else
            {
                entity = ObjectMapper.Map<OpportunityContact>(input);
                entity.IsActive = true;
            }
            entity.Id = _repoOpportunityContact.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            var oppContact = _repoOpportunityContact.GetAll().Where(x => x.Id == entity.Id).Include(x => x.Contact).SingleOrDefault();
            return ObjectMapper.Map<OpportunityContactOutputDto>(oppContact);
        }
        //Opportunity User
        [UnitOfWork]
        private OpportunityUserOutputDto CreateOrUpdateOpportunityUser(OpportunityUserInputDto input)
        {
            OpportunityUser entity;
            entity = new OpportunityUser();
            if (input.Id > 0)
            {
                var entityUser = _repoOpportunityUser.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityUser);
            }
            else
            {
                entity = ObjectMapper.Map<OpportunityUser>(input);
            }
            entity.Id = _repoOpportunityUser.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            var oppUser = _repoOpportunityUser.GetAll().Where(x => x.Id == entity.Id).Include(x => x.User).SingleOrDefault();
            // gui email
            //var opp = _repoOpportunity.GetAll().Where(x => x.Id == input.OpportunityId).SingleOrDefault();
            //var status = _repoOpportunityCategory.GetAll().Where(x => x.Id == opp.StatusId).SingleOrDefault();
            //var lastComment = _repoComment.GetAll()
            //    .Where(x => x.ModuleId == ActivityConstant.Opportunity
            //    && x.ReferenceId == input.OpportunityId
            //    ).OrderByDescending(x => x.CreationTime)
            //    .SingleOrDefault();
            //var host = _configHelper.GetServerRoot();
            //var link = string.Format(host + "/real-estate/opportunity-details?id={0}", input.OpportunityId);
            //var email = oppUser.User.EmailAddress.ToString();
            //var content = "This opportunity has been assign for you ";
            //SendMailChangeOpportunity(content, oppUser.User.FullName, email, opp.OpportunityName, status.Name.ToString(), status.Name.ToString(), (lastComment == null) ? "" : lastComment.Description.ToString(), link);

            return ObjectMapper.Map<OpportunityUserOutputDto>(oppUser);
        }


        // create or update full contat, organizationUnit, lead, user
        //Opportunity OrganizationUnit
        [UnitOfWork]
        private OpportunityOrganizationUnit CreateOrUpdateOpportunityOrganizationUnit(long opportunityId, OpportunityOrganizationUnitInputDto input)
        {
            OpportunityOrganizationUnit entity;
            if (input.Id > 0)
            {
                var entityOrganizationUnit = _repoOpportunityOrganizationUnit.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityOrganizationUnit);
            }
            else
            {
                entity = ObjectMapper.Map<OpportunityOrganizationUnit>(input);
                entity.IsActive = true;
            }
            entity.OpportunityId = opportunityId;
            entity.Id = _repoOpportunityOrganizationUnit.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            return entity;
        }

        //Opportunity Contact
        [UnitOfWork]
        private OpportunityContact CreateOrUpdateOpportunityContact(long opportunityId, OpportunityContactInputDto input)
        {
            OpportunityContact entity;
            if (input.Id > 0)
            {
                var entityContact = _repoOpportunityContact.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityContact);
            }
            else
            {
                entity = ObjectMapper.Map<OpportunityContact>(input);
                entity.IsActive = true;
            }
            entity.OpportunityId = opportunityId;
            entity.Id = _repoOpportunityContact.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            return entity;
        }
        //Users 
        public List<OpportunityUserOutputDto> CreateOrUpdateListOpportunityUserV1(long opportunityId, List<long> userIds)
        {

            var currentOpportunityUser = _repoOpportunityUser.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            userIds.ForEach(x =>
            {
                var objUpdate = currentOpportunityUser.FirstOrDefault(obj => obj.UserId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoOpportunityUser.Update(objUpdate);
                    return;
                }
                var entity = new OpportunityUser
                {
                    OpportunityId = opportunityId,
                    UserId = x,
                    IsActive = true
                };
                _repoOpportunityUser.Insert(entity);
            });
            var deleteEntities = currentOpportunityUser.Where(x => userIds.All(y => y != x.UserId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityUser.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityUser = _repoOpportunityUser.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive).Include(x => x.User).ToList();
            return ObjectMapper.Map<List<OpportunityUserOutputDto>>(listOpportunityUser);
        }
        //Users 
        public List<OpportunityLeadOutputDto> CreateOrUpdateListOpportunityLeadV1(long opportunityId, List<long> leadIds)
        {

            var currentOpportunityLead = _repoOpportunityLead.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            leadIds.ForEach(x =>
            {
                var objUpdate = currentOpportunityLead.FirstOrDefault(obj => obj.UserId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoOpportunityLead.Update(objUpdate);
                    return;
                }
                var entity = new OpportunityLead
                {
                    OpportunityId = opportunityId,
                    UserId = x,
                    IsActive = true
                };
                _repoOpportunityLead.Insert(entity);
            });
            var deleteEntities = currentOpportunityLead.Where(x => leadIds.All(y => y != x.UserId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityLead.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityLead = _repoOpportunityLead.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive).Include(x => x.User).ToList();
            return ObjectMapper.Map<List<OpportunityLeadOutputDto>>(listOpportunityLead);
        }
        //AssetClass
        //  [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        public List<OpportunityAssetClassOutputDto> CreateOrUpdateListOpportunityAssetClass(long opportunityId, List<int> assetClassIds)
        {

            var currentOpportunityAssetClass = _repoOpportunityAssetClass.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            assetClassIds.ForEach(x =>
            {
                var objUpdate = currentOpportunityAssetClass.FirstOrDefault(obj => obj.AssetClassId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoOpportunityAssetClass.Update(objUpdate);
                    return;
                }
                var entity = new OpportunityAssetClass
                {
                    OpportunityId = opportunityId,
                    AssetClassId = x,
                    IsActive = true
                };
                _repoOpportunityAssetClass.Insert(entity);
            });
            var deleteEntities = currentOpportunityAssetClass.Where(x => assetClassIds.All(y => y != x.AssetClassId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityAssetClass.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityAssetClass = _repoOpportunityAssetClass.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive).Include(x => x.AssetClass).ToList();
            return ObjectMapper.Map<List<OpportunityAssetClassOutputDto>>(listOpportunityAssetClass);
        }

        //Instruction
        // [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        public List<OpportunityInstructionOutputDto> CreateOrUpdateListOpportunityInstruction(long opportunityId, List<int> instructionIds)
        {

            var currentOpportunityInstruction = _repoOpportunityInstruction.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            instructionIds.ForEach(x =>
            {
                var objUpdate = currentOpportunityInstruction.FirstOrDefault(obj => obj.InstructionId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoOpportunityInstruction.Update(objUpdate);
                    return;
                }
                var entity = new OpportunityInstruction
                {
                    OpportunityId = opportunityId,
                    InstructionId = x,
                    IsActive = true
                };
                _repoOpportunityInstruction.Insert(entity);
            });
            var deleteEntities = currentOpportunityInstruction.Where(x => instructionIds.All(y => y != x.InstructionId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityInstruction.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityInstruction = _repoOpportunityInstruction.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive)
                .Include(x => x.Instruction)
                .ToList();
            return ObjectMapper.Map<List<OpportunityInstructionOutputDto>>(listOpportunityInstruction);
        }

        //Property
        // [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        public List<OpportunityPropertyOutputDto> CreateOrUpdateListOpportunityProperty(long opportunityId, List<long> propertyIds)
        {

            var currentOpportunityProperty = _repoOpportunityProperty.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            propertyIds.ForEach(x =>
            {
                var objUpdate = currentOpportunityProperty.FirstOrDefault(obj => obj.PropertyId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoOpportunityProperty.Update(objUpdate);
                    return;
                }
                var entity = new OpportunityProperty
                {
                    OpportunityId = opportunityId,
                    PropertyId = x,
                    IsActive = true
                };
                _repoOpportunityProperty.Insert(entity);
            });
            var deleteEntities = currentOpportunityProperty.Where(x => propertyIds.All(y => y != x.PropertyId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityProperty.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityProperty = _repoOpportunityProperty.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive).Include(x => x.Property).ToList();
            return ObjectMapper.Map<List<OpportunityPropertyOutputDto>>(listOpportunityProperty);
        }
        //Project
        //   [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        public List<OpportunityProjectOutputDto> CreateOrUpdateListOpportunityProject(long opportunityId, List<long> projectIds)
        {

            var currentOpportunityProject = _repoOpportunityProject.GetAll().Where(x => x.OpportunityId == opportunityId).ToList();
            projectIds.ForEach(x =>
            {
                var objUpdate = currentOpportunityProject.FirstOrDefault(obj => obj.ProjectId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoOpportunityProject.Update(objUpdate);
                    return;
                }
                var entity = new OpportunityProject
                {
                    OpportunityId = opportunityId,
                    ProjectId = x,
                    IsActive = true
                };
                _repoOpportunityProject.Insert(entity);
            });
            var deleteEntities = currentOpportunityProject.Where(x => projectIds.All(y => y != x.ProjectId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoOpportunityProject.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listOpportunityProject = _repoOpportunityProject.GetAll().Where(x => x.OpportunityId == opportunityId && x.IsActive).Include(x => x.Project).ToList();
            return ObjectMapper.Map<List<OpportunityProjectOutputDto>>(listOpportunityProject);
        }

        //export excel
        private DataTable GenarateColumnOpportunity()
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("No",typeof (string)),
                    new DataColumn("CreationTime",typeof (string)),
                    new DataColumn("Status",typeof (string)),
                    new DataColumn("Department",typeof (string)),
                    new DataColumn("Company",typeof (string)),
                    new DataColumn("Contact",typeof (string)),
                    new DataColumn("OpportunityName",typeof (string)),
                    new DataColumn("Probability",typeof (string)),
                    new DataColumn("LeadBy",typeof (string)),
                    new DataColumn("Assigned",typeof (string)),
                    new DataColumn("Link",typeof (string))

                }
            };
            return dt;
        }


        private DataTable GenarateDataOpportunity(DataTable dt, List<ExportOpportunityOutput> data)
        {
            var no = 1;
            data.ToList().ForEach(x =>
            {
                var r = dt.NewRow();
                foreach (DataColumn c in dt.Columns)
                {
                    try
                    {
                        switch (c.ColumnName)
                        {
                            case "No":
                                r[c.ColumnName] = no;
                                continue;
                            case "CreationTime":
                                r[c.ColumnName] = x.CreationTime.ToString();
                                continue;
                            case "OpportunityName":
                                r[c.ColumnName] = x.OpportunityName.ToString();
                                continue;
                            case "Status":
                                r[c.ColumnName] = x.Status;
                                continue;
                            case "Department":
                                r[c.ColumnName] = x.Department;
                                continue;
                            case "Company":
                                r[c.ColumnName] = x.Company;
                                continue;
                            case "Probability":
                                r[c.ColumnName] = x.Probability;
                                continue;
                            //case "Amount":
                            //    r[c.ColumnName] = x.Amount.ToString();
                            //continue;
                            case "LeadBy":
                                r[c.ColumnName] = x.LeadBy.ToString();
                                continue;
                            case "Assigned":
                                r[c.ColumnName] = x.Assigned.ToString();
                                continue;
                            case "Contact":
                                r[c.ColumnName] = x.Contact.ToString();
                                continue;
                            case "Link":
                                r[c.ColumnName] = x.Link.ToString();
                                continue;
                        }
                    }
                    catch { continue; }
                }
                dt.Rows.Add(r);
                no++;
            });
            return dt;
        }

        private async Task<OpportunityOutputDto> GetDetails(long opportunityId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<OpportunityFilterDto>("SpFilterOpportunity",
                    new
                    {
                        @Id = opportunityId
                        ,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<OpportunityOutputDto>>(output).SingleOrDefault();
                return results;
            }
        }

        #endregion
    }
}
