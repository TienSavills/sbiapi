﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Opportunity
{
    public static class OpportunityConstant
    {
        public const string  AssetClass= "AssetClass";
        public const string Industry = "Industry";
        public const string Instruction = "Instruction";
        public const string ClientType = "ClientType";
        public const string LeadSource = "LeadSource";
        public const string OpportunityStatus = "OpportunityStatus";
        public const string OpportunityStage = "OpportunityStage";
        public const string PaymentStatus = "PaymentStatus";
        public const string OppStatusContract = "Contract";
        public const string OppStageOnhold = "On-hold";
        public const string OppStageBilled = "Billed";
        public const string OppStageWip = "Wip";
        public const string OppStageCanceled = "Canceled";
        public const string OppStageSigned = "Signed";
        public const string OppStageInterrupted = "Interrupted";

        public const string HcmcFs = "PL-HCMCFS";
        public const string HcmcRes = "PL-HCMCRES";
        public const string HcmcBcs = "PL-HCMCBCS";
        public const string HcmcIndustrial = "PL-HCMCINDUSTRIAL";
        public const string HcmcBd = "PL-HCMCBD";
        public const string HcmcPm = "PL-HCMCPM";
        public const string HcmcSale = "PL-HCMCSALES";
        public const string HcmcInvestment = "PL-INVESTMENT";
        public const string HcmcLeasing = "PL-HCMCLEASING";
        public const string HcmcRetail = "PL-HCMCRETAIL";

        public const string HnFs = "PL-HNFS";
        public const string HnRes = "PL-HNRES";
        public const string HnBcs = "PL-HNBCS";
        public const string HnIndustrial = "PL-HNINDUSTRIAL";
        public const string HnBd = "PL-HNBD";
        public const string HnPm = "PL-HNPM";
        public const string HnSale = "PL-HNSALES";
        public const string HnInvestment = "PL-HNINVESTMENT";
        public const string HnLeasing = "PL-HNLEASING";
        public const string HnRetail = "PL-HNRETAIL";
    }
}
