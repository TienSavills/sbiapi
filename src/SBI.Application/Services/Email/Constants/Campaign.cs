﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Email.Constants
{
    public class Campaign
    {
        public const string Content = "CONTENT";
        public const string Gender = "GENDER";
        public const string CampaignName = "CAMPAIGN_NAME";
        public const string ContactName = "CONTACTNAME";
        public const string CampaignSubcription = "CAMPAIGN_SUBCRIPTION";
        public const string Link = "CAMPAIGN_LINK";
    }
}
