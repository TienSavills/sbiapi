﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.TenantTypes;
using CRM.Entities.TenantTypes;
using Microsoft.EntityFrameworkCore;

namespace CRM.Services.TenantTypes
{
    public class TenantTypeAppService : SBIAppServiceBase, ITenantTypeAppService
    {
        private readonly IRepository<TenantType, int> _repositoryTenantType;
        public TenantTypeAppService(IRepository<TenantType, int> repositoryTenantType)
        {
            _repositoryTenantType = repositoryTenantType;
        }
        public async Task<PagedResultDto<TenantTypeOutputDto>> GetListTenantType(FilterBasicInputDto input)
        {

            var query = _repositoryTenantType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.TypeName.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber-1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.TypeName)
                .ToListAsync();
            var output = ObjectMapper.Map<List<TenantTypeOutputDto>>(results);
            return new PagedResultDto<TenantTypeOutputDto>(
                totalCount,
                output
            );

        }

        public async Task<List<TenantTypeOutputDto>> GetListTenantType(string keyword)
        {

            var query = _repositoryTenantType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.TypeName.ToUpper().Contains(keyword.ToUpper()));

            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<TenantTypeOutputDto>>(results);

        }
    }
}