﻿using Abp.Application.Services;
using CRM.Application.Shared.ProjectConstructionMaps;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using CRM.Entities.ProjectConstructionMaps;
using CRM.SBIMap;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using Abp.Linq.Extensions;
using System.Linq;
using CRM.EntitiesCustom;
using Abp.UI;
using CRM.Services.Currencys;
using CRM.Entities.Projects;
using Microsoft.EntityFrameworkCore;

namespace CRM.Services.ProjectConstructionMaps
{
    public class ProjectConstructionMapAppService : SBIAppServiceBase, IProjectConstructionMapAppService
    {
        private readonly IRepository<ProjectConstructionMap, long> _repoProjectConstructionMap;
        private readonly IRepository<Project, long> _repoProject;
        private readonly IRepository<Entities.ConstructionStatus.ConstructionStatus, int> _repoConstructionStatus;
        private ConfigSetting _options;
        public ProjectConstructionMapAppService(
            IOptions<ConfigSetting> options
            ,IRepository<ProjectConstructionMap, long> repoProjectConstructionMap
            , IRepository<Project, long> repoProject
            , IRepository<Entities.ConstructionStatus.ConstructionStatus, int> repoConstructionStatus)
        {
            _repoProjectConstructionMap = repoProjectConstructionMap;
            _repoProject = repoProject;
            _repoConstructionStatus = repoConstructionStatus;
            _options = options.Value;
            
        }
        #region public API
        public async Task<ProjectConstructionMapOutputDto> CreateProjectConstructionMapAsync(ProjectConstructionMapInputDto input)
        {
            ValidateCreateOrUpdateProjectConstruction(input);
            var entity = ObjectMapper.Map<ProjectConstructionMap>(input);
            entity.IsActive = true;
            entity.Id = await _repoProjectConstructionMap.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<ProjectConstructionMapOutputDto>(entity);
        }

        public async Task<List<ProjectConstructionMapOutputDto>> FilterProjectConstructionMapAsync(ProjectConstructionMapFilterInputDto filters)
        {
            var query =  _repoProjectConstructionMap.GetAll()
                .Include(x => x.Project)
                .Include(x => x.ConstructionStatus)
                .WhereIf(filters.ProjectId.HasValue,x=>x.ProjectId== filters.ProjectId)
                .WhereIf(!string.IsNullOrEmpty(filters.Keyword)
                , x => x.Project.ProjectName.ToUpper().Contains(filters.Keyword.ToUpper())
                ||x.ConstructionStatus.StateName.ToUpper().Contains(filters.Keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<ProjectConstructionMapOutputDto>>(results);
        }


        public async Task<ProjectConstructionMapOutputDto> GetProjectConstructionMapDetail(long id)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("ProjectConstructionMapIdNotFound"));
            var results = await _repoProjectConstructionMap.GetAll().Where(x => x.Id == id)
                .Include(x => x.Project)
                .Include(x => x.ConstructionStatus).SingleOrDefaultAsync();
            if (results == null)
                throw new UserFriendlyException(L("ProjectConstructionMapIdNotFound"));
            return ObjectMapper.Map<ProjectConstructionMapOutputDto>(results);
        }

        public async Task<ProjectConstructionMapOutputDto> UpdateProjectConstructionMapAsync(long id, ProjectConstructionMapInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("ProjectConstructionMapIdNotFound"));
            var entity = _repoProjectConstructionMap.Get(id);
            if (entity==null)
                throw new UserFriendlyException(L("ProjectConstructionMapIdNotFound"));
            ValidateCreateOrUpdateProjectConstruction(input);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoProjectConstructionMap.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<ProjectConstructionMapOutputDto>(entity);
        }
        #endregion
        #region Private API
        private void ValidateCreateOrUpdateProjectConstruction(ProjectConstructionMapInputDto input)
        {
            if(_repoProject.Count(x=>x.Id==input.ProjectId)==0)
                throw new UserFriendlyException(L("ProjectIdNotFound"));
            if (_repoConstructionStatus.Count(x => x.Id == input.ConstructionStatusId) == 0)
                throw new UserFriendlyException(L("ConstructionStatusIdNotFound"));
        }
        #endregion
    }
}
