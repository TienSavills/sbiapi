﻿using Abp.Application.Services;
using Dapper;
using Microsoft.Extensions.Options;
using CRM.Application.Shared.ReportClaims;
using CRM.SBIMap;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.ReportClaims
{
    public class ReportClaimAppService : SBIAppServiceBase, IReportClaimAppService
    {
        private ConfigSetting _options;
        public ReportClaimAppService(IOptions<ConfigSetting> options)
        {
            _options = options.Value;
        }
        public async  Task<dynamic> GetReportClaim(string paramJson, string nameStore)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>(nameStore,
                new
                {
                    @ParamJson = paramJson
                }, commandType: CommandType.StoredProcedure);
                return output.ToList();
            }
        }
    }
}
