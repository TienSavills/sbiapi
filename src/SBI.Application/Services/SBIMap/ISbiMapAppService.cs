﻿using System.Collections.Generic;
using Abp.Application.Services;

namespace CRM.Services.SBIMap
{
    public interface ISbiMapAppService: IApplicationService
    {
        List<dynamic> GetSBIMap(long? fromValue, long? toValue, string instructionId = null, string statusId = null, string assetClass = null,
          string departmentId = null, string projectType = null);
    }
}
