﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Abp.Domain.Uow;
using Dapper;
using Microsoft.Extensions.Options;
using CRM.SBIMap;

namespace CRM.Services.SBIMap
{
    public class SbiMapAppService : SBIAppServiceBase, ISbiMapAppService
    {
        private ConfigSetting _options;
       public SbiMapAppService(IOptions<ConfigSetting> options)
        {
            _options = options.Value;
        }

        [UnitOfWork(isTransactional: false)]
        public List<dynamic> GetSBIMap(long? fromValue, long? toValue, string instructionId = null, string statusId = null, string assetClass = null,
          string departmentId = null, string projectType = null)
        {
            using (var conn = new SqlConnection(_options.SBIMap))
            {
                var output = conn.Query<dynamic>("SBIGetMap",
                new
                {
                    @InstructionId = instructionId,
                    @StatusId = statusId,
                    @AssetClass = assetClass,
                    @DepartmentId = departmentId,
                    @FromValue = fromValue,
                    @ToValue = toValue,
                    @ProjectType = projectType
                }, commandType: CommandType.StoredProcedure).ToList();
                return output;
            }
        }

        public List<dynamic> GetSource(string tableSource)
        {
            using (var conn = new SqlConnection(_options.SBIMap))
            {
                var output = conn.Query<dynamic>("GetSource",
                new
                {
                    @TableName= tableSource
                }, commandType: CommandType.StoredProcedure).ToList();
                return output;
            }
        }
    }
}
