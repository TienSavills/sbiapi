﻿using System;
using Abp.Notifications;
using CRM.Application.Shared.Dto;

namespace CRM.Services.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}