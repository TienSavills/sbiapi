using System.ComponentModel.DataAnnotations;

namespace CRM.Services.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}