using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using CRM.Authorization.Users;

namespace CRM.Services.Users.Dto
{
    public class RoleName
    {

        public string RoleNames { get; set; }
    }
}
