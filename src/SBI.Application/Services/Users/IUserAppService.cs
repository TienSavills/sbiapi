using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Services.Roles.Dto;
using CRM.Services.Users.Dto;
using CRM.Application.Shared.Users;
using CRM.Application.Shared.CatTypes;

namespace CRM.Services.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task ChangeLanguage(ChangeUserLanguageDto input);
        PagedResultDto<UserDto> GetListUsers(FilterBasicInputDto input);
        Task<UserDto> Get(long id);
        Task<ListResultDto<RoleDto>> GetRoles();
    }
}
