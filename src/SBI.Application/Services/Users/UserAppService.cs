using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using CRM.Authorization;
using CRM.Authorization.Roles;
using CRM.Authorization.Users;
using CRM.Services.Roles.Dto;
using CRM.Services.Users.Dto;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using CRM.Application.Shared.CatTypes;
using CRM.SBIMap;
using Microsoft.Extensions.Options;
using CRM.EntitiesCustom;
using CRM.Notifications.Interfaces;
using Abp.Notifications;
using Abp.Organizations;
using Castle.DynamicProxy;
using CRM.Application.Shared.Users;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using Abp.Extensions;

namespace CRM.Services.Users
{
    //[AbpAuthorize(PermissionNames.Pages_Users)]
    public class UserAppService :
        AsyncCrudAppService<User, UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private ConfigSetting _options;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            IPasswordHasher<User> passwordHasher,
            IAppNotifier appNotifier,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IOptions<ConfigSetting> options)
            : base(repository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
            _appNotifier = appNotifier;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _options = options.Value;
        }

        //[AbpAuthorize(PermissionNames.PagesAdministration_Staff_Create)]
        public override async Task<UserDto> Create(CreateUserDto input)
        {
            CheckCreatePermission();

            var user = ObjectMapper.Map<User>(input);

            user.TenantId = AbpSession.TenantId;
            user.Password = _passwordHasher.HashPassword(user, input.Password);
            user.IsEmailConfirmed = true;

            CheckErrors(await _userManager.CreateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }

            CurrentUnitOfWork.SaveChanges();
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);
            return MapToEntityDto(user);
        }

        public override async Task<UserDto> Update(UserDto input)
        {
            CheckUpdatePermission();

            var user = await _userManager.GetUserByIdAsync(input.Id);

            MapToEntity(input, user);

            CheckErrors(await _userManager.UpdateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }

            return await Get(input);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            await _userManager.DeleteAsync(user);
        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        public PagedResultDto<UserDto> GetListUsers(FilterBasicInputDto input)
        {
            var query = Repository.GetAllIncluding(x => x.Roles)
                .WhereIf(input.IsActive.HasValue, x => x.IsActive == input.IsActive)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x =>  x.UserName.ToUpper().Contains(input.Keyword.ToUpper()) || x.EmailAddress.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = query.Count();
            List<UserDto> output = new List<UserDto>();
            var items = query.OrderByDescending(x => x.LastModificationTime ?? x.CreationTime).Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize).ToList();
            items.ForEach(x => output.Add(MapToEntityDto(x)));
            return new PagedResultDto<UserDto>(
                totalCount,
                output
            );
            //using (var conn = new SqlConnection(_options.SBICon))
            //{
            //    var output = await conn.QueryAsync<UserFilterDto>("[SpFilterUsers]",
            //        new
            //        {
            //            @Keyword=input.Keyword,
            //            @PageNumber = input.PageNumber,
            //            @PageSize=input.PageSize,
            //        }, commandType: CommandType.StoredProcedure);
            //    var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
            //    var results = ObjectMapper.Map<List<UserDto>>(output);
            //    return new PagedResultDto<UserDto>(totalCount, results);
            //}
        }

        public async Task<UserDto> Get(long id)
        {
            var user = await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            return MapToEntityDto(user);
        }

        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await SettingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }

        protected override UserDto MapToEntityDto(User user)
        {
            var roles = _roleManager.Roles.Where(r => user.Roles.Any(ur => ur.RoleId == r.Id))
                .Select(r => r.NormalizedName);
            var userDto = base.MapToEntityDto(user);
            userDto.RoleNames = roles.ToArray();
            return userDto;
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            return user;
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
