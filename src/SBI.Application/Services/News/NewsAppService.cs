﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.BackgroundJobs;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using CRM.Application.Shared.AttachmentDocuments;
using CRM.Application.Shared.News;
using CRM.Application.Shared.Project.Dto;
using CRM.Authorization;
using CRM.Authorization.Users;
using CRM.Configuration;
using CRM.Entities.AttachmentDocuments;
using CRM.Entities.News;
using CRM.Notifications.Interfaces;
using CRM.Services.AttachmentDocuments;
using Microsoft.EntityFrameworkCore;

namespace CRM.Services.News
{
    [AbpAuthorize]
    public class NewsAppService : SbiDomainServiceBase, INewsAppService
    {
        private readonly IRepository<Entities.News.News, long> _repositoryNews;
        private readonly IRepository<NewLanguageTexts> _repositoryNewsLanguageTexts;
        private readonly IRepository<AttachmentDocument, long> _repositoryAttachmentDocument;
        private readonly ITranslationLanguageTextsCore _translationLanguageTextsCore;
        private readonly IConfigHelper _configHelper;

        public NewsAppService(IRepository<Entities.News.News, long> repositoryNews,
            ITranslationLanguageTextsCore translationLanguageTextsCore,
            IRepository<NewLanguageTexts> repositoryNewsLanguageTexts,
            IRepository<AttachmentDocument, long> repositoryAttachmentDocument,
            IConfigHelper configHelper,
            UserManager userManager)
        {
            _repositoryNews = repositoryNews;
            _translationLanguageTextsCore = translationLanguageTextsCore;
            _repositoryNewsLanguageTexts = repositoryNewsLanguageTexts;
            _repositoryAttachmentDocument = repositoryAttachmentDocument;
            _configHelper = configHelper;
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<NewsOutputDto>> GetAllAsync(PagedNewsResultRequestDto input)
        {
            var query = from cat in _repositoryNews.GetAll().Include(x => x.CreatorUser)
                    .Include(x => x.LastModifierUser).OrderBy(x => x.SortOrder)
                        join langText in _repositoryNewsLanguageTexts.GetAll()
                                .Where(x => x.LanguageName == Thread.CurrentThread.CurrentUICulture.Name) on cat.SubjectNameId
                            equals langText.Key into langG
                        from langText in langG.DefaultIfEmpty()
                        where (string.IsNullOrEmpty(input.Keyword) || langText.Value.Contains(input.Keyword))
                              && (!input.IsActive.HasValue || cat.IsActive == input.IsActive)
                              && (input.CategoryIds.Count() == 0 || input.CategoryIds.Contains(cat.CategoryId))
                              && (!input.IsPublic.HasValue || cat.IsPublic == input.IsPublic)
                        select cat;
            var totalCount = await query.CountAsync();
            var items = await query.OrderByDescending(x => x.LastModificationTime ?? x.CreationTime).PageBy(input)
                .ToListAsync();
            var output = ObjectMapper.Map<List<NewsOutputDto>>(items);
            await _translationLanguageTextsCore.AppendTranslation((x, tran) => x.Subject = tran?.Value,
                x => x.SubjectNameId, output.ToArray());
            await _translationLanguageTextsCore.AppendTranslation((x, tran) => x.ShortDescription = tran?.Value,
                x => x.ShortDescriptionNameId, output.ToArray());
            await _translationLanguageTextsCore.AppendTranslation((x, tran) => x.Content = tran?.Value,
                x => x.ContentNameId, output.ToArray());
            await AppendFileUrl<NewsOutputDto, AttachmentDocumentOutputDto>(FileConstant.News, x => x.Id, (x, file) => x.File = file, output.ToArray());
            return new PagedResultDto<NewsOutputDto>(
                totalCount,
                output
            );
        }

        //    [AbpAuthorize(PermissionNames.Pages_Administration_News_Create)]
        [UnitOfWork]
        public async Task<NewsOutputDto> CreateAsync(NewsCreateDto input)
        {
            var entity = ObjectMapper.Map<Entities.News.News>(input);
            AppendInitNewData(entity);
            var id = await _repositoryNews.InsertAndGetIdAsync(entity);
            await _translationLanguageTextsCore.UpdateTranslations(entity.SubjectNameId, input.Subjects);
            await _translationLanguageTextsCore.UpdateTranslations(entity.ContentNameId, input.Contents);
            await _translationLanguageTextsCore.UpdateTranslations(entity.ShortDescriptionNameId,
                input.ShortDescriptions);
            await CurrentUnitOfWork.SaveChangesAsync();
            return ObjectMapper.Map<NewsOutputDto>(entity);
        }

        //  [AbpAuthorize(PermissionNames.Pages_Administration_News_Update)]
        [UnitOfWork]
        public async Task<NewsOutputDto> UpdateAsync(NewsUpdateDto input)
        {
            var entity = await _repositoryNews.GetAsync(input.Id);
            ObjectMapper.Map(input, entity);
            await _repositoryNews.UpdateAsync(entity);
            await _translationLanguageTextsCore.UpdateTranslations(entity.SubjectNameId, input.Subjects);
            await _translationLanguageTextsCore.UpdateTranslations(entity.ContentNameId, input.Contents);
            await _translationLanguageTextsCore.UpdateTranslations(entity.ShortDescriptionNameId,
                input.ShortDescriptions);
            await CurrentUnitOfWork.SaveChangesAsync();
            return ObjectMapper.Map<NewsOutputDto>(entity);
        }

        //      [AbpAuthorize(PermissionNames.Pages_Administration_News_Update)]
        [UnitOfWork]
        public async Task Active(long id, bool isActive)
        {
            var entity = await _repositoryNews.GetAsync(id);
            entity.IsActive = isActive;
            await _repositoryNews.UpdateAsync(entity);
        }

        [UnitOfWork]
        public async Task Public(EntityDto<long> input, bool isPublic)
        {
            var entity = await _repositoryNews.GetAsync(input.Id);
            entity.IsPublic = isPublic;
            await _repositoryNews.UpdateAsync(entity);
        }

        [AbpAllowAnonymous]
        public async Task<NewsOutputDto> GetAsync(long id)
        {
            var entity = await _repositoryNews.GetAll()
                .Include(x => x.Category)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser).FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
                throw new EntityNotFoundException(typeof(Entities.News.News), id);
            var output = ObjectMapper.Map<NewsOutputDto>(entity);
            await _translationLanguageTextsCore.AppendTranslation((x, t) => x.Subject = t.Value,
                x => entity.SubjectNameId, output);
            await _translationLanguageTextsCore.AppendTranslation((x, t) => x.Content = t.Value,
                x => entity.ContentNameId, output);
            await _translationLanguageTextsCore.AppendTranslation((x, t) => x.ShortDescription = t.Value,
                x => entity.ShortDescriptionNameId, output);
            await AppendFileUrl<NewsOutputDto, AttachmentDocumentOutputDto>(FileConstant.News, x => x.Id,
                (x, file) => x.File = file, output);
            return output;
        }

        #region Private Function

        private static void AppendInitNewData(Entities.News.News entity)
        {
            entity.UniqueId = Guid.NewGuid();
            entity.SubjectNameId = Guid.NewGuid();
            entity.ContentNameId = Guid.NewGuid();
            entity.ShortDescriptionNameId = Guid.NewGuid();
        }

        public async Task<NewsEditOutputDto> GetEventForEdit(long id)
        {
            var entity = await _repositoryNews.GetAll()
                .Include(x => x.Category)
                .Include(x => x.CreatorUser)
                .Include(x => x.LastModifierUser)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
                throw new EntityNotFoundException(typeof(Entities.News.News), id);
            var output = ObjectMapper.Map<NewsEditOutputDto>(entity);
            await _translationLanguageTextsCore.AppendTranslations(x => entity.SubjectNameId, (x, lang) => x.Subjects = lang, output);
            await _translationLanguageTextsCore.AppendTranslations(x => entity.ContentNameId, (x, lang) => x.Contents = lang, output);
            await _translationLanguageTextsCore.AppendTranslations(x => entity.ShortDescriptionNameId, (x, lang) => x.ShortDescriptions = lang, output);
            await AppendFileUrl<NewsEditOutputDto, AttachmentDocumentOutputDto>(FileConstant.News, x => x.Id,
                (x, file) => x.File = file, output);
            return output;
        }
        //private async Task<string> AppendUrlMainPhoto(long newId)
        //{
        //    var file = await _repositoryAttachmentDocument.GetAll()
        //        .Where(x => x.ParentModuleName == FileConstant.News && x.ParentId == newId && x.IsMainPhoto)
        //        .SingleOrDefaultAsync();
        //    return _configHelper.GetConfigUrlThumFileId(file.Id.ToString());
        //}
        public async Task AppendFileUrl<TModel, TFile>(string moduleName, Func<TModel, long?> select, Action<TModel, TFile> repareModel, params TModel[] models)
            where TFile : AttachmentDocumentOutputDto
        {
            if (models != null && models.Any())
            {
                var referenceIds = models.Select(x => select.Invoke(x).ToString()).ToList();
                var files = await _repositoryAttachmentDocument.GetAll().Where(x => x.IsMainPhoto && x.ParentModuleName == moduleName.ToUpper() && referenceIds.Contains(x.ParentId.ToString())).ToListAsync();
                foreach (var model in models)
                {
                    var item = files.FirstOrDefault(i => i.ParentId.ToString()?.ToUpper() == select.Invoke(model).ToString()?.ToUpper());
                    if (item != null)
                    {
                        var fileModel = ObjectMapper.Map<TFile>(item);
                        fileModel.UrlThumb = _configHelper.GetConfigUrlThumFileId(item.Id.ToString());
                        fileModel.UrlDownload = _configHelper.GetConfigUrlDownloadFileByFileId(item.Id.ToString());
                        repareModel.Invoke(model, fileModel);
                    }
                    else
                        repareModel.Invoke(model, null);
                }
            }
        }
        #endregion
    }
}
