using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Castle.Core.Logging;
using Microsoft.Extensions.Options;
using CRM.Application.Shared.AttachmentDocuments;
using CRM.Configuration;
using CRM.Entities.AttachmentDocuments;
using CRM.Net.MimeTypes;
using CRM.SBIMap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Abp.Collections.Extensions;
using CRM.Entities.DocumentTypes;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;

namespace CRM.Services.AttachmentDocuments
{
    public class AttachmentDocumentAppService : SBIAppServiceBase, IAttachmentDocumentAppService
    {
        private readonly IRepository<AttachmentDocument, long> _repoAttachmentDocument;
        private readonly IRepository<DocumentType, int> _repoDocumentType;
        private readonly IAppConfigurationAccessor _appConfiguration;
        private readonly IConfigHelper _configHelper;
        public AttachmentDocumentAppService(IRepository<AttachmentDocument, long> repoAttachmentDocument
            , IAppConfigurationAccessor appConfiguration
            , IConfigHelper configHelper
            , IRepository<DocumentType, int> repoDocumentType)
        {
            this._repoAttachmentDocument = repoAttachmentDocument;
            _repoDocumentType = repoDocumentType;
            _appConfiguration = appConfiguration;
            _configHelper = configHelper;
            Logger = NullLogger.Instance;
        }
        [AbpAuthorize]
        public async Task DeleteFile(long fileId)
        {
            var entity = await _repoAttachmentDocument.GetAsync(fileId);
            entity.IsDeleted = true;
            await _repoAttachmentDocument.UpdateAsync(entity);
        }

        [AbpAllowAnonymous]
        public Task<byte[]> DownLoadFileById(long fileId)
        {
            var file = _repoAttachmentDocument.GetAll().Where(x => x.Id == fileId).SingleOrDefault();
            if (file == null) throw new UserFriendlyException("NotExistFile");
            var filePath = Path.Combine(_appConfiguration.Configuration["FilePath"],
               file.TenantId.ToString(),
                file.ParentModuleName.ToUpper(),
                file.ParentId.ToString(),
                file.Id.ToString().ToUpper() + "-" + file.FileName.ToString());

            if (!File.Exists(filePath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"), filePath);
            }
            var fileBytes = File.ReadAllBytesAsync(filePath);
            return fileBytes;
        }
        [AbpAllowAnonymous]
        public Task<byte[]> DownLoadThumbById(long fileId)
        {
            var file = _repoAttachmentDocument.GetAll().Where(x => x.Id == fileId).SingleOrDefault();
            if (file == null) throw new UserFriendlyException("NotExistFile");
            var filePath = Path.Combine(_appConfiguration.Configuration["FilePath"],
               file.TenantId.ToString(),
                file.ParentModuleName.ToUpper(),
                file.ParentId.ToString(),
                file.Id.ToString().ToUpper() + "-Thumb-" + file.FileName.ToString());

            if (!File.Exists(filePath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"), filePath);
            }
            var fileBytes = File.ReadAllBytesAsync(filePath);
            return fileBytes;
        }
        [AbpAllowAnonymous]
        public List<AttachmentDocumentOutputDto> GetByReferenceId(string moduleName, long parentId, string type)
        {
            if (!new string[] { FileConstant.Document, FileConstant.Image }.Contains(type.ToUpper()))
                throw new UserFriendlyException(L("FileTypeNotExist"));
            var listTypeId = FileConstant.Image == type.ToUpper() ? _repoDocumentType.GetAll().Where(x => x.TypeName.ToUpper() == FileConstant.PhotoType).Select(x => x.Id).ToList() : _repoDocumentType.GetAll().Where(x => x.TypeName.ToUpper() != FileConstant.PhotoType).Select(x => x.Id).ToList();
            var listResult = _repoAttachmentDocument.GetAll().Include(x => x.DocumentType)
                .Where(x => listTypeId.Contains(x.DocumentTypeId))
                .Where(x => x.ParentId == parentId && x.ParentModuleName == moduleName.ToUpper() && x.IsDeleted == false)
                .Select(p => ObjectMapper.Map<AttachmentDocumentOutputDto>(p))
                .OrderBy(x => x.OrderNum).ToList();
            listResult.ForEach(x =>
            {
                x.UrlDownload = _configHelper.GetConfigUrlDownloadFileByFileId(x.Id.ToString());
                if (x.MimeType.ToUpper().StartsWith("IMAGE/"))
                    x.UrlThumb = _configHelper.GetConfigUrlThumFileId(x.Id.ToString());
            });
            return listResult;
        }

        [AbpAuthorize]
        public async Task<AttachmentDocumentOutputDto> UpdateDocument(long id, AttachmentDocumentUpdateInputDto input)
        {
            var entity = await _repoAttachmentDocument.GetAsync(id);
            if (entity == null) throw new UserFriendlyException(L("FileNotExist"));
            ObjectMapper.Map(input, entity);
            await _repoAttachmentDocument.UpdateAsync(entity);
            var result = ObjectMapper.Map<AttachmentDocumentOutputDto>(entity);
            return result;
        }
        [AbpAllowAnonymous]
        public async Task<AttachmentDocumentOutputDto> GetDocById(long fileId)
        {
            try
            {
                var file = await _repoAttachmentDocument.GetAsync(fileId);
                var result = ObjectMapper.Map<AttachmentDocumentOutputDto>(file);
                result.UrlDownload = _configHelper.GetConfigUrlDownloadFileByFileId(file.Id.ToString());
                if (result.MimeType.ToUpper().StartsWith("IMAGE/"))
                    result.UrlThumb = _configHelper.GetConfigUrlThumFileId(result.Id.ToString());
                return result;
            }
            catch
            {
                throw new UserFriendlyException(L("FileNotExist"));
            }
        }
        [AbpAuthorize]
        public async Task MarkMainPhoto(string moduleName, long parentId, long fileId)
        {
            await MainPhoto(moduleName, parentId, fileId);
        }
        [AbpAuthorize]
        [UnitOfWork]
        public async Task UploadFile(long parentId, AttachmentDocumentInputDto input)
        {
            try
            {

                //Type is Document or Photo
                ValidateUploadFile(input);
                if (!new string[] { FileConstant.Document, FileConstant.Image }.Contains(input.Type.ToUpper()))
                    throw new UserFriendlyException(L("FileTypeNotExist"));
                int defaultTypeId;
                //Image is PhotoType
                if (FileConstant.Image == input.Type.ToUpper())
                {
                    defaultTypeId = _repoDocumentType.GetAll().Where(x => x.TypeName.ToUpper() == FileConstant.PhotoType).Select(x => x.Id).SingleOrDefault();
                }
                //Document is Others
                else
                {
                    defaultTypeId = _repoDocumentType.GetAll().Where(x => x.TypeName.ToUpper() == FileConstant.Others).Select(x => x.Id).SingleOrDefault();
                }

               
                // 2. Parse Base64
                var fileString = Convert.FromBase64String(input.FileToUpload);
                // 3. Write Info to DB
                var file = ObjectMapper.Map<AttachmentDocument>(input);
                file.TenantId = 1;
                file.DocumentTypeId = defaultTypeId;
                file.IsActive = true;
                var subFileName = file.FileName.Split('.');
                file.Extension = subFileName[subFileName.Count() - 1].ToString();
                file.ParentModuleName = input.ParentModuleName.ToUpper();
                var path = _appConfiguration.Configuration["FilePath"];
                var newPath = Path.Combine(path,
                    file.TenantId.ToString(),
                    file.ParentModuleName,
                    parentId.ToString());
                Logger.Info($"---------Before CreateDirectory: {newPath}");
                Directory.CreateDirectory(newPath);
                Logger.Info($"---------Create path upload new file: {newPath}");
                file.FilePath = Path.Combine(
                   file.TenantId.ToString(),
                     file.ParentModuleName,
                     parentId.ToString());
                file.ParentId = parentId;
                var fileId = await _repoAttachmentDocument.InsertAndGetIdAsync(file);
                // 3. Write file
                var thumFile = Path.Combine(newPath, fileId.ToString() + "-Thumb-" + file.FileName);
                newPath = Path.Combine(newPath, fileId.ToString() + "-" + file.FileName);
                Logger.Info($"---------Write path upload new file: {newPath}");
                await File.WriteAllBytesAsync(newPath, fileString);
                if ((file.MimeType.ToUpper().StartsWith("IMAGE/")))
                {
                    Image image = Image.FromFile(newPath);
                    Image thumb = image.GetThumbnailImage(200, 200, () => false, IntPtr.Zero);
                    thumb.Save(thumFile);
                }
                if (input.IsMainPhoto)
                {
                    await MainPhoto(input.ParentModuleName, parentId, fileId);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        #region Private Method
        private async Task MainPhoto(string moduleName, long parentId, long fileId)
        {
            var listPhoto = _repoAttachmentDocument.GetAll()
                .Where(x => x.ParentModuleName.ToUpper() == moduleName.ToUpper() && x.ParentId == parentId && x.Id != fileId).ToList();
            listPhoto.ForEach(x =>
            {
                x.IsMainPhoto = false;
                _repoAttachmentDocument.Update(x);
            });
            var entity = await _repoAttachmentDocument.GetAsync(fileId);
            if (entity == null) throw new UserFriendlyException(L("FileNotExist"));
            entity.IsMainPhoto = true;
            await _repoAttachmentDocument.UpdateAsync(entity);
        }
        private void ValidateUploadFile(AttachmentDocumentInputDto input)
        {
            if (string.IsNullOrEmpty(input.FileName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.FileName)));
            if (string.IsNullOrEmpty(input.FileToUpload))
                throw new UserFriendlyException(L("RequiredField", nameof(input.FileToUpload)));
            if (string.IsNullOrEmpty(input.ParentModuleName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.ParentModuleName)));
            if (new string[] {FileConstant.Property, FileConstant.Project, FileConstant.Floor
                , FileConstant.Unit, FileConstant.Company, FileConstant.Contact, FileConstant.Opportunity
                , FileConstant.Stream,FileConstant.Request, FileConstant.Deal, FileConstant.Invoice, FileConstant.Listing, FileConstant.News}
                .Contains(input.ParentModuleName.ToUpper()) == false)
                throw new UserFriendlyException(L("NotExist", nameof(input.ParentModuleName)));
            if (string.IsNullOrEmpty(input.MimeType))
                throw new UserFriendlyException(L("RequiredField", nameof(input.MimeType)));
            if (string.IsNullOrEmpty(input.Type))
                throw new UserFriendlyException(L("RequiredField", nameof(input.Type)));
        }
        #endregion
    }
}
