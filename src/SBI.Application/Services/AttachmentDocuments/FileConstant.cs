﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.AttachmentDocuments
{
    public class FileConstant
    {
        public const string Property = "PROPERTY";
        public const string Company = "COMPANY";
        public const string Project = "PROJECT";
        public const string Floor = "FLOOR";
        public const string Unit = "UNIT";
        public const string Contact = "CONTACT";
        public const string Opportunity = "OPPORTUNITY";
        public const string Stream = "STREAM";
        public const string Request = "REQUEST";
        public const string Deal = "DEAL";
        public const string Invoice = "INVOICE";
        public const string Listing = "LISTING";
        public const string News = "NEWS";

        public const string Image = "IMAGE";
        public const string Document = "DOCUMENT";
        public const string PhotoType = "PHOTOS";
        public const string Others = "OTHERS";
    }
}
