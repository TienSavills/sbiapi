﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using CRM.Application.Shared.Rate;
using CRM.Application.Shared.Statistic;
using CRM.Application.Shared.Unit;
using CRM.Application.Shared.UnitHistory;
using CRM.Configuration;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using Dapper;
using Microsoft.Extensions.Options;

namespace CRM.Services.Statistic
{
    [AbpAuthorize]
    public class StatisticAppService : SBIAppServiceBase, IStatisticAppService
    {
        private ConfigSetting _options;
        private readonly IConfigHelper _configHelper;
        public StatisticAppService(
              IOptions<ConfigSetting> options
              , IConfigHelper configHelper
            )
        {
            _options = options.Value;
            _configHelper = configHelper;
        }
        public async Task<dynamic> CountStatusCompanyAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("[SpCountCompanyStatus]",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<dynamic> CountStatusContactAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("[SpCountContactStatus]",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<dynamic> CountStatusDealAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("[SpCountDealStatus]",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<dynamic> CountStatusOpportunityAsync(DateTime? dateFrom, DateTime? dateTo, int[] organizationUnitIds, string departmentIds, int? officeId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                IEnumerable<dynamic> output = await conn.QueryAsync<dynamic>("[SpCountOpportunityStatus]",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @DateFrom = dateFrom,
                        @DateTo = dateTo,
                        @DepartmentIds = departmentIds,
                        @OrganizationUnitIds = base.ParseToString(organizationUnitIds),
                        @OfficeId = officeId
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<dynamic> CountStatusOpportunityAsyncV1(DateTime? dateFrom, DateTime? dateTo, int[] departmentIds, int? officeId)
        {
            string depart = string.Empty;
            if (departmentIds.Any())
            {
                foreach (var department in departmentIds)
                {
                    depart = depart + "," + department.ToString();
                }
            }
            else
            {
                depart = null;
            }
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("[SpCountOpportunityStatus]",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @DateFrom = dateFrom,
                        @DateTo = dateTo,
                        @DepartmentIds = depart,
                        @OfficeId = officeId
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<dynamic> CountStatusPropertyAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("[SpCountPropertyStatus]",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<dynamic> CountStatusRequestAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("[SpCountRequestStatus]",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<List<UnitOutputDto>> GetListUnitExpiryAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitFilterDto>("SpUnitExpiry",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<UnitOutputDto>>(output);
                return new List<UnitOutputDto>(results);
            }
        }
        public async Task<dynamic> CountUnitExpiryAsync(long? projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("spCountUnitExpiry",
                    new
                    {
                        @ProjectId = projectId
                        ,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<dynamic> CountUnitStatusAsync(long? projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("spCountUnitStatus",
                    new
                    {
                        @ProjectId = projectId
                        ,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountUnitTypeAsync(long? projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("spCountUnitType",
                    new
                    {
                        @ProjectId = projectId
                        ,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountOpportunityDeparmentStatusAsync(DateTime? dateFrom, DateTime? dateTo, int[] organizationUnitIds, string departmentIds, int? officeId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpCountOpportunityDepartmentStatus",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @DateFrom = dateFrom,
                        @DateTo = dateTo,
                        @DepartmentIds = departmentIds,
                        @OrganizationUnitIds = base.ParseToString(organizationUnitIds),
                        @OfficeId = officeId
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountOpportunityStageAsync(DateTime? dateFrom, DateTime? dateTo, string departmentIds, int? officeId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpCountOpportunityStage",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @DateFrom = dateFrom,
                        @DateTo = dateTo,
                        @DepartmentIds = departmentIds,
                        @OfficeId = officeId
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountOpportunityStageAsyncV1(DateTime? dateFrom, DateTime? dateTo, int[] departmentIds, int? officeId)
        {
            string depart = string.Empty;
            if (departmentIds.Any())
            {
                foreach (var department in departmentIds)
                {
                    depart = depart + "," + department.ToString();
                }
            }
            else
            {
                depart = null;
            }
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpCountOpportunityStage",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @DateFrom = dateFrom,
                        @DateTo = dateTo,
                        @DepartmentIds = depart,
                        @OfficeId = officeId
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> ReportCommercial(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpReportCommercial",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @FilterJson = input
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountOpportunityLastModifyAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpCountOpportunityLastModify",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountLastModifyAsync()
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpCountContactLastModify",
                    new
                    {
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> DealOpportunityRevenue(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpPivotRevenue",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @FilterJson = input
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> MMMReport(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("crm_PivotFinancialRpt",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @FilterJson = input
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> UserActivities(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpReportUserActivity",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @FilterJson = input
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> UserActivitiesFromTo(DateTime dateFrom, DateTime dateTo, string teamIds, int? officeId, string keyword)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("SpReportUserActivity1",
                    new
                    {
                        @UserId = AbpSession.UserId.Value,
                        @DateFrom = dateFrom,
                        @DateTo = dateTo,
                        @TeamIds = teamIds,
                        @OfficeId = officeId,
                        @Keyword = keyword
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountProjectTypeAsync(long? projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("spCountProjectType",
                    new
                    {
                        @ProjectId = projectId
                        ,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountProjectFacilityAsync(long? projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("spCountProjectFacility",
                    new
                    {
                        @ProjectId = projectId
                        ,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }
        public async Task<dynamic> CountProjectGradeAsync(long? projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("spCountProjectGrade",
                    new
                    {
                        @ProjectId = projectId
                        ,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output;
            }
        }

        public async Task<RateOutputDto> ExchangeRate(string basic)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<RateOutputDto>("ExchangeRate",
                    new
                    {
                        @Basic = basic
                    }, commandType: CommandType.StoredProcedure);
                return output.SingleOrDefault();
            }
        }

        public string GetLinkDashboard()
        {
           return _configHelper.GetLinkDashboard();
        }
    }
}
