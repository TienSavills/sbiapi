﻿using System.Collections.Generic;
using System.Linq;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using CRM.Application.Shared.Contact;
//using CRM.Entities.ProjectTenant;
//using CRM.Entities.ProjectLandlord;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using Abp.Application.Services.Dto;
using CRM.SBIMap;
using Microsoft.Extensions.Options;
using System.Data;
using CRM.EntitiesCustom;
//using CRM.Entities.PropertyLandlords;
//using CRM.Entities.PropertyTenants;
//using CRM.Entities.TenantTypes;
using System;
using System.Text.RegularExpressions;
using System.Globalization;
using CRM.Entities.ContactAddress;
using CRM.Entities.Provinces;
using CRM.Entities.Districs;
using CRM.Entities.Countries;
using CRM.Entities.ContactOrganizationUnit;
using CRM.Entities.ContactUser;
using CRM.Entities.OtherCategory;
using CRM.Entities.ContactEmail;
using CRM.Entities.ContactPhone;
using CRM.Application.Shared.ContactEmail;
using CRM.Application.Shared.ContactPhone;
using CRM.Application.Shared.ContactUser;
using CRM.Application.Shared.ContactOrganizationUnit;
using CRM.Services.Contact;
using Abp.Collections.Extensions;
using CRM.Application.Shared.CatTypes;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.ContactAddress;
using CRM.Entities.CompanyContact;
using Abp.Authorization;
using Abp.Runtime.Session;
using Castle.Core.Internal;
using CRM.Services.Organizations;
using CRM.Application.Shared.ContactTypeMap;
using CRM.Entities.CompanyTypeMap;
using CRM.Entities.ContactTypeMap;
using CRM.Email;
using Castle.DynamicProxy.Generators;
using CRM.Authorization;

namespace CRM.Services.Contact
{
    [AbpAuthorize]
    public class ContactAppService : SBIAppServiceBase, IContactAppService
    {
        private readonly IRepository<Entities.Contacts.Contact, long> _repositoryContact;
        private readonly IRepository<ContactAddress, long> _repositoryContactAddress;
        private readonly IRepository<Province, int> _repositoryProvince;
        private readonly IRepository<District, int> _repositoryDistrict;
        private readonly IRepository<Country, int> _repositoryCountry;
        private readonly IRepository<ContactOrganizationUnit, long> _repositoryContactOrganizationUnit;
        private readonly IRepository<ContactUser, long> _repositoryContactUser;
        private readonly IRepository<OtherCategory, int> _repositoryOtherCategory;
        private readonly IRepository<ContactEmail, long> _repositoryContactEmail;
        private readonly IRepository<ContactPhone, long> _repositoryContactPhone;
        private readonly IRepository<CompanyContact, long> _repositoryCompanyContact;
        private readonly OrganizationUnitAppService _servicesOrganizationUnit;
        private readonly IRepository<Entities.OpportunityContact.OpportunityContact, long> _repositoryOpportunityContact;
        private readonly IRepository<Entities.Activity.Activity, long> _repositoryActivity;
        private readonly IRepository<Entities.Comment.Comment, long> _repositoryComment;
        private readonly IRepository<Entities.ContactTypeMap.ContactTypeMap, long> _repositoryContactTypeMap;
        private readonly IEmailSenderHelper _iEmailSenderHelper;

        private ConfigSetting _options;
        bool invalid = false;
        public ContactAppService(IRepository<Entities.Contacts.Contact, long> repositoryContact
             , IRepository<Entities.Comment.Comment, long> repositoryComment
            , IRepository<ContactAddress, long> repositoryContactAddress
            , IRepository<Province, int> repositoryProvince
            , IRepository<District, int> repositoryDistrict
            , IRepository<Country, int> repositoryCountry
            , IRepository<ContactEmail, long> repositoryContactEmail
            , IRepository<ContactPhone, long> repositoryContactPhone
            , IRepository<ContactOrganizationUnit, long> repositoryContactOrganizationUnit
            , IRepository<ContactUser, long> repositoryContactUser
            , IRepository<OtherCategory, int> repositoryOtherCategory
            , IRepository<CompanyContact, long> repositoryCompanyContact
            , IRepository<Entities.ContactTypeMap.ContactTypeMap, long> repositoryContactTypeMap
            , OrganizationUnitAppService servicesOrganizationUnit
            , IRepository<Entities.OpportunityContact.OpportunityContact, long> repositoryOpportunityContact
            , IRepository<Entities.Activity.Activity, long> repositoryActivity
            , IEmailSenderHelper iEmailSenderHelper
            , IOptions<ConfigSetting> options
            )
        {
            _repositoryContact = repositoryContact;
            _repositoryContactTypeMap = repositoryContactTypeMap;
            _repositoryComment = repositoryComment;
            _repositoryActivity = repositoryActivity;
            _repositoryContactAddress = repositoryContactAddress;
            _repositoryProvince = repositoryProvince;
            _repositoryDistrict = repositoryDistrict;
            _repositoryContactEmail = repositoryContactEmail;
            _repositoryContactPhone = repositoryContactPhone;
            _repositoryContactOrganizationUnit = repositoryContactOrganizationUnit;
            _repositoryContactUser = repositoryContactUser;
            _repositoryOtherCategory = repositoryOtherCategory;
            _repositoryCountry = repositoryCountry;
            _servicesOrganizationUnit = servicesOrganizationUnit;
            _repositoryCompanyContact = repositoryCompanyContact;
            _repositoryOpportunityContact = repositoryOpportunityContact;
            _iEmailSenderHelper = iEmailSenderHelper;
            _options = options.Value;
        }
        #region Public API
        public async Task<bool> MergeContact(List<long> sourceContactIds, long targetContactId)
        {
            //check permission
            var ou = await _servicesOrganizationUnit.GetOrganizationUnitsByUser(AbpSession.UserId.Value);
            if (!ou.Items.Any(x => x.Id == Organizations.OrganizationsConstant.Manager))
            {
                throw new UserFriendlyException(L("ContactSBITeam"));
            }

            // update from activity source to activity target
            foreach (var sourceContactId in sourceContactIds)
            {
                var sourceActivitys = await _repositoryActivity.GetAll().Where(x => x.ReferenceId == sourceContactId && x.IsActive == true
                && x.ModuleId == Activiy.ActivityConstant.Contact).ToListAsync();
                foreach (var sourceActivity in sourceActivitys)
                {
                    sourceActivity.ReferenceId = targetContactId;
                    await _repositoryActivity.InsertOrUpdateAsync(sourceActivity);
                }
            }

            // update from comment source to comment target
            foreach (var sourceContactId in sourceContactIds)
            {
                var sourceComments = await _repositoryComment.GetAll().Where(x => x.ReferenceId == sourceContactId && x.IsActive == true
                && x.ModuleId == Activiy.ActivityConstant.Contact).ToListAsync();
                foreach (var sourceComment in sourceComments)
                {
                    sourceComment.ReferenceId = targetContactId;
                    await _repositoryComment.InsertOrUpdateAsync(sourceComment);
                }
            }
            // update from contact source to contact target
            var targetContactIds = await _repositoryCompanyContact.GetAll().Where(x => x.ContactId == targetContactId && x.IsActive == true).Select(x => x.ContactId).ToListAsync();
            foreach (var sourceContactId in sourceContactIds)
            {
                var sourceContacts = await _repositoryCompanyContact.GetAll().Where(x => x.ContactId == sourceContactId && x.IsActive == true).ToListAsync();
                foreach (var sourceContact in sourceContacts)
                {
                    if (!targetContactIds.Contains(sourceContact.ContactId))
                    {
                        sourceContact.ContactId = targetContactId;
                        sourceContact.IsPrimary = false;
                        await _repositoryCompanyContact.InsertOrUpdateAsync(sourceContact);
                    }
                }
            }
            // update from opportunity source to opportunity target
            var targetOpportunityIContactds = await _repositoryOpportunityContact.GetAll().Where(x => x.ContactId == targetContactId && x.IsActive == true).Select(x => x.Id).ToListAsync();
            foreach (var sourceContactId in sourceContactIds)
            {
                var sourceOpportunityContacts = await _repositoryOpportunityContact.GetAll().Where(x => x.ContactId == sourceContactId && x.IsActive == true).ToListAsync();
                foreach (var sourceOpportunityContact in sourceOpportunityContacts)
                {
                    if (!targetOpportunityIContactds.Contains(sourceOpportunityContact.Id))
                    {
                        sourceOpportunityContact.ContactId = targetContactId;
                        await _repositoryOpportunityContact.InsertOrUpdateAsync(sourceOpportunityContact);
                    }
                }
            }

            // update from contact phone source to contact phone target
            var targetContactPhones = await _repositoryContactPhone.GetAll().Where(x => x.ContactId == targetContactId && x.IsActive == true).Select(x => x.Phone).ToListAsync();
            foreach (var sourceContactId in sourceContactIds)
            {
                var sourceContactPhones = await _repositoryContactPhone.GetAll().Where(x => x.ContactId == sourceContactId && x.IsActive == true).ToListAsync();
                foreach (var sourceContactPhone in sourceContactPhones)
                {
                    if (!targetContactPhones.Contains(sourceContactPhone.Phone))
                    {
                        sourceContactPhone.ContactId = targetContactId;
                        sourceContactPhone.IsPrimary = false;
                        await _repositoryContactPhone.InsertOrUpdateAsync(sourceContactPhone);
                    }
                }
            }
            // update from contact email source to contact email target
            var targetContactEmails = await _repositoryContactEmail.GetAll().Where(x => x.ContactId == targetContactId && x.IsActive == true).Select(x => x.Email).ToListAsync();
            foreach (var sourceContactId in sourceContactIds)
            {
                var sourceContactEmails = await _repositoryContactEmail.GetAll().Where(x => x.ContactId == sourceContactId && x.IsActive == true).ToListAsync();
                foreach (var sourceContactEmail in sourceContactEmails)
                {
                    if (!targetContactEmails.Contains(sourceContactEmail.Email))
                    {
                        sourceContactEmail.ContactId = targetContactId;
                        sourceContactEmail.IsPrimary = false;
                        await _repositoryContactEmail.InsertOrUpdateAsync(sourceContactEmail);
                    }
                }
            }
            // deactive source Contact
            foreach (var sourceContactId in sourceContactIds)
            {
                var sourceContacts = await _repositoryContact.GetAll().Where(x => x.Id == sourceContactId && x.IsActive == true).ToListAsync();
                foreach (var sourceContact in sourceContacts)
                {
                    sourceContact.IsActive = false;
                    sourceContact.Description = " Merged from " + targetContactId.ToString();
                    await _repositoryContact.InsertOrUpdateAsync(sourceContact);
                }
            }
            return true;
        }

        public bool CheckExistContact(ContactValidateInputDto input)
        {
            if (input.ContactPhones == null) throw new UserFriendlyException(L("InputPhone"));
            input.ContactPhones.ForEach(ValidateContactPhone);
            if (input.ContactEmails == null) throw new UserFriendlyException(L("InputEmail"));
            input.ContactEmails.ForEach(ValidateContactEmail);
            if (input.ContactCompany == null) throw new UserFriendlyException(L("InputCompany"));
            input.ContactCompany.ForEach(ValidateContactCompany);
            input.ContactAddress?.ForEach(ValidateContactAddress);
            input.ContactUsers?.ForEach(ValidateContactUser);
            input.ContactOrganizationUnits?.ForEach(ValidateContactOrganizationUnit);
            return true;
        }

        public bool RequestShare(long contactId)
        {
            if (!_repositoryContactUser.GetAll().Any(x => x.ContactId == contactId && x.UserId == AbpSession.UserId.Value))
            {
                ContactUser entity = new ContactUser
                {
                    ContactId = contactId
                    ,
                    UserId = AbpSession.UserId.Value
                    ,
                    IsRequest = true
                    ,
                    IsFollwer = false
                };
                entity.Id = _repositoryContactUser.InsertOrUpdateAndGetId(entity);
                return true;
            }
            return true;
        }

        public bool ApproveShare(long id)
        {
            var contactUser = _repositoryContactUser.GetAll().Where(x => x.Id == id).SingleOrDefault();
            if (contactUser == null) throw new UserFriendlyException("NotExistId");
            contactUser.IsActive = true;
            _repositoryContactUser.Update(contactUser);
            return true;
        }

        public bool RejectShare(long id)
        {
            var contactUser = _repositoryContactUser.GetAll().Where(x => x.Id == id).SingleOrDefault();
            if (contactUser == null) throw new UserFriendlyException("NotExistId");
            contactUser.IsRequest = false;
            _repositoryContactUser.Update(contactUser);
            return true;
        }

        public async Task<List<ExistContactDto>> GetListPendingApprove(string filterJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ExistContactDto>("SpApprovePending",
                    new
                    {
                        @FilterJson = filterJson,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                return output.ToList();
            }
        }

        public async Task<ContactOutputDto> CreateOrUpdateContactAsync(ContactFullInputDto input)
        {
            ValidateCreateOrUpdateContactFull(input);
            CheckHiddenContact(input);
            var entity = new Entities.Contacts.Contact();
            if (input.Id > 0)
            {
                entity = await _repositoryContact.GetAsync(input.Id);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repositoryContact.UpdateAsync(entityUpdate);
            }
            else
            {
                var entityInsert = ObjectMapper.Map(input, entity);
                entity.IsActive = true;
                entity.IsVerified = true;
                entity.Id = await _repositoryContact.InsertAndGetIdAsync(entityInsert);
            }
            if (input.ContactAddress != null)
            {
                ValidateContactListAddress(input.ContactAddress);
                await UpdateChilds(
                      _repositoryContactAddress,
                      x => x.ContactId = entity.Id,
                      x => x.ContactId == entity.Id,
                      (x, y) => x.Id == y.Id,
                      input.ContactAddress.ToArray()
                      );
            }
            if (input.ContactCompany != null)
            {
                ValidateContactCompanyList(entity.Id, input.ContactCompany);
                await UpdateChilds(
                  _repositoryCompanyContact,
                  x => x.ContactId = entity.Id,
                  x => x.ContactId == entity.Id,
                  (x, y) => x.CompanyId == y.CompanyId,
                  input.ContactCompany.Select(x => new ContactCompanyInputDto { ContactId = entity.Id, IsPrimary = x.IsPrimary, CompanyId = x.CompanyId, IsActive = true }).ToArray()
                         );
            }
            if (input.ContactPhone != null)
            {
                ValidateContactListPhone(input.ContactPhone);
                await UpdateChilds(
                _repositoryContactPhone,
                x => x.ContactId = entity.Id,
                x => x.ContactId == entity.Id,
                (x, y) => x.Phone == y.Phone,
                input.ContactPhone.Select(x => new ContactPhoneInputDto
                {
                    ContactId = entity.Id,
                    CountryId = x.CountryId,
                    Phone =x.Phone,
                    PhoneTypeId = x.PhoneTypeId,
                    IsPrimary = x.IsPrimary,
                    IsActive = true
                }).ToArray()
                );
            }
            if (input.ContactEmail != null)
            {
                ValidateContactListEmail(entity.Id, input.ContactEmail);
                await UpdateChilds(
                _repositoryContactEmail,
                x => x.ContactId = entity.Id,
                x => x.ContactId == entity.Id,
                (x, y) => x.Email == y.Email,
                input.ContactEmail.Select(x => new ContactEmailInputDto
                {
                    ContactId = entity.Id,
                    Email = x.Email,
                    IsPrimary = x.IsPrimary,
                    IsInvalid = x.IsInvalid,
                    IsOptedOut = x.IsOptedOut,
                    IsActive = true
                }).ToArray());
            }
            if (input.ContactTypeIds != null)
            {
                ValidateContactTypeV1(input.ContactTypeIds);
                await UpdateChilds(
                      _repositoryContactTypeMap,
                      x => x.ContactId = entity.Id,
                      x => x.ContactId == entity.Id,
                      (x, y) => x.ContactTypeId == y.ContactTypeId,
                      input.ContactTypeIds.Select(x => new ContactTypeMapInputDto { ContactTypeId = x, ContactId = entity.Id, IsActive = true }).ToArray()
                      );
            }
            if (input.ContactOrganizationUnitIds != null)
            {
                await UpdateChilds(
                     _repositoryContactOrganizationUnit,
                     x => x.ContactId = entity.Id,
                     x => x.ContactId == entity.Id,
                     (x, y) => x.OrganizationUnitId == y.OrganizationUnitId,
                     input.ContactOrganizationUnitIds.Select(x => new ContactOrganizationUnitInputDto { OrganizationUnitId = x, ContactId = entity.Id, IsActive = true }).ToArray()
                     );
            }
            if (input.ContactUserIds != null)
            {
                await UpdateChilds(
                         _repositoryContactUser,
                         x => x.ContactId = entity.Id,
                         x => x.ContactId == entity.Id,
                         (x, y) => x.UserId == y.UserId,
                         input.ContactUserIds.Select(x => new ContactUserInputDto { UserId = x, ContactId = entity.Id, IsActive = true }).ToArray()
                         );
            }
            return await GetDetails(entity.Id, true);
        }

        public async Task<ContactOutputDto> CreateContactAsync(ContactInputDto input)
        {
            ValidateCreateOrUpdateContact(input);
            var entity = ObjectMapper.Map<Entities.Contacts.Contact>(input);
            entity.IsActive = true;
            entity.IsVerified = true;
            entity.Id = await _repositoryContact.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<ContactOutputDto>(entity);
        }

        public async Task<ContactOutputDto> UpdateContactAsync(long contactId, ContactInputDto input)
        {
            if (contactId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("ContactIdNotFound"));
            ValidateCreateOrUpdateContact(input);
            var entity = await _repositoryContact.GetAsync(contactId);
            entity.Id = contactId;
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repositoryContact.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<ContactOutputDto>(entity);
        }

        public async Task<ContactOutputDto> UpdateContactAddressAsync(long contactId, List<ContactAddressInputDto> contactAddress)
        {
            var entity = await _repositoryContact.GetAsync(contactId);
            var output = ObjectMapper.Map<ContactOutputDto>(entity);
            if (contactAddress != null && contactAddress.Any())
            {
                ValidateContactListAddress(contactAddress);
                output.ContactAddress = ObjectMapper.Map<List<ContactAddressOutputDto>>(CreateOrUpdateListContactAddress(contactId, contactAddress));
            }
            return output;
        }

        public async Task<PagedResultDto<ContactOutputDto>> GetListContact(string filterJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ContactFilterDto>("[SpFilterContact]",
                    new
                    {
                        @IsShowFull = await ShowFullContact(false),
                        @FilterJson = filterJson,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ContactOutputDto>>(output);
                return new PagedResultDto<ContactOutputDto>(totalCount, results);
            }
        }

        public async Task<List<ContactOutputDto>> FilterContactForTarget(string filterJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ContactFilterDto>("[SpFilterContactForTarget]",
                    new
                    {
                        @FilterJson = filterJson,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ContactOutputDto>>(output);
                return results;
            }
        }

        public async Task<PagedResultDto<ContactOutputDto>> GetListExistContact(string filterJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ContactFilterDto>("SpExistContact",
                    new
                    {
                        @FilterJson = filterJson,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ContactOutputDto>>(output);
                return new PagedResultDto<ContactOutputDto>(totalCount, results);
            }
        }

        public async Task<ContactOutputDto> GetContactDetails(long id, bool isShowFull)
        {
            return await GetDetails(id, isShowFull);
        }

        public async Task<PagedResultDto<ContactCompanyOutputDto>> GetListContactCompany(FilterBasicInputDto input, long contactId)
        {
            var query = _repositoryCompanyContact.GetAll()
              .Include(x => x.Company)
              .Where(x => x.ContactId == contactId);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ContactCompanyOutputDto>>(results);
            return new PagedResultDto<ContactCompanyOutputDto>(
                totalCount,
                output
            );
        }

        public List<ContactCompanyOutputDto> CreateOrUpdateContactCompany(long contactId, List<ContactCompanyInputDto> input)
        {
            ValidateContactCompanyList(contactId, input);
            var output = ObjectMapper.Map<List<ContactCompanyOutputDto>>(CreateOrUpdateListContactCompany(contactId, input));
            return output;
        }

        private List<ContactCompanyOutputDto> CreateOrUpdateListContactCompanyV1(long contactId, List<ContactCompanyInputDto> contactCompany)
        {

            var currentContactCompany = _repositoryCompanyContact.GetAll().Where(x => x.ContactId == contactId).ToList();
            var contactCompanyIds = contactCompany.Select(x => x.Id).ToList();
            var entities = new List<CompanyContact>();
            contactCompany.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactCompany(contactId, x));
            });
            var deleteEntities = currentContactCompany.Where(x => contactCompanyIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryCompanyContact.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listContactCompany = _repositoryCompanyContact.GetAll().Where(x => x.ContactId == contactId && x.IsActive);
            return ObjectMapper.Map<List<ContactCompanyOutputDto>>(listContactCompany);
        }

        public List<ContactPhoneOutputDto> CreateOrUpdateContactPhone(long contactId, List<ContactPhoneInputDto> contactPhones)
        {
            ValidateContactListPhone(contactPhones);
            var output = ObjectMapper.Map<List<ContactPhoneOutputDto>>(CreateOrUpdateListContactPhone(contactId, contactPhones));
            return output;
        }

        public List<ContactEmailOutputDto> CreateOrUpdateContactEmail(long contactId, List<ContactEmailInputDto> contactEmails)
        {
            ValidateContactListEmail(contactId, contactEmails);
            var output = ObjectMapper.Map<List<ContactEmailOutputDto>>(CreateOrUpdateListContactEmail(contactId, contactEmails));
            return output;
        }

        public List<ContactUserOutputDto> CreateOrUpdateContactUser(long contactId, List<ContactUserInputDto> contactUsers)
        {
            ValidateContactListUser(contactUsers);
            var output = ObjectMapper.Map<List<ContactUserOutputDto>>(CreateOrUpdateListContactUser(contactId, contactUsers));
            return output;
        }

        public List<ContactOrganizationUnitOutputDto> CreateOrUpdateContactOrganizationUnit(long contactId, List<ContactOrganizationUnitInputDto> contactContactOrganizationUnits)
        {
            ValidateContactListOrganizationUnit(contactId, contactContactOrganizationUnits);
            var output = ObjectMapper.Map<List<ContactOrganizationUnitOutputDto>>(CreateOrUpdateListContactOrganizationUnit(contactId, contactContactOrganizationUnits));
            return output;
        }

        public List<ContactTypeMapOutputDto> CreateOrUpdateContactTypeMap(List<ContactTypeMapInputDto> input)
        {
            var output = ObjectMapper.Map<List<ContactTypeMapOutputDto>>(CreateOrUpdateListContactType(input));
            return output;
        }

        private List<ContactTypeMapOutputDto> CreateOrUpdateListContactTypeV1(long contactId, List<int> contactTypeIds)
        {

            var currentContactTypes = _repositoryContactTypeMap.GetAll().Where(x => x.ContactId == contactId).ToList();
            contactTypeIds.ForEach(x =>
            {
                var objUpdate = currentContactTypes.FirstOrDefault(obj => obj.ContactTypeId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryContactTypeMap.Update(objUpdate);
                    return;
                }
                var entity = new ContactTypeMap
                {
                    ContactId = contactId,
                    ContactTypeId = x,
                    IsActive = true
                };
                _repositoryContactTypeMap.Insert(entity);
            });
            var deleteEntities = currentContactTypes.Where(x => contactTypeIds.All(y => y != x.ContactTypeId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryContactTypeMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listContactTypeMaps = _repositoryContactTypeMap.GetAll().Where(x => x.ContactId == contactId && x.IsActive)
                .Include(x => x.ContactType).ToList();
            return ObjectMapper.Map<List<ContactTypeMapOutputDto>>(listContactTypeMaps);
        }

        private List<ContactOrganizationUnitV1OutputDto> CreateOrUpdateListContactOrganizationUnitV1(long contactId, List<long> organizationUnitIds)
        {
            var currentContactOrganizationUnits = _repositoryContactOrganizationUnit.GetAll().Where(x => x.ContactId == contactId).ToList();
            organizationUnitIds.ForEach(x =>
            {
                var objUpdate = currentContactOrganizationUnits.FirstOrDefault(obj => obj.OrganizationUnitId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryContactOrganizationUnit.Update(objUpdate);
                    return;
                }
                var entity = new ContactOrganizationUnit
                {
                    ContactId = contactId,
                    OrganizationUnitId = x,
                    IsActive = true
                };
                _repositoryContactOrganizationUnit.Insert(entity);
            });
            var deleteEntities = currentContactOrganizationUnits.Where(x => organizationUnitIds.All(y => y != x.OrganizationUnitId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryContactOrganizationUnit.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listContactOrganizationUnits = _repositoryContactOrganizationUnit.GetAll().Where(x => x.ContactId == contactId && x.IsActive)
                .Include(x => x.OrganizationUnit).ToList();
            return ObjectMapper.Map<List<ContactOrganizationUnitV1OutputDto>>(listContactOrganizationUnits);
        }

        public async Task<PagedResultDto<ContactEmailOutputDto>> GetListContactEmail(FilterBasicInputDto input, long contactId)
        {
            var query = _repositoryContactEmail.GetAll()
                .Where(x => x.ContactId == contactId && x.IsActive)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.Email.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ContactEmailOutputDto>>(results);
            return new PagedResultDto<ContactEmailOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ContactEmailOutputDto>> GetListStatusContactEmail(FilterBasicInputDto input)
        {
            var query = _repositoryContactEmail.GetAll()
                .Include(x => x.Contact)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.Email.ToUpper().Contains(input.Keyword.ToUpper()))
                .WhereIf((input.IsInvalid.HasValue),
                    x => x.IsInvalid == input.IsInvalid.Value);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ContactEmailOutputDto>>(results);
            return new PagedResultDto<ContactEmailOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ContactPhoneOutputDto>> GetListContactPhone(FilterBasicInputDto input, long contactId)
        {
            var query = _repositoryContactPhone.GetAll()
                .Include(x => x.PhoneType)
                .Where(x => x.ContactId == contactId && x.IsActive)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.Phone.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ContactPhoneOutputDto>>(results);
            return new PagedResultDto<ContactPhoneOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ContactOrganizationUnitOutputDto>> GetListContactOrganizationUnit(FilterBasicInputDto input, long contactId)
        {
            var query = _repositoryContactOrganizationUnit.GetAll()
                .Include(x => x.OrganizationUnit)
                .Where(x => x.ContactId == contactId && x.IsActive);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ContactOrganizationUnitOutputDto>>(results);
            return new PagedResultDto<ContactOrganizationUnitOutputDto>(
                totalCount,
                output
            );
        }

        public async Task<PagedResultDto<ContactUserOutputDto>> GetListContactUser(FilterBasicInputDto input, long contactId)
        {
            var query = _repositoryContactUser.GetAll()
                .Include(x => x.User)
                .Where(x => x.ContactId == contactId && x.IsActive);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<ContactUserOutputDto>>(results);
            return new PagedResultDto<ContactUserOutputDto>(
                totalCount,
                output
            );
        }


        #endregion

        #region Action
        private List<CompanyContact> CreateOrUpdateListContactCompany(long contactId, List<ContactCompanyInputDto> inputs)
        {
            var entities = new List<CompanyContact>();
            inputs.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactCompany(contactId, x));
            });
            return entities;
        }
        private List<ContactTypeMapOutputDto> CreateOrUpdateListContactType(List<ContactTypeMapInputDto> inputs)
        {
            ValidateContactType(inputs);
            var entities = new List<ContactTypeMapOutputDto>();
            inputs.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactType(x));
            });
            return entities;
        }
        private List<ContactAddress> CreateOrUpdateListContactAddress(long contactId, List<ContactAddressInputDto> contactAddress)
        {

            var entities = new List<ContactAddress>();
            contactAddress.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactAddress(contactId, x));
            });
            return entities;
        }
        private List<ContactAddressOutputDto> CreateOrUpdateListContactAddressV1(long contactId, List<ContactAddressInputDto> contactAddress)
        {

            var currentContactAddress = _repositoryContactAddress.GetAll().Where(x => x.ContactId == contactId).ToList();
            var contactAddressIds = contactAddress.Select(x => x.Id).ToList();
            var entities = new List<ContactAddress>();
            contactAddress.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactAddress(contactId, x));
            });
            var deleteEntities = currentContactAddress.Where(x => contactAddressIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryContactAddress.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listContactAddress = _repositoryContactAddress.GetAll().Where(x => x.ContactId == contactId && x.IsActive);
            return ObjectMapper.Map<List<ContactAddressOutputDto>>(listContactAddress);
        }
        private List<ContactEmail> CreateOrUpdateListContactEmail(long contactId, List<ContactEmailInputDto> contactEmails)
        {

            var entities = new List<ContactEmail>();
            contactEmails.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactEmail(contactId, x));
            });
            return entities;
        }
        private List<ContactEmailOutputDto> CreateOrUpdateListContactEmailV1(long contactId, List<ContactEmailInputDto> contactEmail)
        {

            var currentContactEmail = _repositoryContactEmail.GetAll().Where(x => x.ContactId == contactId).ToList();
            var contactEmailIds = contactEmail.Select(x => x.Id).ToList();
            var entities = new List<ContactEmail>();
            contactEmail.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactEmail(contactId, x));
            });
            var deleteEntities = currentContactEmail.Where(x => contactEmailIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryContactEmail.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listContactEmail = _repositoryContactEmail.GetAll().Where(x => x.ContactId == contactId && x.IsActive);
            return ObjectMapper.Map<List<ContactEmailOutputDto>>(listContactEmail);
        }
        private List<ContactPhone> CreateOrUpdateListContactPhone(long contactId, List<ContactPhoneInputDto> contactPhones)
        {
            var entities = new List<ContactPhone>();
            contactPhones.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactPhone(contactId, x));
            });
            return entities;
        }
        private List<ContactPhoneOutputDto> CreateOrUpdateListContactPhoneV1(long contactId, List<ContactPhoneInputDto> contactPhone)
        {

            var currentContactPhone = _repositoryContactPhone.GetAll().Where(x => x.ContactId == contactId).ToList();
            var contactPhoneIds = contactPhone.Select(x => x.Id).ToList();
            var entities = new List<ContactPhone>();
            contactPhone.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactPhone(contactId, x));
            });
            var deleteEntities = currentContactPhone.Where(x => contactPhoneIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryContactPhone.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listContactPhone = _repositoryContactPhone.GetAll().Where(x => x.ContactId == contactId && x.IsActive);
            return ObjectMapper.Map<List<ContactPhoneOutputDto>>(listContactPhone);
        }
        private List<ContactUser> CreateOrUpdateListContactUser(long contactId, List<ContactUserInputDto> contactUsers)
        {
            var entities = new List<ContactUser>();
            contactUsers.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactUser(contactId, x));
            });
            return entities;
        }
        private List<ContactOrganizationUnit> CreateOrUpdateListContactOrganizationUnit(long contactId, List<ContactOrganizationUnitInputDto> contactOrganizationUnits)
        {

            var entities = new List<ContactOrganizationUnit>();
            contactOrganizationUnits.ForEach(x =>
            {
                entities.Add(CreateOrUpdateContactOrganizationUnit(contactId, x));
            });
            return entities;
        }
        #endregion

        #region Validate
        private bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }

        public void ValidateContactIds(List<long> contactIds)
        {
            if (_repositoryContact.GetAll().Count(x => contactIds.Contains(x.Id) && x.IsActive) < contactIds.Count)
                throw new UserFriendlyException(L("ContactIdNotFound"));
        }
        public void ValidateCreateOrUpdateContact(ContactInputDto input)
        {
            if (string.IsNullOrEmpty(input.ContactName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.ContactName)));
            if (string.IsNullOrEmpty(input.Gender))
                throw new UserFriendlyException(L("RequiredField", nameof(input.Gender)));
            if (!input.NationalityId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.NationalityId)));
        }
        public void ValidateCreateOrUpdateContactFull(ContactFullInputDto input)
        {
            if (string.IsNullOrEmpty(input.ContactName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.ContactName)));
            if (string.IsNullOrEmpty(input.Gender))
                throw new UserFriendlyException(L("RequiredField", nameof(input.Gender)));
            if (!input.NationalityId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.NationalityId)));
        }
        private void ValidateContactTypeV1(List<int> contactTypeIds)
        {
            if (_repositoryOtherCategory.Count(x => contactTypeIds.Contains(x.Id) && x.IsActive) < contactTypeIds.Count)
                throw new UserFriendlyException(L("ContactTypeNotFound"));
        }
        private void ValidateContactType(List<ContactTypeMapInputDto> contactTypeMap)
        {
            contactTypeMap.ForEach(ValidateContactType);
        }
        private void ValidateContactType(ContactTypeMapInputDto contactType)
        {
            if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == contactType.ContactTypeId))
                throw new UserFriendlyException(L("ContactTypeNotFound"));
            if (contactType.Id > 0)
            {
                if (_repositoryContactTypeMap.GetAll().Any(x => x.Id != contactType.Id && x.ContactId == contactType.ContactId && x.ContactTypeId == contactType.ContactTypeId && x.IsActive == true))
                    throw new UserFriendlyException(L("ExistContactType"));
            }
            else
            {
                if (_repositoryContactTypeMap.GetAll().Any(x => x.ContactId == contactType.ContactId && x.ContactTypeId == contactType.ContactTypeId && x.IsActive == true))
                    throw new UserFriendlyException(L("ExistContactType"));
            }
        }
        private void ValidateContactCompanyList(long contactId, List<ContactCompanyInputDto> inputs)
        {
            if (!inputs.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("NeedOneCompanyIsPrimary")); ;
            inputs.ForEach(ValidateContactCompany);
        }

        private void ValidateContactCompany(ContactCompanyInputDto input)
        {
            if (input.Id > 0)
            {
                if (!_repositoryCompanyContact.GetAll().Any(x => x.Id == input.Id))
                    throw new UserFriendlyException(L("ContactCompanyIdNotFound"));
            }
            else
            {
                if (_repositoryCompanyContact.GetAll().Any(x => x.ContactId == input.CompanyId
                && x.CompanyId == input.CompanyId && x.IsActive))
                    throw new UserFriendlyException(L("ExistContactCompany"));
            }
        }
        private void ValidateContactListAddress(List<ContactAddressInputDto> contactAddress)
        {
            contactAddress.ForEach(ValidateContactAddress);
        }

        private void ValidateContactAddress(ContactAddressInputDto contactAddress)
        {
            if (contactAddress.Id > 0)
            {
                if (!_repositoryContactAddress.GetAll().Any(x => x.Id == contactAddress.Id))
                    throw new UserFriendlyException(L("ContactAddressIdNotFound"));
            }
            if (contactAddress.DistrictId.HasValue)
            {
                if (!_repositoryDistrict.GetAll().Any(x => x.Id == contactAddress.DistrictId.Value))
                    throw new UserFriendlyException(L("ContactDistrictIdNotFound"));
            }
            if (contactAddress.ProvinceId.HasValue)
            {
                if (!_repositoryProvince.GetAll().Any(x => x.Id == contactAddress.ProvinceId.Value))
                    throw new UserFriendlyException(L("ContactProvinceIdNotFound"));
            }
            if (!_repositoryCountry.GetAll().Any(x => x.Id == contactAddress.CountryId))
                throw new UserFriendlyException(L("ContactCountryIdNotFound"));
            if (contactAddress.Longitude != null && contactAddress.Latitude != null)
            {
                if (contactAddress.Latitude < AppConsts.GeocodeValues.MinLat
                    || contactAddress.Latitude > AppConsts.GeocodeValues.MaxLat
                    || contactAddress.Longitude < AppConsts.GeocodeValues.MinLng
                    || contactAddress.Longitude > AppConsts.GeocodeValues.MaxLng)
                    throw new UserFriendlyException(L("ContactLatLngInvalid"));
            }
            if (string.IsNullOrEmpty(contactAddress.Address))
                throw new UserFriendlyException(L("RequiredField", nameof(ContactAddress.Address)));
        }

        public void ValidateTennatContact(List<long> contactIds, List<int> tenantTypeIds)
        {
            if (_repositoryContact.GetAll().Count(x => contactIds.Contains(x.Id) && x.IsActive) < contactIds.Count)
                throw new UserFriendlyException(L("ContactIdNotFound"));
            //if (_repositoryTenantType.GetAll().Count(x => tenantTypeIds.Contains(x.Id) && x.IsActive) < tenantTypeIds.Count)
            //    throw new UserFriendlyException(L("TenantTypeIdNotFound"));
        }

        //ContactEmail
        private void ValidateContactListEmail(long contactId, List<ContactEmailInputDto> contactEmails)
        {
            if (!contactEmails.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("NeedOneEmailIsPriamry"));
            contactEmails.ForEach(ValidateContactEmail);
        }
        private void ValidateContactEmail(ContactEmailInputDto contactEmail)
        {
            if (string.IsNullOrEmpty(contactEmail.Email))
            {
                throw new UserFriendlyException(L("RequiredField", nameof(ContactEmail.Email)));
            }

            //update
            if (contactEmail.Id > 0)
            {
                if (!_repositoryContactEmail.GetAll().Any(x => x.Id == contactEmail.Id))
                    throw new UserFriendlyException(L("ContactEmailIdNotFound"));
                if (_repositoryContactEmail.GetAll().Include(x => x.Contact).Any(x => x.Email.ToUpper() == contactEmail.Email.ToUpper()
                  && x.Id != contactEmail.Id && x.IsActive && x.Contact.IsActive && !contactEmail.Email.Contains(ContactConstant.Hiden)))
                    throw new UserFriendlyException(L("ExistEmail", nameof(contactEmail.Email)));
            }
            //insert
            else
            {
                if (_repositoryContactEmail.GetAll().Include(x => x.Contact)
                    .Any(x => x.Email.ToUpper() == contactEmail.Email.ToUpper() && x.IsActive && x.Contact.IsActive))
                    throw new UserFriendlyException(L("ExistEmail", nameof(contactEmail.Email)));
            }
        }
        //ContactPhone
        private void ValidateContactListPhone(List<ContactPhoneInputDto> contactPhones)
        {
            if (!contactPhones.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("NeedOnePhoneIsPrimary"));
            contactPhones.ForEach(ValidateContactPhone);
        }
        private void ValidateContactPhone(ContactPhoneInputDto contactPhone)
        {
            if (string.IsNullOrEmpty(contactPhone.Phone))
            {
                throw new UserFriendlyException(L("RequiredField", nameof(contactPhone.Phone)));
            }
            if (contactPhone.PhoneTypeId > 0)
            {
                if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == contactPhone.PhoneTypeId
                && x.TypeCode.ToUpper().Contains(ContactConstant.PhoneType) && x.IsActive))
                    throw new UserFriendlyException(L("NotExistPhoneType"));
            }
            if (contactPhone.Id > 0)
            {
                if (!_repositoryContactPhone.GetAll().Any(x => x.Id == contactPhone.Id))
                    throw new UserFriendlyException(L("CompanyPhoneIdNotFound"));
                if (_repositoryContactPhone.GetAll().Include(x => x.Contact)
                    .Any(x => x.Phone == contactPhone.Phone && x.Id != contactPhone.Id && x.Contact.IsActive && x.IsActive == true&& !contactPhone.Phone.Contains(ContactConstant.Hiden)))
                    throw new UserFriendlyException(L("ExistPhone"));
            }
            else
            {
                if (_repositoryContactPhone.GetAll().Include(x => x.Contact).Any(x => x.Phone == contactPhone.Phone && x.Contact.IsActive && x.IsActive == true))
                    throw new UserFriendlyException(L("ExistPhone"));
            }

        }
        //ContactUser
        private void ValidateContactListUser(List<ContactUserInputDto> contactUsers)
        {
            contactUsers.ForEach(ValidateContactUser);
        }
        private void ValidateContactUser(ContactUserInputDto contactUser)
        {
            if (contactUser.Id > 0)
            {
                if (!_repositoryContactUser.GetAll().Any(x => x.Id == contactUser.Id))
                    throw new UserFriendlyException(L("ContactUserIdNotFound"));
            }
            else
            {
                if (_repositoryContactUser.GetAll().Any(x => x.ContactId == contactUser.ContactId && x.IsActive == true && x.UserId == contactUser.UserId && x.IsFollwer == contactUser.IsFollwer))
                    throw new UserFriendlyException(L("ExistContactUser"));
            }
        }
        private void ValidateContactUser(long contactId, long userId)
        {
            if (_repositoryContactUser.GetAll().Any(x => x.ContactId == contactId && x.UserId == userId))
                throw new UserFriendlyException(L("ExistContactUser"));
        }
        //OrganizationUnit
        private void ValidateContactListOrganizationUnit(long contactId, List<ContactOrganizationUnitInputDto> contactOrganizationUnits)
        {
            contactOrganizationUnits.ForEach(ValidateContactOrganizationUnit);
        }
        private void ValidateContactOrganizationUnit(ContactOrganizationUnitInputDto contactOrganizationUnit)
        {
            if (contactOrganizationUnit.Id > 0)
            {
                if (!_repositoryContactOrganizationUnit.GetAll().Any(x => x.Id == contactOrganizationUnit.Id))
                    throw new UserFriendlyException(L("ContactOrganizationIdNotFound"));
            }
            else
            {
                if (_repositoryContactOrganizationUnit.GetAll().Any(x => x.ContactId == contactOrganizationUnit.ContactId && x.IsActive == true && x.OrganizationUnitId == contactOrganizationUnit.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistContactOrganizationUnit"));
            }
        }
        #endregion

        #region Private API
        [UnitOfWork]
        private ContactEmail CreateOrUpdateContactEmail(long contactId, ContactEmailInputDto input)
        {
            if (IsValidEmail(input.Email))
            {
                ContactEmail entity;
                var checkMail = _iEmailSenderHelper.CheckEmailAsync(input.Email).Result;
                if (input.Id > 0)
                {
                    var entityEmail = _repositoryContactEmail.Get(input.Id);
                    entity = ObjectMapper.Map(input, entityEmail);
                }
                else
                {
                    entity = ObjectMapper.Map<ContactEmail>(input);
                    entity.IsActive = true;
                    //update emmail must run job check email
                }
                if (checkMail.debounce.code != "0")
                {
                    entity.IsCheckEmail = true;
                    entity.Code = checkMail.debounce.code;
                    var transactional = checkMail.debounce.send_transactional;
                    entity.IsInvalid = transactional == "0" ? true : false;
                    entity.Reason = checkMail.debounce.reason;
                    entity.Result = checkMail.debounce.result;
                    entity.Role = checkMail.debounce.role;
                    entity.SendTransactional = checkMail.debounce.send_transactional;
                    entity.FeeEmail = checkMail.debounce.free_email;
                }
                entity.ContactId = contactId;
                entity.Id = _repositoryContactEmail.InsertOrUpdateAndGetId(entity);
                return entity;
            }
            else
            {
                throw new UserFriendlyException(L("ErrorFormatEmail"));
            }
        }
        [UnitOfWork]
        private CompanyContact CreateOrUpdateContactCompany(long contactId, ContactCompanyInputDto input)
        {
            CompanyContact entity;
            if (input.Id > 0)
            {
                var entityContact = _repositoryCompanyContact.Get(input.Id);
                entity = ObjectMapper.Map(input, entityContact);
            }
            else
            {
                entity = ObjectMapper.Map<CompanyContact>(input);
                entity.IsActive = true;
            }
            entity.ContactId = contactId;
            entity.Id = _repositoryCompanyContact.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        [UnitOfWork]
        private ContactTypeMapOutputDto CreateOrUpdateContactType(ContactTypeMapInputDto input)
        {
            ContactTypeMap entity = new ContactTypeMap();
            if (input.Id > 0)
            {
                var entityContact = _repositoryContactTypeMap.Get(input.Id);
                entity = ObjectMapper.Map(input, entityContact);
            }
            else
            {
                entity = ObjectMapper.Map<ContactTypeMap>(input);
            }
            entity.ContactId = input.ContactId;
            entity.Id = _repositoryContactTypeMap.InsertOrUpdateAndGetId(entity);
            var contactType = _repositoryContactTypeMap.GetAll().Where(x => x.Id == entity.Id)
               .Include(x => x.ContactType).SingleOrDefault();
            var result = ObjectMapper.Map<ContactTypeMapOutputDto>(contactType);
            return result;
        }
        [UnitOfWork]
        private ContactPhone CreateOrUpdateContactPhone(long contactId, ContactPhoneInputDto input)
        {
            ContactPhone entity;
            if (input.Id > 0)
            {
                var entityPhone = _repositoryContactPhone.Get(input.Id);
                entity = ObjectMapper.Map(input, entityPhone);
            }
            else
            {
                entity = ObjectMapper.Map<ContactPhone>(input);
                entity.IsActive = true;
            }
            entity.ContactId = contactId;
            entity.Id = _repositoryContactPhone.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        [UnitOfWork]
        private ContactUser CreateOrUpdateContactUser(long contactId, ContactUserInputDto input)
        {
            ContactUser entity;
            if (input.Id > 0)
            {
                var entityUser = _repositoryContactUser.Get(input.Id);
                entity = ObjectMapper.Map(input, entityUser);
            }
            else
            {
                entity = ObjectMapper.Map<ContactUser>(input);
            }
            entity.ContactId = contactId;
            entity.Id = _repositoryContactUser.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        private List<ContactUserOutputDto> CreateOrUpdateListContactUserV1(long contactId, List<long> userIds)
        {

            var currentContactUsers = _repositoryContactUser.GetAll().Where(x => x.ContactId == contactId).ToList();
            userIds.ForEach(x =>
            {
                var objUpdate = currentContactUsers.FirstOrDefault(obj => obj.UserId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryContactUser.Update(objUpdate);
                    return;
                }
                var entity = new ContactUser
                {
                    ContactId = contactId,
                    UserId = x,
                    IsFollwer = true,
                    IsActive = true
                };
                _repositoryContactUser.Insert(entity);
            });
            var deleteEntities = currentContactUsers.Where(x => userIds.All(y => y != x.UserId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryContactUser.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listContactUsers = _repositoryContactUser.GetAll().Where(x => x.ContactId == contactId && x.IsActive)
                .Include(x => x.User).ToList();
            return ObjectMapper.Map<List<ContactUserOutputDto>>(listContactUsers);
        }
        [UnitOfWork]
        private ContactOrganizationUnit CreateOrUpdateContactOrganizationUnit(long contactId, ContactOrganizationUnitInputDto input)
        {
            ContactOrganizationUnit entity;
            if (input.Id > 0)
            {
                var entityOrganizationUnit = _repositoryContactOrganizationUnit.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityOrganizationUnit);
            }
            else
            {
                entity = ObjectMapper.Map<ContactOrganizationUnit>(input);
            }
            entity.ContactId = contactId;
            entity.Id = _repositoryContactOrganizationUnit.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        [UnitOfWork]
        private ContactAddress CreateOrUpdateContactAddress(long contactId, ContactAddressInputDto input)
        {
            ContactAddress entity;
            if (input.Id >= 0)
            {
                var entityAddress = _repositoryContactAddress.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityAddress);
            }
            else
            {
                entity = ObjectMapper.Map<ContactAddress>(input);
            }
            entity.ContactId = contactId;
            entity.Id = _repositoryContactAddress.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        private async Task<ContactOutputDto> GetDetails(long id, bool isShowFull)
        {

            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ContactFilterDto>("[SpFilterContact]",
                    new
                    {
                        @IsShowFull = await ShowFullContact(isShowFull),
                        @Id = id,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var result = ObjectMapper.Map<ContactOutputDto>(output.SingleOrDefault());
                return result;
            }
        }
        private async Task<bool> ShowFullContact(bool isShowFull)
        {
            if (await PermissionChecker.IsGrantedAsync(AbpSession.ToUserIdentifier(),
                    PermissionNames.PagesAdministration_Contact_FullDetail)
                && isShowFull == true) return true;
            return false;
        }

        private ContactFullInputDto CheckHiddenContact( ContactFullInputDto input)
        {
           input.ContactPhone.ForEach(x=>
           {
               if (x.Phone.Contains(ContactConstant.Hiden))
               {
                   var entity =  _repositoryContactPhone.Get(x.Id);
                   x.Phone = entity.Phone;
               }
           });
           input.ContactEmail.ForEach(x =>
           {
               if (x.Email.Contains(ContactConstant.Hiden))
               {
                   var entity = _repositoryContactEmail.Get(x.Id);
                   x.Email = entity.Email;
               }
           });
           return input;
        }

        #endregion

    }
}
