﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Organizations;
using Abp.UI;
using CRM.Application.Shared.Deal;
using CRM.Application.Shared.DealAdjust;
using CRM.Application.Shared.DealInvoiceInfo;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Application.Shared.Payment;
using CRM.Authorization.Users;
using CRM.Entities.DealAdjust;
using CRM.Entities.DealOpportunity;
using CRM.Entities.DealShare;
using CRM.Entities.OpportuityCategory;
using CRM.Entities.Payment;
using CRM.EntitiesCustom;
using CRM.Notifications.Interfaces;
using CRM.SBIMap;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CRM.Services.Opportunity;

namespace CRM.Services.Deal
{
    [AbpAuthorize]
    public class DealAppService : SBIAppServiceBase, IDealAppService
    {
        private ConfigSetting _options;
        private readonly IRepository<Entities.Opportunity.Opportunity, long> _repoOpportunity;
        private readonly IRepository<DealShare, long> _repoDealShare;
        private readonly IRepository<DealOpportunity, long> _repoDealOpportunity;
        private readonly IRepository<OrganizationUnit, long> _repoOrganizationUnit;
        private readonly IRepository<Entities.Deal.Deal, long> _repoDeal;
        private readonly IRepository<DealAdjust, long> _repoDealAdjust;
        private readonly IRepository<Entities.Payment.Payment, long> _repoPayment;
        private readonly IRepository<OpportunityCategory, int> _repoOpportunityCategory;
        private readonly UserManager _userManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        public DealAppService
            (
            IRepository<Entities.Opportunity.Opportunity, long> repoOpportunity
            , IRepository<DealShare, long> repoDealShare
            , IRepository<DealOpportunity, long> repoDealOpportunity
            , IRepository<Entities.Deal.Deal, long> repoDeal
            , IRepository<Payment, long> repoPayment
            , IRepository<OpportunityCategory, int> repoOpportunityCategory
            , IRepository<OrganizationUnit, long> repoOrganizationUnit
            , IRepository<DealAdjust, long> repoDealAdjust
            , UserManager userManager
            , IAppNotifier appNotifier
            , IRepository<OrganizationUnit, long> organizationUnitRepository
            , IOptions<ConfigSetting> options
            )
        {
            _repoOpportunity = repoOpportunity;
            _repoDealShare = repoDealShare;
            _repoDealOpportunity = repoDealOpportunity;
            _repoDeal = repoDeal;
            _appNotifier = appNotifier;
            _repoOpportunityCategory = repoOpportunityCategory;
            _repoOrganizationUnit = repoOrganizationUnit;
            _repoPayment = repoPayment;
            _userManager = userManager;
            _repoDealAdjust = repoDealAdjust;
            _organizationUnitRepository = organizationUnitRepository;
            _options = options.Value;
        }
        public async Task<DealOutputDto> CreateDealAsync(DealInputDto input)
        {
            var output = await CreateOrUpdateDealAsync(null, input);
            return output;
        }
        public async Task<DealOutputDto> CreateOrDealFullAsync(DealFullInputDto input)
        {
            var output = await CreateOrUpdateDealFullAsync(input);
            return output;
        }
        public async Task<PagedResultDto<TrackingInvoiceOutputDto>> SpFilterTrackingInvoices(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TrackingInvoiceFilterDto>("SpFilterTrackingInvoices",
                   new
                   {
                       @FilterJson = input,
                       @UserId = AbpSession.UserId.Value
                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<TrackingInvoiceOutputDto>>(output);
                return new PagedResultDto<TrackingInvoiceOutputDto>(totalCount, results);
            }
        }
        public async Task<TrackingInvoiceOutputDto> GetDealTrackingInvoice(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TrackingInvoiceFilterDto>("SpFilterTrackingInvoices",
                   new
                   {
                       @Id = id,
                       @UserId = AbpSession.UserId.Value
                   }, commandType: CommandType.StoredProcedure);
                var result = ObjectMapper.Map<TrackingInvoiceOutputDto>(output.SingleOrDefault());
                return result;
            }
        }
        public async Task<PagedResultDto<DealOutputDto>> FilterDeal(string input)
        {
            return await FilterDealGroupDepartment(input, null);
        }
        public async Task<PagedResultDto<DealOutputDto>> FilterDealAdvisory(string input)
        {
            var departmentIds = await GetDepartmentIds(new List<string>(new string[]
            {
                OpportunityConstant.HcmcBcs, OpportunityConstant.HcmcFs, OpportunityConstant.HcmcRes,
                OpportunityConstant.HnBcs, OpportunityConstant.HnFs, OpportunityConstant.HnRes
            }));
            return await FilterDealGroupDepartment(input, departmentIds);
        }
        public async Task<PagedResultDto<DealOutputDto>> FilterDealCommercial(string input)
        {
            var departmentIds = await GetDepartmentIds(new List<string>(new string[]
            {
                OpportunityConstant.HcmcIndustrial, OpportunityConstant.HnIndustrial
                , OpportunityConstant.HcmcPm, OpportunityConstant.HnPm
                , OpportunityConstant.HcmcBd, OpportunityConstant.HnBd
                , OpportunityConstant.HcmcInvestment, OpportunityConstant.HnInvestment
                , OpportunityConstant.HcmcSale, OpportunityConstant.HnSale
                , OpportunityConstant.HcmcLeasing, OpportunityConstant.HnLeasing
                , OpportunityConstant.HcmcRetail, OpportunityConstant.HnRetail
            }));
            return await FilterDealGroupDepartment(input, departmentIds);
        }
        public async Task<DealInvoiceInfoOutputDto> GetDealInvoiceInfo(long dealId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<DealInvoiceInfoOutputDto>("GetDealInvoiceInfo",
                   new
                   {
                       @DealId = dealId,
                       @UserId = AbpSession.UserId.Value
                   }, commandType: CommandType.StoredProcedure);
                return output.SingleOrDefault();
            }
        }
        public async Task<DealOutputDto> GetDealDetail(long dealId)
        {
            return await GetDetails(dealId);
        }
        public async Task<dynamic> GetRptInvoice(long paymentId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("rptInvoice",
                    new
                    {
                        @PaymentId = paymentId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<dynamic>>(output).SingleOrDefault();
                return results;
            }
        }
        public async Task<dynamic> GetInvoice(long paymentId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<dynamic>("rptInvoice1",
                    new
                    {
                        @PaymentId = paymentId
                    }, commandType: CommandType.StoredProcedure);
                return output.SingleOrDefault();
            }
        }
        public async Task<DealOutputDto> UpdateDealAsync(long dealId, DealInputDto input)
        {
            if (dealId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("DealIdNotFound"));
            var output = await CreateOrUpdateDealAsync(dealId, input);
            return output;
        }
        public List<DealShareOutputDto> CreateOrUpdateDealShare(List<DealShareInputDto> dealShares)
        {
            ValidateListDealShare(dealShares);
            var output = ObjectMapper.Map<List<DealShareOutputDto>>(CreateOrUpdateListDealShare(dealShares));
            return output;
        }
        private List<DealShareOutputDto> CreateOrUpdateDealShareV1(long dealId, List<DealShareInputDto> dealShare)
        {
            var currentDealShare = _repoDealShare.GetAll().Where(x => x.DealId == dealId).ToList();
            var dealShareIds = dealShare.Select(x => x.Id).ToList();
            var entities = new List<DealShare>();
            dealShare.ForEach(x =>
            {
                entities.Add(CreateOrUpdateDealShare(dealId, x));
            });
            var deleteEntities = currentDealShare.Where(x => dealShareIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoDealShare.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listDealShare = _repoDealShare.GetAll().Where(x => x.DealId == dealId && x.IsActive);
            return ObjectMapper.Map<List<DealShareOutputDto>>(listDealShare);
        }
        private List<DealAdjustOutputDto> CreateOrUpdateDealAdjustV1(long dealId, List<DealAdjustInputDto> dealAdjust)
        {
            var currentDealAdjust = _repoDealAdjust.GetAll().Where(x => x.DealId == dealId).ToList();
            var dealAdjustIds = dealAdjust.Select(x => x.Id).ToList();
            var entities = new List<DealAdjust>();
            dealAdjust.ForEach(x =>
            {
                entities.Add(CreateOrUpdateDealAdjust(dealId, x));
            });
            var deleteEntities = currentDealAdjust.Where(x => dealAdjustIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoDealAdjust.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listDealAdjust = _repoDealAdjust.GetAll().Where(x => x.DealId == dealId && x.IsActive);
            return ObjectMapper.Map<List<DealAdjustOutputDto>>(listDealAdjust);
        }
        public List<DealOpportunityOutputDto> CreateOrUpdateDealOpportunity(List<DealOpportunityInputDto> dealOpportunitys)
        {
            ValidateListDealOpportunity(dealOpportunitys);
            var dealId = dealOpportunitys.Select(x => x.DealId).FirstOrDefault();
            var listOppId = dealOpportunitys.Select(x => x.OpportunityId);
            var currentOppId = _repoDealOpportunity.GetAll().Where(x => x.DealId == dealId && x.IsActive).Select(y => y.OpportunityId);
            var totalFee = _repoOpportunity.GetAll().Where(x => listOppId.Contains(x.Id) || currentOppId.Contains(x.Id)).Sum(y => y.Amount);
            var output = ObjectMapper.Map<List<DealOpportunityOutputDto>>(CreateOrUpdateListDealOpportunity(dealOpportunitys));
            var deal = _repoDeal.Get(dealId);
            deal.FeeAmount = totalFee.Value;
            _repoDeal.Update(deal);
            return output;
        }
        public List<DealOpportunityOutputDto> CreateOrUpdateDealOpportunityV1(long dealId, List<DealOpportunityInputDto> dealOpportunitys)
        {

            ValidateListDealOpportunity(dealOpportunitys);
            var output = new List<DealOpportunityOutputDto>();
            dealOpportunitys.ForEach(x =>
            {
                output.Add(CreateOrUpdateDealOpportunityV1(dealId, x));
            });
            return output;
        }
        public List<PaymentOutputDto> CreateOrUpdatePayment(List<PaymentInputDto> payments)
        {
            ValidateListPayment(payments);
            var output = ObjectMapper.Map<List<PaymentOutputDto>>(CreateOrUpdateListPayment(payments, false));
            if (output != null)
            {
                var totalPaymentAmount = payments.Sum(x => x.EstAmount);
            }
            return output;
        }
        public List<PaymentOutputDto> CreateOrUpdatePaymentAdjust(List<PaymentInputDto> payments)
        {
            ValidateListPayment(payments);
            var output = ObjectMapper.Map<List<PaymentOutputDto>>(CreateOrUpdateListPayment(payments, true));
            if (output != null)
            {
                var totalPaymentAmount = payments.Sum(x => x.EstAmount);
            }
            return output;
        }

        public async Task<PaymentOutputDto> UpdateTrackingInvoices(TrackingInvoiceInputDto input)
        {
            Payment entity = await _repoPayment.GetAsync(input.Id);

            // gui notify cho nguoi tao Payment
            if (entity.StatusId != input.StatusId)
            {
                var creatorUser = await _userManager.GetUserByIdAsync(entity.CreatorUserId.Value);
                await _appNotifier.SendMessageAsync(new List<UserIdentifier> { creatorUser.ToUserIdentifier() }, string.Format("{0}-TheAccountantHasUpdatedTheInvoice", entity.InvoiceNo));
            }
            entity = ObjectMapper.Map(input, entity);
            await _repoPayment.UpdateAsync(entity);
            CurrentUnitOfWork.SaveChanges();
            var payment = _repoPayment.GetAll().Where(x => x.Id == entity.Id).Include(x => x.Status).SingleOrDefault();
            return ObjectMapper.Map<PaymentOutputDto>(payment);
        }

        #region validate 
        private void ValidateDealInfor(DealInputDto infor)
        {
            if (infor.OrganizationUnitId == 0)
                throw new UserFriendlyException(L("OrganizationUnitIdNotFound"));

            if (_repoOpportunityCategory.Count(x => x.Id == infor.StatusId && x.IsActive &&
            x.TypeCode.ToUpper() == DealConstant.TypeCodeDealStatus.ToUpper()) == 0)
                throw new UserFriendlyException(L("StatusIdNotFound"));
            if (infor.Id.HasValue)
            {
                if (_repoOpportunityCategory.GetAll().Any(x => x.Id == infor.StatusId && x.IsActive &&
                 x.TypeCode.ToUpper() == DealConstant.TypeCodeDealStatus.ToUpper()
                 && x.Name.ToUpper() == DealConstant.DealActive.ToUpper()))
                {
                    var listOppIdActive = _repoDealOpportunity.GetAll().Where(x => x.DealId == infor.Id).Select(x => x.OpportunityId);
                    if (_repoDealOpportunity.GetAll().Any(x => listOppIdActive.Contains(x.OpportunityId) && x.IsActive && x.DealId != infor.Id))
                    {
                        throw new UserFriendlyException(L("OpportunityExistOtherDeal"));
                    }
                }

                if (_repoOpportunityCategory.GetAll().Any(x => x.Id == infor.StatusId && x.IsActive &&
                x.TypeCode.ToUpper() == DealConstant.TypeCodeDealStatus.ToUpper()
                && x.Name.ToUpper() == DealConstant.DealCancel.ToUpper()))
                {
                    if (_repoPayment.GetAll().Any(x => x.DealId == infor.Id && x.IsActive))
                    {
                        throw new UserFriendlyException(L("DealExistOtherPayment"));
                    }
                }
            }
        }
        private void ValidateDealFullInfor(DealFullInputDto infor)
        {
            if (infor.DealShare == null || !infor.DealShare.Any(x => x.IsPrimary == true))
                throw new UserFriendlyException(L("InputPrimaryDepartment"));

            if (_repoOpportunityCategory.Count(x => x.Id == infor.StatusId && x.IsActive &&
            x.TypeCode.ToUpper() == DealConstant.TypeCodeDealStatus.ToUpper()) == 0)
                throw new UserFriendlyException(L("StatusIdNotFound"));

            if ((infor.DealShare != null || !infor.DealShare.Any()) && infor.FeeAmount != infor.DealShare.Sum(x => x.FeeAmount))
            {
                throw new UserFriendlyException(L("FeeAmount!=ShareAmount"));
            }
            if (infor.Id>0)
            {
                if (_repoOpportunityCategory.GetAll().Any(x => x.Id == infor.StatusId && x.IsActive &&
                 x.TypeCode.ToUpper() == DealConstant.TypeCodeDealStatus.ToUpper()
                 && x.Name.ToUpper() == DealConstant.DealActive.ToUpper()))
                {
                    var listOppIdActive = _repoDealOpportunity.GetAll().Where(x => x.DealId == infor.Id).Select(x => x.OpportunityId);
                    if (_repoDealOpportunity.GetAll().Any(x => listOppIdActive.Contains(x.OpportunityId) && x.IsActive && x.DealId != infor.Id))
                    {
                        throw new UserFriendlyException(L("OpportunityExistOtherDeal"));
                    }
                }

                if (_repoOpportunityCategory.GetAll().Any(x => x.Id == infor.StatusId && x.IsActive &&
                x.TypeCode.ToUpper() == DealConstant.TypeCodeDealStatus.ToUpper()
                && x.Name.ToUpper() == DealConstant.DealCancel.ToUpper()))
                {
                    if (_repoPayment.GetAll().Any(x => x.DealId == infor.Id && x.IsActive))
                    {
                        throw new UserFriendlyException(L("DealExistOtherPayment"));
                    }
                }
            }
        }
        //DealAdjust
        private void ValidateListDealAdjust(List<DealAdjustInputDto> dealAdjusts)
        {
            dealAdjusts.ForEach(x => ValidateDealAdjust(x));
        }
        private void ValidateDealAdjust(DealAdjustInputDto dealAdjust)
        {
            if (dealAdjust.Id > 0)
            {
                if (!_repoDealAdjust.GetAll().Any(x => x.Id == dealAdjust.Id))
                    throw new UserFriendlyException(L("DealAjustIdNotFound"));
            }
        }
        //DealShare
        private void ValidateListDealShare(List<DealShareInputDto> dealShares)
        {

            dealShares.ForEach(x => ValidateDealShare(x));
        }
        private void ValidateDealShare(DealShareInputDto dealShare)
        {
            if (!dealShare.UserId.HasValue && !dealShare.OrganizationUnitId.HasValue)
            {
                throw new UserFriendlyException(L("OrganizationUnitOrUserIdNotFound"));
            }

            if (dealShare.Id > 0)
            {
                if (!_repoDealShare.GetAll().Any(x => x.Id == dealShare.Id))
                    throw new UserFriendlyException(L("DealShareIdNotFound"));
                //if (_repoDealShare.GetAll().Any(x =>
                //    x.Id != dealShare.Id && x.DealId == dealShare.DealId && x.IsActive == true
                //    && (x.OrganizationUnitId == dealShare.OrganizationUnitId || x.UserId == dealShare.UserId)))
                //    throw new UserFriendlyException(L("ExistDealOpportunity"));
            }
            //else
            //{
            //    if (_repoDealShare.GetAll().Any(x => x.DealId == dealShare.DealId && x.IsActive == true
            //    && (x.OrganizationUnitId == dealShare.OrganizationUnitId || x.UserId == dealShare.UserId)))
            //        throw new UserFriendlyException(L("ExistDealShare"));
            //}
        }
        //Payment
        private void ValidateListPayment(List<PaymentInputDto> payments)
        {
            payments.ForEach(x => ValidatePayment(x));
        }
        private void ValidatePayment(PaymentInputDto payment)
        {
            if (_repoOpportunityCategory.Count(x => x.Id == payment.StatusId && x.IsActive &&
          x.TypeCode.ToUpper() == DealConstant.PaymentStatus.ToUpper()) == 0)
                throw new UserFriendlyException(L("StatusIdNotFound"));
            //update
            if (payment.Id.HasValue)
            {
                if (!_repoPayment.GetAll().Any(x => x.Id == payment.Id.Value))
                    throw new UserFriendlyException(L("PaymentIdNotFound"));
                //check total amount payment<=deal.amount
                //var totalAmount = _repoPayment.GetAll().Where(x => x.DealId == payment.DealId && x.IsActive == true && x.Id != payment.Id).Sum(y => y.EstAmount) + payment.EstAmount;
                //if (_repoDeal.Get(payment.DealId).FeeAmount < totalAmount || _repoDeal.Get(payment.DealId).FeeAmount == 0)
                //    throw new UserFriendlyException(L("DealAmount>=TotalInvoiceAmount"));
            }
            //Insert
            //else
            //{
            //check total amount payment<=deal.amount
            //var totalAmount = _repoPayment.GetAll().Where(x => x.DealId == payment.DealId && x.IsActive == true).Sum(y => y.EstAmount) + payment.EstAmount;
            //if (_repoDeal.Get(payment.DealId).FeeAmount < totalAmount || _repoDeal.Get(payment.DealId).FeeAmount == 0)
            //    throw new UserFriendlyException(L("DealAmount>=TotalInvoiceAmount"));
            //}
        }
        //Deal Opportunity
        private void ValidateListDealOpportunity(List<DealOpportunityInputDto> dealOpportunitys)
        {
            dealOpportunitys.ForEach(x => ValidateDealOpportunity(x));
        }
        private void ValidateDealOpportunity(DealOpportunityInputDto dealOpportunity)
        {

            //update
            if (dealOpportunity.Id > 0)
            {
                if (!_repoDealOpportunity.GetAll().Any(x => x.Id == dealOpportunity.Id))
                    throw new UserFriendlyException(L("DealOpportunityIdNotFound"));
            }
            //Insert
            else
            {
                if (_repoDealOpportunity.GetAll().Any(x =>
                x.IsActive == true && x.OpportunityId == dealOpportunity.OpportunityId))
                    throw new UserFriendlyException(L("ExistDealOpportunity"));
                if (_repoPayment.GetAll().Any(x =>
                x.IsActive == true && x.DealId == dealOpportunity.DealId))
                    throw new UserFriendlyException(L("ExistPayment"));
            }
        }

        #endregion

        #region Private API
        private async Task<PagedResultDto<DealOutputDto>> FilterDealGroupDepartment(string input,
            string groupDepartmentIds)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<DealFilterDto>("SpFilterDeal",
                    new
                    {
                        @FilterJson = input
                        ,
                        @GroupDepartmentIds = groupDepartmentIds
                        ,
                        @UserId = AbpSession.UserId.Value

                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<DealOutputDto>>(output);
                return new PagedResultDto<DealOutputDto>(totalCount, results);
            }
        }

        private async Task<string> GetDepartmentIds(List<string> groups)
        {
            var departmentIds = await _organizationUnitRepository.GetAll().Where(x => groups.Any(y => x.Code == y)).Select(y => y.Id).ToListAsync();
            return string.Join(",", departmentIds);
        }
        private async Task<DealOutputDto> CreateOrUpdateDealFullAsync(DealFullInputDto input)
        {
            {
                ValidateDealFullInfor(input);
                var primaryDepartment = input.DealShare.Where(x => x.IsPrimary == true).SingleOrDefault();
                input.OrganizationUnitId = primaryDepartment.OrganizationUnitId.Value;
                var entity = new Entities.Deal.Deal();
                if (input.Id > 0)
                {
                    if (input.Id < AppConsts.MinValuePrimaryKey)
                        throw new UserFriendlyException(L("DealIdNotFound"));
                    entity = await _repoDeal.GetAsync(input.Id);
                    var entityUpdate = ObjectMapper.Map(input, entity);
                    var dealStatusName = _repoOpportunityCategory.GetAll()
                      .Where(x => x.TypeCode.ToUpper() == Deal.DealConstant.TypeCodeDealStatus.ToUpper()
                      && x.Id == input.StatusId).Select(y => y.Name).FirstOrDefault();

                    int? opportunityStageId = _repoOpportunityCategory.GetAll()
                      .Where(x => x.IsActive == true && x.TypeCode.ToUpper() == Opportunity.OpportunityConstant.OpportunityStage.ToUpper() && x.Name.ToUpper() == dealStatusName.ToUpper()).Select(y => y.Id).FirstOrDefault();
                    if (opportunityStageId.HasValue)
                    {
                        var opportunities = _repoDealOpportunity.GetAll().Where(x => x.DealId == input.Id && x.IsActive == true);
                        opportunities.ToList().ForEach(x =>
                        {
                            var opportunity = _repoOpportunity.Get(x.OpportunityId);
                            opportunity.StageId = opportunityStageId.Value;
                            opportunity.Probability = 100;
                            _repoOpportunity.Update(opportunity);
                        });

                    }
                    await _repoDeal.UpdateAsync(entityUpdate);
                }
                else
                {
                    var entityInsert = ObjectMapper.Map<Entities.Deal.Deal>(input);
                    entityInsert.IsActive = true;
                    entityInsert.StatusId = _repoOpportunityCategory.GetAll().Where(x => x.Name.ToUpper() == DealConstant.DealBilled.ToUpper()
                    && x.TypeCode.ToUpper() == Deal.DealConstant.TypeCodeDealStatus.ToUpper()).FirstOrDefault().Id;
                    entity.Id = await _repoDeal.InsertAndGetIdAsync(entityInsert);
                }
                var output = ObjectMapper.Map<DealOutputDto>(entity);
                if (input.DealShare != null)
                {
                    await UpdateChilds(
                       _repoDealShare,
                       x => x.DealId = entity.Id,
                       x => x.DealId == entity.Id,
                       (x, y) => x.Id == y.Id,
                       input.DealShare.ToArray()
                       );
                    //if (input.DealShare.FirstOrDefault().OrganizationUnitId.HasValue)
                    //{
                    //    CreateOrUpdateDealShareV1(output.Id, input.DealShare);
                    //}
                    //else
                    //    output.DealUser = ObjectMapper.Map<List<DealShareOutputDto>>(CreateOrUpdateDealShareV1(output.Id, input.DealShare));
                }
                if (input.DealAdjust != null)
                {
                    ValidateListDealAdjust(input.DealAdjust);
                    await UpdateChilds(
                         _repoDealAdjust,
                         x => x.DealId = entity.Id,
                         x => x.DealId == entity.Id,
                         (x, y) => x.Id == y.Id,
                         input.DealAdjust.ToArray()
                         );

                    //output.DealAdjust = ObjectMapper.Map<List<DealAdjustOutputDto>>(CreateOrUpdateDealAdjustV1(output.Id, input.DealAdjust));
                }
                if (input.DealOpportunity != null)
                {
                    ValidateListDealOpportunity(input.DealOpportunity);
                    await UpdateChilds(
                        _repoDealOpportunity,
                        x => x.DealId = entity.Id,
                        x => x.DealId == entity.Id,
                        (x, y) => x.Id == y.Id,
                        input.DealOpportunity.ToArray()
                        );
                    //output.DealOpportunity = ObjectMapper.Map<List<DealOpportunityOutputDto>>(CreateOrUpdateDealOpportunityV1(output.Id, input.DealOpportunity));
                }
                return await GetDealDetail(entity.Id);
            }
        }
        //Create or Update
        private async Task<DealOutputDto> CreateOrUpdateDealAsync(long? dealId,
           DealInputDto input)
        {
            var output = new DealOutputDto();
            if (dealId.HasValue)
            {
                if (dealId < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("DealIdNotFound"));
                var entity = _repoDeal.Get(dealId.Value);

                ValidateDealInfor(input);
                var entityUpdate = ObjectMapper.Map(input, entity);
                var dealStatusName = _repoOpportunityCategory.GetAll()
                  .Where(x => x.TypeCode.ToUpper() == Deal.DealConstant.TypeCodeDealStatus.ToUpper()
                  && x.Id == input.StatusId).Select(y => y.Name).FirstOrDefault();

                int? opportunityStageId = _repoOpportunityCategory.GetAll()
                  .Where(x => x.IsActive == true && x.TypeCode.ToUpper() == Opportunity.OpportunityConstant.OpportunityStage.ToUpper() && x.Name.ToUpper() == dealStatusName.ToUpper()).Select(y => y.Id).FirstOrDefault();
                if (opportunityStageId.HasValue)
                {
                    var opportunities = _repoDealOpportunity.GetAll().Where(x => x.DealId == dealId && x.IsActive == true);
                    opportunities.ToList().ForEach(x =>
                    {
                        var opportunity = _repoOpportunity.Get(x.OpportunityId);
                        opportunity.StageId = opportunityStageId.Value;
                        opportunity.Probability = 100;
                        _repoOpportunity.Update(opportunity);
                    });

                }
                await _repoDeal.UpdateAsync(entityUpdate);
                output = ObjectMapper.Map<DealOutputDto>(entity);
            }
            else
            {
                ValidateDealInfor(input);
                var entity = ObjectMapper.Map<Entities.Deal.Deal>(input);
                entity.IsActive = true;
                entity.StatusId = _repoOpportunityCategory.GetAll().Where(x => x.Name.ToUpper() == DealConstant.DealBilled.ToUpper()
                && x.TypeCode.ToUpper() == Deal.DealConstant.TypeCodeDealStatus.ToUpper()).FirstOrDefault().Id;
                entity.Id = await _repoDeal.InsertAndGetIdAsync(entity);
                output = ObjectMapper.Map<DealOutputDto>(entity);
            }

            return output;
        }

        //Deal Share
        private List<DealShareOutputDto> CreateOrUpdateListDealShare(List<DealShareInputDto> dealShares)
        {
            var entities = new List<DealShareOutputDto>();
            dealShares.ForEach(x =>
            {
                entities.Add(CreateOrUpdateDealShare(x));
            });
            return entities;
        }

        //Deal Opportunity
        private List<DealOpportunityOutputDto> CreateOrUpdateListDealOpportunity(List<DealOpportunityInputDto> dealOpportunitys)
        {
            var entities = new List<DealOpportunityOutputDto>();
            dealOpportunitys.ForEach(x =>
            {
                entities.Add(CreateOrUpdateDealOpportunity(x));
            });
            return entities;
        }
        //Payment
        private List<PaymentOutputDto> CreateOrUpdateListPayment(List<PaymentInputDto> payments, bool isAdjust)
        {
            var entities = new List<PaymentOutputDto>();
            payments.ForEach(x =>
            {
                entities.Add(CreateOrUpdatePayment(x, isAdjust));
            });
            return entities;
        }
        #endregion

        #region Action

        //Deal SHare
        [UnitOfWork]
        private DealShareOutputDto CreateOrUpdateDealShare(DealShareInputDto input)
        {
            DealShare entity = new DealShare();
            if (input.Id > 0)
            {
                var entityUser = _repoDealShare.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityUser);
            }
            else
            {
                entity = ObjectMapper.Map<DealShare>(input);
            }
            entity.Id = _repoDealShare.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            var dealShare = _repoDealShare.GetAll().Where(x => x.Id == entity.Id).Include(x => x.User).SingleOrDefault();
            return ObjectMapper.Map<DealShareOutputDto>(dealShare);
        }

        [UnitOfWork]
        private DealShare CreateOrUpdateDealShare(long dealId, DealShareInputDto input)
        {
            DealShare entity = new DealShare();
            if (input.Id > 0)
            {
                var entityDealShare = _repoDealShare.Get(input.Id);
                entity = ObjectMapper.Map(input, entityDealShare);
            }
            else
            {
                entity = ObjectMapper.Map<DealShare>(input);
                entity.IsActive = true;
            }
            entity.DealId = dealId;
            entity.Id = _repoDealShare.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            var dealShare = _repoDealShare.GetAll().Where(x => x.Id == entity.Id).Include(x => x.OrganizationUnit).SingleOrDefault();
            return entity;
        }
        //deal Adjust
        [UnitOfWork]
        private DealAdjust CreateOrUpdateDealAdjust(long dealId, DealAdjustInputDto input)
        {
            DealAdjust entity = new DealAdjust();
            if (input.Id > 0)
            {
                var entityDealAdjust = _repoDealAdjust.Get(input.Id);
                entity = ObjectMapper.Map(input, entityDealAdjust);
            }
            else
            {
                entity = ObjectMapper.Map<DealAdjust>(input);
                entity.IsActive = true;
            }
            entity.DealId = dealId;
            entity.Id = _repoDealAdjust.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            // var dealAdjust = _repoDealAdjust.GetAll().Where(x => x.Id == entity.Id).Include(x => x.OrganizationUnit).SingleOrDefault();
            return entity;
        }
        //DealShare
        [UnitOfWork]
        private DealOpportunityOutputDto CreateOrUpdateDealOpportunity(DealOpportunityInputDto input)
        {
            DealOpportunity entity = new DealOpportunity();
            if (input.Id != null && input.Id > 0)
            {
                var entityDealOpp = _repoDealOpportunity.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityDealOpp);
            }
            else
            {
                entity = ObjectMapper.Map<DealOpportunity>(input);
                entity.IsActive = true;
            }
            entity.Id = _repoDealOpportunity.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();
            var dealOpportunity = _repoDealOpportunity.GetAll().Where(x => x.Id == entity.Id).Include(x => x.Opportunity).SingleOrDefault();
            return ObjectMapper.Map<DealOpportunityOutputDto>(dealOpportunity);
        }
        [UnitOfWork]
        private DealOpportunityOutputDto CreateOrUpdateDealOpportunityV1(long dealId, DealOpportunityInputDto input)
        {
            DealOpportunity entity = new DealOpportunity();
            if (input.Id > 0)
            {
                var entityDealOpp = _repoDealOpportunity.Get(input.Id);
                entity = ObjectMapper.Map(input, entityDealOpp);
            }
            else
            {
                entity = ObjectMapper.Map<DealOpportunity>(input);
                entity.IsActive = true;
            }
            entity.Id = _repoDealOpportunity.InsertOrUpdateAndGetId(entity);
            entity.DealId = dealId;
            CurrentUnitOfWork.SaveChanges();
            var dealOpportunity = _repoDealOpportunity.GetAll().Where(x => x.Id == entity.Id).Include(x => x.Opportunity).SingleOrDefault();
            return ObjectMapper.Map<DealOpportunityOutputDto>(dealOpportunity);
        }

        //Paymet
        [UnitOfWork]
        private PaymentOutputDto CreateOrUpdatePayment(PaymentInputDto input, bool? isAdjust)
        {
            Payment entity = new Payment();
            var paymentStatus = _repoOpportunityCategory.FirstOrDefault(x => x.Id == input.StatusId);
            if (input.Id != null)
            {
                var entityPay = _repoPayment.GetAll().Where(x => x.Id == input.Id)
                    .Include(x => x.Status)
                    .Include(x => x.Deal)
                    .SingleOrDefault();
                //if (entityPay.IsSign.HasValue && entityPay.IsSign.Value)
                //    throw new UserFriendlyException(L("PaymentIsBlock"));

                if (string.IsNullOrEmpty(entityPay.InvoiceNo) && entityPay.EstAmount != 0 && new string[] { DealConstant.PaymentWriteOff.ToUpper(), DealConstant.PaymentInv.ToUpper(), DealConstant.PaymentPaid.ToUpper() }.Contains(paymentStatus.Name.ToUpper()))
                {
                    var maxNumInvoice = _repoPayment.GetAll().Max(x => (long?)x.NumInvoice) ?? 0;
                    var organizationUnitName = _repoOrganizationUnit.GetAll().Where(x => x.Id == (long?)entityPay.Deal.OrganizationUnitId).Select(x => x.DisplayName).SingleOrDefault();
                    if (!string.IsNullOrEmpty(organizationUnitName))
                    {
                        var invoiceNo = string.Format("{0}/{1}/{2}/{3}/{4}", new object[] {organizationUnitName,DateTime.Now.Year.ToString()
                        ,DateTime.Now.Month.ToString(),DateTime.Now.Day.ToString(),string.Format($"{ maxNumInvoice+1:000000}")});
                        entityPay.InvoiceNo = invoiceNo;
                        entityPay.NumInvoice = maxNumInvoice + 1;
                    }
                }
                entity = ObjectMapper.Map(input, entityPay);
            }
            else
            {
                entity = ObjectMapper.Map<Payment>(input);
                entity.IsActive = true;
                var deal = _repoDeal.GetAll().Where(x => x.Id == entity.DealId)
                    .Include(x => x.OrganizationUnit).SingleOrDefault();
                if (string.IsNullOrEmpty(entity.InvoiceNo) && entity.EstAmount != 0 && new string[] { DealConstant.PaymentWriteOff.ToUpper(), DealConstant.PaymentInv.ToUpper(), DealConstant.PaymentPaid.ToUpper() }.Contains(paymentStatus.Name.ToUpper()))
                {
                    var maxNumInvoice = _repoPayment.GetAll().Max(x => (long?)x.NumInvoice) ?? 0;
                    var organizationUnitName = _repoOrganizationUnit.GetAll().Where(x => x.Id == (long?)deal.OrganizationUnitId).Select(x => x.DisplayName).SingleOrDefault();
                    if (!string.IsNullOrEmpty(organizationUnitName))
                    {
                        var invoiceNo = string.Format("{0}/{1}/{2}/{3}/{4}", new object[] {organizationUnitName,DateTime.Now.Year.ToString()
                        ,DateTime.Now.Month.ToString(),DateTime.Now.Day.ToString(),string.Format($"{ maxNumInvoice+1:000000}")});
                        entity.InvoiceNo = invoiceNo;
                        entity.NumInvoice = maxNumInvoice + 1;
                    }
                }
            }
            entity.IsAdjust = isAdjust.HasValue ? isAdjust.Value : false;
            entity.Id = _repoPayment.InsertOrUpdateAndGetId(entity);
            CurrentUnitOfWork.SaveChanges();

            var payment = _repoPayment.GetAll().Where(x => x.Id == entity.Id).Include(x => x.Status).SingleOrDefault();
            return ObjectMapper.Map<PaymentOutputDto>(payment);
        }
        private async Task<DealOutputDto> GetDetails(long opportunityId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<DealFilterDto>("SpFilterDeal",
                    new
                    {
                        @Id = opportunityId,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<DealOutputDto>>(output).SingleOrDefault();
                return results;
            }
        }
        #endregion  
    }
}
