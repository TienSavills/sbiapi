﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Deal
{
    public static class DealConstant
    {
        public const string DealCancel = "Cancel";
        public const string DealActive = "Active";
        public const string DealBilled = "Billed";
        public const string DealWip = "Wip";
        public const string DealOnhold = "Onhold";
        public const string OppStageSigned = "Signed";
        public const string OppStageInterrupted = "Interrupted";
     

        public const string PaymentStatus = "PaymentStatus";
        public const string PaymentForecast = "Forecast";
        public const string PaymentInv = "Inv";
        public const string PaymentPaid = "Paid";
        public const string PaymentWriteOff = "Write Off";
        public const string TargetCodeOpportunityStage = "OpportunityStage";
        public const string TargetCodeStageAdvisory = "StageAdvisory";
        public const string TargetCodeStageCommercial = "StageCommercial";
        public const string TypeCodeDealStatus = "DealStatus";
        public const string TypeCodeDealStatusAdvisory = "DealStatusAdvisory";
        public const string TypeCodeDealStatusCommercial = "DealStatusCommercial";
    }
}
