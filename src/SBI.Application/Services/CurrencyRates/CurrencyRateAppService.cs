﻿using Abp.Application.Services;
using CRM.Application.Shared.CurrencyRates;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using CRM.Entities.Currencys;
using CRM.SBIMap;
using CRM.Entities.CurrencyRates;
using Microsoft.Extensions.Options;
using Abp.UI;
using System.Linq;
using Abp.Linq.Extensions;
using CRM.Services.Currencys;
using Microsoft.EntityFrameworkCore;
using Abp.Collections.Extensions;

namespace CRM.Services.CurrencyRates
{
    public class CurrencyRateAppService : SBIAppServiceBase, ICurrencyRateAppService
    {
        private readonly IRepository<Currency, int> _repoCurrency;
        private readonly IRepository<CurrencyRate, long> _repoCurrencyRate;
        private ConfigSetting _options;
        public CurrencyRateAppService (
             IRepository<Currency, int> repoCurrency
            , IRepository<CurrencyRate, long> repoCurrencyRate
            , IOptions<ConfigSetting> options
            )
        {
            _repoCurrency = repoCurrency;
            _repoCurrencyRate = repoCurrencyRate;
            _options = options.Value;
        }
        public async Task<CurrencyRateOutputDto> CreateCurrencyRateAsync(CurrencyRateInputDto input)
        {
            ValidateCreateCurrencyRate(input);
            var listRate = _repoCurrencyRate.GetAll().Where(x => x.CurrencyId == input.CurrencyId).ToList();
            if (listRate != null)
            {
                foreach (var objUpdate in listRate)
                {
                    objUpdate.ToDate = DateTime.UtcNow;
                    objUpdate.IsActive = false;
                    _repoCurrencyRate.Update(objUpdate);
                }
            }
            var entity = ObjectMapper.Map<CurrencyRate>(input);
            entity.IsActive = true;
            entity.FromDate = DateTime.UtcNow;
            entity.ToDate =Convert.ToDateTime( CurrencyConstant.DefaultDate);
            entity.Id = await _repoCurrencyRate.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<CurrencyRateOutputDto>(entity);
        }

        public async Task<CurrencyRateOutputDto> GetCurrencyRateDetail(long id)
        {
            var query = _repoCurrencyRate.GetAll().Include(x => x.Currency)
              .Where(x=>x.Id==id&&x.IsActive==true);
            var results = await query.SingleOrDefaultAsync();
            return ObjectMapper.Map<CurrencyRateOutputDto>(results);
        }

        public async Task<List<CurrencyRateOutputDto>> GetListCurrencyRates(CurrencyRateFilterInputDto filters)
        {
            var query = _repoCurrencyRate.GetAll().Include(x => x.Currency)
                .Where(x=>x.IsActive==true)
                .WhereIf(string.IsNullOrEmpty(filters.Keyword)==false, x => x.Currency.CurrencyCode.ToUpper().Contains(filters.Keyword.ToUpper()));
            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<CurrencyRateOutputDto>>(results);
        }

        public async Task<CurrencyRateOutputDto> UpdateCurrencyRateAsync(long id, CurrencyRateInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("RateIdNotFound"));
            var entity = _repoCurrencyRate.Get(id);

            ValidateUpdateCurrencyRate(input);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoCurrencyRate.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<CurrencyRateOutputDto>(entity);

        }
        public void ValidateCreateCurrencyRate(CurrencyRateInputDto input)
        {
            if (_repoCurrency.GetAll().Count(x => x.Id == input.CurrencyId && x.IsActive) < 1)
                throw new UserFriendlyException(L("CurrencyIdNotFound"));
            if (input.RateBaseVND==0)
                throw new UserFriendlyException(L("RateBaseVND"));
        }

        public void ValidateUpdateCurrencyRate( CurrencyRateInputDto input)
        {
        }
    }
}
