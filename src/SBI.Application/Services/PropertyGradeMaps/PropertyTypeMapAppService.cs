﻿using System.Collections.Generic;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using CRM.Application.Shared.PropertyGradeMaps;
using CRM.Entities.PropertyGradeMaps;

namespace CRM.Services.PropertyGradeMaps
{
    public class PropertyGradeMapAppService : SBIAppServiceBase, IPropertyGradeMapAppService
    {
        private readonly IRepository<PropertyGradeMap, long> _repoPropertyGradeMap;

        public PropertyGradeMapAppService(IRepository<PropertyGradeMap, long> repoPropertyGradeMap)
        {
            _repoPropertyGradeMap = repoPropertyGradeMap;
        }
        public Task<long> CreatePropertyGradeMap(PropertyGradeMapInputDto input)
        {
            var data = ObjectMapper.Map<PropertyGradeMap>(input);
            var id = _repoPropertyGradeMap.InsertOrUpdateAndGetIdAsync(data);
            return id;
        }

        public List<PropertyGradeMapOutputDto> GetListPropertyGradeMap(long propertyId)
        {
            var data = _repoPropertyGradeMap.GetAll().Where(x => x.PropertyId == propertyId)
                .Include(x=>x.Grade);
            return ObjectMapper.Map<List<PropertyGradeMapOutputDto>>(data);
        }
        public Task<long> UpdatePropertyGradeMap(long id, PropertyGradeMapInputDto input)
        {
            var entity = ObjectMapper.Map<PropertyGradeMap>(input);
            return _repoPropertyGradeMap.InsertOrUpdateAndGetIdAsync(entity);
        }
    }
}
