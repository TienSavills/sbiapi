﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using CRM.Application.Shared.Unit;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using CRM.Application.Shared.OpportunityAssetClass;
using CRM.Application.Shared.UnitFacilityMaps;
using CRM.Application.Shared.UnitViewMaps;
using CRM.Entities.UnitAddress;
using CRM.Entities.UnitFacilityMaps;
using CRM.Entities.UnitViewMaps;

namespace CRM.Services.Unit
{
    public class UnitAppService : SBIAppServiceBase, IUnitAppService
    {
        private readonly IRepository<Entities.Unit.Unit, long> _repoUnit;
        private readonly IRepository<UnitFacilityMaps, long> _repoUnitFacilityRepository;
        private readonly IRepository<UnitViewMaps, long> _repoUnitViewRepository;
        private readonly IRepository<UnitAddress, long> _repoUnitAddressRepository;
        private readonly IRepository<Entities.Floor.Floor, long> _repoFloor;
        private readonly IRepository<Entities.UnitCategory.UnitCategory, int> _repoUnitCategory;
        private readonly IRepository<Entities.Projects.Project, long> _repoProject;
        private readonly ConfigSetting _options;
        public UnitAppService(
            IRepository<Entities.Unit.Unit, long> repoUnit
            , IRepository<UnitFacilityMaps, long> repoUnitFacilityRepository
            , IRepository<UnitViewMaps, long> repoUnitViewRepository
            , IRepository<Entities.Floor.Floor, long> repoFloor
            , IRepository<Entities.Projects.Project, long> repoProject
            , IRepository<Entities.UnitCategory.UnitCategory, int> repoUnitCategory
            , IRepository<UnitAddress, long> repoUnitAddressRepository
            , IOptions<ConfigSetting> options
            )
        {
            _repoUnit = repoUnit;
            _repoFloor = repoFloor;
            _repoProject = repoProject;
            _repoUnitFacilityRepository = repoUnitFacilityRepository;
            _repoUnitViewRepository = repoUnitViewRepository;
            _repoUnitAddressRepository = repoUnitAddressRepository;
            _repoUnitCategory = repoUnitCategory;
            _options = options.Value;
        }
        public async Task<UnitOutputDto> CreateOrUpdateAsync(long? id, UnitInputDto input)
        {
            if (!_repoUnitCategory.GetAll().Where(x => x.Id == input.StatusId).Any())
                throw new UserFriendlyException(L("StatusIdNotFound"));
            if (!_repoUnitCategory.GetAll().Where(x => x.Id == input.UnitTypeId).Any())
                throw new UserFriendlyException(L("UnitTypeIdNotFound"));
            if (!_repoFloor.GetAll().Where(x => x.Id == input.FloorId).Any())
                throw new UserFriendlyException(L("FloorIdNotFound"));

            if (id.HasValue)
            {
                if (id < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("UnitIdNotFound"));
                var entity = await _repoUnit.GetAsync(id.Value);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoUnit.UpdateAsync(entityUpdate);
                return ObjectMapper.Map<UnitOutputDto>(entity);
            }
            else
            {
                var entity = ObjectMapper.Map<Entities.Unit.Unit>(input);
                entity.Id = await _repoUnit.InsertAndGetIdAsync(entity);
                entity.IsActive = true;
                return ObjectMapper.Map<UnitOutputDto>(entity);
            }
        }

        //Create Update for Residential
        public async Task<UnitResOutputDto> CreateOrUpdateResidentialAsync(UnitResInputDto input)
        {
            if (!_repoUnitCategory.GetAll().Where(x => x.Id == input.StatusId).Any())
                throw new UserFriendlyException(L("StatusIdNotFound"));
            if (!_repoUnitCategory.GetAll().Where(x => x.Id == input.UnitTypeId).Any())
                throw new UserFriendlyException(L("UnitTypeIdNotFound"));

            Entities.Unit.Unit entity;
            if (input.Id>0)
            {
                if (input.Id < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("UnitIdNotFound"));
                entity = await _repoUnit.GetAsync(input.Id);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoUnit.UpdateAsync(entityUpdate);
            }
            else
            {
                entity = ObjectMapper.Map<Entities.Unit.Unit>(input);
                entity.IsActive = true;
                entity.Id = await _repoUnit.InsertAndGetIdAsync(entity);
            }
            await CurrentUnitOfWork.SaveChangesAsync();
            if (input.FacilityIds != null)
            {
                await UpdateChilds(
                    _repoUnitFacilityRepository,
                    x => x.UnitId = entity.Id,
                    x => x.UnitId == entity.Id,
                    (x, y) => x.FacilityId == y.FacilityId,
                    input.FacilityIds.Select(x => new UnitFacilityMapInputDto()
                    { FacilityId = x, UnitId = entity.Id, IsActive = true }).ToArray()
                );
            }
            if (input.ViewIds != null)
            {
                await UpdateChilds(
                    _repoUnitViewRepository,
                    x => x.UnitId = entity.Id,
                    x => x.UnitId == entity.Id,
                    (x, y) => x.ViewId == y.ViewId,
                    input.ViewIds.Select(x => new UnitViewMapInputDto()
                    { ViewId = x, UnitId = entity.Id, IsActive = true }).ToArray()
                );
            }
            if (input.UnitAddress != null)
            {
                await UpdateChilds(
                    _repoUnitAddressRepository,
                    x => x.UnitId = entity.Id,
                    x => x.UnitId == entity.Id,
                    (x, y) => x.Id == y.Id,
                    input.UnitAddress.ToArray()
                );
            }
            return ObjectMapper.Map<UnitResOutputDto>(entity);
        }

        public async Task<PagedResultDto<UnitOutputDto>> GetListUnits(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitFilterDto>("SpFilterUnit",
                    new
                    {

                        @FilterJson = input
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<UnitOutputDto>>(output);
                return new PagedResultDto<UnitOutputDto>(totalCount, results);
            }
        }
        //List Unit for Residential
        public async Task<PagedResultDto<UnitResOutputDto>> GetListUnitResAsync(long? projectId, long? floorId, string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitResFilterDto>("SpFilterUnitResident",
                    new
                    {
                        @ProjectId = projectId,
                        @FloorId = floorId,
                        @FilterJson = input
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<UnitResOutputDto>>(output);
                return new PagedResultDto<UnitResOutputDto>(totalCount, results);
            }
        }

        public async Task<UnitResOutputDto> GetUnitResDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitResFilterDto>("SpFilterUnitResident",
                    new
                    {
                        @Id = id
                    }, commandType: CommandType.StoredProcedure);
                return ObjectMapper.Map<UnitResOutputDto>(output.SingleOrDefault()); ;
            }
        }
        public async Task<List<UnitOutputDto>> GetListUnitsByFloor(long floorId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitFilterDto>("SpGetListUnitByProjectFloor",
                    new
                    {
                        @FloorId = floorId
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<UnitOutputDto>>(output);
                return results;
            }
        }

        public async Task<List<UnitOutputDto>> GetListUnitsByProject(long projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitFilterDto>("SpGetListUnitByProjectFloor",
                    new
                    {
                        @ProjectId = projectId
                    }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<UnitOutputDto>>(output);
                return results;
            }
        }

        public async Task<UnitOutputDto> GetUnitDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitFilterDto>("SpFilterUnit",
                    new
                    {
                        @Id = id
                    }, commandType: CommandType.StoredProcedure);
                var result = ObjectMapper.Map<UnitOutputDto>(output.SingleOrDefault());
                return result;
            }
        }

        public async Task<List<UnitOutputDto>> MatchingRequest(long requestId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<UnitOutputDto>("SpFilterMatchingUnits",
                   new
                   {
                       @RequestId = requestId
                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<UnitOutputDto>>(output);
                return results;
            }
        }
        public async Task<UnitOutputDto> OrderUnit(long id, int num)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("UnitIdNotFound"));
            var entity = await _repoUnit.GetAsync(id);
            entity.Order = num;
            await _repoUnit.UpdateAsync(entity);
            return ObjectMapper.Map<UnitOutputDto>(entity);
        }
        public async Task<List<UnitOutputDto>> OrderListUnit(List<long> ids)
        {
            int i = 0;
            List<Entities.Unit.Unit> listEntities = new List<Entities.Unit.Unit>();
            ids.ForEach(x =>
            {
                var entity = _repoUnit.Get(x);
                entity.Order = i;
                i++;
                _repoUnit.UpdateAsync(entity);
                listEntities.Add(entity);
            });
            return ObjectMapper.Map<List<UnitOutputDto>>(listEntities);
        }
    }
}
