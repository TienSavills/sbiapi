﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using CRM.Application.Shared.Listings;
using CRM.Application.Shared.ListingUsers;
using CRM.Authorization;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using Dapper;
using Microsoft.Extensions.Options;


namespace CRM.Services.Listings
{

    public class ListingAppService : SBIAppServiceBase, IListingAppService
    {
        private readonly IRepository<Entities.Listings.Listings, long> _repoListing;
        private readonly IRepository<Entities.Floor.Floor, long> _repoFloor;
        private readonly IRepository<Entities.ListingCategory.ListingCategory, int> _repoListingCategory;
        private readonly IRepository<Entities.ListingUsers.ListingUsers, long> _repoListingListingUser;
        private readonly IRepository<Entities.Projects.Project, long> _repoProject;
        private readonly ConfigSetting _options;
        public ListingAppService(
            IRepository<Entities.Listings.Listings, long> repoListing
            , IRepository<Entities.Floor.Floor, long> repoFloor
            , IRepository<Entities.Projects.Project, long> repoProject
            , IRepository<Entities.ListingCategory.ListingCategory, int> repoListingCategory
            , IRepository<Entities.ListingUsers.ListingUsers, long> repoListingListingUser
            , IOptions<ConfigSetting> options
            )
        {
            _repoListing = repoListing;
            _repoFloor = repoFloor;
            _repoProject = repoProject;
            _repoListingCategory = repoListingCategory;
            _repoListingListingUser = repoListingListingUser;
            _options = options.Value;
        }

        [AbpAuthorize(PermissionNames.Pages_Administration_Listing_Create)]
        public async Task<ListingOutputDto> CreateOrUpdateAsync(ListingInputDto input)
        {
            if (!_repoListingCategory.GetAll().Where(x => x.Id == input.StatusId).Any())
                throw new UserFriendlyException(L("StatusIdNotFound"));
            if (!_repoListingCategory.GetAll().Where(x => x.Id == input.StatusId).Any())
                throw new UserFriendlyException(L("StatusIdNotFound"));
            if (!_repoListingCategory.GetAll().Where(x => x.Id == input.TypeId).Any())
                throw new UserFriendlyException(L("ListingTypeIdNotFound"));
            if (!_repoFloor.GetAll().Where(x => x.Id == input.FloorId).Any())
                throw new UserFriendlyException(L("FloorIdNotFound"));
            if (!_repoProject.GetAll().Where(x => x.Id == input.ProjectId).Any())
                throw new UserFriendlyException(L("ProjectIdNotFound"));

            Entities.Listings.Listings entity;
            if (input.Id > 0)
            {
                if (input.Id < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("ListingIdNotFound"));
                entity = await _repoListing.GetAsync(input.Id);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoListing.UpdateAsync(entityUpdate);
            }
            else
            {
                entity = ObjectMapper.Map<Entities.Listings.Listings>(input);
                entity.IsActive = true;
                entity.Id = await _repoListing.InsertAndGetIdAsync(entity);
            }
            await CurrentUnitOfWork.SaveChangesAsync();
            if (input.UserIds != null)
            {
                await UpdateChilds(
                    _repoListingListingUser,
                    x => x.ListingId = entity.Id,
                    x => x.ListingId == entity.Id,
                    (x, y) => x.UserId == y.UserId,
                    input.UserIds.Select(x => new ListingUserInputDto()
                    { UserId = x, ListingId = entity.Id, IsActive = true }).ToArray()
                );
            }
            return ObjectMapper.Map<ListingOutputDto>(entity);

        }

        [AbpAuthorize(PermissionNames.Pages_Administration_Listing_Read)]
        public async Task<PagedResultDto<ListingOutputDto>> GetListListings(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ListingFilterDto>("SpFilterListing",
                    new
                    {
                        @FilterJson = input,
                        @UserId = AbpSession.UserId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ListingOutputDto>>(output);
                return new PagedResultDto<ListingOutputDto>(totalCount, results);
            }
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<ListingOutputDto>> ListingForLandingPages(long? projectId, string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ListingFilterDto>("SpFilterListing",
                    new
                    {
                        @FilterJson = input,
                        @ProjectId = projectId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ListingOutputDto>>(output);
                return new PagedResultDto<ListingOutputDto>(totalCount, results);
            }
        }

        [AbpAuthorize(PermissionNames.Pages_Administration_Company_Read)]
        public async Task<PagedResultDto<ListingOutputDto>> GetListListingForClient(long companyId, string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ListingFilterDto>("SpFilterListing",
                    new
                    {
                        @FilterJson = input
                        ,
                        @ClientId = companyId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ListingOutputDto>>(output);
                return new PagedResultDto<ListingOutputDto>(totalCount, results);
            }
        }
        [AbpAllowAnonymous]
        public async Task<ListingOutputDto> GetListingDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ListingFilterDto>("SpFilterListing",
                    new
                    {
                        @Id = id
                    }, commandType: CommandType.StoredProcedure);
                return ObjectMapper.Map<ListingOutputDto>(output.SingleOrDefault()); ;
            }
        }
        [AbpAuthorize(PermissionNames.Pages_Administration_Listing_Detail)]
        public async Task<List<ListingOutputDto>> MatchingListing(long inquiryId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ListingOutputDto>("SpFilterMatchingUnitRes",
                   new
                   {
                       @InquiryId = inquiryId
                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<ListingOutputDto>>(output);
                return results;
            }
        }

    }
}
