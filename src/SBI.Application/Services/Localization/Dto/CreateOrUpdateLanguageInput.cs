﻿using System.ComponentModel.DataAnnotations;

namespace CRM.Services.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}