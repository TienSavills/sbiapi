﻿using System.Collections.Generic;

namespace CRM.Services.Localization.Dto
{
    public class ClientLanguageTextListDto
    {
        public string Id { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public Dictionary<string, string> Data { get; set; }
    }
}
