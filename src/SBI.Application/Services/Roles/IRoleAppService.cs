﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CRM.Services.Roles.Dto;

namespace CRM.Services.Roles
{
    public interface IRoleAppService : IAsyncCrudAppService<RoleDto, int, PagedResultRequestDto, CreateRoleDto, RoleDto>
    {
        Task<ListResultDto<FlatPermissionDto>> GetAllPermissions();
        Task<GetRoleForEditOutput> GetRoleForEdit(EntityDto input);
        Task<ListResultDto<RoleListDto>> GetRolesAsync(GetRolesInput input);
    }
}