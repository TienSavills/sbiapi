﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
