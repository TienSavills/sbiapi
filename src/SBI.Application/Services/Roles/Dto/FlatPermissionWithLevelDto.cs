﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.Roles.Dto
{
    public class FlatPermissionWithLevelDto : FlatPermissionDto
    {
        public int Level { get; set; }
    }
}
