﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.Company;
using CRM.Application.Shared.CompanyAddress;
using CRM.Application.Shared.CompanyContact;
using CRM.Application.Shared.CompanyEmail;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyPhone;
using CRM.Application.Shared.CompanyTypeMap;
using CRM.Application.Shared.CompanyUser;
using CRM.Entities.Company;
using CRM.Entities.CompanyAddress;
using CRM.Entities.CompanyContact;
using CRM.Entities.CompanyEmail;
using CRM.Entities.CompanyOrganizationUnit;
using CRM.Entities.CompanyPhone;
using CRM.Entities.CompanyTypeMap;
using CRM.Entities.CompanyUser;
using CRM.Entities.Countries;
using CRM.Entities.Districs;
using CRM.Entities.OtherCategory;
using CRM.Entities.Provinces;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using CRM.Services.Organizations;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
//using CRM.Entities.PropertyLandlords;
//using CRM.Entities.PropertyTenants;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
//using CRM.Entities.ProjectTenant;
//using CRM.Entities.ProjectLandlord;
using System.Threading.Tasks;

namespace CRM.Services.Company
{
    [AbpAuthorize]
    public class CompanyAppService : SBIAppServiceBase, ICompanyAppService
    {
        private readonly IRepository<Entities.Company.Company, long> _repositoryCompany;
        //private readonly IRepository<ProjectTenant, long> _repositoryProjectTenant;
        //private readonly IRepository<ProjectLandlord, long> _repositoryProjectLandlord;
        //private readonly IRepository<PropertyTenant, long> _repositoryPropertyTenant;
        //private readonly IRepository<PropertyLandlord, long> _repositoryPropertyLandlord;
        //private readonly IRepository<TenantType, int> _repositoryTenantType;
        private readonly OrganizationUnitAppService _servicesOrganizationUnit;
        private readonly IRepository<CompanyAddress, long> _repositoryCompanyAddress;
        private readonly IRepository<CompanyEmail, long> _repositoryCompanyEmail;
        private readonly IRepository<CompanyPhone, long> _repositoryCompanyPhone;
        private readonly IRepository<Province, int> _repositoryProvince;
        private readonly IRepository<District, int> _repositoryDistrict;
        private readonly IRepository<Country, int> _repositoryCountry;
        private readonly IRepository<CompanyOrganizationUnit, long> _repositoryCompanyOrganizationUnit;
        private readonly IRepository<CompanyUser, long> _repositoryCompanyUser;
        private readonly IRepository<OtherCategory, int> _repositoryOtherCategory;
        private readonly IRepository<CompanyContact, long> _repositoryCompanyContact;
        private readonly IRepository<Entities.Opportunity.Opportunity, long> _repositoryOpportunity;
        private readonly IRepository<Entities.UnitHistory.UnitHistory, long> _repositoryUnitHistory;
        private readonly IRepository<Entities.Projects.Project, long> _repositoryProject;
        private readonly IRepository<Entities.Activity.Activity, long> _repositoryActivity;
        private readonly IRepository<Entities.Comment.Comment, long> _repositoryComment;
        private readonly IRepository<CompanyTypeMap, long> _repositoryCompanyTypeMap;
        private readonly ConfigSetting _options;
        bool invalid = false;

        public CompanyAppService(IRepository<Entities.Company.Company, long> repositoryCompany
            , IRepository<Entities.Comment.Comment, long> repositoryComment
            , IRepository<CompanyAddress, long> repositoryCompanyAddress
            , IRepository<Province, int> repositoryProvince
            , IRepository<District, int> repositoryDistrict
            , IRepository<Country, int> repositoryCountry
            , IRepository<CompanyEmail, long> repositoryCompanyEmail
            , IRepository<CompanyPhone, long> repositoryCompanyPhone
            , IRepository<CompanyOrganizationUnit, long> repositoryCompanyOrganizationUnit
            , IRepository<CompanyUser, long> repositoryCompanyUser
            , IRepository<OtherCategory, int> repositoryOtherCategory
            , IRepository<CompanyContact, long> repositoryCompanyContact
            , IRepository<Entities.Opportunity.Opportunity, long> repositoryOpportunity
            , IRepository<Entities.UnitHistory.UnitHistory, long> repositoryUnitHistory
            , IRepository<Entities.Projects.Project, long> repositoryProject
            , IRepository<Entities.Activity.Activity, long> repositoryActivity
            , IRepository<Entities.CompanyTypeMap.CompanyTypeMap, long> repositoryCompanyTypeMap
            , OrganizationUnitAppService servicesOrganizationUnit
            , IOptions<ConfigSetting> options
            )
        {
            _servicesOrganizationUnit = servicesOrganizationUnit;
            _repositoryCompanyTypeMap = repositoryCompanyTypeMap;
            _repositoryUnitHistory = repositoryUnitHistory;
            _repositoryCompany = repositoryCompany;
            _repositoryActivity = repositoryActivity;
            _repositoryComment = repositoryComment;
            _repositoryProject = repositoryProject;
            _repositoryOpportunity = repositoryOpportunity;
            _repositoryCompanyAddress = repositoryCompanyAddress;
            _repositoryProvince = repositoryProvince;
            _repositoryDistrict = repositoryDistrict;
            _repositoryCountry = repositoryCountry;
            _repositoryCompanyEmail = repositoryCompanyEmail;
            _repositoryCompanyPhone = repositoryCompanyPhone;
            _repositoryCompanyOrganizationUnit = repositoryCompanyOrganizationUnit;
            _repositoryCompanyUser = repositoryCompanyUser;
            _repositoryOtherCategory = repositoryOtherCategory;
            _repositoryCompanyContact = repositoryCompanyContact;
            _options = options.Value;
        }
        #region Public API
        //public async Task<List<AuditLogOutput>> GetAuditLogs(long id)
        //{
        //    var result = await _auditLogServiceCore.GetAuditLogs<Entities.Company.Company>(id, async entities =>
        //    {
        //        #region Team Display Name

        //        var listTeam = entities.Where(x => x.PropertyName == nameof(Entities.Company.Company.LeadSourceId)).ToList();
        //        await _workOrderTeamCore.AppendTeam<AuditLogSimpleDto, TeamOutputDto>(
        //            x => _parseHelper.ParseInt(x.NewValue) ?? 0,
        //            (x, team) => x.NewValueDisplay = team?.Name,
        //            listTeam.ToArray());
        //        await _workOrderTeamCore.AppendTeam<AuditLogSimpleDto, TeamOutputDto>(
        //            x => _parseHelper.ParseInt(x.OriginalValue) ?? 0,
        //            (x, team) => x.OriginalValueDisplay = team?.Name,
        //            listTeam.ToArray());

        //        #endregion

        //        #region TeamUser Display Name

        //        var listTeamUser = entities.Where(x => x.PropertyName == nameof(WorkOrder.TeamUserId)).ToList();
        //        await _workOrderManagementCore.AppendTeamUser<AuditLogSimpleDto, TeamOutputDto>(
        //            x => _parseHelper.ParseInt(x.NewValue) ?? 0,
        //            (x, teamUser) => x.NewValueDisplay = teamUser?.Name,
        //            listTeamUser.ToArray());
        //        await _workOrderManagementCore.AppendTeamUser<AuditLogSimpleDto, TeamOutputDto>(
        //            x => _parseHelper.ParseInt(x.OriginalValue) ?? 0,
        //            (x, teamUser) => x.OriginalValueDisplay = teamUser?.Name,
        //            listTeamUser.ToArray());

        //        #endregion

        //        #region ObserverUser Display Name

        //        var listObserverUser = entities.Where(x => x.PropertyName == nameof(WorkOrder.ObserverUserId)).ToList();
        //        await _userManager.AppendMember(
        //            (x, user) => x.NewValueDisplay = user?.DisplayName,
        //            x => _parseHelper.ParseLong(x.NewValue),
        //            listObserverUser.ToArray());
        //        await _userManager.AppendMember(
        //            (x, user) => x.OriginalValueDisplay = user?.DisplayName,
        //            x => _parseHelper.ParseLong(x.OriginalValue),
        //            listObserverUser.ToArray());

        //        #endregion

        //        #region Status Display Name

        //        var listStatus = entities.Where(x => x.PropertyName == nameof(WorkOrder.StatusId)).ToList();
        //        await _workOrderManagementCore.AppendStatus(
        //            (x, status) => x.NewValueDisplay = status?.Name,
        //            x => _parseHelper.ParseLong(x.NewValue) ?? 0,
        //            listStatus.ToArray());
        //        await _workOrderManagementCore.AppendStatus(
        //            (x, status) => x.OriginalValueDisplay = status?.Name,
        //            x => _parseHelper.ParseLong(x.OriginalValue) ?? 0,
        //            listStatus.ToArray());

        //        #endregion

        //        #region Priority Display Name

        //        var listPriority = entities.Where(x => x.PropertyName == nameof(WorkOrder.PriorityId)).ToList();
        //        await _workOrderManagementCore.AppendPriority(
        //            (x, priority) => x.NewValueDisplay = priority?.Name,
        //            x => _parseHelper.ParseLong(x.NewValue) ?? 0,
        //            listPriority.ToArray());
        //        await _workOrderManagementCore.AppendPriority(
        //            (x, priority) => x.OriginalValueDisplay = priority?.Name,
        //            x => _parseHelper.ParseLong(x.OriginalValue) ?? 0,
        //            listPriority.ToArray());

        //        #endregion

        //        #region Area Display Name

        //        var listArea = entities.Where(x => x.PropertyName == nameof(WorkOrder.AreaId)).ToList();
        //        await _workOrderManagementCore.AppendArea(
        //            (x, area) => x.NewValueDisplay = area?.Name,
        //            x => _parseHelper.ParseLong(x.NewValue) ?? 0,
        //            listArea.ToArray());
        //        await _workOrderManagementCore.AppendArea(
        //            (x, area) => x.OriginalValueDisplay = area?.Name,
        //            x => _parseHelper.ParseLong(x.OriginalValue) ?? 0,
        //            listArea.ToArray());

        //        #endregion

        //        #region Category Display Name

        //        var listCategory = entities.Where(x => x.PropertyName == nameof(WorkOrder.CategoryId)).ToList();
        //        await _workOrderManagementCore.AppendCategory<AuditLogSimpleDto, WoManagementSimpleDto>(
        //            (x, category) => x.NewValueDisplay = category?.Name,
        //            x => _parseHelper.ParseLong(x.NewValue) ?? 0,
        //            listCategory.ToArray());
        //        await _workOrderManagementCore.AppendCategory<AuditLogSimpleDto, WoManagementSimpleDto>(
        //            (x, category) => x.OriginalValueDisplay = category?.Name,
        //            x => _parseHelper.ParseLong(x.OriginalValue) ?? 0,
        //            listCategory.ToArray());

        //        #endregion

        //        #region SubCategory Display Name

        //        var listSubCategory = entities.Where(x => x.PropertyName == nameof(WorkOrder.SubCategoryId)).ToList();
        //        await _workOrderManagementCore.AppendSubCategory<AuditLogSimpleDto, WoManagementSimpleDto>(
        //            (x, subCategory) => x.NewValueDisplay = subCategory?.Name,
        //            x => _parseHelper.ParseLong(x.NewValue) ?? 0,
        //            listSubCategory.ToArray());
        //        await _workOrderManagementCore.AppendSubCategory<AuditLogSimpleDto, WoManagementSimpleDto>(
        //            (x, subCategory) => x.OriginalValueDisplay = subCategory?.Name,
        //            x => _parseHelper.ParseLong(x.OriginalValue) ?? 0,
        //            listSubCategory.ToArray());

        //        #endregion

        //        #region PaymentStatus Display Name

        //        var listPaymentStatus = entities.Where(x => x.PropertyName == nameof(WorkOrder.PaymentStatusId)).ToList();
        //        await _workOrderManagementCore.AppendPaymentStatus(
        //            (x, paymentStatus) => x.NewValueDisplay = paymentStatus?.Name,
        //            x => _parseHelper.ParseLong(x.NewValue) ?? 0,
        //            listPaymentStatus.ToArray());
        //        await _workOrderManagementCore.AppendPaymentStatus(
        //            (x, paymentStatus) => x.OriginalValueDisplay = paymentStatus?.Name,
        //            x => _parseHelper.ParseLong(x.OriginalValue) ?? 0,
        //            listPaymentStatus.ToArray());

        //        #endregion

        //    }, new[]
        //    {
        //        nameof(WorkOrder.TeamId),
        //        nameof(WorkOrder.TeamUserId),
        //        nameof(WorkOrder.ObserverUserId),
        //        nameof(WorkOrder.StatusId),
        //        nameof(WorkOrder.PriorityId),
        //        nameof(WorkOrder.AreaId),
        //        nameof(WorkOrder.CategoryId),
        //        nameof(WorkOrder.SubCategoryId),
        //        nameof(WorkOrder.PaymentStatusId),
        //        nameof(WorkOrder.StartDate),
        //        nameof(WorkOrder.EndDate),
        //        nameof(WorkOrder.ActualStartDate),
        //        nameof(WorkOrder.ActualEndDate),
        //        nameof(WorkOrder.Description),
        //        nameof(WorkOrder.OtherDescription)
        //    });
        //    return result;
        //}
        public async Task<CompanyOutputDto> CreateOrUpdateCompanyFullAsync(CompanyFullInputDto input)
        {
            ValidateCreateOrUpdateCompanyFull(input);
            var entity = new Entities.Company.Company();
            if (input.Id.HasValue && input.Id > 0)
            {
                entity = await _repositoryCompany.GetAsync(input.Id.Value);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repositoryCompany.UpdateAsync(entityUpdate);
            }
            else
            {
                var entityInsert = ObjectMapper.Map(input, entity);
                entity.IsActive = true;
                entity.IsVerified = true;
                entity.Id = await _repositoryCompany.InsertAndGetIdAsync(entityInsert);
            }
            var result = ObjectMapper.Map<CompanyOutputDto>(entity);
            if (input.CompanyTypeIds != null)
            {
                ValidateCompanyTypeV1(input.CompanyTypeIds);
                // result.CompanyTypeMap = ObjectMapper.Map<List<CompanyTypeMapOutputDto>>(CreateOrUpdateListCompanyTypeV1(entity.Id, input.CompanyTypeIds));
                await UpdateChilds(
                       _repositoryCompanyTypeMap,
                       x => x.CompanyId = entity.Id,
                       x => x.CompanyId == entity.Id,
                       (x, y) => x.CompanyTypeId == y.CompanyTypeId,
                       input.CompanyTypeIds.Select(x => new CompanyTypeMapInputDto { CompanyTypeId = x, CompanyId = entity.Id, IsActive = true }).ToArray()
                       );
            }
            if (input.CompanyAddress != null)
            {
                ValidateCompanyListAddress(input.CompanyAddress);
                await UpdateChilds(
                          _repositoryCompanyAddress,
                          x => x.CompanyId = entity.Id,
                          x => x.CompanyId == entity.Id,
                          (x, y) => x.Id == y.Id,
                          input.CompanyAddress.ToArray()
                          );
            }
            if (input.CompanyContact != null)
            {
                ValidateCompanyContactList(entity.Id, input.CompanyContact);
                //result.CompanyContact = ObjectMapper.Map<List<CompanyContactOutputDto>>(CreateOrUpdateListCompanyContactV1(entity.Id, input.CompanyContact));
                await UpdateChilds(
                         _repositoryCompanyContact,
                         x => x.CompanyId = entity.Id,
                         x => x.CompanyId == entity.Id,
                         (x, y) => x.ContactId == y.ContactId,
                         input.CompanyContact.Select(x => new CompanyContactInputDto { CompanyId = entity.Id, ContactId = x.ContactId, IsPrimary = x.IsPrimary, IsActive = true }).ToArray()
                         );
            }
            if (input.CompanyOrganizationUnitIds != null)
            {
                await UpdateChilds(
                       _repositoryCompanyOrganizationUnit,
                       x => x.CompanyId = entity.Id,
                       x => x.CompanyId == entity.Id,
                       (x, y) => x.OrganizationUnitId == y.OrganizationUnitId,
                        input.CompanyOrganizationUnitIds.Select(x => new CompanyOrganizationUnitInputDto { OrganizationUnitId = x, CompanyId = entity.Id, IsActive = true }).ToArray()
                       );
                // result.CompanyOrganizationUnit = ObjectMapper.Map<List<CompanyOrganizationUnitV1OutputDto>>(CreateOrUpdateListCompanyOrganizationUnitV1(entity.Id, input.CompanyOrganizationUnitIds));
            }
            if (input.CompanyUserIds != null)
            {
                // result.CompanyUser = ObjectMapper.Map<List<CompanyUserV1OutputDto>>(CreateOrUpdateCompanyUserV1(entity.Id, input.CompanyUserIds));

                await UpdateChilds(
                       _repositoryCompanyUser,
                       x => x.CompanyId = entity.Id,
                       x => x.CompanyId == entity.Id,
                       (x, y) => x.UserId == y.UserId,
                        input.CompanyUserIds.Select(x => new CompanyUserInputDto { UserId = x, CompanyId = entity.Id, IsActive = true }).ToArray()
                       );
            }
            //if (input.CompanyUser != null)
            //{
            //    ValidateCompanyListUser(entity.Id, input.CompanyUser);
            //    result.CompanyUser = ObjectMapper.Map<List<CompanyUserOutputDto>>(CreateOrUpdateCompanyUser(entity.Id, input.CompanyUser));
            //}
            if (input.CompanyPhone != null)
            {
                ValidateCompanyListPhone(input.CompanyPhone);
                await UpdateChilds(
                       _repositoryCompanyPhone,
                       x => x.CompanyId = entity.Id,
                       x => x.CompanyId == entity.Id,
                       (x, y) => x.Phone == y.Phone,
                       input.CompanyPhone.Select(x => new CompanyPhoneInputDto { CompanyId = entity.Id, CountryId = x.CountryId, Phone = x.Phone, PhoneTypeId = x.PhoneTypeId, IsPrimary = x.IsPrimary, IsActive = true }).ToArray()
                       );
                //result.CompanyPhone = ObjectMapper.Map<List<CompanyPhoneOutputDto>>(CreateOrUpdateListCompanyPhoneV1(entity.Id, input.CompanyPhone));
            }
            return await GetDetails(entity.Id);
        }
        public async Task<CompanyOutputDto> CreateCompanyAsync(CompanyInputDto input)
        {
            ValidateCreateOrUpdateCompany(input);
            var entity = ObjectMapper.Map<Entities.Company.Company>(input);
            entity.IsActive = true;
            entity.IsVerified = true;
            entity.Id = await _repositoryCompany.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<CompanyOutputDto>(entity);
        }
        public async Task<CompanyOutputDto> UpdateCompanyAsync(long companyId, CompanyInputDto input)
        {
            if (companyId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("CompanyIdNotFound"));
            ValidateCreateOrUpdateCompany(input);
            var entity = await _repositoryCompany.GetAsync(companyId);
            entity.Id = companyId;
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repositoryCompany.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<CompanyOutputDto>(entity);
        }
        public async Task<CompanyOutputDto> UpdateCompanyAddressAsync(long companyId, List<CompanyAddressInputDto> companyAddress)
        {
            var entity = await _repositoryCompany.GetAsync(companyId);
            var output = ObjectMapper.Map<CompanyOutputDto>(entity);
            if (companyAddress != null && companyAddress.Any())
            {
                ValidateCompanyListAddress(companyAddress);
                output.CompanyAddress = ObjectMapper.Map<List<CompanyAddressOutputDto>>(CreateOrUpdateListCompanyAddress(entity.Id, companyAddress));
            }
            return output;
        }
        public List<CompanyTypeMapOutputDto> CreateOrUpdateCompanyTypeMapV1(long companyId, List<int> companyTypeIds)
        {
            ValidateCompanyTypeV1(companyTypeIds);
            var output = ObjectMapper.Map<List<CompanyTypeMapOutputDto>>(CreateOrUpdateListCompanyTypeV1(companyId, companyTypeIds));
            return output;
        }
        public List<CompanyTypeMapOutputDto> CreateOrUpdateCompanyTypeMap(List<CompanyTypeMapInputDto> companyTypes)
        {
            ValidateCompanyType(companyTypes);
            var output = ObjectMapper.Map<List<CompanyTypeMapOutputDto>>(CreateOrUpdateListCompanyType(companyTypes));
            return output;
        }
        public List<CompanyPhoneOutputDto> CreateOrUpdateCompanyPhone(long companyId, List<CompanyPhoneInputDto> companyPhones)
        {
            ValidateCompanyListPhone(companyPhones);
            var output = ObjectMapper.Map<List<CompanyPhoneOutputDto>>(CreateOrUpdateListCompanyPhone(companyId, companyPhones));
            return output;
        }
        public List<CompanyEmailOutputDto> CreateOrUpdateCompanyEmail(long companyId, List<CompanyEmailInputDto> companyEmails)
        {
            ValidateCompanyListEmail(companyEmails);
            var output = ObjectMapper.Map<List<CompanyEmailOutputDto>>(CreateOrUpdateListCompanyEmail(companyId, companyEmails));
            return output;
        }
        public List<CompanyUserOutputDto> CreateOrUpdateCompanyUser(long companyId, List<CompanyUserInputDto> companyUsers)
        {
            ValidateCompanyListUser(companyId, companyUsers);
            var output = ObjectMapper.Map<List<CompanyUserOutputDto>>(CreateOrUpdateListCompanyUser(companyId, companyUsers));
            return output;
        }
        public List<CompanyUserOutputDto> CreateOrUpdateCompanyUserV1(long companyId, List<long> userIds)
        {
            var output = ObjectMapper.Map<List<CompanyUserOutputDto>>(CreateOrUpdateListCompanyUserV1(companyId, userIds));
            return output;
        }
        public List<CompanyOrganizationUnitOutputDto> CreateOrUpdateCompanyOrganizationUnit(long companyId, List<CompanyOrganizationUnitInputDto> companyCompanyOrganizationUnits)
        {
            ValidateCompanyListOrganizationUnit(companyId, companyCompanyOrganizationUnits);
            var output = ObjectMapper.Map<List<CompanyOrganizationUnitOutputDto>>(CreateOrUpdateListCompanyOrganizationUnit(companyId, companyCompanyOrganizationUnits));
            return output;
        }
        public List<CompanyOrganizationUnitOutputDto> CreateOrUpdateCompanyOrganizationUnitV1(long companyId, List<long> organizationUnitIds)
        {
            var output = ObjectMapper.Map<List<CompanyOrganizationUnitOutputDto>>(CreateOrUpdateListCompanyOrganizationUnitV1(companyId, organizationUnitIds));
            return output;
        }
        public async Task<PagedResultDto<CompanyOutputDto>> GetListCompany(string filterJson)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<CompanyFilterDto>("[SpFilterCompany]",
                    new
                    {
                        @FilterJson = filterJson,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<CompanyOutputDto>>(output);
                return new PagedResultDto<CompanyOutputDto>(totalCount, results);
            }
        }

        public async Task<CompanyOutputDto> GetCompanyDetails(long id)
        {
            return await GetDetails(id);
        }
        public async Task<PagedResultDto<CompanyEmailOutputDto>> GetListCompanyEmail(FilterBasicInputDto input, long companyId)
        {
            var query = _repositoryCompanyEmail.GetAll()
                .Where(x => x.CompanyId == companyId && x.IsActive)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.Email.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<CompanyEmailOutputDto>>(results);
            return new PagedResultDto<CompanyEmailOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<CompanyPhoneOutputDto>> GetListCompanyPhone(FilterBasicInputDto input, long companyId)
        {
            var query = _repositoryCompanyPhone.GetAll()
                .Include(x => x.PhoneType)
                .Where(x => x.CompanyId == companyId && x.IsActive)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.Phone.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<CompanyPhoneOutputDto>>(results);
            return new PagedResultDto<CompanyPhoneOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<CompanyOrganizationUnitOutputDto>> GetListCompanyOrganizationUnit(FilterBasicInputDto input, long companyId)
        {
            var query = _repositoryCompanyOrganizationUnit.GetAll()
                .Include(x => x.OrganizationUnit)
                .Where(x => x.CompanyId == companyId && x.IsActive);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<CompanyOrganizationUnitOutputDto>>(results);
            return new PagedResultDto<CompanyOrganizationUnitOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<CompanyUserOutputDto>> GetListCompanyUser(FilterBasicInputDto input, long companyId)
        {
            var query = _repositoryCompanyUser.GetAll()
                .Include(x => x.User)
                .Where(x => x.CompanyId == companyId && x.IsActive);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<CompanyUserOutputDto>>(results);
            return new PagedResultDto<CompanyUserOutputDto>(
                totalCount,
                output
            );
        }
        public async Task<PagedResultDto<CompanyContactOutputDto>> GetListCompanyContact(FilterBasicInputDto input, long companyId)
        {
            var query = _repositoryCompanyContact.GetAll()
              .Include(x => x.Contact)
              .Where(x => x.CompanyId == companyId && x.IsActive);
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber - 1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.LastModificationTime)
                .ThenBy(x => x.CreationTime)
                .ToListAsync();
            var output = ObjectMapper.Map<List<CompanyContactOutputDto>>(results);
            return new PagedResultDto<CompanyContactOutputDto>(
                totalCount,
                output
            );
        }

        public List<CompanyContactOutputDto> CreateOrUpdateCompanyContact(long companyId, List<CompanyContactInputDto> input)
        {
            ValidateCompanyContactList(companyId, input);
            var output = ObjectMapper.Map<List<CompanyContactOutputDto>>(CreateOrUpdateListCompanyContact(companyId, input));
            return output;
        }
        public List<CompanyContactOutputDto> CreateOrUpdateCompanyContactV1(long companyId, List<CompanyContactInputDto> input)
        {
            ValidateCompanyContactList(companyId, input);
            var output = ObjectMapper.Map<List<CompanyContactOutputDto>>(CreateOrUpdateListCompanyContactV1(companyId, input));
            return output;
        }
        public async Task<bool> MergeCompany(List<long> sourceCompanyIds, long targetCompanyId)
        {
            //check permission
            var ou = await _servicesOrganizationUnit.GetOrganizationUnitsByUser(AbpSession.UserId.Value);
            if (!ou.Items.Any(x => x.Id == Organizations.OrganizationsConstant.Manager))
            {
                throw new UserFriendlyException(L("ContactSBITeam"));
            }
            // update from contact source to contact target
            var targetContactIds = await _repositoryCompanyContact.GetAll().Where(x => x.CompanyId == targetCompanyId && x.IsActive == true).Select(x => x.ContactId).ToListAsync();
            foreach (var sourceCompanyId in sourceCompanyIds)
            {
                var sourceContacts = await _repositoryCompanyContact.GetAll().Where(x => x.CompanyId == sourceCompanyId && x.IsActive == true).ToListAsync();
                foreach (var sourceContact in sourceContacts)
                {
                    if (!targetContactIds.Contains(sourceContact.ContactId))
                    {
                        sourceContact.CompanyId = targetCompanyId;
                        sourceContact.IsPrimary = false;
                        await _repositoryCompanyContact.InsertOrUpdateAsync(sourceContact);
                    }
                }
            }
            // update from activity source to activity target
            foreach (var sourceCompanyId in sourceCompanyIds)
            {
                var sourceActivitys = await _repositoryActivity.GetAll().Where(x => x.ReferenceId == sourceCompanyId && x.IsActive == true
                && x.ModuleId == Activiy.ActivityConstant.Company).ToListAsync();
                foreach (var sourceActivity in sourceActivitys)
                {
                    sourceActivity.ReferenceId = targetCompanyId;
                    await _repositoryActivity.InsertOrUpdateAsync(sourceActivity);
                }
            }

            // update from comment sourece to comment target
            foreach (var sourceCompanyId in sourceCompanyIds)
            {
                var sourceComments = await _repositoryComment.GetAll().Where(x => x.ReferenceId == sourceCompanyId && x.IsActive == true
                && x.ModuleId == Activiy.ActivityConstant.Company).ToListAsync();
                foreach (var sourceComment in sourceComments)
                {
                    sourceComment.ReferenceId = targetCompanyId;
                    await _repositoryComment.InsertOrUpdateAsync(sourceComment);
                }
            }

            // update from opportunity source to opportunity target
            var targetOpportunityIds = await _repositoryOpportunity.GetAll().Where(x => x.CompanyId == targetCompanyId && x.IsActive == true).Select(x => x.Id).ToListAsync();
            foreach (var sourceCompanyId in sourceCompanyIds)
            {
                var sourceOpportunitys = await _repositoryOpportunity.GetAll().Where(x => x.CompanyId == sourceCompanyId && x.IsActive == true).ToListAsync();
                foreach (var sourceOpportunity in sourceOpportunitys)
                {
                    if (!targetOpportunityIds.Contains(sourceOpportunity.Id))
                    {
                        sourceOpportunity.CompanyId = targetCompanyId;
                        await _repositoryOpportunity.InsertOrUpdateAsync(sourceOpportunity);
                    }
                }
            }
            // update from Unithistory source to Unithistory target
            var targetUnitHistoryIds = await _repositoryUnitHistory.GetAll().Where(x => x.OrgTenantId == targetCompanyId && x.IsActive == true).Select(x => x.Id).ToListAsync();
            foreach (var sourceCompanyId in sourceCompanyIds)
            {
                var sourceUnitHistorys = await _repositoryUnitHistory.GetAll().Where(x => x.OrgTenantId == sourceCompanyId && x.IsActive == true).ToListAsync();
                foreach (var sourceUnitHistory in sourceUnitHistorys)
                {
                    if (!targetUnitHistoryIds.Contains(sourceUnitHistory.Id))
                    {
                        sourceUnitHistory.OrgTenantId = targetCompanyId;
                        await _repositoryUnitHistory.InsertOrUpdateAsync(sourceUnitHistory);
                    }
                }
            }
            // update from Project PM, Landlord source to Project PM, Landlord target
            foreach (var sourceCompanyId in sourceCompanyIds)
            {
                var sourceProjects = await _repositoryProject.GetAll().Where(x => (x.PropertyManagementId == sourceCompanyId || x.LandlordId == sourceCompanyId) && x.IsActive == true).ToListAsync();
                foreach (var sourceProject in sourceProjects)
                {
                    if (sourceProject.LandlordId == sourceCompanyId)
                        sourceProject.LandlordId = targetCompanyId;
                    if (sourceProject.PropertyManagementId == sourceCompanyId)
                        sourceProject.PropertyManagementId = targetCompanyId;
                    await _repositoryProject.InsertOrUpdateAsync(sourceProject);
                }
            }
            // deactive sourceCompany
            foreach (var sourceCompanyId in sourceCompanyIds)
            {
                var sourceCompanys = await _repositoryCompany.GetAll().Where(x => x.Id == sourceCompanyId && x.IsActive == true).ToListAsync();
                foreach (var sourceCompany in sourceCompanys)
                {
                    sourceCompany.IsActive = false;
                    sourceCompany.Description = "Merged from " + targetCompanyId.ToString();
                    await _repositoryCompany.InsertOrUpdateAsync(sourceCompany);
                }
            }
            return true;
        }
        #endregion

        #region Action
        private List<CompanyContact> CreateOrUpdateListCompanyContact(long companyId, List<CompanyContactInputDto> inputs)
        {
            var entities = new List<CompanyContact>();
            inputs.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyContact(companyId, x));
            });
            return entities;
        }
        private List<CompanyContactOutputDto> CreateOrUpdateListCompanyContactV1(long companyId, List<CompanyContactInputDto> inputs)
        {
            var currentCompanyContact = _repositoryCompanyContact.GetAll().Where(x => x.CompanyId == companyId).ToList();
            var companyContactIds = inputs.Select(x => x.Id).ToList();
            var entities = new List<CompanyContact>();
            inputs.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyContact(companyId, x));
            });
            var deleteEntities = currentCompanyContact.Where(x => companyContactIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryCompanyContact.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listCompanyContact = _repositoryCompanyContact.GetAll().Where(x => x.CompanyId == companyId && x.IsActive);
            return ObjectMapper.Map<List<CompanyContactOutputDto>>(listCompanyContact);
        }
        private List<CompanyAddress> CreateOrUpdateListCompanyAddress(long companyId, List<CompanyAddressInputDto> CompanyAddress)
        {

            var entities = new List<CompanyAddress>();
            CompanyAddress.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyAddress(companyId, x));
            });
            return entities;
        }
        private List<CompanyAddressOutputDto> CreateOrUpdateListCompanyAddressV1(long companyId, List<CompanyAddressInputDto> companyAddress)
        {
            var currentCompanyAddress = _repositoryCompanyAddress.GetAll().Where(x => x.CompanyId == companyId).ToList();
            var companyAddressIds = companyAddress.Select(x => x.Id).ToList();
            var entities = new List<CompanyAddress>();
            companyAddress.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyAddress(companyId, x));
            });
            var deleteEntities = currentCompanyAddress.Where(x => companyAddressIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryCompanyAddress.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listCompanyAddress = _repositoryCompanyAddress.GetAll().Where(x => x.CompanyId == companyId && x.IsActive);
            return ObjectMapper.Map<List<CompanyAddressOutputDto>>(listCompanyAddress);
        }
        private List<CompanyOrganizationUnit> CreateOrUpdateListCompanyOrganizationUnit(long companyId, List<CompanyOrganizationUnitInputDto> companyOrganizationUnits)
        {

            var entities = new List<CompanyOrganizationUnit>();
            companyOrganizationUnits.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyOrganizationUnit(companyId, x));
            });
            return entities;
        }

        private List<CompanyEmail> CreateOrUpdateListCompanyEmail(long companyId, List<CompanyEmailInputDto> companyEmails)
        {

            var entities = new List<CompanyEmail>();
            companyEmails.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyEmail(companyId, x));
            });
            return entities;
        }
        private List<CompanyTypeMapOutputDto> CreateOrUpdateListCompanyType(List<CompanyTypeMapInputDto> companyTypes)
        {
            ValidateCompanyType(companyTypes);
            var entities = new List<CompanyTypeMapOutputDto>();
            companyTypes.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyType(x));
            });
            return entities;
        }
        private List<CompanyTypeMapOutputDto> CreateOrUpdateListCompanyTypeV1(long companyId, List<int> companyTypeIds)
        {

            var currentCompanyTypes = _repositoryCompanyTypeMap.GetAll().Where(x => x.CompanyId == companyId).ToList();
            companyTypeIds.ForEach(x =>
            {
                var objUpdate = currentCompanyTypes.FirstOrDefault(obj => obj.CompanyTypeId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryCompanyTypeMap.Update(objUpdate);
                    return;
                }
                var entity = new CompanyTypeMap
                {
                    CompanyId = companyId,
                    CompanyTypeId = x,
                    IsActive = true
                };
                _repositoryCompanyTypeMap.Insert(entity);
            });
            var deleteEntities = currentCompanyTypes.Where(x => companyTypeIds.All(y => y != x.CompanyTypeId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryCompanyTypeMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listCompanyTypeMaps = _repositoryCompanyTypeMap.GetAll().Where(x => x.CompanyId == companyId && x.IsActive)
                .Include(x => x.CompanyType).ToList();
            return ObjectMapper.Map<List<CompanyTypeMapOutputDto>>(listCompanyTypeMaps);
        }
        private List<CompanyPhone> CreateOrUpdateListCompanyPhone(long companyId, List<CompanyPhoneInputDto> companyPhones)
        {
            var entities = new List<CompanyPhone>();
            companyPhones.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyPhone(companyId, x));
            });
            return entities;
        }
        private List<CompanyPhoneOutputDto> CreateOrUpdateListCompanyPhoneV1(long companyId, List<CompanyPhoneInputDto> companyPhone)
        {
            var currentCompanyPhone = _repositoryCompanyPhone.GetAll().Where(x => x.CompanyId == companyId).ToList();
            var companyPhoneIds = companyPhone.Select(x => x.Id).ToList();
            var entities = new List<CompanyPhone>();
            companyPhone.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyPhone(companyId, x));
            });
            var deleteEntities = currentCompanyPhone.Where(x => companyPhoneIds.All(y => y != x.Id)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryCompanyPhone.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listCompanyPhone = _repositoryCompanyPhone.GetAll().Where(x => x.CompanyId == companyId && x.IsActive);
            return ObjectMapper.Map<List<CompanyPhoneOutputDto>>(listCompanyPhone);
        }
        private List<CompanyUser> CreateOrUpdateListCompanyUser(long companyId, List<CompanyUserInputDto> companyUsers)
        {
            var entities = new List<CompanyUser>();
            companyUsers.ForEach(x =>
            {
                entities.Add(CreateOrUpdateCompanyUser(companyId, x));
            });
            return entities;
        }
        private List<CompanyUserOutputDto> CreateOrUpdateListCompanyUserV1(long companyId, List<long> userIds)
        {

            var currentCompanyUsers = _repositoryCompanyUser.GetAll().Where(x => x.CompanyId == companyId).ToList();
            userIds.ForEach(x =>
            {
                var objUpdate = currentCompanyUsers.FirstOrDefault(obj => obj.UserId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryCompanyUser.Update(objUpdate);
                    return;
                }
                var entity = new CompanyUser
                {
                    CompanyId = companyId,
                    UserId = x,
                    IsActive = true
                };
                _repositoryCompanyUser.Insert(entity);
            });
            var deleteEntities = currentCompanyUsers.Where(x => userIds.All(y => y != x.UserId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryCompanyUser.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listCompanyUsers = _repositoryCompanyUser.GetAll().Where(x => x.CompanyId == companyId && x.IsActive)
                .Include(x => x.User).ToList();
            return ObjectMapper.Map<List<CompanyUserOutputDto>>(listCompanyUsers);
        }
        private List<CompanyOrganizationUnitV1OutputDto> CreateOrUpdateListCompanyOrganizationUnitV1(long companyId, List<long> organizationUnitIds)
        {

            var currentCompanyOrganizationUnits = _repositoryCompanyOrganizationUnit.GetAll().Where(x => x.CompanyId == companyId).ToList();
            organizationUnitIds.ForEach(x =>
            {
                var objUpdate = currentCompanyOrganizationUnits.FirstOrDefault(obj => obj.OrganizationUnitId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryCompanyOrganizationUnit.Update(objUpdate);
                    return;
                }
                var entity = new CompanyOrganizationUnit
                {
                    CompanyId = companyId,
                    OrganizationUnitId = x,
                    IsActive = true
                };
                _repositoryCompanyOrganizationUnit.Insert(entity);
            });
            var deleteEntities = currentCompanyOrganizationUnits.Where(x => organizationUnitIds.All(y => y != x.OrganizationUnitId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryCompanyOrganizationUnit.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            var listCompanyOrganizationUnits = _repositoryCompanyOrganizationUnit.GetAll().Where(x => x.CompanyId == companyId && x.IsActive)
                .Include(x => x.OrganizationUnit).ToList();
            return ObjectMapper.Map<List<CompanyOrganizationUnitV1OutputDto>>(listCompanyOrganizationUnits);
        }
        #endregion

        #region Validate

        private bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
        public void ValidateCompanyIds(List<long> companies)
        {
            if (_repositoryCompany.GetAll().Count(x => companies.Contains(x.Id) && x.IsActive) < companies.Count)
                throw new UserFriendlyException(L("CompanyIdNotFound"));
        }
        public void ValidateCreateOrUpdateCompany(CompanyInputDto input)
        {
            if (string.IsNullOrEmpty(input.BusinessName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.BusinessName)));
            if (string.IsNullOrEmpty(input.LegalName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.LegalName)));
            if (!input.NationalityId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.NationalityId)));
            if (!input.LeadSourceId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.LeadSourceId)));
            if (!input.ClientTypeId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.ClientTypeId)));
            if (!input.IndustryId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.IndustryId)));
            if (!input.Id.HasValue)
            {
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.Vatcode) && x.Vatcode.ToUpper() == input.Vatcode.ToUpper() && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateVATCode"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.LegalName) && x.LegalName.ToUpper() == input.LegalName.ToUpper() && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.BusinessName) && x.BusinessName.ToUpper() == input.BusinessName.ToUpper() && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));

            }
            else
            {
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.Vatcode) && x.Vatcode.ToUpper() == input.Vatcode.ToUpper() && x.Id != input.Id && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateVATCode"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.LegalName) && x.LegalName.ToUpper() == input.LegalName.ToUpper() && x.Id != input.Id && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.BusinessName) && x.BusinessName.ToUpper() == input.BusinessName.ToUpper() && x.Id != input.Id && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));
            }
        }
        public void ValidateCreateOrUpdateCompanyFull(CompanyFullInputDto input)
        {
            if (string.IsNullOrEmpty(input.BusinessName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.BusinessName)));
            if (string.IsNullOrEmpty(input.LegalName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.LegalName)));
            if (!input.NationalityId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.NationalityId)));
            if (!input.LeadSourceId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.LeadSourceId)));
            if (!input.ClientTypeId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.ClientTypeId)));
            if (!input.IndustryId.HasValue)
                throw new UserFriendlyException(L("RequiredField", nameof(input.IndustryId)));
            if (!input.Id.HasValue)
            {
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.Vatcode) && x.Vatcode.ToUpper() == input.Vatcode.ToUpper() && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateVATCode"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.LegalName) && x.LegalName.ToUpper() == input.LegalName.ToUpper() && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.BusinessName) && x.BusinessName.ToUpper() == input.BusinessName.ToUpper() && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));

            }
            else
            {
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.Vatcode) && x.Vatcode.ToUpper() == input.Vatcode.ToUpper() && x.Id != input.Id && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateVATCode"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.LegalName) && x.LegalName.ToUpper() == input.LegalName.ToUpper() && x.Id != input.Id && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));
                if (_repositoryCompany.GetAll().Any(x => !string.IsNullOrEmpty(input.BusinessName) && x.BusinessName.ToUpper() == input.BusinessName.ToUpper() && x.Id != input.Id && x.IsActive == true))
                    throw new UserFriendlyException(L("DuplicateCompany"));
            }
        }

        private void ValidateCompanyListAddress(List<CompanyAddressInputDto> companyAddress)
        {
            companyAddress.ForEach(ValidateCompanyAddress);
        }
        private void ValidateCompanyAddress(CompanyAddressInputDto companyAddress)
        {
            if (companyAddress.Id > 0)
            {
                if (!_repositoryCompanyAddress.GetAll().Any(x => x.Id == companyAddress.Id))
                    throw new UserFriendlyException(L("CompanyAddressIdNotFound"));
            }
            if (companyAddress.DistrictId.HasValue)
            {
                if (!_repositoryDistrict.GetAll().Any(x => x.Id == companyAddress.DistrictId.Value))
                    throw new UserFriendlyException(L("CompanyDistrictIdNotFound"));
            }
            if (companyAddress.ProvinceId.HasValue)
            {
                if (!_repositoryProvince.GetAll().Any(x => x.Id == companyAddress.ProvinceId.Value))
                    throw new UserFriendlyException(L("CompanyProvinceIdNotFound"));
            }
            if (!_repositoryCountry.GetAll().Any(x => x.Id == companyAddress.CountryId))
                throw new UserFriendlyException(L("CompanyCountryIdNotFound"));
            if (companyAddress.Longitude != null || companyAddress.Latitude != null)
            {
                if (companyAddress.Latitude < AppConsts.GeocodeValues.MinLat
                    || companyAddress.Latitude > AppConsts.GeocodeValues.MaxLat
                    || companyAddress.Longitude < AppConsts.GeocodeValues.MinLng
                    || companyAddress.Longitude > AppConsts.GeocodeValues.MaxLng)
                    throw new UserFriendlyException(L("CompanyLatLngInvalid"));
            }
            if (string.IsNullOrEmpty(companyAddress.Address))
                throw new UserFriendlyException(L("RequiredField", nameof(CompanyAddress.Address)));
        }
        private void ValidateCompanyListEmail(List<CompanyEmailInputDto> companyEmails)
        {
            companyEmails.ForEach(ValidateCompanyEmail);
        }
        private void ValidateCompanyEmail(CompanyEmailInputDto companyEmail)
        {
            if (string.IsNullOrEmpty(companyEmail.Email))
            {
                throw new UserFriendlyException(L("RequiredField", nameof(CompanyEmail.Email)));
            }
            if (companyEmail.Id.HasValue)
            {
                if (!_repositoryCompanyEmail.GetAll().Any(x => x.Id == companyEmail.Id.Value))
                    throw new UserFriendlyException(L("CompanyEmailIdNotFound"));
                if (_repositoryCompanyEmail.GetAll().Any(x => x.Email.ToUpper() == companyEmail.Email.ToUpper()
                && x.Id != companyEmail.Id && x.IsActive))
                    throw new UserFriendlyException(L("ExistEmail"));
            }
            else
            {
                if (_repositoryCompanyEmail.GetAll().Any(x => x.Email.ToUpper() == companyEmail.Email.ToUpper() && x.IsActive))
                    throw new UserFriendlyException(L("ExistEmail"));
            }
        }
        private void ValidateCompanyTypeV1(List<int> companyTypeIds)
        {
            if (_repositoryOtherCategory.Count(x => companyTypeIds.Contains(x.Id) && x.IsActive) < companyTypeIds.Count)
                throw new UserFriendlyException(L("CompanyTypeNotFound"));
        }
        private void ValidateCompanyType(List<CompanyTypeMapInputDto> companyTypeMap)
        {
            companyTypeMap.ForEach(ValidateCompanyType);
        }
        private void ValidateCompanyType(CompanyTypeMapInputDto companyType)
        {
            if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == companyType.CompanyTypeId))
                throw new UserFriendlyException(L("CompanyTypeNotFound"));
            if (companyType.Id != 0)
            {
                if (_repositoryCompanyTypeMap.GetAll().Any(x => x.Id != companyType.Id && x.CompanyId == companyType.CompanyId && x.CompanyTypeId == companyType.CompanyTypeId && x.IsActive == true))
                    throw new UserFriendlyException(L("ExistCompanyType"));
            }
            else
            {
                if (_repositoryCompanyTypeMap.GetAll().Any(x => x.CompanyId == companyType.CompanyId && x.CompanyTypeId == companyType.CompanyTypeId && x.IsActive == true))
                    throw new UserFriendlyException(L("ExistCompanyType"));
            }
        }

        private void ValidateCompanyListPhone(List<CompanyPhoneInputDto> companyPhones)
        {
            companyPhones.ForEach(ValidateCompanyPhone);
        }
        private void ValidateCompanyPhone(CompanyPhoneInputDto companyPhone)
        {
            if (string.IsNullOrEmpty(companyPhone.Phone))
            {
                throw new UserFriendlyException(L("RequiredField", nameof(CompanyPhone.Phone)));
            }
            if (companyPhone.PhoneTypeId.HasValue)
            {
                if (!_repositoryOtherCategory.GetAll().Any(x => x.Id == companyPhone.PhoneTypeId
                && x.TypeCode.ToUpper().Contains(CompanyConstant.PhontType) && x.IsActive))
                    throw new UserFriendlyException(L("NotExistPhoneType"));
            }
            if (companyPhone.Id > 0)
            {
                if (!_repositoryCompanyPhone.GetAll().Any(x => x.Id == companyPhone.Id))
                    throw new UserFriendlyException(L("CompanyPhoneIdNotFound"));
                if (_repositoryCompanyPhone.GetAll().Include(x => x.Company).Any(x => x.Phone == companyPhone.Phone
                  && x.Id != companyPhone.Id && x.IsActive && x.Company.IsActive))
                    throw new UserFriendlyException(L("ExistPhone"));
            }
            else
            {
                if (_repositoryCompanyPhone.GetAll().Include(x => x.Company).Any(x => x.Phone == companyPhone.Phone
                && x.IsActive && x.Company.IsActive))
                    throw new UserFriendlyException(L("ExistPhone"));
            }

        }

        private void ValidateCompanyListUser(long companyId, List<CompanyUserInputDto> companyUsers)
        {
            companyUsers.ForEach(x => ValidateCompanyUser(companyId, x));
        }
        private void ValidateCompanyUser(long companyId, CompanyUserInputDto companyUser)
        {
            if (companyUser.Id > 0)
            {
                if (!_repositoryCompanyUser.GetAll().Any(x => x.Id == companyUser.Id))
                    throw new UserFriendlyException(L("CompanyUserIdNotFound"));
            }
            else
            {
                if (_repositoryCompanyUser.GetAll().Any(x => x.CompanyId == companyId && x.IsActive == true && x.UserId == companyUser.UserId && x.IsFollwer == companyUser.IsFollwer))
                    throw new UserFriendlyException(L("ExistCompanyUser"));
            }
        }
        //CompanyContact
        private void ValidateCompanyContactList(long companyId, List<CompanyContactInputDto> inputs)
        {
            inputs.ForEach(x => ValidateCompanyContact(companyId, x));
        }
        private void ValidateCompanyContact(long companyId, CompanyContactInputDto input)
        {
            if (input.Id > 0)
            {
                if (!_repositoryCompanyContact.GetAll().Any(x => x.Id == input.Id))
                    throw new UserFriendlyException(L("CompanyContactIdNotFound"));
            }
            else
            {
                if (_repositoryCompanyContact.GetAll().Any(x => x.CompanyId == companyId && x.ContactId == input.ContactId))
                    throw new UserFriendlyException(L("ExistCompanyContact"));
            }
        }

        private void ValidateCompanyListOrganizationUnit(long companyId, List<CompanyOrganizationUnitInputDto> companyOrganizationUnits)
        {
            companyOrganizationUnits.ForEach(x => ValidateCompanyOrganizationUnit(companyId, x));
        }
        private void ValidateCompanyOrganizationUnit(long companyId, CompanyOrganizationUnitInputDto companyOrganizationUnit)
        {
            //update
            if (companyOrganizationUnit.Id > 0)
            {
                if (!_repositoryCompanyOrganizationUnit.GetAll().Any(x => x.Id == companyOrganizationUnit.Id))
                    throw new UserFriendlyException(L("CompanyOrganizationIdNotFound"));
            }
            //Insert
            else
            {
                if (_repositoryCompanyOrganizationUnit.GetAll().Any(x => x.CompanyId == companyId && x.IsActive == true && x.OrganizationUnitId == companyOrganizationUnit.OrganizationUnitId))
                    throw new UserFriendlyException(L("ExistCompanyOrganizationUnit"));
            }
        }
        #endregion

        #region Private API
        //    [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        private CompanyEmail CreateOrUpdateCompanyEmail(long companyId, CompanyEmailInputDto input)
        {
            if (IsValidEmail(input.Email))
            {
                CompanyEmail entity = new CompanyEmail();
                if (input.Id != null)
                {
                    var entityEmail = _repositoryCompanyEmail.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                    entity = ObjectMapper.Map(input, entityEmail);
                }
                else
                {
                    entity = ObjectMapper.Map<CompanyEmail>(input);
                    entity.IsActive = true;
                }
                entity.CompanyId = companyId;
                entity.Id = _repositoryCompanyEmail.InsertOrUpdateAndGetId(entity);
                return entity;
            }
            else
            {
                throw new UserFriendlyException(L("ErrorFormatEmail"));
            }
        }

        private CompanyPhone CreateOrUpdateCompanyPhone(long companyId, CompanyPhoneInputDto input)
        {
            CompanyPhone entity = new CompanyPhone();
            {
                var entityPhone = _repositoryCompanyPhone.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityPhone);
            }
            entity.CompanyId = companyId;
            entity.Id = _repositoryCompanyPhone.InsertOrUpdateAndGetId(entity);
            return entity;
        }

        private CompanyTypeMapOutputDto CreateOrUpdateCompanyType(CompanyTypeMapInputDto input)
        {
            CompanyTypeMap entity = new CompanyTypeMap();
            {
                var entityType = _repositoryCompanyTypeMap.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityType);
            }
            entity.CompanyId = input.CompanyId;
            entity.Id = _repositoryCompanyTypeMap.InsertOrUpdateAndGetId(entity);
            var companyType = _repositoryCompanyTypeMap.GetAll().Where(x => x.Id == entity.Id)
                .Include(x => x.CompanyType).SingleOrDefault();
            var result = ObjectMapper.Map<CompanyTypeMapOutputDto>(companyType);
            return result;
        }

        private CompanyUser CreateOrUpdateCompanyUser(long companyId, CompanyUserInputDto input)
        {
            CompanyUser entity = new CompanyUser();
            {
                var entityUser = _repositoryCompanyUser.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityUser);
            }
            entity.CompanyId = companyId;
            entity.Id = _repositoryCompanyUser.InsertOrUpdateAndGetId(entity);
            return entity;
        }
        //  [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        private CompanyOrganizationUnit CreateOrUpdateCompanyOrganizationUnit(long companyId, CompanyOrganizationUnitInputDto input)
        {
            CompanyOrganizationUnit entity = new CompanyOrganizationUnit();
            if (input.Id > 0)
            {
                var entityOrganizationUnit = _repositoryCompanyOrganizationUnit.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityOrganizationUnit);
            }
            else
            {
                entity = ObjectMapper.Map<CompanyOrganizationUnit>(input);
                entity.IsActive = true;
            }
            entity.CompanyId = companyId;
            entity.Id = _repositoryCompanyOrganizationUnit.InsertOrUpdateAndGetId(entity);
            return entity;
        }

        //   [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        private CompanyAddress CreateOrUpdateCompanyAddress(long companyId, CompanyAddressInputDto input)
        {
            CompanyAddress entity = new CompanyAddress();
            if (input.Id > 0)
            {
                var entityAddress = _repositoryCompanyAddress.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityAddress);
            }
            else
            {
                entity = ObjectMapper.Map<CompanyAddress>(input);
                entity.IsActive = true;
            }
            entity.CompanyId = companyId;
            entity.Id = _repositoryCompanyAddress.InsertOrUpdateAndGetId(entity);
            return entity;
        }

        [UnitOfWork]
        private CompanyContact CreateOrUpdateCompanyContact(long companyId, CompanyContactInputDto input)
        {
            CompanyContact entity = new CompanyContact();
            if (input.Id > 0)
            {
                var entityContact = _repositoryCompanyContact.GetAll().Where(x => x.Id == input.Id).SingleOrDefault();
                entity = ObjectMapper.Map(input, entityContact);
            }
            else
            {
                entity = ObjectMapper.Map<CompanyContact>(input);
                entity.IsActive = true;
            }
            entity.CompanyId = companyId;
            entity.Id = _repositoryCompanyContact.InsertOrUpdateAndGetId(entity);
            return entity;
        }

        private async Task<CompanyOutputDto> GetDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<CompanyFilterDto>("[SpFilterCompany]",
                    new
                    {
                        @Id = id,
                        @UserId = AbpSession.UserId.Value
                    }, commandType: CommandType.StoredProcedure);
                var result = ObjectMapper.Map<CompanyOutputDto>(output.SingleOrDefault());
                return result;
            }
        }
        #endregion
    }
}
