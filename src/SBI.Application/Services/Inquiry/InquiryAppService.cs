﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using CRM.Application.Shared.Inquiries;
using CRM.Application.Shared.InquiryAddress;
using CRM.Application.Shared.InquiryFacilityMaps;
using CRM.Application.Shared.InquiryUsers;
using CRM.Application.Shared.InquiryViewMaps;
using CRM.Entities.Facing;
using CRM.Entities.View;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using Dapper;
using Microsoft.Extensions.Options;

namespace CRM.Services.Inquiry
{
    public class InquiryAppService : SBIAppServiceBase, IInquiryAppService
    {
        private readonly IRepository<Entities.Inquiries.Inquiry, long> _repoInquiry;
        private readonly IRepository<Entities.Facilities.Facility, int> _repoFacility;
        private readonly IRepository<View, int> _repoView;
        private readonly IRepository<Entities.InquiryCategory.InquiryCategory, int> _repoInquiryCategory;
        private readonly IRepository<Entities.ListingCategory.ListingCategory, int> _repoListingCategory;
        private readonly IRepository<Entities.InquiryAddress.InquiryAddress, long> _repoInquiryAddress;
        private readonly IRepository<Entities.InquiryFacilityMaps.InquiryFacilityMaps, long> _repoInquiryFacilityMaps;
        private readonly IRepository<Entities.InquiryViewMaps.InquiryViewMaps, long> _repoInquiryViewMaps;
        private readonly IRepository<Entities.InquiryUsers.InquiryUsers, long> _repoInquiryUsers;
        private readonly ConfigSetting _options;
        public InquiryAppService(
            IRepository<Entities.Inquiries.Inquiry, long> repoInquiry
            , IRepository<Entities.InquiryAddress.InquiryAddress, long> repoInquiryAddress
            , IRepository<Entities.InquiryFacilityMaps.InquiryFacilityMaps, long> repoInquiryFacilityMaps
            , IRepository<Entities.InquiryViewMaps.InquiryViewMaps, long> repoInquiryViewMaps
            , IRepository<Entities.InquiryUsers.InquiryUsers, long> repoInquiryUsers
            , IRepository<Entities.Facilities.Facility, int> repoFacility
            , IRepository<View, int> repoView
            , IRepository<Entities.InquiryCategory.InquiryCategory, int> repoInquiryCategory
            , IRepository<Entities.ListingCategory.ListingCategory, int> repoListingCategory
            , IOptions<ConfigSetting> options
            )
        {
            _repoFacility = repoFacility;
            _repoView = repoView;
            _repoInquiryCategory = repoInquiryCategory;
            _repoListingCategory = repoListingCategory;
            _repoInquiryAddress = repoInquiryAddress;
            _repoInquiry = repoInquiry;
            _repoInquiryFacilityMaps = repoInquiryFacilityMaps;
            _repoInquiryViewMaps = repoInquiryViewMaps;
            _repoInquiryUsers = repoInquiryUsers;
            _options = options.Value;
        }
        public async Task<InquiryOutputDto> CreateOrUpdateAsync(InquiryInputDto input)
        {
            await ValidationCreateUpdateInquiry (input);
            Entities.Inquiries.Inquiry entity;
            if (input.Id > 0)
            {
                if (input.Id < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("InquiryIdNotFound"));
                entity = await _repoInquiry.GetAsync(input.Id);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoInquiry.UpdateAsync(entityUpdate);
            }
            else
            {
                entity = ObjectMapper.Map<Entities.Inquiries.Inquiry>(input);
                entity.IsActive = true;
                entity.Id = await _repoInquiry.InsertAndGetIdAsync(entity);
            }
            await CurrentUnitOfWork.SaveChangesAsync();
            if (input.FacitityIds != null)
            {
                await UpdateChilds(
                    _repoInquiryFacilityMaps,
                    x => x.InquiryId = entity.Id,
                    x => x.InquiryId == entity.Id,
                    (x, y) => x.FacilityId == y.FacilityId,
                    input.FacitityIds.Select(x => new InquiryFacilityMapInputDto()
                    { FacilityId = x, InquiryId = entity.Id, IsActive = true }).ToArray()
                );
            }
            if (input.ViewIds != null)
            {
                await UpdateChilds(
                    _repoInquiryViewMaps,
                    x => x.InquiryId = entity.Id,
                    x => x.InquiryId == entity.Id,
                    (x, y) => x.ViewId == y.ViewId,
                    input.ViewIds.Select(x => new InquiryViewMapInputDto()
                    { ViewId = x, InquiryId = entity.Id, IsActive = true }).ToArray()
                );
            }
            if (input.UserIds != null)
            {
                await UpdateChilds(
                    _repoInquiryUsers,
                    x => x.InquiryId = entity.Id,
                    x => x.InquiryId == entity.Id,
                    (x, y) => x.UserId == y.UserId,
                    input.UserIds.Select(x => new InquiryUserInputDto()
                    { UserId = x, InquiryId = entity.Id, IsActive = true }).ToArray()
                );
            }
            if (input.InquiryAddress != null)
            {
                await UpdateChilds(
                    _repoInquiryAddress,
                    x => x.InquiryId = entity.Id,
                    x => x.InquiryId == entity.Id,
                    (x, y) => x.Id == y.Id,
                    input.InquiryAddress.ToArray()
                    );
            }
            return ObjectMapper.Map<InquiryOutputDto>(entity);

        }
        public async Task<PagedResultDto<InquiryOutputDto>> GetListInquiries(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<InquiryFilterDto>("SpFilterInquiry",
                    new
                    {
                        @FilterJson = input,
                        @UserId = AbpSession.UserId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<InquiryOutputDto>>(output);
                return new PagedResultDto<InquiryOutputDto>(totalCount, results);
            }
        }

        public async Task<PagedResultDto<InquiryOutputDto>> GetListInquiryByClient(long clientId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<InquiryFilterDto>("SpFilterInquiry",
                    new
                    {
                        @ClientId = clientId,
                        @UserId = AbpSession.UserId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<InquiryOutputDto>>(output);
                return new PagedResultDto<InquiryOutputDto>(totalCount, results);
            }
        }

        public async Task<InquiryOutputDto> GetInquiryDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<InquiryFilterDto>("SpFilterInquiry",
                    new
                    {
                        @Id = id
                    }, commandType: CommandType.StoredProcedure);
                return ObjectMapper.Map<InquiryOutputDto>(output.SingleOrDefault()); ;
            }
        }

        public async Task<List<InquiryOutputDto>> MatchingInquiry(long listingId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<InquiryFilterDto>("SpFilterMatchingInquiry",
                   new
                   {
                       @ListingId = listingId
                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<InquiryOutputDto>>(output);
                return results;
            }
        }

        #region Private API

        public async Task ValidationCreateUpdateInquiry(InquiryInputDto input)
        {
            if (await _repoFacility.CountAsync(x => input.FacitityIds.Contains(x.Id) && x.IsActive) < input.FacitityIds.Count)
                throw new UserFriendlyException(L("FacilityIdNotFound"));
            if (await _repoView.CountAsync(x => input.ViewIds.Contains(x.Id) && x.IsActive) < input.ViewIds.Count)
                throw new UserFriendlyException(L("ViewNotFound"));
            if (await _repoListingCategory.CountAsync(x => x.Id == input.TypeId)<1)
                throw new UserFriendlyException(L("TypeIdNotFound"));
            if (await _repoInquiryCategory.CountAsync(x => x.Id == input.SourceId) < 1)
                throw new UserFriendlyException(L("SourceIdNotFound"));
            if (await _repoInquiryCategory.CountAsync(x => x.Id == input.StatusId) < 1)
                throw new UserFriendlyException(L("StatusIdNotFound"));
        }

        #endregion
    }
}
