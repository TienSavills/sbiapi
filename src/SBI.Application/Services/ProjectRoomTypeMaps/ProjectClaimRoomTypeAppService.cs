﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using CRM.Application.Shared.ProjectClaimRoomTypeMaps;
using CRM.Entities.ProjectClaimRoomTypeMaps;
using CRM.Entities.RoomTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace CRM.Services.ProjectRoomTypeMaps
{
    public class ProjectClaimRoomTypeAppService:SBIAppServiceBase
    {
        private readonly IRepository<ProjectClaimRoomTypeMap, long> _repositoryProjectClaimRoom;
        private readonly IRepository<RoomType, int> _repositoryRoomType;
        public ProjectClaimRoomTypeAppService(IRepository<ProjectClaimRoomTypeMap, long> repositoryProjectClaimRoom
            , IRepository<RoomType, int> repositoryRoomType)
        {
            _repositoryProjectClaimRoom = repositoryProjectClaimRoom;
            _repositoryRoomType = repositoryRoomType;
        }
       // [UnitOfWork (isolationLevel:System.Transactions.IsolationLevel.RepeatableRead)]
        public List<ProjectClaimRoomTypeMap> CreateOrUpdateListProjectClaimRoomType(long projectClaimId, List<ProjectClaimRoomTypeMapInputDto> projectClaimRoomTypes)
        {
            var currentProjectClaimRoomTypes = _repositoryProjectClaimRoom.GetAll().Where(x => x.ProjectClaimId == projectClaimId).ToList();
            var deleteEntities = currentProjectClaimRoomTypes.Where(x => projectClaimRoomTypes.All(y => y.RoomTypeId != x.RoomTypeId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryProjectClaimRoom.Update(x);
            });
            //List<ProjectClaimRoomTypeMap> listResult = new List<ProjectClaimRoomTypeMap>();
            projectClaimRoomTypes.ForEach(x =>
            {
                var objUpdate = currentProjectClaimRoomTypes.FirstOrDefault(obj => obj.RoomTypeId == x.RoomTypeId);
                if (objUpdate != null)
                {

                    objUpdate.IsActive = true;
                    objUpdate.NumberOfRoom = x.NumberOfRoom;
                    _repositoryProjectClaimRoom.Update(objUpdate);
                    return;
                }
                var entity = new ProjectClaimRoomTypeMap()
                {
                    ProjectClaimId = projectClaimId,
                    RoomTypeId = x.RoomTypeId,
                    NumberOfRoom = x.NumberOfRoom,
                    //RoomType = _repositoryRoomType.GetAll().Where(y => y.Id == x.RoomTypeId).SingleOrDefault(),
                    IsActive = true
                };
                //listResult.Add(entity);
                _repositoryProjectClaimRoom.Insert(entity);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repositoryProjectClaimRoom.GetAll().Where(x => x.ProjectClaimId == projectClaimId && x.IsActive).Include(x => x.RoomType).ToList();
            //return listResult;
        }

        public void ValidateProjectClaimRoomType(List<int> projectClaimRoomTypes)
        {
            if (_repositoryRoomType.GetAll().Count(x => projectClaimRoomTypes.Contains(x.Id) && x.IsActive) < projectClaimRoomTypes.Count)
                throw new UserFriendlyException(L("ExistRoomType"));
        }
    }
}
