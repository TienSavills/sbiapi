﻿using System.Collections.Generic;
using System.Linq;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using CRM.Application.Shared.Facilities;
using CRM.Entities.PropertyFacilityMaps;
using CRM.Entities.Facilities;
using CRM.Entities.ProjectFacilityMaps;
using Microsoft.EntityFrameworkCore;
using System.Transactions;

namespace CRM.Services.Facilities
{
    public class FacilityAppService : SBIAppServiceBase, IFacilityAppService
    {
        private readonly IRepository<Facility, int> _repositoryFacility;
        private readonly IRepository<ProjectFacilityMap, long> _repoProjectFacilityMap;
        private readonly IRepository<PropertyFacilityMap, long> _repositoryPropertyFacilityMap;
        public FacilityAppService(IRepository<Facility, int> repoFacility, IRepository<ProjectFacilityMap, long> repoProjectFacilityMap, IRepository<PropertyFacilityMap, long> repositoryPropertyFacilityMap)
        {
            _repositoryFacility = repoFacility;
            _repoProjectFacilityMap = repoProjectFacilityMap;
            _repositoryPropertyFacilityMap = repositoryPropertyFacilityMap;
        }

        public List<FacilityOutputDto> GetListFacility(int? id)
        {
            var data = _repositoryFacility.GetAll();
            return ObjectMapper.Map<List<FacilityOutputDto>>(data);
        }
        public void ValidateProjectFacilities(List<int> projectFacilities)
        {
            if (_repositoryFacility.GetAll().Count(x => projectFacilities.Contains(x.Id) && x.IsActive) < projectFacilities.Count)
                throw new UserFriendlyException(L("ProjectFacilityIdNotFound"));
        }
        public List<ProjectFacilityMap> CreateOrUpdateListProjectFacilities(long projectId, List<int> projectFacilities)
        {

            var currentProjectFacilities = _repoProjectFacilityMap.GetAll().Where(x => x.ProjectId == projectId).ToList();
            projectFacilities.ForEach(x =>
            {
                var objUpdate = currentProjectFacilities.FirstOrDefault(obj => obj.FacilityId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoProjectFacilityMap.Update(objUpdate);
                    return;
                }
                var entity = new ProjectFacilityMap
                {
                    ProjectId = projectId,
                    FacilityId = x,
                    IsActive = true
                };
                _repoProjectFacilityMap.Insert(entity);
            });
            var deleteEntities = currentProjectFacilities.Where(x => projectFacilities.All(y => y != x.FacilityId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoProjectFacilityMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repoProjectFacilityMap.GetAll().Where(x => x.ProjectId == projectId && x.IsActive).Include(x => x.Facility).ToList();
        }

        [UnitOfWork(isolationLevel:IsolationLevel.RepeatableRead)]
        public List<PropertyFacilityMap> CreateOrUpdateListPropertyFacilities(long propertyId, List<int> propertyFacilities)
        {
            var currentPropertyFacilities = _repositoryPropertyFacilityMap.GetAll().Where(x => x.PropertyId == propertyId).ToList();
            propertyFacilities.ForEach(x =>
            {
                var objUpdate = currentPropertyFacilities.FirstOrDefault(obj => obj.FacilityId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryPropertyFacilityMap.Update(objUpdate);
                    return;
                }
                var entity = new PropertyFacilityMap
                {
                    PropertyId = propertyId,
                    FacilityId = x,
                    IsActive = true
                };
                _repositoryPropertyFacilityMap.Insert(entity);
            });
            var deleteEntities = currentPropertyFacilities.Where(x => propertyFacilities.All(y => y != x.FacilityId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryPropertyFacilityMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repositoryPropertyFacilityMap.GetAll().Where(x => x.PropertyId == propertyId && x.IsActive).Include(x => x.Facility).ToList();
        }
    }
}
