﻿using Abp.Domain.Repositories;
using Abp.UI;
using CRM.Application.Shared.Floor;
using CRM.Entities.Projects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Services.Floor
{
    public class FloorAppServices : SBIAppServiceBase, IFloorAppServices
    {
        private readonly IRepository<Entities.Floor.Floor, long> _repoFloor;
        private readonly IRepository<Project, long> _repoProject;
        public FloorAppServices(IRepository<Entities.Floor.Floor, long> repoFloor
            , IRepository<Project, long> repoProject
            )
        {
            _repoFloor = repoFloor;
            _repoProject = repoProject;
        }
        public async Task<FloorOutputDto> CreateOrUpdateAsync(long? id, FloorInputDto input)
        {
            if (!_repoProject.GetAll().Where(x => x.Id == input.ProjectId).Any())
                throw new UserFriendlyException(L("ProjectIdNotFound"));

            if (id.HasValue)
            {
                if (id < AppConsts.MinValuePrimaryKey)
                    throw new UserFriendlyException(L("FloorIdNotFound"));
                var entity = _repoFloor.Get(id.Value);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoFloor.UpdateAsync(entityUpdate);
                return ObjectMapper.Map<FloorOutputDto>(entity);
            }
            else
            {
                var entity = ObjectMapper.Map<Entities.Floor.Floor>(input);
                entity.IsActive = true;
                entity.Id = await _repoFloor.InsertAndGetIdAsync(entity);
                return ObjectMapper.Map<FloorOutputDto>(entity);
            }
        }

        public async Task<FloorOutputDto> GetFloorDetails(long id)
        {
            var data = await _repoFloor.GetAll().Where(x => x.Id == id)
              .Include(x => x.Project).SingleOrDefaultAsync();
            return ObjectMapper.Map<FloorOutputDto>(data);
        }

        public async Task<List<FloorOutputDto>> GetListFloors(long projectId)
        {
            var data = await _repoFloor.GetAll().Where(x => x.ProjectId == projectId)
                .Include(x => x.Project).ToListAsync();
            return ObjectMapper.Map<List<FloorOutputDto>>(data);
        }

        public async Task<FloorOutputDto> OrderFloor(long id, int num)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("FloorIdNotFound"));
            var entity = _repoFloor.Get(id);
            entity.Order = num;
            await _repoFloor.UpdateAsync(entity);
            return ObjectMapper.Map<FloorOutputDto>(entity);
        }
        public async Task<List<FloorOutputDto>> OrderListFloor(List<long> ids)
        {
            int i = 0;
            List<Entities.Floor.Floor> listEnities = new List<Entities.Floor.Floor>();
            ids.ForEach(x => {
                var entity = _repoFloor.Get(x);
                entity.Order = i;
                i++;
                _repoFloor.UpdateAsync(entity);
                listEnities.Add(entity);
            });
            return ObjectMapper.Map<List<FloorOutputDto>>(listEnities);
        }
    }
}
