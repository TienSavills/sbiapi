﻿using System.Collections.Generic;
using System.Linq;
using Abp.Domain.Repositories;
using Abp.Collections.Extensions;
using Abp.Domain.Uow;
using Abp.UI;
using CRM.Entities.ProjectTypeMaps;
using CRM.Entities.PropertyTypeMaps;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using CRM.Application.Shared.CatTypes;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using CRM.Entities.PropertyValidation;
using CRM.Application.Shared.PropertyTypes;
using CRM.Entities.PropertyTypes;

namespace CRM.Services.ProjectTypes
{
    public class ProjectTypeAppService : SBIAppServiceBase, IProjectTypeAppService
    {
        private readonly IRepository<PropertyType, int> _repoPropertyType;
        private readonly IRepository<PropertyValidation, int> _repoPropertyValidation;
        private readonly IRepository<ProjectTypeMap, long> _repoProjectTypeMap;
        private readonly IRepository<PropertyTypeMap, long> _repositoryPropertyTypeMap;


        public ProjectTypeAppService(IRepository<PropertyType, int> repoPropertyType, IRepository<ProjectTypeMap, long> repoProjectTypeMap, IRepository<PropertyTypeMap, long> repositoryPropertyTypeMap
            , IRepository<PropertyValidation, int> repoPropertyValidation)
        {
            _repoPropertyValidation = repoPropertyValidation;
            _repoPropertyType = repoPropertyType;
            _repoProjectTypeMap = repoProjectTypeMap;
            _repositoryPropertyTypeMap = repositoryPropertyTypeMap;
        }
        public async Task<PagedResultDto<PropertyTypeOutputDto>> GetListPropertyType(FilterBasicInputDto input)
        {

            var query = _repoPropertyType.GetAll().Include(x=>x.ProjectTypeMap)
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(input.Keyword),
                    x => x.TypeName.ToUpper().Contains(input.Keyword.ToUpper()));
            var totalCount = await query.CountAsync();
            var results = await query
                .Skip((input.PageNumber-1) * input.PageSize)
                .Take(input.PageSize)
                .OrderBy(x => x.TypeName)
                .ToListAsync();
            var output = ObjectMapper.Map<List<PropertyTypeOutputDto>>(results);
            foreach(var item in output)
            {
                var propertyValidation =await _repoPropertyValidation.SingleAsync(x => x.PropertyTypeId == item.Id);
                if (propertyValidation != null)
                {
                    item.RequiredBuilding = propertyValidation.RequiredBuilding;
                    item.RequiredFloor = propertyValidation.RequiredFloor;
                    item.RequiredUnit = propertyValidation.RequiredUnit;
                }
            }
            return new PagedResultDto<PropertyTypeOutputDto>(
                totalCount,
                output
            );

        }
        public async Task<List<PropertyTypeOutputDto>> GetListPropertyType(string keyword)
        {

            var query = _repoPropertyType.GetAll()
                .Where(x => x.IsActive == true)
                .WhereIf(!string.IsNullOrEmpty(keyword),
                    x => x.TypeName.ToUpper().Contains(keyword.ToUpper()));

            var results = await query.ToListAsync();
          
            var output= ObjectMapper.Map<List<PropertyTypeOutputDto>>(results);
            foreach (var item in output)
            {
                var propertyValidation = await _repoPropertyValidation.GetAll().Where(x => x.PropertyTypeId == item.Id).SingleOrDefaultAsync();
                if (propertyValidation != null)
                {
                    item.RequiredBuilding = propertyValidation.RequiredBuilding;
                    item.RequiredFloor = propertyValidation.RequiredFloor;
                    item.RequiredUnit = propertyValidation.RequiredUnit;
                }
            }
            return output;
        }
        public void ValidatePropertyTypes(List<int> propertyTypes)
        {
            if (_repoPropertyType.GetAll().Count(x => propertyTypes.Contains(x.Id) && x.IsActive) < propertyTypes.Count)
                throw new UserFriendlyException(L("ProjectTypeIdNotFound"));
        }
       // [UnitOfWork(isolationLevel: System.Transactions.IsolationLevel.RepeatableRead)]
        public List<ProjectTypeMap> CreateOrUpdateListProjectTypes(long projectId, List<int> projectTypes)
        {
            var currentProjectTypes = _repoProjectTypeMap.GetAll().Where(x => x.ProjectId == projectId).ToList();
            projectTypes.ForEach(x =>
            {
                var objUpdate = currentProjectTypes.FirstOrDefault(obj => obj.PropertyTypeId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoProjectTypeMap.Update(objUpdate);
                    return;
                }
                var entity = new ProjectTypeMap
                {
                    ProjectId = projectId,
                    PropertyTypeId = x,
                    IsActive = true
                };
                _repoProjectTypeMap.Insert(entity);
            });
            var deleteEntities = currentProjectTypes.Where(x => projectTypes.All(y => y != x.PropertyTypeId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoProjectTypeMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repoProjectTypeMap.GetAll().Where(x => x.ProjectId == projectId && x.IsActive).Include(x=>x.PropertyType).ToList();
        }
      //  [UnitOfWork(isolationLevel: System.Transactions.IsolationLevel.RepeatableRead)]
        public List<PropertyTypeMap> CreateOrUpdateListPropertyTypes(long propertyId, List<int> propertyTypes)
        {
            var currentPropertyTypes = _repositoryPropertyTypeMap.GetAll().Where(x => x.PropertyId == propertyId).ToList();
            propertyTypes.ForEach(x =>
            {
                var objUpdate = currentPropertyTypes.FirstOrDefault(obj => obj.PropertyTypeId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryPropertyTypeMap.Update(objUpdate);
                    return;
                }
                var entity = new PropertyTypeMap
                {
                    PropertyId = propertyId,
                    PropertyTypeId = x,
                    IsActive = true
                };
                _repositoryPropertyTypeMap.Insert(entity);
            });
            var deleteEntities = currentPropertyTypes.Where(x => propertyTypes.All(y => y != x.PropertyTypeId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryPropertyTypeMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repositoryPropertyTypeMap.GetAll().Where(x => x.PropertyId == propertyId && x.IsActive).Include(x => x.PropertyType).ToList();
        }
    }
}
