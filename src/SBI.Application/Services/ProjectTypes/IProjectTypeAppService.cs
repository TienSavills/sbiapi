﻿using Abp.Application.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using CRM.Application.Shared.CatTypes;
using CRM.Application.Shared.PropertyTypes;

namespace CRM.Services.ProjectTypes
{
    public interface IProjectTypeAppService : IApplicationService
    {
        Task<PagedResultDto<PropertyTypeOutputDto>> GetListPropertyType(FilterBasicInputDto input);
        Task<List<PropertyTypeOutputDto>> GetListPropertyType(string keyword);
    }
}
