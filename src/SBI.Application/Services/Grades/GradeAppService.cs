﻿using System.Collections.Generic;
using System.Linq;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using CRM.Entities.PropertyGradeMaps;
using CRM.Entities.ProjectGradeMaps;
using Microsoft.EntityFrameworkCore;
using CRM.Entities.Grades;
using CRM.Application.Shared.Grades;
using Abp.Collections.Extensions;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using System.Transactions;

namespace CRM.Services.Grades
{
    public class GradeAppService : SBIAppServiceBase, IGradeAppService
    {
        private readonly IRepository<Grade, int> _repositoryGrade;
        private readonly IRepository<ProjectGradeMap, long> _repoProjectTypeMap;
        private readonly IRepository<PropertyGradeMap, long> _repositoryPropertyGradeMap;
        public GradeAppService(IRepository<Grade, int> repoGrade, IRepository<ProjectGradeMap, long> repoProjectTypeMap, IRepository<PropertyGradeMap, long> repositoryPropertyGradeMap
          )
        {
            _repositoryGrade = repoGrade;
            _repoProjectTypeMap = repoProjectTypeMap;
            _repositoryPropertyGradeMap = repositoryPropertyGradeMap;
        }

        public List<GradeOutputDto> GetListGrade(int? id)
        {
            var data = _repositoryGrade.GetAll();
            return ObjectMapper.Map<List<GradeOutputDto>>(data);
        }
        public void ValidateProjectGrades(List<int> projectGrades)
        {
            if (_repositoryGrade.GetAll().Count(x => projectGrades.Contains(x.Id) && x.IsActive) < projectGrades.Count)
                throw new UserFriendlyException(L("ProjectGradeIdNotFound"));
        }
        [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        public List<ProjectGradeMap> CreateOrUpdateListProjectGrades(long projectId, List<int> projectGrades)
        {

            var currentProjectGrades = _repoProjectTypeMap.GetAll().Where(x => x.ProjectId == projectId).ToList();
            projectGrades.ForEach(x =>
            {
                var objUpdate = currentProjectGrades.FirstOrDefault(obj => obj.GradeId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repoProjectTypeMap.Update(objUpdate);
                    return;
                }
                var entity = new ProjectGradeMap
                {
                    ProjectId = projectId,
                    GradeId = x,
                    IsActive = true
                };
                _repoProjectTypeMap.Insert(entity);
            });
            var deleteEntities = currentProjectGrades.Where(x => projectGrades.All(y => y != x.GradeId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoProjectTypeMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repoProjectTypeMap.GetAll().Where(x => x.ProjectId == projectId && x.IsActive).Include(x => x.Grade).ToList();
        }

        [UnitOfWork(isolationLevel: IsolationLevel.RepeatableRead)]
        public List<PropertyGradeMap> CreateOrUpdateListPropertyGrades(long propertyId, List<int> propertyGrades)
        {
            var currentPropertyGrades = _repositoryPropertyGradeMap.GetAll().Where(x => x.PropertyId == propertyId).ToList();
            propertyGrades.ForEach(x =>
            {
                var objUpdate = currentPropertyGrades.FirstOrDefault(obj => obj.GradeId == x);
                if (objUpdate != null)
                {
                    if (objUpdate.IsActive) return;
                    objUpdate.IsActive = true;
                    _repositoryPropertyGradeMap.Update(objUpdate);
                    return;
                }
                var entity = new PropertyGradeMap
                {
                    PropertyId = propertyId,
                    GradeId = x,
                    IsActive = true
                };
                _repositoryPropertyGradeMap.Insert(entity);
            });
            var deleteEntities = currentPropertyGrades.Where(x => propertyGrades.All(y => y != x.GradeId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repositoryPropertyGradeMap.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return _repositoryPropertyGradeMap.GetAll().Where(x => x.PropertyId == propertyId && x.IsActive).Include(x => x.Grade).ToList();
        }

        public async Task< List<GradeOutputDto>> GetListGrade(string keyword)
        {
            var query = _repositoryGrade.GetAll()
            .Where(x => x.IsActive == true)
              .WhereIf(!string.IsNullOrEmpty(keyword),
                  x => x.GradeName.ToUpper().Contains(keyword.ToUpper()));

            var results = await query.ToListAsync();
            return ObjectMapper.Map<List<GradeOutputDto>>(results);
        }
    }
}
