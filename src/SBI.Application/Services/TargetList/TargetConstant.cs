﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Services.TargetList
{
    public static class TargetConstant
    {
        public const string Office = "Office";
        public const string Retail = "Retail";
        public const string Leasing = "Leasing";
    }
}
