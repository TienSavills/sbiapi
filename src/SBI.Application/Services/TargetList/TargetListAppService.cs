﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using CRM.Application.Shared.TargetList;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using System.Data;
using System.Data.SqlClient;
using CRM.SBIMap;
using CRM.EntitiesCustom;
using CRM.Entities.TargetContact;
using CRM.Entities.TargetRequest;
using CRM.Application.Shared.TargetContact;
using CRM.Application.Shared.TargetRequest;
using Abp.UI;

namespace CRM.Services.TargetList
{
    public class TargetListAppService : SBIAppServiceBase, ITargetListAppService
    {
        private readonly IRepository<Entities.TargetList.TargetList, long> _repoTargetList;
        private readonly IRepository<Entities.TargetContact.TargetContact, long> _repoTargetContact;
        private readonly IRepository<Entities.TargetRequest.TargetRequest, long> _repoTargetRequest;
        private readonly IRepository<Entities.ContactEmail.ContactEmail, long> _repoContactEmail;
        private ConfigSetting _options;
        public TargetListAppService(IRepository<Entities.TargetList.TargetList, long> repoTargetList
            , IRepository<Entities.TargetContact.TargetContact, long> repoTargetContact
            , IRepository<Entities.TargetRequest.TargetRequest, long> repoTargetRequest
            , IRepository<Entities.ContactEmail.ContactEmail, long> repoContactEmail
            , IOptions<ConfigSetting> options)
        {
            _repoTargetList = repoTargetList;
            _repoTargetContact = repoTargetContact;
            _repoTargetRequest = repoTargetRequest;
            _repoContactEmail = repoContactEmail;
            _options = options.Value;
        }

        public async Task<TargetOutputDto> CreateOrUpdateAsync(TargetInputDto input)
        {
            return await CreateOrUpdateTargetListAsync(input);
        }

        public async Task<PagedResultDto<TargetOutputDto>> GetListTargets(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TargetFilterDto>("SpFilterTarget",
                   new
                   {
                       @FilterJson = input
                       ,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<TargetOutputDto>>(output);
                return new PagedResultDto<TargetOutputDto>(totalCount, results);
            }
        }
        public async Task<PagedResultDto<TargetContactOutputDto>> GetListTargetContacts(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TargetContactFilterDto>("SpFilterTargetContact",
                   new
                   {
                       @FilterJson = input,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<TargetContactOutputDto>>(output);
                return new PagedResultDto<TargetContactOutputDto>(totalCount, results);
            }
        }
        public async Task<PagedResultDto<TargetRequestOutputDto>> GetListTargetRequests(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TargetRequestFilterDto>("SpFilterTargetRequest",
                   new
                   {
                       @FilterJson = input,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<TargetRequestOutputDto>>(output);
                return new PagedResultDto<TargetRequestOutputDto>(totalCount, results);
            }
        }

        public async Task<PagedResultDto<TargetContactOutputDto>> GetListCampaignContacts(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TargetContactFilterDto>("SpFilterCampaignContact",
                   new
                   {
                       @FilterJson = input,
                       @UserId = AbpSession.UserId.Value
                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<TargetContactOutputDto>>(output);
                return new PagedResultDto<TargetContactOutputDto>(totalCount, results);
            }
        }

        public async Task<PagedResultDto<TargetRequestOutputDto>> GetListCampaignRequests(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TargetRequestFilterDto>("SpFilterCampaignRequest",
                   new
                   {
                       @FilterJson = input,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<TargetRequestOutputDto>>(output);
                return new PagedResultDto<TargetRequestOutputDto>(totalCount, results);
            }
        }
        public async Task<TargetOutputDto> GetTargetDetails(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TargetFilterDto>("SpFilterTarget",
                   new
                   {
                       @Id = id
                       ,
                       @UserId = AbpSession.UserId.Value

                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<TargetOutputDto>(output.SingleOrDefault());
                return results;
            }
        }
        //supcription
        public async Task<List<TargetOutputDto>> GetTargetListByEmail(string email)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<TargetFilterDto>("SpTargetListByEmail",
                   new
                   {
                       @Email = email
                   }, commandType: CommandType.StoredProcedure);
                var results = ObjectMapper.Map<List<TargetOutputDto>>(output);
                return results;
            }
        }

        public async Task<List<TargetContactOutputDto>> CreateOrUpdateTargetByEmail(string email, string targetName)
        {
            long? contactId = _repoContactEmail.GetAll().Where(x => x.Email.ToUpper() == email.ToUpper()).Select(x => x.ContactId).FirstOrDefault();
            if (!contactId.HasValue || contactId == 0) throw new UserFriendlyException(L("ContactIdNotFound"));
            var arrTargetName = targetName.Split(',');
            var deleteTargetStaticId = await _repoTargetList.GetAll().Where(x => !arrTargetName.Contains(x.TargetName) && x.IsStatic == true).Select(y => y.Id).ToListAsync();
            List<TargetContact> targetContacts = new List<TargetContact>();
            foreach (var target in arrTargetName)
            {
                long? targetId = await _repoTargetList.GetAll().Where(x => x.TargetName.ToUpper() == target.ToUpper()).Select(x => x.Id).SingleOrDefaultAsync();
                if (!targetId.HasValue || !contactId.HasValue) throw new UserFriendlyException(L("ContactIdOrTargetIdNotFound"));
                var targetContact = _repoTargetContact.GetAll().Where(x => x.ContactId == contactId && x.TargetId == targetId).FirstOrDefault();
                if (targetContact != null)
                {
                    targetContact.IsActive = true;
                    _repoTargetContact.Update(targetContact);
                    targetContacts.Add(targetContact);
                }
                else
                {
                    var entity = new TargetContact
                    {
                        ContactId = contactId.Value,
                        TargetId = targetId.Value,
                        IsActive = true,
                    };
                    _repoTargetContact.Insert(entity);
                    targetContacts.Add(entity);
                }
            }
            var deleteEntities = _repoTargetContact.GetAll().Where(x => x.ContactId == contactId && deleteTargetStaticId.Contains(x.TargetId)).ToList();
            deleteEntities.ForEach(x =>
            {
                x.IsActive = false;
                _repoTargetContact.Update(x);
            });
            CurrentUnitOfWork.SaveChanges();
            return ObjectMapper.Map<List<TargetContactOutputDto>>(targetContacts); ;
        }
        private async Task<TargetOutputDto> CreateOrUpdateTargetListAsync(TargetInputDto input)
        {
            var output = new TargetOutputDto();
            if (input.Id.HasValue)
            {
                var entity = _repoTargetList.Get(input.Id.Value);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoTargetList.UpdateAsync(entityUpdate);
                CreateOrUpdateTargetContact(input.Id.Value, input.contactIds);
                CreateOrUpdateTargetRequest(input.Id.Value, input.requestIds);
                output = ObjectMapper.Map<TargetOutputDto>(entity);
            }
            else
            {
                var entity = ObjectMapper.Map<Entities.TargetList.TargetList>(input);
                entity.IsActive = true;
                entity.Id = await _repoTargetList.InsertAndGetIdAsync(entity);
                CreateOrUpdateTargetContact(entity.Id, input.contactIds);
                CreateOrUpdateTargetRequest(entity.Id, input.requestIds);
                output = ObjectMapper.Map<TargetOutputDto>(entity);
            }
            return output;
        }
        private void CreateOrUpdateTargetContact(long targetId, List<long?> contactIds)
        {
            if (contactIds != null)
            {
                var currentTargetContact = _repoTargetContact.GetAll().Where(x => x.TargetId == targetId).ToList();
                contactIds.ForEach(x =>
                {
                    var objUpdate = currentTargetContact.FirstOrDefault(obj => obj.ContactId == x);
                    if (objUpdate != null)
                    {
                        if (objUpdate.IsActive) return;
                        objUpdate.IsActive = true;
                        _repoTargetContact.Update(objUpdate);
                        return;
                    }
                    var entity = new TargetContact
                    {
                        ContactId = x.Value,
                        TargetId = targetId,
                        IsActive = true,
                    };
                    _repoTargetContact.Insert(entity);
                });
                var deleteEntities = currentTargetContact.Where(x => contactIds.All(y => y != x.ContactId)).ToList();
                deleteEntities.ForEach(x =>
                {
                    x.IsActive = false;
                    _repoTargetContact.Update(x);
                });
                CurrentUnitOfWork.SaveChanges();
            }
        }
        private void CreateOrUpdateTargetRequest(long targetId, List<long?> requestIds)
        {
            if (requestIds != null)
            {
                var currentTargetRequest = _repoTargetRequest.GetAll().Where(x => x.TargetId == targetId).ToList();
                requestIds.ForEach(x =>
                {
                    var objUpdate = currentTargetRequest.FirstOrDefault(obj => obj.RequestId == x);
                    if (objUpdate != null)
                    {
                        if (objUpdate.IsActive) return;
                        objUpdate.IsActive = true;
                        _repoTargetRequest.Update(objUpdate);
                        return;
                    }
                    var entity = new TargetRequest
                    {
                        RequestId = x.Value,
                        TargetId = targetId,
                        IsActive = true,
                    };
                    _repoTargetRequest.Insert(entity);
                });
                var deleteEntities = currentTargetRequest.Where(x => requestIds.All(y => y != x.RequestId)).ToList();
                deleteEntities.ForEach(x =>
                {
                    x.IsActive = false;
                    _repoTargetRequest.Update(x);
                });
                CurrentUnitOfWork.SaveChanges();
            }
        }

    }
}
