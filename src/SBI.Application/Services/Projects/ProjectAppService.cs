﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using CRM.Application.Shared.Project;
using CRM.Application.Shared.Project.Dto;
using CRM.Application.Shared.ProjectAddresss;
using CRM.Application.Shared.ProjectFacilityMaps;
using CRM.Application.Shared.ProjectGradeMaps;
using CRM.Application.Shared.ProjectLandlord;
using CRM.Application.Shared.ProjectTenant;
using CRM.Application.Shared.ProjectTransportationMaps;
using CRM.Application.Shared.ProjectTypeMaps;
using CRM.Authorization;
using CRM.Entities.Countries;
using CRM.Entities.Districs;
using CRM.Entities.Facilities;
using CRM.Entities.ProjectAddresss;
using CRM.Entities.ProjectFacilityMaps;
using CRM.Entities.ProjectGradeMaps;
//using CRM.Entities.ProjectLandlord;
using CRM.Entities.Projects;
//using CRM.Entities.ProjectTenant;
using CRM.Entities.ProjectTypeMaps;
using CRM.Entities.PropertyTypes;
using CRM.Entities.Provinces;
using CRM.EntitiesCustom;
using CRM.SBIMap;
using CRM.Services.Company;
using CRM.Services.Facilities;
using CRM.Services.Grades;
using CRM.Services.ProjectTypes;
using CRM.Services.Transportations;
using Dapper;
using Microsoft.Extensions.Options;

namespace CRM.Services.Projects
{
   
    public class ProjectAppService : SBIAppServiceBase, IProjectAppService
    {
        private readonly IRepository<Project, long> _repoProject;
        private readonly IRepository<ProjectFacilityMap, long> _repoProjectFacilityMap;
        private readonly IRepository<Facility, int> _repositoryFacility;
        private readonly IRepository<ProjectTypeMap, long> _repoProjectTypeMap;
        private readonly IRepository<PropertyType, int> _repositoryPropertyType;
        private readonly IRepository<ProjectAddress, long> _repoProjectAddress;
        //private readonly IRepository<ProjectTenant, long> _repoProjectTenant;
        //private readonly IRepository<ProjectLandlord, long> _repoProjectLandlord;
        private readonly IRepository<Country, int> _repositoryCountry;
        private readonly IRepository<District, int> _repositoryDistrict;
        private readonly IRepository<Province, int> _repositoryProvince;
        private readonly IRepository<Entities.ConstructionStatus.ConstructionStatus, int> _repositoryConstructionStatus;
        private readonly FacilityAppService _facilityAppService;
        private readonly TransportationAppService _transportationAppService;
        private readonly ProjectTypeAppService _projectTypeAppService;
        private readonly CompanyAppService _companyAppService;
        private readonly GradeAppService _gradeAppService;
        private readonly IRepository<ProjectGradeMap, long> _repoProjectGradeMap;


        private readonly ConfigSetting _options;
        public ProjectAppService(IRepository<Project, long> repoProject,
            IRepository<ProjectFacilityMap, long> repoProjectFacilityMap,
            IRepository<ProjectTypeMap, long> repoProjectTypeMap,
            IRepository<ProjectAddress, long> repoProjectAddress,
            //IRepository<ProjectTenant, long> repoProjectTenant,
            //IRepository<ProjectLandlord, long> repoProjectLandlord,
            IOptions<ConfigSetting> options,
            IRepository<Country, int> repositoryCountry,
            IRepository<District, int> repositoryDistrict,
            IRepository<Province, int> repositoryProvince,
            IRepository<Facility, int> repoProjectFacility,
            IRepository<PropertyType, int> repositoryPropertyType,
            FacilityAppService facilityAppService,
            TransportationAppService transportationAppService,
            ProjectTypeAppService projectTypeAppService,
            IRepository<Entities.ConstructionStatus.ConstructionStatus, int> repositoryConstructionStatus,
            CompanyAppService companyAppService,
            GradeAppService gradeAppService,
            IRepository<ProjectGradeMap, long> repoProjectGradeMap)
        {
            _repoProject = repoProject;
            _repoProjectFacilityMap = repoProjectFacilityMap;
            _repoProjectTypeMap = repoProjectTypeMap;
            _repoProjectAddress = repoProjectAddress;
            _repositoryCountry = repositoryCountry;
            _repositoryDistrict = repositoryDistrict;
            _repositoryProvince = repositoryProvince;
            _repositoryFacility = repoProjectFacility;
            _repositoryPropertyType = repositoryPropertyType;
            _facilityAppService = facilityAppService;
            _transportationAppService = transportationAppService;
            _projectTypeAppService = projectTypeAppService;
            _repositoryConstructionStatus = repositoryConstructionStatus;
            _companyAppService = companyAppService;
            //_repoProjectTenant = repoProjectTenant;
            //_repoProjectLandlord = repoProjectLandlord;
            _gradeAppService = gradeAppService;
            _repoProjectGradeMap = repoProjectGradeMap;
            _options = options.Value;
        }

        #region public function <APIs>

        /// <summary>
        /// TO NGOC AN, 09 Oct 2018
        /// Get project detail, included: ProjectAddress, ProjectFacilityMap, ProjectTransportationMap, ProjectTypeMap
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [AbpAllowAnonymous]
        public async Task<ProjectOutputDto> GetProjectDetail(long projectId)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ProjectFilterDto>("SpFilterProject",
                    new
                    {
                        @Id = projectId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ProjectOutputDto>>(output).SingleOrDefault();
                return results;
            }
        }
        [AbpAuthorize(PermissionNames.Pages_Administration_ProjectResidential_Create)]
        public async Task<ProjectOutputDto> CreateOrUpdateAsync(ProjectInputDto input)
        {
            ValidateCreateProject(input);
            var entity = new Project();
            if (input.Id.HasValue && input.Id > 0)
            {
                entity = await _repoProject.GetAsync(input.Id.Value);
                var entityUpdate = ObjectMapper.Map(input, entity);
                await _repoProject.UpdateAsync(entityUpdate);
            }
            else
            {
                var entityInsert = ObjectMapper.Map(input, entity);
                entity.IsActive = true;
                entity.Id = await _repoProject.InsertAndGetIdAsync(entityInsert);
            }
            var output = ObjectMapper.Map<ProjectOutputDto>(entity);

            #region Insert or update project facilities

            if (input.FacilityIds != null)
            {

                _facilityAppService.ValidateProjectFacilities(input.FacilityIds);
                output.ProjectFacilities = ObjectMapper.Map<List<ProjectFacilityMapOutputDto>>(_facilityAppService.CreateOrUpdateListProjectFacilities(entity.Id, input.FacilityIds));
            }

            #endregion
            #region Insert or update project Transportations

            if (input.TransportationIds != null)
            {

                output.ProjectTransportations = ObjectMapper.Map<List<ProjectTransportationMapOutputDto>>(_transportationAppService.CreateOrUpdateListProjectTransportations(entity.Id, input.TransportationIds));
            }

            #endregion

            #region Insert or update project Grade

            if (input.GradeIds != null)
            {
                _gradeAppService.ValidateProjectGrades(input.GradeIds);
                output.ProjectGrades = ObjectMapper.Map<List<ProjectGradeMapOutputDto>>(_gradeAppService.CreateOrUpdateListProjectGrades(entity.Id, input.GradeIds));
            }

            #endregion

            #region Insert or update project types

            if (input.ProjectTypeIds != null)
            {
                _projectTypeAppService.ValidatePropertyTypes(input.ProjectTypeIds);
                output.ProjectTypes = ObjectMapper.Map<List<ProjectTypeMapOutputDto>>(_projectTypeAppService.CreateOrUpdateListProjectTypes(entity.Id, input.ProjectTypeIds));
            }
            #endregion

            return output;
        }
        [AbpAuthorize(PermissionNames.Pages_Administration_ProjectResidential_Create)]
        public async Task<ProjectOutputDto> CreateProjectAsync(ProjectInputDto input)
        {
            ValidateCreateProject(input);
            var entity = ObjectMapper.Map<Project>(input);
            entity.IsActive = true;
            entity.Id = await _repoProject.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<ProjectOutputDto>(entity);
        }

        [AbpAuthorize(PermissionNames.Pages_Administration_ProjectResidential_Update)]
        public async Task<ProjectOutputDto> UpdateProjectAsync(long projectId, ProjectInputDto input)
        {
            if (projectId < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("ProjectIdNotFound"));
            var entity = await _repoProject.GetAsync(projectId);

            ValidateCreateProject(input);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoProject.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<ProjectOutputDto>(entity);
        }

        public async Task<ProjectOutputDto> UpdateProjectDetailAsync(long projectId,
            List<ProjectAddressInputDto> projectAddress,
            ProjectFacilityMapInputDto projectFacilities,
            ProjectTypeMapInputDto projectTypes,
            //List<ProjectTenantInputDto> projectTenants,
            //ProjectLandlordInputDto projectLandlords,
            ProjectGradeMapInputDto projectGrades)
        {
            var entity = await _repoProject.GetAsync(projectId);
            var output = ObjectMapper.Map<ProjectOutputDto>(entity);
            if (projectAddress != null && projectAddress.Any())
            {
                ValidateProjectListAddress(  projectAddress);
                output.ProjectAddress = ObjectMapper.Map<List<ProjectAddressOutputDto>>(CreateOrUpdateListProjectAddress(entity.Id, projectAddress));
            }

            #region Insert or update project facilities

            if (projectFacilities.FacilityIds != null)
            {
                _facilityAppService.ValidateProjectFacilities(projectFacilities.FacilityIds);
                output.ProjectFacilities = ObjectMapper.Map<List<ProjectFacilityMapOutputDto>>(_facilityAppService.CreateOrUpdateListProjectFacilities(entity.Id, projectFacilities.FacilityIds));
                
            }

            #endregion

            #region Insert or update project types

            if (projectTypes.ProjectTypeIds != null)
            {
                _projectTypeAppService.ValidatePropertyTypes(projectTypes.ProjectTypeIds);
                output.ProjectTypes = ObjectMapper.Map<List<ProjectTypeMapOutputDto>>(_projectTypeAppService.CreateOrUpdateListProjectTypes(entity.Id, projectTypes.ProjectTypeIds));
            }
        
            #endregion

            #region Insert or update project grade

            if (projectGrades.GradeIds != null)
            {
                _gradeAppService.ValidateProjectGrades(projectGrades.GradeIds);
                output.ProjectGrades = ObjectMapper.Map<List<ProjectGradeMapOutputDto>>(_gradeAppService.CreateOrUpdateListProjectGrades(entity.Id, projectGrades.GradeIds));
            }
            #endregion

            return output;
        }
        [AbpAllowAnonymous]
        public async Task<PagedResultDto<ProjectOutputDto>> FilterProject(string input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<ProjectFilterDto>("SpFilterProject",
                   new
                   {
                       @FilterJson = input
                   }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<ProjectOutputDto>>(output);
                return new PagedResultDto<ProjectOutputDto>(totalCount, results);
            }
        }
        #endregion

        #region Private Method


        private List<ProjectAddress> CreateOrUpdateListProjectAddress(long projectId, List<ProjectAddressInputDto> projectAddress)
        {

            var entities = new List<ProjectAddress>();
            projectAddress.ForEach(x =>
            {
                entities.Add(CreateOrUpdateProjectAddress(projectId, x));
            });
            return entities;
        }

        private ProjectAddress CreateOrUpdateProjectAddress(long projectId, ProjectAddressInputDto input)
        {
            var entityAddress = _repoProjectAddress.GetAll().Where(x => x.ProjectId == projectId).SingleOrDefault();
            ProjectAddress entity = new ProjectAddress();
            if (entityAddress != null)
            {
                var addressId = entityAddress.Id;
                entity = ObjectMapper.Map(input, entityAddress);
                entity.Id = addressId;
            }
            else
            {
                 entity = ObjectMapper.Map<ProjectAddress>(input);
            }
            entity.ProjectId = projectId;
            entity.IsActive = true;
            _repoProjectAddress.InsertOrUpdate(entity);
            return entity;
        }

        private void ValidateProjectListAddress( List<ProjectAddressInputDto> projectAddress)
        {
            projectAddress.ForEach(ValidateProjectAddress);
        }

        private void ValidateProjectAddress(ProjectAddressInputDto projectAddress)
        {
            if (projectAddress.Id.HasValue)
            {
                if (!_repoProjectAddress.GetAll().Any(x => x.Id == projectAddress.Id.Value))
                    throw new UserFriendlyException(L("ProjectAddressIdNotFound"));
            }
            if (projectAddress.DistrictId.HasValue)
            {
                if (!_repositoryDistrict.GetAll().Any(x => x.Id == projectAddress.DistrictId.Value))
                    throw new UserFriendlyException(L("ProjectDistrictIdNotFound"));
            }
            if (projectAddress.ProvinceId.HasValue)
            {
                if (!_repositoryProvince.GetAll().Any(x => x.Id == projectAddress.ProvinceId.Value))
                    throw new UserFriendlyException(L("ProjectProvinceIdNotFound"));
            }
            if (!_repositoryCountry.GetAll().Any(x => x.Id == projectAddress.CountryId))
                throw new UserFriendlyException(L("ProjectCountryIdNotFound"));
            if (projectAddress != null)
            {
                if (projectAddress.Latitude < AppConsts.GeocodeValues.MinLat
                    || projectAddress.Latitude > AppConsts.GeocodeValues.MaxLat
                    || projectAddress.Longitude < AppConsts.GeocodeValues.MinLng
                    || projectAddress.Longitude > AppConsts.GeocodeValues.MaxLng)
                    throw new UserFriendlyException(L("ProjectLatLngInvalid"));
            }
            if (string.IsNullOrEmpty(projectAddress.Address))
                throw new UserFriendlyException(L("RequiredField", nameof(projectAddress.Address)));

        }

        private void ValidateCreateProject(ProjectInputDto input)
        {
            if (string.IsNullOrEmpty(input.ProjectName))
                throw new UserFriendlyException(L("RequiredField", nameof(input.ProjectName)));
            if (string.IsNullOrEmpty(input.Description))
                throw new UserFriendlyException(L("RequiredField", nameof(input.Description)));
        }
        #endregion
    }
}
