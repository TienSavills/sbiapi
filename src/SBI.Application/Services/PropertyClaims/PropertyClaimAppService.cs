﻿using Abp.Application.Services;
using CRM.Application.Shared.PropertyClaims;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using CRM.Entities.PropertyClaims;
using CRM.SBIMap;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using System.Linq;
using CRM.EntitiesCustom;
using Abp.UI;
using CRM.Services.Currencys;

namespace CRM.Services.PropertyClaims
{
    public class PropertyClaimAppService : SBIAppServiceBase, IPropertyClaimAppService
    {
        private readonly IRepository<PropertyClaim, long> _repoPropertyClaim;
        private ConfigSetting _options;
        public PropertyClaimAppService(
            IOptions<ConfigSetting> options
            ,IRepository<PropertyClaim, long> repoPropertyClaim)
        {
            _repoPropertyClaim = repoPropertyClaim;
            _options = options.Value;
        }
        public async Task<PropertyClaimOutputDto> CreatePropertyClaimAsync(PropertyClaimInputDto input)
        {
            ValidateCreatePropertyClaim(input);
            var listPropertyClaim = _repoPropertyClaim.GetAll().Where(x => x.PropertyId == input.PropertyId).ToList();
            if (listPropertyClaim != null)
            {
                foreach (var objUpdate in listPropertyClaim)
                {
                    objUpdate.ToDate = DateTime.UtcNow;
                    objUpdate.IsActive = false;
                    _repoPropertyClaim.Update(objUpdate);
                }
            }
            var entity = ObjectMapper.Map<PropertyClaim>(input);
            entity.IsActive = true;
            entity.FromDate = DateTime.UtcNow;
            entity.ToDate = Convert.ToDateTime(CurrencyConstant.DefaultDate);
            entity.Id = await _repoPropertyClaim.InsertAndGetIdAsync(entity);
            return ObjectMapper.Map<PropertyClaimOutputDto>(entity);
        }

        public async Task<PagedResultDto<PropertyClaimOutputDto>> FilterPropertyClaimAsync(PropertyClaimFilterInputDto input)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<PropertyClaimFilterDto>("SpFilterPropertyClaim",
                    new
                    {
                        @SearchText = input.Keyword
                        ,@PageNumber = input.PageNumber
                        ,@PageSize = input.PageSize
                        ,@PropertyId =input.PropertyId
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<PropertyClaimOutputDto>>(output);
                return new PagedResultDto<PropertyClaimOutputDto>(totalCount, results);
            }
        }

        public async Task<PropertyClaimOutputDto> GetPropertyClaimDetail(long id)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = await conn.QueryAsync<PropertyClaimFilterDto>("SpFilterPropertyClaim",
                    new
                    {
                        @Id = id,
                    }, commandType: CommandType.StoredProcedure);
                var totalCount = output.FirstOrDefault()?.TotalCount ?? 0;
                var results = ObjectMapper.Map<List<PropertyClaimOutputDto>>(output).SingleOrDefault();
                return results;
            }
        }

        public async Task<PropertyClaimOutputDto> UpdatePropertyClaimAsync(long id, PropertyClaimInputDto input)
        {
            if (id < AppConsts.MinValuePrimaryKey)
                throw new UserFriendlyException(L("PropertyClaimIdNotFound"));
            var entity = _repoPropertyClaim.Get(id);

            ValidateCreatePropertyClaim(input);
            var entityUpdate = ObjectMapper.Map(input, entity);
            await _repoPropertyClaim.UpdateAsync(entityUpdate);
            return ObjectMapper.Map<PropertyClaimOutputDto>(entity);
        }
        private void ValidateCreatePropertyClaim(PropertyClaimInputDto input)
        {
        }
    }
}
