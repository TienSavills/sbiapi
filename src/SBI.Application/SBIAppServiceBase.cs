﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using CRM.Authorization.Users;
using CRM.MultiTenancy;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace CRM
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class SBIAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        protected SBIAppServiceBase()
        {
            LocalizationSourceName = SBIConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
        protected String ParseToString(int[] ids)
        {
            string depart = string.Empty;
            if (ids.Any())
            {
                foreach (var department in ids)
                {
                    depart = depart + "," + department.ToString();
                }
            }
            else
            {
                depart = null;
            }
            return depart;
        }
        protected async Task UpdateChilds<TModelChild, TEntityChild, TEntityChildKey>(
         IRepository<TEntityChild, TEntityChildKey> repositoryChild,
         Action<TEntityChild> repareAdd,
         Expression<Func<TEntityChild, bool>> select,
         Func<TModelChild, TEntityChild, bool> compare,
         params TModelChild[] modes
     )
         where TModelChild : Entity<TEntityChildKey>
         where TEntityChild : Entity<TEntityChildKey>, IPassivable
        {
            var repositoryChildEntities =
                await repositoryChild.GetAll().Where(select).ToListAsync();
            var willDelete = repositoryChildEntities.Where(x => !modes.Any(m => compare(m, x))).ToList();
            if (willDelete.Any())
                foreach (var item in willDelete)
                {
                    item.IsActive = false;
                    await repositoryChild.UpdateAsync(item);
                }
            var willUpdate = repositoryChildEntities.Where(x => modes.Any(m => compare(m, x))).ToList();
            if (willUpdate.Any())
                foreach (var item in willUpdate)
                {
                    var key = item.Id;
                    var itemUpdate = modes.FirstOrDefault(m => compare(m, item));
                    if (itemUpdate != null)
                    {
                        itemUpdate.Id = key;
                        ObjectMapper.Map(itemUpdate, item);
                        await repositoryChild.UpdateAsync(item);
                    }
                }
            var willAdd = modes.Where(x => !repositoryChildEntities.Any(e => compare(x, e))).ToList();
            if (willAdd.Any())
                foreach (var item in willAdd)
                {
                    var addEntity = ObjectMapper.Map<TEntityChild>(item);
                    addEntity.IsActive = true;
                    addEntity.Id = default;
                    repareAdd?.Invoke(addEntity);
                    item.Id = await repositoryChild.InsertAndGetIdAsync(addEntity);
                }
        }

        protected async Task AppendChild<TModel, TModelAppend, TEntity, TPrimaryKey>(
            IRepository<TEntity, TPrimaryKey> repository,
            Func<TModel, TPrimaryKey> select,
            Func<List<TEntity>, Task> repareEntity,
            Action<TModel, TModelAppend> repareModel,
            params TModel[] models)
        //where TModelAppend : SimpleOutput
        where TEntity : Entity<TPrimaryKey>
        {
            if (models != null && models.Any())
            {
                var ids = models.Select(select).Distinct().ToList();
                var entities = await repository.GetAll().Where(x => ids.Contains(x.Id)).AsNoTracking().ToListAsync();
                if (repareEntity != null)
                    await repareEntity(entities);
                foreach (var model in models)
                {
                    var entity = entities.FirstOrDefault(x => x.Id.Equals(select(model)));
                    repareModel(model, ObjectMapper.Map<TModelAppend>(entity));
                }
            }
        }
    }
}
