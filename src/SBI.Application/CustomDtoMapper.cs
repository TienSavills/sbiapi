﻿using System;
using System.Collections.Generic;
using AutoMapper;
using CRM.Application.Shared.PropertyAddresss;
using CRM.Application.Shared.PropertyFacilityMaps;
using CRM.Application.Shared.Property.Dto;
using CRM.Application.Shared.PropertyTypeMap;
using CRM.Application.Shared.Project.Dto;
using CRM.Application.Shared.ProjectAddresss;
using CRM.Application.Shared.ProjectFacilityMaps;
using CRM.Application.Shared.ProjectTypeMaps;
using CRM.Entities.PropertyAddresss;
using CRM.Entities.PropertyFacilityMaps;
using CRM.Entities.PropertyTypeMaps;
using CRM.Entities.Facilities;
using CRM.Entities.Properties;
using CRM.Entities.ProjectAddresss;
using CRM.Entities.ProjectFacilityMaps;
using CRM.Entities.ProjectTypeMaps;
using CRM.EntitiesCustom;
using CRM.Application.Shared.AttachmentDocuments;
using CRM.Entities.AttachmentDocuments;
using CRM.Application.Shared.ProjectTenant;
using CRM.Entities.Company;
using CRM.Application.Shared.Company;
using CRM.Entities.Projects;
using CRM.Application.Shared.PropertyTypes;
using CRM.Entities.PropertyTypes;
using CRM.Application.Shared.ConstructionStatus;
using CRM.Entities.ConstructionStatus;
using CRM.Application.Shared.ProjectGradeMaps;
using CRM.Entities.Grades;
using CRM.Application.Shared.Grades;
using CRM.Entities.ProjectGradeMaps;
using CRM.Entities.PropertyGradeMaps;
using CRM.Application.Shared.PropertyGradeMaps;
using CRM.Entities.Currencys;
using CRM.Application.Shared.Currentcys;
using CRM.Application.Shared.CurrencyRates;
using CRM.Entities.CurrencyRates;
using CRM.Application.Shared.ProjectClaims;
using CRM.Entities.ProjectClaims;
using CRM.Application.Shared.PropertyClaims;
using CRM.Entities.PropertyClaims;
using CRM.Application.Shared.ProjectConstructionMaps;
using CRM.Entities.ProjectConstructionMaps;
using CRM.Application.Shared.PropertyConstructionMaps;
using CRM.Entities.ProperyConstructionMaps;
using CRM.Entities.Districs;
using CRM.Application.Shared.Districts;
using CRM.Entities.Wards;
using CRM.Application.Shared.Wards;
using CRM.Application.Shared.ProjectClaimRoomTypeMaps;
using CRM.Entities.ProjectClaimRoomTypeMaps;
using CRM.Application.Shared.MacroIndicator;
using CRM.Entities.MacroIndicators;
using Abp.Authorization;
using CRM.Services.Roles.Dto;
using CRM.Entities.CompanyAddress;
using CRM.Application.Shared.Contact;
using CRM.Entities.ContactAddress;
using CRM.Entities.Contacts;
using CRM.Entities.CompanyOrganizationUnit;
using CRM.Application.Shared.CompanyPhone;
using CRM.Entities.CompanyPhone;
using CRM.Entities.CompanyEmail;
using CRM.Application.Shared.CompanyEmail;
using CRM.Entities.CompanyUser;
using CRM.Application.Shared.CompanyUser;
using CRM.Entities.ContactPhone;
using CRM.Entities.ContactEmail;
using CRM.Entities.ContactOrganizationUnit;
using CRM.Application.Shared.ContactEmail;
using CRM.Application.Shared.ContactPhone;
using CRM.Entities.ContactUser;
using CRM.Application.Shared.ContactOrganizationUnit;
using CRM.Application.Shared.ContactUser;
using CRM.Application.Shared.Activity;
using CRM.Entities.Activity;
using CRM.Application.Shared.ActivityType;
using CRM.Entities.ActivityType;
using CRM.Application.Shared.ActivityAttendee;
using CRM.Application.Shared.ActivityReminder;
using CRM.Entities.ActivityAttendee;
using CRM.Entities.ActivityReminder;
using CRM.Entities.ActivityUser;
using CRM.Entities.ActivityOrganizationUnit;
using CRM.Application.Shared.ActivityUser;
using CRM.Application.Shared.ActivityOrganizationUnit;
using CRM.Entities.OtherCategory;
using CRM.Application.Shared.OtherCategory;
using CRM.Application.Shared.Request;
using CRM.Entities.Requests;
using CRM.Entities.RequestUser;
using CRM.Application.Shared.RequestUser;
using CRM.Entities.RequestOrganizationUnit;
using CRM.Application.Shared.RequestOrganizationUnit;
using Abp.Organizations;
using CRM.Application.Shared.OrganizationUnit.Dto;
using CRM.Application.Shared.Comment;
using CRM.Application.Shared.CompanyAddress;
using CRM.Application.Shared.Users;
using CRM.Entities.PropertyUser;
using CRM.Application.Shared.PropertyUser;
using CRM.Entities.PropertyOrganizationUnit;
using CRM.Application.Shared.PropertyOrganizationUnit;
using CRM.Entities.CompanyContact;
using CRM.Application.Shared.CompanyContact;
using CRM.Entities.RequestContact;
using CRM.Application.Shared.RequestContact;
using CRM.Entities.PropertyContact;
using CRM.Application.Shared.PropertyContact;
using CRM.Application.Shared.Opportunity;
using CRM.Entities.Opportunity;
using CRM.Application.Shared.OpportunityProperty;
using CRM.Application.Shared.OpportunityUser;
using CRM.Application.Shared.OpportunityOrganizationUnit;
using CRM.Entities.OpportunityProperty;
using CRM.Entities.OpportunityUser;
using CRM.Entities.OpportunityOrganizationUnit;
using CRM.Application.Shared.OpportunityAssetClass;
using CRM.Entities.OpportunityAssetClass;
using CRM.Application.Shared.OpportunityInstruction;
using CRM.Entities.OpportunityInstruction;
using CRM.Application.Shared.OpportunityContact;
using CRM.Entities.OpportunityContact;
using CRM.Application.Shared.CompanyOrganizationUnit;
using CRM.Application.Shared.ContactAddress;
using CRM.Application.Shared.OpportunityCategory;
using CRM.Entities.OpportuityCategory;
using CRM.Application.Shared.Deal;
using CRM.Entities.Deal;
using CRM.Application.Shared.DealOpportunity;
using CRM.Application.Shared.DealShare;
using CRM.Entities.DealOpportunity;
using CRM.Entities.DealShare;
using CRM.Application.Shared.Payment;
using CRM.Entities.Payment;
using CRM.Application.Shared.OpportunityLeadUser;
using CRM.Entities.OpportunityLeadUser;
using CRM.Authorization.Users;
using CRM.Entities.Floor;
using CRM.Application.Shared.Floor;
using CRM.Application.Shared.Unit;
using CRM.Entities.Unit;
using CRM.Application.Shared.UnitHistory;
using CRM.Entities.UnitHistory;
using CRM.Application.Shared.Campaign;
using CRM.Application.Shared.CampaignTarget;
using CRM.Application.Shared.TargetList;
using CRM.Application.Shared.TargetContact;
using CRM.Application.Shared.TargetRequest;
using CRM.ResponseHelper;
using CRM.Application.Shared.OpportunityProject;
using CRM.Entities.OpportunityProject;
using CRM.Application.Shared.Financial;
using CRM.Entities.Financial;
using CRM.Application.Shared.ContactTypeMap;
using CRM.Application.Shared.CompanyTypeMap;
using CRM.Entities.CompanyTypeMap;
using CRM.Entities.ContactTypeMap;
using CRM.Application.Shared.CommentSendEmail;
using CRM.Entities.CommentSendMail;
using Abp.Localization;
using CRM.Application.Shared;
using CRM.Application.Shared.Category;
using CRM.Services.Localization.Dto;
using CRM.Application.Shared.DealAdjust;
using CRM.Entities.DealAdjust;
using CRM.Entities.Countries;
using CRM.Application.Shared.Countries;
using CRM.Application.Shared.Inquiries;
using CRM.Application.Shared.InquiryAddress;
using CRM.Application.Shared.InquiryCategory;
using CRM.Application.Shared.InquiryFacilityMaps;
using CRM.Application.Shared.InquiryUsers;
using CRM.Application.Shared.InquiryViewMaps;
using CRM.Application.Shared.ListingCategory;
using CRM.Application.Shared.Listings;
using CRM.Application.Shared.ListingUsers;
using CRM.Application.Shared.News;
using CRM.Application.Shared.ProjectTransportationMaps;
using CRM.Entities.Provinces;
using CRM.Application.Shared.Provinces;
using CRM.Application.Shared.UnitAddress;
using CRM.Application.Shared.UnitFacilityMaps;
using CRM.Application.Shared.UnitViewMaps;
using CRM.Entities.Facing;
using CRM.Entities.Inquiries;
using CRM.Entities.InquiryAddress;
using CRM.Entities.InquiryCategory;
using CRM.Entities.InquiryFacilityMaps;
using CRM.Entities.InquiryViewMaps;
using CRM.Entities.ListingCategory;
using CRM.Entities.Listings;
using CRM.Entities.ListingUsers;
using CRM.Entities.News;
using CRM.Entities.ProjectTransportationMaps;
using CRM.Entities.Transportation;
using CRM.Entities.UnitAddress;
using CRM.Entities.UnitFacilityMaps;
using CRM.Entities.UnitViewMaps;
using CRM.Entities.View;
using CRM.Services.Users.Dto;
using ProjectAddress = CRM.Entities.ProjectAddresss.ProjectAddress;

namespace CRM
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
        }
        public static void ProjectsMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<ProjectInputDto, Project>();
            configuration.CreateMap<Project, ProjectOutputDto>()
                .ForMember(x => x.ProjectAddress, option => option.MapFrom(src => src.ProjectAddress))
                .ForMember(x => x.ProjectFacilities, option => option.MapFrom(src => src.ProjectFacilityMap))
                .ForMember(x => x.ProjectTransportations, option => option.MapFrom(src => src.ProjectTransportationMaps))
                .ForMember(x => x.ProjectTypes, option => option.MapFrom(src => src.ProjectTypeMap))
                .ForMember(x => x.ProjectGrades, option => option.MapFrom(src => src.ProjectGradeMap));


            configuration.CreateMap<ProjectFilterDto, ProjectOutputDto>()
                .ForMember(x => x.ProjectAddress, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectAddress) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectAddressOutputDto>>(src.JsonProjectAddress) : new List<ProjectAddressOutputDto>()))
                .ForMember(x => x.ProjectTypes, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectTypeMap) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectTypeMapOutputDto>>(src.JsonProjectTypeMap) : new List<ProjectTypeMapOutputDto>()))
                .ForMember(x => x.ProjectFacilities, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectFacilityMap) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectFacilityMapOutputDto>>(src.JsonProjectFacilityMap) : new List<ProjectFacilityMapOutputDto>()))
                .ForMember(x => x.ProjectTransportations, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectTransportationMap) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectTransportationMapOutputDto>>(src.JsonProjectTransportationMap) : new List<ProjectTransportationMapOutputDto>()))
                .ForMember(x => x.ProjectGrades, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectGrade) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectGradeMapOutputDto>>(src.JsonProjectGrade) : new List<ProjectGradeMapOutputDto>())); ;


            configuration.CreateMap<ProjectFilterMapDto, ProjectMapOutputDto>()
              .ForMember(x => x.ProjectTypes, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectTypeMap) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectTypeMapOutputDto>>(src.JsonProjectTypeMap) : new List<ProjectTypeMapOutputDto>()))
              .ForMember(x => x.ProjectFacilities, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectFacilityMap) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectFacilityMapOutputDto>>(src.JsonProjectFacilityMap) : new List<ProjectFacilityMapOutputDto>()))
              .ForMember(x => x.ProjectGrades, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectGrade) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectGradeMapOutputDto>>(src.JsonProjectGrade) : new List<ProjectGradeMapOutputDto>())); ;

            configuration.CreateMap<ProjectAddressInputDto, ProjectAddress>();
            configuration.CreateMap<ProjectAddress, ProjectAddressOutputDto>();

            configuration.CreateMap<UnitAddressInputDto, UnitAddress>();
            configuration.CreateMap<UnitAddress, UnitAddressOutputDto>();

            configuration.CreateMap<Floor, FloorOutputDto>()
               .ForMember(x => x.ProjectName, option => option.MapFrom(src => src.Project.ProjectName));
            configuration.CreateMap<FloorInputDto, Floor>();

            configuration.CreateMap<User, OrganizationUnitUserListDto>();

            configuration.CreateMap<ProjectFacilityMap, ProjectFacilityMapOutputDto>()
                 .ForMember(x => x.Description, option => option.MapFrom(src => src.Facility.Description))
                 .ForMember(x => x.Name, option => option.MapFrom(src => src.Facility.Name));
            configuration.CreateMap<ProjectTransportationMaps, ProjectTransportationMapOutputDto>();

            configuration.CreateMap<UnitFacilityMapInputDto, UnitFacilityMaps>();

            configuration.CreateMap<UnitFacilityMaps,UnitFacilityMapOutputDto>()
                .ForMember(x => x.Description, option => option.MapFrom(src => src.Facility.Description))
                .ForMember(x => x.Name, option => option.MapFrom(src => src.Facility.Name));
            
            configuration.CreateMap<UnitViewMapInputDto, UnitViewMaps>();
            configuration.CreateMap<UnitViewMaps, UnitViewMapOutputDto>()
                .ForMember(x => x.Description, option => option.MapFrom(src => src.View.Description))
                .ForMember(x => x.Name, option => option.MapFrom(src => src.View.Name));

            configuration.CreateMap<Facility, CategoryOutputBasic>();
            configuration.CreateMap<Facing, CategoryOutputBasic>();
            configuration.CreateMap<View, CategoryOutputBasic>();
            configuration.CreateMap<Transportation, CategoryOutputBasic>();
            configuration.CreateMap<ProjectGradeMap, ProjectGradeMapOutputDto>()
               .ForMember(x => x.GradeCode, option => option.MapFrom(src => src.Grade.GradeCode))
                .ForMember(x => x.GradeName, option => option.MapFrom(src => src.Grade.GradeName))
               .ForMember(x => x.GradeGroup, option => option.MapFrom(src => src.Grade.GradeGroup));
            configuration.CreateMap<Grade, GradeOutputDto>();


            configuration.CreateMap<ProjectTypeMap, ProjectTypeMapOutputDto>()
                 .ForMember(x => x.Code, option => option.MapFrom(src => src.PropertyType.Code))
                 .ForMember(x => x.TypeName, option => option.MapFrom(src => src.PropertyType.TypeName));
            configuration.CreateMap<PropertyType, PropertyTypeOutputDto>();

            //configuration.CreateMap<ProjectLandlord, ProjectLandlordOutputDto>()
            //      .ForMember(x => x.LegalName, option => option.MapFrom(src => src.Company.LegalName))
            //     .ForMember(x => x.BusinessName, option => option.MapFrom(src => src.Company.BusinessName))
            //   .ForMember(x => x.Vatcode, option => option.MapFrom(src => src.Company.Vatcode))
            //    .ForMember(x => x.Website, option => option.MapFrom(src => src.Company.Website))
            //    .ForMember(x => x.Description, option => option.MapFrom(src => src.Company.Description))
            //     .ForMember(x => x.IsPersonal, option => option.MapFrom(src => src.Company.IsPersonal)); ;
            //configuration.CreateMap<ProjectTenant, ProjectTenantOutputDto>()
            //      .ForMember(x => x.LegalName, option => option.MapFrom(src => src.Company.LegalName))
            //     .ForMember(x => x.BusinessName, option => option.MapFrom(src => src.Company.BusinessName))
            //   .ForMember(x => x.Vatcode, option => option.MapFrom(src => src.Company.Vatcode))
            //    .ForMember(x => x.Website, option => option.MapFrom(src => src.Company.Website))
            //    .ForMember(x => x.Description, option => option.MapFrom(src => src.Company.Description))
            //     .ForMember(x => x.IsPersonal, option => option.MapFrom(src => src.Company.IsPersonal))
            //        .ForMember(x => x.TenantTypeId, option => option.MapFrom(src => src.TenantTypeId))
            //      .ForMember(x => x.TenantTypeName, option => option.MapFrom(src => src.TenantType));
            configuration.CreateMap<Company, CompanyOutputDto>();
            configuration.CreateMap<CompanyFilterDto, CompanyOutputDto>();

            configuration.CreateMap<ConstructionStatus, ConstructionStatusOuputDto>();

        }
        public static void PropertiesMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<PropertyInputDto, Property>();
            configuration.CreateMap<Property, PropertyOutputDto>();

            configuration.CreateMap<PropertyFilterDto, PropertyOutputDto>()
                 .ForMember(x => x.PropertyAddress, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonPropertyAddress) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<PropertyAddressOutputDto>>(src.JsonPropertyAddress) : new List<PropertyAddressOutputDto>()));
            configuration.CreateMap<PropertyAddressInputDto, PropertyAddress>();
            //.ForMember(x => x.Latitude, option => option.MapFrom(src => src.Geography.Latitude))
            //.ForMember(x => x.Longitude, option => option.MapFrom(src => src.Geography.Longitude));
            configuration.CreateMap<PropertyAddress, PropertyAddressOutputDto>();

            configuration.CreateMap<PropertyFacilityMapInputDto, PropertyFacilityMap>();
            configuration.CreateMap<PropertyFacilityMap, PropertyFacilityMapOutputDto>()
                 .ForMember(x => x.Description, option => option.MapFrom(src => src.Facility.Description))
                 .ForMember(x => x.Name, option => option.MapFrom(src => src.Facility.Name));

            configuration.CreateMap<PropertyTypeMapInputDto, PropertyTypeMap>();
            configuration.CreateMap<PropertyTypeMap, PropertyTypeMapOutputDto>()
                .ForMember(x => x.Code, option => option.MapFrom(src => src.PropertyType.Code))
                 .ForMember(x => x.TypeName, option => option.MapFrom(src => src.PropertyType.TypeName));

            //configuration.CreateMap<PropertyLandlord, PropertyLandlordOutputDto>()
            //      .ForMember(x => x.LegalName, option => option.MapFrom(src => src.Company.LegalName))
            //     .ForMember(x => x.BusinessName, option => option.MapFrom(src => src.Company.BusinessName))
            //   .ForMember(x => x.Vatcode, option => option.MapFrom(src => src.Company.Vatcode))
            //    .ForMember(x => x.Website, option => option.MapFrom(src => src.Company.Website))
            //    .ForMember(x => x.Description, option => option.MapFrom(src => src.Company.Description))
            //     .ForMember(x => x.IsPersonal, option => option.MapFrom(src => src.Company.IsPersonal)); ;
            //configuration.CreateMap<PropertyTenant, PropertyTenantOutputDto>()
            //      .ForMember(x => x.LegalName, option => option.MapFrom(src => src.Company.LegalName))
            //     .ForMember(x => x.BusinessName, option => option.MapFrom(src => src.Company.BusinessName))
            //   .ForMember(x => x.Vatcode, option => option.MapFrom(src => src.Company.Vatcode))
            //    .ForMember(x => x.Website, option => option.MapFrom(src => src.Company.Website))
            //    .ForMember(x => x.Description, option => option.MapFrom(src => src.Company.Description))
            //     .ForMember(x => x.IsPersonal, option => option.MapFrom(src => src.Company.IsPersonal))
            //       .ForMember(x => x.TenantTypeId, option => option.MapFrom(src => src.TenantTypeId))
            //      .ForMember(x => x.TenantTypeName, option => option.MapFrom(src => src.TenantType));

            configuration.CreateMap<PropertyGradeMap, PropertyGradeMapOutputDto>()
               .ForMember(x => x.GradeCode, option => option.MapFrom(src => src.Grade.GradeCode))
                .ForMember(x => x.GradeName, option => option.MapFrom(src => src.Grade.GradeName))
               .ForMember(x => x.GradeGroup, option => option.MapFrom(src => src.Grade.GradeGroup));
            configuration.CreateMap<Grade, GradeOutputDto>();

            configuration.CreateMap<PropertyUser, PropertyUserOutputDto>();
            configuration.CreateMap<PropertyOrganizationUnit, PropertyOrganizationUnitOutputDto>();
            configuration.CreateMap<PropertyUserInputDto, PropertyUser>();
            configuration.CreateMap<PropertyOrganizationUnitInputDto, PropertyOrganizationUnit>();
        }

        public static void ListingMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<ListingInputDto, Listings>();
            configuration.CreateMap<Listings, ListingOutputDto>();
            configuration.CreateMap<ListingCategory, ListingCategoryOutputDto>();
            configuration.CreateMap<ListingUserInputDto, ListingUsers>();
            configuration.CreateMap<ListingUsers, ListingOutputDto>();
            configuration.CreateMap<ListingFilterDto, ListingOutputDto>()
                .ForMember(x => x.Users, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUser) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ListingUserOutputDto>>(src.JsonUser) : new List<ListingUserOutputDto>()));
        }
        public static void UnitMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<UnitInputDto, Unit>();
            configuration.CreateMap<UnitFilterDto, UnitOutputDto>();
            configuration.CreateMap<Unit, UnitOutputDto>()
                .ForMember(x => x.Color, y => y.MapFrom(src => src.Status.Color))
                .ForMember(x => x.FloorName, y => y.MapFrom(src => src.Floor.FloorName))
                .ForMember(x => x.StatusName, y => y.MapFrom(src => src.Status.Name))
                .ForMember(x => x.UnitTypeName, y => y.MapFrom(src => src.UnitType.Name))
                .ForMember(x => x.CreatorUserName, y => y.MapFrom(src => src.CreatorUser.Name))
                .ForMember(x => x.CreatorSurname, y => y.MapFrom(src => src.CreatorUser.Surname))
                .ForMember(x => x.CreationTime, y => y.MapFrom(src => src.CreatorUser.CreationTime))
                .ForMember(x => x.LastModificationTime, y => y.MapFrom(src => src.LastModifierUser.LastModificationTime))
                .ForMember(x => x.LastModifierUserName, y => y.MapFrom(src => src.LastModifierUser.Name))
                .ForMember(x => x.LastModifierSurname, y => y.MapFrom(src => src.LastModifierUser.Surname));

            configuration.CreateMap<UnitResInputDto, Unit>();
            configuration.CreateMap<UnitResFilterDto, UnitResOutputDto>()
                .ForMember(x => x.UnitAddress, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUnitAddress) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<UnitAddressOutputDto>>(src.JsonUnitAddress) : new List<UnitAddressOutputDto>()))
                .ForMember(x => x.UnitFacilities, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUnitFacility) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<UnitFacilityMapOutputDto>>(src.JsonUnitFacility) : new List<UnitFacilityMapOutputDto>()))
                .ForMember(x => x.UnitViews, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUnitView) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<UnitViewMapOutputDto>>(src.JsonUnitView) : new List<UnitViewMapOutputDto>()));
           
            configuration.CreateMap<Unit, UnitResOutputDto>()
                .ForMember(x => x.FloorName, y => y.MapFrom(src => src.Floor.FloorName))
                .ForMember(x => x.StatusName, y => y.MapFrom(src => src.Status.Name))
                .ForMember(x => x.UnitTypeName, y => y.MapFrom(src => src.UnitType.Name))
                .ForMember(x => x.CreatorUserName, y => y.MapFrom(src => src.CreatorUser.Name))
                .ForMember(x => x.CreatorSurname, y => y.MapFrom(src => src.CreatorUser.Surname))
                .ForMember(x => x.CreationTime, y => y.MapFrom(src => src.CreatorUser.CreationTime))
                .ForMember(x => x.LastModificationTime, y => y.MapFrom(src => src.LastModifierUser.LastModificationTime))
                .ForMember(x => x.LastModifierUserName, y => y.MapFrom(src => src.LastModifierUser.Name))
                .ForMember(x => x.LastModifierSurname, y => y.MapFrom(src => src.LastModifierUser.Surname));


            //Unit History
            configuration.CreateMap<UnitHistoryInputDto, UnitHistory>();
            configuration.CreateMap<UnitHistory, UnitHistoryOutputDto>()
                .ForMember(x => x.TenantLegalName, y => y.MapFrom(src => src.OrgTenant.LegalName))
                .ForMember(x => x.TenantBusinessName, y => y.MapFrom(src => src.OrgTenant.BusinessName))
                .ForMember(x => x.UnitName, y => y.MapFrom(src => src.Unit.UnitName))
                .ForMember(x => x.FloorId, y => y.MapFrom(src => src.Unit.Floor.Id))
                .ForMember(x => x.FloorName, y => y.MapFrom(src => src.Unit.Floor.FloorName))
                .ForMember(x => x.ProjectId, y => y.MapFrom(src => src.Unit.Floor.Project.Id))
                .ForMember(x => x.ProjectName, y => y.MapFrom(src => src.Unit.Floor.Project.ProjectName))
                .ForMember(x => x.CreatorUserName, y => y.MapFrom(src => src.CreatorUser.Name))
                .ForMember(x => x.CreatorSurname, y => y.MapFrom(src => src.CreatorUser.Surname))
                .ForMember(x => x.CreationTime, y => y.MapFrom(src => src.CreatorUser.CreationTime))
                .ForMember(x => x.LastModificationTime, y => y.MapFrom(src => src.LastModifierUser.LastModificationTime))
                .ForMember(x => x.LastModifierUserName, y => y.MapFrom(src => src.LastModifierUser.Name))
                .ForMember(x => x.LastModifierSurname, y => y.MapFrom(src => src.LastModifierUser.Surname));
        }
        public static void AttachmentMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<AttachmentDocumentInputDto, AttachmentDocument>();
            configuration.CreateMap<AttachmentDocumentUpdateInputDto, AttachmentDocument>();
            configuration.CreateMap<AttachmentDocument, AttachmentDocumentOutputDto>();
        }
        public static void CurrencyMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Currency, CurrencyOutputDto>();
            configuration.CreateMap<CurrencyRate, CurrencyRateOutputDto>();
            configuration.CreateMap<CurrencyRateInputDto, CurrencyRate>();
        }
        public static void CompanyMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CompanyInputDto, Company>();
            configuration.CreateMap<CompanyFullInputDto, Company>();
            configuration.CreateMap<CompanyFilterDto, CompanyOutputDto>()
            .ForMember(x => x.CompanyAddress, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonCompanyAddress) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompanyAddressOutputDto>>(src.JsonCompanyAddress) : new List<CompanyAddressOutputDto>()))
            .ForMember(x => x.CompanyPhone, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonCompanyPhone) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompanyPhoneOutputDto>>(src.JsonCompanyPhone) : new List<CompanyPhoneOutputDto>()))
            .ForMember(x => x.CompanyTypeMap, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonCompanyType) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompanyTypeMapOutputDto>>(src.JsonCompanyType) : new List<CompanyTypeMapOutputDto>()))
            .ForMember(x => x.CompanyUser, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUser) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompanyUserV1OutputDto>>(src.JsonUser) : new List<CompanyUserV1OutputDto>()))
            .ForMember(x => x.CompanyOrganizationUnit, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonOrganizationUnit) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompanyOrganizationUnitV1OutputDto>>(src.JsonOrganizationUnit) : new List<CompanyOrganizationUnitV1OutputDto>()));
           
            configuration.CreateMap<CompanyAddressInputDto, CompanyAddress>();
            configuration.CreateMap<CompanyAddress, CompanyAddressOutputDto>();
            configuration.CreateMap<Company, CompanyOutputDto>();

            configuration.CreateMap<CompanyTypeMapInputDto, CompanyTypeMap>();
            configuration.CreateMap<CompanyTypeMap, CompanyTypeMapOutputDto>()
                    .ForMember(x => x.CompanyTypeName, option => option.MapFrom(src => src.CompanyType.Name));
            configuration.CreateMap<ContactTypeMapInputDto, ContactTypeMap>();
            configuration.CreateMap<ContactTypeMap, ContactTypeMapOutputDto>()
                 .ForMember(x => x.ContactTypeName, option => option.MapFrom(src => src.ContactType.Name));
            configuration.CreateMap<ContactFullInputDto, Contact>();
            configuration.CreateMap<ContactInputDto, Contact>();
            configuration.CreateMap<ContactFilterDto, ContactOutputDto>()
            .ForMember(x => x.ContactAddress, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonContactAddress) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContactAddressOutputDto>>(src.JsonContactAddress) : new List<ContactAddressOutputDto>()))
            .ForMember(x => x.ContactUser, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUser) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContactUserV1OutputDto>>(src.JsonUser) : new List<ContactUserV1OutputDto>()))
            .ForMember(x => x.ContactEmail, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonContactEmail) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContactEmailOutputDto>>(src.JsonContactEmail) : new List<ContactEmailOutputDto>()))
            .ForMember(x => x.ContactOrganizationUnit, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonOrganizationUnit) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContactOrganizationUnitV1OutputDto>>(src.JsonOrganizationUnit) : new List<ContactOrganizationUnitV1OutputDto>()))
            .ForMember(x => x.ContactPhone, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonContactPhone) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContactPhoneOutputDto>>(src.JsonContactPhone) : new List<ContactPhoneOutputDto>()))
            .ForMember(x => x.ContactCompany, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonContactCompany) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContactCompanyOutputDto>>(src.JsonContactCompany) : new List<ContactCompanyOutputDto>()))
            .ForMember(x => x.ContactTypeMap, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonContactType) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContactTypeMapOutputDto>>(src.JsonContactType) : new List<ContactTypeMapOutputDto>()));
            configuration.CreateMap<ContactAddressInputDto, ContactAddress>();
            configuration.CreateMap<ContactAddress, ContactAddressOutputDto>();
            configuration.CreateMap<Contact, ContactOutputDto>();

            configuration.CreateMap<CompanyPhone, CompanyPhoneOutputDto>()
                .ForMember(x => x.PhoneTypeName, option => option.MapFrom(src => src.PhoneType.Name));
            configuration.CreateMap<CompanyEmail, CompanyEmailOutputDto>();
            configuration.CreateMap<CompanyEmailInputDto, CompanyEmail>();
            configuration.CreateMap<CompanyPhoneInputDto, CompanyPhone>();

            configuration.CreateMap<CompanyUser, CompanyUserOutputDto>()
                   .ForMember(x => x.User, option => option.MapFrom(src => src.User));
            configuration.CreateMap<CompanyOrganizationUnit, CompanyOrganizationUnitOutputDto>()
                  .ForMember(x => x.OrganizationUnit, option => option.MapFrom(src => src.OrganizationUnit));
            configuration.CreateMap<CompanyOrganizationUnit, CompanyOrganizationUnitV1OutputDto>()
                .ForMember(x => x.OrganizationUnitName, option => option.MapFrom(src => src.OrganizationUnit.DisplayName));
            configuration.CreateMap<CompanyUserInputDto, CompanyUser>();
            configuration.CreateMap<CompanyOrganizationUnitInputDto, CompanyOrganizationUnit>();

            configuration.CreateMap<ContactPhone, ContactPhoneOutputDto>()
                  .ForMember(x => x.PhoneTypeName, option => option.MapFrom(src => src.PhoneType.Name));
            configuration.CreateMap<ContactEmail, ContactEmailOutputDto>()
                 .ForMember(x => x.ContactName, option => option.MapFrom(src => src.Contact.ContactName));
            configuration.CreateMap<ContactEmailInputDto, ContactEmail>();
            configuration.CreateMap<ContactPhoneInputDto, ContactPhone>();

            configuration.CreateMap<ContactUser, ContactUserOutputDto>()
                   .ForMember(x => x.User, option => option.MapFrom(src => src.User));
            configuration.CreateMap<ContactOrganizationUnit, ContactOrganizationUnitOutputDto>()
                  .ForMember(x => x.OrganizationUnit, option => option.MapFrom(src => src.OrganizationUnit));
            configuration.CreateMap<ContactOrganizationUnit, ContactOrganizationUnitV1OutputDto>()
                 .ForMember(x => x.OrganizationUnitName, option => option.MapFrom(src => src.OrganizationUnit.DisplayName));
            configuration.CreateMap<ContactUserInputDto, ContactUser>();
            configuration.CreateMap<ContactOrganizationUnitInputDto, ContactOrganizationUnit>();

            configuration.CreateMap<CompanyContact, CompanyContactOutputDto>()
                .ForMember(x => x.ContactName, option => option.MapFrom(src => src.Contact.ContactName));
            configuration.CreateMap<CompanyContactInputDto, CompanyContact>();

            configuration.CreateMap<CompanyContact, ContactCompanyOutputDto>()
              .ForMember(x => x.BusinessName, option => option.MapFrom(src => src.Company.BusinessName))
               .ForMember(x => x.LegalName, option => option.MapFrom(src => src.Company.LegalName));
            configuration.CreateMap<ContactCompanyInputDto, CompanyContact>();

            configuration.CreateMap<RequestContact, RequestContactOutputDto>()
               .ForMember(x => x.ContactName, option => option.MapFrom(src => src.Contact.ContactName));
            configuration.CreateMap<RequestContactInputDto, RequestContact>();

            configuration.CreateMap<PropertyContact, PropertyContactOutputDto>()
              .ForMember(x => x.ContactName, option => option.MapFrom(src => src.Contact.ContactName));
            configuration.CreateMap<PropertyContactInputDto, PropertyContact>();
        }
        public static void ProjectProperyMapping(IMapperConfigurationExpression configuration)
        {

            //claim
            configuration.CreateMap<ProjectClaimInputDto, ProjectClaim>();
            configuration.CreateMap<ProjectClaim, ProjectClaimOutputDto>();
            configuration.CreateMap<ProjectClaimRoomTypeMap, ProjectClaimRoomTypeMapOutputDto>()
             .ForMember(x => x.RoomTypeName, option => option.MapFrom(src => src.RoomType.RoomTypeName));
            configuration.CreateMap<ProjectClaimFilterDto, ProjectClaimOutputDto>()
            .ForMember(x => x.ProjectTenants, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectTenant) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectTenantOutputDto>>(src.JsonProjectTenant) : new List<ProjectTenantOutputDto>()))
            .ForMember(x => x.ProjectGrades, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectGrade) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectGradeMapOutputDto>>(src.JsonProjectGrade) : new List<ProjectGradeMapOutputDto>()))
            .ForMember(x => x.ProjectClaimRoomType, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProjectClaimRoomType) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectClaimRoomTypeMapOutputDto>>(src.JsonProjectClaimRoomType) : new List<ProjectClaimRoomTypeMapOutputDto>()));

            configuration.CreateMap<PropertyClaimInputDto, PropertyClaim>();
            configuration.CreateMap<PropertyClaim, PropertyClaimOutputDto>();
            configuration.CreateMap<PropertyClaimFilterDto, PropertyClaimOutputDto>();

            //Construction
            configuration.CreateMap<ProjectConstructionMapInputDto, ProjectConstructionMap>();
            configuration.CreateMap<ProjectConstructionMap, ProjectConstructionMapOutputDto>();

            configuration.CreateMap<PropertyConstructionMapInputDto, PropertyConstructionMap>();
            configuration.CreateMap<PropertyConstructionMap, PropertyConstructionMapOutputDto>();

            //District Ward

            configuration.CreateMap<District, DistrictOutputDto>();
            configuration.CreateMap<Country, CountryOutputDto>();
            configuration.CreateMap<Province, ProvinceOutputDto>();

            configuration.CreateMap<Ward, WardOutputDto>();
            configuration.CreateMap<OtherCategory, OtherCategoryOutputDto>();

            configuration.CreateMap<MacroIndicatorInputDto, MacroIndicator>();
            configuration.CreateMap<MacroIndicatorFilterDto, MacroIndicatorOutputDto>();
            configuration.CreateMap<MacroIndicator, MacroIndicatorOutputDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();
        }
        public static void ActivityMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<ActivityType, ActivityTypeOutputDto>();
            configuration.CreateMap< ActivityFullInputDto, Activity> ();
            configuration.CreateMap<ActivityInputDto, Activity>();
            configuration.CreateMap<Activity, ActivityOutputDto>()
            .ForMember(x => x.StatusName, option => option.MapFrom(src => src.Status.Name))
            .ForMember(x => x.TypeName, option => option.MapFrom(src => src.Type.Name))
            .ForMember(x => x.ModuleName, option => option.MapFrom(src => src.Module.Name))
            .ForMember(x => x.CreatorUserName, option => option.MapFrom(src => src.CreatorUser.Name))
            .ForMember(x => x.CreatorUserSurName, option => option.MapFrom(src => src.CreatorUser.Surname))
            .ForMember(x => x.LastModifierUserName, option => option.MapFrom(src => src.LastModifierUser.Name))
            .ForMember(x => x.LastModifierUserSurName, option => option.MapFrom(src => src.LastModifierUser.Surname))
            .ForMember(x => x.TaskStatusName, option => option.MapFrom(src => src.TaskStatus.Name))
            .ForMember(x => x.TaskPriorityName, option => option.MapFrom(src => src.TaskPriority.Name));
            configuration.CreateMap<ActivityAttendeeInputDto, ActivityAttendee>();
            configuration.CreateMap<ActivityAttendee, ActivityAttendeeOutputDto>()
              .ForMember(x => x.ModuleName, option => option.MapFrom(src => src.Module.Name))
              .ForMember(x => x.StatusName, option => option.MapFrom(src => src.Status.Name));
            configuration.CreateMap<ActivityReminderInputDto, ActivityReminder>();
            configuration.CreateMap<ActivityReminder, ActivityReminderOutputDto>()
                .ForMember(x => x.FormatName, option => option.MapFrom(src => src.Format.Name))
                .ForMember(x => x.TypeName, option => option.MapFrom(src => src.Type.Name)); ;

            configuration.CreateMap<ActivityUser, ActivityUserOutputDto>()
                  .ForMember(x => x.UserName, option => option.MapFrom(src => src.User.UserName));
            configuration.CreateMap<ActivityOrganizationUnit, ActivityOrganizationUnitOutputDto>()
                .ForMember(x=>x.OrganizationUnitName,option=> option.MapFrom(src=>src.OrganizationUnit.DisplayName));
            configuration.CreateMap<ActivityUserInputDto, ActivityUser>();
            configuration.CreateMap<ActivityOrganizationUnitInputDto, ActivityOrganizationUnit>();

        }
        public static void RequestMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<RequestFilterDto, RequestOutputDto>();
            configuration.CreateMap<RequestInputDto, Request>();
            configuration.CreateMap<Request, RequestOutputDto>();
            configuration.CreateMap<RequestUser, RequestUserOutputDto>()
                   .ForMember(x => x.User, option => option.MapFrom(src => src.User));
            configuration.CreateMap<RequestOrganizationUnit, RequestOrganizationUnitOutputDto>()
                  .ForMember(x => x.OrganizationUnit, option => option.MapFrom(src => src.OrganizationUnit));
            configuration.CreateMap<RequestUserInputDto, RequestUser>();
            configuration.CreateMap<RequestOrganizationUnitInputDto, RequestOrganizationUnit>();
        }
        public static void InquiryMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<InquiryFilterDto, InquiryOutputDto>()
                .ForMember(x => x.Address, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonAddress) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<InquiryAddressOutputDto>>(src.JsonAddress) : new List<InquiryAddressOutputDto>()))
                .ForMember(x => x.Facilities, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonFacility) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<InquiryFacilityMapOutputDto>>(src.JsonFacility) : new List<InquiryFacilityMapOutputDto>()))
                .ForMember(x => x.Users, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonView) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<InquiryUserOutputDto>>(src.JsonUser) : new List<InquiryUserOutputDto>()))
                .ForMember(x => x.Users, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUser) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<ListingUserOutputDto>>(src.JsonUser) : new List<ListingUserOutputDto>()));
        configuration.CreateMap<InquiryInputDto, Inquiry>();
            configuration.CreateMap<Inquiry, InquiryOutputDto>();
            configuration.CreateMap<InquiryCategory, InquiryCategoryOutputDto>();
            configuration.CreateMap<InquiryFacilityMapInputDto, InquiryFacilityMaps>();
            configuration.CreateMap<InquiryViewMapInputDto, InquiryViewMaps>();
            configuration.CreateMap<InquiryAddressInputDto, InquiryAddress>();
        }
        public static void OrganizationUnitMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();
            configuration.CreateMap<UserFilterDto, UserOutputDto>();
            configuration.CreateMap<UserFilterDto, UserDto>();
            //.ForMember(x => x.RoleNames, option => option.MapFrom(src => !string.IsNullOrEmpty(src.RoleNames) ? Newtonsoft.Json.JsonConvert.DeserializeObject (src.RoleNames) :new List<string>()));
        }
        public static void CommentMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CommentInputDto, Entities.Comment.Comment>();
            configuration.CreateMap<Entities.Comment.Comment, CommentOutputDto>();
            configuration.CreateMap<CommentSendEmailInputDto, CommentSendEmail>();
            configuration.CreateMap<CommentSendEmail, CommentSendEmailOutputDto>()
              .ForMember(x => x.UserName, option => option.MapFrom(src => src.User.UserName))
                   .ForMember(x => x.EmailAddress, option => option.MapFrom(src => src.User.EmailAddress));
        }
        public static void CampaignMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CampaignFilterDto, CampaignOutputDto>()
                .ForMember(x => x.CampaignTarget, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonTarget) ? Newtonsoft.Json.JsonConvert.DeserializeObject(src.JsonTarget) : new List<CampaignTargetOutputDto>()));
            configuration.CreateMap<CampaignInputDto, Entities.Campaign.Campaign>();
            configuration.CreateMap<Entities.Campaign.Campaign, CampaignOutputDto>();

            configuration.CreateMap<TargetFilterDto, TargetOutputDto>();
            configuration.CreateMap<TargetInputDto, Entities.TargetList.TargetList>();
            configuration.CreateMap<Entities.TargetList.TargetList, TargetOutputDto>();

            configuration.CreateMap<TargetContactFilterDto, TargetContactOutputDto>();
            configuration.CreateMap<TargetContactInputDto, Entities.TargetContact.TargetContact>();
            configuration.CreateMap<Entities.TargetContact.TargetContact, TargetContactOutputDto>();

            configuration.CreateMap<TargetRequestFilterDto, TargetRequestOutputDto>();
            configuration.CreateMap<TargetRequestInputDto, Entities.TargetRequest.TargetRequest>();
            configuration.CreateMap<Entities.TargetRequest.TargetRequest, TargetRequestOutputDto>();

            configuration.CreateMap<Entities.EmailHistory.EmailHistory, EmailHistoryOutputDto>()
                .ForMember(x => x.ContactName, option => option.MapFrom(src => src.Contact.ContactName))
                .ForMember(x => x.CampaignName, option => option.MapFrom(src => src.Campaign.Name));

        }
        public static void OpportunityMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Entities.Opportunity.Opportunity, OpportunityOutputDto>();
            configuration.CreateMap<OpportunityInputDto, Entities.Opportunity.Opportunity>();
            configuration.CreateMap<OpportunityFilterDto, OpportunityOutputDto>()
                .ForMember(x => x.AssetClass, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonAssetClass) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityAssetClassOutputDto>>(src.JsonAssetClass) : new List<OpportunityAssetClassOutputDto>()))
                .ForMember(x => x.Instruction, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonInstruction) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityInstructionOutputDto>>(src.JsonInstruction) : new List<OpportunityInstructionOutputDto>()))
                .ForMember(x => x.Property, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProperty) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityPropertyOutputDto>>(src.JsonProperty) : new List<OpportunityPropertyOutputDto>()))
                .ForMember(x => x.Project, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonProject) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityProjectOutputDto>>(src.JsonProject) : new List<OpportunityProjectOutputDto>()))
                .ForMember(x => x.OpportunityContact, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonContact) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityContactOutputDto>>(src.JsonContact) : new List<OpportunityContactOutputDto>()))
                .ForMember(x => x.OpportunityOrganizationUnit, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonOrganizationUnit) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityOrganizationUnitOutputDto>>(src.JsonOrganizationUnit) : new List<OpportunityOrganizationUnitOutputDto>()))
                .ForMember(x => x.OpportunityUser, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUser) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityUserOutputDto>>(src.JsonUser) : new List<OpportunityUserOutputDto>()))
                .ForMember(x => x.OpportunityLead, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonLead) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityLeadOutputDto>>(src.JsonLead) : new List<OpportunityLeadOutputDto>()));
            configuration.CreateMap<OpportunityCategory, OpportunityCategoryOutputDto>();
            configuration.CreateMap<OpportunityFullInputDto, Entities.Opportunity.Opportunity>();
            configuration.CreateMap<OpportunityPropertyInputDto, OpportunityProperty>();
            configuration.CreateMap<OpportunityProperty, OpportunityPropertyOutputDto>()
                .ForMember(x => x.PropertyName, option => option.MapFrom(src => src.Property.PropertyName));
            configuration.CreateMap<OpportunityProjectInputDto, OpportunityProject>();
            configuration.CreateMap<OpportunityProject, OpportunityProjectOutputDto>()
                .ForMember(x => x.ProjectName, option => option.MapFrom(src => src.Project.ProjectName));

            configuration.CreateMap<OpportunityUserInputDto, OpportunityUser>();
            configuration.CreateMap<OpportunityUser, OpportunityUserOutputDto>()
                 .ForMember(x => x.FullName, option => option.MapFrom(src => src.User.Surname + " " + src.User.Name))
                 .ForMember(x => x.SurName, option => option.MapFrom(src => src.User.Surname))
                 .ForMember(x => x.Name, option => option.MapFrom(src => src.User.Name));

            configuration.CreateMap<OpportunityLeadInputDto, OpportunityLead>();
            configuration.CreateMap<OpportunityLead, OpportunityLeadOutputDto>()
                 .ForMember(x => x.FullName, option => option.MapFrom(src => src.User.Surname + " " + src.User.Name))
                 .ForMember(x => x.SurName, option => option.MapFrom(src => src.User.Surname))
                 .ForMember(x => x.Name, option => option.MapFrom(src => src.User.Name));

            configuration.CreateMap<OpportunityOrganizationUnitInputDto, OpportunityOrganizationUnit>();
            configuration.CreateMap<OpportunityOrganizationUnit, OpportunityOrganizationUnitOutputDto>()
                .ForMember(x => x.OrganizationUnitName, option => option.MapFrom(src => src.OrganizationUnit.DisplayName));

            configuration.CreateMap<OpportunityAssetClassInputDto, OpportunityAssetClass>();
            configuration.CreateMap<OpportunityAssetClass, OpportunityAssetClassOutputDto>()
                 .ForMember(x => x.AssetClassName, option => option.MapFrom(src => src.AssetClass.Name));

            configuration.CreateMap<OpportunityInstructionInputDto, OpportunityInstruction>();
            configuration.CreateMap<OpportunityInstruction, OpportunityInstructionOutputDto>()
                 .ForMember(x => x.InstructionName, option => option.MapFrom(src => src.Instruction.Name));

            configuration.CreateMap<OpportunityContactInputDto, OpportunityContact>();
            configuration.CreateMap<OpportunityContact, OpportunityContactOutputDto>()
                .ForMember(x => x.ContactName, option => option.MapFrom(src => src.Contact.ContactName));
        }
        public static void DealMapping(IMapperConfigurationExpression configuration)
        {

            configuration.CreateMap<DealInputDto, Deal>();
            configuration.CreateMap<Deal, DealOutputDto>();
            configuration.CreateMap<DealFullInputDto, Deal>();
            configuration.CreateMap<DealFilterDto, DealOutputDto>()
                    .ForMember(x => x.DealOpportunity, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonOpportunity) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<DealOpportunityOutputDto>>(src.JsonOpportunity) : new List<DealOpportunityOutputDto>()))
                    .ForMember(x => x.DealAdjust, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonAdjust) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<DealAdjustOutputDto>>(src.JsonAdjust) : new List<DealAdjustOutputDto>()))
                    .ForMember(x => x.DealOrganizationUnit, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonOrganizationUnit) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<DealShareOutputDto>>(src.JsonOrganizationUnit) : new List<DealShareOutputDto>()))
                    .ForMember(x => x.DealUser, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonUser) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<DealShareOutputDto>>(src.JsonUser) : new List<DealShareOutputDto>()))
                    .ForMember(x => x.Payment, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonPayment) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentOutputDto>>(src.JsonPayment) : new List<PaymentOutputDto>()))
                    .ForMember(x => x.PaymentAdjust, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonPaymentAdjust) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentOutputDto>>(src.JsonPaymentAdjust) : new List<PaymentOutputDto>()));

            configuration.CreateMap<PaymentInputDto, Payment>();
            configuration.CreateMap<Payment, PaymentOutputDto>()
                .ForMember(x => x.StatusName, option => option.MapFrom(src => src.Status.Name));

            configuration.CreateMap<TrackingInvoiceInputDto, Payment>();
            configuration.CreateMap<TrackingInvoiceFilterDto, TrackingInvoiceOutputDto>()
              .ForMember(x => x.DealOrganizationUnit, option => option.MapFrom(src => !string.IsNullOrEmpty(src.JsonOrganizationUnit) ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<DealShareOutputDto>>(src.JsonOrganizationUnit) : new List<DealShareOutputDto>()));

            configuration.CreateMap<DealOpportunityInputDto, DealOpportunity>();
            configuration.CreateMap<DealOpportunity, DealOpportunityOutputDto>()
                .ForMember(x => x.OpportunityName, option => option.MapFrom(src => src.Opportunity.OpportunityName))
                  .ForMember(x => x.DealName, option => option.MapFrom(src => src.Deal.DealName));

            configuration.CreateMap<DealAdjustInputDto, DealAdjust>();
            configuration.CreateMap<DealAdjust, DealAdjustOutputDto>()
                .ForMember(x => x.OrganizationUnitName, option => option.MapFrom(src => src.OrganizationUnit.DisplayName));

            configuration.CreateMap<DealShareInputDto, DealShare>();
            configuration.CreateMap<DealShare, DealShareOutputDto>()
                 .ForMember(x => x.FullName, option => option.MapFrom(src => src.User.Surname + " " + src.User.Name))
                 .ForMember(x => x.SurName, option => option.MapFrom(src => src.User.Surname))
                 .ForMember(x => x.Name, option => option.MapFrom(src => src.User.Name))
                 .ForMember(x => x.OrganizationUnitName, option => option.MapFrom(src => src.OrganizationUnit.DisplayName));
        }
        public static void FinancialMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CapacityInputDto, Capacity>();
            configuration.CreateMap<Capacity, CapacityOutputDto>();
            configuration.CreateMap<CapacityFilterDto, CapacityOutputDto>();
        }
        public static void NewMapping(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<News, NewsEditOutputDto>();
            configuration.CreateMap<News, NewsOutputDto>();
            configuration.CreateMap<NewsCreateDto, News>();
            configuration.CreateMap<NewsUpdateDto, News>();
            configuration.CreateMap<NewsCategory, NewsCategoryOutputDto>();
        }
    }
}

