using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using CRM.Authorization;

namespace  CRM
{
    [DependsOn(
        typeof(SBICoreModule), 
        typeof(AbpAutoMapperModule)
        //typeof(AbpEntityFrameworkCoreModule),
        //typeof(AbpDapperModule)
        )]
    public class SBIApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<SBIAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(SBIApplicationModule).GetAssembly();
            IocManager.RegisterAssemblyByConvention(thisAssembly);
            //DapperExtensions.DapperExtensions.SetMappingAssemblies(new List<Assembly> { typeof(SBIApplicationModule).GetAssembly() });
            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.AttachmentMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.ProjectsMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.PropertiesMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CompanyMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CurrencyMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.ProjectProperyMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.ActivityMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.RequestMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.OrganizationUnitMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CommentMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.OpportunityMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.DealMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.UnitMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.ListingMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.InquiryMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CampaignMapping); 
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.FinancialMapping);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.NewMapping);
        }
    }
}
