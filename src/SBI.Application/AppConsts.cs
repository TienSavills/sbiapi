﻿namespace  CRM
{
    public class AppConsts
    {
        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public const string DefaultPassPhrase = "gsKxGZ012HLL3MI5";

        public const int MinValuePrimaryKey = 1;
        public const int MaxValueResearchType = 3;
        public const int MaxLimitSize = 52428800;
        public static class GeocodeValues
        {
            public const decimal MinLat = -90;
            public const decimal MaxLat = +90;
            public const decimal MinLng = -180;
            public const decimal MaxLng = +180;
            public const decimal InvalidLat = 181;
            public const decimal InvalidLng = 181;
            public const string StateComponent = "administrative_area_level_1";
            public const string CountryComponent = "country";
            public const string PoliticalComponent = "political";
        }
        public const string LinkCompany = "/company-detail/{0}?commentId={1}";
        public const string LinkContact = "/contact-detail/{0}?commentId={1}";
        public const string LinkOpportunity = "/opportunity-detail/{0}?commentId={1}";
        public const string LinkDeal = "/deal-contract-detail/{0}?commentId={1}";
        public const string LinkRequest = "/requirement-detail/{0}?commentId={1}";
        public const string LinkListing = "/Listing-detail/{0}?commentId={1}";
    }
}
