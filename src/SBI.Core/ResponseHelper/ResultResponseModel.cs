﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.ResponseHelper
{
    public class ResultResponseModel
    {
        [JsonProperty]
        public dynamic Result { get; set; }
        public string TargetUrl { get; set; }
        public bool Success { get; set; }
        public Error Error { get; set; }
        public bool UnAuthorizedRequest { get; set; }
    }
    public class Error
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
