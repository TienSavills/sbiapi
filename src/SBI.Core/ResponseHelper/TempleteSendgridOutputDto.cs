﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.ResponseHelper
{

    public class Version
    {
        public string Id { get; set; }
        public string Template_id { get; set; }
        public int Active { get; set; }
        public string Name { get; set; }
        public bool Generate_plain_content { get; set; }
        public string Subject { get; set; }
        public string Updated_at { get; set; }
        public string Editor { get; set; }
    }

    public class Template
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Generation { get; set; }
        public string Updated_at { get; set; }
        public List<Version> Versions { get; set; }
    }

    public class TempleteSendgridOutputDto
    {
        public List<Template> Templates { get; set; }
    }
}
