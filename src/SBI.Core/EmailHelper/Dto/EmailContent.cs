﻿namespace CRM.EmailHelper.Dto
{
    public class EmailContent
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
