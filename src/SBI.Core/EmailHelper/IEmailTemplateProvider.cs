﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EmailHelper
{
    public interface IEmailTemplateProvider : ITransientDependency
    {
        string GetEmailTemplate(string pathTemplate);
        string BindDataToTemplate(string bodyContent, Dictionary<string, string> values);
    }
}
