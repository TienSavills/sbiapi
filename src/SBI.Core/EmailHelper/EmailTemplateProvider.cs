﻿using Abp.IO.Extensions;
using Abp.Reflection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CRM.EmailHelper.Dto;
using System.Reflection;

namespace CRM.EmailHelper
{
    public class EmailTemplateProvider: IEmailTemplateProvider
    {
        private string _parameterPattern = "{{:{0}:}}";
        private string _pattern = @"\{:\w+:\}";
        public EmailTemplateProvider()
        {
        }
        public string GetEmailTemplate(string pathTemplate)
        {
            var names = Assembly.GetExecutingAssembly().GetManifestResourceNames();

            using (var stream = typeof(EmailTemplateProvider).GetAssembly().GetManifestResourceStream(pathTemplate))
            {
                var bytes = stream.GetAllBytes();
                var template = Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
                return template;
            }
        }

        public string BindDataToTemplate(string bodyContent, Dictionary<string, string> values)
        {
            if (values == null || !values.Any())
            {
                CleanContentEmptyInContentMesage(ref bodyContent);
                return bodyContent;
            }
            if (string.IsNullOrEmpty(bodyContent))
                return string.Empty;
            foreach (var parameter in values)
            {
                bodyContent = bodyContent.Replace(GenerateExpressionForParameter(parameter.Key), parameter.Value);
            }
            return bodyContent;
        }

        public EmailContent BindDataToTemplate(EmailContent emailContent, Dictionary<string, string> values)
        {
            foreach (var parameter in values)
            {
                emailContent.Subject = emailContent.Subject.Replace(GenerateExpressionForParameter(parameter.Key), parameter.Value);
                emailContent.Body = emailContent.Body.Replace(GenerateExpressionForParameter(parameter.Key), parameter.Value);
            }
            return emailContent;
        }

        public string GenerateExpressionForParameter(string name)
        {
            return string.Format(_parameterPattern, name.ToUpper());
        }

        public void CleanContentEmptyInEmailContent(ref EmailContent emailContent)
        {
            emailContent.Subject = RemoveContentEmpty(emailContent.Subject);
            emailContent.Body = RemoveContentEmpty(emailContent.Body);
        }

        public void CleanContentEmptyInContentMesage(ref string content)
        {
            content = RemoveContentEmpty(content);
        }

        public string RemoveContentEmpty(string content)
        {
            var rgx = new Regex(_pattern);
            return rgx.Replace(content, "");
        }
    }
}
