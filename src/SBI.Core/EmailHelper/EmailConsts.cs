﻿namespace CRM.EmailHelper
{
    public class EmailConsts
    {
        public class Opportunity
        {
            public const string DisplayName = "DISPLAYNAME";
            public const string Company = "OPP_COMPANY";
            public const string Contact = "OPP_CONTACT";
            public const string DateCreated = "OPP_CREATED";
            public const string Department = "OPP_DEPARTMENT";
            public const string Lead = "OPP_LEAD";
            public const string Assigned = "OPP_ASSIGNED";
            public const string Fee = "OPP_FEE";
            public const string Description = "OPP_DESCRIPTION";
            public const string OpportunityName = "OPPORTUNITY_NAME";
            public const string Status = "OPP_STATUS";
            public const string Link = "OPP_LINK";
        }
        public class ResetPass
        {
            public const string Link = "Link_Reset";
            public const string ResetCode = "Reset_Code";
        }
        public class WeeklyReportOpportunity
        {
            public const string DisplayName = "DISPLAYNAME";
            public const string FromToDate = "FROMTODATE";
            public const string LinkUserActivity = "OPP_LINKUSERACTIVITY";
            public const string LinkDepartment = "OPP_LINKDEPARTMENT";
        }
        public class Reminder
        {
            public const string DisplayName = "DISPLAYNAME";
            public const string TaskName = "TAKS_NAME";
            public const string Status = "TASK_STATUS";
            public const string Description = "TAKS_DESCRIPTION";
            public const string StartDate = "TASK_STARTDATE";
            public const string Link = "TASK_LINK";
        }
        public class Commment
        {
            public const string DisplayName = "DISPLAYNAME";
            public const string Name = "_NAME";
            public const string Contend = "_CONTEND";
            public const string Link = "_LINK";
        }
    }
}
