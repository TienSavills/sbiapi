﻿
using Abp.Dependency;
using System.Threading.Tasks;

namespace CRM.Authentication.Helper
{
    public interface IAuthenHelperManager : ITransientDependency
    {
        Task<bool> SendPasswordResetCode(CRM.Authorization.Users.SendPasswordResetCodeInput input);

    }
}
