﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using CRM.Authorization.Users;
using CRM.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Abp;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using CRM.Authentication.Helper.Dto;
using CRM.HttpClientHelper;
using Castle.Core.Logging;
using Newtonsoft.Json;
using System;

namespace CRM.Authentication.Helper
{
    public class AuthenHelperManager : AbpServiceBase, IAuthenHelperManager
    {
        private readonly string _authenicateRootAddress;
        private readonly IRepository<UserAccount, long> _repositoryuserAccount;
        private readonly string _verificationAccountUrl = "/api/services/app/Account/VerificationAccount";
        private readonly string _sendPasswordResetCode = "/SendPasswordResetCode";
        private readonly IAppConfigurationAccessor _appConfiguration;
        private readonly IHttpClientHelper _paymentHttpClientHelper;
        private readonly ILogger _logger;

        public AuthenHelperManager(
            IRepository<UserAccount, long> userAccountRepository,
           IHttpClientHelper paymentHttpClientHelper, ILogger logger,
           IAppConfigurationAccessor appConfiguration)
        {
            _authenicateRootAddress = appConfiguration.Configuration["App:AuthenicateRootAddress"];
            _repositoryuserAccount = userAccountRepository;
            _paymentHttpClientHelper = paymentHttpClientHelper;
            _logger = logger;
        }

        public Task<bool> SendPasswordResetCode(SendPasswordResetCodeInput input)
        {
            throw new NotImplementedException();
        }
    }
}
