﻿namespace CRM.Authentication.Helper.Dto
{
    public class VerifyAccountDto
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsNewMember { get; set; }
        public bool IsActive { get; set; }
        public string ReturnUrl { get; set; }
    }
}
