﻿using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;

namespace CRM.Authentication.Helper.Dto
{
    public class HasAccountInput
    {
        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}
