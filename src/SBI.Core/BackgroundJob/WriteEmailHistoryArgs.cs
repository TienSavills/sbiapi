﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.BackgroundJob
{
    [Serializable]
    public class WriteEmailHistoryArgs
    {
        public int? StatusCode { get; set; }
        public string ResultBody { get; set; }
        public string Module { get; set; }
        public string SentTo { get; set; }
        public string EmailSubject { get; set; }
        public string EmailContent { get; set; }
        public bool IsSuccess { get; set; }
        public long ContactId { get; set; }
        public long CampaignId { get; set; }
    }
}
