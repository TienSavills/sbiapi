﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.BackgroundJob
{
    public static class ReminderConstant
    {
        public const string days = "DAY";
        public const string hours = "HOURS";
        public const string minutes = "MINUTE";
        public const int Manager = 13;
    }
}
