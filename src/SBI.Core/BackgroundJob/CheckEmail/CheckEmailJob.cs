﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using CRM.Configuration;
using CRM.Email;
using CRM.Entities.ContactEmail;
using CRM.Entities.EmailHistory;
using System.Linq;

namespace CRM.BackgroundJob.CheckEmail
{
    public class CheckEmailJob : BackgroundJob<CheckEmailArgs>, ITransientDependency
    {
        private readonly IRepository<Entities.ContactEmail.ContactEmail, long> _repoContactEmail;
        private readonly IRepository<Entities.HistoryVerifyEmail.HistoryVerifyEmail, long> _repoHistoryVerifyEmail;
        private readonly IEmailSenderHelper _iEmailSenderHelper;
        private readonly IConfigHelper _configHelper;
        public CheckEmailJob(IRepository<ContactEmail, long> repoContactEmail
            , IRepository<Entities.HistoryVerifyEmail.HistoryVerifyEmail, long> repoHistoryVerifyEmail
            , IEmailSenderHelper iEmailSenderHelper
            , IConfigHelper configHelper)
        {
            _repoContactEmail = repoContactEmail;
            _iEmailSenderHelper = iEmailSenderHelper;
            _repoHistoryVerifyEmail = repoHistoryVerifyEmail;
            _configHelper = configHelper;
        }

        public override void Execute(CheckEmailArgs args)
        {
            _repoContactEmail.Update(args.Id, contactEmail =>
            {
                // chi lay email co code = 4 va 5
                var checkMail = _iEmailSenderHelper.CheckEmailAsync(args.Email).Result;
                if (checkMail.debounce.code != "0")
                {
                    contactEmail.IsCheckEmail = true;
                    contactEmail.Code = checkMail.debounce.code;
                    var transactional = checkMail.debounce.send_transactional;
                    contactEmail.IsInvalid = transactional == "0" ? true : false;
                    contactEmail.Reason = checkMail.debounce.reason;
                    contactEmail.Result = checkMail.debounce.result;
                    contactEmail.Role = checkMail.debounce.role;
                    contactEmail.SendTransactional = checkMail.debounce.send_transactional;
                    contactEmail.FeeEmail = checkMail.debounce.free_email;
                    _repoContactEmail.Update(contactEmail);
                    CurrentUnitOfWork.SaveChanges();
                    Logger.Info($" Update Check Email success { contactEmail.Id.ToString()}.");
                }
                else
                {
                    Logger.Error($"code=0");
                }
            });
        }
    }
}
