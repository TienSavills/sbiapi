﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.BackgroundJob.CheckEmail
{
    [Serializable]
    public class CheckEmailArgs
    {
        public long Id { get; set; }
        public string Email { get; set; }
    }
}
