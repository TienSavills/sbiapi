﻿using Abp.Authorization.Users;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using CRM.Authorization.Users;
using CRM.Configuration;
using CRM.Opportunity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRM.BackgroundJob
{
    public class CRMSendOpportunityWeekly : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly IRepository<Entities.Opportunity.Opportunity, long> _repoOpportunity;
        private readonly IExportOpportunity _exportOpportunity;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<User, long> _repoUser;
        private readonly IConfigHelper _configHelper;
        //private readonly IRepository<AbpOrgan _exportOpportunity;
        public CRMSendOpportunityWeekly(AbpTimer timer
             , IRepository<Entities.Opportunity.Opportunity, long> repoOpportunity
             , IExportOpportunity exportOpportunity
             , IRepository<User, long> repoUser
             , IConfigHelper configHelper
             , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
           )
           : base(timer)
        {
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _repoOpportunity = repoOpportunity;
            _exportOpportunity = exportOpportunity;
            _configHelper = configHelper;
            _repoUser = repoUser;
            Timer.Period = 1000 * 60 * 60 ; //1 tieng (good for tests, but normally will be more)
        }
        [UnitOfWork]
        protected override void DoWork()
        {
            try
            {
                DateTime utcTime = DateTime.Now.ToUniversalTime();
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);
                Logger.Error($"kiem tra BGJ LocalTime: {localTime}");
                Logger.Error($"kiem tra BGJ ServerTime: {utcTime}");
                Logger.Error($"kiem tra BGJ Hour: {localTime.Hour}");
                Logger.Error($"kiem tra BGJ DayOfWeek: {localTime.DayOfWeek}");
                if (localTime.DayOfWeek == DayOfWeek.Monday && localTime.Hour<=23
                && _configHelper.GetHostRequest().Contains(Common.SbiHost))
                {
                    var user = _repoUser.Get(10048);
                    //var userManagers = _userOrganizationUnitRepository.GetAll().Where(x => x.OrganizationUnitId == ReminderConstant.Manager).ToList();
                    //foreach (var userManager in userManagers)
                    //{
                    //    var user = _repoUser.Get(userManager.UserId);
                       _exportOpportunity.SendMailWeeklyReport(user.UserName, user.EmailAddress);
                    //}
                }
                //Logger.Error($"BGJ thu ba or tu Tien LocalTime: {localTime}");
                //Logger.Error($"BGJ DayOfWeek: {localTime.DayOfWeek}");
                //Logger.Error($"Get Host: { _configHelper.GetHostRequest()}");
                //if (localTime.DayOfWeek == DayOfWeek.Thursday && localTime.Hour == 17
                //&& _configHelper.GetHostRequest().Contains("http://sbi.savills.com.vn"))
                //{
                //    var user = _repoUser.Get(10048);
                //    _exportOpportunity.SendMailWeeklyReport(user.UserName, user.EmailAddress);
                //}
                //if (localTime.DayOfWeek == DayOfWeek.Wednesday && localTime.Hour < 6 && localTime.Hour >= 5)
                ////&& _configHelper.GetHostRequest().ToUpper().Contains("SBI.SAVILL.COM.VN"))
                //{
                //    var user = _repoUser.Get(10048);
                //    _exportOpportunity.SendMailWeeklyReport(user.UserName, user.EmailAddress);
                //}
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
            }
        }
    }
}
