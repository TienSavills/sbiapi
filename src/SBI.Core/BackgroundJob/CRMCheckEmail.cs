﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using CRM.BackgroundJob.CheckEmail;
using CRM.Configuration;
using CRM.Email;
using CRM.Entities.ContactEmail;
using CRM.Entities.HistoryVerifyEmail;
using CRM.Entities.Opportunity;
using CRM.Entities.OpportunityUser;
using Microsoft.EntityFrameworkCore;

namespace CRM.BackgroundJob
{
    public class CRMCheckEmail : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly IRepository<Entities.ContactEmail.ContactEmail, long> _repoContactEmail;
        private readonly IRepository<Entities.HistoryVerifyEmail.HistoryVerifyEmail, long> _repoHistoryVerifyEmail;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IEmailSenderHelper _iEmailSenderHelper;
        private readonly IConfigHelper _configHelper;
        public CRMCheckEmail(AbpTimer timer, IRepository<ContactEmail, long> repoContactEmail
            , IRepository<Entities.HistoryVerifyEmail.HistoryVerifyEmail, long> repoHistoryVerifyEmail
            , IEmailSenderHelper iEmailSenderHelper
            , IConfigHelper configHelper
            , IBackgroundJobManager backgroundJobManager
            , IAppConfigurationAccessor appConfiguration)
            : base(timer)
        {
            _repoContactEmail = repoContactEmail;
            _iEmailSenderHelper = iEmailSenderHelper;
            _repoHistoryVerifyEmail = repoHistoryVerifyEmail;
            _configHelper = configHelper;
            _backgroundJobManager = backgroundJobManager;
            //_repoOpportunityUser = repoOpportunityUser;
            Timer.Period = 1000 * 60 * 60 * 5; //20 phut 1 lan (good for tests, but normally will be more)
        }
        //public async Task ValidateEmail(int index)
        //{
        //    var count = await _repoContactEmail.CountAsync();
        //    if (index < count)
        //    {
        //        //todo: chedck val.....
        //         _backgroundJobManager.Enqueue<CheckEmailJob, CheckEmailArgs>(
        //               new CheckEmailArgs
        //               {
        //                   Id = contactEmail.Id,
        //                   Email = contactEmail.Email,
        //                   Index = index + 1
        //               });
        //    }
        //}
        [UnitOfWork]
        protected override void DoWork()
        {
            try
            {
                DateTime utcTime = DateTime.Now.ToUniversalTime();
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);
                Logger.Error($"local time={localTime.Day}");
                if (localTime.Day == 13)
                {
                    // chi lay email co code = 4 va 5
                    var contactEmail = _repoContactEmail.GetAll()
                .Where(x => x.Email.Contains("@") && x.IsActive && (x.IsCheckEmail == null || x.IsCheckEmail == false
                || new string[] { CheckMailConstant.CodeDeliverable, CheckMailConstant.CodeAcceptAll }.Contains(x.Code.ToString()))).ToList();
                    Logger.Error($"Sbi Host ={_configHelper.GetHostRequest()}");
                    if (_configHelper.GetHostRequest().Contains(Common.SbiHost))
                    {
                        contactEmail.ForEach(x =>
                        _backgroundJobManager.Enqueue<CheckEmailJob, CheckEmailArgs>(
                       new CheckEmailArgs
                       {
                           Id = x.Id,
                           Email = x.Email
                       }));
                    }
                    //contactEmail.ForEach(x =>
                    //{
                    //    var checkMail = _iEmailSenderHelper.CheckEmailAsync(x.Email).Result;
                    //    if (checkMail.debounce.code != "0")
                    //    {
                    //        x.IsCheckEmail = true;
                    //        x.Code = checkMail.debounce.code;
                    //        var transactional = checkMail.debounce.send_transactional;
                    //        x.IsInvalid = transactional == "0" ? true : false;
                    //        x.Reason = checkMail.debounce.reason;
                    //        x.Result = checkMail.debounce.result;
                    //        x.Role = checkMail.debounce.role;
                    //        x.SendTransactional = checkMail.debounce.send_transactional;
                    //        x.FeeEmail = checkMail.debounce.free_email;
                    //        _repoContactEmail.Update(x);
                    //        CurrentUnitOfWork.SaveChanges();
                    //        Logger.Info($"{DateTime.Now.ToShortDateString()} Update Check Email success { x.Id.ToString()}.");
                    //    }
                    //    else
                    //    {
                    //        Logger.Error($"code=0");
                    //    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
            }

        }
    }
}
