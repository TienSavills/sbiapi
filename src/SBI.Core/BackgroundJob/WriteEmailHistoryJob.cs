﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using CRM.Entities.EmailHistory;

namespace CRM.BackgroundJob
{
    public class WriteEmailHistoryJob : BackgroundJob<WriteEmailHistoryArgs>, ITransientDependency
    {
        private readonly IRepository<EmailHistory, long> _repositoryEmailHistory;
        public WriteEmailHistoryJob(IRepository<EmailHistory, long> repositoryEmailHistory)
        {
            _repositoryEmailHistory = repositoryEmailHistory;
        }
        public override void Execute(WriteEmailHistoryArgs args)
        {
            Logger.Info($"Call Successfully: Execute Write log email history job!_____________________________________________________");
            var obj = new EmailHistory()
            {
                CampaignId=args.CampaignId,
                ContactId=args.ContactId,
                SentTo = args.SentTo,
                EmailSubject = args.EmailSubject,
                EmailContent = args.EmailContent,
                Module = args.Module,
                StatusCode = args.StatusCode,
                IsSuccess = args.IsSuccess,
                ResultBody = args.ResultBody,
            };
            Logger.Info($"WriteLogSendEmail Successfully: {args.SentTo}.");
            _repositoryEmailHistory.Insert(obj);
        }
    }
}
