﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.BackgroundJob
{
    public static class CheckMailConstant
    {
        public const string Result = "Safe to Send";
        public const string CodeDeliverable = "5";
        public const string CodeAcceptAll = "4"; 
    }
    public static class Common
    {
        public const string SbiHost = "http://sbi.savills.com.vn";
    }
}
