﻿using System;
using System.Linq;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Extensions;
using Abp.Linq.Extensions;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using CRM.Email;
using CRM.Entities.ActivityUser;
using CRM.Entities.ContactEmail;
using CRM.Entities.Opportunity;
using CRM.Entities.OpportunityUser;
using Microsoft.EntityFrameworkCore;

namespace CRM.BackgroundJob
{
    public class CRMReminderWorker : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly IRepository<Entities.ActivityReminder.ActivityReminder, long> _repoActivityReminder;
        private readonly IRepository<Entities.Activity.Activity, long> _repoActivity;
        private readonly IEmailSenderHelper _emailEmailSenderHelper;
        private readonly IRepository<ActivityUser, long> _repoActivityUser;
        public CRMReminderWorker(AbpTimer timer, IRepository<Entities.ActivityReminder.ActivityReminder, long> repoActivityReminder
            , IRepository<Entities.ActivityType.ActivityType, int> repoActivityType
            , IRepository<Entities.Activity.Activity, long> repoActivity
            , IEmailSenderHelper emailEmailSenderHelper
            , IRepository<ActivityUser, long> repoActivityUser
            )
            : base(timer)
        {
            _repoActivityReminder = repoActivityReminder;
            _repoActivityReminder = repoActivityReminder;
            _repoActivity = repoActivity;
            _emailEmailSenderHelper = emailEmailSenderHelper;
            _repoActivityUser = repoActivityUser;
            Timer.Period = 1000 * 60 * 60 * 24; //5 phut (good for tests, but normally will be more)
        }

        [UnitOfWork]
        protected override void DoWork()
        {
            try
            {
                // reminder trong ngay va loai la phut
                var reminder = _repoActivityReminder.GetAll()
                    .Include(x => x.Type)
                    .Include(x => x.Activity).ThenInclude(y => y.Status)
                    .Include(x => x.Format)
                    .Where(x => x.Format.Name.ToUpper() == ReminderConstant.minutes && x.Activity.DateStart <= DateTime.Now.AddMinutes(x.Value)
                    || (x.Format.Name.ToUpper() == ReminderConstant.hours && x.Activity.DateStart <= DateTime.Now.AddHours(x.Value)
                    || (x.Format.Name.ToUpper() == ReminderConstant.days && x.Activity.DateStart <= DateTime.Now.AddDays(x.Value))))
                    .Where(x => x.Activity.DateStart != null && x.IsReminder != true).ToList();
                reminder.ForEach(x =>
                {
                    var userAssgin = _repoActivityUser.GetAll()
                    .Include(y => y.User)
                    .Where(y => y.ActivityId == x.ActivityId && x.IsActive == true).ToList();
                    userAssgin.ForEach(z =>
                    {
                        string log = z.User.UserName + z.User.EmailAddress.ToString() + x.Activity.DateStart.ToString() + x.Activity.Status.Name + x.Activity.ActivityName + x.Activity.Description + "";
                        Logger.Info($"before send reminder:{log}");
                        _emailEmailSenderHelper.SendReminderActity(z.User.UserName, z.User.EmailAddress.ToString(), x.Activity.DateStart.ToString(), x.Activity.Status.Name, x.Activity.ActivityName, x.Activity.Description, "");
                        Logger.Info($" send reminder");
                    });
                    x.IsReminder = true;
                    _repoActivityReminder.Update(x);
                });
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
            }
        }
    }
}
