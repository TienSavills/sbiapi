﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;
using System.Collections.Generic;

namespace CRM.Authorization
{
    public class SBIAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            var activity = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Activity) ?? context.CreatePermission(PermissionNames.Pages_Administration_Activity, L(PermissionNames.Pages_Administration_Activity));
            activity.CreateChild(PermissionNames.Pages_Administration_Activity_Create);
            activity.CreateChild(PermissionNames.Pages_Administration_Activity_Update);
            activity.CreateChild(PermissionNames.Pages_Administration_Activity_Delete);
            activity.CreateChild(PermissionNames.Pages_Administration_Activity_Detail);
            activity.CreateChild(PermissionNames.Pages_Administration_Activity_Read);

            var company  = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Company) ?? context.CreatePermission(PermissionNames.Pages_Administration_Company, L(PermissionNames.Pages_Administration_Company));
            company.CreateChild(PermissionNames.Pages_Administration_Company_Create);
            company.CreateChild(PermissionNames.Pages_Administration_Company_Update);
            company.CreateChild(PermissionNames.Pages_Administration_Company_Delete);
            company.CreateChild(PermissionNames.Pages_Administration_Company_Detail);
            company.CreateChild(PermissionNames.Pages_Administration_Company_Read);


            var contact = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Contact) ?? context.CreatePermission(PermissionNames.Pages_Administration_Contact, L(PermissionNames.Pages_Administration_Contact));
            contact.CreateChild(PermissionNames.Pages_Administration_Contact_Create);
            contact.CreateChild(PermissionNames.Pages_Administration_Contact_Update);
            contact.CreateChild(PermissionNames.Pages_Administration_Contact_Delete);
            contact.CreateChild(PermissionNames.Pages_Administration_Contact_Detail);
            contact.CreateChild(PermissionNames.Pages_Administration_Contact_Read);
            contact.CreateChild(PermissionNames.PagesAdministration_Contact_FullDetail);

            var opportunityAdvisory = context.GetPermissionOrNull(PermissionNames.Pages_Administration_OpportunityAdvisory) ?? context.CreatePermission(PermissionNames.Pages_Administration_OpportunityAdvisory, L(PermissionNames.Pages_Administration_OpportunityAdvisory));
            opportunityAdvisory.CreateChild(PermissionNames.Pages_Administration_OpportunityAdvisory_Create);
            opportunityAdvisory.CreateChild(PermissionNames.Pages_Administration_OpportunityAdvisory_Update);
            opportunityAdvisory.CreateChild(PermissionNames.Pages_Administration_OpportunityAdvisory_Delete);
            opportunityAdvisory.CreateChild(PermissionNames.Pages_Administration_OpportunityAdvisory_Detail);
            opportunityAdvisory.CreateChild(PermissionNames.Pages_Administration_OpportunityAdvisory_Read);

            var opportunityResidential = context.GetPermissionOrNull(PermissionNames.Pages_Administration_OpportunityResidential) ?? context.CreatePermission(PermissionNames.Pages_Administration_OpportunityResidential, L(PermissionNames.Pages_Administration_OpportunityResidential));
            opportunityResidential.CreateChild(PermissionNames.Pages_Administration_OpportunityResidential_Create);
            opportunityResidential.CreateChild(PermissionNames.Pages_Administration_OpportunityResidential_Update);
            opportunityResidential.CreateChild(PermissionNames.Pages_Administration_OpportunityResidential_Delete);
            opportunityResidential.CreateChild(PermissionNames.Pages_Administration_OpportunityResidential_Detail);
            opportunityResidential.CreateChild(PermissionNames.Pages_Administration_OpportunityResidential_Read);

            var opportunityCommercial = context.GetPermissionOrNull(PermissionNames.Pages_Administration_OpportunityCommercial) ?? context.CreatePermission(PermissionNames.Pages_Administration_OpportunityCommercial, L(PermissionNames.Pages_Administration_OpportunityCommercial));
            opportunityCommercial.CreateChild(PermissionNames.Pages_Administration_OpportunityCommercial_Create);
            opportunityCommercial.CreateChild(PermissionNames.Pages_Administration_OpportunityCommercial_Update);
            opportunityCommercial.CreateChild(PermissionNames.Pages_Administration_OpportunityCommercial_Delete);
            opportunityCommercial.CreateChild(PermissionNames.Pages_Administration_OpportunityCommercial_Detail);
            opportunityCommercial.CreateChild(PermissionNames.Pages_Administration_OpportunityCommercial_Read);

            var campaign = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Campaign) ?? context.CreatePermission(PermissionNames.Pages_Administration_Campaign, L(PermissionNames.Pages_Administration_Campaign));
            campaign.CreateChild(PermissionNames.Pages_Administration_Campaign_Create);
            campaign.CreateChild(PermissionNames.Pages_Administration_Campaign_Update);
            campaign.CreateChild(PermissionNames.Pages_Administration_Campaign_Delete);
            campaign.CreateChild(PermissionNames.Pages_Administration_Campaign_Detail);
            campaign.CreateChild(PermissionNames.Pages_Administration_Campaign_Read);

            var dealAdvisory = context.GetPermissionOrNull(PermissionNames.Pages_Administration_DealAdvisory) ?? context.CreatePermission(PermissionNames.Pages_Administration_DealAdvisory, L(PermissionNames.Pages_Administration_DealAdvisory));
            dealAdvisory.CreateChild(PermissionNames.Pages_Administration_DealAdvisory_Create);
            dealAdvisory.CreateChild(PermissionNames.Pages_Administration_DealAdvisory_Update);
            dealAdvisory.CreateChild(PermissionNames.Pages_Administration_DealAdvisory_Delete);
            dealAdvisory.CreateChild(PermissionNames.Pages_Administration_DealAdvisory_Detail);
            dealAdvisory.CreateChild(PermissionNames.Pages_Administration_DealAdvisory_Read);

            var dealResidential = context.GetPermissionOrNull(PermissionNames.Pages_Administration_DealResidential) ?? context.CreatePermission(PermissionNames.Pages_Administration_DealResidential, L(PermissionNames.Pages_Administration_DealResidential));
            dealResidential.CreateChild(PermissionNames.Pages_Administration_DealResidential_Create);
            dealResidential.CreateChild(PermissionNames.Pages_Administration_DealResidential_Update);
            dealResidential.CreateChild(PermissionNames.Pages_Administration_DealResidential_Delete);
            dealResidential.CreateChild(PermissionNames.Pages_Administration_DealResidential_Detail);
            dealResidential.CreateChild(PermissionNames.Pages_Administration_DealResidential_Read);

            var dealCommercial = context.GetPermissionOrNull(PermissionNames.Pages_Administration_DealCommercial) ?? context.CreatePermission(PermissionNames.Pages_Administration_DealCommercial, L(PermissionNames.Pages_Administration_DealCommercial));
            dealCommercial.CreateChild(PermissionNames.Pages_Administration_DealCommercial_Create);
            dealCommercial.CreateChild(PermissionNames.Pages_Administration_DealCommercial_Update);
            dealCommercial.CreateChild(PermissionNames.Pages_Administration_DealCommercial_Delete);
            dealCommercial.CreateChild(PermissionNames.Pages_Administration_DealCommercial_Detail);
            dealCommercial.CreateChild(PermissionNames.Pages_Administration_DealCommercial_Read);
            
            var project = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Project) ?? context.CreatePermission(PermissionNames.Pages_Administration_Project, L(PermissionNames.Pages_Administration_Project));
            project.CreateChild(PermissionNames.Pages_Administration_Project_Create);
            project.CreateChild(PermissionNames.Pages_Administration_Project_Update);
            project.CreateChild(PermissionNames.Pages_Administration_Project_Delete);
            project.CreateChild(PermissionNames.Pages_Administration_Project_Detail);
            project.CreateChild(PermissionNames.Pages_Administration_Project_Read);

            var projectResidential = context.GetPermissionOrNull(PermissionNames.Pages_Administration_ProjectResidential) ?? context.CreatePermission(PermissionNames.Pages_Administration_ProjectResidential, L(PermissionNames.Pages_Administration_ProjectResidential));
            projectResidential.CreateChild(PermissionNames.Pages_Administration_ProjectResidential_Create);
            projectResidential.CreateChild(PermissionNames.Pages_Administration_ProjectResidential_Update);
            projectResidential.CreateChild(PermissionNames.Pages_Administration_ProjectResidential_Delete);
            projectResidential.CreateChild(PermissionNames.Pages_Administration_ProjectResidential_Detail);
            projectResidential.CreateChild(PermissionNames.Pages_Administration_ProjectResidential_Read);

            var unit = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Unit) ?? context.CreatePermission(PermissionNames.Pages_Administration_Unit, L(PermissionNames.Pages_Administration_Unit));
            unit.CreateChild(PermissionNames.Pages_Administration_Unit_Create);
            unit.CreateChild(PermissionNames.Pages_Administration_Unit_Update);
            unit.CreateChild(PermissionNames.Pages_Administration_Unit_Delete);
            unit.CreateChild(PermissionNames.Pages_Administration_Unit_Detail);
            unit.CreateChild(PermissionNames.Pages_Administration_Unit_Read);

            var unitResidential = context.GetPermissionOrNull(PermissionNames.Pages_Administration_UnitResidential) ?? context.CreatePermission(PermissionNames.Pages_Administration_UnitResidential, L(PermissionNames.Pages_Administration_UnitResidential));
            unitResidential.CreateChild(PermissionNames.Pages_Administration_UnitResidential_Create);
            unitResidential.CreateChild(PermissionNames.Pages_Administration_UnitResidential_Update);
            unitResidential.CreateChild(PermissionNames.Pages_Administration_UnitResidential_Delete);
            unitResidential.CreateChild(PermissionNames.Pages_Administration_UnitResidential_Detail);
            unitResidential.CreateChild(PermissionNames.Pages_Administration_UnitResidential_Read);

            var listing = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Listing) ?? context.CreatePermission(PermissionNames.Pages_Administration_Listing, L(PermissionNames.Pages_Administration_Listing));
            listing.CreateChild(PermissionNames.Pages_Administration_Listing_Create);
            listing.CreateChild(PermissionNames.Pages_Administration_Listing_Update);
            listing.CreateChild(PermissionNames.Pages_Administration_Listing_Delete);
            listing.CreateChild(PermissionNames.Pages_Administration_Listing_Detail);
            listing.CreateChild(PermissionNames.Pages_Administration_Listing_Read);

            var inquiry = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Inquiry) ?? context.CreatePermission(PermissionNames.Pages_Administration_Inquiry, L(PermissionNames.Pages_Administration_Inquiry));
            inquiry.CreateChild(PermissionNames.Pages_Administration_Inquiry_Create);
            inquiry.CreateChild(PermissionNames.Pages_Administration_Inquiry_Update);
            inquiry.CreateChild(PermissionNames.Pages_Administration_Inquiry_Delete);
            inquiry.CreateChild(PermissionNames.Pages_Administration_Inquiry_Detail);
            inquiry.CreateChild(PermissionNames.Pages_Administration_Inquiry_Read);

            var news = context.GetPermissionOrNull(PermissionNames.Pages_Administration_News) ?? context.CreatePermission(PermissionNames.Pages_Administration_News, L(PermissionNames.Pages_Administration_News));
            news.CreateChild(PermissionNames.Pages_Administration_News_Create);
            news.CreateChild(PermissionNames.Pages_Administration_News_Update);
            news.CreateChild(PermissionNames.Pages_Administration_News_Delete);
            news.CreateChild(PermissionNames.Pages_Administration_News_Detail);
            news.CreateChild(PermissionNames.Pages_Administration_News_Read);

            var requirement = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Requirement) ?? context.CreatePermission(PermissionNames.Pages_Administration_Requirement, L(PermissionNames.Pages_Administration_Requirement));
            requirement.CreateChild(PermissionNames.Pages_Administration_Requirement_Create);
            requirement.CreateChild(PermissionNames.Pages_Administration_Requirement_Update);
            requirement.CreateChild(PermissionNames.Pages_Administration_Requirement_Delete);
            requirement.CreateChild(PermissionNames.Pages_Administration_Requirement_Detail);
            requirement.CreateChild(PermissionNames.Pages_Administration_Requirement_Read);

            var financial = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Financial) ?? context.CreatePermission(PermissionNames.Pages_Administration_Financial, L(PermissionNames.Pages_Administration_Financial));
            financial.CreateChild(PermissionNames.Pages_Administration_Financial_Create);
            financial.CreateChild(PermissionNames.Pages_Administration_Financial_Update);
            financial.CreateChild(PermissionNames.Pages_Administration_Financial_Delete);
            financial.CreateChild(PermissionNames.Pages_Administration_Financial_Detail);
            financial.CreateChild(PermissionNames.Pages_Administration_Financial_Read);

            var accountant = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Accountant) ?? context.CreatePermission(PermissionNames.Pages_Administration_Accountant, L(PermissionNames.Pages_Administration_Accountant));
            accountant.CreateChild(PermissionNames.Pages_Administration_Accountant_Create);
            accountant.CreateChild(PermissionNames.Pages_Administration_Accountant_Update);
            accountant.CreateChild(PermissionNames.Pages_Administration_Accountant_Delete);
            accountant.CreateChild(PermissionNames.Pages_Administration_Accountant_Detail);
            accountant.CreateChild(PermissionNames.Pages_Administration_Accountant_Read);

            //var administration = context.CreatePermission(PermissionNames.Pages_Administration, L(PermissionNames.Pages_Administration));

            var roles =  context.GetPermissionOrNull(PermissionNames.Pages_Administration_Role) ?? context.CreatePermission(PermissionNames.Pages_Administration_Role, L(PermissionNames.Pages_Administration_Role));
            roles.CreateChild(PermissionNames.Pages_Administration_Role_Create);
            roles.CreateChild(PermissionNames.Pages_Administration_Role_Update);
            roles.CreateChild(PermissionNames.Pages_Administration_Role_Delete);
            roles.CreateChild(PermissionNames.Pages_Administration_Role_Read);
            roles.CreateChild(PermissionNames.Pages_Administration_Role_Detail);

            var user = context.GetPermissionOrNull(PermissionNames.Pages_Administration_User) ?? context.CreatePermission(PermissionNames.Pages_Administration_User, L(PermissionNames.Pages_Administration_User));
            user.CreateChild(PermissionNames.Pages_Administration_User_Create);
            user.CreateChild(PermissionNames.Pages_Administration_User_Update);
            user.CreateChild(PermissionNames.Pages_Administration_User_Delete);
            user.CreateChild(PermissionNames.Pages_Administration_User_Read);
            user.CreateChild(PermissionNames.Pages_Administration_User_Detail);


            var report = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Report) ?? context.CreatePermission(PermissionNames.Pages_Administration_Report, L(PermissionNames.Pages_Administration_Report));
            report.CreateChild(PermissionNames.Pages_Administration_Report_Opportunity);
            report.CreateChild(PermissionNames.Pages_Administration_Report_Department);
            report.CreateChild(PermissionNames.Pages_Administration_Report_Commercial);
            report.CreateChild(PermissionNames.Pages_Administration_Report_RevenueForecast);
            report.CreateChild(PermissionNames.Pages_Administration_Report_MMM);
            report.CreateChild(PermissionNames.Pages_Administration_Map);

            var language = context.GetPermissionOrNull(PermissionNames.Pages_Administration_Languages) ?? context.CreatePermission(PermissionNames.Pages_Administration_Languages, L(PermissionNames.Pages_Administration_Languages));
            language.CreateChild(PermissionNames.Pages_Administration_Languages_Create);
            language.CreateChild(PermissionNames.Pages_Administration_Languages_Read);
            language.CreateChild(PermissionNames.Pages_Administration_Languages_Update);
            language.CreateChild(PermissionNames.Pages_Administration_Languages_Delete);
            language.CreateChild(PermissionNames.Pages_Administration_Languages_ChangeTexts);

        //administration.CreateChild(PermissionNames.Pages_Administration_AuditLogs);

        //var organizationUnits = administration.CreateChild(PermissionNames.Pages_Administration_OrganizationUnits);
        //organizationUnits.CreateChild(PermissionNames.Pages_Administration_OrganizationUnits_ManageOrganizationTree);
        //organizationUnits.CreateChild(PermissionNames.Pages_Administration_OrganizationUnits_ManageMembers);

        //administration.CreateChild(PermissionNames.Pages_Administration_UiCustomization);

    }
        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SBIConsts.LocalizationSourceName);
        }
    }
    internal static class AppAuthExtension
    {
        public static Permission CreateChild(this Permission permission, string name, MultiTenancySides multiTenancySides = MultiTenancySides.Tenant | MultiTenancySides.Host)
        {
            return permission.CreateChildPermission(name, L(name), null, multiTenancySides);
        }

        public static void CreateChilds(this Permission permission, List<string> names, MultiTenancySides multiTenancySides = MultiTenancySides.Tenant | MultiTenancySides.Host)
        {
            names.ForEach(name => permission.CreateChild(name, multiTenancySides));
        }
        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SBIConsts.LocalizationSourceName);
        }
    }
}
