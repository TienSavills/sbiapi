﻿using System.DirectoryServices.AccountManagement;
using System.Threading.Tasks;
using Abp.Zero.Ldap.Configuration;

namespace CRM.Authorization.Ldap
{
    public class MyLdapSettings : ILdapSettings
    {
        public async Task<string> GetContainer(int? tenantId)
        {
            return "savills.com.vn";
        }

        public async Task<ContextType> GetContextType(int? tenantId)
        {
            return ContextType.Domain;
        }

        public async Task<string> GetDomain(int? tenantId)
        {
            return "savills.com.vn";
        }

        public async Task<bool> GetIsEnabled(int? tenantId)
        {
            return true;
        }

        public async Task<string> GetPassword(int? tenantId)
        {
            return "Truc123456";
        }

        public async Task<string> GetUserName(int? tenantId)
        {
            return "tcongtien@savills.com.vn";
        }
    }
}
