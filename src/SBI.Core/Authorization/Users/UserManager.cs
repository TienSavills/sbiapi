﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Organizations;
using Abp.Runtime.Caching;
using CRM.Authorization.Roles;
using System.Threading.Tasks;
using CRM.Authorization.Users.Dto;
using System.Linq;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;

namespace CRM.Authorization.Users
{
    public class UserManager : AbpUserManager<Role, User>

    {
        private readonly IObjectMapper _objectMapper;
        private readonly IRepository<User, long> _repositoryUser;
        public UserManager(
            RoleManager roleManager,
            UserStore store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<UserManager<User>> logger,
            IPermissionManager permissionManager,
            IUnitOfWorkManager unitOfWorkManager,
            ICacheManager cacheManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IOrganizationUnitSettings organizationUnitSettings,
            ISettingManager settingManager,
            IRepository<User, long> repositoryUser,
            IObjectMapper objectMapper)
            : base(
                roleManager,
                store,
                optionsAccessor,
                passwordHasher,
                userValidators,
                passwordValidators,
                keyNormalizer,
                errors,
                services,
                logger,
                permissionManager,
                unitOfWorkManager,
                cacheManager,
                organizationUnitRepository,
                userOrganizationUnitRepository,
                organizationUnitSettings,
                settingManager)
        {
            _objectMapper = objectMapper;
            repositoryUser = _repositoryUser;
        }
        public async Task AppendMember<TModel>(Action<TModel, UserSimpleDto> repare, Func<TModel, long?> select, params TModel[] models)
        {
            var userId = models.Select(select).ToList();
            var entities = await _repositoryUser.GetAll().Where(x => userId.Contains(x.Id)).ToListAsync();

            if (models.Any())
            {
                foreach (var model in models)
                {
                    var entity = entities.FirstOrDefault(x => select.Invoke(model) == x.Id);
                    repare.Invoke(model, entity != null ? _objectMapper.Map<UserSimpleDto>(entity) : null);
                }
            }
        }
        public async Task AppendUser<TModel>(Action<TModel, UserSimpleDto> repare, Func<TModel, long?> select, params TModel[] models)
        {
            var userId = models.Select(select).ToList();
            var entities = await _repositoryUser.GetAll().Where(x => userId.Contains(x.Id)).ToListAsync();

            if (models.Any())
            {
                foreach (var model in models)
                {
                    var entity = entities.FirstOrDefault(x => select.Invoke(model) == x.Id);
                    repare.Invoke(model, entity != null ? _objectMapper.Map<UserSimpleDto>(entity) : null);
                }
            }
        }
    }
}
