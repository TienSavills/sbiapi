﻿using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;

namespace CRM.Authorization.Users
{
    public class SendPasswordResetCodeInput
    {
        [Required]
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}