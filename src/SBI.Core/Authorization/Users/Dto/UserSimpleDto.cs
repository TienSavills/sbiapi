﻿namespace CRM.Authorization.Users.Dto
{
    public class UserSimpleDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string FileUrl { get; set; }
    }
}
