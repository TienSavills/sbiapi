﻿namespace CRM.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Administration_Dashboard = "PagesAdministration.Dashboard";
        public const string Pages_Administration_Company = "PagesAdministration.Company";
        public const string Pages_Administration_Company_Create = "PagesAdministration.Company.Create";
        public const string Pages_Administration_Company_Read = "PagesAdministration.Company.Read";
        public const string Pages_Administration_Company_Update = "PagesAdministration.Company.Update";
        public const string Pages_Administration_Company_Delete = "PagesAdministration.Company.Delete";
        public const string Pages_Administration_Company_Detail = "PagesAdministration.Company.Detail";

        public const string Pages_Administration_Contact = "PagesAdministration.Contact";
        public const string Pages_Administration_Contact_Create = "PagesAdministration.Contact.Create";
        public const string Pages_Administration_Contact_Read = "PagesAdministration.Contact.Read";
        public const string Pages_Administration_Contact_Update = "PagesAdministration.Contact.Update";
        public const string Pages_Administration_Contact_Delete = "PagesAdministration.Contact.Delete";
        public const string Pages_Administration_Contact_Detail = "PagesAdministration.Contact.Detail";
        public const string PagesAdministration_Contact_FullDetail = "PagesAdministration.Contact.FullDetail";

        public const string Pages_Administration_OpportunityAdvisory = "PagesAdministration.OpportunityAdvisory";
        public const string Pages_Administration_OpportunityAdvisory_Create = "PagesAdministration.OpportunityAdvisory.Create";
        public const string Pages_Administration_OpportunityAdvisory_Read = "PagesAdministration.OpportunityAdvisory.Read";
        public const string Pages_Administration_OpportunityAdvisory_Update = "PagesAdministration.OpportunityAdvisory.Update";
        public const string Pages_Administration_OpportunityAdvisory_Delete = "PagesAdministration.OpportunityAdvisory.Delete";
        public const string Pages_Administration_OpportunityAdvisory_Detail = "PagesAdministration.OpportunityAdvisory.Detail";



        public const string Pages_Administration_OpportunityCommercial = "PagesAdministration.OpportunityCommercial";
        public const string Pages_Administration_OpportunityCommercial_Create = "PagesAdministration.OpportunityCommercial.Create";
        public const string Pages_Administration_OpportunityCommercial_Read = "PagesAdministration.OpportunityCommercial.Read";
        public const string Pages_Administration_OpportunityCommercial_Update = "PagesAdministration.OpportunityCommercial.Update";
        public const string Pages_Administration_OpportunityCommercial_Delete = "PagesAdministration.OpportunityCommercial.Delete";
        public const string Pages_Administration_OpportunityCommercial_Detail = "PagesAdministration.OpportunityCommercial.Detail";


        public const string Pages_Administration_OpportunityResidential = "PagesAdministration.OpportunityResidential";
        public const string Pages_Administration_OpportunityResidential_Create = "PagesAdministration.OpportunityResidential.Create";
        public const string Pages_Administration_OpportunityResidential_Read = "PagesAdministration.OpportunityResidential.Read";
        public const string Pages_Administration_OpportunityResidential_Update = "PagesAdministration.OpportunityResidential.Update";
        public const string Pages_Administration_OpportunityResidential_Delete = "PagesAdministration.OpportunityResidential.Delete";
        public const string Pages_Administration_OpportunityResidential_Detail = "PagesAdministration.OpportunityResidential.Detail";

        public const string Pages_Administration_Campaign = "PagesAdministration.Campaign";
        public const string Pages_Administration_Campaign_Create = "PagesAdministration.Campaign.Create";
        public const string Pages_Administration_Campaign_Read = "PagesAdministration.Campaign.Read";
        public const string Pages_Administration_Campaign_Update = "PagesAdministration.Campaign.Update";
        public const string Pages_Administration_Campaign_Delete = "PagesAdministration.Campaign.Delete";
        public const string Pages_Administration_Campaign_Detail = "PagesAdministration.Campaign.Detail";


        public const string Pages_Administration_DealAdvisory = "PagesAdministration.DealAdvisory";
        public const string Pages_Administration_DealAdvisory_Create = "PagesAdministration.DealAdvisory.Create";
        public const string Pages_Administration_DealAdvisory_Read = "PagesAdministration.DealAdvisory.Read";
        public const string Pages_Administration_DealAdvisory_Update = "PagesAdministration.DealAdvisory.Update";
        public const string Pages_Administration_DealAdvisory_Delete = "PagesAdministration.DealAdvisory.Delete";
        public const string Pages_Administration_DealAdvisory_Detail = "PagesAdministration.DealAdvisory.Detail";

        public const string Pages_Administration_DealCommercial = "PagesAdministration.DealCommercial";
        public const string Pages_Administration_DealCommercial_Create = "PagesAdministration.DealCommercial.Create";
        public const string Pages_Administration_DealCommercial_Read = "PagesAdministration.DealCommercial.Read";
        public const string Pages_Administration_DealCommercial_Update = "PagesAdministration.DealCommercial.Update";
        public const string Pages_Administration_DealCommercial_Delete = "PagesAdministration.DealCommercial.Delete";
        public const string Pages_Administration_DealCommercial_Detail = "PagesAdministration.DealCommercial.Detail";

        public const string Pages_Administration_DealResidential = "PagesAdministration.DealResidential";
        public const string Pages_Administration_DealResidential_Create = "PagesAdministration.DealResidential.Create";
        public const string Pages_Administration_DealResidential_Read = "PagesAdministration.DealResidential.Read";
        public const string Pages_Administration_DealResidential_Update = "PagesAdministration.DealResidential.Update";
        public const string Pages_Administration_DealResidential_Delete = "PagesAdministration.DealResidential.Delete";
        public const string Pages_Administration_DealResidential_Detail = "PagesAdministration.DealResidential.Detail";

        public const string Pages_Administration_Project = "PagesAdministration.Project";
        public const string Pages_Administration_Project_Create = "PagesAdministration.Project.Create";
        public const string Pages_Administration_Project_Read = "PagesAdministration.Project.Read";
        public const string Pages_Administration_Project_Update = "PagesAdministration.Project.Update";
        public const string Pages_Administration_Project_Delete = "PagesAdministration.Project.Delete";
        public const string Pages_Administration_Project_Detail = "PagesAdministration.Project.Detail";

        public const string Pages_Administration_ProjectResidential = "PagesAdministration.ProjectResidential";
        public const string Pages_Administration_ProjectResidential_Create = "PagesAdministration.ProjectResidential.Create";
        public const string Pages_Administration_ProjectResidential_Read = "PagesAdministration.ProjectResidential.Read";
        public const string Pages_Administration_ProjectResidential_Update = "PagesAdministration.ProjectResidential.Update";
        public const string Pages_Administration_ProjectResidential_Delete = "PagesAdministration.ProjectResidential.Delete";
        public const string Pages_Administration_ProjectResidential_Detail = "PagesAdministration.ProjectResidential.Detail";

        public const string Pages_Administration_Unit = "PagesAdministration.Unit";
        public const string Pages_Administration_Unit_Create = "PagesAdministration.Unit.Create";
        public const string Pages_Administration_Unit_Read = "PagesAdministration.Unit.Read";
        public const string Pages_Administration_Unit_Update = "PagesAdministration.Unit.Update";
        public const string Pages_Administration_Unit_Delete = "PagesAdministration.Unit.Delete";
        public const string Pages_Administration_Unit_Detail = "PagesAdministration.Unit.Detail";

        public const string Pages_Administration_UnitResidential = "PagesAdministration.UnitResidential";
        public const string Pages_Administration_UnitResidential_Create = "PagesAdministration.UnitResidential.Create";
        public const string Pages_Administration_UnitResidential_Read = "PagesAdministration.UnitResidential.Read";
        public const string Pages_Administration_UnitResidential_Update = "PagesAdministration.UnitResidential.Update";
        public const string Pages_Administration_UnitResidential_Delete = "PagesAdministration.UnitResidential.Delete";
        public const string Pages_Administration_UnitResidential_Detail = "PagesAdministration.UnitResidential.Detail";

        public const string Pages_Administration_Requirement = "PagesAdministration.Requirement";
        public const string Pages_Administration_Requirement_Create = "PagesAdministration.Requirement.Create";
        public const string Pages_Administration_Requirement_Read = "PagesAdministration.Requirement.Read";
        public const string Pages_Administration_Requirement_Update = "PagesAdministration.Requirement.Update";
        public const string Pages_Administration_Requirement_Delete = "PagesAdministration.Requirement.Delete";
        public const string Pages_Administration_Requirement_Detail = "PagesAdministration.Requirement.Detail";

        public const string Pages_Administration_Activity = "PagesAdministration.Activity";
        public const string Pages_Administration_Activity_Create = "PagesAdministration.Activity.Create";
        public const string Pages_Administration_Activity_Read = "PagesAdministration.Activity.Read";
        public const string Pages_Administration_Activity_Update = "PagesAdministration.Activity.Update";
        public const string Pages_Administration_Activity_Delete = "PagesAdministration.Activity.Delete";
        public const string Pages_Administration_Activity_Detail = "PagesAdministration.Activity.Detail";

        public const string Pages_Administration_Financial = "PagesAdministration.Financial";
        public const string Pages_Administration_Financial_Create = "PagesAdministration.Financial.Create";
        public const string Pages_Administration_Financial_Read = "PagesAdministration.Financial.Read";
        public const string Pages_Administration_Financial_Update = "PagesAdministration.Financial.Update";
        public const string Pages_Administration_Financial_Delete = "PagesAdministration.Financial.Delete";
        public const string Pages_Administration_Financial_Detail = "PagesAdministration.Financial.Detail";

        public const string Pages_Administration_Accountant = "PagesAdministration.Accountant";
        public const string Pages_Administration_Accountant_Create = "PagesAdministration.Accountant.Create";
        public const string Pages_Administration_Accountant_Read = "PagesAdministration.Accountant.Read";
        public const string Pages_Administration_Accountant_Update = "PagesAdministration.Accountant.Update";
        public const string Pages_Administration_Accountant_Delete = "PagesAdministration.Accountant.Delete";
        public const string Pages_Administration_Accountant_Detail = "PagesAdministration.Accountant.Detail";

        public const string Pages_Administration_Languages = "PagesAdministration.Languages";
        public const string Pages_Administration_Languages_Create = "PagesAdministration.Languages.Create";
        public const string Pages_Administration_Languages_Read = "PagesAdministration.Languages.Read";
        public const string Pages_Administration_Languages_Update = "PagesAdministration.Languages.Update";
        public const string Pages_Administration_Languages_Delete = "PagesAdministration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "PagesAdministration.Languages.ChangeTexts";

        public const string Pages_Administration_Role = "PagesAdministration.Role";
        public const string Pages_Administration_Role_Create = "PagesAdministration.Role.Create";
        public const string Pages_Administration_Role_Read = "PagesAdministration.Role.Read";
        public const string Pages_Administration_Role_Update = "PagesAdministration.Role.Update";
        public const string Pages_Administration_Role_Delete = "PagesAdministration.Role.Delete";
        public const string Pages_Administration_Role_Detail = "PagesAdministration.Role.Detail";

        public const string Pages_Administration_User = "PagesAdministration.User";
        public const string Pages_Administration_User_Create = "PagesAdministration.User.Create";
        public const string Pages_Administration_User_Read = "PagesAdministration.User.Read";
        public const string Pages_Administration_User_Update = "PagesAdministration.User.Update";
        public const string Pages_Administration_User_Delete = "PagesAdministration.User.Delete";
        public const string Pages_Administration_User_Detail = "PagesAdministration.User.Detail";

        public const string Pages_Administration_Report = "PagesAdministration.Report";
        public const string Pages_Administration_Report_Opportunity = "PagesAdministration.Report.Opportunity";
        public const string Pages_Administration_Report_Department = "PagesAdministration.Report.Department";
        public const string Pages_Administration_Report_Commercial = "PagesAdministration.Report.Commercial";
        public const string Pages_Administration_Report_RevenueForecast = "PagesAdministration.Report.RevenueForecast";
        public const string Pages_Administration_Report_MMM = "PagesAdministration.Report.MMM";
        public const string Pages_Administration_Map = "PagesAdministration.Map";

        public const string Pages_Administration_News           = "PagesAdministration.News";
        public const string Pages_Administration_News_Create    = "PagesAdministration.News.Create";
        public const string Pages_Administration_News_Read      = "PagesAdministration.News.Read";
        public const string Pages_Administration_News_Update    = "PagesAdministration.News.Update";
        public const string Pages_Administration_News_Delete    = "PagesAdministration.News.Delete";
        public const string Pages_Administration_News_Detail    = "PagesAdministration.News.Detail";

        public const string Pages_Administration_Listing        = "PagesAdministration.Listing";
        public const string Pages_Administration_Listing_Create = "PagesAdministration.Listing.Create";
        public const string Pages_Administration_Listing_Read   = "PagesAdministration.Listing.Read";
        public const string Pages_Administration_Listing_Update = "PagesAdministration.Listing.Update";
        public const string Pages_Administration_Listing_Delete = "PagesAdministration.Listing.Delete";
        public const string Pages_Administration_Listing_Detail = "PagesAdministration.Listing.Detail";

        public const string Pages_Administration_Inquiry        = "PagesAdministration.Inquiry";
        public const string Pages_Administration_Inquiry_Create = "PagesAdministration.Inquiry.Create";
        public const string Pages_Administration_Inquiry_Read   = "PagesAdministration.Inquiry.Read";
        public const string Pages_Administration_Inquiry_Update = "PagesAdministration.Inquiry.Update";
        public const string Pages_Administration_Inquiry_Delete = "PagesAdministration.Inquiry.Delete";
        public const string Pages_Administration_Inquiry_Detail = "PagesAdministration.Inquiry.Detail";
    }
}
