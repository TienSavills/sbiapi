﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Opportunity
{
    public interface IExportOpportunity : ITransientDependency
    {
        Task<bool> SendMailWeeklyReport(string displayName, string emailAddress);
    }
}
