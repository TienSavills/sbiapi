﻿using Abp.AppFactory.Interfaces;
using Abp.AppFactory.SendGrid.Email;
using Abp.Domain.Repositories;
using AutoMapper;
using CRM.Configuration;
using CRM.Email;
using CRM.EmailHelper;
using CRM.EntitiesCustom;
using CRM.ExportHelper;
using CRM.SBIMap;
using Dapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Opportunity
{
    public class ExportOpportunity : IExportOpportunity
    {
        private readonly IConfigHelper _configHelper;
        private readonly ISendGrid _sendGrid;
        private readonly IEmailSenderHelper _emailSenderHelper;
        private ConfigSetting _options;
        private readonly IRepository<Entities.Opportunity.Opportunity, long> _repoOpportunity;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IExportHelper _exportHelper;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        public ExportOpportunity(IConfigHelper configHelper
            , IEmailSenderHelper emailSenderHelper
             , IOptions<ConfigSetting> options
             , IHostingEnvironment hostingEnvironment
             , IRepository<Entities.Opportunity.Opportunity, long> repoOpportunity
             , IExportHelper exportHelper
             , ISendGrid sendGrid
             , IEmailTemplateProvider emailTemplateProvider)
        {
            _sendGrid=sendGrid;
            _configHelper = configHelper;
            _options = options.Value;
            _exportHelper = exportHelper;
            _repoOpportunity = repoOpportunity;
            _hostingEnvironment = hostingEnvironment;
            _emailTemplateProvider = emailTemplateProvider;
            _emailSenderHelper = emailSenderHelper;
        }
        public async Task<bool> SendMailWeeklyReport(string displayName, string emailAddress )
        {
            var subject = "Weekly report";
            var emailContent = new StringBuilder(_emailTemplateProvider.GetEmailTemplate($"CRM.EmailTemplate.OpportunityWeeklyReport.html"));
            DateTime dateFrom = DateTime.Now.AddDays(-7);
            DateTime dateTo = DateTime.Now.AddDays(-1);
            var FromToDate = string.Format("From {0} to {1}", new object[] { dateFrom.ToString("dd/MM/yyyy"), dateTo.ToString("dd/MM/yyyy") });
            var host = _configHelper.GetServerRoot();
            var linkDepartment = string.Format(host + "/reports/opportunity-departments?dateFrom={0}&dateTo={1}", new object[] { dateFrom.ToShortDateString(), dateTo.ToShortDateString() });
            var linkUserActivity = string.Format(host + "/reports/user-activity?dateFrom={0}&dateTo={1}", new object[] { dateFrom.ToShortDateString(), dateTo.ToShortDateString() });
            var dataBindings = new Dictionary<string, string>() {
                  {EmailConsts.WeeklyReportOpportunity.DisplayName, displayName},
                        {EmailConsts.WeeklyReportOpportunity.FromToDate, FromToDate},
                      {EmailConsts.WeeklyReportOpportunity.LinkDepartment, linkDepartment},
                      {EmailConsts.WeeklyReportOpportunity.LinkUserActivity, linkUserActivity},
            };
            //List<Attachment> listAtt = new List<Attachment>();
            //listAtt.Add(GenerateOpportunityFile(DateTime.Now).Result);
            var bodyHtmlContent = _emailTemplateProvider.BindDataToTemplate(emailContent.ToString(), dataBindings);
            var response = await _sendGrid.SendAsync(new SendGridEmail()
            {
                SenderName = "CRM Notify",
                SenderEmailAddress = "info@sadec.co",
                SubjectContent = subject,
                BodyHtmlContent = bodyHtmlContent,
                RecepientEmailAddress = emailAddress,
            });
            //await _emailSenderHelper.SendAsyncWithAttachments("info@sadec.co"
            //    , emailAddress
            //    , subject
            //    , bodyHtmlContent
            //    , null, true);
            return response.StatusCode == HttpStatusCode.Accepted ? true : false;
        }
        private Attachment GenerateOpportunityFile(long userId, DateTime? filterDate)
        {
            using (var conn = new SqlConnection(_options.SBICon))
            {
                var output = conn.Query<ExportOpportunityOutput>("SpExportOpportunity",
                   new
                   {
                       @UserId = userId
                       ,
                       @FilterDate = filterDate

                   }, commandType: CommandType.StoredProcedure);
                var dt = GenarateColumnOpportunity();
                var bidingData = GenarateDataOpportunity(dt, output.AsList());
                var filePath = string.Format($"{ _hostingEnvironment.ContentRootPath} { @"\CRM.EmailTemplate\ExportOpportunity.xlsx"}");
                var wbContent = _exportHelper.GetWorkbookContent(dt, 1, 2);
                var memoryFile = _exportHelper.ExportWorkbookFile(wbContent, filePath);
                var attachment = new Attachment()
                {
                    Content = Convert.ToBase64String(memoryFile.ToArray()),
                    Filename = "ReportOpportunity.xls",
                    Type = "application/octet-stream"
                };
                return attachment;
            }
        }
        //export excel
        private DataTable GenarateColumnOpportunity()
        {
            var dt = new DataTable
            {
                Columns =
                {
                    new DataColumn("No",typeof (string)),
                    new DataColumn("OpportunityName",typeof (string)),
                    new DataColumn("Status",typeof (string)),
                    new DataColumn("Department",typeof (string)),
                    new DataColumn("Assigned",typeof (string)),
                    new DataColumn("LeadBy",typeof (string)),
                    new DataColumn("CreationTime",typeof (string)),
                    new DataColumn("LastModificationTime",typeof (string)),
                }
            };
            return dt;
        }


        private DataTable GenarateDataOpportunity(DataTable dt, List<ExportOpportunityOutput> data)
        {
            var no = 1;
            data.ForEach(x =>
            {
                var r = dt.NewRow();
                foreach (DataColumn c in dt.Columns)
                {
                    try
                    {
                        switch (c.ColumnName)
                        {
                            case "No":
                                r[c.ColumnName] = no;
                                continue;
                            case "OpportunityName":
                                r[c.ColumnName] = x.OpportunityName.ToString();
                                continue;
                            case "Status":
                                r[c.ColumnName] = x.Status;
                                continue;
                            case "Department":
                                r[c.ColumnName] = x.Department;
                                continue;
                            case "LeadBy":
                                r[c.ColumnName] = x.LeadBy;
                                continue;
                            case "Assigned":
                                r[c.ColumnName] = x.Assigned;
                                continue;
                            case "CreationTime":
                                r[c.ColumnName] = x.CreationTime.ToString();
                                continue;
                            case "Company":
                                r[c.ColumnName] = x.Company.ToString();
                                continue;
                            case "Probability":
                                r[c.ColumnName] = x.Probability;
                                continue;
                            case "Amount":
                                r[c.ColumnName] = x.Amount.ToString();
                                continue;
                        }
                    }
                    catch { continue; }
                }
                dt.Rows.Add(r);
                no++;
            });
            return dt;
        }
    }
}
