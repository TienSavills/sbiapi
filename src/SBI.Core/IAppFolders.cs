﻿namespace CRM
{ 
    public interface IAppFolders
    {
        string TempFileDownloadFolder { get; set; }

        string SampleProfileImagesFolder { get; set; }

        string WebLogsFolder { get; set; }
    }
}