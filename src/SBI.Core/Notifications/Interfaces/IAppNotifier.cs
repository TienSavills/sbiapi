﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using Abp.Dependency;
using Abp.Notifications;
using CRM.Authorization.Users;

namespace CRM.Notifications.Interfaces
{
    public interface IAppNotifier : ITransientDependency
    {

        Task WelcomeToTheApplicationAsync(User user);
        Task SendNofifyUpdateStatusInvoice(User user);
        Task SendMessageAsync(List<UserIdentifier> user, string message, NotificationSeverity severity = NotificationSeverity.Info);
    }
}
