﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Notifications
{
    public static class AppNotificationNames
    {
        public const string WelcomeToTheApplication = "App.WelcomeToTheApplication";
        public const string UpdateInvoice = "App.UpdateInvoice";
        public const string NewUserRegistered = "App.NewUserRegistered";
        public const string NewTenantRegistered = "App.NewTenantRegistered";
        public const string GdprDataPrepared = "App.GdprDataPrepared";
        public const string TenantsMovedToEdition = "App.TenantsMovedToEdition";
        public const string DownloadInvalidImported = "App.DownloadInvalidImported";
        public const string ImportSuccessMessage = "App.ImportedSuccessfully";
    }
}
