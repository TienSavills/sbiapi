using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Notifications;
using CRM.Authorization.Users;
using CRM.Notifications.Interfaces;


namespace CRM.Notifications
{
    public class AppNotifier : SbiDomainServiceBase, IAppNotifier
    {
        private readonly INotificationPublisher _notificationPublisher;

        public AppNotifier(INotificationPublisher notificationPublisher)
        {
            _notificationPublisher = notificationPublisher;
        }

        public Task WelcomeToTheApplicationAsync(User user)
        {
            return _notificationPublisher.PublishAsync(
                 AppNotificationNames.WelcomeToTheApplication,
                 new MessageNotificationData(L("WelcomeToTheApplicationNotificationMessage")),
                 severity: NotificationSeverity.Success,
                 userIds: new[] { user.ToUserIdentifier() }
                 );
        }
        public Task SendNofifyUpdateStatusInvoice(User user)
        {
            return _notificationPublisher.PublishAsync(
                AppNotificationNames.UpdateInvoice,
                new MessageNotificationData(L("TheAccountantHasUpdatedTheInvoiceStatus")),
                severity: NotificationSeverity.Success,
                userIds: new[] { user.ToUserIdentifier() }
                );
        }

        //This is for test purposes
        public Task SendMessageAsync(List<UserIdentifier> user, string message, NotificationSeverity severity = NotificationSeverity.Info)
        {
            return _notificationPublisher.PublishAsync(
                "App.SimpleMessage",
                new MessageNotificationData(L(message)),
                severity: severity,
                userIds: user.ToArray()
                );
        }
      
    }
}