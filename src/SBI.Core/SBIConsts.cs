﻿namespace  CRM
{
    public class SBIConsts
    {
        public const string LocalizationSourceName = "SBI";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const string DefaultTempalte = "CRM.Core.EmailTemplate.OpportunityChanged.html";
        public const string LocalizationWebMainMenu = "WebMainMenu";
        public const string LocalizationWebError = "WebError";
        public const string LocalizationWebNotification = "WebNotification";
        public const string LocalizationWebLabel = "WebLabel";
        public const string LocalizationWebCategory = "WebCategory";

        public const string LocalizationMobileResident = "MobileResident";
        public const string LocalizationMobileAdmin = "MobileAdmin";

        public const string LocalizationServiceRoles = "ServiceRoles";
        public const string LocalizationServiceError = "ServiceError";
        public const string LocalizationServiceNotification = "ServiceNotification";
    }
}
