﻿using Abp.Dependency;

namespace CRM.System
{
    public class AppConfiguration : ISingletonDependency
    {
        public string ServerRootAddress { get; set; }
        public string ClientRootAddress { get; set; }
    }
}
