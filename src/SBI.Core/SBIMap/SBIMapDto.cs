﻿namespace CRM.SBIMap
{

    public class SBIMapOutputDto
    { 
        public string ProjectName { get; set; }
        public string GeoCode { get; set; }
        public string FileUrl { get; set; }
        public string Description { get; set; }
    }

}