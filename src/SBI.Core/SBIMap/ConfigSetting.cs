﻿namespace CRM.SBIMap
{
    public class ConfigSetting
    {
        public string SBIMap { get; set; }
        public string FileSetting { get; set; }
        public string SBICon { get; set; }
    }

}