﻿
using CRM.Entities.Contacts;

namespace CRM.EntitiesCustom
{
    public class ExistContactDto 
    {
        public int Id { get; set; }
        public string Level { get; set; }
        public string CongtactName { get; set; }
        public string Gender { get; set; }
        public string BusinessName { get; set; }
        public string LegalName { get; set; }
        public string PrimaryPhone { get; set; }
        public string PrimaryEmail { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public bool IsInvalid { get; set; }
        public string Requestor { get; set; }
    }
}
