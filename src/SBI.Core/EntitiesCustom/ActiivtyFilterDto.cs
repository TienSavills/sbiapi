﻿using CRM.Entities.Activity;

namespace CRM.EntitiesCustom
{
    public class ActiivtyFilterDto:Activity, ITotalCount
    {
        public string Module { get; set; }
        public string Status { get; set; }
        public string LastModifierUserName { get; set; }
        public string CreatorUserName { get; set; }
        public int TotalCount { get; set; }
    }
}
