﻿using CRM.Entities.MacroIndicators;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class MacroIndicatorFilterDto: MacroIndicator, ITotalCount
    {
        public string CountryName { get; set; }
        public string DistrictName { get; set; }
	    public string ProvinceName { get; set; }
        public int TotalCount { get; set; }
    }
}
