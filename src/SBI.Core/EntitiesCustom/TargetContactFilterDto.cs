﻿using CRM.Entities.TargetContact;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class TargetContactFilterDto : TargetContact, ITotalCount
    {
        public string ContactName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string Title { get; set; }
        public string Gender { get; set; }
        public int TotalCount { get; set; }
    }
}
