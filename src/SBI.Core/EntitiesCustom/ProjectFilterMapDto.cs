﻿using CRM.Entities.Projects;

namespace CRM.EntitiesCustom
{
    public class ProjectFilterMapDto : Project, ITotalCount
    {
        public string JsonProjectFacilityMap { get; set; }
        public string JsonProjectTypeMap { get; set; }
        public string JsonProjectGrade { get; set; }
        public string ThumbImage { get; set; }
        public string StateName { get; set; }
        public string LandlordName { get; set; }
        public string PropertyManagementName { get; set; }
        public string ContactName { get; set; }
        public string CountryName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int TotalCount { get; set; }
    }
}
