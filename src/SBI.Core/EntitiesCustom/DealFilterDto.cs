﻿using CRM.Entities.Deal;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class DealFilterDto : Deal, ITotalCount
    {
        public string StatusName { get; set; }
        public string ContactName { get; set; }
        public string BusinessName { get; set; }
        public string LegalName { get; set; }
        public long? Revenue { get; set; }
        public long? Paid { get; set; }
        public long? Outstanding { get; set; }
        public long? Forecast { get; set; }
        public string OrganizationUnitName { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierName { get; set; }
        public string LastModifierUserName { get; set; }
        public string JsonOrganizationUnit { get; set; }
        public string JsonPayment { get; set; }
        public string JsonUser { get; set; }
        public string JsonOpportunity { get; set; }
        public string JsonAdjust { get; set; }
        public string JsonPaymentAdjust { get; set; }
        public int TotalCount { get; set; }
    }
}
