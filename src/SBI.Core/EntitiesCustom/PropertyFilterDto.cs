﻿using CRM.Entities.Properties;

namespace CRM.EntitiesCustom
{
    public class PropertyFilterDto : Property, ITotalCount
    {
        public string JsonPropertyAddress { get; set; }
       // public string JsonPropertyFacilityMap { get; set; }
        //public string JsonPropertyTransportationMap { get; set; }
        //public string JsonPropertyTypeMap { get; set; }
        //public string JsonPropertyTenant { get; set; }
        //public string JsonPropertyLandlord { get; set; }
        //public string JsonPropertyGrade { get; set; }
        //public string UrlMainPhoto { get; set; }
        //public string StateName { get; set; }
        public string ProjectName { get; set; }
        public string RequestType { get; set; }
        public string Status { get; set; }
        public string Currency { get; set; }

        //public string TenureTypeName { get; set; }
        public int TotalCount { get; set; }
    }
}
