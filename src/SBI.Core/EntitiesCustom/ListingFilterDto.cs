﻿using CRM.Entities.Unit;
using System;
using System.Collections.Generic;
using System.Text;
using CRM.Entities.Listings;

namespace CRM.EntitiesCustom
{
    public class ListingFilterDto : Listings, ITotalCount
    {
        public string ProjectName { get; set; }
        public string FloorName { get; set; }
        public string UnitName { get; set; }
        public string StatusName { get; set; }
        public string TypeName { get; set; }
        public string UrlMainPhoto { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public string JsonUser { get; set; }
        public int TotalCount { get; set; }
    }
}
