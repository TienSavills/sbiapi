﻿using CRM.Entities.TargetRequest;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
   public  class TargetRequestFilterDto:TargetRequest, ITotalCount
    {
        public long ContactId { get; set; }
        public string ContactName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string Title { get; set; }
        public string Gender { get; set; }
        public int TotalCount { get; set; }
    }
}
