﻿using CRM.Entities.Unit;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class UnitFilterDto : Unit, ITotalCount
    {
        public long ProjectId { get; set; }
        public string FloorName { get;set;}
        public string ProjectName { get; set; }
        public string StatusName { get; set; }
        public string UnitTypeName { get; set; }
        public string Color { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public DateTime? StartDate { get; set; }
        public long? OrgTenantId { get; set; }
        public string OrgTenantBusinessName { get; set; }
        public string OrgTenantLegalName { get; set; }
        public string CreatorUserSurname { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserSurname { get; set; }
        public string LastModifierUserName { get; set; }
        public string ExpiredType { get; set; }
        public string JsonUnitHistory { get; set; }
        public int TotalCount { get; set; }
    }
}
