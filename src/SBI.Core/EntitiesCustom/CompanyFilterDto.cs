﻿
using CRM.Entities.Company;
using System;

namespace CRM.EntitiesCustom
{
    public class CompanyFilterDto : Company, ITotalCount
    {
        public string JsonCompanyAddress{ get; set; }
        public string JsonCompanyPhone { get; set; }
        public string JsonCompanyType { get; set; }
        public string JsonOrganizationUnit { get; set; }
        public string JsonUser { get; set; }
        public string LeadSourceName { get; set; }
        public string ClientTypeName { get; set; }
        public string IndustryName { get; set; }
        public string IndustryLevel2Name { get; set; }
        public string KeyClientManagement { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public string ParentBusinessName { get; set; }
        public string ParentLegalName { get; set; }
        public string NationalityName { get; set; }
        public int TotalCount { get; set; }
    }
}
