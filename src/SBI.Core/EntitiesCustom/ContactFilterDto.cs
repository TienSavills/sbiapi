﻿
using CRM.Entities.Contacts;

namespace CRM.EntitiesCustom
{
    public class ContactFilterDto : Contact, ITotalCount
    {
        public string JsonContactAddress{ get; set; }
        public string JsonContactPhone { get; set; }
        public string JsonContactEmail { get; set; }
        public string JsonContactCompany { get; set; }
        public string JsonContactType { get; set; }
        public string JsonUser { get; set; }
        public string JsonOrganizationUnit { get; set; }
        public string ContactTypeName { get; set; }
        public string Level { get; set; }
        public string BusinessName { get; set; }
        public string LegalName { get; set; }
        public string Industry { get; set; }
        public string LeadSource { get; set; }
        public string NationalityName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public bool IsInvalid { get; set; }
        public int TotalCount { get; set; }
    }
}
