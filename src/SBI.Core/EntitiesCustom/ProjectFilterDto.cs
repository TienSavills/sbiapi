﻿using CRM.Entities.Projects;

namespace CRM.EntitiesCustom
{
    public class ProjectFilterDto : Project, ITotalCount
    {
        public string JsonProjectAddress { get; set; }
        public string JsonProjectFacilityMap { get; set; }
        public string JsonProjectTransportationMap { get; set; }
        public string JsonProjectTypeMap { get; set; }
        public string JsonProjectTenant { get; set; }
        public string JsonProjectLandlord { get; set; }
        public string JsonProjectGrade { get; set; }
        public string UrlMainPhoto { get; set; }
        public string StateName { get; set; }
        public string TenureTypeName { get; set; }
        public string LandlordName { get; set; }
        public string PropertyManagementName { get; set; }
        public string ContactName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public int TotalCount { get; set; }
    }
}
