﻿using CRM.Entities.Financial;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class CapacityFilterDto : Capacity, ITotalCount
    {
        public string OrganizationUnitName { get; set; }
        public int TotalCount { get; set; }
    }
}
