﻿
using CRM.Authorization.Users;
using CRM.Entities.Contacts;

namespace CRM.EntitiesCustom
{
    public class UserFilterDto : User, ITotalCount
    {
        public string RoleNames { get; set; }
        public int TotalCount { get; set; }
    }
}
