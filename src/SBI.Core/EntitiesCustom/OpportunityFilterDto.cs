﻿using CRM.EntitiesCustom;
using CRM.Entities.Opportunity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class OpportunityFilterDto : Entities.Opportunity.Opportunity, ITotalCount
    {
        public string SourceName { get; set; }
        public decimal? PercentCompleted { get; set; }
        public string ClientTypeName { get; set; }
        public string StatusName { get; set; }
        public string IndustryName { get; set; }
        public string IndustryLevel2Name { get; set; }
        public string OrganizationUnitName { get; set; }
        public string PrimaryInstructionName { get; set; }
        public string PrimaryAssetClassName { get; set; }
        public string BusinessName { get; set; }
        public string StageName { get; set; }
        public string LegalName { get; set; }
        public string CurrencyName { get; set; }
        public string CompanyName { get; set; }
        public long? DealId { get; set; }
        public string DealName { get; set; }
        public string UnitName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string ContractNumber { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierName { get; set; }
        public string LastModifierUserName { get; set; }
        public string JsonAssetClass { get; set; }
        public string JsonInstruction { get; set; }
        public string JsonOrganizationUnit { get; set; }
        public string JsonUser { get; set; }
        public string JsonLead { get; set; }
        public string JsonContact { get; set; }
        public string JsonProperty { get; set; }
        public string JsonProject { get; set; }
        public int TotalCount { get; set; }
    }
}
