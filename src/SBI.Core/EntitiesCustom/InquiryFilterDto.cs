﻿using CRM.Entities.Unit;
using System;
using System.Collections.Generic;
using System.Text;
using CRM.Entities.Inquiries;
using CRM.Entities.Listings;

namespace CRM.EntitiesCustom
{
    public class InquiryFilterDto : Inquiry, ITotalCount
    {
        public string TypeName { get; set; }
        public string ClientName { get; set; }
        public string StatusName { get; set; }
        public string FacingName { get; set; }
        public string SouceName { get; set; }
        public string CreatorUserName { get; set; }
        public string LastModifierUserName { get; set; }
        public string JsonFacility { get; set; }
        public string JsonView { get; set; }
        public string JsonAddress { get; set; }
        public string JsonUser { get; set; }
        public int TotalCount { get; set; }
    }
}
