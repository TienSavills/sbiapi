﻿using CRM.Entities.Campaign;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class CampaignFilterDto:Campaign, ITotalCount
    {
        public string StatusName { get; set; }
        public string TypeName { get; set; }
        public string CreatorSurname { get; set; }
        public string CreatorName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierName { get; set; }
        public string JsonTarget { get; set; }
        public int TotalCount { get; set; }
    }
}
