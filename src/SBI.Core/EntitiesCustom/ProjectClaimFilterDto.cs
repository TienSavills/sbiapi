﻿using CRM.Entities.ProjectClaims;
using CRM.Entities.Projects;
using System;

namespace CRM.EntitiesCustom
{
    public class ProjectClaimFilterDto : ProjectClaim, ITotalCount
    {
        public string ProjectName { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string StateName { get; set; }
        public string ProjectStateName { get; set; }
        public string Address { get; set; }
        public double? USD { get; set; }
        public double? CurrentUSD { get; set; }
        public string JsonProjectTenant { get; set; }
        public string JsonProjectGrade { get; set; }
        public string JsonProjectClaimRoomType { get; set; }
        public int TotalCount { get; set; }
    }
}
