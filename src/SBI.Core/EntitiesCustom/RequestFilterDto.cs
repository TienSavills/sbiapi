﻿using CRM.EntitiesCustom;
using CRM.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class RequestFilterDto : Request, ITotalCount
    {
        public string RequestTypeName { get; set; }
        public string StatusName { get; set; }
        public string RequirementTypeName { get; set; }
        public string TenantBusinessName { get; set; }
        public string TenantLegalName { get; set; }
        public string GradeName { get; set; }
        public string SourceName { get; set; }
        public string CountryName { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string LastModifierUserName { get; set; }
        public string CreatorUserName { get; set; }
        public int TotalCount { get; set; }
    }
}
