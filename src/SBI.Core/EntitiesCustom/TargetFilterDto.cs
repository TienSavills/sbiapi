﻿using CRM.Entities.Campaign;
using CRM.Entities.TargetList;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class TargetFilterDto:TargetList, ITotalCount
    {
        public string CreatorSurname { get; set; }
        public string CreatorName { get; set; }
        public string LastModifierSurname { get; set; }
        public string LastModifierName { get; set; }
        public int TotalCount { get; set; }
    }
}
