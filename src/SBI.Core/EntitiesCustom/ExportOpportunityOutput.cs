﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.EntitiesCustom
{
    public class ExportOpportunityOutput
    {
        public long OpportunityId { get; set; }
        public string OpportunityName { get; set; }
        public string Status { get; set; }
        public string Department { get; set; }
        public string Company { get; set; }
        public string Contact { get; set; }
        public string Description { get; set; }
        public long? Amount { get; set; }
        public string LeadBy { get; set; }
        public string Assigned { get; set; }
        public string Probability { get; set; }
        public string Link { get; set; }
        public string CreatorUserName { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
