﻿using CRM.Entities.ProjectClaims;
using CRM.Entities.Projects;
using CRM.Entities.PropertyClaims;
using System;

namespace CRM.EntitiesCustom
{
    public class PropertyClaimFilterDto : PropertyClaim, ITotalCount
    {
        public string PropertyName { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string StateName { get; set; }
        public DateTime? LaunchingTime { get; set; }
        public int TotalCount { get; set; }
    }
}
