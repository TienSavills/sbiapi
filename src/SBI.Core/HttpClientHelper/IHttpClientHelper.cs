﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Dependency;
using CRM.ResponseHelper;

namespace CRM.HttpClientHelper
{
    public interface IHttpClientHelper : ITransientDependency
    {
        Task<string> GetFile(string queryString);
        Task<ResultApiModel> GetAsync(string url);
        Task<TempleteSendgridOutputDto> GetTempleteAsync(string url,string token);
        Task<string> PostXmlAsync(string url, string xmlString);
        Task<ResultApiModel> PostAsync(string url, string data);
        Task<ResultResponseModel> ApiPostAsync(string url, string data);
        Task<ResultApiModel> PutAsync(string url, string data);
    }
}
