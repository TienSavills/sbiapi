﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Newtonsoft.Json;
using CRM.ResponseHelper;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace CRM.HttpClientHelper
{
    public class HttpClientHelper : IHttpClientHelper
    {
        private static readonly HttpClient _httpClient = new HttpClient();
        private readonly ILogger _logger;
        public HttpClientHelper(ILogger logger)
        {
            _logger = logger;
        }
        public async Task<string> GetFile(string queryString)
        {
            // The actual Get method
            using (var response = await _httpClient.GetAsync($"{queryString}"))
            {
                using (var content = response.Content)
                {
                    var result = await content.ReadAsStringAsync();
                    return result;
                }
            }
        }

        public async Task<ResultApiModel> GetAsync(string url)
        {
            try
            {
                using (var result = await _httpClient.GetAsync(url))
                {
                    string content = await result.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResultApiModel>(content);
                }
            }
            catch (Exception)
            {
                return new ResultApiModel()
                {
                };
            }
        }
        public async Task<TempleteSendgridOutputDto> GetTempleteAsync(string url,string token)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await client.GetAsync(url);
                    string content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TempleteSendgridOutputDto>(content);
                }
            }
            catch (Exception ex)
            {
                return new TempleteSendgridOutputDto()
                {
                };
            }
        }
        public async Task<string> PostXmlAsync(string url, string xmlString)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(xmlString, Encoding.UTF8, "application/xml"); ;

                var response = await client.PostAsync(url, content);

                return await response.Content.ReadAsStringAsync();
            }
        }
        public async Task<ResultApiModel> PostAsync(string url, string data)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var body = new StringContent(data, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, body);
                    string resultContent = await result.Content.ReadAsStringAsync();
                    _logger.Info($"Send request to: {url}, Data reponse: {data}, Result:{JsonConvert.SerializeObject(resultContent)}");
                    var resultApi = JsonConvert.DeserializeObject<ResultApiModel>(resultContent);
                    return resultApi;
                }
            }
            catch (Exception ex)
            {
                _logger.Info($"Send request to: {url}, Data: {data}, Result:{ex.Message}");
                return new ResultApiModel()
                {

                };
            }
        }

        public async Task<ResultResponseModel> ApiPostAsync(string url, string data)
        {

            try
            {
                using (var client = new HttpClient())
                {
                    var body = new StringContent(data, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, body);
                    string resultContent = await result.Content.ReadAsStringAsync();
                    _logger.Info($"Send request to: {url}, Data reponse: {data}, Result:{JsonConvert.SerializeObject(resultContent)}");
                    var resultApi = JsonConvert.DeserializeObject<ResultResponseModel>(resultContent);
                    return resultApi;
                }
            }
            catch (Exception ex)
            {
                _logger.Info($"Send request to: {url}, Data: {data}, Result:{ex.Message}");
                return new ResultResponseModel()
                {

                };
            }
        }
        public async Task<ResultApiModel> PutAsync(string url, string data)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var body = new StringContent(data, Encoding.UTF8, "application/json");
                    var result = await client.PutAsync(url, body);
                    string resultContent = await result.Content.ReadAsStringAsync();
                    _logger.Info($"Send request to: {url}, Data: {data}, Result:{JsonConvert.SerializeObject(resultContent)}");
                    var resultApi = JsonConvert.DeserializeObject<ResultApiModel>(resultContent);
                    return resultApi;
                }
            }
            catch (Exception ex)
            {
                _logger.Info($"Send request to: {url}, Data: {data}, Result:{ex.Message}");
                return new ResultApiModel()
                {

                };
            }
        }

    }
}
