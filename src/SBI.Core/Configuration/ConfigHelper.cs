﻿using Castle.Core.Logging;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Configuration
{
    public class ConfigHelper : IConfigHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string _hostBase;
        private readonly string _fileServerRootUrl;
        private readonly string _downloadFileByGuid;
        private readonly string _downloadFileById;
        private readonly string _downloadThumById;
        private readonly string _linkDashboard;
        public ILogger Logger { get; set; }
        public ConfigHelper(IAppConfigurationAccessor appConfiguration, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _hostBase = appConfiguration.Configuration["App:FileServerRootAddress"];
            _downloadFileByGuid = appConfiguration.Configuration["UrlConfig:DownloadFileByGuid"];
            _downloadFileById = appConfiguration.Configuration["UrlConfig:DownloadFileById"];
            _downloadThumById = appConfiguration.Configuration["UrlConfig:DownloadThumById"];
            _fileServerRootUrl = appConfiguration.Configuration["App:FileServerRootAddress"];
            _linkDashboard = appConfiguration.Configuration["UrlConfig:LinkDashboard"];
            Logger = NullLogger.Instance;
        }

        public string GetHostRequest()
        {
            return _hostBase;
        }
        public string GetConfigUrlDownloadFileByGuid(Guid? guid)
        {
            return guid.HasValue ? $"{_downloadFileByGuid}{guid}" : string.Empty;
        }

        public string GetConfigUrlDownloadFileByGuidStr(string guidStr)
        {
            return !string.IsNullOrEmpty(guidStr) ? $"{_downloadFileByGuid}{guidStr}" : string.Empty;
        }
        public string GetConfigUrlDownloadFileByFileId(string fileId)
        {
            return !string.IsNullOrEmpty(fileId) ? $"{_downloadFileById}{fileId}" : string.Empty;
        }
        private string GetHostBase()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            return $"http://{request.Host.Value}";
        }

        public string GetConfigUrlThumFileId(string fileId)
        {
            return !string.IsNullOrEmpty(fileId) ? $"{_downloadThumById}{fileId}" : string.Empty;
        }

        public string GetLinkDashboard()
        {
            return _linkDashboard;
        }
        public string GetServerRoot()
        {
            return _fileServerRootUrl;
        }

    }
}
