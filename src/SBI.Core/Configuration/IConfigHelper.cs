﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Configuration
{
    public interface IConfigHelper : ITransientDependency
    {
        string GetHostRequest();
        string GetConfigUrlDownloadFileByGuid(Guid? guid);
        string GetConfigUrlDownloadFileByGuidStr(string guidStr);
        string GetConfigUrlDownloadFileByFileId(string fileId);
        string GetConfigUrlThumFileId(string fileId);
        string GetServerRoot();
        string GetLinkDashboard();
    }
}
