﻿using Abp;
using Abp.Domain.Entities.Auditing;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Timing;
using Abp.Zero;
using Abp.Zero.Configuration;
using Abp.Configuration.Startup;
using Abp.Hangfire;
using CRM.Authorization.Roles;
using CRM.Authorization.Users;
using CRM.Configuration;
using CRM.Localization;
using CRM.MultiTenancy;
using CRM.Timing;
using Abp.Zero.Ldap;
using Abp.Dependency;
using CRM.Email;
using CRM.ExportHelper;
using CRM.Authentication.Helper;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Abp.IO;

namespace  CRM
{
    [DependsOn(typeof(AbpZeroCoreModule), typeof(AbpZeroLdapModule))]
    [DependsOn(typeof(AbpHangfireAspNetCoreModule))]
    public class SBICoreModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        public SBICoreModule(IHostingEnvironment env)
        {
            _env = env;
        }
        public override void PreInitialize()
        {

            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            Configuration.EntityHistory.Selectors.Add(
                new NamedTypeSelector(
                    "Abp.FullAuditedEntities",
                    type => typeof(IFullAudited).IsAssignableFrom(type)
                )
            );

            // Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            SBILocalizationConfigurer.Configure(Configuration.Localization);

            // Enable this line to create a multi-tenant application.
             Configuration.MultiTenancy.IsEnabled = SBIConsts.MultiTenancyEnabled;

           // Enable LDAP authentication(It can be enabled only if MultiTenancy is disabled!)
            //Configuration.Modules.ZeroLdap().Enable(typeof(AppLdapAuthenticationSource));
            //IocManager.Register<ILdapSettings, MyLdapSettings>(); //change default setting source
            //Configuration.Modules.Zero().UserManagement.ExternalAuthenticationSources.Add<AppLdapAuthenticationSource>();

            // Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);
            Configuration.ReplaceService<IEmailSenderHelper, SendGridSender>(DependencyLifeStyle.Transient);
            Configuration.ReplaceService<IExportHelper, ExportHelper.ExportHelper>(DependencyLifeStyle.Transient);
            Configuration.ReplaceService<IAuthenHelperManager, AuthenHelperManager>(DependencyLifeStyle.Transient);
            Configuration.ReplaceService<IAppFolders, AppFolders>(DependencyLifeStyle.Transient);
            Configuration.Settings.Providers.Add<AppSettingProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SBICoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
            //SetAppFolders();
        }
        //private void SetAppFolders()
        //{
        //    var appFolders = IocManager.Resolve<AppFolders>();

        //    appFolders.SampleProfileImagesFolder = Path.Combine(_env.WebRootPath, $"Common{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}SampleProfilePics");
        //    appFolders.TempFileDownloadFolder = Path.Combine(_env.WebRootPath, $"Temp{Path.DirectorySeparatorChar}Downloads");
        //    appFolders.WebLogsFolder = Path.Combine(_env.ContentRootPath, $"App_Data{Path.DirectorySeparatorChar}Logs");

        //    try
        //    {
        //        DirectoryHelper.CreateIfNotExists(appFolders.TempFileDownloadFolder);
        //    }
        //    catch { }
        //}
    }
}
