﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace CRM.Localization
{
    public static class SBILocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "CRM.Localization.SourceFiles"
                    )
                )
            );
             localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationWebMainMenu,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.WebMainMenu"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationWebError,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.WebError"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationWebNotification,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.WebNotification"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationWebLabel,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.WebLabel"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationWebCategory,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.WebCategory"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationMobileResident,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.MobileResident"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationMobileAdmin,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.MobileAdmin"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationServiceRoles,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.ServiceRoles"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationServiceError,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.ServiceError"
                    )
                )
            );
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SBIConsts.LocalizationServiceNotification,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SBILocalizationConfigurer).GetAssembly(),
                        "PicoSolution.Localization.ServiceNotification"
                    )
                )
            );
        }
    }
}
