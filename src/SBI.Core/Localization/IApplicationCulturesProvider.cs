﻿using Abp.Application.Services;
using System.Globalization;

namespace CRM.Core.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}