﻿using System.ComponentModel.DataAnnotations;
using Abp.Localization;

namespace CRM.Localization
{
    public class LocalizationDto
    {

        [Required]
        [StringLength(ApplicationLanguage.MaxNameLength)]
        public string LanguageName { get; set; }

        [StringLength(ApplicationLanguageText.MaxValueLength)]
        public string Value { get; set; }
    }
}
