﻿using Abp.Application.Services;
using Abp.Dependency;
using CRM.ResponseHelper;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Email
{
    public interface IEmailSenderHelper : ITransientDependency
    {
        Task SendAsync(long campaignId, long contactId, string templet, string from, string to, string subject, string body, string module);
        Task<bool> SendAsyncWithAttachments(string from, string to, string subject, string body, List<SendGrid.Helpers.Mail.Attachment> attachments, bool normalize = true);
        Task<ResultApiModel> CheckEmailAsync(string emailAddress);
        Task<TempleteSendgridOutputDto> GetTempleteSendgrid(string templetType);
        Task SendReminderActity(string displayName, string emailAddress, string startDate, string status, string taskName, string description, string link);

    }
}
