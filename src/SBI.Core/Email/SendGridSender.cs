﻿using Abp.AppFactory.Interfaces;
using Abp.AppFactory.SendGrid.Email;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Net.Mail.Smtp;
using Abp.UI;
using Castle.Core.Logging;
using CRM.Authorization.Users;
using CRM.BackgroundJob;
using CRM.Configuration;
using CRM.EmailHelper;
using CRM.HttpClientHelper;
using CRM.ResponseHelper;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Email
{
    public class SendGridSender : IEmailSenderHelper
    {
        private readonly ISmtpEmailSenderConfiguration _emailConfiguration;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly ISettingManager _settingManager;
        private readonly UserManager _userManager;
        private readonly ILogger _logger;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly ISendGrid _sendGrid;
        private readonly IHttpClientHelper _iHpptClientHelper;
        private readonly IRepository<Entities.EmailHistory.EmailHistory, long> _repoEmailHistory;
        private readonly string _apiKeySendgrid;
        private readonly string _rootUrlSendgrid;
        private readonly string _getTempleteSendgrid = "/templates?generations={0}";
        private readonly string _userServiceUrl;
        private readonly string _paramaterEmail = "?api={0}&email={1}";
        private readonly string _apiKey;
        private readonly SendGridClient _client;
        public SendGridSender(ISettingManager settingManager
            , IEmailTemplateProvider emailTemplateProvider
            , UserManager userManager
            , ILogger logger
            , IBackgroundJobManager backgroundJobManager
            , IHttpClientHelper iHpptClientHelper
            , IAppConfigurationAccessor appConfiguration
            , ISendGrid sendGrid
            , ISmtpEmailSenderConfiguration emailConfiguration
            , IRepository<Entities.EmailHistory.EmailHistory, long> repoEmailHistory)
        {
            _repoEmailHistory = repoEmailHistory;
            _settingManager = settingManager;
            _userManager = userManager;
            _logger = logger;
            _sendGrid = sendGrid;
            _emailTemplateProvider = emailTemplateProvider;
            _userServiceUrl = appConfiguration.Configuration["App:DebouncelRootAddress"];
            _apiKey = appConfiguration.Configuration["Debounce:ApiKey"];
            _apiKeySendgrid = appConfiguration.Configuration["SendGrid:Key"];
            _rootUrlSendgrid = appConfiguration.Configuration["App:SendgridlRootAddress"];
            _iHpptClientHelper = iHpptClientHelper;
            _backgroundJobManager = backgroundJobManager;
            _emailConfiguration = emailConfiguration;
            _client = new SendGridClient(_apiKeySendgrid);
        }

        public async Task<ResultApiModel> CheckEmailAsync(string emailAddress)
        {
            var url = $"{_userServiceUrl}{string.Format(_paramaterEmail, new object[] { _apiKey, emailAddress })}";
            var results = await _iHpptClientHelper.GetAsync(url);
            if (results == null)
                throw new UserFriendlyException("ErorConnectingWithVendorSite");
            return results;
        }

        public async Task<TempleteSendgridOutputDto> GetTempleteSendgrid(string templeteType)
        {

            var url = $"{_rootUrlSendgrid}{string.Format(_getTempleteSendgrid, new object[] { templeteType })}";
            var results = await _iHpptClientHelper.GetTempleteAsync(url, _apiKeySendgrid);
            if (results == null)
                throw new UserFriendlyException("ErorConnectingWithVendorSite");
            return results;
        }
        public async Task<bool> SendAsyncWithAttachments(string from, string to, string subject, string body, List<SendGrid.Helpers.Mail.Attachment> attachments, bool normalize = true)
        {
            return await SendEmailAsync(from, to, subject, body, attachments, true);
        }
        public async Task<bool> SendEmailAsync(string from, string to, string subject, string body, List<SendGrid.Helpers.Mail.Attachment> attachments, bool isBodyHtml = true)
        {
            try
            {
                _logger.Info($"Begin send SendAsync.");

                var addressFrom = new EmailAddress(from);
                var addressTo = new EmailAddress(to);
                var bodyHtml = string.Empty;
                if (isBodyHtml)
                {
                    bodyHtml = body;
                    body = string.Empty;
                }
                var mail = MailHelper.CreateSingleEmail(addressFrom, addressTo, subject, body, bodyHtml);
                if (attachments.Any())
                {
                    mail.AddAttachments(attachments);
                }
                var response = await _client.SendEmailAsync(mail);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Info($"Execute SendAsync email. Failed {ex.Message}.");
                return false;
            }
        }
        public async Task SendReminderActity(string displayName, string emailAddress, string startDate, string status, string taskName, string description, string link)
        {
            var subject = "Reminder";
            var emailContent = new StringBuilder(_emailTemplateProvider.GetEmailTemplate($"CRM.EmailTemplate.EmailReminder.html"));
            var dataBindings = new Dictionary<string, string>() {
                      {EmailConsts.Reminder.StartDate, startDate},
                      {EmailConsts.Reminder.Status, status},
                      {EmailConsts.Reminder.DisplayName, displayName},
                      {EmailConsts.Reminder.TaskName, taskName},
                      {EmailConsts.Reminder.Description, description},
                      {EmailConsts.Reminder.Link, link}
            };

            var bodyHtmlContent = _emailTemplateProvider.BindDataToTemplate(emailContent.ToString(), dataBindings);

            var response = await _sendGrid.SendAsync(new SendGridEmail()
            {
                SenderName = "SBI Notify",
                SenderEmailAddress = "vncrm@savills.com.vn",
                SubjectContent = subject,
                BodyHtmlContent = bodyHtmlContent,
                RecepientEmailAddress = emailAddress,
            });
        }

        public async Task SendAsync(long campaignId, long contactId, string templete, string from, string to, string subject, string body, string module)
        {
            try
            {
                _logger.Info($"Begin send SendAsync.");
                var user = await _userManager.FindByEmailAsync(to);
                if (!string.IsNullOrEmpty(templete))
                {
                    _logger.Info($" IF Execute SendAsync email. Successfully: {to}.___________________________________");
                    var addressFrom = new EmailAddress(from, "CRM Notify");
                    var addressTo = new EmailAddress(to, "");
                    var mail = MailHelper.CreateSingleEmail(addressFrom, addressTo, subject, string.Empty, body);
                    mail.SetTemplateId(templete);
                    var response = await _client.SendEmailAsync(mail);
                    if (response.StatusCode == HttpStatusCode.Accepted)
                    {
                        await WriteLogSendEmail(campaignId, contactId, (int)response.StatusCode, response.Body.ReadAsStringAsync().Result, module, to, subject, body, true);
                        _logger.Info($"Execute SendAsync email. Successfully: {to}.___________________________________");
                    }
                }
                else
                {
                    _logger.Info($"  ELSE Execute SendAsync email. Successfully: {to}.___________________________________");
                    var response = await _sendGrid.SendAsync(new SendGridEmail()
                    {
                        SenderName = "CRM Notify",
                        SenderEmailAddress = from,
                        SubjectContent = subject,
                        BodyHtmlContent = body,
                        RecepientEmailAddress = to
                    });
                    if (response.StatusCode == HttpStatusCode.Accepted)
                    {
                        await WriteLogSendEmail(campaignId, contactId, (int)response.StatusCode, response.Body.ReadAsStringAsync().Result, module, to, subject, body, true);
                        _logger.Info($"Execute SendAsync email. Successfully: {to}.___________________________________");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Info($"Execute SendAsync email. Failed {ex.Message}.");
                await WriteLogSendEmail(campaignId, contactId, 0, ex.Message, module, to, subject, body, false);
            }
        }
        private async Task WriteLogSendEmail(long campaignId, long contactId, int? statusCode, string resultBody, string module, string sentTo, string emailSubject, string emailContent, bool isSuccess)
        {
            await _backgroundJobManager.EnqueueAsync<WriteEmailHistoryJob, WriteEmailHistoryArgs>(
                new WriteEmailHistoryArgs
                {
                    CampaignId = campaignId,
                    ContactId = contactId,
                    SentTo = sentTo,
                    EmailSubject = emailSubject,
                    EmailContent = emailContent,
                    Module = module,
                    StatusCode = statusCode,
                    IsSuccess = isSuccess,
                    ResultBody = resultBody
                });
        }

    }
}
