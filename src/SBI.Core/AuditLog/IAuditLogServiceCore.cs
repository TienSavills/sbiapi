﻿using Abp.Dependency;
using CRM.Application.Shared.AuditLong.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM.Core.AuditLog;

namespace CRM.Application.Shared.AuditLong
{
    public interface IAuditLogServiceCore : ITransientDependency
    {
        Task<List<AuditLogOutput>> GetAuditLogs<T>(string[] properties, Func<List<AuditLogSimpleDto>, Task> repareLogs,
            params long[] ids);
    }
}
