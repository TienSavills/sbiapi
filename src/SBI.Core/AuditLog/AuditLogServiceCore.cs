﻿using Abp.Domain.Repositories;
using Abp.EntityHistory;
using CRM.Authorization.Users;
using CRM.Authorization.Users.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM.Application.Shared.AuditLong;

namespace CRM.Core.AuditLog
{
    public class AuditLogServiceCore : SbiDomainServiceBase, IAuditLogServiceCore
    {
        private readonly UserManager _userManager;
        private readonly IRepository<EntityChange, long> _repositoryEntityChange;
        private readonly IRepository<EntityChangeSet, long> _repositoryEntityChangeSet;
        private readonly IRepository<EntityPropertyChange, long> _repositoryEntityPropertyChange;
        public AuditLogServiceCore(UserManager userManager, IRepository<EntityChange, long> repositoryEntityChange, IRepository<EntityChangeSet, long> repositoryEntityChangeSet, IRepository<EntityPropertyChange, long> repositoryEntityPropertyChange)
        {
            _userManager = userManager;
            _repositoryEntityChange = repositoryEntityChange;
            _repositoryEntityChangeSet = repositoryEntityChangeSet;
            _repositoryEntityPropertyChange = repositoryEntityPropertyChange;
        }

        public async Task<List<AuditLogOutput>> GetAuditLogs<T>(string[] properties, Func<List<AuditLogSimpleDto>, Task> repareLogs, params long[] ids)
        {
            var idsString = ids?.Select(x => x.ToString()).ToList() ?? new List<string>();
            var query =
                from e in _repositoryEntityChange.GetAll()
                    .Where(x => idsString.Contains(x.EntityId))
                join p in _repositoryEntityPropertyChange.GetAll()
                    on e.Id equals p.EntityChangeId
                join c in _repositoryEntityChangeSet.GetAll() on e.EntityChangeSetId equals c.Id
                where
                    //e.EntityId == id.ToString()
                    e.EntityTypeFullName == typeof(T).FullName
                    && p.NewValue != p.OriginalValue
                    && properties.Contains(p.PropertyName)
                select new AuditLogSimpleDto
                {
                    Id = c.Id,
                    UserId = c.UserId ?? 0,
                    ChangeTime = e.ChangeTime,
                    PropertyName = p.PropertyName,
                    OriginalValue = p.OriginalValue,
                    OriginalValueDisplay = p.OriginalValue,
                    NewValue = p.NewValue,
                    NewValueDisplay = p.NewValue,
                    EntityTypeFullName = e.EntityTypeFullName
                };
            var entities = await query.ToListAsync();
            //var entities = await _commonRepository.GetAuditLogs<T>(id, properties);

            await repareLogs(entities);

            #region Get Output
            var results = new List<AuditLogOutput>();
            entities.ForEach(e =>
            {
                e.NewValueDisplay = e.NewValueDisplay?.Trim('\"');
                e.OriginalValueDisplay = e.OriginalValueDisplay?.Trim('\"');
                if (!string.IsNullOrEmpty(e.NewValueDisplay) || !string.IsNullOrEmpty(e.OriginalValueDisplay))
                {
                    var output = results.FirstOrDefault(x => x.Id == e.Id);
                    if (output == null)
                    {
                        output = new AuditLogOutput
                        {
                            Id = e.Id,
                            ChangeTime = e.ChangeTime,
                            User = new UserSimpleDto
                            {
                                Id = e.UserId
                            }
                        };
                        results.Add(output);
                    }
                    output.Items.Add(ObjectMapper.Map<AuditLogItemOutput>(e));
                }
            });
            #endregion

            await _userManager.AppendUser((x, user) => x.User = user, x => x.User.Id, results.ToArray());
            return results;
        }
    }
}
