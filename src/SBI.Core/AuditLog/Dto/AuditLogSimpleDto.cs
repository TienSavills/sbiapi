﻿using System;

namespace CRM.Core.AuditLog
{
    public class AuditLogSimpleDto
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public DateTime ChangeTime { get; set; }
        public long EntityChangeId { get; set; }
        public string PropertyName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        public string EntityTypeFullName { get; set; }
        public string NewValueDisplay { get; set; }
        public string OriginalValueDisplay { get; set; }
    }
}
