﻿using System;

namespace CRM.Core.AuditLog
{
    public class AuditLogItemOutput
    {
        public DateTime ChangeTime { get; set; }
        public string PropertyName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        public string NewValueDisplay { get; set; }
        public string OriginalValueDisplay { get; set; }
    }
}
