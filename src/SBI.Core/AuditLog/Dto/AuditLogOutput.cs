﻿using CRM.Authorization.Users.Dto;
using System;
using System.Collections.Generic;

namespace CRM.Core.AuditLog
{
    public class AuditLogOutput
    {
        public long Id { get; set; }
        public DateTime ChangeTime { get; set; }
        public UserSimpleDto User { get; set; }
        public List<AuditLogItemOutput> Items { get; set; } = new List<AuditLogItemOutput>();
    }
}
