﻿using System;
using System.Collections.Generic;

namespace CRM.Application.Shared.AuditLong.Dto
{
    public class AuditLogDto
    {
        public DateTime ChangeTime { get; set; }
        public IList<AuditLogUserDto> Users { get; set; }
    }
    public class AuditLogUserDto
    {
        public string DisplayName { get; set; }
        public IList<AuditLogProperty> Logs { get; set; }
    }
    public class AuditLogProperty
    {
        public string PropertyName { get; set; }
        public string NewValue { get; set; }
        public string OriginalValue { get; set; }
    }
}
