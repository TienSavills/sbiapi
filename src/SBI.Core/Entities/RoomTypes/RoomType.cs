﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.RoomTypes
{
    public partial class  RoomType:CreationAuditedEntity<int>
    {
        public string RoomTypeName { get; set; }
        public bool IsActive { get; set; }
    }
}
