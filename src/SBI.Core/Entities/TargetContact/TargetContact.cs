﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.TargetContact
{
    public class TargetContact:FullAuditedEntity<long>
    {
        public long TargetId { get; set; }
        public long ContactId { get; set; }
        public bool IsActive { get; set; }
    }
}
