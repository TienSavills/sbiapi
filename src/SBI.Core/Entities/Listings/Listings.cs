﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.Listings
{
    public class Listings:FullAuditedEntity<long>,IPassivable
    {
        public long? ProjectId { get; set; }
        public long? FloorId { get; set; }
        public string ListingName { get; set; }
        public long UnitId { get; set; }
        public decimal? Size { get; set; }
        public int StatusId  { get; set; }
        public int TypeId { get; set; }
        public DateTime? DateAvailable { get; set; }
        public decimal? TotalCommission { get; set; }
        public decimal Price { get; set; }
        public string FloorZone { get; set; }
        public  bool IsKey { get; set; }
        public string Description { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsPublic { get; set; }
        public bool IsActive { get; set; }
    }
}
