﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.ProjectConstructionMaps
{
    public class ProjectConstructionMap:CreationAuditedEntity<long>
    {
        public long ProjectId { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime ActualDate { get; set; }
        public int ConstructionStatusId { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public ConstructionStatus.ConstructionStatus ConstructionStatus { get; set; }
        public Project Project { get; set; }
    }
}
