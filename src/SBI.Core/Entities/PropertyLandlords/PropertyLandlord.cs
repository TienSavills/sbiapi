﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.PropertyLandlords
{
    public partial class PropertyLandlord : FullAuditedEntity<long>
    {
        public long PropertyId { get; set; }
        public long CompanyId { get; set; }
        public bool IsActive { get; set; }
        public Company.Company Company { get; set; }
        public Property Property { get; set; }
    }
}
