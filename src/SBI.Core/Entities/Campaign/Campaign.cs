﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.Campaign
{
    public class Campaign:FullAuditedEntity<long>
    {
        public string Name { get; set; }
        public string ContentEmail { get; set; }
        public string Template { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public int SendCount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
