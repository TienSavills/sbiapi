﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.OtherCategory
{
    public class OtherCategory : CreationAuditedEntity<int>
    {
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string Color { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
