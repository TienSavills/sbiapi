﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.Districs
{
   
    public partial class District : FullAuditedEntity<int>, IPassivable
    {
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public int ProvinceId { get; set; }
        public Provinces.Province Province { get; set; }
        public bool IsActive { get; set; }
    }
}
