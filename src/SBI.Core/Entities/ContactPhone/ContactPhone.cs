﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ContactPhone
{
    public class ContactPhone : FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public Contacts.Contact Contact { get; set; }
        public int? PhoneTypeId { get; set; }
        public OtherCategory.OtherCategory PhoneType { get; set; }
        public bool? IsPrimary { get; set; }
        public string Phone { get; set; }
        public int? CountryId { get; set; }
        public Entities.Countries.Country Country { get; set; }
        public bool IsActive { get; set; }
    }
}
