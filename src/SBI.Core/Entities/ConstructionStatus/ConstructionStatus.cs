﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using CRM.Entities.Projects;

namespace CRM.Entities.ConstructionStatus
{
    public partial class ConstructionStatus : Entity<int>
    {
        public string Code { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
    }
}
