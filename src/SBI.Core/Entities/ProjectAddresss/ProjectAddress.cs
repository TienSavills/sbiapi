﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;

namespace CRM.Entities.ProjectAddresss
{
    public partial class ProjectAddress : FullAuditedEntity<long>, IPassivable
    {
        public long ProjectId { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public bool IsActive { get; set; }
        public int? RegionId { get; set; }
        public Project Project { get; set; }
    }
}
