﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.TargetRequest
{
    public class TargetRequest:FullAuditedEntity<long>
    {
        public long TargetId { get; set; }
        public long RequestId { get; set; }
        public bool IsActive { get; set; }
    }
}
