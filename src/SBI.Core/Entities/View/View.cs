﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.PropertyFacilityMaps;
using CRM.Entities.ProjectFacilityMaps;

namespace CRM.Entities.View
{
    public partial class View : CreationAuditedEntity<int>, IPassivable
    {
        public View()
        {
            UnitViewMaps = new HashSet<UnitViewMaps.UnitViewMaps>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Icon { get; set; }
        public ICollection<UnitViewMaps.UnitViewMaps> UnitViewMaps { get; set; }
    }
}
