﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.ProjectClaims;
using CRM.Entities.RoomTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ProjectClaimRoomTypeMaps
{
    public partial class ProjectClaimRoomTypeMap : CreationAuditedEntity<long>
    {
        public long ProjectClaimId { get; set; }
        public int RoomTypeId { get; set; }
        public int NumberOfRoom { get; set; }
        public bool IsActive { get; set; }
        public ProjectClaim ProjectClaim { get; set; }
        public RoomType RoomType { get; set; }
    }
}
