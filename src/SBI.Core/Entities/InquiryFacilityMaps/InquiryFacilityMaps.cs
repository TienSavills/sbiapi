﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Facilities;

namespace CRM.Entities.InquiryFacilityMaps
{
    public partial class InquiryFacilityMaps : FullAuditedEntity<long>, IPassivable
    {
        public long InquiryId { get; set; }
        public int FacilityId { get; set; }
        public bool IsActive { get; set; }
        public Inquiries.Inquiry Inquiry { get; set; }
        public Facility Facility { get; set; }
    }
}
