﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.ProjectGradeMaps;
using CRM.Entities.Projects;
using CRM.Entities.Properties;
using CRM.Entities.PropertyGradeMaps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.Grades
{
    public partial class Grade:CreationAuditedEntity<int>
    {
        public Grade()
        {
            ProjectGradeMap = new HashSet<ProjectGradeMap>();
            PropertyGradeMap = new HashSet<PropertyGradeMap>();
        }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string GradeGroup { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        public ICollection<ProjectGradeMap> ProjectGradeMap { get; set; }
        public ICollection<PropertyGradeMap> PropertyGradeMap { get; set; }
    }
}
