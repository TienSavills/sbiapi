﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ContactTypeMap
{
    public class ContactTypeMap : FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public int ContactTypeId { get; set; }
        public OtherCategory.OtherCategory ContactType { get; set; }
        public bool IsActive { get; set; }
    }
}
