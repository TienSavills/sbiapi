﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Contacts;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ContactAddress
{
    public class ContactAddress:FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPrimary { get; set; }
        public Contact Contact { get; set; }
    }
}
