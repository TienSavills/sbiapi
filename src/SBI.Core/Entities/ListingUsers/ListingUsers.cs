﻿using System;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.ListingUsers
{
    public class ListingUsers:FullAuditedEntity<long>,IPassivable
    {
      public long ListingId { get; set; }
      public long UserId { get; set; }
      public bool IsActive { get; set; }
    }
}
