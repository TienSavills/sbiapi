﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.LeadSource
{
    public class LeadSource:CreationAuditedEntity<int>
    {
        public string LeadSourceName { get; set; }
        public string LeadSourceDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
