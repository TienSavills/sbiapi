﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Comment
{
    public class Comment : FullAuditedEntity<long>
    {
        public int ModuleId { get; set; }
        public ActivityType.ActivityType Module { get; set; }
        public long ReferenceId { get; set; }
        public long? ParentId { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public User CreatorUser { get; set; }
        public User LastModifierUser { get; set; }
        public bool? IsSend { get; set; }
    }
}
