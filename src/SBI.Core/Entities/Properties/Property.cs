﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.PropertyAddresss;
using CRM.Entities.PropertyFacilityMaps;
//using CRM.Entities.PropertyTenants;
//using CRM.Entities.PropertyLandlords;
using CRM.Entities.Projects;
using CRM.Entities.PropertyTypeMaps;
using CRM.Entities.PropertyGradeMaps;
//using CRM.Entities.TenantTypes;
using CRM.Entities.TenureTypes;

namespace CRM.Entities.Properties
{
    public partial class Property : FullAuditedEntity<long>
    {
        public Property()
        {
            PropertyAddress = new HashSet<PropertyAddress>();
            PropertyFacilityMap = new HashSet<PropertyFacilityMap>();
            PropertyTypeMap = new HashSet<PropertyTypeMap>();
            //PropertyTenant = new HashSet<PropertyTenant>();
            //PropertyLandlord = new HashSet<PropertyLandlord>();
            PropertyGradeMap = new HashSet<PropertyGradeMap>();
        }
        public long? ProjectId { get; set; }
        public string PropertyCode { get; set; }
        public string PropertyName { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public int? RequestTypeId { get; set; }
        public int? StatusId { get; set; }
        public int ? CurrencyId { get; set; }
        public decimal? Square { get; set; }
        public int? Floor { get; set; }
        public int? FloorCount { get; set; }
        public int? BedroomCount { get; set; }
        public int? YearBuilt { get; set; }
        public int? BarthroomCount { get; set; }
        public int? UnitsCount { get; set; }
        public decimal? Price { get; set; }
        public Project Project { get; set; }
        public ICollection<PropertyAddress> PropertyAddress { get; set; }
        public ICollection<PropertyFacilityMap> PropertyFacilityMap { get; set; }
        public ICollection<PropertyTypeMap> PropertyTypeMap { get; set; }
        //public ICollection<PropertyTenant> PropertyTenant { get; set; }
        //public ICollection<PropertyLandlord> PropertyLandlord { get; set; }
        public ICollection<PropertyGradeMap> PropertyGradeMap { get; set; }

    }
}
