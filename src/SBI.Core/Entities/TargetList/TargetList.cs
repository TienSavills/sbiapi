﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.TargetList
{
    public class TargetList:FullAuditedEntity<long>
    {
      
        public string TargetName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsStatic { get; set; }
    }
}
