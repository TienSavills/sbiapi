﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;

namespace CRM.Entities.Countries
{
    public partial class Country : FullAuditedEntity<int>, IPassivable
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string PhoneCode { get; set; }
        public string Continent { get; set; }
        public bool IsActive { get; set; }
        public List<Provinces.Province> Provinces { get; set; }
    }
}
