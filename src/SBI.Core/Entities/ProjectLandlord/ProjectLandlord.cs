﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;

namespace CRM.Entities.ProjectLandlord
{
    public partial class ProjectLandlord:FullAuditedEntity<long>
    {
        public long ProjectId { get; set; }
        public long CompanyId { get; set; }
        public bool IsActive { get; set; }
        public Company.Company Company { get; set; }
        public Project Project { get; set; }
    }
}
