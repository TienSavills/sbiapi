﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.ProperyConstructionMaps
{
    public class PropertyConstructionMap:CreationAuditedEntity<long>
    {
        public long PropertyId { get; set; }
        public int ConstructionStatusId { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime ActualDate { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public ConstructionStatus.ConstructionStatus ConstructionStatus { get; set; }
        public Property Property { get; set; }
    }
}
