﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ContactEmail
{
    public class ContactEmail : FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public Entities.Contacts.Contact Contact { get;set;}
        public string Email { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? IsOptedOut { get; set; }
        public bool? IsCheckEmail { get; set; }
        public bool? IsInvalid { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }
        public string Result { get; set; }
        public string Reason { get; set; }
        public string Role { get; set; }
        public string FeeEmail { get; set; }
        public string SendTransactional { get; set; }

    }
}
