﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Financial
{
    public class Capacity:FullAuditedEntity<long>
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public long OrganizationUnitId { get; set; }
        public decimal? BudgetRevenue { get; set; }
        public decimal? Expense { get; set; }
        public decimal? BudgetExpense { get; set; }
        public decimal? ActRevenue { get; set; }
        public bool? IsActive { get; set; }
    }
}
