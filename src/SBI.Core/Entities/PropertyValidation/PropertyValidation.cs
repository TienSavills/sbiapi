﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.PropertyValidation
{
    public partial class PropertyValidation:CreationAuditedEntity<int>,IPassivable
    {
        public int PropertyTypeId { get; set; }
        public bool RequiredBuilding { get; set; }
        public bool RequiredFloor { get; set; }
        public bool RequiredUnit { get; set; }
        public bool IsActive { get; set; }
    }
}
