﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.OpportuityCategory;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Payment
{
    public class Payment:FullAuditedEntity<long>, IPassivable
    {
        public long DealId { get; set; }
        public Deal.Deal Deal { get; set; }
        public decimal EstPercent { get; set; }
        public DateTime EstDate { get; set; }
        public decimal EstAmount { get; set; }
        public DateTime? ActDate { get; set; }
        public decimal? ActAmount { get; set; }
        public decimal VATAmount { get; set; }
        public int StatusId { get; set; }
        public OpportunityCategory Status { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? UsdAmount { get; set; }
        public long? NumInvoice { get; set; }
        public DateTime? RevenueDate { get; set; }
        public bool? IsSign { get; set; }
        public bool? IsAdjust { get; set; }
        public string PaymentTimeLine { get; set; }
    }
}
