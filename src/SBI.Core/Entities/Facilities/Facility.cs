﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.PropertyFacilityMaps;
using CRM.Entities.ProjectFacilityMaps;

namespace CRM.Entities.Facilities
{
    public partial class Facility : FullAuditedEntity<int>, IPassivable
    {
        public Facility()
        {
            UnitFacilityMaps = new HashSet<UnitFacilityMaps.UnitFacilityMaps>();
            ProjectFacilityMap = new HashSet<ProjectFacilityMap>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
        public ICollection<UnitFacilityMaps.UnitFacilityMaps> UnitFacilityMaps { get; set; }
        public ICollection<ProjectFacilityMap> ProjectFacilityMap { get; set; }
    }
}
