﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Currencys;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.CurrencyRates
{
    public partial class CurrencyRate : CreationAuditedEntity<long>
    {
        public int CurrencyId { get; set; }
        public decimal RateBaseVND { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool IsActive { get; set; }
        public Currency Currency{ get; set; }
    }
}
