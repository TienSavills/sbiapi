﻿using System.Collections.Generic;
using System.ComponentModel;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using CRM.Entities.ProjectAddresss;
using CRM.Entities.ProjectFacilityMaps;
using CRM.Entities.ProjectTypeMaps;
using CRM.Entities.ProjectGradeMaps;
using System;
//using CRM.Entities.TenantTypes;
using CRM.Entities.TenureTypes;

namespace CRM.Entities.Projects
{
    public partial class Project : FullAuditedEntity<long>, IPassivable
    {
        public Project()
        {
            Property = new HashSet<Property>();
            ProjectAddress = new HashSet<ProjectAddress>();
            ProjectFacilityMap = new HashSet<ProjectFacilityMap>();
            //ProjectLandlord = new HashSet<ProjectLandlord.ProjectLandlord>();
            //ProjectTenant = new HashSet<ProjectTenant.ProjectTenant>();
            ProjectTypeMap = new HashSet<ProjectTypeMap>();
            ProjectGradeMap = new HashSet<ProjectGradeMap>();
        }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public int? NumberOfFloors { get; set; }
        public int? NumberOfUnits { get; set; }
        public DateTime? LaunchingTime { get; set; }
        public DateTime? BuiltDate { get; set; }
        public string Description { get; set; }
        public DateTime? YearRenovated { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        public bool? OutOfMarket { get; set; }
        public DateTime? OutOfDate { get; set; }
        public int? TenureTypeId { get; set; }
        public TenureType TenureType { get; set; }
        public ConstructionStatus.ConstructionStatus ConstructionStatus { get; set; }
        public ICollection<Property> Property { get; set; }
        public ICollection<ProjectAddress> ProjectAddress { get; set; }
        public ICollection<ProjectFacilityMap> ProjectFacilityMap { get; set; }
        public ICollection<ProjectTransportationMaps.ProjectTransportationMaps> ProjectTransportationMaps { get; set; }
        public ICollection<ProjectTypeMap> ProjectTypeMap { get; set; }
        public ICollection<ProjectGradeMap> ProjectGradeMap { get; set; }
        public long? LandlordId { get; set; }
        public long? PropertyManagementId { get; set; }
        public decimal? TotalSize { get; set; }
        public decimal? LandArea { get; set; }
        public bool? IsEnableStackingPlan { get; set; }
        public decimal? CeilingHeight { get; set; }
        public string BackupPower { get; set; }
        public string AirConditioning { get; set; }
        public decimal? ElectricityCost { get; set; }
        public decimal? WaterCost { get; set; }
        public decimal? MotorbikeCost { get; set; }
        public decimal? DedicatedCarCost { get; set; }
        public decimal? ManagementFee { get; set; }
        public decimal? AskingRent { get; set; }
        public decimal? AskingPrice { get; set; }
        public long? ContactId { get; set; }
        public long? Price { get; set; }
    }
}
