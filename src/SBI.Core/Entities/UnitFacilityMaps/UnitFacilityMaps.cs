﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Facilities;

namespace CRM.Entities.UnitFacilityMaps
{
    public partial class UnitFacilityMaps : FullAuditedEntity<long>, IPassivable
    {
        public long UnitId { get; set; }
        public int FacilityId { get; set; }
        public bool IsActive { get; set; }
        public Unit.Unit Unit { get; set; }
        public Facility Facility { get; set; }
    }
}
