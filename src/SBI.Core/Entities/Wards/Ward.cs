﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Districs;

namespace CRM.Entities.Wards
{
    public partial class Ward:CreationAuditedEntity<int>,IPassivable
    {
        public string WardCode { get; set; }
        public string WardName { get; set; }
        public int DistrictId { get; set; }
        public bool IsActive { get; set; }
        public District District { get; set; }
    }
}
