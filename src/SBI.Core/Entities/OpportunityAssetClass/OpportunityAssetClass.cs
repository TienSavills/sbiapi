﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.OpportuityCategory;

namespace CRM.Entities.OpportunityAssetClass
{
    public class OpportunityAssetClass : FullAuditedEntity<long>, IPassivable
    {
        public long OpportunityId { get; set; }
        public Entities.Opportunity.Opportunity Opportunity { get; set; }
        public int AssetClassId { get; set; }
        public OpportunityCategory AssetClass { get; set; }
        public bool IsActive { get; set; }
    }
}
