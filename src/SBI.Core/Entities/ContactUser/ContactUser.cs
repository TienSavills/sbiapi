﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ContactUser
{
    public class ContactUser : FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public Contacts.Contact Contact { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public bool? IsRequest { get; set; }
        public bool? IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
