﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using CRM.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using CRM.Authorization.Users;

namespace CRM.Entities.ClientInquiries
{
    public partial class ClientInquiry : FullAuditedEntity<long>, IPassivable
    {

        public string Gender { get; set; }
        public string ClientName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Messenge { get; set; }
        public int StatusId { get; set; }
        public bool IsActive { get; set; }
        public ClientInquiryCategory Status { get; set; }
        public virtual User CreatorUser { get; set; }
        public virtual User LastModifierUser { get; set; }

    }
}
