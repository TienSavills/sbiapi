﻿using System;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.ClientInquiries
{
    public class ClientInquiryUsers : FullAuditedEntity<long>,IPassivable
    {
      public long ClientInquiryId { get; set; }
      public long UserId { get; set; }
      public bool IsActive { get; set; }
    }
}
