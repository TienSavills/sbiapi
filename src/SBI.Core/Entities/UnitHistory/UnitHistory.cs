﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.UnitHistory
{
    public class UnitHistory : FullAuditedEntity<long>, IPassivable
    {
        public long OrgTenantId { get; set; }
        public Company.Company OrgTenant { get; set; }
        public long UnitId { get; set; }
        public Unit.Unit Unit { get; set; }
        public int? Cars { get; set; }
        public int? Motorbikes { get; set; }
        public string ItServices { get; set; }
        public string BackupPower { get; set; }
        public decimal? Space { get; set; }
        public int? HeadCount { get; set; }
        public decimal? Monthly { get; set; }
        public decimal? Per { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string ReasonMove { get; set; }
        public bool IsActive { get; set; }
        public User CreatorUser { get; set; }
        public User LastModifierUser { get; set; }
        public string Description { get; set; }
        public int? Incentives { get; set; }
        //public int? Term { get; set; }
    }
}
