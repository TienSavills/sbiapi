﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Countries;
using CRM.Entities.Districs;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Collections.Generic;

namespace CRM.Entities.Provinces
{
    public partial class Province : FullAuditedEntity<int>, IPassivable
    {
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public bool IsActive { get; set; }
        public List<District> Districts { get; set; }
    }
}
