﻿using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.ListingCategory
{
    public class ListingCategory : CreationAuditedEntity<int>
    {
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string Color { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
