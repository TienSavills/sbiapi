﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using CRM.Entities.ActivityType;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Activity
{
    public class Activity:FullAuditedEntity<long>
    {
        public string ActivityName { get; set; }
        public int? TypeId { get; set; }
        public CRM.Entities.ActivityType.ActivityType Type { get; set; }
        public int ModuleId { get; set; }
        public ActivityType.ActivityType Module { get; set; }
        public long ReferenceId { get; set; }
        public int? StatusId { get; set; }
        public ActivityType.ActivityType Status { get; set; }
        public int? TaskStatusId { get; set; }
        public ActivityType.ActivityType TaskStatus { get; set; }
        public int? TaskPriorityId { get; set; }
        public ActivityType.ActivityType TaskPriority { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? DateStart { get; set; }
        public bool? Direction { get; set; }
        public User CreatorUser { get; set; }
        public User LastModifierUser { get; set; }
        public List<ActivityReminder.ActivityReminder> ActivityReminder { get; set; }
        public List<ActivityUser.ActivityUser> ActivityUser { get; set; }
        public List<ActivityOrganizationUnit.ActivityOrganizationUnit> ActivityOrganizationUnit { get; set; }
    }
}
