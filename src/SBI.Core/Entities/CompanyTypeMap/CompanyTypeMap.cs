﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.CompanyTypeMap
{
    public class CompanyTypeMap : FullAuditedEntity<long>, IPassivable
    {
        public long CompanyId { get; set; }
        public Company.Company Company { get; set; }
        public int CompanyTypeId { get; set; }
        public OtherCategory.OtherCategory CompanyType { get; set; }
        public bool IsActive { get; set; }
    }
}
