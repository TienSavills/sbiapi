﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using CRM.Entities.PropertyTypes;

namespace CRM.Entities.PropertyTypeMaps
{
    public partial class PropertyTypeMap : CreationAuditedEntity<long>, IPassivable
    {
        public long PropertyId { get; set; }
        public int PropertyTypeId { get; set; }
        public bool IsActive { get; set; }
        public Property Property { get; set; }
        public PropertyType PropertyType { get; set; }
    }
}
