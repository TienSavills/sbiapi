﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.Inquiries
{
    public class Inquiry : FullAuditedEntity<long>, IPassivable
    {
        public long ClientId { get; set; }
        public int TypeId { get; set; }
        public string Description { get; set; }
        public decimal? FromSize { get; set; }
        public decimal? ToSize { get; set; }
        public int StatusId { get; set; }
        public long? FromPrice { get; set; }
        public long? ToPrice { get; set; }
        public int? FacingId { get; set; }
        public int SourceId { get; set; }
        public int? LivingRoom { get; set; }
        public int? BedRoom { get; set; }
        public int? HelperRoom { get; set; }
        public int? BathRoom { get; set; }
        public int? Balcony { get; set; }
        public int? MotobikePark { get; set; }
        public int? CarPark { get; set; }
        public bool IsActive { get; set; }
    }
}
