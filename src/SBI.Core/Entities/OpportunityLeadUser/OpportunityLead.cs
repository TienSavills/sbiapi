﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using CRM.Entities.Properties;
using CRM.Entities.Opportunity;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;

namespace CRM.Entities.OpportunityLeadUser
{
    public class OpportunityLead : FullAuditedEntity<long>, IPassivable
    {
        public long OpportunityId { get; set; }
        public Entities.Opportunity.Opportunity Opportunity { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public bool IsActive { get; set; }
    }
}
