﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
//using CRM.Entities.TenantTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.PropertyTenants
{
    public partial class PropertyTenant : FullAuditedEntity<long>
    {
        public long PropertyId { get; set; }
        public long CompanyId { get; set; }
        public bool IsActive { get; set; }
        public Company.Company Company { get; set; }
        public Property Property { get; set; }
        public int TenantTypeId { get; set; }
        public TenantType TenantType { get; set; }
    }
}
