﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Entities.Properties;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.PropertyOrganizationUnit
{
    public class PropertyOrganizationUnit : FullAuditedEntity<long>
    {
        public long PropertyId { get; set; }
        public Property Property { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnit OrganizationUnit { get; set; }
        public bool IsActive { get; set; }
    }
}
