﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Grades;
using CRM.Entities.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ProjectGradeMaps
{
    public partial class ProjectGradeMap:CreationAuditedEntity<long>
    {
        public long ProjectId { get; set; }
        public int GradeId { get; set; }
        public bool IsActive { get; set; }
        public Project Project { get; set; }
        public Grade Grade { get; set; }
    }
}
