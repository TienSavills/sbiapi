﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.MacroIndicators
{
    public class MacroIndicator:FullAuditedEntity<long>
    {
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public DateTime? DataDate { get; set; }
        public int? Domestic { get; set; }
        public int? International { get; set; }
        public int? FDINumberOfProject { get; set; }
        public int? Population { get; set; }
        public int? AirportCapacity { get; set; }
        public decimal? TourismRevenue { get; set; }
        public decimal? GDP { get; set; }
        public decimal? FDIRegistered { get; set; }
        public bool IsActive { get; set; }
    }
}
