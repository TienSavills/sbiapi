﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.PropertyTenants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.TenantTypes
{
    public partial class TenantType:CreationAuditedEntity<int>
    {
        public  TenantType()
        {
            //ProjectTenant = new HashSet<ProjectTenant.ProjectTenant>();
            PropertyTenant = new HashSet<PropertyTenant>();
        }
        public string Code { get; set; }
        public string TypeName { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        //public ICollection<ProjectTenant.ProjectTenant> ProjectTenant { get; set; }
        public ICollection<PropertyTenant> PropertyTenant { get; set; }
    }
}
