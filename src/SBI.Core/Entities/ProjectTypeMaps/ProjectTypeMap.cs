﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;
//using CRM.Entities.PropertyLandlords;
using CRM.Entities.PropertyTypes;

namespace CRM.Entities.ProjectTypeMaps
{
    public partial class ProjectTypeMap : CreationAuditedEntity<long>, IPassivable
    {
        public long ProjectId { get; set; }
        public int PropertyTypeId { get; set; }
        public bool IsActive { get; set; }

        public Project Project { get; set; }
        public PropertyType PropertyType { get; set; }
    }
}
