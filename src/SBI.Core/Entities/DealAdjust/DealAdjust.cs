﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.DealAdjust
{
    public class DealAdjust : FullAuditedEntity<long>, IPassivable
    {
        public long DealId { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnit OrganizationUnit { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IsActive { get; set; }
    }
}
