﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Facilities;

namespace CRM.Entities.UnitViewMaps
{
    public partial class UnitViewMaps : FullAuditedEntity<long>, IPassivable
    {
        public long UnitId { get; set; }
        public int ViewId { get; set; }
        public bool IsActive { get; set; }
        public Unit.Unit Unit { get; set; }
        public View.View View { get; set; }
    }
}
