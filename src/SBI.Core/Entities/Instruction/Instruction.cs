﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Instruction
{
   public class Instruction : CreationAuditedEntity<int>
    {
        public string InstructionName { get; set; }
        public string InstructionDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
