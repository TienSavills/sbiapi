﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Facilities;
using CRM.Entities.Projects;

namespace CRM.Entities.ProjectTransportationMaps
{
    public partial class ProjectTransportationMaps : CreationAuditedEntity<long>,IPassivable
    {
        public long ProjectId { get; set; }
        public int TransportationId { get; set; }
        public bool IsActive { get; set; }

        public Transportation.Transportation Transportation { get; set; }
        public Project Project { get; set; }
    }
}
