﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.DealOpportunity
{
    public class DealOpportunity:FullAuditedEntity<long>, IPassivable
    {
        public long DealId { get; set; }
        public Deal.Deal Deal { get; set; }
        public long OpportunityId { get; set; }
        public Opportunity.Opportunity Opportunity { get; set; }
        public bool IsActive { get; set; }
    }
}
