﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Company;
using CRM.Entities.OpportuityCategory;
using CRM.Entities.Properties;
using CRM.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Opportunity
{
    public class Opportunity : FullAuditedEntity<long>, IPassivable
    {
        public long CompanyId { get; set; }
        public Company.Company Company { get; set; }
        public long? RequestId { get; set; }
        public Request Request { get; set; }
        public int? CurrencyId { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? SourceId { get; set; }
        public int? IndustryId { get; set; }
        public int StatusId { get; set; }
        public OpportunityCategory Status { get; set; }
        public int? StageId { get; set; }
        public decimal? Probability { get; set; }
        public int? ClientTypeId { get; set; }
        public string OpportunityName { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public long? BudgetAmount { get; set; }
        public string FullPostalAddress { get; set; }
        public DateTime? ExpectedCloseDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public int? PrimaryInstructionId { get; set; }
        public int? PrimaryAssetClassId { get; set; }
        public long? OrganizationUnitId { get; set; }
        public bool IsActive { get; set; }
        public long? ValuationValues { get; set; }
        public string GeoCode { get; set; }
        public long? UnitId { get; set; }
        public decimal? Size { get; set; }
    }
}
