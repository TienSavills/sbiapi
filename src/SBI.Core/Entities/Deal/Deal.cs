﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Authorization.Users;
using CRM.Entities.OpportuityCategory;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Deal
{
    public class Deal : FullAuditedEntity<long>, IPassivable
    {
        public string DealName { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public decimal FeeAmount { get; set; }
        public int StatusId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string ContractNumber { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnit OrganizationUnit { get; set; }
        public OpportunityCategory Status { get; set; }
        public User CreatorUser { get; set; }
        public User LastModifierUser { get; set; }
        public decimal? PercentCompleted { get; set; }
        public DateTime? CommencementDate { get; set; }
        public DateTime? ReportDate { get; set; }
        public long? ValuationValues { get; set; }
        public int? Term { get; set; }
        public long? ContactId { get; set; }
        public long? CompanyId { get; set; }
        public string ATTN { get; set; }
        public string InvoiceLegalName { get; set; }
        public string InvoiceBusinessName { get; set; }
        public string CompanyAddress { get; set; }
        public string AddSavills { get; set; }
        public string CompanyVATCode { get; set; }
        public string EmployeeSign { get; set; }
        public string TitleEmployee { get; set; }
        public string ContactAddress { get; set; }
        public string ContactVATCode { get; set; }
        public DateTime? DraftReport { get; set; }
        public DateTime? FinalReport { get; set; }
    }
}
