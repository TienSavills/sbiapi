﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;

namespace CRM.Entities.UnitAddress
{
    public partial class UnitAddress : FullAuditedEntity<long>, IPassivable
    {
        public long UnitId { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public bool IsActive { get; set; }
        public Unit.Unit Unit { get; set; }
    }
}
