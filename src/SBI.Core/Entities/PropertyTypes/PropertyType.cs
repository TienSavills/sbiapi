﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.ProjectTypeMaps;
using CRM.Entities.PropertyTypeMaps;

namespace CRM.Entities.PropertyTypes
{
    public partial class PropertyType : FullAuditedEntity<int>, IPassivable
    {
        public PropertyType()
        {
            PropertyTypeMap = new HashSet<PropertyTypeMap>();
            ProjectTypeMap = new HashSet<ProjectTypeMap>();
        }

        public string Code { get; set; }
        public string TypeName { get; set; }
        public string Target { get; set; }
        public bool IsActive { get; set; }

        public ICollection<PropertyTypeMap> PropertyTypeMap { get; set; }
        public ICollection<ProjectTypeMap> ProjectTypeMap { get; set; }
    }
}
