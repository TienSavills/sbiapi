﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Floor
{
    public class Floor : FullAuditedEntity<long>
    {
        public long ProjectId { get; set; }
        public Project Project { get; set; }
        public string FloorName { get; set; }
        public decimal? Size { get; set; }
        public int? Order { get; set; }
        public bool IsActive { get; set; }
    }
}
