﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using CRM.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.RequestUser
{
    public class RequestUser : FullAuditedEntity<long>, IPassivable
    {
        public long RequestId { get; set; }
        public Request Request { get; set; }
        public long UserID { get; set; }
        public User User { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
