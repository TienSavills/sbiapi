﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.CompanyUser
{
    public class CompanyUser : FullAuditedEntity<long>, IPassivable
    {
        public long CompanyId { get; set; }
        public Company.Company Company { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
