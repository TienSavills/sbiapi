﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Unit
{
    public class Unit : FullAuditedEntity<long>, IPassivable
    {
        public long? FloorId { get; set; }
        public Entities.Floor.Floor Floor { get; set; }
        public string UnitName { get; set; }
        public decimal? Size { get; set; }
        public int StatusId { get; set; }
        public UnitCategory.UnitCategory Status { get; set; }
        public string Description { get; set; }
        public int UnitTypeId { get; set; }
        public UnitCategory.UnitCategory UnitType { get; set; }
        public decimal? Monthly { get; set; }
         public decimal? Per { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public User CreatorUser { get; set; }
        public User LastModifierUser { get; set; }
        public long? Price { get; set; }
        public int? MotobikePark { get; set; }
        public int? CarPark { get; set; }
        public int? Balcony { get; set; }
        public int? HelperRoom { get; set; }
        public int BedRoom { get; set; }
        public int BathRoom { get; set; }
        public int? LivingRoom { get; set; }
        public int FacingId { get; set; }
        public decimal? SaleAble { get; set; }
        public decimal? Gross { get; set; }
        public bool? IsPet { get; set; }
    }
}
