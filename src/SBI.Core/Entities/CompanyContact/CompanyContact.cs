﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Company;
using CRM.Entities.Contacts;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.CompanyContact
{
    public class CompanyContact : FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public Contact Contact { get; set; }
        public long CompanyId { get; set; }
        public bool? IsPrimary { get; set; }
        public Company.Company Company { get; set; }
        public bool IsActive { get; set; }
    }
}
