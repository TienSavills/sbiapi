﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Inquiries;

namespace CRM.Entities.InquiryAddress
{
    public partial class InquiryAddress : FullAuditedEntity<long>, IPassivable
    {
        public long InquiryId { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public bool IsActive { get; set; }
        public Inquiry Inquiry { get; set; }
    }
}
