﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ActivityReminder
{
    public class ActivityReminder : FullAuditedEntity<long>
    {
        public long ActivityId { get; set; }
        public Activity.Activity Activity{get;set;}
        public int ModuleId { get; set; }
        public ActivityType.ActivityType Module { get; set; }
        public long ReferenceId { get; set; }
        public int TypeId { get; set; }
        public ActivityType.ActivityType Type { get; set; }
        public int FormatId { get; set; }
        public ActivityType.ActivityType Format { get; set; }
        public int Value { get; set; }
        public bool? IsReminder { get; set; }
        public bool IsActive { get; set;  }
        public User CreatorUser { get; set; }
        public User LastModifierUser { get; set; }
    }
}
