﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using CRM.Entities.Facilities;

namespace CRM.Entities.PropertyFacilityMaps
{
    public partial class PropertyFacilityMap : CreationAuditedEntity<long>, IPassivable
    {
        public long PropertyId { get; set; }
        public int FacilityId { get; set; }
        public bool IsActive { get; set; }
        public Property Property { get; set; }
        public Facility Facility { get; set; }
    }
}
