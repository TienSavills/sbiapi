﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.OpportunityProperty
{
    public class OpportunityProperty:FullAuditedEntity<long>, IPassivable
    {
        public long PropertyId { get; set; }
        public Property Property { get; set; }
        public long OpportunityId { get; set; }
        public bool IsActive { get; set; }
    }
}
