﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Contacts;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.PropertyContact
{
    public class PropertyContact:FullAuditedEntity<long>
    {
        public long ContactId { get; set; }
        public Contact Contact { get; set; }
        public long PropertyId { get; set; }
        public bool IsActive { get; set; }
    }
}
