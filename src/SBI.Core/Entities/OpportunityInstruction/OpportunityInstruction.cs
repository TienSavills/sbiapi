﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.OpportuityCategory;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.OpportunityInstruction
{
    public class OpportunityInstruction:FullAuditedEntity<long>
    {
        public long OpportunityId { get; set; }
        public Entities.Opportunity.Opportunity Opportunity { get; set; }
        public int InstructionId { get; set; }
        public OpportunityCategory Instruction { get; set; }
        public bool IsActive { get; set; }
    }
}
