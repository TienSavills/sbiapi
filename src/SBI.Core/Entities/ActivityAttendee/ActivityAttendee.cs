﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using CRM.Entities.ActivityType;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ActivityAttendee
{
    public class ActivityAttendee:FullAuditedEntity<long>
    {
        public long ActivityId { get; set; }
        public int ModuleId { get; set; }
        public ActivityType.ActivityType Module { get; set; }
        public long ReferenceId { get; set; }
        public int StatusId { get; set; }
        public ActivityType.ActivityType Status { get; set; }
        public bool IsActive { get; set; }
        public User CreatorUser { get; set; }
        public User LastModifierUser { get; set; }
    }
}
