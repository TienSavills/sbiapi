﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ActivityOrganizationUnit
{
    public class ActivityOrganizationUnit : FullAuditedEntity<long>
    {
        public long ActivityId { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnit OrganizationUnit { get; set; }
        public bool IsActive { get; set; }
    }
}
