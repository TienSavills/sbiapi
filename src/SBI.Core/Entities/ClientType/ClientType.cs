﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ClientType
{
    public class ClientType : CreationAuditedEntity<int>
    {
        public string ClientTypeName { get; set; }
        public string ClientTypeDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
