﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Level
{
    public class Level:CreationAuditedEntity<int>
    {
        public string LevelName { get; set; }
        public string LevelDescription { get; set; }
        public bool IsActive { get; set; }
    }
}