﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.OpportunityOrganizationUnit
{
    public class OpportunityOrganizationUnit : FullAuditedEntity<long>, IPassivable
    {
        public long OpportunityId { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnit OrganizationUnit { get; set; }
        public int? InstructionId { get; set; }
        public long? FeeAmount { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPrimary { get; set; }
    }
}
