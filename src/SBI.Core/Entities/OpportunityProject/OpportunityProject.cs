﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.OpportunityProject
{
    public class OpportunityProject:FullAuditedEntity<long>, IPassivable
    {
        public long ProjectId { get; set; }
        public Project Project { get; set; }
        public long OpportunityId { get; set; }
        public bool IsActive { get; set; }
    }
}
