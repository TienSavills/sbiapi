﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;
using CRM.Entities.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Contacts
{
    public partial class Contact : FullAuditedEntity<long>,IPassivable
    {
        public string CAPID { get; set; }
        public string Description { get; set; }
        public bool? IsVietnamese { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Title { get; set; }
        public int? LevelId { get; set; }
        public int? IndustryId { get; set; }
        public int? ClientTypeId { get; set; }
        public int? LeadSourceId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsNotCall { get; set; }
         public bool? EmailOptOut { get; set; }
        public long? PropertyId { get; set; }
        public Property Property { get; set; }
        public long? RequestId { get; set; }
        public Request Request { get; set; }
        public bool? IsVerified { get; set; }
        public int? NationalityId { get; set; }
        public int? ContactTypeId { get; set; }
    }
}
