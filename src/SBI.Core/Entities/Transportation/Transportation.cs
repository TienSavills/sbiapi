﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.Transportation
{
    public partial class Transportation : CreationAuditedEntity<int>, IPassivable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Icon { get; set; }
    }
}
