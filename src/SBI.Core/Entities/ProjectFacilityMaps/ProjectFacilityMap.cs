﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Facilities;
using CRM.Entities.Projects;

namespace CRM.Entities.ProjectFacilityMaps
{
    public partial class ProjectFacilityMap:CreationAuditedEntity<long>,IPassivable
    {
        public long ProjectId { get; set; }
        public int FacilityId { get; set; }
        public bool IsActive { get; set; }

        public Facility Facility { get; set; }
        public Project Project { get; set; }
    }
}
