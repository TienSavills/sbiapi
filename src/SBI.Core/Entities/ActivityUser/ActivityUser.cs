﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using CRM.Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ActivityUser
{
    public class ActivityUser : FullAuditedEntity<long>
    {
        public long ActivityId { get; set; }
        public CRM.Entities.Activity.Activity Activity { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
