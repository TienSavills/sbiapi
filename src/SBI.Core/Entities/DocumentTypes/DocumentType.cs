﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.AttachmentDocuments;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.DocumentTypes
{
    public partial class DocumentType:CreationAuditedEntity<int>
    {
        public  string TypeName { get; set; }
        public   bool IsActive { get; set; }
    }
}
