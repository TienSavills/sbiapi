﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Grades;
using CRM.Entities.Properties;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.PropertyGradeMaps
{
    public partial class PropertyGradeMap : CreationAuditedEntity<long>
    {
        public long PropertyId { get; set; }
        public int GradeId { get; set; }
        public bool IsActive { get; set; }
        public Property Property { get; set; }
        public Grade Grade { get; set; }
    }
  
}
