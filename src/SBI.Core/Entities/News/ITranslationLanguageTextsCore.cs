﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Dependency;
using CRM.Localization;

namespace CRM.Entities.News
{
    public interface ITranslationLanguageTextsCore : ITransientDependency
    {
        Task UpdateTranslations(Guid guid, List<LocalizationDto> localizations);
        Task AppendTranslation<T>(Action<T, NewLanguageTexts> repare, Func<T, Guid?> select, params T[] models);
        Task AppendTranslations<T>(Func<T, Guid?> selectKey, Action<T, List<LocalizationDto>> repareModel,
            params T[] models);
    }
}
