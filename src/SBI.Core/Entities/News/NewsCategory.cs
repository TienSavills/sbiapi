﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.News
{
    public partial class NewsCategory : CreationAuditedEntity<int>, IPassivable
    {
        public NewsCategory()
        {
            News = new HashSet<News>();
        }
        public int? ParentId { get; set; }
        public Guid NameId { get; set; }
        public string TypeCode { get; set; }
        public string Description { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<News> News { get; set; }
    }
}
