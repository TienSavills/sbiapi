﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;

namespace CRM.Entities.News
{
    public partial class News : FullAuditedEntity<long>, IPassivable
    {
        public string TypeCode { get; set; }
        public Guid UniqueId { get; set; }
        public int CategoryId { get; set; }
        public Guid SubjectNameId { get; set; }
        public Guid ContentNameId { get; set; }
        public Guid ShortDescriptionNameId { get; set; }
        public global::System.DateTime? StartTime { get; set; }
        public global::System.DateTime? EndTime { get; set; }
        public int SortOrder { get; set; }
        public virtual User CreatorUser { get; set; }
        public virtual User LastModifierUser { get; set; }
        public virtual NewsCategory Category { get; set; }
        public bool IsActive { get; set; }
        public bool IsPublic { get; set; }
    }
}
