﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.News
{
    public partial class NewLanguageTexts : FullAuditedEntity<int>
    {
        public Guid Key { get; set; }
        public string Value { get; set; }
        public string LanguageName { get; set; }
    }
}
