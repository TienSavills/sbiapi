﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Localization;
using Abp.MultiTenancy;
using CRM.Localization;
using Microsoft.EntityFrameworkCore;

namespace CRM.Entities.News
{
    public class TranslationLanguageTextsCore : SbiDomainServiceBase, ITranslationLanguageTextsCore
    {
        private readonly IApplicationLanguageManager _applicationLanguageManager;
        private readonly IRepository<NewLanguageTexts, int> _repositoryTranslationLanguageTexts;
        public TranslationLanguageTextsCore(IApplicationLanguageManager applicationLanguageManager, IRepository<NewLanguageTexts, int> repositoryTranslationLanguageTexts)
        {
            _applicationLanguageManager = applicationLanguageManager;
            _repositoryTranslationLanguageTexts = repositoryTranslationLanguageTexts;
        }
        public async Task UpdateTranslation(Guid guid, string value, string culture = null)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (string.IsNullOrEmpty(culture))
                    culture = Thread.CurrentThread.CurrentUICulture.Name;
                var entity = await _repositoryTranslationLanguageTexts.FirstOrDefaultAsync(x => x.Key == guid && x.LanguageName == culture);
                if (entity != null)
                {
                    entity.Value = value;
                    await _repositoryTranslationLanguageTexts.UpdateAsync(entity);
                }
                else
                {
                    entity = new NewLanguageTexts
                    {
                        Key = guid,
                        Value = value,
                        LanguageName = culture
                    };
                    await _repositoryTranslationLanguageTexts.InsertAndGetIdAsync(entity);
                }
            }
        }
        public async Task UpdateTranslations(Guid guid, List<LocalizationDto> localizations)
        {
            if (localizations != null && localizations.Any())
            {
                foreach (var localization in localizations)
                    await UpdateTranslation(guid, localization.Value, localization.LanguageName);
            }
        }
        public async Task AppendTranslation<T>(Action<T, NewLanguageTexts> repare, Func<T, Guid?> select, params T[] models)
        {
            if (models != null && models.Any())
            {
                var guids = models.Select(select).Where(x => x.HasValue).ToList();
                var lang = Thread.CurrentThread.CurrentUICulture.Name;
                var entities = await _repositoryTranslationLanguageTexts.GetAll().Where(x => guids.Contains(x.Key) && x.LanguageName == lang).AsNoTracking().ToListAsync();
                foreach (var model in models)
                {
                    var entity = entities.FirstOrDefault(x => x.Key == select.Invoke(model));
                    if (entity != null)
                        repare.Invoke(model, entity);
                }
            }
        }

        public async Task AppendTranslations<T>(Func<T, Guid?> selectKey, Action<T, List<LocalizationDto>> repareModel, params T[] models)
        {
            if (models != null && models.Any())
            {
                var keys = models.Select(selectKey).ToList();
                var languages = await _applicationLanguageManager.GetLanguagesAsync(null);
                var entities = await _repositoryTranslationLanguageTexts.GetAll().Where(x => keys.Contains(x.Key))
                    .ToListAsync();
                foreach (var model in models)
                {
                    var langs =
                        from a in languages.Where(x=>x.IsDisabled==false)
                        join b in entities.Where(x => x.Key == selectKey.Invoke(model))
                            on a.Name equals b.LanguageName into bG
                        from bS in bG.DefaultIfEmpty()
                        select new LocalizationDto
                        {
                            LanguageName = a.Name,
                            Value = bS?.Value ?? ""
                        };
                    var langModels = langs.ToList();
                    repareModel.Invoke(model, langModels);
                }
            }
        }
    }
}
