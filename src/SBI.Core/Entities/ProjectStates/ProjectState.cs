﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ProjectStates
{
    public partial class ProjectState:CreationAuditedEntity<int>
    {
        public string ProjectStateName { get; set; }
        public bool IsActive { get; set; }
    }
}
