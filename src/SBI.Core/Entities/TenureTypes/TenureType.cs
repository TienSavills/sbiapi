﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.TenureTypes
{
    public partial class TenureType:CreationAuditedEntity<int>
    {
        public string TenureTypeName { get; set; }
        public bool IsActive { get; set; }
    }
}
