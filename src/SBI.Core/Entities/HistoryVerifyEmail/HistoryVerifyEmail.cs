﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.HistoryVerifyEmail
{
    public class HistoryVerifyEmail:CreationAuditedEntity<long>
    {
        public long ContactEmailId { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public string Result { get; set; }
        public string Reason { get; set; }
        public string Role { get; set; }
        public string FeeEmail { get; set; }
        public string SendTransactional { get; set; }
    }
}
