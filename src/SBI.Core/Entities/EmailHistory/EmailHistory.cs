﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.EmailHistory
{
    public class EmailHistory:CreationAuditedEntity<long>
    {
        public string Module { get; set; }
        public long ContactId { get; set; }
        public Contacts.Contact Contact { get; set; }
        public long CampaignId { get; set; }
        public Campaign.Campaign Campaign { get; set; }
        public string SentTo { get; set; }
        public string EmailSubject { get; set; }
        public string EmailContent { get; set; }
        public string ResultBody { get; set; }
        public int? StatusCode { get; set; }
        public bool IsSuccess { get; set; }
    }
}
