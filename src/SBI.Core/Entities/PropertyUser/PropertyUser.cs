﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using CRM.Entities.Properties;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.PropertyUser
{
    public class PropertyUser : FullAuditedEntity<long>
    {
        public long PropertyId { get; set; }
        public Property Property { get; set; }
        public long UserID { get; set; }
        public User User { get; set; }
        public bool IsFollwer { get; set; }
        public bool IsActive { get; set; }
    }
}
