﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.Industry
{
    public class Industry : CreationAuditedEntity<int>
    {
        public string IndustryName { get; set; }
        public string IndustryDescription { get; set; }
        public int? ParentId { get; set; }
        public string TypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
