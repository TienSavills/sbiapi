﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Currencys;
using CRM.Entities.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ProjectClaims
{
    public class ProjectClaim : FullAuditedEntity<long>
    {
        public long ProjectId { get; set; }
        public int? NumberOfUnits { get; set; }
        public int? ConstructionStatusId { get; set; }
        public int? OccupancyUnit { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? RentPrice { get; set; }
        public bool? IncludedVAT { get; set; }
        public decimal? VATRate { get; set; }
        public string Description { get; set; }
        public bool? IncludedServiceCharge { get; set; }
        public decimal? ServiceChargeRate { get; set; }
        public decimal? LeasableTotalArea { get; set; }
        public decimal? LeasedArea { get; set; }
        public DateTime? DataDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool IsActive { get; set; }
        public Project Project { get; set; }
        public Currency Currency { get; set; }
        public ConstructionStatus.ConstructionStatus ConstructionStatus { get; set; }
        public int? DataPeriod { get; set; }
        public int? Period { get; set; }
        public int ResearchTypeId { get; set; }
        public int? Year { get; set; }
        public DateTime? ExpectedLaunchingTime { get; set; }

        public int? ProjectStateId { get; set; }
        public ProjectStates.ProjectState ProjectState { get; set; }
    }
}
