﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.DealShare
{
    public class DealShare:FullAuditedEntity<long>, IPassivable
    {
        public long DealId { get; set; }
        public long? OrganizationUnitId { get; set; }
        public OrganizationUnit OrganizationUnit { get; set; }
        public int? InstructionId { get; set; }
        public long? UserId { get; set; }
        public User User { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
