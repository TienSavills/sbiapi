﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.Currencys
{
    public partial class Currency:CreationAuditedEntity<int>
    {
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
    }
}
