﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.DocumentTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.AttachmentDocuments
{
    public class AttachmentDocument:FullAuditedEntity<long>, IPassivable,IMustHaveTenant
    {
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string FilePath { get; set; }
        public long ParentId { get; set; }
        public string ParentModuleName { get; set; }
        [DefaultValue (true)]
        public bool IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsMainPhoto { get; set; }
        public int TenantId { get; set; }
        public string Extension { get; set; }
        public string Type { get; set; }
        public int OrderNum { get; set; }
        public int DocumentTypeId { get; set; }
        public DocumentType DocumentType { get; set; }
    }
}
