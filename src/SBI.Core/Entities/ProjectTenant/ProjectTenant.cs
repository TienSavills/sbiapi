﻿using Abp.Domain.Entities.Auditing;
using CRM.Entities.Projects;
using CRM.Entities.TenantTypes;

namespace CRM.Entities.ProjectTenant
{
    public partial class ProjectTenant:FullAuditedEntity<long>
    {
        public long ProjectId { get; set; }
        public int TenantTypeId { get; set; }
        public long CompanyId { get; set; }
        public bool IsActive { get; set; }
        public Company.Company Company { get; set; }
        public Project Project { get; set; }
        public TenantType TenantType { get; set; }
    }
}
