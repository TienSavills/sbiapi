﻿using Abp.Domain.Entities.Auditing;
using CRM.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.CommentSendMail
{
    public class CommentSendEmail:FullAuditedEntity<long>
    {
        public long CommentId { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public bool? IsSend { get; set; }
        public bool IsActive { get; set; }
    }
}
