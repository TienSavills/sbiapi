﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.OpportunityContact
{
    public class OpportunityContact : FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public Contacts.Contact Contact { get; set; }
        public long OpportunityId { get; set; }
        public bool? IsPrimary { get; set; }
        public bool IsActive { get; set; }
    }
}
