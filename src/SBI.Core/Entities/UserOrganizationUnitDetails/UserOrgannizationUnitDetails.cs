﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.UserOrganizationUnitDetails
{
    public class UserOrganizationUnitDetails:CreationAuditedEntity<long>
    {
        public long UserOrganizationUnitId { get; set; }
        public bool IsHead { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
