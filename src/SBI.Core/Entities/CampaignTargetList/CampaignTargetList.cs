﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.CampaignTarget
{
    public class CampaignTargetList : FullAuditedEntity<long>
    {
        public long CampaignId { get; set; }
        public long TargetId { get; set; }
        public bool IsActive { get; set; }
        public int TypeId { get; set; }
    }
}
