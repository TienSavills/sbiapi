﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CRM.Entities.CompanyAddress
{
    public class CompanyAddress:FullAuditedEntity<long>,IPassivable
    {
        public long CompanyId { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool? IsPrimary { get; set; }
        public Company.Company Company { get; set; }
    }
}
