﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.ContactOrganizationUnit
{
    public class ContactOrganizationUnit : FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public long OrganizationUnitId { get; set; }
        public OrganizationUnit OrganizationUnit { get; set; }
        public bool IsActive { get; set; }
    }
}
