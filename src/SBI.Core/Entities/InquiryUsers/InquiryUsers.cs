﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace CRM.Entities.InquiryUsers
{
    public class InquiryUsers : FullAuditedEntity<long>,IPassivable
    {
      public long InquiryId { get; set; }
      public long UserId { get; set; }
      public bool IsActive { get; set; }
    }
}
