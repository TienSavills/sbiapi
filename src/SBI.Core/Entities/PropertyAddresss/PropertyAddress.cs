﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Properties;

namespace CRM.Entities.PropertyAddresss
{
    public partial class PropertyAddress : FullAuditedEntity<long>, IPassivable
    {
        public long PropertyId { get; set; }
        public int CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public bool IsActive { get; set; }
        public Property Property { get; set; }
    }
}
