﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.AssetClass
{
    public class AssetClass : CreationAuditedEntity<int>
    {
        public string AssetClassName { get; set; }
        public string AssetClassDescription { get; set; }
        public bool IsActive { get; set; }
    }
}
