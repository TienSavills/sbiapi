﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Contacts;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Entities.RequestContact
{
    public class RequestContact:FullAuditedEntity<long>, IPassivable
    {
        public long ContactId { get; set; }
        public Contact Contact { get; set; }
        public long RequestId { get; set; }
        public bool IsActive { get; set; }
    }
}
