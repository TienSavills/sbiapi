﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CRM.Entities.Inquiries;

namespace CRM.Entities.InquiryViewMaps
{
    public partial class InquiryViewMaps : FullAuditedEntity<long>, IPassivable
    {
        public long InquiryId { get; set; }
        public int ViewId { get; set; }
        public bool IsActive { get; set; }
        public Inquiry Inquiry { get; set; }
        public View.View View { get; set; }
    }
}
