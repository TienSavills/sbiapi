﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Microsoft.EntityFrameworkCore;

namespace CRM
{
    public abstract class SbiDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected SbiDomainServiceBase()
        {
            LocalizationSourceName = SBIConsts.LocalizationSourceName;
        }

        public async Task UpdatePassivableChild<TModelChild, TEntityChild, TEntityChildKey>(
            IRepository<TEntityChild, TEntityChildKey> repositoryChild,
            Action<TEntityChild> repareAdd,
            Expression<Func<TEntityChild, bool>> select,
            Func<TModelChild, TEntityChild, bool> compare,
            params TModelChild[] models
        )
            where TModelChild : Entity<TEntityChildKey>
            where TEntityChild : Entity<TEntityChildKey>, IPassivable
        {
            var repositoryChildEntities =
                await repositoryChild.GetAll().Where(select).ToListAsync();
            var willDelete = repositoryChildEntities.Where(x => !models.Any(m => compare(m, x))).ToList();
            if (willDelete.Any())
                foreach (var item in willDelete)
                {
                    item.IsActive = false;
                    await repositoryChild.UpdateAsync(item);
                }

            var willUpdate = repositoryChildEntities.Where(x => models.Any(m => compare(m, x))).ToList();
            if (willUpdate.Any())
                foreach (var item in willUpdate)
                {
                    var key = item.Id;
                    ObjectMapper.Map(models.FirstOrDefault(m => compare(m, item)), item);
                    item.Id = key;
                    item.IsActive = true;
                    await repositoryChild.UpdateAsync(item);
                }

            var willAdd = models.Where(x => !repositoryChildEntities.Any(e => compare(x, e))).ToList();
            if (willAdd.Any())
                foreach (var item in willAdd)
                {
                    var addEntity = ObjectMapper.Map<TEntityChild>(item);
                    addEntity.IsActive = true;
                    addEntity.Id = default;
                    repareAdd?.Invoke(addEntity);
                    await repositoryChild.InsertAndGetIdAsync(addEntity);
                }
        }

        public async Task UpdateChild<TModelChild, TEntityChild, TEntityChildKey>(
            IRepository<TEntityChild, TEntityChildKey> repositoryChild,
            Action<TEntityChild> repareAdd,
            Expression<Func<TEntityChild, bool>> select,
            Func<TModelChild, TEntityChild, bool> compare,
            params TModelChild[] models
        )
            where TModelChild : Entity<TEntityChildKey>
            where TEntityChild : Entity<TEntityChildKey>
        {
            var repositoryChildEntities =
                await repositoryChild.GetAll().Where(select).ToListAsync();
            var willDelete = repositoryChildEntities.Where(x => !models.Any(m => compare(m, x))).ToList();
            if (willDelete.Any())
                foreach (var item in willDelete)
                {
                    await repositoryChild.DeleteAsync(item);
                }

            var willUpdate = repositoryChildEntities.Where(x => models.Any(m => compare(m, x))).ToList();
            if (willUpdate.Any())
                foreach (var item in willUpdate)
                {
                    var key = item.Id;
                    ObjectMapper.Map(models.FirstOrDefault(m => compare(m, item)), item);
                    item.Id = key;
                    await repositoryChild.UpdateAsync(item);
                }

            var willAdd = models.Where(x => !repositoryChildEntities.Any(e => compare(x, e))).ToList();
            if (willAdd.Any())
                foreach (var item in willAdd)
                {
                    var addEntity = ObjectMapper.Map<TEntityChild>(item);
                    addEntity.Id = default;
                    repareAdd?.Invoke(addEntity);
                    await repositoryChild.InsertAndGetIdAsync(addEntity);
                }
        }

        protected async Task UpdateChilds<TModelChild, TEntityChild, TEntityChildKey>(
            IRepository<TEntityChild, TEntityChildKey> repositoryChild,
            Action<TEntityChild> repareAdd,
            Expression<Func<TEntityChild, bool>> select,
            Func<TModelChild, TEntityChild, bool> compare,
            params TModelChild[] modes
        )
            where TModelChild : Entity<TEntityChildKey>
            where TEntityChild : Entity<TEntityChildKey>
        {
            var repositoryChildEntities =
                await repositoryChild.GetAll().Where(select).ToListAsync();
            var willDelete = repositoryChildEntities.Where(x => !modes.Any(m => compare(m, x))).ToList();
            if (willDelete.Any())
                foreach (var item in willDelete)
                {
                    await repositoryChild.DeleteAsync(item);
                }

            var willUpdate = repositoryChildEntities.Where(x => modes.Any(m => compare(m, x))).ToList();
            if (willUpdate.Any())
                foreach (var item in willUpdate)
                {
                    var key = item.Id;
                    var itemUpdate = modes.FirstOrDefault(m => compare(m, item));
                    if (itemUpdate != null)
                    {
                        itemUpdate.Id = key;
                        ObjectMapper.Map(itemUpdate, item);
                        await repositoryChild.UpdateAsync(item);
                    }
                }

            var willAdd = modes.Where(x => !repositoryChildEntities.Any(e => compare(x, e))).ToList();
            if (willAdd.Any())
                foreach (var item in willAdd)
                {
                    var addEntity = ObjectMapper.Map<TEntityChild>(item);
                    addEntity.Id = default;
                    repareAdd?.Invoke(addEntity);
                    item.Id = await repositoryChild.InsertAndGetIdAsync(addEntity);
                }
        }

        protected async Task AppendChild<TModel, TModelAppend, TEntity, TPrimaryKey>(
                IRepository<TEntity, TPrimaryKey> repository,
                Func<TModel, TPrimaryKey> select,
                Func<List<TEntity>, Task> repareEntity,
                Action<TModel, TModelAppend> repareModel,
                params TModel[] models)
            //where TModelAppend : SimpleOutput
            where TEntity : Entity<TPrimaryKey>
        {
            if (models != null && models.Any())
            {
                var ids = models.Select(select).Distinct().ToList();
                var entities = await repository.GetAll().Where(x => ids.Contains(x.Id)).AsNoTracking().ToListAsync();
                if (repareEntity != null)
                    await repareEntity(entities);
                foreach (var model in models)
                {
                    var entity = entities.FirstOrDefault(x => x.Id.Equals(select(model)));
                    repareModel(model, ObjectMapper.Map<TModelAppend>(entity));
                }
            }
        }
    }
}
