﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace CRM.ExportHelper
{
    public class ExcelWorkbookContent
    {
        public List<ExcelSheetContent> SheetDatas { get; set; }
    }
    public class ExcelSheetContent
    {
        public string SheetName { get; set; }

        public ExcelDataTableContent DataTableContent { get; set; }

        public List<ExcelCellContent> CellContents { get; set; }
    }
    public class ExcelDataTableContent
    {
        public DataTable DataTable { get; set; }
        public int HeaderRowIndex { get; set; }
        public int HeaderColIndex { get; set; }
        public bool IsExportHeader { get; set; }
        public bool IsBorderCell { get; set; }
    }
    public class ExcelCellContent
    {
        public int RowIndex { get; set; }
        public int ColIndex { get; set; }
        public string Content { get; set; }
        public string FormatValue { get; set; }
        public ExcelPictureData PictureData { get; set; }
    }
    public class ExcelPictureData
    {
        public string Path { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
    public class ReportResultModel
    {
        public bool IsPdf { get; set; }
        public MemoryStream MemoryStream { get; set; }
    }
}
