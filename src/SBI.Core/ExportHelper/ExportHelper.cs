﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
//using OfficeOpenXml;
//using OfficeOpenXml.Style;

namespace CRM.ExportHelper
{
    public class ExportHelper : IExportHelper
    {
        public ExportHelper()
        {
        }

        public MemoryStream ExportDataTableFile(ExcelDataTableContent dtContent)
        {
            throw new NotImplementedException();
        }

        public MemoryStream ExportSingleSheetFile(ExcelSheetContent sheetContent, string filePath)
        {
            throw new NotImplementedException();
        }

        public MemoryStream ExportWorkbook(ExcelWorkbookContent wbContent)
        {
            throw new NotImplementedException();
        }

        public MemoryStream ExportWorkbookFile(ExcelWorkbookContent wbContent, string filePath)
        {
            var file = GetFileInfo(filePath);
            if (file == null) return null;

            if (wbContent.SheetDatas == null) return null;

            using (var s = new OfficeOpenXml.ExcelPackage(file))
            {
                foreach (var sheetContent in wbContent.SheetDatas) BindData(sheetContent, s);

                return GetByteFromMs(s);
            }
        }

        public ExcelWorkbookContent GetWorkbookContent(DataTable dt)
        {
            throw new NotImplementedException();
        }

        public ExcelWorkbookContent GetWorkbookContent(DataTable dt, int headerColIndex, int headerRowIndex)
        {
            var dataTableContent = new ExcelDataTableContent { IsExportHeader = true, DataTable = dt, HeaderColIndex = headerColIndex, HeaderRowIndex = headerRowIndex, IsBorderCell = true };
            var wb = new ExcelWorkbookContent
            {
                SheetDatas = new List<ExcelSheetContent> {
                    new ExcelSheetContent { DataTableContent = dataTableContent, SheetName = "WeeklyReport" }
                }
            };
            return wb;
        }
        private static FileInfo GetFileInfo(string tempPath)
        {
            return string.IsNullOrEmpty(tempPath) ? null : new FileInfo(tempPath);
        }
        private static void BindData(ExcelSheetContent sheetContent, ExcelPackage s)
        {
            var ws = s.Workbook.Worksheets[sheetContent.SheetName] ?? CreateSheet(s, sheetContent.SheetName);
            ExportListCellData(ws, sheetContent.CellContents);
            ExportDatatable(ws, sheetContent.DataTableContent);

        }
        private static MemoryStream GetByteFromMs(ExcelPackage s)
        {
            return new MemoryStream(s.GetAsByteArray());
        }
        private static ExcelWorksheet CreateSheet(ExcelPackage package, string sheetName)
        {
            package.Workbook.Worksheets.Add(sheetName);
            var ws = package.Workbook.Worksheets[sheetName];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }
        private static void ExportListCellData(OfficeOpenXml.ExcelWorksheet ws, List<ExcelCellContent> listCellContents)
        {
            if (listCellContents == null) return;
            foreach (var cellContent in listCellContents)
            {
                var cell = ws.Cells[cellContent.RowIndex, cellContent.ColIndex];

                if (cellContent.PictureData == null)
                {
                    cell.Value = cellContent.Content;
                    if (!string.IsNullOrEmpty(cellContent.FormatValue))
                        cell.StyleName = cellContent.FormatValue;
                }
                else
                {
                    Image logo = null;

                    if (new Uri(cellContent.PictureData.Path).IsFile)
                        logo = Image.FromFile(cellContent.PictureData.Path);
                    else
                    {
                        var wc = new WebClient();
                        var imageStream = wc.OpenRead(cellContent.PictureData.Path);
                        if (imageStream != null)
                            logo = Image.FromStream(imageStream);
                    }

                    var picture = ws.Drawings.AddPicture(cellContent.Content, logo);
                    picture.From.Column = cellContent.ColIndex - 1;
                    picture.From.Row = cellContent.RowIndex - 1;
                    picture.SetSize(cellContent.PictureData.Width, cellContent.PictureData.Height);
                }
            }
        }
        private static void ExportDatatable(ExcelWorksheet ws, ExcelDataTableContent dtContent)
        {
            var rowEndIndex = dtContent.DataTable.Rows.Count + dtContent.HeaderRowIndex;
            var colEndIndex = dtContent.DataTable.Columns.Count + dtContent.HeaderColIndex - 1;

            var dataRange = ws.Cells[dtContent.HeaderRowIndex, dtContent.HeaderColIndex, rowEndIndex, colEndIndex];

            dataRange.LoadFromDataTable(dtContent.DataTable, dtContent.IsExportHeader);

            if (dtContent.IsBorderCell)
            {
                //Setting Top/left,right/bottom borders.
                var border = dataRange.Style.Border;
                border.Bottom.Style =
                    border.Top.Style =
                    border.Left.Style =
                    border.Right.Style = ExcelBorderStyle.Thin;
            }

            var rowIndex = dtContent.HeaderRowIndex;
            foreach (var s in dtContent.DataTable.Rows)
            {
                rowIndex++;
                ws.Row(rowIndex).Height = ws.Row(dtContent.HeaderRowIndex + 1).Height;
            }
            for (var i = 1; i <= 10; i++)
            {
                ws.Column(i).AutoFit();
            }
        }
        //public MemoryStream ExportDataTableFile(ExcelDataTableContent dtContent)
        //{
        //    using (var s = new ExcelPackage())
        //    {
        //        var ws = CreateSheet(s, "Sheet 1");

        //        ExportDatatable(ws, dtContent);

        //        return GetByteFromMs(s);
        //    }
        //}
        //public MemoryStream ExportSingleSheetFile(ExcelSheetContent sheetContent, string filePath)
        //{
        //    var file = GetFileInfo(filePath);
        //    if (file == null) return null;

        //    using (var s = new ExcelPackage(file))
        //    {
        //        BindData(sheetContent, s);

        //        return GetByteFromMs(s);
        //    }
        //}
        //public MemoryStream ExportWorkbookFile(ExcelWorkbookContent wbContent, string filePath)
        //{
        //    var file = GetFileInfo(filePath);
        //    if (file == null) return null;

        //    if (wbContent.SheetDatas == null) return null;

        //    using (var s = new OfficeOpenXml.ExcelPackage(file))
        //    {
        //        foreach (var sheetContent in wbContent.SheetDatas) BindData(sheetContent, s);

        //        return GetByteFromMs(s);
        //    }
        //}

        //public MemoryStream ExportWorkbook(ExcelWorkbookContent wbContent)
        //{
        //    if (wbContent.SheetDatas == null) return null;

        //    using (var s = new OfficeOpenXml.ExcelPackage())
        //    {
        //        foreach (var sheetContent in wbContent.SheetDatas) BindData(sheetContent, s);

        //        return GetByteFromMs(s);
        //    }
        //}

        //public ExcelWorkbookContent GetWorkbookContent(DataTable dt)
        //{
        //    var dataTableContent = new ExcelDataTableContent { IsExportHeader = true, DataTable = dt, HeaderColIndex = 1, HeaderRowIndex = 1, IsBorderCell = true };
        //    var wb = new ExcelWorkbookContent
        //    {
        //        SheetDatas = new List<ExcelSheetContent> {
        //            new ExcelSheetContent { DataTableContent = dataTableContent, SheetName = "Sheet1" }
        //        }
        //    };
        //    return wb;
        //}

        //public ExcelWorkbookContent GetWorkbookContent(DataTable dt, int headerColIndex, int headerRowIndex)
        //{
        //    var dataTableContent = new ExcelDataTableContent { IsExportHeader = true, DataTable = dt, HeaderColIndex = headerColIndex, HeaderRowIndex = headerRowIndex, IsBorderCell = true };
        //    var wb = new ExcelWorkbookContent
        //    {
        //        SheetDatas = new List<ExcelSheetContent> {
        //            new ExcelSheetContent { DataTableContent = dataTableContent, SheetName = "Sheet1" }
        //        }
        //    };
        //    return wb;
        //}

        //#region Private Funciton


        //private static void BindData(ExcelSheetContent sheetContent, ExcelPackage s)
        //{
        //    var ws = s.Workbook.Worksheets[sheetContent.SheetName] ?? CreateSheet(s, sheetContent.SheetName);
        //    ExportListCellData(ws, sheetContent.CellContents);
        //    ExportDatatable(ws, sheetContent.DataTableContent);

        //}
        //private static void ExportDatatable(ExcelWorksheet ws, ExcelDataTableContent dtContent)
        //{
        //    var rowEndIndex = dtContent.DataTable.Rows.Count + dtContent.HeaderRowIndex;
        //    var colEndIndex = dtContent.DataTable.Columns.Count + dtContent.HeaderColIndex - 1;

        //    var dataRange = ws.Cells[dtContent.HeaderRowIndex, dtContent.HeaderColIndex, rowEndIndex, colEndIndex];

        //    dataRange.LoadFromDataTable(dtContent.DataTable, dtContent.IsExportHeader);

        //    if (dtContent.IsBorderCell)
        //    {
        //        //Setting Top/left,right/bottom borders.
        //        var border = dataRange.Style.Border;
        //        border.Bottom.Style =
        //            border.Top.Style =
        //            border.Left.Style =
        //            border.Right.Style = ExcelBorderStyle.Thin;
        //    }

        //    var rowIndex = dtContent.HeaderRowIndex;
        //    foreach (var s in dtContent.DataTable.Rows)
        //    {
        //        rowIndex++;
        //        ws.Row(rowIndex).Height = ws.Row(dtContent.HeaderRowIndex + 1).Height;
        //    }
        //}
        //private static void ExportListCellData(OfficeOpenXml.ExcelWorksheet ws, List<ExcelCellContent> listCellContents)
        //{
        //    if (listCellContents == null) return;
        //    foreach (var cellContent in listCellContents)
        //    {
        //        var cell = ws.Cells[cellContent.RowIndex, cellContent.ColIndex];

        //        if (cellContent.PictureData == null)
        //        {
        //            cell.Value = cellContent.Content;
        //            if (!string.IsNullOrEmpty(cellContent.FormatValue))
        //                cell.StyleName = cellContent.FormatValue;
        //        }
        //        else
        //        {
        //            Image logo = null;

        //            if (new Uri(cellContent.PictureData.Path).IsFile)
        //                logo = Image.FromFile(cellContent.PictureData.Path);
        //            else
        //            {
        //                var wc = new WebClient();
        //                var imageStream = wc.OpenRead(cellContent.PictureData.Path);
        //                if (imageStream != null)
        //                    logo = Image.FromStream(imageStream);
        //            }

        //            var picture = ws.Drawings.AddPicture(cellContent.Content, logo);
        //            picture.From.Column = cellContent.ColIndex - 1;
        //            picture.From.Row = cellContent.RowIndex - 1;
        //            picture.SetSize(cellContent.PictureData.Width, cellContent.PictureData.Height);
        //        }
        //    }
        //}
        //private static FileInfo GetFileInfo(string tempPath)
        //{
        //    return string.IsNullOrEmpty(tempPath) ? null : new FileInfo(tempPath);
        //}
        //private static ExcelWorksheet CreateSheet(ExcelPackage package, string sheetName)
        //{
        //    package.Workbook.Worksheets.Add(sheetName);
        //    var ws = package.Workbook.Worksheets[sheetName];
        //    ws.Name = sheetName; //Setting Sheet's name
        //    ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
        //    ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

        //    return ws;
        //}
        //private static MemoryStream GetByteFromMs(ExcelPackage s)
        //{
        //    return new MemoryStream(s.GetAsByteArray());
        //}


        //#endregion

    }
}
