﻿using System.Data;
using System.IO;

namespace CRM.ExportHelper
{
    public interface IExportHelper
    {
        MemoryStream ExportDataTableFile(ExcelDataTableContent dtContent);
        MemoryStream ExportSingleSheetFile(ExcelSheetContent sheetContent, string filePath);
        MemoryStream ExportWorkbookFile(ExcelWorkbookContent wbContent, string filePath);
        MemoryStream ExportWorkbook(ExcelWorkbookContent wbContent);
        ExcelWorkbookContent GetWorkbookContent(DataTable dt);
        ExcelWorkbookContent GetWorkbookContent(DataTable dt, int headerColIndex, int headerRowIndex);

    }
}
