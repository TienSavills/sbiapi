﻿using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;

namespace CRM.EntityFrameworkCore
{
    [DependsOn(
        typeof(SBICoreModule), 
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
   
    public class SBIEntityFrameworkModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<SBIDbContext>(options => 
                {
                    if (options.ExistingConnection != null)
                    {
                        SBIDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        SBIDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
                //Configuration.Modules.AbpEfCore().AddDbContext<SBIMapContext>(options =>
                //{
                //    options.DbContextOptions.UseSqlServer(options.ConnectionString);
                //});
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SBIEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            //if (!SkipDbSeed)
            //{
            //    SeedHelper.SeedHostDb(IocManager);
            //}
        }
    }
}
