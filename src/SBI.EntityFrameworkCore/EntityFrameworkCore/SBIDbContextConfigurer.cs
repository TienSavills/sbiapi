using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace CRM.EntityFrameworkCore
{
    public static class SBIDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<SBIDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<SBIDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
