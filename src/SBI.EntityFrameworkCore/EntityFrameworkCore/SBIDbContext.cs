﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using CRM.Authorization.Roles;
using CRM.Authorization.Users;
using CRM.Entities.PropertyAddresss;
using CRM.Entities.PropertyFacilityMaps;
using CRM.Entities.PropertyTypeMaps;
using CRM.Entities.Company;
using CRM.Entities.Countries;
using CRM.Entities.Districs;
using CRM.Entities.Facilities;
using CRM.Entities.Properties;
using CRM.Entities.ProjectAddresss;
using CRM.Entities.ProjectFacilityMaps;
//using CRM.Entities.ProjectLandlord;
//using CRM.Entities.ProjectTenant;
using CRM.Entities.ProjectTypeMaps;
using CRM.Entities.Provinces;
using CRM.Entities.Wards;
using CRM.MultiTenancy;
using CRM.Entities.AttachmentDocuments;
//using CRM.Entities.PropertyTenants;
//using CRM.Entities.PropertyLandlords;
using CRM.Entities.Projects;
using CRM.Entities.PropertyValidation;
using CRM.Entities.ConstructionStatus;
using CRM.Entities.PropertyTypes;
//using CRM.Entities.TenantTypes;
using CRM.Entities.Grades;
using CRM.Entities.PropertyGradeMaps;
using CRM.Entities.ProjectGradeMaps;
using CRM.Entities.Currencys;
using CRM.Entities.CurrencyRates;
using CRM.Entities.PropertyClaims;
using CRM.Entities.ProjectClaims;
using CRM.Entities.ProperyConstructionMaps;
using CRM.Entities.ProjectConstructionMaps;
using CRM.Entities.DocumentTypes;
using CRM.Entities.TenureTypes;
using CRM.Entities.ProjectStates;
using CRM.Entities.RoomTypes;
using CRM.Entities.ProjectClaimRoomTypeMaps;
using CRM.Entities.MacroIndicators;
using CRM.Entities.CompanyAddress;
using CRM.Entities.Contacts;
using CRM.Entities.ContactAddress;
using CRM.Entities.LeadSource;
using CRM.Entities.Industry;
using CRM.Entities.ClientType;
using CRM.Entities.Level;
using CRM.Entities.CompanyEmail;
using CRM.Entities.CompanyOrganizationUnit;
using CRM.Entities.CompanyPhone;
using CRM.Entities.CompanyUser;
using CRM.Entities.Comment;
using CRM.Entities.CompanyCategory;
using CRM.Entities.OtherCategory;
using CRM.Entities.ContactEmail;
using CRM.Entities.ContactOrganizationUnit;
using CRM.Entities.ContactPhone;
using CRM.Entities.ContactUser;
using CRM.Entities.ActivityAttendee;
using CRM.Entities.ActivityReminder;
using CRM.Entities.ActivityType;
using CRM.Entities.Activity;
using CRM.Entities.ActivityOrganizationUnit;
using CRM.Entities.ActivityUser;
using CRM.Entities.Requests;
using CRM.Entities.RequestUser;
using CRM.Entities.RequestOrganizationUnit;
using CRM.Entities.PropertyUser;
using CRM.Entities.PropertyOrganizationUnit;
using CRM.Entities.CompanyContact;
using CRM.Entities.RequestContact;
using CRM.Entities.PropertyContact;
using CRM.Entities.OpportunityContact;
using CRM.Entities.OpportunityProperty;
using CRM.Entities.OpportunityInstruction;
using CRM.Entities.OpportunityAssetClass;
using CRM.Entities.OpportunityUser;
using CRM.Entities.OpportunityOrganizationUnit;
using CRM.Entities.Opportunity;
using CRM.Entities.OpportuityCategory;
using CRM.Entities.Deal;
using CRM.Entities.DealOpportunity;
using CRM.Entities.DealShare;
using CRM.Entities.Payment;
using CRM.Entities.Instruction;
using CRM.Entities.AssetClass;
using CRM.Entities.OpportunityLeadUser;
using Abp.Authorization.Users;
using CRM.Application.Shared.OrganizationUnit.Dto;
using CRM.Entities.UserOrganizationUnitDetails;
using CRM.Entities.Floor;
using CRM.Entities.Unit;
using CRM.Entities.UnitCategory;
using CRM.Entities.UnitHistory;
using CRM.Entities.ProjectCategory;
using CRM.Entities.Campaign;
using CRM.Entities.TargetList;
using CRM.Entities.CampaignCategory;
using CRM.Entities.CampaignTarget;
using CRM.Entities.ClientInquiries;
using CRM.Entities.TargetContact;
using CRM.Entities.TargetRequest;
using CRM.Entities.HistoryVerifyEmail;
using CRM.Entities.EmailHistory;
using CRM.Entities.OpportunityProject;
using CRM.Entities.Financial;
using CRM.Storage;
using CRM.Entities.CommentSendMail;
using CRM.Entities.CompanyTypeMap;
using CRM.Entities.ContactTypeMap;
using CRM.Entities.DealAdjust;
using CRM.Entities.Facing;
using CRM.Entities.Inquiries;
using CRM.Entities.InquiryAddress;
using CRM.Entities.InquiryCategory;
using CRM.Entities.InquiryFacilityMaps;
using CRM.Entities.InquiryUsers;
using CRM.Entities.InquiryViewMaps;
using CRM.Entities.ListingCategory;
using CRM.Entities.Listings;
using CRM.Entities.ListingUsers;
using CRM.Entities.News;
using CRM.Entities.ProjectTransportationMaps;
using CRM.Entities.Transportation;
using CRM.Entities.UnitAddress;
using CRM.Entities.UnitFacilityMaps;
using CRM.Entities.UnitViewMaps;
using CRM.Entities.View;

namespace CRM.EntityFrameworkCore
{
    public class SBIDbContext : AbpZeroDbContext<Tenant, Role, User, SBIDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<AttachmentDocument> AttachmentDocument { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        //public virtual DbSet<PropertyTenant> PropertyTenant { get; set; }
        //public virtual DbSet<PropertyLandlord> PropertyLandlord { get; set; }
        public virtual DbSet<BinaryObject> BinaryObject { get; set; }
        public virtual DbSet<PropertyAddress> PropertyAddress { get; set; }
        public virtual DbSet<PropertyFacilityMap> PropertyFacilityMap { get; set; }
        public virtual DbSet<PropertyTypeMap> PropertyTypeMap { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CompanyAddress> CompanyAddress { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<ContactAddress> ContactAddress { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<Facility> Facility { get; set; }
        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<ProjectAddress> ProjectAddress { get; set; }
        public virtual DbSet<ProjectFacilityMap> ProjectFacilityMap { get; set; }
        //public virtual DbSet<ProjectLandlord> ProjectLandlord { get; set; }
        public virtual DbSet<ConstructionStatus> ConstructionStatus { get; set; }
        //public virtual DbSet<ProjectTenant> ProjectTenant { get; set; }
        public virtual DbSet<PropertyType> PropertyType { get; set; }
        public virtual DbSet<ProjectTypeMap> ProjectTypeMap { get; set; }
        public virtual DbSet<ProjectCategory> ProjectCategory { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<Ward> Ward { get; set; }
        public virtual DbSet<PropertyValidation> PropertyValidation { get; set; }
        //public virtual DbSet<TenantType> TenantType { get; set; }
        public virtual DbSet<Grade> Grade { get; set; }
        public virtual DbSet<ProjectGradeMap> ProjectGradeMap { get; set; }
        public virtual DbSet<PropertyGradeMap> PropertyGradeMap { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<CurrencyRate> CurrencyRate { get; set; }
        public virtual DbSet<PropertyClaim> PropertyClaim { get; set; }
        public virtual DbSet<ProjectClaim> ProjectClaim { get; set; }
        public virtual DbSet<PropertyConstructionMap> PropertyConstructionMap { get; set; }
        public virtual DbSet<ProjectConstructionMap> ProjectConstructionMap { get; set; }
        public virtual DbSet<TenureType> TenureType { get; set; }
        public virtual DbSet<ProjectState> ProjectState { get; set; }
        public virtual DbSet<RoomType> RoomType { get; set; }
        public virtual DbSet<ProjectClaimRoomTypeMap> ProjectClaimRoomTypeMap { get; set; }
        public virtual DbSet<LeadSource> LeadSource { get; set; }
        public virtual DbSet<Industry> Industry { get; set; }
        public virtual DbSet<ClientType> ClientType { get; set; }
        public virtual DbSet<Instruction> Instruction { get; set; }
        public virtual DbSet<AssetClass> AssetClass { get; set; }
        public virtual DbSet<Level> Level { get; set; }
        public virtual DbSet<CompanyCategory> CompanyCategory { get; set; }
        public virtual DbSet<OtherCategory> OtherCategory { get; set; }
        public virtual DbSet<CompanyEmail> CompanyEmail { get; set; }
        public virtual DbSet<CompanyOrganizationUnit> CompanyOrganizationUnit { get; set; }
        public virtual DbSet<CompanyPhone> CompanyPhone { get; set; }
        public virtual DbSet<CompanyUser> CompanyUser { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<ContactEmail> ContactEmail { get; set; }
        public virtual DbSet<ContactOrganizationUnit> ContactOrganizationUnit { get; set; }
        public virtual DbSet<ContactPhone> ContactPhone { get; set; }
        public virtual DbSet<ContactUser> ContactUser { get; set; }

        public virtual DbSet<Activity> Activity { get; set; }
        public virtual DbSet<ActivityAttendee> ActivityAttendee { get; set; }
        public virtual DbSet<ActivityReminder> ActivityReminder { get; set; }
        public virtual DbSet<ActivityType> ActivityType { get; set; }
        public virtual DbSet<ActivityOrganizationUnit> ActivityOrganizationUnit { get; set; }
        public virtual DbSet<ActivityUser> ActivityUser { get; set; }
        public virtual DbSet<Request> Request { get; set; }
        public virtual DbSet<RequestUser> RequestUser { get; set; }
        public virtual DbSet<RequestOrganizationUnit> RequestOrganizationUnit { get; set; }
        public virtual DbSet<PropertyUser> PropertyUser { get; set; }
        public virtual DbSet<PropertyOrganizationUnit> PropertyOrganizationUnit { get; set; }
        public virtual DbSet<CompanyContact> CompanyContact { get; set; }
        public virtual DbSet<RequestContact> RequestContact { get; set; }
        public virtual DbSet<PropertyContact> PropertyContact { get; set; }
        public virtual DbSet<Entities.Opportunity.Opportunity> Opportunity { get; set; }
        public virtual DbSet<OpportunityContact> OpportunityContact { get; set; }
        public virtual DbSet<OpportunityProperty> OpportunityProperty { get; set; }
        public virtual DbSet<OpportunityProject> OpportunityProject{ get; set; }
        public virtual DbSet<OpportunityInstruction> OpportunityInstruction { get; set; }
        public virtual DbSet<OpportunityAssetClass> OpportunityAssetClass { get; set; }
        public virtual DbSet<OpportunityUser> OpportunityUser { get; set; }
        public virtual DbSet<OpportunityLead> OpportunityLead { get; set; }
        public virtual DbSet<OpportunityOrganizationUnit> OpportunityOrganizationUnit { get; set; }
        public virtual DbSet<OpportunityCategory> OpportunityCategory { get; set; }
        public virtual DbSet<Deal> Deal { get; set; }
        public virtual DbSet<DealOpportunity> DealOpportunity { get; set; }
        public virtual DbSet<DealShare> DealShare { get; set; }
        public virtual DbSet<DealAdjust> DealAdjust { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<UserOrganizationUnitDetails> UserOrganizationUnitDetails { get; set; }
        public virtual DbSet<Floor> Floor { get; set; }
        public virtual DbSet<Unit> Unit { get; set; }
        public virtual DbSet<UnitCategory> UnitCategory { get; set; }
        public virtual DbSet<UnitHistory> UnitHistory { get; set; }
        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<TargetList> TargetList { get; set; }
        public virtual DbSet<CampaignCategory> CampaignCategory { get; set; }
        public virtual DbSet<CampaignTargetList> CampaignTargetList { get; set; }
        public virtual DbSet<TargetContact> TargetContact { get; set; }
        public virtual DbSet<TargetRequest> TargetRequest { get; set; }
        public virtual DbSet<HistoryVerifyEmail> HistoryVerifyEmail { get; set; }
        public virtual DbSet<EmailHistory> EmailHistory { get; set; }
        public virtual DbSet<Capacity> Capacity { get; set; }
        public virtual DbSet<CommentSendEmail> CommentSendEmail { get; set; }
        public virtual DbSet<CompanyTypeMap> CompanyTypeMap { get; set; }
        public virtual DbSet<ContactTypeMap> ContactTypeMap { get; set; }
        public virtual DbSet<View> View { get; set; }
        public virtual DbSet<UnitAddress> UnitAddress { get; set; }
        public virtual DbSet<UnitViewMaps> UnitViewMaps { get; set; }
        public virtual DbSet<UnitFacilityMaps> UnitFacilityMaps { get; set; }
        public virtual DbSet<Listings> Listings { get; set; }
        public virtual DbSet<ListingUsers> ListingUsers { get; set; }
        public virtual DbSet<ListingCategory> ListingCategory { get; set; }
        public virtual DbSet<InquiryUsers> InquiryUsers { get; set; }
        public virtual DbSet<Inquiry> Inquiry { get; set; }
        public virtual DbSet<InquiryViewMaps> InquiryViewMaps { get; set; }
        public virtual DbSet<InquiryCategory> InquiryCategory { get; set; }
        public virtual DbSet<InquiryFacilityMaps> InquiryFacilityMaps { get; set; }
        public virtual DbSet<InquiryAddress> InquiryAddress { get; set; }
        public virtual DbSet<Transportation> Transportation { get; set; }
        public virtual DbSet<ProjectTransportationMaps> ProjectTransportationMaps { get; set; }
        public virtual DbSet<Facing> Facing { get; set; }
        public virtual DbSet<NewLanguageTexts> NewLanguageTexts { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NewsCategory> NewsCategory { get; set; }
        public virtual DbSet<ClientInquiryUsers> ClientInquiryUsers { get; set; }
        public virtual DbSet<ClientInquiry> ClientInquiry { get; set; }

        public SBIDbContext(DbContextOptions<SBIDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
