﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using CRM.Configuration;
using CRM.Web;

namespace CRM.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class SBIDbContextFactory : IDesignTimeDbContextFactory<SBIDbContext>
    {
        public SBIDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SBIDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            SBIDbContextConfigurer.Configure(builder, configuration.GetConnectionString(SBIConsts.ConnectionStringName));

            return new SBIDbContext(builder.Options);
        }
    }
}
